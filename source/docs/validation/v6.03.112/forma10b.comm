# 3D point element in combined tension + shear
# J2 plasticity + isotropic hardening
DEBUT(LANG='EN')

# Material parameters
E = 195000.0
nu = 0.3
E_T = 1930.0
sig_y = 181.0

# Define material
steel = DEFI_MATERIAU(ELAS = _F(E = E,
                                NU = nu),
                      ECRO_LINE = _F(D_SIGM_EPSI = E_T,
                                     SY = sig_y))

# Define homogeneous stress functions
# Tension
sigma_fn = DEFI_FONCTION(NOM_PARA = 'INST',
                         VALE = (0.0, 0.0,
                                 1.0, 151.2,
                                 2.0, 257.2,
                                 3.0, 259.3,
                                 4.0, 0),
                         PROL_DROITE = 'EXCLU',
                         PROL_GAUCHE = 'EXCLU')

# Shear
tau_fn = DEFI_FONCTION(NOM_PARA = 'INST',
                       VALE = (0.0, 0.0,
                               1.0, 93.1,
                               2.0, 33.1,
                               3.0, 0.0,
                               4.0, 0.0),
                       PROL_DROITE = 'EXCLU',
                       PROL_GAUCHE = 'EXCLU')

# Define time steps
num_steps = 1
l_times = DEFI_LIST_REEL(DEBUT = 0.0,
                         INTERVALLE = (_F(JUSQU_A = 0.81,
                                          NOMBRE = num_steps),
                                       _F(JUSQU_A = 1.0,
                                          NOMBRE = num_steps),
                                       _F(JUSQU_A = 2.0,
                                          NOMBRE = num_steps),
                                       _F(JUSQU_A = 3.0,
                                          NOMBRE = num_steps)))

# Event-driven time-stepping
t_auto = DEFI_LIST_INST(METHODE = 'MANUEL',
                        DEFI_LIST = _F(LIST_INST = l_times),
                        ECHEC = _F(EVENEMENT = 'DELTA_GRANDEUR',
                                   VALE_REF = 0.2e-2,
                                   NOM_CHAM = 'VARI_ELGA',
                                   NOM_CMP = 'V1',
                                   SUBD_NIVEAU = 10))

# Do point simulation
result = SIMU_POINT_MAT(COMPORTEMENT = _F(RELATION = 'VMIS_ISOT_LINE'),
                        MATER = steel,
                        INCREMENT = _F(LIST_INST = t_auto),
                        NEWTON = _F(PREDICTION='ELASTIQUE',
                                    MATRICE = 'TANGENTE',
                                    REAC_ITER = 1),
                        CONVERGENCE = _F(RESI_GLOB_RELA = 1.E-8),
                        SIGM_IMPOSE = _F(SIXX = sigma_fn,
                                         SIXY = tau_fn),
                        INFO = 1)

#  Write the results to a table
IMPR_TABLE(TABLE = result,
           SEPARATEUR = ',')

# Save the strains for plotting
eps_xx = RECU_FONCTION(TABLE = result,
                       PARA_X = 'INST',
                       PARA_Y = 'EPXX')
eps_xy = RECU_FONCTION(TABLE = result,
                       PARA_X = 'INST',
                       PARA_Y = 'EPXY')

IMPR_FONCTION(FORMAT = 'TABLEAU',
              SEPARATEUR = ',',
              COURBE = _F(FONCTION = eps_xx))
IMPR_FONCTION(FORMAT = 'TABLEAU',
              SEPARATEUR = ',',
              COURBE = _F(FONCTION = eps_xy))


# State A
TEST_TABLE(REFERENCE = 'ANALYTIQUE',
           VALE_CALC = 0.014829714,
           VALE_REFE = 0.014829999999999999,
           NOM_PARA = 'EPXX',
           TABLE = result,
           FILTRE = _F(PRECISION = 1.0000000000000001E-05,
                       NOM_PARA = 'INST',
                       VALE = 1.0))

TEST_TABLE(REFERENCE = 'ANALYTIQUE',
           VALE_CALC = 0.013601401,
           VALE_REFE = 0.0136014,
           NOM_PARA = 'EPXY',
           TABLE = result,
           FILTRE = _F(PRECISION = 1.0000000000000001E-05,
                       NOM_PARA = 'INST',
                       VALE = 1.0))

# State B
TEST_TABLE(REFERENCE = 'ANALYTIQUE',
           PRECISION = 5.0000000000000001E-3,
           VALE_CALC = 0.035401234,
           VALE_REFE = 0.035264999999999998,
           NOM_PARA = 'EPXX',
           TABLE = result,
           FILTRE = _F(PRECISION = 1.0000000000000001E-05,
                       NOM_PARA = 'INST',
                       VALE = 2.0))

TEST_TABLE(REFERENCE = 'ANALYTIQUE',
           PRECISION = 0.014999999999999999,
           VALE_CALC = 0.020245631,
           VALE_REFE = 0.020471,
           NOM_PARA = 'EPXY',
           TABLE = result,
           FILTRE = _F(PRECISION = 1.0000000000000001E-05,
                       NOM_PARA = 'INST',
                       VALE = 2.0))

# State C
TEST_TABLE(REFERENCE = 'AUTRE_ASTER',
           PRECISION = 5.0000000000000001E-3,
           VALE_CALC = 0.035412003,
           VALE_REFE = 0.035277700000000002,
           NOM_PARA = 'EPXX',
           TABLE = result,
           FILTRE = _F(PRECISION = 1.0000000000000001E-05,
                       NOM_PARA = 'INST',
                       VALE = 3.0))

TEST_TABLE(REFERENCE = 'AUTRE_ASTER',
           PRECISION = 0.014999999999999999,
           VALE_CALC = 0.020024964,
           VALE_REFE = 0.020246199999999999,
           NOM_PARA = 'EPXY',
           TABLE = result,
           FILTRE = _F(PRECISION = 1.0000000000000001E-05,
                       NOM_PARA = 'INST',
                       VALE = 3.0))

# End
FIN();
