.. role:: red
.. role:: blue
.. role:: green
.. role:: gray
.. role:: python(code)
   :language: python
.. |br| raw:: html

   <br/>

.. _r7.01.08:

***********************************************************************
**[R7.01.08]** : MAZARS damage model for concrete
***********************************************************************

The :blue:`MAZARS` model makes it possible to describe the elastic-damage behavior of concrete. This model is 3D, 
isotropic, and is based on a damage criterion expressed in terms of strain and describing traction-compression 
asymmetry. The "original" model does not take into account of the restoration of stiffness in the event of re-closing 
of cracks, and does not take into account any plastic deformations or viscous effects which may be observed during 
the deformation of a concrete. The version implemented in ``Code_Aster`` takes into account the latest improvements. 
This reformulation of the Mazars model of the 1980s makes it possible to better describe the behavior of the concrete 
in biaxial-compression and in pure shear.  

The 1D version of the model makes it possible to account for the restoration 
of stiffness in the event of re-closing of microcracks, it is only used with multi-fiber beams 
:ref:`[R5.03.09] <r5.03.09>`.

.. _r7.01.08.1:

Introduction
============

.. _r7.01.08.1.1:

An elastic-damage constitutive model
------------------------------------

The :blue:`MAZARS` model [#bib1]_ is a simple model, reputed to be robust, based on damage mechanics [#bib2]_, which 
makes it possible to describe the reduction in the material stiffness due to the creation of micro-cracks in the 
concrete. It is based on a single scalar internal variable :math:`D` describing isotropic damage, but distinguishes
between damage in tension and damage in compression. The version implemented in ``Code_Aster`` corresponds to the 
reformulation of 2012 [#bib3]_. The main modification, compared to the original model, is the introduction of a new 
internal variable, :math:`Y`, corresponding to the maximum equivalent strain reached during the loading. Therefore, the 
damage is no longer the internal variable in the revised model. In addition, its evolution model is simplified in 
order to eliminate the notions of damage of tension and compression.

Contrary to the model :blue:`ENDO_ISOT_BETON`, this model does not include the phenomenon of re-closing of cracks 
(restoration of stiffness). Moreover, the :blue:`MAZARS` model does not take into account any plastic deformations or 
viscous effects which can be observed during the deformation of concrete.

The 1D version of the :blue:`MAZARS` model is described in :ref:`[R5.03.09] <r5.03.09>`. In this specific case, the 
model is able to account for the phenomenon of re-closing cracks. The 1D version of the model can only be used with
multi-fiber beams.

.. _r7.01.08.1.2:

Limitations and methods of regularization
-----------------------------------------

Like all softening laws, the :blue:`MAZARS` model poses difficulties related to the phenomenon of strain localization.

Physically, the heterogeneity of the micro-structure of concrete induces interactions at a distance between the cracks 
formed [#bib4]_. Thus, the deformations are located in a thin band, called a localization band, and form macro-cracks. 
The stress state at a material point can no longer be described solely by the characteristics at the point but must
also take into account its surrounding points. In the case of a local model, indication regarding the fracture length
scale is not includes. Therefore, no information is available on the width of the localization band which then becomes 
zero with increasing mesh refinement. This results in rupture without dissipation of energy which is physically
unacceptable.

Mathematically, localization makes the problem ill-posed because the softening causes a loss of ellipticity of the 
differential equations that describe the process of deformations [#bib5]_. Numerical solutions do not converge to 
physically acceptable solutions despite mesh refinements.

.. _r7.01.08.1.3:

Thermal coupling
----------------

For some studies, it may be interesting to be able to take into account the modification of material parameters due 
to the effect of temperature. This is possible in ``Code_Aster`` (:blue:`MAZARS_FO` combined or not with 
:blue:`ELAS_FO`). The assumptions made for thermal coupling are:

* thermal expansion is assumed to be linear:

  .. math::
     :label: eq_r7.01.08.1.3-1

     \boldsymbol{\varepsilon}^{\text{th}} = \alpha(T - T_{\text{ref}}) \mathbf{I}_d

  with :math:`\alpha` = constant or function of the temperature,

* thermo-mechanical interactions are not taken into account, i.e. the effect of the state of mechanical stress on 
  the thermal deformation of the concrete is not modeled,

* concerning the evolution of the material parameters with the temperature, it is considered that these depend not 
  on the current temperature but on the maximum temperature :math:`T_{\text{max}}` experienced by the material during 
  its history, (effect cited in the literature),

* only the elastic deformation (mechanical) induces damage.

.. admonition:: Remark 

   Due to numerical constraints, the initial value of :math:`T_{\text{max}}` is initialized to 0.  As a consequence, 
   one cannot use material parameters defined for negative temperatures (if necessary, this problem can however be 
   circumvented by entering all the temperatures in Kelvin instead of C).

.. _r7.01.08.1.4:

Mazars' model in the presence of a dessication or hydration field
------------------------------------------------------------------

The use of :blue:`ELAS_FO` and/or :blue:`MAZARS_FO` under operator :red:`DEFI_MATERIAU` allows us to define a 
constitutive model that depend on the material parameters of dessication or hydration. Furthermore, the strains 
associated with endogenous shrinkage :math:`\boldsymbol{\varepsilon}_{\text{re}}` and dessication shinkage 
:math:`\boldsymbol{\varepsilon}_{\text{rd}}` are taken into account in the model, in the following (linear) form 
(cf :ref:`[R7.01.12] <r7.01.12>`):

.. math::
   :label: eq_r7.01.08.1.4-1

   \boldsymbol{\varepsilon}^{\text{re}} = -\beta \xi \mathbf{I}_d

.. math::
   :label: eq_r7.01.08.1.4-2

   \boldsymbol{\varepsilon}^{\text{rd}} = -\kappa ( C_{\text{ref}} - C) \mathbf{I}_d

where :math:`\xi` is the hydration, :math:`C` the water concentration (dessication field in the terminology 
of ``Code_Aster``), :math:`C_{\text{ref}}` is the initial water concentration (or reference dessication). Finally,
:math:`\beta` is the endogenous shrinkage coefficient and :math:`\kappa` the dessication shrinkage coefficient to 
be entered in :red:`DEFI_MATERIAU`, factor keyword :blue:`ELAS_FO`, operands :blue:`B_ENDO` and :blue:`K_DESSIC`. As 
we have said in the previous section, the choice that was made in the implementation of the :blue:`MAZARS` model is 
that only the elastic strain induces damage. Consequently, if we model a concrete specimen which dries or hydrates 
freely and uniformly, we will obtain a non-zero strain field and a perfectly zero stress field.

We first present the description of the model then some data on the identification of parameters. Finally, we expose 
the principles of numerical implementation in ``Code_Aster``.

.. _r7.01.08.2:

MAZARS models
=============

.. _r7.01.08.2.1:

Original Mazars model
---------------------

The MAZARS model was developed within the framework of the mechanics of the damage. This model is detailed in the 
thesis of MAZARS [#bib1]_.

The stress is given by the following relation:

.. math::
   :label: eq_r7.01.08.2.1-1

   \boldsymbol{\sigma} = (1- D ) \mathbb{E} \boldsymbol{\varepsilon}^e

where :

* :math:`\mathbb{E}` is the stiffness matrix,
* :math:`D` is the damage variable
* :math:`\boldsymbol{\varepsilon}^e` is the elastic strain 
  :math:`\boldsymbol{\varepsilon}^e = \boldsymbol{\varepsilon} - \boldsymbol{\varepsilon}^{\text{th}} - \boldsymbol{\varepsilon}^{\text{rd}} - \boldsymbol{\varepsilon}^{\text{re}}`
* :math:`\boldsymbol{\varepsilon}^{\text{th}} = \alpha (T - T_{\text{ref}}) \mathbf{I}_d` is the thermal expansion
* :math:`\boldsymbol{\varepsilon}^{\text{re}} = -\beta \xi \mathbf{I}_d` is the endogenous shrinkage (related to 
  hydration)
* :math:`\boldsymbol{\varepsilon}^{\text{rd}} = -\kappa (C_{\text{ref}} - C) \mathbf{I}_d` is the shrinkage due to 
  desiccation (related to drying)

The damage variable :math:`D` is between 0 for healthy materials, and 1 for damaged material. The damage is controlled 
by the equivalent strain :math:`\varepsilon_{\text{eq}}` allowing us convert a triaxial state to a uniaxial state. As 
tensions are primary in the phenomenon of concrete cracking, the equivalent strain introduced is defined starting 
from the positive eigenvalues of the strain tensor, that is to say:

.. math::

   \varepsilon_{\text{eq}} = 
     \sqrt{\langle\boldsymbol{\varepsilon}\rangle_{+} : \langle\boldsymbol{\varepsilon}\rangle_{+}}

where in the principal reference coordinate system of the strain tensor:

.. math::
   :label: eq_r7.01.08.2.1-2

   \varepsilon_{\text{eq}} = \sqrt{\langle\varepsilon_1\rangle_{+}^2 + 
                                   \langle\varepsilon_2\rangle_{+}^2 + 
                                   \langle\varepsilon_3\rangle_{+}^2}

knowing that the positive part :math:`\langle\rangle_{+}` is defined such that if :math:`\varepsilon_i`
is the principal strain in direction :math:`i` :

.. math::
   :label: eq_r7.01.08.2.1-3

   \begin{cases}
     \langle\varepsilon_i\rangle_{+} = \varepsilon_i & \quad \text{if} \quad \varepsilon_i \ge 0 \\
     \langle\varepsilon_i\rangle_{+} = 0 & \quad \text{if} \quad \varepsilon_i < 0
   \end{cases}

.. admonition:: Remark 

   In the case of a thermomechanical loading, only the elastic strain 
   :math:`\boldsymbol{\varepsilon}^e = \boldsymbol{\varepsilon} - \boldsymbol{\varepsilon}^{\text{th}}` 
   contributes to the evolution of the damage from where: 

   .. math::

      \varepsilon_{\text{eq}} = \sqrt{\langle\boldsymbol{\varepsilon}^e\rangle_{+}: 
                                      \langle\boldsymbol{\varepsilon}^e\rangle_{+}}.

:math:`\varepsilon_{\text{eq}}` is an indicator of the state of tension in the material which generates the damage. 
This quantity defines the load surface :math:`f` such that:

.. math::
   :label: eq_r7.01.08.2.1-4

   f = \varepsilon_{\text{eq}} - K(D) = 0

where :math:`K(D) = \varepsilon_{d0}` if :math:`D = 0`. :math:`\varepsilon_{d0}` is the threshold damage strain.

When the equivalent strain reaches this value, damage is activated. :math:`D` is defined as a combination of two modes 
of damage defined by :math:`D_t` and :math:`D_c`, varying between 0 and 1 according to the associated state of damage, 
and corresponding respectively to damage in tension and compression. The relationship between these variables is as 
follows:

.. math::
   :label: eq_r7.01.08.2.1-5

   D = \alpha_t^\beta D_t +\alpha_c^\beta D_c

where :math:`\beta` is a coefficient which is introduced later and used to improve the behavior in shear. Usually its 
value is fixed at 1.06. The coefficients :math:`\alpha_t` and :math:`\alpha_c` create the link between the damage and 
the state of tension or compression. When tension is activated :math:`\alpha_t = 1` whereas :math:`\alpha_t = 0`, and 
conversely in compression.

A particularity of this model is its explicit form which implies that all quantities are calculated directly without 
using a linearization algorithm such as Newton-Raphson. Thus, the damage evolution laws for :math:`D_t` and 
:math:`D_c` are functions only of the equivalent strain :math:`\varepsilon_{\text{eq}}`:

.. math::
   :label: eq_r7.01.08.2.1-6

   D_t = 1 - \frac{(1 - A_t)\varepsilon_{d0}}{\varepsilon_{\text{eq}}} - A_t \exp\left(-B_t(\varepsilon_{\text{eq}} -
                \varepsilon_{d0})\right)

.. math::
   :label: eq_r7.01.08.2.1-7

   D_c = 1 - \frac{(1 - A_c)\varepsilon_{d0}}{\varepsilon_{\text{eq}}} - A_c \exp\left(-B_c(\varepsilon_{\text{eq}} -
                \varepsilon_{d0})\right)

with :math:`A_t, A_c, B_t`, and :math:`B_c` being the material parameters to be identified. These parameters
allow modulation of the shape of the post-peak curve. They are obtained using tensile and compression tests.

.. _r7.01.08.2.2:

Revised Mazars model
--------------------

Although widely used, the Original Mazars model has shortcomings in the modeling of the behavior of concrete during 
shear and biaxial compression loading. A comparison between the load surfaces of the two models is given in :numref:`fig_r7.01.08.2.2-4`.

.. _fig_r7.01.08.2.2-1:

.. figure:: r7.01.08/fig1_r7.01.08.svg
   :height: 300 px
   :align: center

   Comparison of the surfaces of initiation of damage and rupture of Mazars models in the plane :math:`\sigma_3 = 0`
   for C30 concrete.

The revised formulation has two major modifications:

1. improvement of behavior in biaxial compression,
2. simplification and improvement of shear behavior.

The original 1980s Mazars model [#bib1]_ greatly underestimates the resistance of the concrete in biaxial compression. 
The first modification made in the revised model improves upon the original by correcting the 
equivalent strain when at least one principal stress is negative, using a variable :math:`\gamma` :

.. math::
   :label: eq_r7.01.08.2.2-1

   \varepsilon_{\text{eq}}^{\text{corr}} = \gamma \varepsilon_{\text{eq}}
     = \gamma \sqrt{\langle\boldsymbol{\varepsilon}\rangle_{+} : \langle\boldsymbol{\varepsilon}\rangle_{+}}

with :math:`\gamma` bounded between 0 and 1 and defined as:

.. math::
   :label: eq_r7.01.08.2.2-2

   \gamma = 
   \begin{cases}
     -\frac{\sqrt{\sum_i \langle\tilde{\sigma}_i\rangle_{-}^2}}{\sum_i \langle\tilde{\sigma}_i\rangle_{-}} & 
      \text{if at least one effective stress is negative} \\
     1 & \text{otherwise}
   \end{cases}

In this context, the effective stress is defined as:

.. math::
   :label: eq_r7.01.08.2.2-3

   \tilde{\boldsymbol{\sigma}} = \frac{\boldsymbol{\sigma}}{1- D}

The definition of :math:`\langle \cdot \rangle_{-}` is similar to :eq:`eq_r7.01.08.2.1-3` :

.. math::
   :label: eq_r7.01.08.2.2-4

   \begin{cases}
     \langle\tilde{\sigma}_i\rangle_{-} = \tilde{\sigma}_i & \quad \text{if} \quad \tilde{\sigma}_i \le 0 \\
     \langle\tilde{\sigma}_i\rangle_{-} = 0 & \quad \text{if} \quad \tilde{\sigma}_i > 0
   \end{cases}


where :math:`\tilde{\sigma}_i` is a principal effective stress.

The improvement in shear behavior is achieved by the introduction of a new internal variable: :math:`Y`. This 
corresponds to the maximum equivalent strain attained during loading. Its initial value :math:`Y_0` is 
:math:`\varepsilon_{d0}`. :math:`Y` is defined by the equation :

.. math::
   :label: eq_r7.01.08.2.2-5

   Y = \text{max}(\varepsilon_{d0}, \text{max}(\varepsilon_{\text{eq}}^{\text{corr}}))

The loading function is:

.. math::
   :label: eq_r7.01.08.2.2-6

   f = \varepsilon_{\text{eq}}^{\text{corr}} - Y

The evolution of the damage is given by:

.. math::
   :label: eq_r7.01.08.2.2-7

   D = 1 - \frac{(1-A) Y_0}{Y} - A \exp\left(- B(Y - Y_0)\right)

In this expression, it is the variables :math:`A` and :math:`B` which make it possible to reproduce the brittle 
behavior of concrete in tension and work hardened behavior in compression.
To best represent the experimental results, the following evolution laws have been chosen for :math:`A` and 
:math:`B`:

.. math::
   :label: eq_r7.01.08.2.2-8

   A = A_t \left[ 2r^2 (1-2k) - r (1-4k) \right] + A_c ( 2r^2 - 3r + 1)

and

.. math::
   :label: eq_r7.01.08.2.2-9

   B = r^2 B_t + (1-r^2) B_c

where the expression for :math:`r` is:

.. math::
   :label: eq_r7.01.08.2.2-10

   r = \frac{\sum_i \langle\tilde{\sigma}_i\rangle_{+}}{\sum_i ∣\tilde{\sigma}_i∣}


A new variable :math:`r` appears in these equations which tells us about the state of stress. When :math:`r` is 
equal to 1 (corresponding to the tension sector), the variables :math:`A` and :math:`B` are equivalent to the 
parameters :math:`A_t` and :math:`B_t`. Therefore, :eq:`eq_r7.01.08.2.2-7` is identical to :eq:`eq_r7.01.08.2.1-6`. 

Conversely, if :math:`r` is zero (corresponding to the compressions sector), then :math:`A = A_c, B = B_c` and 
:eq:`eq_r7.01.08.2.2-7` is identical to :eq:`eq_r7.01.08.2.1-7`. :numref:`fig_r7.01.08.2.2-2` shows the
values of these variables :math:`A, B, r` and :math:`\gamma` when :math:`\sigma 3 = 0` and how they change 
depending on the sign of the principal stresses.

.. _fig_r7.01.08.2.2-2:

.. figure:: r7.01.08/fig2_r7.01.08.png
   :height: 300 px
   :align: center

   Evolution of variables :math:`A`, :math:`B`, :math:`r` and :math:`\gamma` in the plane :math:`\sigma_3 = 0`.

In the equation :eq:`eq_r7.01.08.2.2-8` a new parameter appears: :math:`k`. It introduces an asymptote to the
curve :math:`\sigma` - :math:`\varepsilon` in shear and it is defined by:

.. math::
   :label: eq_r7.01.08.2.2-11

   k = \frac{A_{\text{shear}}}{A_t}

where :math:`A_{\text{shear}}` defines the residual stress in pure shear. It is similar to :math:`A_t` for
this loading case. The recommended value for :math:`k` is 0.7. A value of :math:`k` less than 1 is very useful in 
modeling the effects of friction between concrete and reinforcement in reinforced concrete structures because it 
induces a residual shear stress. For :math:`k = 1` one recovers the behavior of the original Mazars model 
(see :numref:`fig_r7.01.08.2.2-3`) .

.. _fig_r7.01.08.2.2-3:

.. figure:: r7.01.08/fig3_r7.01.08.svg
   :height: 300 px
   :align: center

   Stress-strain curve during a pure shear test at a Gauss point

The original model underestimates the strength of concrete in pure shear. The new formulation makes it possible to 
increase this resistance in pure shear passing from 2.5 MPa to 3.5 MPa for a C30 concrete. This value depends on those 
of the input material parameters (:math:`A_t, A_c, B_t`, and :math:`B_c`). The local response of the revised Mazars 
model under successive loading of tension-compression is shown in
:numref:`fig_r7.01.08.2.2-4`.

.. _fig_r7.01.08.2.2-4:

.. figure:: r7.01.08/fig4_r7.01.08.svg
   :height: 300 px
   :align: center

   Stress-strain response of the Mazars model for uniaxial loading.

:numref:`fig_r7.01.08.2.2-4` is used to display a number of characteristics of the :blue:`MAZARS` model:

* the damage affects the stiffness of the concrete,
* there are no irreversible deformations,
* the responses in traction and compression are quite asymmetrical

.. note::

   The original and revised Mazars models do not take into account the unilateral nature of the concrete, namely the 
   re-closing of cracks during transition from a state of tension in a state of compression.

.. _r7.01.08.3:

Identification of parameters
============================

Besides the thermo-elastic parameters :math:`E, \nu, \alpha`, the revised :blue:`MAZARS` model requires 6 material 
parameters: :math:`A_c, B_c, A_t, B_t, \varepsilon_{d0}, k`.

* :math:`\varepsilon_{d0}` is the damage threshold. It affects the peak stress but also the shape of the 
  post-peak curve. Indeed, the stress drop is much less when :math:`\varepsilon_{d0}` is small. In general,
  :math:`\varepsilon_{d0}` is between  0.5 and 1.5 :math:`10^{-4}`.

The coefficients :math:`A` and :math:`B` make it possible to modulate the shape of the post-peak curve. They are
defined by the equations :eq:`eq_r7.01.08.2.2-8` and :eq:`eq_r7.01.08.2.2-9` which depend on the parameters of the 
original Mazars model (:math:`A_t, B_t, A_c` and :math:`B_c`) and of :math:`r`:

* :math:`A` introduces a horizontal asymptote which is the axis of :math:`\varepsilon` for :math:`A = 1` and the 
  horizontal line passing through the peak for :math:`A = 0` (cf. :numref:`fig_r7.01.08.3-1`). In the tension regime, 
  :math:`A` is equivalent to :math:`A_t` (and vice versa in compressions regime: :\math:`A = A_c`). In general, 
  :math:`A_c` is between 1 and 2 and :math:`A_t` between 0.7 and 1.

* :math:`B` depending on its value may correspond to a sudden drop in stress (:math:`B` > 10,000 ) or a preliminary 
  phase of increase in stress followed, after passing through a maximum, by a more or less rapid decrease as one 
  can see in :numref:`fig_r7.01.08.3-2`. In the tensile domain, :math:`B` is equivalent to :math:`B_t` (and vice 
  versa in compression, :math:`B = B_c`). In general :math:`B_c` is between 1000 and 2000 and :math:`B_t` between 
  9000 and 21000 .

* :math:`k` introduces a horizontal asymptote in pure shear on the stress-strain curve if its value is different 
  from 1 for :math:`A_t = 1`, :eq:`eq_r7.01.08.2.2-11`. The value recommended is 0.7.

.. _fig_r7.01.08.3-1:

.. figure:: r7.01.08/fig5_r7.01.08.png
   :height: 300 px
   :align: center

   Influence of the parameter :math:`A_t`

.. _fig_r7.01.08.3-2:

.. figure:: r7.01.08/fig6_r7.01.08.png
   :height: 300 px
   :align: center

   Influence of the parameter :math:`B_t`

One way to obtain a set of parameters is to have the results of uniaxial tests in compression and tension (for tension 
we can use other types of tests, "Brazilian" tests for example). If one uses strain gradient regularization 
(see :numref:`r7.01.08.1.2`), it is recommended to fix the parameters of the model at the same time as the 
characteristic length scale :math:`L_c`. A few authors (see [#bib7]_) also suggest that we calibrate :math:`L_c` 
using experimental tests on several specimens of different sizes; in fact, the characteristic length is linked to 
the size of the zone of energy dissipation which could be at the origin of structural scale effects.

.. _r7.01.08.4:

Numerical solution
===================

.. _r7.01.08.4.1:

Evaluation of the internal variable :math:`Y`
---------------------------------------------

The calculation of :math:`Y` follows an explicit scheme. The steps are as follows:

* Calculation of elastic and thermal strains
* Calculation of the principal elastic stresses and evaluation of :math:`\gamma` (:eq:`eq_r7.01.08.2.2-2`).
* Calculation of the equivalent strain (:eq:`eq_r7.01.08.2.1-2` and :eq:`eq_r7.01.08.2.2-1`).
* Calculation of variables :math:`r`, :math:`A` and :math:`B`
* Calculation of the internal variable :math:`Y` (:eq:`eq_r7.01.08.2.2-5`).

  * If :math:`Y \le Y^{-}` then :math:`Y^{+} = Y^{-}`.
  * If :math:`Y > Y^{-}` then :math:`Y^{+} = Y`.

.. note:: 

   Currently the variable stored during calculations is :math:`\varepsilon_{\text{eq}}` so as not to modify
   the existing couplings with :blue:`UMLV`. A condition on the strictly increasing evolution of the damage 
   allows this simplification in the case where :math:`\gamma` varies.

.. _r7.01.08.4.2:

Damage assessment
-----------------

The damage is calculated in all cases with the equation :eq:`eq_r7.01.08.2.2-7`.

.. math::

   D = 1 - \frac{(1-A) Y_0}{Y} - A\exp\left( -B(Y - Y_0) \right)

It is important to specify that we impose on :math:`D` to be between 0 and 1 because it is possible to have 
values outside this range depending on the choice of material parameters.

.. _r7.01.08.4.3:

Calculation of stress
---------------------

After evaluating :math:`D`, we simply calculate:

.. math::
   :label: eq_r7.01.08.4.3-1

   \boldsymbol{\sigma} = (1-D) \mathbb{A} \boldsymbol{\varepsilon}^e

.. _r7.01.08.4.4:

Calculation of the tangent matrix
---------------------------------

One of the disadvantages of the Mazars model is the absence of a tangent matrix. It is not possible to compute 
this matrix because of the use of the MacCauley operator in the calculation of the equivalent strain 
:eq:`eq_r7.01.08.2.1-2`, of :math:`\gamma` :eq:`eq_r7.01.08.2.2-2` and :math:`r`. However, it is possible to use 
an approximation during radial loading.

We are looking for the tensor :math:`\mathbb{M}` such that 
:math:`\dot{\boldsymbol{\sigma}} = \mathbb{M} \dot{\boldsymbol{\varepsilon}}` knowing that 
:math:`\boldsymbol{\sigma} = (1-D) \mathbb{A} \boldsymbol{\varepsilon}`. The matrix is thus the sum of two terms, one 
with constant damage, the other due to the evolution of damage:

.. math::
   :label: eq_r7.01.08.4.4-1

   \dot{\boldsymbol{\sigma}} = (1-D) \mathbb{A} \dot{\boldsymbol{\varepsilon}} - 
      \mathbb{A} \boldsymbol{\varepsilon} \dot{D}

The first term is the Hooke operator, multiplied by the factor :math:`1-D`. The second requires the assessment 
of the damage increment :math:`\dot{D}`.

If a radial loading is imposed, the variables :math:`\gamma, r, A` and :math:`B` are constant. Then

.. math::
   :label: eq_r7.01.08.4.4-2

   \dot{D} = \frac{\partial D}{\partial Y} \frac{\partial Y}{\partial (\gamma \varepsilon_{\text{eq}})}
             \frac{\partial (\gamma \varepsilon_{\text{eq}})}{\partial\boldsymbol{\varepsilon}}
             \dot{\boldsymbol{\varepsilon}}

with

.. math::
   :label: eq_r7.01.08.4.4-3

   \frac{\partial Y}{\partial (\gamma\varepsilon_{\text{eq}})}
     \frac{\partial (\gamma\varepsilon_{\text{eq}})}{\partial\boldsymbol{\varepsilon}} = 
       \frac{\gamma \langle\boldsymbol{\varepsilon}\rangle_{+}}{\varepsilon_{\text{eq}}}

Under this radial loading condition, the strain increment is written:

.. math::
   :label: eq_r7.01.08.4.4-4

   \dot{D} = \left[ \frac{(1-A) Y_0}{Y^2} + AB\exp\left[B(Y - Y_0)\right]\right] 
               \frac{\gamma \langle\boldsymbol{\varepsilon}\rangle_{+}}{\varepsilon_{\text{eq}}}
               \dot{\boldsymbol{\varepsilon}}

.. admonition:: Remarks 

   1. Given the simplifications made, in the general case the tangent matrix is not consistent. Also, it can 
      happen that the recomputation of the tangent matrix during Newton's iterations do not help convergence. In 
      this case, use the secant matrix by imposing :red:`STAT_NON_LINE` (:blue:`NEWTON =_F(REAC_ITER = 0)`).

   2. In the general case, the tangent matrix is unsymmetrical. It is possible to force it to be symmerical
      with the keyword :blue:`SOLVEUR = _F(SYME = 'OUI')` of :red:`STAT_NON_LINE`.

   3. The analytical expression of the tangent matrix is valid only for radial loading (:math:`dr = d\gamma = 0`). 
      In other cases, the quadratic convergence of the method is no longer guaranteed.

.. _r7.01.08.4.5:

Stored internal variables
-------------------------

We indicate below the internal variables stored at each Gauss point for the :blue:`MAZARS` model:

* :green:`V1` : :math:`D`: damage variable
* :green:`V2` : damage indicator (0 if elastic, 1 if damaged, i.e., as soon as :math:`D` is no longer zero)
* :green:`V3` : :math:`T_{\text{max}}` : maximum temperature :math:`\theta` reached at the Gauss point
* :green:`V4` : :math:`\varepsilon_{\text{eq}} = \sqrt{\langle\boldsymbol{\varepsilon}\rangle_{+}: \langle\boldsymbol{\varepsilon}\rangle_{+}}` : the equivalent strain

.. _r7.01.08.5:

Features and verification
=========================

The :blue:`MAZARS` constitutive relation under keyword :blue:`COMPORTEMENT` of :red:`STAT_NON_LINE`, is usable in 
``Code_Aster`` with various models:

* :blue:`3D, D_PLAN, AXIS, C_PLAN` (analytical formulation established, do not use the :blue:`DEBORST` method)
* coupled with the models of :blue:`THHM` (see :ref:`[R7.01.11] <r7.01.11>`).

The :blue:`MAZARS` model can be coupled with the creep model :blue:`BETON_UMLV_FP` (see :ref:`[R7.01.06] <r7.01.06>`) 
via the keyword :blue:`KIT_DDI`.

The constitutive law is verified by the following tests:

* :green:`COMP007b`: :ref:`[V6.07.107] <v6.07.107>`: Compression-expansion test for study of coupled thermal fracture
* :ref:`[V7.22.129] <v7.22.179>`: Compression-expansion test for study of coupled thermal fracture 
* :green:`SSLA103`: :ref:`[V3.06.103] <v3.06.103>`: Calculation of desiccation shrinkage and endogenous shrinkage of a cylinder
* :green:`SSNP113` : :ref:`[V6.03.113] <v6.03.113>` : Rotation of the principal stresses (MAZARS model)
* :green:`SSNP161` : :ref:`[V6.03.161] <v6.03.161>` : Kupfer biaxial tests
* :green:`SSNV169` : :ref:`[V6.04.169] <v6.04.169>` : Creep-damage coupling
* :green:`WTNV121` : :ref:`[V7.31.121] <v7.31.121>` : Wetting of the concrete with a damage law


.. _r7.01.08.6:

Bibliography
============

.. [#bib1] J. Mazars (1984). Application of the damage mechanics to nonlinear behavior and structural concrete 
           failure , Doctoral thesis of the University of Paris VI.
.. [#bib2] J. Lemaitre, JL Chaboche (1988), Mechanics of solid materials. Ed. Dunod.
.. [#bib3] J. Mazars, F. Hamon. A new strategy to formulate a 3D damage model for concrete under monotonic, 
           cyclic and severe loadings. Engineering Structures, 2012
.. [#bib4] H. Askes (2000). Advanced spatial discretization strategies for localized failure, mesh adaptivity
           and meshless methods , PhD thesis, Delft University of Technology, Faculty of Civil Engineering and 
           Geosciences.
.. [#bib5] RHJ Peerlings, R. de Borst, WAM Brekelmans, JHP de Vree, I. Spee (1996). Some observations on localization 
           in non local and gradient damage models, Eur. J. Mech. A / Solids,15 (6), 937-953.
.. [#bib6] G. Pijaudier-Cabot, J. Mazars, J. Pulikowski (1991). Steel-concrete bond analysis with nolocal continuous 
           damage, J. Structural Engrg ASCE, 117, 862-882.
.. [#bib7] C. Le Bellego, JF Dubé, G. Pijaudier-Cabot, B. Gerard (2003). Calibration of nonlocal damagemodel from 
           size effect tests , Eur. J. Mech. A / Solids, 22 (1), 33-46.

