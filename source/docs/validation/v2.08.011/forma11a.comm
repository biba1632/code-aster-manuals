# Modal analysis of a straight bar made of beam elements
# POU_D_E
DEBUT(LANG='EN')

# Read the mesh
mesh = LIRE_MAILLAGE(FORMAT = 'MED',
                     UNITE = 20)

# Check mesh connectivity
MACR_INFO_MAIL(MAILLAGE = mesh,
               QUALITE = 'OUI',
               INTERPENETRATION = 'OUI',
               CONNEXITE = 'OUI',
               TAILLE = 'OUI',
               PROP_CALCUL = 'OUI')

# Assign model
model = AFFE_MODELE(AFFE = _F(MODELISATION=('POU_D_E'),
                              PHENOMENE='MECANIQUE',
                              TOUT='OUI'),
                    MAILLAGE = mesh)

# Beam element properties
section = AFFE_CARA_ELEM(MODELE = model,
                         POUTRE = _F(GROUP_MA = ('bar'),
                                     SECTION = 'RECTANGLE',
                                     CARA = ('HY', 'HZ'),
                                     VALE = (0.05, 0.05)))

# Assign material
steel = DEFI_MATERIAU(ELAS=_F(E = 210000000000.0,
                              NU = 0.3,
                              RHO = 7800.0))

mater = AFFE_MATERIAU(AFFE = _F(MATER = (steel),
                                TOUT = 'OUI'),
                      MODELE = model)

# Assign BCs
disp_bc = AFFE_CHAR_MECA(DDL_IMPO = _F(GROUP_NO = ('point_A'),
                                       LIAISON = 'ENCASTRE'),
                         MODELE = model)

# Assemble stiffness and mass matrices
ASSEMBLAGE(CARA_ELEM = section,
           CHAM_MATER = mater,
           CHARGE = (disp_bc),
           MATR_ASSE = (_F(MATRICE = CO('stif_mat'),
                           OPTION = 'RIGI_MECA'),
                        _F(MATRICE = CO('mass_mat'),
                           OPTION = 'MASS_MECA')),
           MODELE = model,
           NUME_DDL = CO('num_dof'))

# Modal analysis (0): PLUS_PETITE/TRI_DIAG
mode_0 = CALC_MODES(CALC_FREQ = _F(NMAX_FREQ = 11),
                    INFO = 2,
                    MATR_MASS = mass_mat,
                    MATR_RIGI = stif_mat,
                    OPTION = 'PLUS_PETITE',
                    SOLVEUR_MODAL = _F(METHODE = 'TRI_DIAG'))

# Modal analysis (1): PLUS_PETITE/QZ
mode_1 = CALC_MODES(CALC_FREQ = _F(NMAX_FREQ = 11),
                    INFO = 2,
                    MATR_MASS = mass_mat,
                    MATR_RIGI = stif_mat,
                    OPTION = 'PLUS_PETITE',
                    SOLVEUR_MODAL = _F(METHODE = 'QZ'))

# Modal analysis (2): CENTRE/QZ
mode_2 = CALC_MODES(CALC_FREQ = _F(FREQ = 2597.04,
                                   NMAX_FREQ = 11),
                    INFO = 2,
                    MATR_MASS = mass_mat,
                    MATR_RIGI = stif_mat,
                    OPTION = 'CENTRE',
                    SOLVEUR_MODAL = _F(METHODE='QZ'))

# Modal analysis (3): ALL/QZ
mode_3 = CALC_MODES(MATR_MASS = mass_mat,
                    MATR_RIGI = stif_mat,
                    OPTION = 'TOUT',
                    SOLVEUR_MODAL = _F(METHODE = 'QZ'))

# Modal analysis (4): BANDE/SORENSEN
mode_sor = CALC_MODES(CALC_FREQ = _F(FREQ = (0.0, 6000.0)),
                      MATR_MASS = mass_mat,
                      MATR_RIGI = stif_mat,
                      NORM_MODE = _F(NORME = 'EUCL'),
                      OPTION = 'BANDE',
                      SOLVEUR_MODAL = _F(METHODE='SORENSEN'),
                      TITRE = 'Sorensen')

# Modal analysis (5): BANDE/Lanzcos (TRI_DIAG)
mode_lan = CALC_MODES(CALC_FREQ = _F(FREQ = (0.0, 6000.0)),
                      MATR_MASS = mass_mat,
                      MATR_RIGI = stif_mat,
                      NORM_MODE = _F(NORME = 'EUCL'),
                      OPTION ='BANDE',
                      SOLVEUR_MODAL = _F(METHODE = 'TRI_DIAG'),
                      TITRE ='Lanzcos')

# Modal analysis (6): BANDE/Bathe (JACOBI)
mode_jac = CALC_MODES(CALC_FREQ = _F(FREQ = (0.0, 6000.0)),
                      MATR_MASS = mass_mat,
                      MATR_RIGI = stif_mat,
                      NORM_MODE = _F(NORME = 'EUCL'),
                      OPTION = 'BANDE',
                      SOLVEUR_MODAL = _F(METHODE = 'JACOBI'),
                      TITRE = 'Bathe',
                      VERI_MODE = _F(SEUIL = 1e-05))

# Modal analysis (7): BANDE/QZ
mode_qz = CALC_MODES(CALC_FREQ = _F(FREQ = (0.0, 6000.0)),
                     MATR_MASS = mass_mat,
                     MATR_RIGI = stif_mat,
                     NORM_MODE = _F(NORME = 'EUCL'),
                     OPTION = 'BANDE',
                     SOLVEUR_MODAL =_F(METHODE = 'QZ'),
                     TITRE = 'QZ',
                     VERI_MODE = _F(SEUIL = 1e-05))

# Modal analysis (8): AJUSTE/MULT_FRONT
mode_inv = CALC_MODES(CALC_FREQ = _F(FREQ = (0.0, 6000.0)),
                      MATR_MASS = mass_mat,
                      MATR_RIGI = stif_mat,
                      NORM_MODE = _F(NORME = 'EUCL'),
                      OPTION = 'AJUSTE',
                      SOLVEUR = _F(METHODE = 'MULT_FRONT'),
                      TITRE = 'inverse')

# Write out material and section properties
mat_sec = POST_ELEM(MODELE = model,
                    CHAM_MATER = mater,
                    CARA_ELEM = section,
                    MASS_INER = _F(TOUT = 'OUI'))
IMPR_TABLE(TABLE = mat_sec,
           SEPARATEUR = ',')

# Write results in tables
IMPR_RESU(FORMAT = 'RESULTAT',
          RESU = _F(RESULTAT = mode_0,
                    TOUT_PARA = 'OUI',
                    TOUT_CHAM = 'NON'),
          UNITE = 80)

IMPR_RESU(FORMAT = 'RESULTAT',
          RESU = _F(RESULTAT = mode_1,
                    TOUT_PARA = 'OUI',
                    TOUT_CHAM = 'NON'),
          UNITE = 80)

IMPR_RESU(FORMAT='RESULTAT',
          RESU = _F(RESULTAT = mode_2,
                    TOUT_PARA = 'OUI',
                    TOUT_CHAM = 'NON'),
          UNITE = 80)

IMPR_RESU(FORMAT='RESULTAT',
          RESU = _F(RESULTAT = mode_3,
                    TOUT_PARA = 'OUI',
                    TOUT_CHAM = 'NON'),
          UNITE = 80)

IMPR_RESU(FORMAT='RESULTAT',
          RESU = _F(RESULTAT = mode_sor,
                    TOUT_PARA = 'OUI',
                    TOUT_CHAM = 'NON'),
          UNITE = 80)

IMPR_RESU(FORMAT='RESULTAT',
          RESU = _F(RESULTAT = mode_lan,
                    TOUT_PARA = 'OUI',
                    TOUT_CHAM = 'NON'),
          UNITE = 80)

IMPR_RESU(FORMAT='RESULTAT',
          RESU = _F(RESULTAT = mode_jac,
                    TOUT_PARA = 'OUI',
                    TOUT_CHAM = 'NON'),
          UNITE = 80)

IMPR_RESU(FORMAT='RESULTAT',
          RESU = _F(RESULTAT = mode_qz,
                    TOUT_PARA = 'OUI',
                    TOUT_CHAM = 'NON'),
          UNITE = 80)

IMPR_RESU(FORMAT='RESULTAT',
          RESU = _F(RESULTAT = mode_inv,
                    TOUT_PARA = 'OUI',
                    TOUT_CHAM = 'NON'),
          UNITE = 80)

# Write displacement results in tables
IMPR_RESU(FORMAT = 'RESULTAT',
          RESU = (_F(IMPR_COOR = 'OUI',
                     NOM_CHAM = 'DEPL',
                     NOM_CMP = ('DY', 'DZ'),
                     NUME_ORDRE = (1, 2),
                     RESULTAT = mode_sor),
                  _F(IMPR_COOR='OUI',
                     NOM_CHAM = 'DEPL',
                     NOM_CMP = ('DY', 'DZ'),
                     NUME_ORDRE = (1, 2),
                     RESULTAT = mode_lan),
                  _F(IMPR_COOR='OUI',
                     NOM_CHAM = 'DEPL',
                     NOM_CMP = ('DY', 'DZ'),
                     NUME_ORDRE = (1, 2),
                     RESULTAT = mode_jac),
                  _F(IMPR_COOR='OUI',
                     NOM_CHAM = 'DEPL',
                     NOM_CMP = ('DY', 'DZ'),
                     NUME_ORDRE = (1, 2),
                     RESULTAT = mode_qz),
                  _F(IMPR_COOR='OUI',
                     NOM_CHAM = 'DEPL',
                     NOM_CMP = ('DY', 'DZ'),
                     NUME_ORDRE = (1, 2),
                     RESULTAT = mode_inv)),
          UNITE = 81)

# Write out results in MED format for viz
IMPR_RESU(FORMAT = 'MED',
          RESU = (_F(MAILLAGE = mesh),
                  _F(RESULTAT = mode_0),
                  _F(RESULTAT = mode_1),
                  _F(RESULTAT = mode_2),
                  _F(RESULTAT = mode_3),
                  _F(RESULTAT = mode_sor),
                  _F(RESULTAT = mode_lan),
                  _F(RESULTAT = mode_jac),
                  _F(RESULTAT = mode_qz),
                  _F(RESULTAT = mode_inv)),
          UNITE = 82)

# Verification tests
TEST_RESU(RESU = _F(NUME_ORDRE = 6,
                    PARA = 'FREQ',
                    RESULTAT = mode_1,
                    VALE_CALC = 2597.040657041,
                    CRITERE = 'RELATIF'))

TEST_RESU(RESU = _F(NUME_ORDRE = 11,
                    PARA = 'FREQ',
                    RESULTAT = mode_2,
                    VALE_CALC = 5769.9054933878997,
                    CRITERE = 'RELATIF'))

TEST_RESU(RESU = _F(NUME_ORDRE = 1,
                    PARA = 'FREQ',
                    RESULTAT = mode_3,
                    VALE_CALC = 167.63819422541999,
                    CRITERE = 'RELATIF'))

TEST_RESU(RESU = _F(NUME_ORDRE = 1,
                    PARA = 'FREQ',
                    RESULTAT = mode_sor,
                    VALE_CALC = 167.63819422438999,
                    CRITERE = 'RELATIF'))

TEST_RESU(RESU = _F(NUME_ORDRE = 4,
                    PARA = 'FREQ',
                    RESULTAT = mode_lan,
                    VALE_CALC = 1050.6045040198001,
                    CRITERE = 'RELATIF'))

TEST_RESU(RESU = _F(NUME_ORDRE = 7,
                    PARA = 'FREQ',
                    RESULTAT = mode_jac,
                    VALE_CALC = 2942.3746290629001,
                    CRITERE = 'RELATIF'))

TEST_RESU(RESU = _F(NUME_ORDRE = 11,
                    PARA = 'FREQ',
                    RESULTAT = mode_qz,
                    VALE_CALC = 5769.9054933878997,
                    CRITERE = 'RELATIF'))

# End
FIN()
