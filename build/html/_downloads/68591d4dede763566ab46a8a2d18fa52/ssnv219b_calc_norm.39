#=====================================================================================
# Extract the analytical and numerical displacements/contact pressures/gaps
# and compute the error norm
#=====================================================================================

def calcErrorNorm(result, mesh, model, el_group_vol, el_group_surf, U_x, U_y, U_z, F_n):

  """ result         : computed result concept
      mesh           : mesh
      model          : model
      el_group       : element group for which error norm is to be calculated

      U_x, U_y, U_z  : analytical displacements
      F_n            : analytical contact pressures

      output: {'u_ana': disp_ana, 'p_ana': pres_ana, 
               'u_cal': disp_cal, 'p_cal': pres_cal, 
               'u_err': disp_err, 'p_err': pres_err,
               'u_err_L2': u_err_L2, 'p_err_L2': p_err_L2} 
  """

  # ========================================================================================
  # Extract the analytical and computed dislacement solutions
  # ========================================================================================
 
  # Get the node positions
  pos = CREA_CHAMP(OPERATION = 'EXTR', 
                   TYPE_CHAM = 'NOEU_GEOM_R',
                   NOM_CHAM = 'GEOMETRIE', 
                   MAILLAGE = mesh, 
                   INFO = 1)

  # Set up arrays for saving analytical displacements
  disp = CREA_CHAMP(OPERATION = 'AFFE',
                    TYPE_CHAM = 'NOEU_NEUT_F',
                    MAILLAGE = mesh,
                    AFFE = (_F(GROUP_MA = el_group_vol,
                               NOM_CMP = 'X1', 
                               VALE_F = U_x),
                            _F(GROUP_MA = el_group_vol,
                               NOM_CMP = 'X2', 
                               VALE_F = U_y),
                            _F(GROUP_MA = el_group_vol,
                               NOM_CMP = 'X3', 
                               VALE_F = U_z)))

  # Compute analytical field values at nodes
  disp_pos = CREA_CHAMP(OPERATION = 'EVAL',
                        TYPE_CHAM = 'NOEU_NEUT_R',
                        CHAM_F = disp,
                        CHAM_PARA = pos)

  # Assign the anaytical field to a displacement variable
  disp_ana = CREA_CHAMP(OPERATION = 'ASSE',
                        TYPE_CHAM = 'NOEU_DEPL_R',
                        MAILLAGE = mesh,
                        ASSE = (_F(GROUP_MA = el_group_vol,
                                   CHAM_GD = disp_pos,
                                   NOM_CMP = 'X1',
                                   NOM_CMP_RESU = 'DX'),
                                _F(GROUP_MA = el_group_vol,
                                   CHAM_GD = disp_pos,
                                   NOM_CMP = 'X2',
                                   NOM_CMP_RESU = 'DY'),
                                _F(GROUP_MA = el_group_vol,
                                   CHAM_GD = disp_pos,
                                   NOM_CMP = 'X3',
                                   NOM_CMP_RESU = 'DZ')))

  # Extract the computed displacements
  disp_cal = CREA_CHAMP(OPERATION = 'EXTR',
                        INST = 1.0,
                        NOM_CHAM = 'DEPL', 
                        TYPE_CHAM = 'NOEU_DEPL_R',
                        RESULTAT = result)

  # Compute error
  disp_err = CREA_CHAMP(OPERATION = 'ASSE', 
                        TYPE_CHAM = 'NOEU_DEPL_R', 
                        MODELE = model,
                        ASSE = (_F(GROUP_MA = el_group_vol, 
                                   CHAM_GD = disp_cal,
                                   CUMUL = 'OUI', 
                                   COEF_R = 1.0, 
                                   NOM_CMP = 'DX', 
                                   NOM_CMP_RESU = 'DX'),
                                _F(GROUP_MA = el_group_vol, 
                                   CHAM_GD = disp_ana, 
                                   CUMUL = 'OUI', 
                                   COEF_R = -1.0, 
                                   NOM_CMP = 'DX', 
                                   NOM_CMP_RESU = 'DX'),
                                _F(GROUP_MA = el_group_vol, 
                                   CHAM_GD = disp_cal,
                                   CUMUL = 'OUI', 
                                   COEF_R = 1.0, 
                                   NOM_CMP = 'DY', 
                                   NOM_CMP_RESU = 'DY'),
                                _F(GROUP_MA = el_group_vol, 
                                   CHAM_GD = disp_ana, 
                                   CUMUL = 'OUI', 
                                   COEF_R = -1.0, 
                                   NOM_CMP = 'DY', 
                                   NOM_CMP_RESU = 'DY'),
                                _F(GROUP_MA = el_group_vol, 
                                   CHAM_GD = disp_cal,
                                   CUMUL = 'OUI', 
                                   COEF_R = 1.0, 
                                   NOM_CMP = 'DZ', 
                                   NOM_CMP_RESU = 'DZ'),
                                _F(GROUP_MA = el_group_vol, 
                                   CHAM_GD = disp_ana, 
                                   CUMUL = 'OUI', 
                                   COEF_R = -1.0, 
                                   NOM_CMP = 'DZ', 
                                   NOM_CMP_RESU = 'DZ')))

  # ========================================================================================
  # Extract analytical solution for contact pressure
  # ========================================================================================

  # Set up data structure for saving analytical pressure
  pres = CREA_CHAMP(OPERATION = 'AFFE',
                    TYPE_CHAM = 'NOEU_NEUT_F',
                    MAILLAGE = mesh,
                    AFFE = (_F(GROUP_MA = el_group_surf, 
                               NOM_CMP = 'X1', 
                               VALE_F = F_n)))

  # Compute analytical field values at nodes
  pres_pos = CREA_CHAMP(OPERATION = 'EVAL',
                        TYPE_CHAM = 'NOEU_NEUT_R',
                        CHAM_F = pres,
                        CHAM_PARA = pos)

  # Assign the analytical field to a LAGS_C variable
  pres_ana = CREA_CHAMP(OPERATION = 'ASSE',
                        TYPE_CHAM = 'NOEU_DEPL_R',
                        MAILLAGE = mesh,
                        ASSE = (_F(GROUP_MA = el_group_surf,
                                   CHAM_GD = pres_pos,
                                   NOM_CMP = 'X1',
                                   NOM_CMP_RESU = 'LAGS_C')))

  # Extract the computed pressure
  pres_cal = CREA_CHAMP(OPERATION = 'EXTR',
                        INST = 1.,
                        NOM_CHAM = 'DEPL', 
                        TYPE_CHAM = 'NOEU_DEPL_R',
                        RESULTAT = result)

  # Compute error
  pres_err = CREA_CHAMP(OPERATION = 'ASSE', 
                        TYPE_CHAM = 'NOEU_DEPL_R', 
                        MODELE = model,
                        ASSE = (_F(GROUP_MA = el_group_vol, 
                                   CHAM_GD = pres_cal, 
                                   CUMUL = 'OUI', 
                                   COEF_R = 1.0, 
                                   NOM_CMP = 'DX', 
                                   NOM_CMP_RESU = 'LAGS_C'),
                               _F(GROUP_MA = el_group_vol, 
                                   CHAM_GD = pres_ana, 
                                   CUMUL = 'OUI', 
                                   COEF_R = -1.0, 
                                   NOM_CMP = 'DX', 
                                   NOM_CMP_RESU = 'LAGS_C')))

  # ========================================================================================
  # The displacement solution at the integration points
  # ========================================================================================
  pos_G = CREA_CHAMP(OPERATION = 'DISC', 
                     TYPE_CHAM = 'ELGA_GEOM_R', 
                     PROL_ZERO = 'OUI',
                     CHAM_GD = pos, 
                     MODELE = model)

  disp_G = CREA_CHAMP(OPERATION = 'AFFE',
                      TYPE_CHAM = 'ELGA_NEUT_F',
                      MODELE = model,
                      PROL_ZERO = 'OUI',
                      AFFE = (_F(GROUP_MA = el_group_vol,
                                 NOM_CMP = 'X1',
                                 VALE_F = U_x),
                              _F(GROUP_MA = el_group_vol,
                                 NOM_CMP = 'X2',
                                 VALE_F = U_y),
                              _F(GROUP_MA = el_group_vol,
                                 NOM_CMP = 'X3',
                                 VALE_F = U_z)))

  u_ana_G = CREA_CHAMP(OPERATION = 'EVAL',
                       TYPE_CHAM = 'ELGA_NEUT_R',
                       CHAM_F = disp_G,
                       CHAM_PARA = pos_G)

  u_cal_N = CREA_CHAMP(OPERATION = 'ASSE',
                       TYPE_CHAM = 'NOEU_NEUT_R',
                       MAILLAGE = mesh,
                       ASSE = (_F(GROUP_MA = el_group_vol,
                                  CHAM_GD = disp_cal,
                                  NOM_CMP = 'DX',
                                  NOM_CMP_RESU = 'X1'),
                               _F(GROUP_MA = el_group_vol,
                                  CHAM_GD = disp_cal,
                                  NOM_CMP = 'DY',
                                  NOM_CMP_RESU = 'X2'),
                               _F(GROUP_MA = el_group_vol,
                                  CHAM_GD = disp_cal,
                                  NOM_CMP = 'DZ',
                                  NOM_CMP_RESU = 'X3')))

  u_cal_G = CREA_CHAMP(OPERATION = 'DISC',
                       TYPE_CHAM = 'ELGA_NEUT_R',
                       MODELE = model,
                       PROL_ZERO = 'OUI',
                       CHAM_GD = u_cal_N)

  # Calculate error
  u_err_G = CREA_CHAMP(OPERATION = 'ASSE', 
                       TYPE_CHAM = 'ELGA_DEPL_R', 
                       MODELE = model,
                       PROL_ZERO = 'OUI',
                       ASSE = (_F(GROUP_MA = el_group_vol, 
                                  CHAM_GD = u_cal_G,
                                  CUMUL = 'OUI', 
                                  COEF_R = 1.0, 
                                  NOM_CMP = 'X1', 
                                  NOM_CMP_RESU = 'DX'),
                               _F(GROUP_MA = el_group_vol, 
                                  CHAM_GD = u_ana_G, 
                                  CUMUL = 'OUI', 
                                  COEF_R = -1.0, 
                                  NOM_CMP = 'X1', 
                                  NOM_CMP_RESU = 'DX'),
                               _F(GROUP_MA = el_group_vol, 
                                  CHAM_GD = u_cal_G,
                                  CUMUL = 'OUI', 
                                  COEF_R = 1.0, 
                                  NOM_CMP = 'X2', 
                                  NOM_CMP_RESU = 'DY'),
                               _F(GROUP_MA = el_group_vol, 
                                  CHAM_GD = u_ana_G, 
                                  CUMUL = 'OUI', 
                                  COEF_R = -1.0, 
                                  NOM_CMP = 'X2', 
                                  NOM_CMP_RESU = 'DY'),
                               _F(GROUP_MA = el_group_vol, 
                                  CHAM_GD = u_cal_G,
                                  CUMUL = 'OUI', 
                                  COEF_R = 1.0, 
                                  NOM_CMP = 'X3', 
                                  NOM_CMP_RESU = 'DZ'),
                               _F(GROUP_MA = el_group_vol, 
                                  CHAM_GD = u_ana_G, 
                                  CUMUL = 'OUI', 
                                  COEF_R = -1.0, 
                                  NOM_CMP = 'X3', 
                                  NOM_CMP_RESU = 'DZ')))

  # Calculate L2 norm of displacement error
  u_tab_G = POST_ELEM(NORME = _F(TYPE_NORM = 'L2',
                                 GROUP_MA = el_group_vol,
                                 CHAM_GD = u_err_G,
                                 MODELE = model))

  u_err_L2 = u_tab_G['VALE_NORM', 1]

  # Save table
  IMPR_TABLE(TABLE = u_tab_G,
             SEPARATEUR = ',')

  # ========================================================================================
  # The contact pressure solution at integration points
  # ========================================================================================
  pres_G = CREA_CHAMP(OPERATION = 'AFFE',
                      TYPE_CHAM = 'ELGA_NEUT_F',
                      MODELE = model,
                      PROL_ZERO = 'OUI',
                      AFFE = (_F(GROUP_MA = el_group_surf,
                                 NOM_CMP = 'X1',
                                 VALE_F = F_n)))

  p_ana_G = CREA_CHAMP(OPERATION = 'EVAL',
                       TYPE_CHAM = 'ELGA_NEUT_R',
                       CHAM_F = pres_G,
                       CHAM_PARA = pos_G)

  p_cal_N = CREA_CHAMP(OPERATION = 'ASSE',
                       TYPE_CHAM = 'NOEU_NEUT_R',
                       MAILLAGE = mesh,
                       ASSE = (_F(GROUP_MA = el_group_surf,
                                  CHAM_GD = disp_cal,
                                  NOM_CMP = 'LAGS_C',
                                  NOM_CMP_RESU = 'X1')))

  p_cal_G = CREA_CHAMP(OPERATION = 'DISC',
                       TYPE_CHAM = 'ELGA_NEUT_R',
                       MODELE = model,
                       PROL_ZERO = 'OUI',
                       CHAM_GD = p_cal_N)

  # Calculate error
  p_err_G = CREA_CHAMP(OPERATION = 'ASSE', 
                       TYPE_CHAM = 'ELGA_DEPL_R', 
                       MODELE = model,
                       PROL_ZERO = 'OUI',
                       ASSE = (_F(GROUP_MA = el_group_surf, 
                                  CHAM_GD = p_cal_G, 
                                  CUMUL = 'OUI', 
                                  COEF_R = 1., 
                                  NOM_CMP = 'X1', 
                                  NOM_CMP_RESU = 'DX'),
                              _F(GROUP_MA = el_group_surf, 
                                  CHAM_GD = p_ana_G, 
                                  CUMUL = 'OUI', 
                                  COEF_R = -1., 
                                  NOM_CMP = 'X1', 
                                  NOM_CMP_RESU = 'DX')))

  # Calculate L2 error norm of pressure error
  p_tab_G = POST_ELEM(NORME = _F(TYPE_NORM = 'L2',
                                 GROUP_MA = el_group_surf,
                                 CHAM_GD = p_err_G,
                                 MODELE = model))

  p_err_L2 = p_tab_G['VALE_NORM', 1]

  # Write error table
  IMPR_TABLE(TABLE = p_tab_G,
             SEPARATEUR = ',')

  # ========================================================================================
  # Delete all temporary concepts
  # ========================================================================================
  DETRUIRE(CONCEPT = (_F(NOM = pos),
                      _F(NOM = disp),
                      _F(NOM = disp_pos),
                      _F(NOM = pres),
                      _F(NOM = pres_pos),
                      _F(NOM = pos_G),
                      _F(NOM = disp_G),
                      _F(NOM = u_ana_G),
                      _F(NOM = u_cal_N),
                      _F(NOM = u_cal_G),
                      _F(NOM = u_err_G),
                      _F(NOM = u_tab_G),
                      _F(NOM = pres_G),
                      _F(NOM = p_ana_G),
                      _F(NOM = p_cal_N),
                      _F(NOM = p_cal_G),
                      _F(NOM = p_err_G),
                      _F(NOM = p_tab_G)),
           INFO = 1)

  return {'u_ana': disp_ana, 'p_ana': pres_ana, 
          'u_cal': disp_cal, 'p_cal': pres_cal, 
          'u_err': disp_err, 'p_err': pres_err,
          'u_err_L2': u_err_L2, 'p_err_L2': p_err_L2} 

# ===============================================================================================

