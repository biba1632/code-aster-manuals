# Python variables
import numpy as np

# Reference value (value computed after three refinement stages)
ener_ref  =  6.75073756E-5

# Initialize Python lists
energy_vs_refine     = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
err_energy_vs_refine = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
energy_vs_refine_NUM     = np.zeros((len(energy_vs_refine), 2))
err_energy_vs_refine_NUM = np.zeros((len(err_energy_vs_refine), 2))

for i in range(len(energy_vs_refine)) :
    energy_vs_refine_NUM[i][1] = 0.0
    err_energy_vs_refine_NUM[i][1] = 0.0
    energy_vs_refine_NUM[i][0] = i
    err_energy_vs_refine_NUM[i][0] = i

# Number of refinement steps
nb_calc = 2

# Initialize arrays to store concepts
nb_calc1 = nb_calc + 1
nb_calc2 = nb_calc1 + 1

# Thermal
meshT    = [None]*nb_calc1
modT     = [None]*nb_calc1
modT2    = [None]*nb_calc1
matT     = [None]*nb_calc1
bcT      = [None]*nb_calc1
loadT    = [None]*nb_calc1
resT     = [None]*nb_calc1
resT2    = [None]*nb_calc1

# Mechanical
meshM    = [None]*nb_calc1
modM     = [None]*nb_calc1
matM     = [None]*nb_calc1
matTM    = [None]*nb_calc1
bcM      = [None]*nb_calc1
loadM    = [None]*nb_calc1
resM     = [None]*nb_calc1

# Energy
energy   = [None]*nb_calc1

# Aster calculations
DEBUT(LANG = 'EN',
      PAR_LOT='NON')

# Define materials
steel_M = DEFI_MATERIAU(ELAS = _F(E = 210.E3,
                                  NU = 0.2,
                                  ALPHA = 0.))

steel_T = DEFI_MATERIAU(THER = _F(LAMBDA = 33.5,
                                  RHO_CP = 526.E4))

# Time steps for STAT_NON_LINE
l_times = DEFI_LIST_REEL(DEBUT = 0.0, 
                         INTERVALLE = _F(JUSQU_A = 1.0,
                                         NOMBRE = 1))
f_times = DEFI_FONCTION(NOM_PARA = 'INST',
                        VALE = (0.0, 0.0,
                                1.0, 1.0))

# Read thermal and mechanical meshes
num_calc = 0
meshT[num_calc] = LIRE_MAILLAGE(FORMAT = 'MED',
                                UNITE = 20)
meshM[num_calc] = LIRE_MAILLAGE(FORMAT = "ASTER",
                                UNITE = 21)

# Start refinement loop
for num_calc in range(0, nb_calc1) :

   # Assign P1 2D lumped elements for the thermal calculation
   modT[num_calc] = AFFE_MODELE(MAILLAGE = meshT[num_calc],
                                AFFE = _F(TOUT = 'OUI', 
                                          MODELISATION = 'PLAN_DIAG',
                                          PHENOMENE = 'THERMIQUE'))

   # Assign material to thermal elements
   matT[num_calc] = AFFE_MATERIAU(MAILLAGE = meshT[num_calc],
                                  AFFE = _F(GROUP_MA = 'GM38',
                                            MATER = steel_T))

   # Assign P2 2D lumped elements for thermal to mechanical projection
   modT2[num_calc] = AFFE_MODELE(MAILLAGE = meshM[num_calc],
                                 AFFE = _F(TOUT = 'OUI', 
                                           MODELISATION = 'PLAN_DIAG',
                                           PHENOMENE = 'THERMIQUE'))

   # Assign P2 plane stress elements for mechanical calculation
   modM[num_calc] = AFFE_MODELE(MAILLAGE = meshM[num_calc],
                                AFFE = _F(TOUT = 'OUI', 
                                          MODELISATION = 'C_PLAN',
                                          PHENOMENE = 'MECANIQUE'))

   # Assign material to mechanical elements
   matM[num_calc] = AFFE_MATERIAU(MAILLAGE = meshM[num_calc],
                                  AFFE = _F(GROUP_MA = 'GM38',
                                            MATER = steel_M))

   # Thermal flux BC on the left edge + ecxhange at the cylinders
   bcT[num_calc] = AFFE_CHAR_THER(MODELE = modT[num_calc],
                                  FLUX_REP = (_F(GROUP_MA = 'GM33',
                                                 FLUN = -400)))

   loadT[num_calc] = AFFE_CHAR_THER(MODELE = modT[num_calc],
                                    ECHANGE = (_F(GROUP_MA = 'GM36',
                                                  COEF_H = 1000,
                                                  TEMP_EXT = 350),
                                               _F(GROUP_MA = 'GM37',
                                                  COEF_H = 5000,
                                                  TEMP_EXT = 150)))

   # Linear transient thermal calculation
   resT[num_calc] = THER_LINEAIRE(MODELE = modT[num_calc],
                                  CHAM_MATER = matT[num_calc],
                                  EXCIT = (_F(CHARGE = loadT[num_calc]),
                                           _F(CHARGE = bcT[num_calc])))

   # Post-process for nodal flux 
   resT[num_calc] = CALC_CHAMP(reuse = resT[num_calc],
                               RESULTAT = resT[num_calc],
                               THERMIQUE = ('FLUX_ELNO'))

   # Post-process for residual error indicator (at nodes and elements)
   resT[num_calc] = CALC_ERREUR(reuse = resT[num_calc],
                                RESULTAT = resT[num_calc],
                                TOUT = 'OUI',
                                OPTION = ('ERTH_ELEM', 'ERTH_ELNO'))

   # Write the result in MED format
   DEFI_FICHIER(FICHIER = 'fort.55',
                UNITE = 55, 
                TYPE = 'BINARY')
   IMPR_RESU(FORMAT = 'MED',
             UNITE = 55,
             RESU = _F(MAILLAGE = meshT[num_calc],
                       RESULTAT = resT[num_calc],
                       NOM_CHAM = ('TEMP', 'FLUX_ELNO', 'ERTH_ELNO')))
   DEFI_FICHIER(ACTION = 'LIBERER',
                UNITE = 55)


   # Compute projected temperature field
   resT2[num_calc] = PROJ_CHAMP(METHODE = 'COLLOCATION',
                                RESULTAT = resT[num_calc],
                                MODELE_1 = modT[num_calc],
                                MODELE_2 = modT2[num_calc])

   # Assign thermo-mechanical properties
   matTM[num_calc] = AFFE_MATERIAU(MAILLAGE = meshM[num_calc],
                                   AFFE = _F(GROUP_MA = 'GM38',
                                             MATER = steel_M),
                                   AFFE_VARC = _F(TOUT = 'OUI',
                                                  EVOL = resT2[num_calc],
                                                  VALE_REF = 0.,
                                                  NOM_VARC = 'TEMP',
                                                  NOM_CHAM = 'TEMP',),)

   # Fixed BCs at X=0 : GM39 (X=0,Y=0) and GM40 (X=0,Y=20)
   bcM[num_calc] = AFFE_CHAR_MECA(MODELE = modM[num_calc],
                                  FACE_IMPO = (_F(GROUP_MA = 'GM39',
                                                  DX = 0.0,
                                                  DY = 0.0),
                                               _F(GROUP_MA = 'GM40',
                                                  DX = 0.0,
                                                  DY = 0.0)))

   # Load at X = 55
   loadM[num_calc] = AFFE_CHAR_MECA(MODELE = modM[num_calc],
                                    PRES_REP = (_F(GROUP_MA = 'GM34',
                                                   PRES = -1.E-1)))

   # Elastic calculation with STAT_NON_LINE
   resM[num_calc] = STAT_NON_LINE(MODELE = modM[num_calc],
                                  CHAM_MATER = matTM[num_calc],
                                  EXCIT = (_F(CHARGE = bcM[num_calc]),
                                           _F(CHARGE = loadM[num_calc],
                                              FONC_MULT = f_times)),
                                  COMPORTEMENT = (_F(RELATION = 'ELAS',
                                                     TOUT = 'OUI')),
                                  INCREMENT = _F(LIST_INST = l_times))

   # Post-process to add stresses at nodes
   resM[num_calc] = CALC_CHAMP(reuse = resM[num_calc],
                               RESULTAT = resM[num_calc],
                               CONTRAINTE = ('SIGM_ELNO'))


   # Post-process to add residual error indicator for mechanics
   resM[num_calc] = CALC_ERREUR(reuse = resM[num_calc],
                                RESULTAT = resM[num_calc],
                                TOUT = 'OUI',
                                OPTION = ('ERME_ELEM', 'ERME_ELNO'))

   # Write result in MED format
   DEFI_FICHIER(FICHIER = 'fort.56',
                UNITE = 56, 
                TYPE = 'BINARY')
   IMPR_RESU(FORMAT = 'MED',
             UNITE = 56,
             RESU = _F(MAILLAGE = meshM[num_calc],
                       RESULTAT = resM[num_calc],
                       NOM_CHAM = ('DEPL', 'SIGM_ELNO', 'ERME_ELNO')))
   DEFI_FICHIER(ACTION = 'LIBERER',
                UNITE = 56)

   # Calculate strain energy
   energy[num_calc] = POST_ELEM(MODELE = modM[num_calc],
                                CHAM_MATER = matTM[num_calc],
                                RESULTAT = resM[num_calc],
                                NUME_ORDRE = 1,
                                ENER_POT = _F(TOUT = 'OUI'))

   # Write the energy to a table
   IMPR_TABLE(TABLE = energy[num_calc],
              #UNITE = 8,
              SEPARATEUR = ',')

   # Save in Python arrays
   energy_vs_refine_NUM[num_calc][1] = energy[num_calc]['TOTALE', 1]
   err_energy_vs_refine_NUM[num_calc][1] = abs((energy_vs_refine_NUM[num_calc][1]-ener_ref)/ener_ref)*100

   # For regression tests
   if num_calc == 0 :
      eren0 = abs((energy_vs_refine_NUM[num_calc][1]-ener_ref)/ener_ref)*100
   if num_calc == 2 :
      eren2 = abs((energy_vs_refine_NUM[num_calc][1]-ener_ref)/ener_ref)*100

   num_calc1 = num_calc + 1

   # Write messages
   print('**************')
   print('ENERGY CONVERGENCE  = ', energy_vs_refine_NUM[:num_calc1])
   print('**************')
   print('ERROR IN ENERGY = ', err_energy_vs_refine_NUM[:num_calc1])

   # Stopping criterion
   if num_calc == nb_calc :
       break

   # Create thermal mesh concept for next iteration
   meshT[num_calc1] = CO('meshT_%d' % (num_calc1))

   # Use HOMARD for free refinement using thermal results
   # input mesh: meshT[num_calc]
   # output mesh: meshT[num_calc1]
   MACR_ADAP_MAIL(ADAPTATION = 'RAFF_DERA',
                  MAILLAGE_N = meshT[num_calc],
                  MAILLAGE_NP1 = meshT[num_calc1],
                  RESULTAT_N = resT[num_calc],
                  NOM_CHAM = 'ERTH_ELEM',
                  NOM_CMP = 'ERTREL',
                  CRIT_RAFF_PE = 0.2,
                  CRIT_DERA_PE = 0.1,
                  QUALITE = 'OUI',
                  TAILLE = 'OUI',
                  CONNEXITE = 'OUI')

   # Create mechanical mesh concept for next iteration
   meshM[num_calc1] = CO('meshM_%d' % (num_calc1))

   # Use HOMARD for free refinement using mechanical results
   # input mesh: meshM[num_calc]
   # output mesh: meshM[num_calc1]
   MACR_ADAP_MAIL(ADAPTATION = 'RAFF_DERA',
                  MAILLAGE_N = meshM[num_calc],
                  MAILLAGE_NP1 = meshM[num_calc1],
                  RESULTAT_N = resM[num_calc],
                  NOM_CHAM = 'ERME_ELEM',
                  NOM_CMP = 'NUEST',
                  NUME_ORDRE = 1,
                  CRIT_RAFF_PE = 0.2,
                  CRIT_DERA_PE = 0.1,
                  QUALITE = 'OUI',
                  TAILLE = 'OUI',
                  CONNEXITE = 'OUI')

# End of refinement loop

# Verification tests
ERREEN0 = FORMULE(VALE = 'eren0*1.',
                  eren0 = eren0,
                  NOM_PARA = 'cylinder')
ERREEN2 = FORMULE(VALE = 'eren2*1.',
                  eren2 = eren2,
                  NOM_PARA = 'cylinder')
TEST_FONCTION(VALEUR = (_F(VALE_CALC = 10.077761341,
                           VALE_PARA = 0.0,
                           NOM_PARA = 'cylinder',
                           FONCTION = ERREEN0),
                        _F(VALE_CALC = 0.45901083599999998,
                           VALE_PARA = 0.0,
                           NOM_PARA = 'cylinder',
                           FONCTION = ERREEN2)))

# End
FIN()
