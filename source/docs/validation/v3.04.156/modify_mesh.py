import SMESH
import salome
from salome.smesh import smeshBuilder

smesh = smeshBuilder.New()

mesh = salome.IDToObject("0:1:1:3")
mesh_6 = smesh.Mesh(mesh)

g_names = mesh_6.GetGroupNames()
for name in g_names:
   gr = mesh_6.GetGroupByName(name)
   gr[0].SetName(name.strip())
 
symm_face_xz = mesh_6.GetGroupByName("symm_face_xz")
symm_face_yz = mesh_6.GetGroupByName("symm_face_yz")
top_face = mesh_6.GetGroupByName("top_face")
bot_face = mesh_6.GetGroupByName("bot_face")
right_face = mesh_6.GetGroupByName("right_face")
back_face = mesh_6.GetGroupByName("back_face")

nodes_415 = mesh_6.CreateEmptyGroup( SMESH.NODE, 'node_415')
nodes = nodes_415.Add([203, 2049, 6699, 1226])
nodes_4727 = mesh_6.CreateEmptyGroup( SMESH.NODE, 'nodes_4727')
nodes = nodes_4727.Add([304, 15735, 6699, 2049, 1226])
nodes_6279 = mesh_6.CreateEmptyGroup( SMESH.NODE, 'nodes_6279')
nodes = nodes_6279.Add([304, 17883, 6699, 1226, 2049])

elem_415 = mesh_6.CreateEmptyGroup( SMESH.FACE, 'elem_415' )
elems = elem_415.Add([415])
elem_4727 = mesh_6.CreateEmptyGroup( SMESH.VOLUME, 'elem_4727' )
elems = elem_4727.Add([4727])
elem_6279 = mesh_6.CreateEmptyGroup( SMESH.VOLUME, 'elem_6279' )
elems = elem_6279.Add([6279])

g_criteria = []
crit = smesh.GetCriterion(SMESH.FACE, SMESH.FT_BelongToMeshGroup, '=', 
                          bot_face[0], SMESH.FT_LogicalNOT)
g_criteria.append(crit)
crit = smesh.GetCriterion(SMESH.FACE, SMESH.FT_BelongToMeshGroup, '=', 
                          top_face[0], SMESH.FT_LogicalNOT)
g_criteria.append(crit)
crit = smesh.GetCriterion(SMESH.FACE, SMESH.FT_BelongToMeshGroup, '=', 
                          right_face[0], SMESH.FT_LogicalNOT)
g_criteria.append(crit)
crit = smesh.GetCriterion(SMESH.FACE, SMESH.FT_BelongToMeshGroup, '=', 
                          back_face[0], SMESH.FT_LogicalNOT)
g_criteria.append(crit)
crit = smesh.GetCriterion(SMESH.FACE, SMESH.FT_BelongToMeshGroup, '=', 
                          symm_face_xz[0], SMESH.FT_LogicalNOT)
g_criteria.append(crit)
crit = smesh.GetCriterion(SMESH.FACE, SMESH.FT_BelongToMeshGroup, '=', 
                          symm_face_yz[0], SMESH.FT_LogicalNOT)
g_criteria.append(crit)

g_filter = smesh.GetFilterFromCriteria(g_criteria)

plane_faces = mesh_6.GroupOnFilter( SMESH.FACE, 'plane_faces', g_filter )


