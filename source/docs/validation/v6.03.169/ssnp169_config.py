# SSNP169 global variables

# geometry
R1  =  1.0
R2  =  0.6
R3  =  0.2

# elastic properties
E1 = 1.e9
E2 = 1.0e9
nu1 = 0.2
nu2 = 0.2

alpha =  1.0e5
p     =  1.0e7

# Some functions
def deter(f):
    return (1-f)**3

def calc_1(pe,pi,Re,Ri):

    f = (Ri/Re)**2

    det = deter(f)

    A =  ( pe*(2*f**2+f+1) -pi*(f**3+f**2+2*f) )/(2*det)
    b = -( pe*(3*f**2+f)-pi*(f**3+3*f**2) )/(6*det)
    c =  ( pe*(f+3)-pi*(3*f+1) )/(6*det)
    d = -( pe*(f**2+f+2)-pi*(2*f**2+f+1) )/(2*det)

    B = b/Ri**2
    C = c*Ri**4
    D = d*Ri**2

    return A,B,C,D

def calc_2(pe,Re):

    A  =  pe/2.
    B  =  -pe/( 6*Re**2 )

    return A,B

#-----------------------------------------
# For constant load
#-----------------------------------------
# contact pressure
pres_cont_c  =  2*p*( 1-nu1**2 )/E1*R1**2/( R1**2-R2**2 )/( (1+nu1)/E1*(R2**2*(1-2*nu1)+R1**2)/(R1**2-R2**2)+(1+nu2)/E2*(1-2*nu2) )

# annulus constants (external)
A11  =  (1+nu1)*(1-2*nu1)*(pres_cont_c*R2*R2-p*R1*R1) / ( E1*(R1*R1-R2*R2) )
B11  =  (pres_cont_c-p)*(1+nu1)*R1*R1*R2*R2 / ( E1*(R1*R1-R2*R2) )

# annulus constants (internal)
A12  =  -(1+nu2)*(1-2*nu2)*pres_cont_c / E2


#-----------------------------------------
# For load that is a function of theta
#-----------------------------------------
f1 = (R2/R1)**2
det1 = deter(f1)

term1 = (1+nu1)/(E1*6*det1)* \
( -3*(2*f1**2+f1+1)+(f1+3)+6*(-f1**2-f1-2)-nu1*(2*(-3*f1**2-f1)+6*(-f1**2-f1-2)) )

term2 = (1+nu1)/(E1*6*det1)*\
( -3*(f1**3+f1**2+2*f1)+(3*f1+1)+6*(-2*f1**2-f1-1)-nu1*(2*(-f1**3-3*f1**2)+6*(-2*f1**2-f1-1)) )

term3 = (1+nu2)/(E2*6)*( -3 + 2*nu2 )

pc = term1/(term3+term2)*alpha

A21 = 0.
B21 = 0.
C21 = 0.
D21 = 0.

A22 = 0.
B22 = 0.

A21,B21,C21,D21  =  calc_1(alpha,pc,R1,R2)
A22,B22          =  calc_2(pc,R2)

