# SSNV112P (mesh same as SSNV112A)

DEBUT(LANG = 'EN')

# Read mesh
mesh = LIRE_MAILLAGE(FORMAT = 'MED')

# Define node groups
mesh = DEFI_GROUP(reuse = mesh,
                  MAILLAGE = mesh,
                  CREA_GROUP_NO = (_F(GROUP_MA = ('A', 'E', 'F'))))

# Define element groups
mesh = DEFI_GROUP(reuse = mesh,
                  MAILLAGE = mesh,
                  CREA_GROUP_MA = (_F(NOM = 'M1',
                                      TYPE_MAILLE = '3D',
                                      OPTION = 'SPHERE',
                                      GROUP_NO_CENTRE = 'A',
                                      RAYON = 0.005)))
                
# Set up correct face normal orientation
mesh = MODI_MAILLAGE(reuse = mesh,
                     MAILLAGE = mesh,
                     ORIE_PEAU_3D = _F(GROUP_MA = ('FACEAB','FACSUP','FACINF','FACEEF','FACEAE')))

# Set up model
model = AFFE_MODELE(MAILLAGE = mesh,
                    AFFE = _F(GROUP_MA = ('VOLUME','FACEEF','FACEAE','FACEAB','FACSUP','FACINF'),
                              PHENOMENE = 'MECANIQUE',
                              MODELISATION = '3D_INCO_UPGB'))

# Define material
inco_mat = DEFI_MATERIAU(ELAS = _F(E = 200000.,
                                   NU = 0.49999,
                                   ALPHA = 0.),
                         ECRO_LINE = _F(D_SIGM_EPSI = 1000.,
                                        SY = 1.E9))

# Assign material
mater = AFFE_MATERIAU(MAILLAGE = mesh,
                      AFFE = _F(TOUT = 'OUI',
                                MATER = inco_mat))

# Assign BCs
bcs = AFFE_CHAR_MECA(MODELE = model,
                     DDL_IMPO = (_F(GROUP_MA = 'FACEAB',
                                    DX = 0.),
                                 _F(GROUP_MA = 'FACSUP',
                                    DZ = 0.),
                                 _F(GROUP_MA = 'FACINF',
                                    DZ = 0.)),
                     FACE_IMPO = (_F(GROUP_MA = 'FACEEF',
                                     DNOR = 0.),
                                  _F(GROUP_MA = 'FACEAE',
                                     DNOR = -6.E-5)))

# Set up timesteps
l_times = DEFI_LIST_REEL(DEBUT = 0.,
                         INTERVALLE = _F(JUSQU_A = 1.,
                                         NOMBRE = 1))

# Set up ramp function
ramp = DEFI_FONCTION(NOM_PARA = 'INST',
                     VALE = (0., 0.,
                             1., 1.))

#--------------------------------------------------------------------------------
# Solve system (singularity detection on, Newton process corrects any errors)
result = STAT_NON_LINE(MODELE = model,
                       CHAM_MATER = mater,
                       EXCIT = _F(CHARGE = bcs,
                                  FONC_MULT = ramp),
                       COMPORTEMENT = _F(RELATION = 'VMIS_ISOT_LINE',
                                         DEFORMATION = 'SIMO_MIEHE'),
                       INCREMENT = _F(LIST_INST = l_times),
                       NEWTON = _F(REAC_ITER = 1,
                                   MATR_RIGI_SYME = 'OUI'),
                       CONVERGENCE = _F(SIGM_REFE = 60.0,
                                        EPSI_REFE = 3.E-4,
                                        RESI_REFE_RELA = 1.E-3),
                       SOLVEUR = _F(METHODE = 'MUMPS',
                                    NPREC = -1))

# Compute stresses
result = CALC_CHAMP(reuse = result,
                    RESULTAT = result,
                    CRITERES = ('SIEQ_ELNO', 'SIEQ_NOEU'),
                    CONTRAINTE = ('SIGM_ELNO', 'SIGM_NOEU'))

# Compute strains
result = CALC_CHAMP(reuse = result,
                    RESULTAT = result,
                    DEFORMATION = ('EPSG_NOEU'))

# Compute error  
result = CALC_ERREUR(reuse = result,
                     RESULTAT = result,
                     OPTION = ('ERME_ELEM','ERME_ELNO'))

# Save results
IMPR_RESU(FORMAT = 'MED',
          UNITE = 80,
          RESU = _F(RESULTAT = result))

#--------------------------------------------------------------------------------
# Verification (comparison with results for nu = 0.5)
TEST_RESU(RESU = (_F(GROUP_NO = 'A',
                     INST = 1.0,
                     REFERENCE = 'ANALYTIQUE',
                     RESULTAT = result,
                     NOM_CHAM = 'DEPL',
                     NOM_CMP = 'DX',
                     VALE_CALC = 0.,
                     VALE_REFE = 0.0,
                     CRITERE = 'ABSOLU',
                     PRECISION = 1.0E-05),
                  _F(GROUP_NO = 'A',
                     INST = 1.0,
                     REFERENCE = 'ANALYTIQUE',
                     RESULTAT = result,
                     NOM_CHAM = 'DEPL',
                     NOM_CMP = 'DY',
                     VALE_CALC = 6.00000000069E-05,
                     VALE_REFE = 6.0E-05,
                     CRITERE = 'RELATIF',
                     PRECISION = 1.E-3),
                  _F(GROUP_NO = 'F',
                     INST = 1.0,
                     REFERENCE = 'ANALYTIQUE',
                     RESULTAT = result,
                     NOM_CHAM = 'DEPL',
                     NOM_CMP = 'DX',
                     VALE_CALC = -2.12187958591E-05,
                     VALE_REFE = -2.1217941000000001E-05,
                     CRITERE = 'RELATIF',
                     PRECISION = 1.E-3),
                  _F(GROUP_NO = 'F',
                     INST = 1.0,
                     REFERENCE = 'ANALYTIQUE',
                     RESULTAT = result,
                     NOM_CHAM = 'DEPL',
                     NOM_CMP = 'DY',
                     VALE_CALC = 2.12187958591E-05,
                     VALE_REFE = 2.1217941000000001E-05,
                     CRITERE = 'RELATIF',
                     PRECISION = 1.E-3)))
  
TEST_RESU(RESU = (_F(NUME_ORDRE = 1,
                     GROUP_NO = 'A',
                     REFERENCE = 'ANALYTIQUE',
                     RESULTAT = result,
                     NOM_CHAM = 'SIGM_NOEU',
                     NOM_CMP = 'SIXX',
                     VALE_CALC = 99.9217246395,
                     VALE_REFE = 99.956599999999995,
                     CRITERE = 'RELATIF',
                     PRECISION = 1.E-2),
                  _F(NUME_ORDRE = 1,
                     GROUP_NO = 'A',
                     REFERENCE = 'ANALYTIQUE',
                     RESULTAT = result,
                     NOM_CHAM = 'SIGM_NOEU',
                     NOM_CMP = 'SIYY',
                     VALE_CALC = -59.8704884242,
                     VALE_REFE = -59.9955,
                     CRITERE = 'RELATIF',
                     PRECISION = 0.02),
                  _F(NUME_ORDRE = 1,
                     GROUP_NO = 'A',
                     REFERENCE = 'ANALYTIQUE',
                     RESULTAT = result,
                     NOM_CHAM = 'SIGM_NOEU',
                     NOM_CMP = 'SIZZ',
                     VALE_CALC = 19.9777096378,
                     VALE_REFE = 19.932600000000001,
                     CRITERE = 'RELATIF',
                     PRECISION = 0.035000000000000003),
                  _F(NUME_ORDRE = 1,
                     GROUP_NO = 'A',
                     REFERENCE = 'ANALYTIQUE',
                     RESULTAT = result,
                     NOM_CHAM = 'SIGM_NOEU',
                     NOM_CMP = 'SIXY',
                     VALE_CALC = 0.0286856307459,
                     VALE_REFE = 0.0,
                     CRITERE = 'ABSOLU',
                     PRECISION = 1.2),
                  _F(NUME_ORDRE = 1,
                     GROUP_NO = 'F',
                     REFERENCE = 'ANALYTIQUE',
                     RESULTAT = result,
                     NOM_CHAM = 'SIGM_NOEU',
                     NOM_CMP = 'SIXX',
                     VALE_CALC = 20.027374205,
                     VALE_REFE = 20.003,
                     CRITERE = 'RELATIF',
                     PRECISION = 5.0000000000000001E-3),
                  _F(NUME_ORDRE = 1,
                     GROUP_NO = 'F',
                     REFERENCE = 'ANALYTIQUE',
                     RESULTAT = result,
                     NOM_CHAM = 'SIGM_NOEU',
                     NOM_CMP = 'SIYY',
                     VALE_CALC = 19.9786273692,
                     VALE_REFE = 20.003,
                     CRITERE = 'RELATIF',
                     PRECISION = 5.0000000000000001E-3),
                  _F(NUME_ORDRE = 1,
                     GROUP_NO = 'F',
                     REFERENCE = 'ANALYTIQUE',
                     RESULTAT = result,
                     NOM_CHAM = 'SIGM_NOEU',
                     NOM_CMP = 'SIZZ',
                     VALE_CALC = 19.9996095878,
                     VALE_REFE = 20.003,
                     CRITERE = 'RELATIF',
                     PRECISION = 5.0000000000000001E-3),
                  _F(NUME_ORDRE = 1,
                     GROUP_NO = 'F',
                     REFERENCE = 'ANALYTIQUE',
                     RESULTAT = result,
                     NOM_CHAM = 'SIGM_NOEU',
                     NOM_CMP = 'SIXY',
                     VALE_CALC = 20.0239407403,
                     VALE_REFE = 20.003,
                     CRITERE = 'RELATIF',
                     PRECISION = 5.0000000000000001E-3)))
  
TEST_RESU(RESU = (_F(GROUP_NO = 'A',
                     INST = 1.0,
                     REFERENCE = 'ANALYTIQUE',
                     RESULTAT = result,
                     NOM_CHAM = 'SIEQ_NOEU',
                     NOM_CMP = 'VMIS',
                     VALE_CALC = 138.384133052,
                     VALE_REFE = 138.52260000000001,
                     CRITERE = 'RELATIF',
                     PRECISION = 0.02,
                     GROUP_MA = 'M1'),
                  _F(GROUP_NO = 'A',
                     INST = 1.0,
                     REFERENCE = 'ANALYTIQUE',
                     RESULTAT = result,
                     NOM_CHAM = 'SIEQ_NOEU',
                     NOM_CMP = 'TRESCA',
                     VALE_CALC = 159.792223363,
                     VALE_REFE = 159.9521,
                     CRITERE = 'RELATIF',
                     PRECISION = 0.02,
                     GROUP_MA = 'M1'),
                  _F(GROUP_NO = 'A',
                     INST = 1.0,
                     REFERENCE = 'ANALYTIQUE',
                     RESULTAT = result,
                     NOM_CHAM = 'SIEQ_NOEU',
                     NOM_CMP = 'PRIN_1',
                     VALE_CALC = -59.8704935738,
                     VALE_REFE = -59.9955,
                     CRITERE = 'RELATIF',
                     PRECISION = 0.02,
                     GROUP_MA = 'M1'),
                  _F(GROUP_NO = 'A',
                     INST = 1.0,
                     REFERENCE = 'ANALYTIQUE',
                     RESULTAT = result,
                     NOM_CHAM = 'SIEQ_NOEU',
                     NOM_CMP = 'PRIN_2',
                     VALE_CALC = 19.9777096378,
                     VALE_REFE = 19.932600000000001,
                     CRITERE = 'RELATIF',
                     PRECISION = 0.035000000000000003,
                     GROUP_MA = 'M1'),
                  _F(GROUP_NO = 'A',
                     INST = 1.0,
                     REFERENCE = 'ANALYTIQUE',
                     RESULTAT = result,
                     NOM_CHAM = 'SIEQ_NOEU',
                     NOM_CMP = 'PRIN_3',
                     VALE_CALC = 99.9217297891,
                     VALE_REFE = 99.956599999999995,
                     CRITERE = 'RELATIF',
                     PRECISION = 1.E-2,
                     GROUP_MA = 'M1'),
                  _F(GROUP_NO = 'A',
                     INST = 1.0,
                     REFERENCE = 'ANALYTIQUE',
                     RESULTAT = result,
                     NOM_CHAM = 'SIEQ_NOEU',
                     NOM_CMP = 'VMIS_SG',
                     VALE_CALC = 138.384133052,
                     VALE_REFE = 138.52260000000001,
                     CRITERE = 'RELATIF',
                     PRECISION = 0.02,
                     GROUP_MA = 'M1')))
  
TEST_RESU(RESU = _F(INST = 1.0,
                    POINT = 1,
                    RESULTAT = result,
                    NOM_CHAM = 'ERME_ELEM',
                    NOM_CMP = 'NUEST',
                    VALE_CALC = 0.148713862986,
                    CRITERE = 'RELATIF',
                    GROUP_MA = 'M1'))
  
TEST_RESU(RESU = _F(GROUP_NO = 'A',
                    INST = 1.0,
                    RESULTAT = result,
                    NOM_CHAM = 'ERME_ELNO',
                    NOM_CMP = 'ERREST',
                    VALE_CALC = 9.52296383685E-05,
                    CRITERE = 'RELATIF',
                    GROUP_MA = 'M1'))
  
  
TEST_RESU(RESU = (_F(NUME_ORDRE = 1,
                     GROUP_NO = 'A',
                     REFERENCE = 'ANALYTIQUE',
                     RESULTAT = result,
                     NOM_CHAM = 'EPSG_NOEU',
                     NOM_CMP = 'EPXX',
                     VALE_CALC = 0.000599576100401,
                     VALE_REFE = 0.0005994604316761909,
                     CRITERE = 'RELATIF',
                     PRECISION = 2.E-4),
                  _F(NUME_ORDRE = 1,
                     GROUP_NO = 'A',
                     REFERENCE = 'ANALYTIQUE',
                     RESULTAT = result,
                     NOM_CHAM = 'EPSG_NOEU',
                     NOM_CMP = 'EPYY',
                     VALE_CALC = -0.000598857526075,
                     VALE_REFE = -0.0006001799999999502,
                     CRITERE = 'RELATIF',
                     PRECISION = 2.5E-3)))
  

# End
FIN()
