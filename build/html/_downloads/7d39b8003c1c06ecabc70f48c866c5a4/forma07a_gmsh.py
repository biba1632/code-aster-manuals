import gmsh
import sys
import math

# Use OpenCASCADE kernel
gmsh.initialize()
model = gmsh.model
occ = model.occ
mesh = model.mesh
model.add("forma07a")

# Parameters
# Box size
L = 20
# initial crack opening
w = 0.005
# Initial crack length
a = 2
# Element size (at points, overwritten later when using
# transfinite interpolation)
lc = 20/10.0

# Create the top and bottom box
box_bot = occ.addBox(0, 0, -L/2, L/2, L/2, L/2)
box_top = occ.addBox(0, 0, 0, L/2, L/2, L/2)

# Join them while retaining the interace between them
box = occ.fragment([(3, box_bot)], [(3, box_top)])

# Add a torus around the crack tip location
torus = occ.addTorus(0, 0, 0, a, 0.5, tag = 1000, angle = math.pi/2)

# Partition the box with the torus
box_torus = occ.fragment(box[0], [(3, torus)])

# Make sure all new geometric entities are synchronized
occ.synchronize()

# Add the circular crack
# Crack tip
p0 = occ.addPoint(0, 0, 0, lc/10)
p1 = occ.addPoint(a, 0, 0, lc/10)
p2 = occ.addPoint(0, a, 0, lc/10)
l1 = occ.addCircleArc(p1, p0, p2)

# Crack face top/bottom points
p3 = occ.addPoint(0, 0, w/2, lc/5)
p4 = occ.addPoint(0, 0, -w/2, lc/5)

# Crack top face lines
l2 = occ.addLine(p3, p1)
l3 = occ.addLine(p2, p3)

# Crack bottom face lines
l4 = occ.addLine(p4, p1)
l5 = occ.addLine(p2, p4)

# Crack opening line
l6 = occ.addLine(p3, p4)

# Add curve loops
loop1 = occ.addCurveLoop([l2, l1, l3])
loop2 = occ.addCurveLoop([l4, l1, l5])
loop3 = occ.addCurveLoop([l6, l4, -l2])
loop4 = occ.addCurveLoop([l6, l5, -l3])

# Add faces
face1 = occ.addSurfaceFilling(loop1)
face2 = occ.addSurfaceFilling(loop2)
face3 = occ.addSurfaceFilling(loop3)
face4 = occ.addSurfaceFilling(loop4)

# Add surface loop
sloop1 = occ.addSurfaceLoop([face1, face4, face2, face3])

# Add volume
crack = occ.addVolume([sloop1])

# Subtract the crack from the box-torus partitioned geometry
box_torus_crack = occ.cut(box_torus[0], [(3, crack)], -1, True, True)
occ.removeAllDuplicates()
occ.synchronize()

# Add physical group for the mesh
box_with_crack = model.addPhysicalGroup(3, [1, 2, 3, 4])
model.setPhysicalName(3, box_with_crack, "box_with_crack")

# Add physical groups for the top and bottom faces
bot_face = model.addPhysicalGroup(2, [11])
top_face = model.addPhysicalGroup(2, [19])
model.setPhysicalName(2, bot_face, "bot_face")
model.setPhysicalName(2, top_face, "top_face")

# Add physical groups for defining the crack
crack_front = model.addPhysicalGroup(1, [32])
model.setPhysicalName(1, crack_front, "crack_front")

crack_top_surf = model.addPhysicalGroup(2, [22, 25])
crack_bot_surf = model.addPhysicalGroup(2, [8, 15])
model.setPhysicalName(2, crack_top_surf, "crack_top_surf")
model.setPhysicalName(2, crack_bot_surf, "crack_bot_surf")

# Add physical groups for the crack start/end vertices
crack_tip_xz = model.addPhysicalGroup(0, [26])
crack_tip_yz = model.addPhysicalGroup(0, [25])
model.setPhysicalName(0, crack_tip_xz, "crack_tip_xz")
model.setPhysicalName(0, crack_tip_yz, "crack_tip_yz")

# Add physical groups for symmetry faces and fixed corner
symm_face_xz = model.addPhysicalGroup(2, [7, 18, 16, 26])
symm_face_yz = model.addPhysicalGroup(2, [6, 17, 13, 24])
model.setPhysicalName(2, symm_face_xz, "symm_face_xz")
model.setPhysicalName(2, symm_face_yz, "symm_face_yz")

fixed_vert = model.addPhysicalGroup(0, [8])
model.setPhysicalName(0, fixed_vert, "fixed_vert")

# Update
occ.synchronize()

# Set element sizes near the crack (these may be overwritten by
# the transfinite interpolation definitions)
crack_pts = [(0, 26), (0, 25), (0, 9), (0, 4), (0, 20), (0, 30), (0, 23), (0, 32)]
mesh.setSize(crack_pts, lc/10)

# Set element sizes for transfinite interpolation
num_nodes = 10
radial_curves = [(1, 31), (1, 33), (1, 48), (1, 29), (1, 30), (1, 47)]
for curve in radial_curves:
    mesh.setTransfiniteCurve(curve[1], num_nodes)
    mesh.setSmoothing(curve[0], curve[1], 100)
    
num_nodes = 20
torus_curves = [(1, 22), (1, 42), (1, 18), (1, 37)]
for curve in torus_curves:
    mesh.setTransfiniteCurve(curve[1], num_nodes)
    mesh.setSmoothing(curve[0], curve[1], 100)
    
num_nodes = 10
crack_curves = [(1, 23), (1, 41)]
for curve in crack_curves:
    mesh.setTransfiniteCurve(curve[1], num_nodes, coef=0.83)
    mesh.setSmoothing(curve[0], curve[1], 100)

crack_curves = [(1, 17), (1, 38)]
for curve in crack_curves:
    mesh.setTransfiniteCurve(curve[1], num_nodes, coef=1.2)
    mesh.setSmoothing(curve[0], curve[1], 100)

num_nodes = 15
box_corner_edges = [(1, 16), (1, 34)]
for curve in box_corner_edges:
    mesh.setTransfiniteCurve(curve[1], num_nodes, coef=1.2)
    mesh.setSmoothing(curve[0], curve[1], 100)
    
box_corner_edges = [(1, 16)]
for curve in box_corner_edges:
    mesh.setTransfiniteCurve(curve[1], num_nodes, coef=0.83)
    mesh.setSmoothing(curve[0], curve[1], 100)

box_symm_edges = [(1, 8), (1, 4)]
for curve in box_symm_edges:
    mesh.setTransfiniteCurve(curve[1], num_nodes, coef=1.5)
    mesh.setSmoothing(curve[0], curve[1], 100)

# Set meshing options
gmsh.option.setNumber('Mesh.RecombinationAlgorithm', 1)
gmsh.option.setNumber('Mesh.Recombine3DLevel', 2)
gmsh.option.setNumber('Mesh.ElementOrder', 2)
gmsh.option.setNumber('Mesh.OptimizeNetgen', 1)
gmsh.option.setNumber('Mesh.MeshSizeMax', 5)
gmsh.option.setNumber('Mesh.MshFileVersion', 2.2)

# Generate mesh
mesh.generate(3)
mesh.removeDuplicateNodes()

# Save mesh
gmsh.write("forma07a_gmsh.msh")

# Finish
gmsh.finalize()
