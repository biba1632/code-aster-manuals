.. _u2.00.01:

*********************************************************
**[U2.00.01]**: List of U2 ``Code_aster`` usage documents
*********************************************************

This document contains an inventory of documents on various analysis 
methods. 


General information on finite elements
========================================

* **[U2.01.02]** Note of use of the boundary conditions treated by elimination
* **[U2.01.04]** Documentation of the sizes of Code_Aster
* **[U2.01.05]** Constraints, efforts, forces and deformations
* **[U2.01.09]** analytical Definition of a stress field and a field of internal variables initial
* **[U2.01.10]** Note of use on the choice of the finite elements
* **[U2.01.11]** Note of use of the handling of fields

Use of structural elements
============================

* **[U2.02.01]** Note of use of the voluminal elements plates, hulls, hulls SHB, grids and membranes
* **[U2.02.02]** Note of use of the elements TUYAU_*
* **[U2.02.03]** Note on the use of discrete elements

Nonlinear mechanics
=====================

* **[U2.04.01]** the Councils of use of STAT_NON_LINE
* **[U2.04.02]** the Councils of implementation of non-linear calculations
* **[U2.04.03]** Choice of the behavior élasto- (visco) - plastic
* :ref:`[U2.04.04] <u2.04.04>` Note of use of the contact
* **[U2.04.07]** Use of transitory methods of resolution for the strongly nonlinear quasi-static problems
* **[U2.06.13]** General advices of use of the operator DYNA_NON_LINE
* **[U2.10.01]** Note of use of the coupling between Code_Aster and the modules of laws of Zmat behavior and UMAT

Fracture, fatigure and damage
===============================

* **[U2.05.00]** methodological Guide on the approaches in breaking process
* **[U2.05.01]** Note  of   use  of  the  operators  of   breaking  process  for the  classical  approach  (non-linear elasticity)
* :ref:`u2.05.02` 
* **[U2.05.04]** Note of use for the calculation of limiting load
* **[U2.05.05]** Structural analysis in fatigue vibratory
* **[U2.05.06]** Realization of calculations of damage into quasi-static
* **[U2.05.07]** Note of use of the models of cohesive zones
* **[U2.05.08]** Realization of a calculation of prediction of rupture per cleavage
* **[U2.05.09]** Note of use of method GTP

Mechanical analyses
=====================

* **[U2.06.01]** Implementation of a calculation of clean modes of a structure
* **[U2.06.03]** Note of modeling of the mechanical cushioning
* **[U2.06.14]** the Councils of implementation of calculations in Interaction Fluid-Structure
* **[U2.08.04]** Note of calculation to buckling
* **[U2.09.02]** Realization of the calculation of an assembly pin-attaches
* **[U2.09.03]** Note of use of the calculation and the postprocessing of a mechanical study according to the RCCM
* **[U2.11.01]** Note on the use of 1D-3D connections

Metallurgy and welding
========================

* **[U2.03.04]** Note of use for calculations thermometallomecanic on steels
* **[U2.03.05]** Note of use for the digital simulation of welding

Civil engineering and soil mechanics
======================================

* **[U2.03.06]** Realization of a study civil engineer with cables of prestressing
* **[U2.03.07]** Panorama of the tools available to carry out structural analyses of concrete Génie Civil
* **[U2.03.08]** Fast dynamics of a prestressed cable using the macro-command ``CALC_EUROPLEXUS``: civil engineerig case study
* **[U2.04.05]** Note of use of model THM
* **[U2.04.06]** How to dig a tunnel: methodology of excavation

Earthquakes
=============

* **[U2.06.07]** Soil-structure interaction in seismic analysis with the ``Code_Aster`` interface - ``MISS3D``
* **[U2.06.08]** Dynamic separation of foundation in ground-structure interaction using method of elastic springs
* **[U2.06.09]** Seismic analysis: application to pipings
* **[U2.06.10]** A study of civil engineering study under seismic loading
* **[U2.06.11]** Analysis of the seismic behaviour of large metal tanks
* **[U2.06.12]** Soil-structure interaction in seismic analysis taking into account spatial variability
* **[U2.06.15]** Calculation of seismic resistance of concrete dams
* **[U2.06.21]** Soil-structure interaction and soil-fluid-structure interaction in seismic analysis with the ``Code_Aster`` interface - ``MISS3D``

Rotating machines
===================

* **[U2.06.31]** Note of modeling of the gyroscopy
* **[U2.06.32]** Note of implementation of calculations of rotors

Model-size reduction
=======================

* **[U2.06.04]** Note for the construction of scale models in dynamics
* **[U2.07.01]** Note of use of modeling FOURIER
* **[U2.07.02]** Note of use of the static under-structuring
* **[U2.07.03]** Realization of a study of structural modification starting from measured data
* **[U2.07.04]** dynamic Condensation of model by under static structuring
* **[U2.07.05]** Implementation of calculation by dynamic under-structuring

Performance and quality of calculations
==========================================

* **[U2.06.41]** dynamic Validation of model per correlation calculation-tests
* **[U2.06.42]** Implementation of a model of modal retiming
* **[U2.08.01]** Use of the indicators of error and associated strategies of adaptation of grids
* **[U2.08.03]** Note of use of the linear solveurs
* **[U2.08.05]** Digital simulation of Monte Carlo
* **[U2.08.06]** Note of use of parallelism
* **[U2.08.07]** Distribution of parametric calculations
* **[U2.08.08]** Use of the Method of the Solutions Manufactured for the software checking
* **[U2.08.09]** Adaptation of grid into non-linear

Tools for pre- and post-processing
==================================

* **[U2.10.02]** Use of ``MFront`` with ``Code_Aster``
* **[U2.51.01]** Note of use of Grace for Code_Aster
* **[U2.51.02]** Layout of curves with Code_Aster

