# Start
DEBUT(LANG='EN')

# Read mesh
mesh = LIRE_MAILLAGE(FORMAT = 'MED')

# Assign 3D mechanics finite elements
model = AFFE_MODELE(MAILLAGE = mesh,
                    AFFE = _F(TOUT = 'OUI',
                              PHENOMENE = 'MECANIQUE',
                              MODELISATION = '3D'))

# Reorient faces where tractions are applied to point outwards
mesh = MODI_MAILLAGE(reuse = mesh,
                     MAILLAGE = mesh,
                     ORIE_PEAU_3D = _F(GROUP_MA = ('top_face','bot_face',)))

# Convert to Barsoum elements at crcak tip
mesh = MODI_MAILLAGE(reuse = mesh,
                     MAILLAGE = mesh,
                     MODI_MAILLE = _F(OPTION = 'NOEUD_QUART',
                                      GROUP_MA_FOND = 'crack_front'))

# Define material
steel = DEFI_MATERIAU(ELAS = _F(E = 2.E11,
                                NU = 0.3))

# Assign material
mater = AFFE_MATERIAU(MAILLAGE = mesh,
                      AFFE = _F(TOUT = 'OUI',
                                MATER = steel))

# Assign displacement bcs
disp_bc = AFFE_CHAR_MECA(MODELE = model,
                         DDL_IMPO = _F(GROUP_NO = 'fixed_vert',
                                       DZ = 0.0),
                         FACE_IMPO = (_F(GROUP_MA = 'symm_face_xz',
                                         DNOR = 0.0),
                                      _F(GROUP_MA = 'symm_face_yz',
                                         DNOR = 0.0)))

# Assign traction bcs
trac_bc = AFFE_CHAR_MECA(MODELE = model,
                         FORCE_FACE = (_F(GROUP_MA = 'top_face',
                                          FZ = 1.E6),
                                       _F(GROUP_MA = 'bot_face',
                                          FZ = -1.E6)))

# Solve
result = MECA_STATIQUE(MODELE = model,
                       CHAM_MATER = mater,
                       EXCIT = (_F(CHARGE = trac_bc),
                                _F(CHARGE = disp_bc)))

# Add stresses to result
result = CALC_CHAMP(reuse = result,
                    RESULTAT = result,
                    CONTRAINTE = ('SIGM_ELNO', 'SIGM_NOEU'))

# Write the result 
IMPR_RESU(FORMAT = 'MED',
          UNITE = 80,
          RESU = _F(RESULTAT = result))


# Define the crack front
crack = DEFI_FOND_FISS(MAILLAGE = mesh,
                       FOND_FISS = _F(GROUP_MA ='crack_front',
                                      GROUP_NO_ORIG = 'crack_tip_xz',
                                      GROUP_NO_EXTR = 'crack_tip_yz'),
                       SYME = 'NON',
                       LEVRE_SUP = _F(GROUP_MA = 'crack_top_surf'),
                       LEVRE_INF = _F(GROUP_MA = 'crack_bot_surf'),
                    )


# Calculate G
# Using Legendre interpolation (default)
r_max=0.5
r_min=0.2
G_LEG = CALC_G(RESULTAT = result,
               OPTION = 'CALC_G',
               THETA = _F(FOND_FISS = crack,
                          R_SUP = r_max,
                          R_INF = r_min),
               LISSAGE = _F(LISSAGE_THETA = 'LEGENDRE',
                            LISSAGE_G = 'LEGENDRE',
                            DEGRE = 5))

# Write table
IMPR_TABLE(TABLE = G_LEG)


# Calculate G
# Using Lagrange interpolation (default)
G_LAG = CALC_G(RESULTAT = result,
               OPTION = 'CALC_G',
               THETA = _F(FOND_FISS = crack,
                          R_SUP = r_max,
                          R_INF = r_min),
               LISSAGE = _F(LISSAGE_THETA = 'LAGRANGE',
                            LISSAGE_G = 'LAGRANGE'))

# Write table
IMPR_TABLE(TABLE = G_LAG)


# Calculate G
# Using node-to-node Lagrange interpolation (default)
G_LAGN = CALC_G(RESULTAT = result,
                OPTION = 'CALC_G',
                THETA = _F(FOND_FISS = crack,
                           R_SUP = r_max,
                           R_INF = r_min),
                LISSAGE = _F(LISSAGE_THETA = 'LAGRANGE',
                             LISSAGE_G = 'LAGRANGE_NO_NO'))

# Write table
IMPR_TABLE(TABLE = G_LAGN)


# Calculate K with POST_K1_K2_K3 
#-------------------------------
K = POST_K1_K2_K3(RESULTAT = result,
                  FOND_FISS = crack,
                  ABSC_CURV_MAXI = 1.5,
                  TYPE_MAILLAGE = 'LIBRE')

# Write table
IMPR_TABLE(TABLE = K)

# Save x-y data
C_G_LEG = RECU_FONCTION(TABLE = G_LEG,
                        PARA_X = 'ABSC_CURV',
                        PARA_Y = 'G')

C_G_LAG = RECU_FONCTION(TABLE = G_LAG,
                        PARA_X = 'ABSC_CURV',
                        PARA_Y = 'G')

C_G_LAGN=RECU_FONCTION(TABLE = G_LAGN,
                       PARA_X = 'ABSC_CURV',
                       PARA_Y = 'G')

IMPR_FONCTION(FORMAT='TABLEAU',
              UNITE=31,
              COURBE=(_F(FONCTION = C_G_LEG),
                      _F(FONCTION = C_G_LAG),
                      _F(FONCTION = C_G_LAGN)),
              SEPARATEUR = ',')

# Verify reference values (for inifnite plate)
G_ref = 11.58648
K1_ref = 1.5957e6
TEST_TABLE(REFERENCE = 'ANALYTIQUE',
           PRECISION = 8.0E-3,
           VALE_CALC = 1607011.55098,
           VALE_REFE = 1.595700E6,
           NOM_PARA = 'K1',
           TYPE_TEST = 'MAX',
           TABLE=K)

# Finish
FIN();
