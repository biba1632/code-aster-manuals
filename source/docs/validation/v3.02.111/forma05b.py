import salome
import GEOM
import math
from salome.geom import geomBuilder

import SMESH, SALOMEDS
from salome.smesh import smeshBuilder

ExportPATH="/home/banerjee/Salome/forma05/"

# Start salome
salome.salome_init()

#--------------------------------------------
# Create geometry
#--------------------------------------------
geompy = geomBuilder.New()

# Create coordinate system
O = geompy.MakeVertex(0, 0, 0)
OX = geompy.MakeVectorDXDYDZ(1, 0, 0)
OY = geompy.MakeVectorDXDYDZ(0, 1, 0)
OZ = geompy.MakeVectorDXDYDZ(0, 0, 1)

# Geometry parameters
crack_width = 1.0e-4
crack_length = 0.1
height = 2.0
width = 1.0

# Create sketch of full plate
sk = geompy.Sketcher2D()
sk.addPoint(0.0, crack_width)
sk.addSegmentAbsolute(0.000000, height/2)
sk.addSegmentAbsolute(width, height/2)
sk.addSegmentAbsolute(width, 0.000000)
sk.addSegmentAbsolute(width, -height/2)
sk.addSegmentAbsolute(0.0, -height/2)
sk.addSegmentAbsolute(0.0, -crack_width)
sk.addSegmentAbsolute(crack_length, 0.000000)
sk.close()

# Add the boundary wire
coord_sys_2D = geompy.MakeMarker(0, 0, 0, 1, 0, 0, 0, 1, 0)
plate_boundary = sk.wire(coord_sys_2D)

# Create the plate face
plate = geompy.MakeFaceWires([plate_boundary], 1)

# Create a partition of the plate
v1 = geompy.MakeVertex(crack_length, 0, 0)
v2 = geompy.MakeVertex(2, 0, 0)
l1 = geompy.MakeLineTwoPnt(v1, v2)

plate_partitioned = geompy.MakePartition([plate], [l1], [], [],
                                         geompy.ShapeType["FACE"], 0, [], 0)

# Create geometry groups
top_edge = geompy.CreateGroup(plate_partitioned, geompy.ShapeType["EDGE"])
bot_edge = geompy.CreateGroup(plate_partitioned, geompy.ShapeType["EDGE"])
crack_top_surf = geompy.CreateGroup(plate_partitioned, geompy.ShapeType["EDGE"])
crack_bot_surf = geompy.CreateGroup(plate_partitioned, geompy.ShapeType["EDGE"])
crack_tip = geompy.CreateGroup(plate_partitioned, geompy.ShapeType["VERTEX"])
top_G = geompy.CreateGroup(plate_partitioned, geompy.ShapeType["VERTEX"])
top_D = geompy.CreateGroup(plate_partitioned, geompy.ShapeType["VERTEX"])

# Locate the top edge
v_test =  geompy.MakeVertex(width/2, height/2, 0)
edge = geompy.GetShapesNearPoint(plate_partitioned, v_test, geompy.ShapeType['EDGE'])
geompy.UnionList(top_edge, [edge])

# Locate point G on the top edge
v_test =  geompy.MakeVertex(0, height/2, 0)
vertex = geompy.GetShapesNearPoint(edge, v_test, geompy.ShapeType['VERTEX'])
geompy.UnionList(top_G, [vertex])

# Locate point D on the top edge
v_test =  geompy.MakeVertex(width, height/2, 0)
vertex = geompy.GetShapesNearPoint(edge, v_test, geompy.ShapeType['VERTEX'])
geompy.UnionList(top_D, [vertex])

# Locate the bottom edge
v_test =  geompy.MakeVertex(width/2, -height/2, 0)
edge = geompy.GetShapesNearPoint(plate_partitioned, v_test, geompy.ShapeType['EDGE'])
geompy.UnionList(bot_edge, [edge])

# Locate the crack top surface
v_test =  geompy.MakeVertex(crack_length/2, crack_width, 0)
edge = geompy.GetShapesNearPoint(plate_partitioned, v_test, geompy.ShapeType['EDGE'])
geompy.UnionList(crack_top_surf, [edge])

# Locate the crack bottom surface
v_test =  geompy.MakeVertex(crack_length/2, -crack_width, 0)
edge = geompy.GetShapesNearPoint(plate_partitioned, v_test, geompy.ShapeType['EDGE'])
geompy.UnionList(crack_bot_surf, [edge])

# Locate the crack tip vertex
v_test =  geompy.MakeVertex(crack_length, 0, 0)
vertex_O = geompy.GetShapesNearPoint(edge, v_test, geompy.ShapeType['VERTEX'])
geompy.UnionList(crack_tip, [vertex_O])

# Add all geometric entities to the study
geompy.addToStudy( O, 'O' )
geompy.addToStudy( OX, 'OX' )
geompy.addToStudy( OY, 'OY' )
geompy.addToStudy( OZ, 'OZ' )
geompy.addToStudy( plate_partitioned, 'plate' )
geompy.addToStudy( vertex_O, 'vertex_O' )
geompy.addToStudyInFather( plate_partitioned, top_edge, 'top_edge' )
geompy.addToStudyInFather( plate_partitioned, bot_edge, 'bot_edge' )
geompy.addToStudyInFather( plate_partitioned, crack_top_surf, 'crack_top_surf' )
geompy.addToStudyInFather( plate_partitioned, crack_bot_surf, 'crack_bot_surf' )
geompy.addToStudyInFather( plate_partitioned, crack_tip, 'crack_tip' )
geompy.addToStudyInFather( plate_partitioned, top_G, 'top_G' )
geompy.addToStudyInFather( plate_partitioned, top_D, 'top_D' )

#--------------------------------------------
# Create Mesh
#--------------------------------------------
smesh = smeshBuilder.New()
mesh = smesh.Mesh(plate_partitioned)

# Meshing parameters
max_size = 0.05
min_size = 0.002
local_size_at_O = 0.002

# Set up meshing algorithm
NETGEN_1D_2D = mesh.Triangle(algo = smeshBuilder.NETGEN_1D2D)
mesh_params = NETGEN_1D_2D.Parameters()
mesh_params.SetMaxSize( max_size )
mesh_params.SetMinSize( min_size )
mesh_params.SetSecondOrder( 0 )
mesh_params.SetOptimize( 1 )
mesh_params.SetFineness( 4 )
mesh_params.SetLocalSizeOnShape(vertex_O, local_size_at_O)

# Compute the mesh
isDone = mesh.Compute()

# Move geometry groups to the mesh
crack_tip_1 = mesh.GroupOnGeom(crack_tip, 'crack_tip', SMESH.NODE)
top_G_1 = mesh.GroupOnGeom(top_G, 'top_G', SMESH.NODE)
top_D_1 = mesh.GroupOnGeom(top_D, 'top_D', SMESH.NODE)
top_edge_1 = mesh.GroupOnGeom(top_edge, 'top_edge', SMESH.EDGE)
bot_edge_1 = mesh.GroupOnGeom(bot_edge, 'bot_edge', SMESH.EDGE)
crack_top_surf_1 = mesh.GroupOnGeom(crack_top_surf, 'crack_top_surf', SMESH.EDGE)
crack_bot_surf_1 = mesh.GroupOnGeom(crack_bot_surf, 'crack_bot_surf', SMESH.EDGE)

# Set names of mesh objects
smesh.SetName(mesh.GetMesh(), 'mesh')
smesh.SetName(crack_tip_1, 'crack_tip')
smesh.SetName(top_G_1, 'top_G')
smesh.SetName(top_D_1, 'top_D')
smesh.SetName(top_edge_1, 'top_edge')
smesh.SetName(bot_edge_1, 'bot_edge')
smesh.SetName(crack_top_surf_1, 'crack_top_surf')
smesh.SetName(crack_bot_surf_1, 'crack_bot_surf')
smesh.SetName(NETGEN_1D_2D.GetAlgorithm(), 'NETGEN 1D-2D')
smesh.SetName(mesh_params, 'NETGEN 2D Parameters_1')

# Save mesh
mesh.ExportMED( r''+ExportPATH+'forma05b.mmed'+'')

# Update objecte tree in GUI
if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()
