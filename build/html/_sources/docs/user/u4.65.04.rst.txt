.. role:: red
.. role:: blue
.. role:: green
.. role:: gray
.. role:: python(code)
   :language: python
.. |br| raw:: html

   <br/>

.. index:: ! ASSE_MATR_GENE

.. _u4.65.04:

************************************************************************
**[U4.65.04]** : Operator ``ASSE_MATR_GENE``
************************************************************************

.. _u4.65.04.1:

Purpose
=======

To assemble the stiffness or mass matrix from the substructures.

Used for dynamic substructuring (modal or harmonic analysis), this operator creates the generalized matrix of
stiffness or mass or possibly of damping (harmonic or transient analysis), by assembly of the generalized
matrices of the corresponding :green:`macr_elem_dyna` type (cf. operator :red:`MACR_ELEM_DYNA`
:ref:`[U4.65.01] <u4.65.01>`) contained in a list defined during the creation of the generalized model
(cf. operator :red:`DEFI_MODELE_GENE` :ref:`[U4.65.02] <u4.65.02>`). The assembled generalized matrix is ​​real
and symmetric (storage of the lower triangular part is sufficient). It is built using a numbering scheme based
on the generalized degrees of freedom and stored as a "skyline" matrix. The addressing tables are those
calculated beforehand by operator :red:`NUME_DDL_GENE` :ref:`[U4.65.03] <u4.65.03>`.

The output concept produced by this operator is of type: :green:`matr_asse_gene_R`.


.. _u4.65.04.2:

Syntax
======

::

  ma_gene [matr_asse_gene_R] = ASSE_MATR_GENE
   (

    ♦ NUME_DDL_GENE = nu_gene,                                [nume_ddl_gene]
    ◊ METHODE = / 'CLASSIQUE'‚                                [default]
                / 'INITIAL',

    # If METHODE = 'CLASSIQUE':

      ♦ OPTION = / 'RIGI_GENE',
                 / 'RIGI_GENE_C',
                 / 'MASS_GENE',
                 / 'AMOR_GENE'
   )
   
.. _u4.65.04.3:

Operands
========


.. _u4.65.04.3.1:

Operand :blue:`NUME_DDL_GENE`
-----------------------------

::

   ♦ NUME_DDL_GENE = nu_gene

Name of the concept :green:`nume_ddl_gene` resulting from the operator :red:`NUME_DDL_GENE`
:ref:`[U4.65.03] <u4.65.03>` which defines the numbering of the equations of the assembled generalized
system, the storage mode of coefficients of the assembled generalized matrix (skyline) and the generalized
model on which diagonal or full assembly operations are carried out.

.. _u4.65.04.3.2:

Operand :blue:`METHODE`
-----------------------

::
   
  ◊ METHOD = 'CLASSIQUE'

Builds a generalized numbering that takes into account the coupling equations by the method of double
Lagrange multipliers or elimination (cf :ref:`R4.06.02 <r4.06.02>`). The method used is chosen according to
the method indicated in the operator :red:`NUME_DDL_GENE` :ref:`[U4.65.03] <u4.65.03>`

::

   ◊ METHODE = 'INITIAL'

Initialize a null matrix of the type :green:`matr_asse_gene_R` which one can fill with Python methods. This
method was created for development.

.. _u4.65.04.3.3:

Operand :blue:`OPTION`
----------------------

::

   ♦ OPTION

The option makes it possible to determine the list of the macro-elements, contained in the concept
:green:`modele_gene` resulting from :red:`DEFI_MODELE_GENE` :ref:`[U4.65.02] <u4.65.02>`, to be assembled.
It defines by the type of the assembled generalized matrix calculated by the operator :red:`ASSE_MATR_GENE`.

::

   'RIGI_GENE'

Calculation of the assembled generalized stiffness matrix, including terms associated with the LAGRANGE
multipliers

::

   'RIGI_GENE_C'

Calculation of the complex generalized stiffness matrix

::

   'MASS_GENE'

Calculation of the assembled generalized mass matrix

::

   'AMOR_GENE'

Computation of the generalized assembled damping matrix.

.. _u4.65.04.4:

Execution phase
===============

The terms corresponding to the projected matrices are assembled without further treatment. On the other hand,
the terms corresponding to the dualisation of the connections are subjected to simple conditioning. They are
multiplied by a uniquely defined factor which is such that the maximum absolute value of the terms of
dualisation is equal to the maximum absolute value of the terms of macro-element stiffness (matrices of the
projected substructures).
