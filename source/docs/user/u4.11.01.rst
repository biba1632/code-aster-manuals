.. role:: red
.. role:: blue
.. role:: green
.. role:: gray
.. role:: python(code)
   :language: python
.. |br| raw:: html

   <br/>

.. index:: ! DEBUT

.. _u4.11.01:

************************************************************************
**[U4.11.01]** : Procedure ``DEBUT``
************************************************************************

.. _u4.11.01.1:

Purpose
=======

Assign resources for memory, disk and files.

The execution of a run consists of a set of commands starting with :red:`DEBUT` and ending with :red:`FIN` 
:ref:`[U4.11.02] <u4.11.02>`, (see also the :red:`POURSUITE` procedure :ref:`[U4.11.03] <u4.11.03>`).

The :red:`DEBUT` command, which is executed as soon as it is read by the Supervisor, performs the 
following tasks:

* definition of the characteristics of the databases (managed by :green:`JEVEUX`) and allocation of
  associated files,
* reading catalogs of elements and commands

The apparently complex syntax of this procedure should not worry the user; a call with the default operands, 
sufficient in most cases, is: 

::

   DEBUT()

The operands are to be used in the case of studies for which database sizes are important or to change 
the various numbers of logical unit of files to values that are different from the numbers assigned by 
default.

Any commands before :red:`DEBUT`, if they are syntactically correct, are ignored.

 
.. _u4.11.01.2:

Syntax
======

::

  DEBUT(

    ◊ PAR_LOT = / 'OUI',                              [DEFAULT]
                / 'NON',
    ◊ IMPR_MACRO = / 'NON',                           [DEFAULT]
                   / 'OUI',
    ◊ LANG = lang,                                    [TXM]
    ◊ BASE = _F(
                ♦ FICHIER = / 'GLOBALE',
                            / 'VOLATILE',
                            / | LONG_ENRE = lenr,     [I]
                              | NMAX_ENRE = nenr,     [I]
                              | LONG_REPE = lrep,     [I]
               ),
    ◊ CODE = _F(
                ♦ NIV_PUB_WEB = / 'INTERNET',
                                / 'INTRANET',
               ),
    ◊ ERREUR = _F(
                  ERREUR_F = / 'ABORT',               [DEFAULT]
                             / 'EXCEPTION',
                 ),
    ◊ IGNORE_ALARM = l_vale,                          [l_Kn]
    ◊ DEBUG = _F(
                 JXVERI = / 'OUI',
                          / 'NON',
                 ENVIMA = 'TEST',                     [l_Kn]
                 ◊ JEVEUX = / 'OUI',
                            / 'NON',
                 ◊ SDVERI = / 'OUI',
                            / 'NON',
                 ◊ HIST_ETAPE = / 'NON',
                                / 'OUI',
                ),
    ◊ MESURE_TEMPS = _F(
                        ◊ NIVE_DETAIL = / 0           [DEFAULT]
                                        / 1
                                        / 2
                                        / 3
                        ◊ MOYENNE = / 'NON'           [DEFAULT]
                                    / 'OUI'
                       ),
    ◊ MEMOIRE = _F(
                   ◊ TAILLE_BLOCK = / 800.,           [DEFAULT]
                                    / tbloc,          [R]
                  ),
    ◊ CATALOGUE = _F(
                     ♦ FICHIER = nfic,                [l_Kn]
                     ◊ UNITE = unit,                  [I]
                    ),
    ◊ RESERVE_CPU = _F(
                       / VALE = value                 [R]
                       / POURCENTAGE = percent        [R]
                       ◊ BORNE = / bv,                [R]
                                 / 900.               [DEFAULT]),
  )

.. _u4.11.01.3:

Displayed messages
==================

At the start of the execution of ``Code_Aster``, a header is displayed. We find there :
* precise identification of the version used: version number, date of the last modifications,
* the date and time of the start of the execution,
* the name, architecture, operating system of the machine,
* the language used for displaying messages,
* the type of parallelism available (MPI / OpenMP), the number of allocated processors,
* the version of the libraries used (when available) for ``hdf5``, ``med``, ``mumps``, ``scotch``,
* then information on the distribution of the memory. For example :

  ::

     Memory limit for execution: 256.00 MB
     consumed by initialization: 148.68 MB
     by the objects of the command set: 17.48 MB
     remainder for dynamic allocation: 89.84 MB
     Swap file size limit: 48.00 GB

Which means :

* 256 MB is the amount of memory requested by the user, it is the total amount that is cannot be
  exceeded during execution.
* 148.68 MB is consumed simply to start the execution (loading the executable, associated dynamic 
  libraries, etc.).
* 17.48 MB is consumed to read the command file (Note: in mode :blue:`PAR_LOT = 'NON'`, the command 
  set is read progressively and this value will then be zero).
* 89.84 MB is the amount of memory available (at this moment) for the objects of the calculation (equal 
  to 256-148.68-17.48).  It is thus seen that computation cannot begin if this value is too low.

During execution, depending on the dynamic allocations made, when this value varies by more than 10% 
(upwards or downwards), a message of the following form is printed:

::

  The memory currently consumed except JEVEUX is 214.08 MB. 
  The limit of the dynamic allocation JEVEUX is fixed at 41.92 MB.

At the end of the execution, a report indicates whether the same calculation can be restarted with 
less memory:

::

  The memory requested at launch is overestimated, it is 256 MB.
  The peak memory used is 216.02 MB.

or, if more memory is required (depending on the platform, the maximum limit may be exceeded without 
the system having interrupted the calculation):

::

  The memory requested at launch is underestimated, it is 256 MB.
  The peak memory used is 273.22 MB.

.. _u4.11.01.4:

Operands
========

.. _u4.11.01.4.1:

Operand :blue:`PAR_LOT` (:gray:`BATCH`)
---------------------------------------

::

  PAR_LOT = 'OUI'                  (DEFAULT)
            'NON'

Batch processing of commands:

If :blue:`'OUI'` (default option), the supervisor analyzes all the commands before request execution.

If :blue:`'NON'`, after analyzing a command, the supervisor requests its execution and then proceed to the 
analysis (and to the execution) of the next command (processing command by command).

.. _u4.11.01.4.2:

Keyword :blue:`IMPR_MACRO` (:gray:`WRITE_MACRO`)
------------------------------------------------

::

   IMPR_MACRO =

Whether or not to print outputs produced by macros in the message file. Reading message files can be 
tedious when it contains all of the output of the subcommands generated by the macro itself. By default, 
only the echo of commands explicitly called by the user in the command set will appear.

.. _u4.11.01.4.3:

Keyword :blue:`LANG`
--------------------

This keyword allows you to choose the display language for the messages output by the code.

If the keyword is not filled in, the environment variables determine the language of the messages 
(reference: http://www.gnu.org/software/gettext/manual/gettext.html#Users). We can, for example, define 
in the ``~/.bashrc`` file: 

::

   export LANG = fr_FR.UTF-8.

The encoding (UTF-8 or ISO-8859-1) allows accented characters to be displayed correctly.

The :red:`LANG` keyword expects a two-letter value, for example 'FR' (for French) or 'EN' (forEnglish).

When a language is chosen (whether by the environment or :red:`LANG`), it is still necessary that the file
translated messages (.mo file) is available. This file is expected under this name:

::

  $ASTER_ROOT/share/locale/`lang`/LC_MESSAGES/aster_`version`.mo

where ``$ASTER_ROOT`` is the main directory of ``Code_Aster`` (e.g.: ``/aster`` or ``/opt/aster``), 
``lang`` is the lowercase name of the language (e.g. en, fr, de ...) and version is the name of the 
version of ``Code_Aster`` used (eg stable, testing, unstable).  i

If the translation file cannot be read, French is used.

.. admonition:: Remark

   Even if the translation file exists, when a message has not been translated, it is displayed
   in French (language in which the messages are written in the source code).

.. _u4.11.01.4.4:

Keyword :blue:`BASE` (:gray:`DATABASE`)
---------------------------------------

::

  BASE =

The functionality of this keyword is to redefine the values of the parameters of the direct access files
associated with "databases" if you do not want to use those set by default.

Default values for parameters associated with databases.

+-----------+-----------+
| GLOBALE   |           |
+-----------+-----------+
| NMAX_ENRE | 62914     |
+-----------+-----------+
| LONG_ENRE | 100 words |
+-----------+-----------+
| LONG_REPE | 2000      |
+-----------+-----------+
| VOLATILE  |           |
+-----------+-----------+
| NMAX_ENRE | 62914     |
+-----------+-----------+
| LONG_ENRE | 100 words |
+-----------+-----------+
| LONG_REPE | 2000      |
+-----------+-----------+

One  word is worth 8 bytes on a 64-bit platform under LINUX 64, TRU64 and IRIX 64, 4 bytes on a 32-bit 
platform under SOLARIS, HP-UX and WINDOWS-NT, LINUX.

Under Linux 64, with the default values, the :red:`DEBUT` procedure will allocate a direct access file from
plus 62914 records of 100 Kwords (the K is equal to 1024) for the 'GLOBALE' database.

.. admonition:: Remark 

   The actual file size is dynamic; it depends on the volume of information to be stored effectively. 
   This size is limited by the operating conditions and a parameter defined among the values characterizing 
   the platform. On the 64-bit Linux reference platform, the initial size is fixed at 48 GB. This value 
   is used to size 2 objects used by the memory manager, it will be automatically modified during execution 
   if necessary.  It is possible to modify this value by passing an argument to the command line of
   the executable after the keyword "-max_base size" where size is a real value measured in MB.
   On 32-bit platforms, the initial size is 2.047 GB (2147483647), but the code manages several files to 
   go beyond this limit when the "-max_base" parameter is amended. For the Global base, which can be saved 
   and re-used as data in a calculation, the initial maximum size in :red:`POURSUITE` (:gray:`CONTINUATION`) 
   is kept as it is if the parameter "-max_base" is not used, but can be redefined as needed.

.. _u4.11.01.4.4.1:

Operand :blue:`FICHIER (:gray:`FILE`)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::
  
  ♦ FILE =

Symbolic name of the database considered.

.. _u4.11.01.4.4.2:

Operands :blue:`LONG_ENRE / NMAX_ENRE / LONG_REPE`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Definition of database parameters (direct access files).

::

  / | LONG_ENRE = lenr

:green:`lenr` is the length of the records in Kwords of the direct access files used.

.. admonition:: Remark 

   The JEVEUX memory manager uses this parameter to determine two types of objects: large objects which 
   will be cut into as many records as necessary, and small objects that will be accumulated in a 
   record-sized buffer before being dumped.

::

   | NMAX_ENRE = nenr

:green:`nenr` is the default number of records, this value is determined from :blue:`LONG_ENRE` and an 
operating parameter on the Linux64 reference platform set at 48 GB (51 539 607 552 bytes) for the maximum 
size of the file associated with a database, if this value was not changed by using the ``-max_base`` 
keyword on the command line of the executable.

.. admonition:: Remark 

   The two operands :blue:`LONG_ENRE` and :blue:`NMAX_ENRE` must be used with caution, bad usage could 
   lead to the abrupt stopping of the program by saturation of the direct access files. Consistency between 
   the maximum file size and the value resulting from the product of the two parameters :blue:`LONG_ENRE` 
   and :blue:`NMAX _ENRE` is checked at the start of execution.

::

   | LONG_REPE = lrep

:green:`lrep` is the initial length of the directory (maximum number of objects addressable by JEVEUX), it
is managed dynamically by the memory manager which extends the size of the directory and all the associated 
system objects as and when required.

.. admonition:: Remark 

   The choice by the user to modify these different parameters definitively determines certain 
   characteristics of the GLOBALE base which can no longer be modified in :red:`POURSUITE`.


.. _u4.11.01.4.5:

Keyword :blue:`CODE`
--------------------

::

   CODE =

This keyword is intended only for the command files of the managed non regression tests with the source 
code.

The presence of this keyword automatically activates the debugging mode :blue:`DEBUG(JXVERI = 'OUI',)` 
which implements checks on JEVEUX objects, at an additional cost to the execution. The behavior in the 
event of an error can be changed.

.. _u4.11.01.4.5.1:

Operand :blue:`NIV_PUB_WEB` (:gray:`LEVEL_POST_WEB`)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

  ♦  NIV_PUB_WEB = 'INTRANET'

Post level indicator. Meaning that the test can only be broadcast on the internal network.

::

     NIV_PUB_WEB = 'INTERNET'

Indicates that the test can be broadcast as is on the external network.

.. _u4.11.01.4.6:

Keyword :blue:`EEREUR` (:gray:`ERROR`)
--------------------------------------

Used to modify the behavior of the code in the event of an <F> type error.

.. _u4.11.01.4.6.1:

Operand :blue:`ERREUR_F`
^^^^^^^^^^^^^^^^^^^^^^^^

If an error occurs, the code interrupts the normal execution of the command set.

::

  ERREUR_F = 'EXCEPTION'      (DEFAULT)

By default, an exception is then thrown (for the detailed definition of a Python exception, refer to the 
Python documentation or to that of the supervisor, cf. :ref:`[U1.03.01] <u1.03.01>`). In this case, the
code executes the command :red:`FIN` (cf :ref:`[U4.11.02] <u4.11.02>`) which then closes the database in 
order to make the continuation of the calculation possible. Note that, although the initial error is said 
to be "fatal" (<F>), the diagnostic is ``<S>_ERROR`` since the exception is "caught" by :red:`FIN`. 
This database will then be copied by the study manager. 

::

  ERREUR_F = 'ABORT' 

This indicates that one explicitly asks the code to interrupt the execution of the set of commands in 
the event 
of a fatal error (<F>). The :red:`END` command is not executed, and the database is therefore not closed 
correctly, it is not copied and no recovery is possible.

.. admonition:: Remarks

   For the execution of the benchmarks by the developers, the stop by :blue:`ABORT` is automatic and
   the default behavior. This is activated by the presence of the keyword factor :blue:`CODE` (except if 
   :blue:`ERREUR_F` specifies something else).

   In the event of a lack of CPU time, of memory, for all errors of type <S> and exceptions, the behavior 
   is that described when :blue:`ERREUR_F = 'EXCEPTION'`.


.. _u4.11.01.4.7:

Keyword :blue:`IGNORE_ALARME`
------------------------------

::

  IGNORE_ALARME =

Allows the user to suppress the display of certain alarms (of which the origin is known) in order to
more easily identify other alarms that may appear.

When executing the :red:`FIN` command, a summary table of the alarms emitted during execution (and the 
number of occurrences). Alarms ignored by the user are preceded by '*' to distinguish them (and they appear 
even if they have not been issued).

An example of ignored alarms is:

::

  IGNORE_ALARME = ('MED_2', 'SUPERVIS_40', ...).

.. _u4.11.01.4.8:

Keyword :blue:`DEBUG` 
---------------------

::

   DEBUG =

Debugging option (reserved for developers and code maintenance).

.. _u4.11.01.4.8.1:

Operand :blue:`JXVERI`
^^^^^^^^^^^^^^^^^^^^^^

::

  JXVERI =

Used to check the integrity of memory segments between two consecutive command executions. By default, the 
execution is carried out without :blue:`"DEBUG"`. This option is always activated in the presence of the 
keyword :blue:`CODE`.

.. _u4.11.01.4.8.2:

Operand :blue:`ENVIMA`
^^^^^^^^^^^^^^^^^^^^^^

::

   ENVIMA = 'TEST'

Used to print the values of the parameters defined in the software package in the RESULT file
ENVIMA characterizing the machine :ref:`[D6.01.01] <d6.01.01>`.

.. _u4.11.01.4.8.3:

Operand :blue:`JEVEUX`
^^^^^^^^^^^^^^^^^^^^^^

::

  ◊ JEVUX =

Allows to activate the operating mode in debug of the JEVEUX memory manager: undelayed disk unloads and 
assignment of value segments to an undefined value :red:`[D6.02.01] <d6.02.01>`.

.. _u4.11.01.4.8.4:

Operand :blue:`SDVERI`
^^^^^^^^^^^^^^^^^^^^^^

::

  SDVERI = 'NON'

The use of this keyword is intended for developers. Please note that this functionality causes a 
significant additional cost during execution. 

This keyword triggers the checking of the data structures 
produced by the operators. It is used within the framework of the procedures of development of the code 
in the tests of non regression. If the keyword :blue:`CODE` is present, this keyword takes the default 
value :blue:`'OUI'`.

.. _u4.11.01.4.8.5:

Operand :blue:`HIST_ETAPE`
^^^^^^^^^^^^^^^^^^^^^^^^^^

::

   HIST_ETAPE = 'NON'

This keyword is used to keep all the history of the steps / commands used. This is greedy in memory and 
should only be used for very specific cases (the commands which require it are indicated in the 
documentation). By default, this history is not stored.

.. _u4.11.01.4.9:

Keyword :blue:`MESURE_TEMPS` (:gray:`MEASURE_TIME`)
----------------------------------------------------

The keyword :blue:`MESURE_TEMPS` makes it possible to choose the level of detail of the output of CPU time
which will be displayed in the message file by the commands carrying out elementary calculations,
solutions of linear systems, unloading objects to disk or MPI communications.

.. _u4.11.01.4.9.1:

Operand :blue:`NIVE_DETAIL` (:gray:`LEVEL_OF_DETAIL`)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

By default, at the end of each command, we will print a line such as:

::

  # 1.Solution.of.linear.systems               CPU. (USER + SYST / SYST / ELAPS): 7.52 0.79 11.22
  # 2.Elementary.calculations.and.assemblies.  CPU. (USER + SYST / SYST / ELAPS): 15.07 0.70 15.77

::

  ◊ NIVE_DETAIL = 0   # no printing.
                = 1   # default printing
                = 2   # more detailed printing:

::

  # 1.Solution.of.linear.systems               CPU (USER + SYST / SYST / ELAPS): 7.72 0.82 8.72
  # 1.1.Numbering,.connectivity.of.the.matrix  CPU (USER + SYST / SYST / ELAPS): 0.21 0.02 0.31
  # 1.2.Factoring.symbolic                     CPU (USER + SYST / SYST / ELAPS): 0.58 0.05 1.28
  # 1.3.Numeric.Factoring. (Or.precond.)       CPU (USER + SYST / SYST / ELAPS): 6.78 0.73 7.71
  # 1.4.Solution                               CPU (USER + SYST / SYST / ELAPS): 0.15 0.02 0.35
  # 2.Elementary.calculations.and.assemblies   CPU (USER + SYST / SYST / ELAPS): 28.87 0.64 29.47
  # 2.1.Routine.calculation                    CPU (USER + SYST / SYST / ELAPS): 26.61 0.56 26.61
  # 2.1.1.Routines.te00ij                      CPU (USER + SYST / SYST / ELAPS): 24.58 0.07 25.78
  # 2.2.Assemblies                             CPU (USER + SYST / SYST / ELAPS): 2.26 0.08 3.36
  # 2.2.1.Assembly.mrices                      CPU (USER + SYST / SYST / ELAPS): 2.02 0.06 3.12
  # 2.2.2.Assemblage.second.members            CPU (USER + SYST / SYST / ELAPS): 0.24 0.02 0.37

::

  ◊ NIVE_DETAIL = = 3   # more detailed prints and incremental print for each time step.

During parallel calculations (MPI), the time spent in communications is also displayed:

::

  # 4 MPI communications                       CPU (USER + SYST / SYST / ELAPS): 12.67 0.50 12.68

.. _u4.11.01.4.9.2:

Operand :blue:`MOYENNE` (:gray:`MEAN`)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

  ◊ MOYENNE = 'NON'    # no display of statistics
            = 'OUI'    # display of statistics

The :blue:`MOYENNE` keyword is used to control the display of additional statistics exclusively for 
parallel calculations. This is the average of the measurements on all processors as well as the standard 
deviation of these measurements. Each displayed time is then completed as follows:

::

  # 1 Solution.linear.systems.               CPU (USER + SYST / SYST / ELAPS): 0.29 0.00 0.35
      (average .... diff..procs)             CPU (USER + SYST / SYST / ELAPS): 0.30 0.00 0.47
      (standard deviation.diff..procs)       CPU (USER + SYST / SYST / ELAPS): 0.01 0.00 0.05

.. _u4.11.01.4.10:

Keyword :blue:`MEMOIRE` (:gray:`MEMORY`) 
----------------------------------------

The allocation of the different data structures is a dynamic allocation, the user indicates the resource 
limits when launching the executable in the access interface.

.. _u4.11.01.4.10.1:

Operand :blue:`TAILLE_GROUP_ELEM` (:gray:`ELEM_GROUP_SIZE`)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

  TAILLE_GROUP_ELEM = tgrel [default: 1000]

This parameter gives the maximum number of finite elements of the same type which will be grouped in
a group of items. This parameter influences the memory and CPU performances of elementary computations and
assemblies.

When increasing :green:`tgrel`, we generally have to save CPU time. On the other hand, JEVEUX objects
are bigger, which can require more memory.

.. _u4.11.01.4.10.2:

Operand :blue:`TAILLE_BLOC` (:gray:`BLOCK_SIZE`)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

  TAILLE_BLOC = tbloc [default: 800]

This parameter gives the size of the blocks of the factorized matrices for the LDLT solver. This size 
is given in kiloR8 (1 kiloR8 = 1024 reals). This parameter influences the number of input / output 
operations and therefore on assembly and solution time. 

By default this value is fixed at 800 kiloR8, i.e. 8 records by default on the direct access file 
associated with the JEVEUX database.

.. _u4.11.01.4.11:

Keyword :blue:`CATALOGUE`
-------------------------

This keyword is reserved for developers, it is used during the compilation operation of catalogs of 
elements to obtain the file in JEVEUX database format.

.. _u4.11.01.4.11.1:

Operand :blue:`FICHIER` (:gray:`FILE`)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

  ♦ FICHIER = nfic

Can only take the value :green:`'CATAELEM'`.

.. _u4.11.01.4.11.2:

Operand :blue:`UNITE`
^^^^^^^^^^^^^^^^^^^^^

::

  ◊ UNITE = unit

Logical unit number associated with the element catalogs. In the construction procedures of the catalog 
of elements one uses a value such as 4. The file ``fort.4`` is obtained starting from the contents of the
catalog source directory using a Python procedure.

.. _u4.11.01.4.12:

Keyword :blue:`RESERVE_CPU`
---------------------------

Used to reserve part of the CPU time allocated to the job to properly end the execution in case of stop 
by lack of CPU time detected by an ``Aster`` command. This mechanism is not useful in the case of a batch 
execution of ``Code_Aster``. The value of this reserve can be indicated as an absolute value or as a 
percentage of the total CPU time. This value is bounded by the value of the keyword :blue:`BORNE`
(:gray:`LIMIT`).

When the keyword :blue:`CODE` is present, i.e. for all the tests of non regression, one systematically 
imposes a CPU time reserve of 10 seconds if the keyword :blue:`RESERVE_CPU` is absent.

After the execution of ``Code_Aster``, it may be necessary to carry out compression operations before 
transferring the results files or the Global database which are sometimes very expensive.

.. _u4.11.01.4.12.1:

Operand :blue:`VALE` (:gray:`VALUE`)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Value expressed in seconds subtracted from the total CPU time, which is the basis for properly stopping 
execution for some global commands.

.. _u4.11.01.4.12.2:

Operand :blue:`POURCENTAGE` (:gray:`PERCENTAGE`)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Percentage subtracted from total CPU time, which is used by some global commands for properly stopping
execution.

.. _u4.11.01.4.12.3:

Operand :blue:`BORNE` (:gray:`LIMIT`)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Maximum value of the time reserved, defaulting 900 seconds.

