.. role:: red
.. role:: blue
.. role:: green
.. role:: gray
.. role:: python(code)
   :language: python
.. |br| raw:: html

   <br/>

.. _reading_the_manuals:

*********************************************************
**[U0.00.01]**: Navigating the ``Code_Aster`` PDF manuals
*********************************************************

PDF versions of the ``Code_Aster`` manuals can be found at the ``Code_Aster`` `website <https://www.code-aster.org/V2/spip.php?rubrique19/>`_.

.. important::
    The location of the PDF manuals may change over time as new versions of ``Code_Aster`` are released.  The original documentation is Copyright 2019 EDF R&D - Licensed under the terms of the GNU FDL (http://www.gnu.org/copyleft/fdl.html)

.. _reading_guide:

Organization
============

The ``Code_Aster`` documentation is divided into five sets of guides.  Separate documents are also
available that complement these guides.

====================   ============ ==
Name                    Symbol       Contents
====================   ============ ==
User guides             U            User commands, user data structures, and examples
Reference guides        R            Theory and analysis methods/algorithms
Data guides             D            Data structures, algorithms, architecture, environment
Validation guides       V            Elementary verification tests
Administration guide    A            Quality plan, development and maintenance procedures, version control
====================   ============ ==

The **User**, **Reference**, and **Validation** guides are the most useful for the average user.
In particular, the **User** guides frequently refer to the **Reference** guides.  The **Data** and **Administration**
guides are largely for developers.  However, the **Administration** guide contains information about
various versions of the software, the evolution of the code, corrected bugs, and approaches to avoid
existing bugs.

.. _guide_numbering:

Document numbering
==================

The guides are subdivided into ten parts (numbered from 0 up to  9), the parts contain booklets (00 up to 99), 
and the booklets contain individual documents (00 up to 99).

The document numbering scheme
-----------------------------

A document can be discovered using a key::

   Guide_symbol_and_part_number.Booklet_number.Document_number

An example of a document key is U1.02.00

Usually one identifies a document by its key. For reasons of convenience, two classes of documents are 
also indicated by the function that it documents: 

* guides on user commands that are indicated by name of the command documented,
* guides on validation test cases, indicated by the test case code.

Examples 

* :ref:`U4.43.01 <u4.43.01>`:  user guide for the command :red:`DEFI_MATERIAU`, the document will also be referred to as :red:`DEFI_MATERIAU`. 
* :ref:`V7.90.04 <v7.90.04>`:  validation guide for test HNSV100: Thermoplasticity in simple traction, the document will be also referred to as HNSV100.

The **User** guides are numbered as follows:

==============================   =================
Purpose                          Guide part number
==============================   =================
Accessing and running the code   :ref:`u1_toc`
Using the models                 :ref:`u2_toc`
Features of the models           :ref:`u3_toc`
Basic commands                   :ref:`u4_toc`
Data exchange commands           :ref:`u7_toc`
==============================   =================

Which version of the code applies to a document?
------------------------------------------------

The PDF version of each guide document contains bibliometric information in page headings such as
the title of the document, the name of the person in charge, the key, the summary, and the
version of the code. Among those, three deserve special attention because they are related to Quality
Assurance and with the update of the documents: 

* the version of the code,
* the revision number of the document,
* the publication date of the revision.

An indicator in top right-hand side of each page specifies the version of ``Code_Aster`` to which the document 
applies. This indicator can take two types of values:

* **Default** Version : if  the document has been validated and qualified
* **Development** Version : if  the document is in the current cycle of development

These versions evolve every 2 years.
 
.. _first_use:

Using ``Code_Aster``
====================

.. note::
   To access ``Code_Aster`` with a graphical user interface and ``astk``, please consult the document 
   :ref:`u1.04.00`.

The simplest way to carry out a calculation with Code_Aster is to start from a similar example 
from the **Validation** guides.  The command files associated with validation test cases can be found in the
``astest`` directory in your ``Code_Aster`` installation, typically under ``/aster/v<x>/STA<x>/astest`` 
where ``<x>`` is the version number.

However, for more general problems a sequence of documents should be consulted as discussed in the example below.

Basic principles of ``Code_Aster``
----------------------------------

  Consult :ref:`u1.03.00` which summarizes the principles of operation.

Simple example usage of ``Code_Aster``
--------------------------------------

  Consult :ref:`u1.05.00` on the calculation of displacements and stresses in 
  an axisymmetric cylinder under hydrostatic pressure.

Mesh
----

  Consult the document :ref:`u3.01.00` which describes the structure of the mesh file 
  used by ``Code_Aster``.

  If the initial mesh has been generated by an external mesher such as GMSH (or GIBI or I-DEAS), please consult

  ============== ==================================================
  **[U3.02.01]** :ref:`u3.02.01`
  **[U3.03.01]** Interface of the mesh file from I-DEAS with Aster 
  **[U3.04.01]** Interface of the mesh file from GIBI with Aster 
  **[U7.01.01]** Procedure PRE_IDEAS 
  **[U7.01.11]** Procedure PRE_GIBI 
  **[U7.01.31]** :ref:`u7.01.31` 
  ============== ==================================================

  The recommended mesh format during the use of ``Code_Aster`` is the MED (Model of Data exchange) format. This is the default format during the reading of meshes and the writing of results.

Commands
--------

  The description of the commands of ``Code_Aster`` are contained in the parts **U4** and **U7** of the **User** guides.  These documents are organized in the **U4** guide according to the principal stages of a calculation:

  ========================================== =========================================================
  **U4.1-**                                  Allocation of resources disc and memory
  **U4.2-, U7.01.- to U7.03.-**              Acquisition of the mesh data
  **U4.3- and U4.4-**                        Modeling (assignment of elements, materials, loads, etc.)
  **U4.5-**                                  Solution of the system of equations (calculation)
  **U4.6-, U4.7-, U4.8-, U7.03 and U7.05.-** Post-processing and examination of the results
  ========================================== =========================================================

  The document :ref:`u4.01.00`, explains in the significance of the  characters and typography used in the documentation of command syntax.

Usage notes
-----------

  Some models or types of model (such as static structures, mechanical cushioning, thin hulls, etc) are the object of usage notes. The related documents can be found in the **U2** part of **User** guides.
  You can begin from the catalog of usage documents: :ref:`u2.00.01`.

Finite elements and modeling
----------------------------

  The mathematical description of the finite element models supported by ``Code_Aster`` can be found in
  the **Reference** guides.  The description of the degrees of freedom of these elements and supported loads, 
  produced fields, non-linearities, etc.) are in the documents: 

  ========= ======================
  **U3.1-** Mechanical models
  **U3.2-** Thermal models
  **U3.3-** Acoustic models
  ========= ======================

``Result`` data
---------------

  The ``Aster`` commands create data objects whose structures are essential for users of the code.

  A generic document that should be consulted first is :

  :ref:`u4.91.01`  

  This document provides
  the detailed syntax needed to write results and meshes to an output file.

  Descriptions of the commands which generate result files in  MED, GMSH, and I-DEAS formats 
  can be found in:

  ============== ====================================================
  **[U7.05.01]** Procedure IMPR_RESU (FORMAT=' IDEAS'),
  **[U7.05.21]** Procedure IMPR_RESU (FORMAT=' MED'),
  **[U7.05.32]** Procedure IMPR_RESU (FORMAT=' GMSH'),
  ============== ====================================================

Creating a command file
-----------------------
  The generation of a command file is a required step in the calculation process. Currently, the user has the
  choice between creating this file by hand or building it using the graphical interface ``EFICAS`` (*Editor of Command file Aster*).  The graphical interface gives access directly to the PDF documentation.

Errors in the command file
--------------------------
  If the command file is built by hand if ``Aster`` detects a syntactical, grammatical, or semantic error 
  in this file,  a brief description of possible reasons for the errors are in :ref:`u1.03.01`.

.. _faq_answers:

Frequently asked questions
==========================

Do I have the most up-to-date version of the document?
------------------------------------------------------

If the document is consulted electronically on the ``Code-Aster`` website, the answer is **YES**.

Which is the documentation which applies to the current version of the code?
----------------------------------------------------------------------------

The documents contained in the website are current even if those carry a code version number
lower than the current version and carry old dates. For each guide, the list of these documents
(documents valid for the current version) appears under the heading **Synopsis**.
