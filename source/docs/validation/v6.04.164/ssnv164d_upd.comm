# SSNV164D

DEBUT(LANG = 'EN')

# Read mesh
mesh_ini = LIRE_MAILLAGE(FORMAT = 'MED')

# Convert cables 3 and 4 from linear to quad for sheath elements
mesh = CREA_MAILLAGE(MAILLAGE = mesh_ini,
                     LINE_QUAD = _F(GROUP_MA = ('cable3', 'cable4')))

# Change outward normal orientation on surfaces
mesh = MODI_MAILLAGE(reuse = mesh,
                     MAILLAGE = mesh,
                     ORIE_LIGNE = (_F(GROUP_MA = 'cable1'),
                                   _F(GROUP_MA = 'cable2'),
                                   _F(GROUP_MA = 'cable3'),
                                   _F(GROUP_MA = 'cable4'),
                                   _F(GROUP_MA = 'cable0')))

# Define node groups from element groups
mesh = DEFI_GROUP(reuse = mesh,
                  MAILLAGE = mesh,
                  CREA_GROUP_NO = (_F(GROUP_MA = 'bot_face'),
                                   _F(GROUP_MA = 'cable1'),
                                   _F(GROUP_MA = 'cable2'),
                                   _F(GROUP_MA = 'cable3'),
                                   _F(GROUP_MA = 'cable4'),
                                   _F(GROUP_MA = 'cable0')))

# Assign finite elements
model = AFFE_MODELE(MAILLAGE = mesh,
                    AFFE = (_F(GROUP_MA = 'vol_all',
                               PHENOMENE = 'MECANIQUE',
                               MODELISATION = '3D'),
                            _F(GROUP_MA = ('cable1', 'cable2', 'cable0'),
                               PHENOMENE = 'MECANIQUE',
                               MODELISATION = 'BARRE'),
                            _F(GROUP_MA = ('cable3', 'cable4'),
                               PHENOMENE = 'MECANIQUE',
                               MODELISATION = 'CABLE_GAINE')))

# Assign cable sections
section = AFFE_CARA_ELEM(MODELE = model,
                         BARRE = _F(GROUP_MA = ('cable1','cable2','cable3','cable4','cable0'),
                                    SECTION = 'CERCLE',
                                    CARA = 'R',
                                    VALE = 2.8209E-2))

# Define materials
concrete = DEFI_MATERIAU(ELAS = _F(E = 4.0E10,
                                   NU = 0.20,
                                   RHO = 2500.0),
                         BPEL_BETON = _F())

steel = DEFI_MATERIAU(ELAS = _F(E = 1.93E11,
                                NU = 0.3,
                                RHO = 7850.0),
                      BPEL_ACIER = _F(F_PRG = 1.94E9,
                                      FROT_COURB = 0.0,
                                      FROT_LINE = 1.5E-3),
                      ECRO_LINE = _F(SY = 1.94E11,
                                     D_SIGM_EPSI = 1000.0))

sheath = DEFI_MATERIAU(ELAS = _F(E = 1.93E11,
                                 NU = 0.3,
                                 RHO = 7850.0),
                       BPEL_ACIER = _F(F_PRG = 1.94E9,
                                       FROT_COURB = 0.0,
                                       FROT_LINE = 1.5E-3),
                       ECRO_LINE = _F(SY = 1.94E11,
                                      D_SIGM_EPSI = 1000.0),
                       CABLE_GAINE_FROT = _F(TYPE = 'FROTTANT', 
                                             PENA_LAGR = 1.E7,
                                             FROT_LINE = 1.5E-3,
                                             FROT_COURB = 0.0))

# Assign materials
mater = AFFE_MATERIAU(MAILLAGE = mesh,
                      AFFE = (_F(GROUP_MA = 'vol_all',
                                 MATER = concrete),
                              _F(GROUP_MA = ('cable1', 'cable2', 'cable0'),
                                 MATER = steel),
                              _F(GROUP_MA = ('cable3', 'cable4'),
                                 MATER = sheath)))

# Define prestress in cables
pre_sig1 = DEFI_CABLE_BP(MODELE = model,
                         CHAM_MATER = mater,
                         CARA_ELEM = section,
                         GROUP_MA_BETON = 'vol_all',
                         DEFI_CABLE = _F(GROUP_MA = 'cable1',
                                         GROUP_NO_ANCRAGE = ('pbot1','ptop1')),
                         TYPE_ANCRAGE = ('ACTIF','PASSIF'),
                         TENSION_INIT = 3.75E6,
                         RECUL_ANCRAGE = 0.001)

pre_sig2 = DEFI_CABLE_BP(MODELE = model,
                         CHAM_MATER = mater,
                         CARA_ELEM = section,
                         GROUP_MA_BETON = 'vol_all',
                         DEFI_CABLE = _F(GROUP_MA = 'cable2',
                                         GROUP_NO_ANCRAGE = ('pbot2','ptop2')),
                         TYPE_ANCRAGE = ('ACTIF','PASSIF'),
                         TENSION_INIT = 3.75E6,
                         RECUL_ANCRAGE = 0.001)

pre_sig3 = DEFI_CABLE_BP(MODELE = model,
                         CHAM_MATER = mater,
                         CARA_ELEM = section,
                         ADHERENT = 'NON',
                         GROUP_MA_BETON = 'vol_all',
                         DEFI_CABLE = _F(GROUP_MA = 'cable3',
                                         GROUP_NO_ANCRAGE = ('pbot3','ptop3')),
                         TYPE_ANCRAGE = ('ACTIF','PASSIF'),
                         TENSION_INIT = 3.75E6,
                         RECUL_ANCRAGE = 0.001)

pre_sig4 = DEFI_CABLE_BP(MODELE = model,
                         CHAM_MATER = mater,
                         CARA_ELEM = section,
                         ADHERENT = 'NON',
                         GROUP_MA_BETON = 'vol_all',
                         DEFI_CABLE = _F(GROUP_MA = 'cable4',
                                       GROUP_NO_ANCRAGE = ('pbot4','ptop4')),
                         TYPE_ANCRAGE = ('ACTIF','PASSIF'),
                         TENSION_INIT = 3.75E6,
                         RECUL_ANCRAGE = 0.001)

pre_sig0 = DEFI_CABLE_BP(MODELE = model,
                         CHAM_MATER = mater,
                         CARA_ELEM = section,
                         GROUP_MA_BETON = 'vol_all',
                         DEFI_CABLE = _F(GROUP_MA = 'cable0',
                                       GROUP_NO_ANCRAGE = ('pbot0','ptop0')),
                         TYPE_ANCRAGE = ('ACTIF','ACTIF'),
                         TENSION_INIT = 3.750000E6,
                         RECUL_ANCRAGE = 0.001,
                         INFO = 2)

# Assign displacement bcs and gravity
bc_u  = AFFE_CHAR_MECA(MODELE = model,
                       DDL_IMPO = (_F(GROUP_NO = 'center_nodes',
                                      DX = 0.0, DY = 0.0),
                                   _F(GROUP_NO = 'px',
                                      DY = 0.0),
                                   _F(GROUP_NO = 'py',
                                      DX = 0.0),
                                   _F(GROUP_NO = 'bot_face',
                                      DZ = 0.0)),
                       PESANTEUR = _F(GRAVITE = 9.8100000000000005,
                                      DIRECTION = (0.0, 0.0, -1.0)))

bc_s  = AFFE_CHAR_MECA(MODELE = model,
                       DDL_IMPO = (_F(GROUP_NO = ('pbot3', 'ptop3', 'pbot4', 'ptop4'),
                                      GLIS = 0.0)))

# Assign sheathed cable BCs
# Assign prestress to cables
bc_f_1 = AFFE_CHAR_MECA(MODELE = model,
                        RELA_CINE_BP = _F(CABLE_BP = pre_sig1,
                                          SIGM_BPEL = 'NON',
                                          RELA_CINE = 'OUI'))

bc_f_2 = AFFE_CHAR_MECA(MODELE = model,
                        RELA_CINE_BP = _F(CABLE_BP = pre_sig2,
                                          SIGM_BPEL = 'NON',
                                          RELA_CINE = 'OUI'))

bc_f_3 = AFFE_CHAR_MECA(MODELE = model,
                        RELA_CINE_BP = _F(CABLE_BP = pre_sig3,
                                          SIGM_BPEL = 'NON',
                                          RELA_CINE = 'OUI'))

bc_f_4 = AFFE_CHAR_MECA(MODELE = model,
                        RELA_CINE_BP = _F(CABLE_BP = pre_sig4,
                                          SIGM_BPEL = 'NON',
                                          RELA_CINE = 'OUI'))

bc_f_0 = AFFE_CHAR_MECA(MODELE = model,
                        RELA_CINE_BP = _F(CABLE_BP = pre_sig0,
                                          SIGM_BPEL = 'NON',
                                          RELA_CINE = 'OUI'))

# Set up timesteps
l_times = DEFI_LIST_REEL(VALE = (0.0, 150.0, 300.0, 450.0, 600.0))

# Step 1: Apply gravity
#--------------------------------------------------------
result1 = STAT_NON_LINE(MODELE = model,
                     CHAM_MATER = mater,
                     CARA_ELEM = section,
                     COMPORTEMENT = (_F(RELATION = 'ELAS'),
                                     _F(RELATION = 'SANS',
                                        GROUP_MA =  ('cable1', 'cable2', 'cable0')),
                                     _F(RELATION = 'KIT_CG',
                                        RELATION_KIT = ('SANS', 'CABLE_GAINE_FROT'),
                                        GROUP_MA =  ('cable3', 'cable4'))),
                     EXCIT = (_F(CHARGE = bc_u),
                              _F(CHARGE = bc_s),
                              _F(CHARGE = bc_f_1),
                              _F(CHARGE = bc_f_2),
                              _F(CHARGE = bc_f_3),
                              _F(CHARGE = bc_f_4),
                              _F(CHARGE = bc_f_0)),
                     INCREMENT = _F(LIST_INST = l_times,
                                    INST_FIN = 150.0),
                     SOLVEUR = _F(METHODE = 'MUMPS'))
                                  
# Step 2: Prestress cables 1 and 2
#--------------------------------------------------------
result1 =  CALC_PRECONT(reuse = result1,
                        ETAT_INIT = _F(EVOL_NOLI = result1),
                        MODELE = model,
                        CHAM_MATER = mater,
                        CARA_ELEM = section,
                        COMPORTEMENT = (_F(RELATION = 'ELAS',
                                           GROUP_MA = 'vol_all'),
                                        _F(RELATION = 'VMIS_ISOT_LINE',
                                           GROUP_MA =  ('cable1', 'cable2', 'cable0')),
                                        _F(RELATION = 'KIT_CG',
                                           RELATION_KIT = ('VMIS_ISOT_LINE', 'CABLE_GAINE_FROT'),
                                           GROUP_MA =  ('cable3', 'cable4'))),
                        EXCIT = (_F(CHARGE = bc_u),
                                 _F(CHARGE = bc_s,
                                    TYPE_CHARGE = 'DIDI')),
                        CABLE_BP = (pre_sig1, pre_sig2),
                        CABLE_BP_INACTIF = (pre_sig3, pre_sig4, pre_sig0),
                        INCREMENT = _F(LIST_INST = l_times,
                                       INST_FIN  = 300.0),
                        NEWTON = _F(REAC_ITER = 1),
                        SOLVEUR = _F(METHODE = 'MUMPS'))

# Step 3: Prestress cables 3 and 4
#--------------------------------------------------------
result1 = CALC_PRECONT(reuse = result1,
                       ETAT_INIT = _F(EVOL_NOLI = result1),
                       MODELE = model,
                       CHAM_MATER = mater,
                       CARA_ELEM = section,
                       COMPORTEMENT = (_F(RELATION = 'ELAS',
                                          GROUP_MA = 'vol_all'),
                                       _F(RELATION = 'VMIS_ISOT_LINE',
                                          GROUP_MA =  ('cable1', 'cable2', 'cable0')),
                                       _F(RELATION = 'KIT_CG',
                                          RELATION_KIT = ('VMIS_ISOT_LINE', 'CABLE_GAINE_FROT'),
                                          GROUP_MA =  ('cable3', 'cable4'))),
                       EXCIT = (_F(CHARGE = bc_u),
                                _F(CHARGE = bc_f_1),
                                _F(CHARGE = bc_f_2)),
                       CABLE_BP = ( pre_sig3 , pre_sig4 ),
                       CABLE_BP_INACTIF = ( pre_sig0 ),
                       INCREMENT = _F(LIST_INST = l_times,
                                      INST_FIN  = 450.0),
                       NEWTON = _F(REAC_ITER = 1),
                       SOLVEUR = _F(METHODE = 'MUMPS'),
                       CONVERGENCE = _F(ITER_GLOB_MAXI = 20))

# Step 4: Prestress cable 0
#-----------------------------------------------------------
result1 = CALC_PRECONT(reuse = result1,
                       ETAT_INIT = _F(EVOL_NOLI = result1),
                       MODELE = model,
                       CHAM_MATER = mater,
                       CARA_ELEM = section,
                       COMPORTEMENT = (_F(RELATION = 'ELAS',
                                          GROUP_MA = 'vol_all'),
                                       _F(RELATION = 'VMIS_ISOT_LINE',
                                          GROUP_MA =  ('cable1', 'cable2', 'cable0')),
                                       _F(RELATION = 'KIT_CG',
                                          RELATION_KIT = ('VMIS_ISOT_LINE', 'CABLE_GAINE_FROT'),
                                          GROUP_MA =  ('cable3', 'cable4'))),
                       EXCIT = (_F(CHARGE = bc_u),
                                _F(CHARGE = bc_s,
                                   TYPE_CHARGE = 'DIDI'),
                                _F(CHARGE = bc_f_1),
                                _F(CHARGE = bc_f_2),
                                _F(CHARGE = bc_f_3),
                                _F(CHARGE = bc_f_4)),
                       CABLE_BP = ( pre_sig0 ),
                       INCREMENT = _F(LIST_INST = l_times,
                                      INST_FIN = 600.0),
                       NEWTON = _F(REAC_ITER = 1))

# Compute stresses
result1 = CALC_CHAMP(reuse = result1,
                     CRITERES = ('SIEQ_ELNO'),
                     CONTRAINTE = ('SIEF_ELNO'),
                     RESULTAT = result1)

# Write output
IMPR_RESU(FORMAT = 'MED',
          UNITE = 80,
          RESU = _F(RESULTAT = result1,
                    INST = (300, 450, 600),
                    NOM_CHAM = ('DEPL', 'SIEF_NOEU', 'SIEF_ELNO', 'SIEQ_ELNO'), 
                    GROUP_MA = 'vol_all'))

IMPR_RESU(FORMAT = 'MED',
          UNITE = 81,
          RESU = _F(RESULTAT = result1,
                    INST = (300, 450, 600),
                    NOM_CHAM = ('DEPL', 'SIEF_ELNO'), 
                    GROUP_MA = ('cable1', 'cable2', 'cable0')))

IMPR_RESU(FORMAT = 'MED',
          UNITE = 82,
          RESU = _F(RESULTAT = result1,
                    INST = (300, 450, 600),
                    NOM_CHAM = ('DEPL', 'SIEF_ELNO'), 
                    GROUP_MA = ('cable3', 'cable4')))

#----------------------------------------------
# Verification

#  cable 1 - t = 300s
TEST_RESU(RESU = (_F(INST = 300.0,
                     GROUP_NO = 'N1', GROUP_MA = 'M5655',
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result1, NOM_CHAM = 'SIEF_ELNO', NOM_CMP = 'N',
                     VALE_CALC =  3.64939314E+06, VALE_REFE = 3.648000E6,
                     CRITERE = 'RELATIF', PRECISION = 1.E-3),
                  _F(INST = 300.0,
                     GROUP_NO = 'N6', GROUP_MA = 'M5660',
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result1, NOM_CHAM = 'SIEF_ELNO', NOM_CMP = 'N',
                     VALE_CALC =  3.67686649E+06, VALE_REFE = 3.675000E6,
                     CRITERE = 'RELATIF', PRECISION = 1.E-3),
                  _F(INST = 300.0,
                     GROUP_NO = 'N11', GROUP_MA = 'M5664',
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result1, NOM_CHAM = 'SIEF_ELNO', NOM_CMP = 'N',
                     VALE_CALC =  3.69519477E+06, VALE_REFE = 3.693000E6,
                     CRITERE = 'RELATIF', PRECISION = 1.E-3),
                  _F(INST = 300.0,
                     GROUP_NO = 'N16', GROUP_MA = 'M5670',
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result1, NOM_CHAM = 'SIEF_ELNO', NOM_CMP = 'N',
                     VALE_CALC =  3.66381927E+06, VALE_REFE = 3.667000E6,
                     CRITERE = 'RELATIF', PRECISION = 1.E-3),
                  _F(INST = 300.0,
                     GROUP_NO = 'N101', GROUP_MA = 'M5674',
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result1, NOM_CHAM = 'SIEF_ELNO', NOM_CMP = 'N',
                     VALE_CALC =  3.64190218E+06, VALE_REFE = 3.640000E6,
                     CRITERE = 'RELATIF', PRECISION = 1.E-3)))

#  cable 1 - t = 450s
TEST_RESU(RESU = (_F(INST = 450.0,
                     GROUP_NO = 'N1', GROUP_MA = 'M5655',
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result1, NOM_CHAM = 'SIEF_ELNO', NOM_CMP = 'N',
                     VALE_CALC =  3564405.35782, VALE_REFE = 3.561000E6,
                     CRITERE = 'RELATIF', PRECISION = 1.E-2),
                  _F(INST = 450.0,
                     GROUP_NO = 'N6', GROUP_MA = 'M5660',
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result1, NOM_CHAM = 'SIEF_ELNO', NOM_CMP = 'N',
                     VALE_CALC =  3591240.2716, VALE_REFE = 3.588000E6,
                     CRITERE = 'RELATIF', PRECISION = 1.E-2),
                  _F(INST = 450.0,
                     GROUP_NO = 'N11', GROUP_MA = 'M5664',
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result1, NOM_CHAM = 'SIEF_ELNO', NOM_CMP = 'N',
                     VALE_CALC =  3614025.02896, VALE_REFE = 3.628000E6,
                     CRITERE = 'RELATIF', PRECISION = 1.E-2),
                  _F(INST = 450.0,
                     GROUP_NO = 'N16', GROUP_MA = 'M5670',
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result1, NOM_CHAM = 'SIEF_ELNO', NOM_CMP = 'N',
                     VALE_CALC =  3641854.2281, VALE_REFE = 3.645000E6,
                     CRITERE = 'RELATIF', PRECISION = 1.E-2),
                  _F(INST = 450.0,
                     GROUP_NO = 'N101', GROUP_MA = 'M5674',
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result1, NOM_CHAM = 'SIEF_ELNO', NOM_CMP = 'N',
                     VALE_CALC =  3622546.1048, VALE_REFE = 3.629000E6,
                     CRITERE = 'RELATIF', PRECISION = 1.E-2)))

#  cable 1 - t = 600s
TEST_RESU(RESU = (_F(INST = 600.0,
                     GROUP_NO = 'N1', GROUP_MA = 'M5655',
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result1, NOM_CHAM = 'SIEF_ELNO', NOM_CMP = 'N',
                     VALE_CALC =  3521416.6901, VALE_REFE = 3.519000E6,
                     CRITERE = 'RELATIF', PRECISION = 1.E-2),
                  _F(INST = 600.0,
                     GROUP_NO = 'N6', GROUP_MA = 'M5660',
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result1, NOM_CHAM = 'SIEF_ELNO', NOM_CMP = 'N',
                     VALE_CALC =  3547917.09904, VALE_REFE = 3.546000E6,
                     CRITERE = 'RELATIF', PRECISION = 1.E-2),
                  _F(INST = 600.0,
                     GROUP_NO = 'N11', GROUP_MA = 'M5664',
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result1, NOM_CHAM = 'SIEF_ELNO', NOM_CMP = 'N',
                     VALE_CALC =  3573020.25026, VALE_REFE = 3.597000E6,
                     CRITERE = 'RELATIF', PRECISION = 1.E-2),
                  _F(INST = 600.0,
                     GROUP_NO = 'N16', GROUP_MA = 'M5670',
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result1, NOM_CHAM = 'SIEF_ELNO', NOM_CMP = 'N',
                     VALE_CALC =  3630837.40231, VALE_REFE = 3.635000E6,
                     CRITERE = 'RELATIF', PRECISION = 1.E-2),
                  _F(INST = 600.0,
                     GROUP_NO = 'N101', GROUP_MA = 'M5674',
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result1, NOM_CHAM = 'SIEF_ELNO', NOM_CMP = 'N',
                     VALE_CALC =  3604564.13487, VALE_REFE = 3.614000E6,
                     CRITERE = 'RELATIF', PRECISION = 1.E-2)))

#  cable 3 - t = 450s
TEST_RESU(RESU = (_F(INST = 450.0,
                     GROUP_NO = 'N41', GROUP_MA = 'M5695',
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result1, NOM_CHAM = 'SIEF_ELNO', NOM_CMP = 'N',
                     VALE_CALC =  3647660.37291, VALE_REFE = 3.647000E6,
                     CRITERE = 'RELATIF', PRECISION = 1.E-3),
                  _F(INST = 450.0,
                     GROUP_NO = 'N46', GROUP_MA = 'M5700',
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result1, NOM_CHAM = 'SIEF_ELNO', NOM_CMP = 'N',
                     VALE_CALC =  3675120.67352, VALE_REFE = 3.675000E6,
                     CRITERE = 'RELATIF', PRECISION = 1.E-3),
                  _F(INST = 450.0,
                     GROUP_NO = 'N51', GROUP_MA = 'M5705',
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result1, NOM_CHAM = 'SIEF_ELNO', NOM_CMP = 'N',
                     VALE_CALC =  3694342.18234, VALE_REFE = 3.695000E6,
                     CRITERE = 'RELATIF', PRECISION = 1.E-3),
                  _F(INST = 450.0,
                     GROUP_NO = 'N56', GROUP_MA = 'M5710',
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result1, NOM_CHAM = 'SIEF_ELNO', NOM_CMP = 'N',
                     VALE_CALC =  3666738.25966, VALE_REFE = 3.667000E6,
                     CRITERE = 'RELATIF', PRECISION = 1.E-3),
                  _F(INST = 450.0,
                     GROUP_NO = 'N103', GROUP_MA = 'M5714',
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result1, NOM_CHAM = 'SIEF_ELNO', NOM_CMP = 'N',
                     VALE_CALC =  3639340.59259, VALE_REFE = 3.640000E6,
                     CRITERE = 'RELATIF', PRECISION = 1.E-3)))

#  cable 3 - t = 600s
TEST_RESU(RESU = (_F(INST = 600.0,
                     GROUP_NO = 'N41', GROUP_MA = 'M5695',
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result1, NOM_CHAM = 'SIEF_ELNO', NOM_CMP = 'N',
                     VALE_CALC =  3605927.69583, VALE_REFE = 3.605000E6,
                     CRITERE = 'RELATIF', PRECISION = 1.0E-2),
                  _F(INST = 600.0,
                     GROUP_NO = 'N46', GROUP_MA = 'M5700',
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result1, NOM_CHAM = 'SIEF_ELNO', NOM_CMP = 'N',
                     VALE_CALC =  3633073.82469, VALE_REFE = 3.632000E6,
                     CRITERE = 'RELATIF', PRECISION = 1.0E-2),
                  _F(INST = 600.0,
                     GROUP_NO = 'N51', GROUP_MA = 'M5705',
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result1, NOM_CHAM = 'SIEF_ELNO', NOM_CMP = 'N',
                     VALE_CALC =  3660424.31492, VALE_REFE = 3.662000E6,
                     CRITERE = 'RELATIF', PRECISION = 1.E-2),
                  _F(INST = 600.0,
                     GROUP_NO = 'N56', GROUP_MA = 'M5710',
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result1, NOM_CHAM = 'SIEF_ELNO', NOM_CMP = 'N',
                     VALE_CALC =  3654489.3354, VALE_REFE = 3.655000E6,
                     CRITERE = 'RELATIF', PRECISION = 1.E-2),
                  _F(INST = 600.0,
                     GROUP_NO = 'N103', GROUP_MA = 'M5714',
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result1, NOM_CHAM = 'SIEF_ELNO', NOM_CMP = 'N',
                     VALE_CALC =  3627183.19162, VALE_REFE = 3.625000E6,
                     CRITERE = 'RELATIF', PRECISION = 1.E-2)))

#  cable 5 - t = 600s
TEST_RESU(RESU = (_F(INST = 600.0,
                     GROUP_NO = 'N81', GROUP_MA = 'M5735',
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result1, NOM_CHAM = 'SIEF_ELNO', NOM_CMP = 'N',
                     VALE_CALC =  3.64939314E+06, VALE_REFE = 3.647000E6,
                     CRITERE = 'RELATIF', PRECISION = 1.E-3),
                  _F(INST = 600.0,
                     GROUP_NO = 'N86', GROUP_MA = 'M5740',
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result1, NOM_CHAM = 'SIEF_ELNO', NOM_CMP = 'N',
                     VALE_CALC =  3.67686649E+06, VALE_REFE = 3.674000E6,
                     CRITERE = 'RELATIF', PRECISION = 1.E-3),
                  _F(INST = 600.0,
                     GROUP_NO = 'N91', GROUP_MA = 'M5745',
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result1, NOM_CHAM = 'SIEF_ELNO', NOM_CMP = 'N',
                     VALE_CALC =  3.69519477E+06, VALE_REFE = 3.695000E6,
                     CRITERE = 'RELATIF', PRECISION = 1.E-3),
                  _F(INST = 600.0,
                     GROUP_NO = 'N96', GROUP_MA = 'M5750',
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result1, NOM_CHAM = 'SIEF_ELNO', NOM_CMP = 'N',
                     VALE_CALC =  3.67135532E+06, VALE_REFE = 3.674000E6,
                     CRITERE = 'RELATIF', PRECISION = 1.E-3),
                  _F(INST = 600.0,
                     GROUP_NO = 'N105', GROUP_MA = 'M5754',
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result1, NOM_CHAM = 'SIEF_ELNO', NOM_CMP = 'N',
                     VALE_CALC =  3.64939314E+06, VALE_REFE = 3.647000E6,
                     CRITERE = 'RELATIF', PRECISION = 1.E-3)))

# End
FIN()
