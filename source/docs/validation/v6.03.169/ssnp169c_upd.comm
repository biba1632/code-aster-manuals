# SSNP169C

import sys

sys.path.append('/home/banerjee/Salome/ssnp169')

# Start
DEBUT(LANG = 'EN')

# Read functions
import ssnp169_config as config
from ssnp169_funcs import *

# Read mesh
mesh1 = LIRE_MAILLAGE(FORMAT = 'MED',
                      INFO = 1)

# Create node grom from element group C
mesh1 = DEFI_GROUP(reuse  = mesh1,
                    MAILLAGE = mesh1,
                    CREA_GROUP_NO = (_F(GROUP_MA = 'annulus')))

# Assign model
model_in = AFFE_MODELE(MAILLAGE = mesh1,
                       AFFE = (_F(GROUP_MA = ('annulus'),
                                  PHENOMENE = 'MECANIQUE',
                                  MODELISATION = 'D_PLAN')))

# Create crack
X0 = 0.
Y0 = 0.
circle = FORMULE(VALE = '(X-X0)**2+(Y-Y0)**2-R2*R2',
                 X0 = X0,
                 Y0 = Y0,
                 R2 = config.R2,
                 NOM_PARA = ['X', 'Y'])

crack = DEFI_FISS_XFEM(MAILLAGE = mesh1,
                       TYPE_DISCONTINUITE = 'INTERFACE',
                       DEFI_FISS = _F(FONC_LN = circle),
                       GROUP_MA_ENRI = 'annulus')

# Modify model and add crack
model_c = MODI_MODELE_XFEM(MODELE_IN = model_in,
                           FISSURE = (crack),
                           CONTACT = 'STANDARD')

model_nc = MODI_MODELE_XFEM(MODELE_IN = model_in,
                            FISSURE = (crack),
                            CONTACT = 'SANS')

# Define contact properties
con_xfem  =  DEFI_CONTACT(MODELE          =  model_c,
                          FORMULATION     =  'XFEM',
                          ITER_CONT_MAXI  =  4,
                          REAC_GEOM       =  'SANS',
                          ZONE = (_F(FISS_MAIT     =  crack,
                                     INTEGRATION   =  'GAUSS',
                                     ORDRE_INT     = 4,
                                     CONTACT_INIT  =  'OUI',
                                     ALGO_CONT     =  'STANDARD',
                                     ALGO_LAGR     =  'VERSION2')))

# Define material
E = config.E1
nu = config.nu1
steel = DEFI_MATERIAU(ELAS = _F(E = E,
                                NU = nu))

# Assign material
mater_c = AFFE_MATERIAU(MAILLAGE = mesh1,
                        MODELE = model_c,
                        AFFE = _F(GROUP_MA = ('annulus'),
                                  MATER = steel))

mater_nc = AFFE_MATERIAU(MAILLAGE = mesh1,
                         MODELE = model_nc,
                         AFFE = _F(GROUP_MA = ('annulus'),
                                   MATER = steel))


# Define boundary conditions
bc_c = AFFE_CHAR_MECA_F(MODELE = model_c,
                        DDL_IMPO = (_F(GROUP_MA = 'outer',
                                       DX = ux,
                                       DY = uy),
                                    _F(GROUP_MA = 'inner',
                                       DX = ux,
                                       DY = uy)),
                        INFO = 1)

bc_nc = AFFE_CHAR_MECA_F(MODELE = model_nc,
                         DDL_IMPO = (_F(GROUP_MA = 'outer',
                                        DX = ux,
                                        DY = uy),
                                     _F(GROUP_MA = 'inner',
                                        DX = ux,
                                        DY = uy)),
                         PRES_REP = (_F(FISSURE = crack,
                                      PRES = p_app)),
                         INFO = 1)


# Time stepping
l_times = DEFI_LIST_REEL(DEBUT = 0.0,
                         INTERVALLE = _F(JUSQU_A = 1.0,
                                         NOMBRE = 1))

# Run
resu_c = STAT_NON_LINE(MODELE = model_c,
                       CHAM_MATER = mater_c,
                       EXCIT = (_F(CHARGE = bc_c)),
                       CONTACT =  con_xfem,
                       COMPORTEMENT = _F(GROUP_MA = ('annulus'),
                                         RELATION = 'ELAS',
                                         DEFORMATION = 'PETIT'),
                       INCREMENT = _F(LIST_INST = l_times,
                                      INST_FIN = 1.0),
                       CONVERGENCE = (_F(ARRET = 'OUI',
                                         RESI_GLOB_RELA = 1.E-6,
                                         ITER_GLOB_MAXI = 30)),
                       SOLVEUR = _F(METHODE = 'MUMPS'),
                       NEWTON = _F(REAC_ITER = 1),
                       ARCHIVAGE = _F(CHAM_EXCLU = 'VARI_ELGA'),
                       INFO = 1)

resu_nc = STAT_NON_LINE(MODELE = model_nc,
                        CHAM_MATER = mater_nc,
                        EXCIT = (_F(CHARGE = bc_nc)),
                        COMPORTEMENT = _F(GROUP_MA = ('annulus'),
                                          RELATION = 'ELAS',
                                          DEFORMATION = 'PETIT'),
                        INCREMENT = _F(LIST_INST = l_times,
                                       INST_FIN = 1.0),
                        CONVERGENCE = (_F(ARRET = 'OUI',
                                          RESI_GLOB_RELA = 1.E-6,
                                          ITER_GLOB_MAXI = 30)),
                        SOLVEUR = _F(METHODE = 'MUMPS'),
                        NEWTON = _F(REAC_ITER = 1),
                        ARCHIVAGE = _F(CHAM_EXCLU = 'VARI_ELGA'),
                        INFO = 1)

# Save results with contact (for visualization)
mesh_c = POST_MAIL_XFEM(MODELE = model_c,
                        INFO = 2)
 
mod_v_c = AFFE_MODELE(MAILLAGE = mesh_c,
                      AFFE = _F(TOUT = 'OUI',
                                PHENOMENE = 'MECANIQUE',
                                MODELISATION = 'D_PLAN'))

r_xfe_c = POST_CHAM_XFEM(MODELE_VISU = mod_v_c,
                         RESULTAT = resu_c,
                         INFO = 2)
 
IMPR_RESU(FORMAT = 'MED',
          UNITE = 80,
          RESU = _F(RESULTAT = r_xfe_c))

# Save results without contact (for visualization)
mesh_nc = POST_MAIL_XFEM(MODELE = model_nc,
                         INFO = 2)
 
mod_v_nc = AFFE_MODELE(MAILLAGE = mesh_nc,
                       AFFE = _F(TOUT = 'OUI',
                                 PHENOMENE = 'MECANIQUE',
                                 MODELISATION = 'D_PLAN'))

r_xfe_nc = POST_CHAM_XFEM(MODELE_VISU = mod_v_nc,
                          RESULTAT = resu_nc,
                          INFO = 2)

IMPR_RESU(FORMAT = 'MED',
          UNITE = 80,
          RESU = _F(RESULTAT = r_xfe_nc))

# Create tables for verification tests
u_c = POST_RELEVE_T(ACTION = _F(INTITULE = 'DEPLE',
                                GROUP_NO = ('NFISSU'),
                                RESULTAT = r_xfe_c,
                                NOM_CHAM = 'DEPL',
                                NUME_ORDRE = 1,
                                TOUT_CMP = 'OUI',
                                OPERATION = 'EXTRACTION'))

tab_u_c = CALC_TABLE(TABLE = u_c,
                     ACTION = (_F(OPERATION  =  'FILTRE',
                                  NOM_PARA   =  'NOEUD',
                                  CRIT_COMP  =  'REGEXP',
                                  VALE_K     =  'N[^P]')))

u_nc = POST_RELEVE_T(ACTION = _F(INTITULE = 'DEPLE',
                                 GROUP_NO = ('NFISSU'),
                                 RESULTAT = r_xfe_nc,
                                 NOM_CHAM = 'DEPL',
                                 NUME_ORDRE = 1,
                                 TOUT_CMP = 'OUI',
                                 OPERATION = 'EXTRACTION'))

tab_u_nc = CALC_TABLE(TABLE = u_nc,
                      ACTION = (_F(OPERATION  =  'FILTRE',
                                   NOM_PARA   =  'NOEUD',
                                   CRIT_COMP  =  'REGEXP',
                                   VALE_K     =  'N[^P]')))

#-----------------------------------------------------------------
# Verification tests (with contact)
# TEST MAX
TEST_TABLE(CRITERE = 'RELATIF',
           REFERENCE = 'ANALYTIQUE',
           VALE_CALC = 0.00441071912513,
           VALE_REFE = ux2s(-config.R2,0.),
           NOM_PARA = 'DX',
           TYPE_TEST = 'MAX',
           TABLE = tab_u_c)

TEST_TABLE(CRITERE = 'RELATIF',
           REFERENCE = 'ANALYTIQUE',
           VALE_CALC = 0.00422620887487,
           VALE_REFE = uy2s(0.,-config.R2),
           NOM_PARA = 'DY',
           TYPE_TEST = 'MAX',
           TABLE = tab_u_c)

TEST_TABLE(CRITERE = 'RELATIF',
           REFERENCE = 'ANALYTIQUE',
           VALE_CALC = -9851917.79424,
           VALE_REFE = pressure(0.,config.R2),
           NOM_PARA = 'LAGS_C',
           TYPE_TEST = 'MAX',
           TABLE = tab_u_c)

# TEST MIN
TEST_TABLE(CRITERE = 'RELATIF',
           REFERENCE = 'ANALYTIQUE',
           VALE_CALC = -0.00441071912513,
           VALE_REFE = ux2s(config.R2,0.),
           NOM_PARA = 'DX',
           TYPE_TEST = 'MIN',
           TABLE = tab_u_c)

TEST_TABLE(CRITERE = 'RELATIF',
           REFERENCE = 'ANALYTIQUE',
           VALE_CALC = -0.00422620887487,
           VALE_REFE = uy2s(0.,config.R2),
           NOM_PARA = 'DY',
           TYPE_TEST = 'MIN',
           TABLE = tab_u_c)

TEST_TABLE(CRITERE = 'RELATIF',
           REFERENCE = 'ANALYTIQUE',
           VALE_CALC = -10148082.2058,
           VALE_REFE = pressure(config.R2,0.),
           NOM_PARA = 'LAGS_C',
           TYPE_TEST = 'MIN',
           TABLE = tab_u_c)

#-----------------------------------------------------------------
# Verification tests (without contact)
# TEST MAX
TEST_TABLE(CRITERE = 'RELATIF',
           REFERENCE = 'ANALYTIQUE',
           VALE_CALC = 0.00441079117684,
           VALE_REFE = ux2s(-config.R2,0.),
           NOM_PARA = 'DX',
           TYPE_TEST = 'MAX',
           TABLE = tab_u_nc)

TEST_TABLE(CRITERE = 'RELATIF',
           REFERENCE = 'ANALYTIQUE',
           VALE_CALC = 0.00422613682316,
           VALE_REFE = uy2s(0.,-config.R2),
           NOM_PARA = 'DY',
           TYPE_TEST = 'MAX',
           TABLE = tab_u_nc)

# TEST MIN
TEST_TABLE(CRITERE = 'RELATIF',
           REFERENCE = 'ANALYTIQUE',
           VALE_CALC = -0.00441079117684,
           VALE_REFE = ux2s(config.R2,0.),
           NOM_PARA = 'DX',
           TYPE_TEST = 'MIN',
           TABLE = tab_u_nc)

TEST_TABLE(CRITERE = 'RELATIF',
           REFERENCE = 'ANALYTIQUE',
           VALE_CALC = -0.00422613682316,
           VALE_REFE = uy2s(0.,config.R2),
           NOM_PARA = 'DY',
           TYPE_TEST = 'MIN',
           TABLE = tab_u_nc)

# End
FIN()
