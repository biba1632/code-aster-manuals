# coding=utf-8
DEBUT(LANG = 'EN')

# Material parameters
E = 200.0e9
nu = 0.3
rho = 8000

# Load/Time parameters
t_final = 0.5
t_inc = 0.002
freq_band = 1.0/(5.*t_inc)
fc = 15.0;
omega = ((2.0 * pi) * fc);

# Create a list of times
t_list = DEFI_LIST_REEL(DEBUT = 0.,
                        INTERVALLE = _F(JUSQU_A = t_final,
                                        PAS = t_inc))

# Create function for computing load scaling
sin_om = FORMULE(VALE = 'sin(omega*INST)',
                 omega = omega,
                 NOM_PARA = 'INST')

# Read mesh
mesh = LIRE_MAILLAGE(FORMAT = 'MED',
                     UNITE = 20)

# Assign 3D elements
model = AFFE_MODELE(AFFE = _F(MODELISATION = ('3D', ),
                              PHENOMENE = 'MECANIQUE',
                              TOUT = 'OUI'),
                    MAILLAGE = mesh)

# Create and assign material
steel = DEFI_MATERIAU(ELAS = _F(E = E,
                                NU = nu,
                                RHO = rho))

mater = AFFE_MATERIAU(AFFE = _F(MATER = steel,
                                TOUT = 'OUI'),
                      MODELE = model)

# Write out material and inertial properties to a table
mat_geom = POST_ELEM(MASS_INER = _F(TOUT = 'OUI'),
                     MODELE = model,
                     CHAM_MATER = mater)

IMPR_TABLE(TABLE = mat_geom,
           SEPARATEUR = ',')
 
# Displacement BCs
disp_bc = AFFE_CHAR_MECA(DDL_IMPO = _F(DX = 0.0,
                                       DY = 0.0,
                                       DZ = 0.0,
                                       GROUP_MA = ('fixed_face')),
                         MODELE = model)

# Body force
gravity = AFFE_CHAR_MECA(MODELE = model,
                         PESANTEUR = _F(GRAVITE = 300.,
                                        DIRECTION = (-1., 0, 1)))

#------------------------------------------------
# (Transient) Linear Dynamic simulation  (GENE)
res_gen = DYNA_LINE(TYPE_CALCUL = "TRAN",
                    BASE_CALCUL = "GENE",
                    BASE_RESU = CO('res_base'),
                    RESU_GENE = CO('tran_gen'),
                    ENRI_STAT = 'NON',
                    MODELE = model,
                    CHAM_MATER = mater,
                    CHARGE = disp_bc,
                    EXCIT = _F(CHARGE = gravity, FONC_MULT = sin_om),
                    BANDE_ANALYSE = 50,
                    SCHEMA_TEMPS = _F(SCHEMA = 'NEWMARK'),
                    INCREMENT = _F(INST_FIN = t_final,
                                   PAS = t_inc))

# Write results
IMPR_RESU(FORMAT = 'MED',
          RESU = _F(RESULTAT = res_gen),
          UNITE = 80)

# Extract data for curves (at point P)
u_x_P = RECU_FONCTION(RESULTAT = res_gen,
                      NOM_CHAM = 'DEPL',
                      NOM_CMP = 'DX',
                      GROUP_NO = 'point_P')

u_z_P = RECU_FONCTION(RESULTAT = res_gen,
                      NOM_CHAM = 'DEPL',
                      NOM_CMP = 'DZ',
                      GROUP_NO = 'point_P')

# Write data to file
IMPR_FONCTION(FORMAT = 'TABLEAU',
              UNITE = 29,
              SEPARATEUR = ',',
              COURBE = (_F(FONCTION = u_x_P)))

IMPR_FONCTION(FORMAT = 'TABLEAU',
              UNITE = 29,
              SEPARATEUR = ',',
              COURBE = (_F(FONCTION = u_z_P)))

#------------------------------------------------
# Compute response in physical basis  (PHYS)
t_sub_step = t_inc / 10
res_phy = DYNA_LINE(TYPE_CALCUL = "TRAN",
                    BASE_CALCUL = "PHYS",
                    MODELE = model,
                    CHAM_MATER = mater,
                    CHARGE = disp_bc,
                    EXCIT = _F(CHARGE = gravity, 
                               FONC_MULT = sin_om),
                    SCHEMA_TEMPS = _F(SCHEMA = 'NEWMARK'),
                    INCREMENT = _F(INST_FIN = t_final,
                                   PAS = t_sub_step),
                    ARCHIVAGE = _F(LIST_INST = t_list, 
                                   PAS_ARCH = 10**8))

# Write results
IMPR_RESU(FORMAT = 'MED',
          RESU = _F(RESULTAT = res_phy),
          UNITE = 81)

# Extract data for curves (at point P)
u_x_P_1 = RECU_FONCTION(RESULTAT = res_phy,
                        NOM_CHAM = 'DEPL',
                        NOM_CMP = 'DX',
                        GROUP_NO = 'point_P')

u_z_P_1 = RECU_FONCTION(RESULTAT = res_phy,
                        NOM_CHAM = 'DEPL',
                        NOM_CMP = 'DZ',
                        GROUP_NO = 'point_P')

# Write data to file
IMPR_FONCTION(FORMAT = 'TABLEAU',
              UNITE = 29,
              SEPARATEUR = ',',
              COURBE = (_F(FONCTION = u_x_P_1)))

IMPR_FONCTION(FORMAT = 'TABLEAU',
              UNITE = 29,
              SEPARATEUR = ',',
              COURBE = (_F(FONCTION = u_z_P_1)))

#------------------------------------------------
# Calculate with manual time-stepping

t_sub_sub_step = t_inc / 100

# Create a list of times
t_list_1 = DEFI_LIST_REEL(DEBUT = 0.,
                          INTERVALLE = _F(JUSQU_A = t_final,
                                          PAS = t_sub_sub_step))

# Set up an interpolation function
load_fn = CALC_FONC_INTERP(FONCTION = sin_om,
                           LIST_PARA = t_list_1,
                           NOM_RESU = 'sin_load',
                           PROL_GAUCHE = 'CONSTANT',
                           PROL_DROITE = 'CONSTANT',
                           INTERPOL = 'LIN')

# Compute (GENE)
res_gen2 = DYNA_LINE(TYPE_CALCUL = "TRAN",
                     BASE_CALCUL = "GENE",
                     MODELE = model,
                     CHAM_MATER = mater,
                     CHARGE = disp_bc,
                     EXCIT = _F(CHARGE = gravity, 
                                FONC_MULT = load_fn),
                     #SCHEMA_TEMPS = _F(SCHEMA = 'DEVOGE'),
                     INCREMENT = _F(INST_FIN = t_final),
                     ORTHO = 'OUI',
                     ARCHIVAGE = _F(LIST_INST = t_list, 
                                    PAS_ARCH = 10**8))

# Write results
IMPR_RESU(FORMAT = 'MED',
          RESU = _F(RESULTAT = res_gen2),
          UNITE = 82)

# Extract data for curves (at point P)
u_x_P_2 = RECU_FONCTION(RESULTAT = res_gen2,
                        NOM_CHAM = 'DEPL',
                        NOM_CMP = 'DX',
                        GROUP_NO = 'point_P')

u_z_P_2 = RECU_FONCTION(RESULTAT = res_gen2,
                        NOM_CHAM = 'DEPL',
                        NOM_CMP = 'DZ',
                        GROUP_NO = 'point_P')

# Write data to file
IMPR_FONCTION(FORMAT = 'TABLEAU',
              UNITE = 29,
              SEPARATEUR = ',',
              COURBE = (_F(FONCTION = u_x_P_2)))

IMPR_FONCTION(FORMAT = 'TABLEAU',
              UNITE = 29,
              SEPARATEUR = ',',
              COURBE = (_F(FONCTION = u_z_P_2)))

# Compare the last two cases 
u_x_P_12 = CALC_FONCTION(COMB = (_F(FONCTION = u_x_P_1, COEF = 1.),
                                 _F(FONCTION = u_x_P_2, COEF = -1.)))

u_z_P_12 = CALC_FONCTION(COMB = (_F(FONCTION = u_z_P_1, COEF = 1.),
                                 _F(FONCTION = u_z_P_2, COEF = -1.)))

max_d_ux = INFO_FONCTION(MAX = _F(FONCTION = u_x_P_12))
max_d_uz = INFO_FONCTION(MAX = _F(FONCTION = u_z_P_12))

# unite = 85
# for st in ['UX3','UZ3','UX4','UZ4','DUZ34']:
    # fichier = 'REPE_OUT/%s.txt' %(st)
    # DEFI_FICHIER(UNITE=unite,FICHIER=  fichier )
    # IMPR_FONCTION(FORMAT='TABLEAU',
              # COURBE=_F(FONCTION=eval(st),),
              # UNITE=unite,);
    # DEFI_FICHIER(UNITE=unite,ACTION='LIBERER');

# Verification of results
TEST_FONCTION(VALEUR = (_F(VALE_CALC = 0.14758711700494,
                           VALE_PARA = 0.05,
                           FONCTION = u_z_P)))

TEST_FONCTION(VALEUR = (_F(VALE_CALC = 5.667541668703E-05,
                           VALE_PARA = 0.05,
                           FONCTION = u_x_P)))

TEST_FONCTION(VALEUR = (_F(VALE_CALC = 0.14604340154756,
                           VALE_PARA = 0.05,
                           FONCTION = u_z_P_1)))

TEST_FONCTION(VALEUR = (_F(VALE_CALC = 1.749786405277E-04,
                           VALE_PARA = 0.05,
                           CRITERE = "ABSOLU",
                           FONCTION = u_x_P_1)))

TEST_FONCTION(VALEUR = (_F(VALE_CALC = 0.14665350269746,
                           VALE_PARA = 0.05,
                           FONCTION = u_z_P_2)))

TEST_FONCTION(VALEUR = (_F(VALE_CALC = 2.191476390357E-04,
                           VALE_PARA = 0.05,
                           CRITERE = "ABSOLU",
                           FONCTION = u_x_P_2)))

TEST_TABLE(CRITERE = 'ABSOLU',
           REFERENCE = 'ANALYTIQUE',
           VALE_CALC = 0.0E+00,
           VALE_REFE = 0.0,
           NOM_PARA = 'DX',
           TYPE_TEST = 'SOMM_ABS',
           TABLE = max_d_ux)

TEST_TABLE(CRITERE = 'ABSOLU',
           REFERENCE = 'ANALYTIQUE',
           VALE_CALC = 0.0E+00,
           VALE_REFE = 0.0,
           NOM_PARA = 'DZ',
           TYPE_TEST = 'SOMM_ABS',
           TABLE = max_d_uz,
           PRECISION = 0.005)

# End
FIN()
