.. role:: red
.. role:: blue
.. role:: green
.. role:: gray
.. role:: python(code)
   :language: python
.. |br| raw:: html

   <br/>

.. index:: viscoplastic, viscoelastic

.. _r5.03.08:

*******************************************************************************************
**[R5.03.08]** : Integration of nonlinear viscoplastic material models in ``STAT_NON_LINE``
*******************************************************************************************

This document the ingredients necessary for the implementation of viscoplastic material models
in the non-linear algorithm :red:`STAT_NON_LINE` :ref:`[R5.03.01] <r5.03.01>`. The input data of all
viscoplastic models in ``Code_Aster`` generally have the same form. Only the method of introducing the 
viscous strain rate function varies.  The strain rate function is input with various keywords which 
allow the user to choose the desired constitutive model. 

These quantities are calculated by a semi-implicit or implicit integration method. From the initial 
state, or from the preceding moment of computation, one computes the stress field resulting from an 
increment of strain.


.. _r5.03.08.1:

Introduction
=============

Here we describe the implementation of the Lemaitre model of non-linear viscoplasticity, which reduces to 
the Norton model for particular values of the parameters.

A variant (depending on the neutron fluence) of the Lemaître model has been added, with a view to modeling 
nuclear fuel assemblies (keyword :blue:`LEMAITRE_IRRA`). This model has an additional inelastic strain: 
the strain magnification.

A viscoplastic model with threshold has been added. This is a material whose behavior is purely elastic 
up to a threshold beyond which the model becomes a particular case of Lemaître model (:blue:`LEMA_SEUIL`).

Finally, a viscoplastic model with creep depending on the logarithmic strain rate was implemented. It is
accessible by the keyword :blue:`VISC_IRRA_LOG`.

For each of these models, it is assumed that the material is isotropic (except for the strain 
magnification, which is uniaxial). They can be used in 3D, in plane strain (:blue:`D_PLAN`) and in 
axisymmetric mode (:blue:`AXIS`).

We present in this document the constitutive equations of the models and their implementation in
``Code_Aster``.


.. _r5.03.08.2:

Basic principle and assumptions
=================================

We assume of the small deformations and decompose the strain tensor into an elastic part, a thermal part, 
an inelastic part (known), and a viscous part. The equations are then:

.. math::

   & \boldsymbol{\varepsilon}_{\text{tot}} = \boldsymbol{\varepsilon}_e + \boldsymbol{\varepsilon}_{th} +
      \boldsymbol{\varepsilon}_a + \boldsymbol{\varepsilon}_v \\
   & \boldsymbol{\sigma} = \mathbb{A}(T) \boldsymbol{\varepsilon}_e \\
   & \dot{\boldsymbol{\varepsilon}}_v = g(\sigma_{\text{eq}}, \lambda , T) 
       \frac{3}{2} \frac{\tilde{\boldsymbol{\sigma}}}{\sigma_{\text{eq}}}

with 

* :math:`\lambda` : cumulated viscous strain 

  .. math::

     \dot{\lambda} = \sqrt{\frac{2}{3}\dot{\boldsymbol{\varepsilon}}_v : \dot{\boldsymbol{\varepsilon}}_v}

* :math:`\tilde{\boldsymbol{\sigma}}` : stress deviator 

  .. math::

     \tilde{\boldsymbol{\sigma}} = \boldsymbol{\sigma} - \frac{1}{3} \text{Tr}(\boldsymbol{\sigma}) \mathbf{I}

* :math:`\sigma_{\text{eq}}`: equivalent stress 

  .. math::

     \sigma_{\text{eq}} = \sqrt{\frac{3}{2} \tilde{\boldsymbol{\sigma}}: \tilde{\boldsymbol{\sigma}}}

* :math:`\mathbb{A}(T)` : elasticity tensor

.. _r5.03.08.3:

Nature of the function :math:`g` for each of the models
=======================================================

.. _r5.03.08.3.1:

:blue:`LEMAITRE` model
----------------------

In this case, :math:`g` is expressed explicitly (:math:`\sigma` is a scalar) as:

.. math::

   g(\sigma, \lambda, T) = \left(\frac{1}{K} \frac{\sigma}{\lambda^{1/m}} \right)^n
   \quad \text{with} \quad
   \frac{1}{K} \ge 0, \quad \frac{1}{m} \ge 0, \quad n > 0

The material data are those provided under the keyword factors: :blue:`LEMAITRE` or : :blue:`LEMAITRE_FO` of the 
operator :red:`DEFI_MATERIAU`.

::

   LEMAITRE = _F(N = n,
                 UN_SUR_K = 1/K, 
                 UN_SUR_M = 1/m)

The Young's modulus :math:`E` and the Poisson's ratio :math:`\nu` are those provided under the keyword factors
:blue:`ELAS` or :blue:`ELAS_FO`.

.. _r5.03.08.3.2:

Fluence-dependent :blue:`LEMAITRE` model (:blue:`LEMAITRE_IRRA`)
-----------------------------------------------------------------

This section describes the fluence-dependence (and its processing) of the viscoplastic model of J. Lemaître, 
introduced for the modeling of fuel assemblies and applicable to 2D and 3D elements and to :blue:`TUYAU`
(:gray:`PIPE`) elements, under the keyword :blue:`LEMAITRE_IRRA`.

.. _r5.03.08.3.2.1:

Model formulation
^^^^^^^^^^^^^^^^^

The equations are as follows:

.. math::

   & \dot{\boldsymbol{\varepsilon}}_v = \frac{3}{2} \dot{p} \frac{\tilde{\boldsymbol{\sigma}}}{\sigma_{\text{eq}}} \\
   & \dot{p} = \left[\frac{\sigma_{\text{eq}}}{p^{1/m}}\right]^ n
               \left(\frac{1}{K}\frac{\varphi}{\varphi_0}+ L \right)^\beta
               \exp\left[-\frac{Q}{R(T+T_0)}\right] \\
   & \dot{[\mathbb{A}^{-1}(T) \boldsymbol{\sigma}]}= \dot{\boldsymbol{\varepsilon}}_{\text{tot}} -
       \dot{\boldsymbol{\varepsilon}}_v - \dot{\boldsymbol{\varepsilon}}_g - \dot{\boldsymbol{\varepsilon}}_{th}

with :math:`T_0` = 273.15 C, :math:`n > 0`, :math:`1/K \ge 0`, :math:`1/m \ge 0`, :math:`\varphi_0 > 0`, 
:math:`Q/R \ge 0`, :math:`L \ge 0`, :math:`\beta` any real.

The coefficients are provided under the keywords :blue:`LEMAITRE_IRRA` and :blue:`ELAS` of :red:`DEFI_MATERIAU` and 
:math:`\varphi` is the neutron flux (quotient of the fluence increment, defined by the keyword :blue:`AFFE_VARC` of
:red:`AFFE_MATERIAU`, per increment of time).

The magnification model is: 

.. math::

   \boldsymbol{\varepsilon}_g(t) = f(T, \varphi_t)\boldsymbol{\varepsilon}_g^0 

where :math:`\varepsilon_g^0` is the uniaxial strain unit in a reference coordinate :math:`R_1` given by the 
user using the keyword :blue:`MASSIF` (see :ref:`[U4.42.01] <u4.42.01>` and :ref:`[U4.43.01] <u4.43.01>`) and
:math:`f(T, \varphi_t)` is also a function defined by the user in :red:`DEFI_MATERIAU` 
(:ref:`[U4.43.01] <u4.43.01>`).

.. admonition:: Remarks

   The fluence, time and flux :math:`\varphi_0` must be expressed in units such as the ratio
   :math:`\varphi/\varphi_0` is dimensionless. :math:`Q/R` is in Kelvin. :math:`T` is in C.

.. _r5.03.08.3.2.2:

Treatment of fluence-dependence
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The model described above corresponds to a normal Lemaître model, defined by the three coefficients 
:math:`n, 1/K^{'}`, and :math:`1/m` with:

.. math::

   \frac{1}{K^{'}} = \left(\frac{1}{K} \frac{\varphi}{\varphi_0} + L \right)^{\beta/n}
      \exp\left(-\frac{Q}{nR (T + T_0)}\right)

So just calculate :math:`1/K^{'}` and provide that data to the calculation instead of :math:`1/K`.

In addition, in the computation of the elastic stress, one adds to the inelastic strains (zero by default) the 
strain magnification expressed above, after having made the change between the local coordinate system and the 
:math:`R_1` coordinate system.

.. _r5.03.08.3.3:

:blue:`LEMA_SEUIL` model
------------------------

For this model, :math:`g` is also expressed explicitly (since it is a special case of the LEMAITRE model
presented above).  If we define

.. math::

   D = \frac{1}{S} \int_0^t \sigma_{\text{eq}}(\tau) d\tau

then the model behaves as follows:

.. math::

   & \text{If} \quad D \le 1 \quad \text{then} \quad g(\sigma, \lambda, T) = 0 (\text{purely elastic behavior}) \\
   & \text{If} \quad D > 1 \quad \text{then} \quad g(\sigma, \lambda, T) = A \left(\frac{2}{\sqrt{3}}\sigma\right) 
       \varphi \quad \text{with} \quad  A \ge 0, \varphi \ge 0

The material data to be input by the user must be provided under the keyword :blue:`LEMA_SEUIL` or 
:blue:`LEMA_SEUIL_FO` of operator :red:`DEFI_MATERIAU`:

::

  LEMA_SEUIL = _F(A = A, S = S)

As for the parameter :math:`\varphi`, it is about the neutron flux which bombards the material (quotient of the 
increment of fluence, defined by the keyword :blue:`AFFE_VARC` of :red:`AFFE_MATERIAU`, by the increment of time).

The Young modulus :math:`E` and the Poisson's ratio :math:`\nu` are those provided under the keyword factors
:blue:`ELAS` or :blue:`ELAS_FO`.

.. _r5.03.08.3.4:

:blue:`VISC_IRRA_LOG` model
---------------------------

For this model, :math:`g` is not expressed explicitly. The behavior is represented by an one-dimensional creep 
test, at constant stress, which involves the time elapsed since the instant when the stress is applied. The model 
is defined by four functions :math:`f_1, g_1, f_2, g_2` describing the evolution of the viscous strain over time:

.. math::
   :label: eq_r5.03.08.3.4-1

   \varepsilon_v = \lambda = f_1(t) g_1(\sigma, T) + f_2(t) g_2(\sigma, T)

The function :math:`g` is then calculated numerically by eliminating the time :math:`t` as follows:

1) for a given triplet :math:`(\sigma, \lambda, T)`, one solves in :math:`t` the equation :eq:`eq_r5.03.08.3.4-1` by 
   the Newton method (see [#bib2]_). We find an approximation of the solution :math:`t(\sigma, \lambda, T )`. 
2) we obtain the value of the function :math:`g(\sigma, \lambda, T)` by differentiating with respect to time
   the equation :eq:`eq_r5.03.08.3.4-1` (see [#bib1]_):

   .. math::

      \dot{\varepsilon}_v = \dot{\lambda} = g(\sigma, \lambda, T) = 
          f_1'(t) g_1(\sigma, T) + f_2'(t) g_2(\sigma, T)

   and by substituting in this new equation the value of :math:`t(\sigma, \lambda, T)` found previously. We 
   find the following uniaxial formulation:

   .. math::

      \dot{\varepsilon}_v = \dot{\lambda} = g(\sigma, \lambda, T) = 
          f_1'(t(\sigma, \lambda, T)) g_1(\sigma, T) + f_2'(t(\sigma, \lambda, T)) g_2(\sigma, T )

The form of the four functions :math:`f_1, g_1, f_2, g_2` is predefined and the user only introduces a few
parameters in the command file.

For :blue:`VISC_IRRA_LOG`, we have:

.. math::

   & f_1(t) = \ln(1 + \omega \varphi t ) \\
   & g_1(\sigma, T) = A \sigma \exp\left(-\frac{Q}{T}\right) \\
   & f_2(t) = \varphi t \\
   & g_2(\sigma, T) = B \sigma \exp\left(-\frac{Q}{T}\right)

The parameter :math:`\varphi` is the neutron flux. It is must be indicated under the keyword factor 
:blue:`AFFE_VARC` via the command variable :blue:`IRRA`.

The parameters :math:`A, B, \omega` and :math:`Q` are those provided under the keyword factor :blue:`VISC_IRRA_LOG` 
of operator :red:`DEFI_MATERIAU`.

Note that for all functions, :math:`t` is expressed in hours, :math:`T` is expressed in C, :math:`\sigma` is 
expressed in MPa.

It is thus necessary to provide coherent data with these units in the command file and in the mesh file. i

The Young modulus :math:`E` and the Poisson's ratio :math:`\nu` are those provided under the keyword factors
:blue:`ELAS` or :blue:`ELAS_FO`.


.. _r5.03.08.4:

Integration of the constitutive relation
========================================

.. _r5.03.08.4.1:

Establishment of the scalar equation for the implicit scheme and with constant elastic coefficients
----------------------------------------------------------------------------------------------------

We denote by :math:`\boldsymbol{\varepsilon}_{\text{tot}}` the total strain at time :math:`t +\Delta t` and by 
:math:`\Delta \boldsymbol{\varepsilon}_{\text{tot}}` the increment of the total strain during the current time 
step. 

We call :math:`\boldsymbol{\varepsilon}_o` the strain at time :math:`t +\Delta t` resulting from thermal expansion 
and inelastic strains (among which possibly appear the magnification strains, cf :numref:`r5.03.08.3.2`). So 
we have :

.. math::

   \Delta \boldsymbol{\varepsilon}_o = 
     [ \alpha(t+\Delta t) (T(t+\Delta t) - T_{\text{ref}}) - \alpha(t)(T(t) - T_{\text{ref}})] \mathbf{I}_3 +
     \boldsymbol{\varepsilon}_a (t+\Delta t) - \boldsymbol{\varepsilon}_a(t)

where :math:`\mathbf{I}_3` is the identity tensor of order 2 in dimension 3.

We set :math:`\Delta\boldsymbol{\varepsilon} = \Delta\boldsymbol{\varepsilon_{\text{tot}}} - \Delta \boldsymbol{\varepsilon}_o`.

As we suppose here that :math:`\mu` is constant, we have the following relation between the deviators of 
:math:`\Delta \boldsymbol{\sigma}` and :math:`\Delta \boldsymbol{\varepsilon}` :

.. math::
   :label: eq_r5.03.08.4.1-1

   \Delta \tilde{\boldsymbol{\sigma}} = 2\mu (\Delta\tilde{\boldsymbol{\varepsilon}} - 
       \Delta\boldsymbol{\varepsilon}_v 

However, the flow rule is written, implicitly:

.. math::
   :label: eq_r5.03.08.4.1-2

   \frac{\Delta \boldsymbol{\varepsilon}_v}{\Delta t} = \frac{3}{2} g(\sigma_{\text{eq}}, \lambda^{-} + 
       (\Delta \boldsymbol{\varepsilon}_v)_{\text{eq}}, T) 
       \frac{\tilde{\boldsymbol{\sigma}}}{\boldsymbol{\sigma}_{\text{eq}}}

We therefore have, by eliminating :math:`\Delta \boldsymbol{\varepsilon}_v` between :eq:`eq_r5.03.08.4.1-1` and 
:eq:`eq_r5.03.08.4.1-2`:

.. math::
   :label: eq_r5.03.08.4.1-3

   & 2\mu \Delta \tilde{\boldsymbol{\varepsilon}} = \Delta \tilde{\boldsymbol{\sigma}} +
     3\mu\Delta t  g(\sigma_{\text{eq}}, \lambda^{-} + (\Delta \boldsymbol{\varepsilon}_v)_{\text{eq}}, T)
     \frac{\tilde{\boldsymbol{\sigma}}}{\sigma_{\text{eq}}} \\
   & (\tilde{\boldsymbol{\sigma}}^{-} + 2\mu\Delta\tilde{\boldsymbol{\varepsilon}}) = 
     \left(1 + 3\mu\Delta t \frac{g(\sigma_{\text{eq}}, \lambda^{-} + 
          (\Delta \boldsymbol{\varepsilon}_v)_{\text{eq}}, T)}{\sigma_{\text{eq}}}\right) 
     \tilde{\boldsymbol{\sigma}}

By posing :math:`\tilde{\boldsymbol{\sigma}}^e = \tilde{\boldsymbol{\sigma}}^{-} + 2\mu\Delta\tilde{\boldsymbol{\varepsilon}}`, we therefore have:

.. math::
   :label: eq_r5.03.08.4.1-4

   \sigma_{\text{eq}}^e = 3\mu \Delta t g(\sigma_{\text{eq}}, \lambda^{-} + 
      ( \Delta \boldsymbol{\varepsilon}_v )_{\text{eq}}, T) + \sigma_{\text{eq}}

However, one has according to :eq:`eq_r5.03.08.4.1-2`:

.. math::

   (\Delta \boldsymbol{\varepsilon}_v)_{\text{eq}} = \Delta t  g (\sigma_{\text{eq}}, 
      \lambda^{-} + (\Delta \boldsymbol{\varepsilon}_v)_{\text{eq}}, T)

From where :

.. math::

   \sigma_{\text{eq}}^e = 3\mu (\Delta \boldsymbol{\varepsilon}_v)_{\text{eq}} + 
     \sigma_{\text{eq}}(\Delta\boldsymbol{\varepsilon}_v)_{\text{eq}} =
     \frac{1}{3\mu} (\sigma_{\text{eq}}^e - \sigma_{\text{eq}})

By substituting this last expression in :eq:`eq_r5.03.08.4.1-4`, one has:

.. math::

   \sigma_{\text{eq}}^e = 3\mu \Delta t  g(\sigma_{\text{eq}}, \lambda^{-} + 
      \frac{1}{3\mu} (\sigma_{\text{eq}}^e-\sigma_{\text{eq}}), T) + \sigma_{\text{eq}}

If we set :math:`\sigma_{\text{eq}}^e, \lambda^{-}, T` and :math:`\Delta t` as known:

.. math::

   f(x) = 3\mu\Delta t \, g(x, \lambda^{-} + \frac{1}{3} \mu(\sigma_{\text{eq}}^e - x), T) + 
      x - \sigma_{\text{eq}}^e

we can then calculate the quantity 
:math:`\sigma_{\text{eq}} = (\boldsymbol{\sigma}^{-} + \Delta\boldsymbol{\sigma})_{\text{eq}}` as being the 
solution of the scalar equation: :math:`f(x) = 0` where :math:`x = \sigma_{\text{eq}}` is convention adopted for the following paragraphs.

In the case of a Lemaître model with threshold, :blue:`LEMA_IRRA_SEUIL`, the preceding equations are 
useful only once the threshold has been crossed. Indeed, below the threshold, the behavior is elastic.

We discretize the threshold implicitly:

.. math::

   D(\boldsymbol{\sigma}^{-} + \Delta \boldsymbol{\sigma}) = 
     \frac{1}{S} \int_0^t(\boldsymbol{\sigma}^{-} + \Delta \boldsymbol{\sigma})_{\text{eq}}(\tau) d\tau

In the same way as for the integration of the elastoplastic constitutive laws of von Mises, one then distinguishes 
two cases.

* :math:`D(\boldsymbol{\sigma}^{-} + \Delta \boldsymbol{\sigma}) \le 1` then 
  :math:`g(\boldsymbol{\sigma}^{-} + \Delta \boldsymbol{\sigma}, \lambda, T) = 0`.

* :math:`D(\boldsymbol{\sigma}^{-} + \Delta \boldsymbol{\sigma}) > 1` then 
  :math:`g(\boldsymbol{\sigma}^{-} + \Delta \boldsymbol{\sigma}, \lambda, T) = A \left(\frac{2}{\sqrt{3}}\sigma_{\text{eq}}\right) \varphi`

This results from the above equation:

.. math::

   g(\boldsymbol{\sigma}^{-} + \Delta \boldsymbol{\sigma}, \lambda, T ) \ne 
     A \left(\frac{2}{\sqrt{3}} \sigma_{\text{eq}}\right) \varphi 
   \quad \implies \quad D(\boldsymbol{\sigma}^{-} +\Delta \boldsymbol{\sigma}) \le 1

Or :math:`g` can only take the value :math:`0` or :math:`A \left(\frac{2}{\sqrt{3}}\sigma_{\text{eq}}\right)\varphi`
therefore

.. math::

   g(\boldsymbol{\sigma}^{-} + \Delta \boldsymbol{\sigma}, \lambda, T ) \ne 
     A \left(\frac{2}{\sqrt{3}} \sigma_{\text{eq}}\right) \varphi 
   \quad \implies \quad D(\boldsymbol{\sigma}^{-} + \Delta \boldsymbol{\sigma}) \le 1 
   \quad \text{and} \quad \Delta\boldsymbol{\sigma} = A \Delta \boldsymbol{\varepsilon}

Alternatively, if

.. math::

   g(\boldsymbol{\sigma}^{-} + \Delta \boldsymbol{\sigma}, \lambda, T ) = 
     A \left(\frac{2}{\sqrt{3}} \sigma_{\text{eq}}\right) \varphi 

   \quad \text{then} \quad D(\boldsymbol{\sigma}^{-} + A \boldsymbol{\varepsilon}) > 1 

The threshold criterion can thus be written 

.. math::

   D(\boldsymbol{\sigma}^{-} + A \Delta \boldsymbol{\varepsilon}) > 1

Or 

.. math::

   D(\boldsymbol{\sigma}^{-} + A \Delta \boldsymbol{\varepsilon}) =
     \frac{1}{S} \int_0^t (\boldsymbol{\sigma}^{-} + A \Delta \boldsymbol{\varepsilon})_{\text{eq}}(\tau) d\tau

By discretizing the time, we then have:

.. math::

   D(\boldsymbol{\sigma}^{-} + A \Delta \boldsymbol{\varepsilon}) =
    \frac{1}{S} \int_0^{t^{-}} (\boldsymbol{\sigma}^{-} + A \Delta \boldsymbol{\varepsilon})_{\text{eq}}(\tau) d\tau +
    \frac{1}{S} \int_{t^{-}}^t (\boldsymbol{\sigma}^{-} + A \Delta \boldsymbol{\varepsilon})_{\text{eq}}(\tau) d\tau

.. math::

   D(\boldsymbol{\sigma}^{-} + A \Delta \boldsymbol{\varepsilon}) =
    \frac{1}{S} \left(D^{-}S + \frac{\sigma_{\text{eq}}^{-} + (\boldsymbol{\sigma}^{-} + 
        A \Delta \boldsymbol{\varepsilon})_{\text{eq}}}{2}(t - t^{-})\right)

.. _r5.03.08.4.2:

Solution of the scalar equation: principle of the ``ZEROF2`` routine
---------------------------------------------------------------------

It can be shown that, if the conditions required in :numref:`r5.03.09.3` on the characteristics of materials are 
satisfied, the function :math:`f` is strictly increasing and the equation :math:`f(x) = 0` admits a unique solution.

If :math:`\sigma_{\text{eq}}^e = 0`, then the solution is :math:`x = 0`. Otherwise, we have: 
:math:`f(0) = -\sigma_{\text{eq}}^e < 0`

The problem therefore consists in finding, for any function :math:`f`, the solution of the equation
:math:`f(x) = 0` knowing that this solution exists, that :math:`f(0) < 0` and that :math:`f` is strictly increasing.

The algorithm adopted in ``ZEROF2`` is as follows:

* we start with :math:`a_0` = 0 and :math:`b_0 = x_{ap}` where :math:`x_{ap}` is an approximation of the solution. 
  If necessary (i.e., if :math:`f(b_0) < 0`), we use the secant method  

  .. math::

     z_n = \frac{a_n f(b_n) - b_n f(a_n)}{f(b_n) - f(a_n)} \quad
     \text{then} \quad  a_{n+1} = b_n \quad \text{and} \quad  b_{n+1} = z_n 

  in one or more iterations for the case where :math:`f(a) < 0` and :math:`f(b) > 0` as seen in the
  figure below :

  .. figure:: r5.03.08/fig1_r5.03.08.png
     :height: 300 px
     :align: center

  In the case of the figure above, this step was completed in one iteration: :math:`a_1 = b_0` and 
  :math:`f(b_1) > 0`.

* we calculate :math:`N_d = \text{round}(\sqrt{N_{\text{max}}})` where :math:`N_{\text{max}}` is the maximum number 
  of iterations allowed. We then solve the equation by the secant method using the bisection method whenever :math:`n`
  is a multiple of :math:`N_d` :

     * Tag 1:

        * If :math:`N_d` is a whole number factor of :math:`n`:

          .. math::

             z n = \frac{a_n + b_n}{2}

        * else

          .. math::

             z_n = \frac{a_n f(b_n) - b_n f(a_n)}{f(b_n) - f(a_n)} \quad

        * endif 

        * :math:`n = n + 1`

        * if :math:`∣f(z)∣ > \epsilon`

          * if :math:`f(z) < 0`

            :math:`a_{n+1} = z_n \quad b_{n+1} = b_n`

          * else

            :math:`a_{n+1} = a_n \quad b_{n+1} = z_n`

          * endif

          * go to Tag 1

        * else

          * The solution is: :math:`x = z_n \rightarrow \quad \text{END}`

        * endif

This second part of the algorithm makes it possible to process problems where :math:`f` is strongly nonlinear in 
a reasonable number of iterations, where the secant method would have converged too slowly. These cases of strong 
non-linearity are encountered in particular with the LEMAITRE model, for large values of :math:`n/m`.

.. _r5.03.08.4.3:

Calculation of the stress at the end of the current time step
--------------------------------------------------------------

According to :eq:`eq_r5.03.08.4.1-3`, if :math:`x` is the solution of the scalar equation, by posing:

.. math::

   b(x, \sigma_{\text{eq}}^e) = \frac{1}{1+3\mu \Delta t \frac{g(x, \lambda^{-} + \frac{1}{3\mu}(\sigma_{\text{eq}}^e - x), T)}{x}} = \frac{x}{\sigma_{\text{eq}}^e}

we have :

.. math::
   :label: eq_r5.03.08.4.3-1

   \tilde{\boldsymbol{\sigma}} = b(x, \sigma_{\text{eq}}^e) \tilde{\boldsymbol{\sigma}}^e

In the case where :math:`\sigma_{\text{eq}}^e = 0`, which is equivalent according to the scalar equation to 
:math:`x = 0`, we extend :math:`b` by continuity. For that, we set 

.. math::

   y(x) = \lambda^{-} + \frac{1}{3\mu} (\sigma_{\text{eq}}^e- x) 

and :math:`G(x) = g(x, y(x), T)`. 

The derivative of :math:`G` is expressed as a function of the partial derivatives of :math:`g` at the point 
:math:`(x, y(x), T)` :

.. math::

   G'(x) = \frac{\partial g}{\partial x} (x, y(x), T) - \frac{1}{3\mu} \frac{\partial g}{\partial y} (x, y(x), T)

The continuation of :math:`b` by continuity then gives:

.. math::

   b(0, 0) = \frac{1}{1 + 3\mu \Delta t G'(0)}

and we have, always in the case where :math:`\sigma _{\text{eq}}^e = 0`, :math:`\tilde{\boldsymbol{\sigma}} = 0`.

Once we have calculated :math:`\tilde{\boldsymbol{\sigma}}`, we obtain :math:`\boldsymbol{\sigma}` by the relation 
(:math:`K` is here assumed to be constant):

.. math::
   :label: eq_r5.03.08.4.3-2

   \boldsymbol{\sigma} = \boldsymbol{\sigma}^{-} + \Delta\boldsymbol{\sigma} = 
      \tilde{\boldsymbol{\sigma}} + \left(\frac{1}{3} \text{Tr}(\boldsymbol{\sigma}^{-}) + 
         K \text{Tr}(\Delta \boldsymbol{\varepsilon}) \right) \mathbf{I}_3

.. _r5.03.08.4.4:

Semi-implicit scheme
--------------------

In the case where :math:`g` does not depend on :math:`\lambda`, the implicit numerical scheme :eq:`eq_r5.03.08.4.1-2`
can be used for the computation of stress at the end of the time step for a given 
:math:`\Delta \boldsymbol{\varepsilon}_v`.
If the stress varies significantly over time (see [#bib2]_), the implicit scheme can lead to large errors.

To remedy that and to improve the solution, we discretize the flow rule in a semi-implicit manner:

.. math::
   :label: eq_r5.03.08.4.4-1

   \frac{\Delta \boldsymbol{\varepsilon}_v}{\Delta t} = 
     \frac{3}{2} g\left(\left(\boldsymbol{\sigma}^{-} + \frac{\Delta \boldsymbol{\sigma}}{2} \right)_{\text{eq}}, 
                        \lambda^{-} + \frac{(\Delta \boldsymbol{\varepsilon}_v)_{\text{eq}}}{2}, 
                        T^{-} + \frac{\Delta T}{2} \right)
       \frac{\tilde{\boldsymbol{\sigma}}^{-} + \frac{\Delta \tilde{\boldsymbol{\sigma}}}{2}} 
            {\left(\boldsymbol{\sigma}^{-} + \frac{\Delta \boldsymbol{\sigma}}{2}\right)_{\text{eq}}}

An economical way of transforming the fully implicit formulation :eq:`eq_r5.03.08.4.1-2`, is  to divide each member 
of equation [eq:`eq_r5.03.08.4.4-1` by 2:

.. math::

   \frac{\Delta \boldsymbol{\varepsilon}_v/2}{\Delta t} = 
     \frac{1}{2} 
       \left[\frac{3}{2} g\left(\left(\boldsymbol{\sigma}^{-} + \frac{\Delta \boldsymbol{\sigma}}{2} \right)_{\text{eq}}, 
                        \lambda^{-} + \frac{(\Delta \boldsymbol{\varepsilon}_v)_{\text{eq}}}{2}, 
                        T^{-} + \frac{\Delta T}{2} \right)
       \frac{\tilde{\boldsymbol{\sigma}}^{-} + \frac{\Delta \tilde{\boldsymbol{\sigma}}}{2}} 
            {\left(\boldsymbol{\sigma}^{-} + \frac{\Delta \boldsymbol{\sigma}}{2}\right)_{\text{eq}}}\right]


and to do the same thing with :eq:`eq_r5.03.08.4.1-1`]:

.. math::

   \frac{\Delta \tilde{\boldsymbol{\sigma}}}{2} = 2\mu \left(\frac{\Delta\tilde{\boldsymbol{\varepsilon}}}{2} - 
       \frac{\Delta\boldsymbol{\varepsilon}_v }{2}\right)

Note that this system has the same form as that of :eq:`eq_r5.03.08.4.1-1` and :eq:`eq_r5.03.08.4.1-2`, the data 
being :math:`\Delta \boldsymbol{\varepsilon}/2` instead of :math:`\Delta \boldsymbol{\varepsilon}`, the unknowns 
being respectively :math:`\Delta\boldsymbol{\sigma}/2` and :math:`\Delta \boldsymbol{\varepsilon}_v/2` instead of 
:math:`\Delta \boldsymbol{\sigma}` and :math:`\Delta \boldsymbol{\varepsilon}_v` and the function :math:`g/2` 
replacing function :math:`g`.

One can thus use the solution process from :numref:`r5.03.08.4.1` with :numref:`r5.03.08.4.3` as well as the 
corresponding algorithm by introducing :math:`\Delta \boldsymbol{\varepsilon}/2` and dividing the function :math:`g` 
by 2. It then remains to multiply the results :math:`\Delta\boldsymbol{\sigma}/2` and
:math:`\Delta \boldsymbol{\varepsilon}_v/2` by 2 to obtain the stress and viscous strain increments calculated by 
the semi-implicit scheme (the :math:`\Delta \boldsymbol{\sigma}` and the 
:math:`\Delta \boldsymbol{\varepsilon}_v` of equation :eq:`eq_r5.03.08.4.4-1`).

Note that the computation of the tangent operator is not affected by this modification of the numerical scheme. 
Indeed, we obviously have:

.. math::

   \frac{\partial\Delta\boldsymbol{\sigma}}{\partial\Delta\boldsymbol{\varepsilon}} =
     \frac{\partial(\Delta \boldsymbol{\sigma}/2)}{\partial(\Delta\boldsymbol{\varepsilon}/2)}


.. _r5.03.08.4.5:

Taking into account of the variation of the elastic coefficients with temperature
----------------------------------------------------------------------------------

If :math:`\mathbb{A}` is the elasticity tensor, we have:

.. math::

   \Delta\boldsymbol{\varepsilon} = \Delta\boldsymbol{\varepsilon}_v + \Delta (\mathbb{A}^{-1} \boldsymbol{\sigma})

with :

.. math::

   \Delta (\mathbb{A}^{-1} \boldsymbol{\sigma}) = \mathbb{A}^{-1}(T^{-} + \Delta T)
      (\boldsymbol{\sigma}^{-} + \Delta \boldsymbol{\sigma}) - \mathbb{A}^{-1}(T^{-}) \boldsymbol{\sigma}^{-}

This results in the equations of :numref:`r5.03.08.4.4` by:

.. math::

   & 2\mu \left(\frac{\Delta\tilde{\boldsymbol{\varepsilon}}}{2}\right) - 
   \left(\tilde{\boldsymbol{\sigma}}^{-} + \frac{\Delta\tilde{\boldsymbol{\sigma}}}{2}\right) = 
   3\mu\Delta t\,\, \\
   & \quad \quad
   \frac{g\left(\left(\boldsymbol{\sigma}^{-} + \frac{\Delta \boldsymbol{\sigma}}{2}\right)_{\text{eq}}, 
          \lambda^{-} + \frac{(\Delta\boldsymbol{\varepsilon}_v)_{\text{eq}}}{2}, 
          T^{-} + \frac{\Delta T}{2}\right)}{2}
   \frac{\left(\tilde{\boldsymbol{\sigma}}^{-} + \frac{\Delta \boldsymbol{\sigma}}{2}\right)}
        {\left(\boldsymbol{\sigma}^{-} + \frac{\Delta \boldsymbol{\sigma}}{2}\right)_{\text{eq}}} - \\
   & \quad\quad\quad \tilde{\boldsymbol{\sigma}}^{-} \left(\frac{2\mu^{-} + 2\mu}{4\mu^{-}}\right)

By setting :

.. math::

   \tilde{\boldsymbol{\sigma}}^e = \left(\frac{2\mu^{-} + 2\mu}{4\mu^{-}}\right) 
       \tilde{\boldsymbol{\sigma}}^{-} + 2\mu \left(\frac{\Delta \tilde{\boldsymbol{\varepsilon}}}{2}\right)

and

.. math::

   \text{Tr}(\boldsymbol{\sigma}^e) = \left(\frac{3K^{-} +3K}{6K^{-}}\right) \text{Tr}(\boldsymbol{\sigma}^{-}) +
     3K \text{Tr}\left(\frac{\Delta\boldsymbol{\varepsilon}}{2}\right)

we can reproduce the preceding case :numref:`r5.03.08.4.4`.

.. _r5.03.08.5:

Calculation of the tangent operator
===================================

In the case where :math:`\sigma_{\text{eq}}^e = 0` and :math:`x = 0`, one takes the elasticity tensor as the
tangent operator.

If not, one obtains this operator by differentiating equation :eq:`eq_r5.03.08.4.3-1` with respect to
:math:`\Delta\boldsymbol{\varepsilon}` :

.. math::

  \frac{\partial \tilde{\boldsymbol{\sigma}}}{\partial\boldsymbol{\varepsilon}} =
  \frac{\partial\Delta\tilde{\boldsymbol{\sigma}}}{\partial\Delta\boldsymbol{\varepsilon}} =
  \frac{\partial b(x, \sigma_{\text{eq}}^e)}{\partial\Delta\boldsymbol{\varepsilon}} \tilde{\boldsymbol{\sigma}}^e 
  + b(x, \sigma_{\text{eq}}^e) \frac{\partial\tilde{\boldsymbol{\sigma}}^e}{\partial\Delta\boldsymbol{\varepsilon}}

then by also differentiating :eq:`eq_r5.03.08.4.3-2` with respect to :math:`\Delta\boldsymbol{\varepsilon}` :

.. math::

  \frac{\partial\Delta\boldsymbol{\sigma}}{\partial\Delta\boldsymbol{\varepsilon}} =
  \frac{\partial\Delta\tilde{\boldsymbol{\sigma}}}{\partial\Delta\boldsymbol{\varepsilon}} +
  K \mathbf{I}_3 \frac{\partial \text{Tr}(\Delta\boldsymbol{\varepsilon})}{\partial\Delta\boldsymbol{\varepsilon}} =
  \frac{\partial\Delta\tilde{\boldsymbol{\sigma}}}{\partial\Delta\boldsymbol{\varepsilon}} +
  K \mathbf{I}_3^{'} \mathbf{I}_3

It will be noted that, in these equations, tensors of order 2 and order 4 are represented by vectors and matrices,
respectively. :math:`\mathbf{I}_3` is here a tensor of order 2, expressed as a vector:t 

.. math::

   \mathbf{I}_3 = \begin{bmatrix} 1 & 1 & 1 & 0 & 0 & 0 \end{bmatrix}^T

We also have:

.. math::

   \frac{\partial b(x, \sigma_{\text{eq}}^e)}{\partial\Delta\boldsymbol{\varepsilon}} =
   \frac{\partial b}{\partial x} \frac{\partial x}{\partial\Delta\boldsymbol{\varepsilon}} +
   \frac{\partial b}{\partial\sigma_{\text{eq}}^e} \frac{\partial\sigma_{\text{eq}}^e}{\partial\Delta\boldsymbol{\varepsilon}}

It is therefore necessary to calculate :math:`\frac{\partial x}{\partial\Delta\boldsymbol{\varepsilon}}`. For 
that, one implicitly derives the scalar equation with respect to :math:`\Delta\boldsymbol{\varepsilon}`.

For simplicity, we will omit hereafter in writing :math:`g` and its derivatives, the parameter :math:`T`.

We then have:

.. math::

   [3\mu\Delta t G^{'}(x) + 1] \frac{\partial x}{\partial\Delta\boldsymbol{\varepsilon}} +
     \Delta t \frac{\partial g}{\partial y}(x,y) \frac{\partial\sigma_{\text{eq}}^e}{\partial\Delta\boldsymbol{\varepsilon}} =
   \frac{\partial\sigma_{\text{eq}}^e}{\partial\Delta\boldsymbol{\varepsilon}}

From where :

.. math::

   \frac{\partial x}{\partial\Delta\boldsymbol{\varepsilon}} & =
     \frac{1 - \Delta t \frac{\partial g}{\partial y}}{1+3\mu\Delta t G^{'}(x)} 
        \frac{\partial\sigma_{\text{eq}}^e}{\partial\Delta\boldsymbol{\varepsilon}} \\
   & =
     \frac{1 - \Delta t \frac{\partial g}{\partial y}}{1+3\mu\Delta t G^{'}(x)} 
        \frac{3\mu}{\sigma_{\text{eq}}^e}\tilde{\boldsymbol{\sigma}}^e 

with the expression for :math:`G^{'}(x)` obtained with :numref:`r5.03.08.4.3`.

We finally obtain the following expression of the tangent operator:

.. math::

   \frac{\partial\Delta\boldsymbol{\sigma}}{\partial\Delta\boldsymbol{\varepsilon}} = 
     K \mathbf{I}_3^T \mathbf{I}_3 + 2\mu [ \gamma (\tilde{\boldsymbol{\sigma}}^e)^T \tilde{\boldsymbol{\sigma}}^e + 
     b(x,\sigma_{\text{eq}}^e) \mathbb{A}]

with

.. math::

   \mathbb{A} = \frac{\partial\Delta\tilde{\boldsymbol{\varepsilon}}}{\partial\Delta\boldsymbol{\varepsilon}} = 
     \mathbf{J}_6 - \frac{1}{3} \mathbf{I}_3^T \mathbf{I}_3

where :math:`\mathbf{J}_6` is the rank 6 identity matrix,

.. math::

   \gamma = \frac{3}{2 (\sigma_{\text{eq}}^e)^3} 
     \left[ \sigma_{\text{eq}}^e \frac{1 - \Delta t\frac{\partial g}{\partial y}}{1+3\mu\Delta t G^{'}(x)}
            - x \right]

.. admonition:: Remark 

   In the case of the :blue:`VISC_IRRA_LOG` model, one easily checks that:

   .. math::

      & G'(x) = \frac{1}{f_1' g_1+ f_2' g_2} 
              \left[ 
        g_1 g_1' ( (f_1')^2 - f_1 f_1'' ) + g_2 g_2' ( (f_2')^2 - f_2 f_2'' ) + \right.\\
      & \qquad \left.  g_1 g_2' ( f_1' f_2' - f_1'' f_2 ) + g_2 g_1' ( f_1' f_2' - f_1 f_2'' ) -
        \frac{1}{3m} ( f_1'' g_1 + f_2'' g_2 ) \right] \\
      & \frac{\partial g}{\partial\lambda} (x, y, T) =
        \frac{f_1'' g_1+ f_2'' g_2}{f_1' g_1 + f_2' g_2}

   where :math:`f_1, f_1', f_1'', f_2, f_2', f_2''` denote the values of :math:`f_1` and :math:`f_2` and 
   their derivatives at point :math:`t(x, y, T)` and where :math:`g_1, g_1', g_2, g_2'` denote the values 
   of :math:`g_1` and :math:`g_2` and their derivatives with respect to :math:`\sigma` at the point 
   :math:`(x, T)` (see [#bib1]_)

.. _r5.03.08.6:

Bibliography
============

.. [#bib1] de BONNIERES P.: Writing in generalized standard form of the viscoplastic constitutive laws
           for Zircaloy, note EDF-DER HI-71/7940-Index A, 1992

.. [#bib2] de BONNIERES P., ZIDI M.: Introduction of viscoplasticity in the thermomechanical modulus
           of Cyrano3: principle, description and validation, EDF-DER HI- note71/8334, 1993.

