# Cube composed of two PENTA6 elements

import sys
import salome
import salome_notebook
from salome.smesh import smeshBuilder
import SMESH, SALOMEDS

ExportPATH="/home/banerjee/Salome/ssnp14/"

# Initialize
salome.salome_init()
notebook = salome_notebook.NoteBook()
smesh = smeshBuilder.New()

# Create mesh
mesh = smesh.Mesh()
smesh.SetName(mesh, 'mesh')

# Add nodes
node1 = mesh.AddNode( 0, 1, 0 )
node2 = mesh.AddNode( 1, 1, 0 )
node3 = mesh.AddNode( 0, 0, 0 )
node4 = mesh.AddNode( 1, 0, 0 )
node5 = mesh.AddNode( 0, 1, 1 )
node6 = mesh.AddNode( 1, 1, 1 )
node7 = mesh.AddNode( 0, 0, 1 )
node8 = mesh.AddNode( 1, 0, 1 )

# Add groups for the nodes
node_2 = mesh.CreateEmptyGroup( SMESH.NODE, 'node_2' )
node_4 = mesh.CreateEmptyGroup( SMESH.NODE, 'node_4' )
node_6 = mesh.CreateEmptyGroup( SMESH.NODE, 'node_6' )
node_8 = mesh.CreateEmptyGroup( SMESH.NODE, 'node_8' )
nbAdd = node_2.Add( [ 2 ] )
nbAdd = node_4.Add( [ 4 ] )
nbAdd = node_6.Add( [ 6 ] )
nbAdd = node_8.Add( [ 8 ] )

# Add volume elements
vol1 = mesh.AddVolume( [ 3, 4, 2, 7, 8, 6 ] )
vol2 = mesh.AddVolume( [ 3, 2, 1, 7, 6, 5 ] )

# Add group for the volumes
cube = mesh.CreateEmptyGroup( SMESH.VOLUME, 'cube' )
nbAdd = cube.Add( [ 1, 2 ] )
penta1 = mesh.CreateEmptyGroup( SMESH.VOLUME, 'penta1' )
nbAdd = penta1.Add( [ 1 ] )
penta2 = mesh.CreateEmptyGroup( SMESH.VOLUME, 'penta2' )
nbAdd = penta2.Add( [ 2 ] )

# Add face elements
face3 = mesh.AddFace( [ 1, 3, 7, 5 ] )
face4 = mesh.AddFace( [ 2, 6, 8, 4 ] )
face5 = mesh.AddFace( [ 5, 6, 2, 1 ] )
face6 = mesh.AddFace( [ 3, 4, 8, 7 ] )
face7 = mesh.AddFace( [ 7, 8, 6, 5 ] )
face8 = mesh.AddFace( [ 3, 1, 2, 4 ] )

# Add groups for the faces
left_face = mesh.CreateEmptyGroup( SMESH.FACE, 'left_face' )
right_face = mesh.CreateEmptyGroup( SMESH.FACE, 'right_face' )
top_face = mesh.CreateEmptyGroup( SMESH.FACE, 'top_face' )
bot_face = mesh.CreateEmptyGroup( SMESH.FACE, 'bot_face' )
front_face = mesh.CreateEmptyGroup( SMESH.FACE, 'front_face' )
back_face = mesh.CreateEmptyGroup( SMESH.FACE, 'back_face' )
nbAdd = left_face.Add( [ 3 ] )
nbAdd = right_face.Add( [ 4 ] )
nbAdd = top_face.Add( [ 5 ] )
nbAdd = bot_face.Add( [ 6 ] )
nbAdd = front_face.Add( [ 7 ] )
nbAdd = back_face.Add( [ 8 ] )

# Export mesh
mesh.ExportMED( r''+ExportPATH+'ssnp14b.mmed'+'')

# Update GUI object tree
if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()
