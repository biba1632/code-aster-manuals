.. role:: red
.. role:: blue
.. role:: green
.. role:: gray
.. role:: python(code)
   :language: python
.. |br| raw:: html

   <br/>

.. index:: ! ASSE_VECT_GENE

.. _u4.65.05:

************************************************************************
**[U4.65.05]** : Operator ``ASSE_VECT_GENE``
************************************************************************

.. _u4.65.05.1:

Purpose
=======

Project the loads on to the modal basis of a substructure.

Within the framework of a harmonic calculation using the methods of substructuring, the operator
:red:`ASSE_VECT_GENE` carries out the projection of the loads of type :green:`cham_no_DEPL_R` resulting from
:red:`ASSE_VECTEUR` :ref:`[U4.61.23] <u4.61.23>`, on the modal basis of the substructure defined by
:red:`DEFI_BASE_MODALE` :ref:`[U4.64.02] <u4.64.02>`. The generalized vectors thus obtained are assembled
from the definition of the generalized model resulting from :red:`DEFI_MODELE_GENE` :ref:`[U4.65.02] <u4.65.02>`.
The final assembled generalized vector is built using the numbering scheme of the generalized degrees of freedom
established previously by the operator :red:`NUME_DDL_GENE` :ref:`[U4.65.03] <u4.65.03>`.

The output concept produced by this operator is of type :green:`vect_asse_gene`.


.. _u4.65.05.2:

Syntax
======

::

  vect_gene [vect_asse_gene] = ASSE_VECT_GENE 
   (

    ♦ NUME_DDL_GENE = nu_gene,                                [nume_ddl_gene]
    ◊ METHODE = / 'CLASSIQUE'‚                                [DEFAULT]
                / 'INITIAL'‚

    # If METHODE = 'CLASSIQUE':

      ♦ CHAR_SOUS_STRUC = _F(
                              ♦ SOUS_STRUC = 'nom_sstruc',    [Kn]
                              ♦ VECT_ASSE = vecas,            [cham_no_DEPL_R]
                            )
   )

.. _u4.65.05.3:

Operands
========


.. _u4.65.05.3.1:

Operand :blue:`NUME_DDL_GENE`
-----------------------------

::

   ♦ NUME_DDL_GENE = nu_gene

Name of the concept of type :green:`nume_ddl_gene` resulting from the operator :red:`NUME_DDL_GENE`
:ref:`[U4.65.03] <u4.65.03>` which defines the numbering of degrees of freedom to be used for the
assembled generalized vector.

.. _u4.65.05.3.2:

Operand :blue:`METHODE`
-----------------------

::

   ◊ METHODE = 'CLASSIQUE',
               'INITIAL'

Type of method used for the assembly of the matrices. The :blue:`'INITIAL'` method initializes a null vector
of type :green:`vect_asse_gene` which one can fill with Python methods. This method was created for development.

.. _u4.65.05.3.3:

Keyword :blue:`CHAR_SOUS_STRUC`
-------------------------------

::

   ♦ CHAR_SOUS_STRUC

Keyword factor to define the loads applied to the structure. The definition of loads is done by the data of
the assembled vector which is associated with it and the name of the substructure on which it is applied.

.. _u4.65.05.3.3.1:

Operand :blue:`SOUS_STRUC`
^^^^^^^^^^^^^^^^^^^^^^^^^^

::

   ♦ SOUS_STRUC = 'struc_name'

Name of the substructure on which the load is applied. It must have been defined before by the operator
:red:`DEFI_MODELE_GENE` :ref:`[U4.65.02] <u4.65.02>`.

.. _u4.65.05.3.3.2:

Operand :blue:`VECT_ASSE`
^^^^^^^^^^^^^^^^^^^^^^^^^^

::

   ♦ VECT_ASSE = vecas

Name of the concept of type :green:`cham_no_DEPL_R` resulting from :red:`ASSE_VECTEUR`
:ref:`[U4.61.23] <u4.61.23>` which defines the spatial distributio of the loads applied to the substructure.

