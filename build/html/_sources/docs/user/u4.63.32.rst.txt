.. role:: red
.. role:: blue
.. role:: green
.. role:: gray
.. role:: python(code)
   :language: python
.. |br| raw:: html

   <br/>

.. index:: ! REST_SOUS_STRUCT

.. _u4.63.32:

************************************************************************
**[U4.63.32]** : Operator ``REST_SOUS_STRUC``
************************************************************************

.. _u4.63.32.1:

Purpose
=======

Converts results computed in generalized coordinates into values expressed in the the physical basis.
dinates.

Results can be computed in a generalized coordinate system by:

* methods of modal superposition that produce a concept of type :green:`tran_gene`
* methods of cyclic sub-structuring (:green:`mode_cycl`)
* dynamic sub-structuring (:green:`mode_gene`) 
* harmonic sub-structuring (:green:`harm_gene`) 

This operator can be used to restore results of the above types to the initial physical coordinate 
basis.

The output (product) concept can be of the following types:

* :green:`dyna_trans`: if the generalized results are from a modal superposition computation
  or from the extrapolation of the results of experimental measurements on a numerical model
  (the input concept is of type :green:`tran_gene`) or from the projection of a 
  :green:`dyna_trans` comcept on a new visualization mesh,
* :green:`mode_meca`: if the input concept to this operator is produced by
  cyclic sub-structuring (:green:`mode_cycl`), 
  dynamic sub-structuring (:green:`mode_gene`) or if computation corresponds to the projection 
  of a :green:`mode_meca` concept on a new visualization mesh,
* :green:`dyna_harmo`: if the input concept is produced from harmonic sub-structuring 
  (:green:`harm_gene`)
* :green:`evol_noli`: if the input concept is produced by  a nonlinear computation combined
   with a modal computation on its nonlinear part.

 
.. _u4.63.32.2:

Syntax
======

::

  resphy [*] = REST_SOUS_STRUC                      

    (

      ♦ / RESULTAT = resu,                      [mode_meca]
                                                [dyna_trans]
                                                [evol_noli]
        / RESU_GENE = tg,                     / [tran_gene]
                                              / [mode_cycl]
                                              / [mode_gene]
                                              / [harm_gene]

      ◊ MODE_MECA = mode,                       [mode_meca]

      ◊ NUME_DDL = num_dof,                     [nume_ddl]

      ◊ / TOUT_ORDRE = 'OUI',
        / NUME_ORDRE = num,                     [l_I]
        / NUME_MODE = num,                      [l_I]
        / TOUT_INST = 'OUI',
        / LIST_INST = list,                     [listr8]
        / INST = inst,                          [l_R]
        / FREQ = freq,                          [l_R]
        / LIST_FREQ = list,                     [listr8]

      ◊ / TOUT_CHAM = 'OUI',
        / NOM_CHAM = (| 'DEPL',
                      | 'VITE',
                      | 'ACCE',                 [DEFAULT]
                      | 'ACCE_ABSOLU',
                      | 'EFGE_ELNO',
                      | 'SIPO_ELNO',
                      | 'SIGM_ELNO',
                      | 'FORC_NODA',),

      ◊ INTERPOL = /'LIN',
                   /'NON',                      [DEFAULT]

      ◊ CRITERE = / 'ABSOLU',
                  / 'RELATIF,                   [DEFAULT]

      ◊ PRECISION = /prec ,                     [R]
                    /1.E-06,                    [DEFAULT]

      ◊ / SECTEUR = numsec,                     [I]
        / SQUELETTE = skeleton,                 [squelette]
        / SOUS_STRUC = name_struct,             [Kn]

      ◊ / GROUP_NO = lgrno,                     [l_co]
        / GROUP_MA = lgrma,                     [l_co]

      ◊ CYCLIQUE = _F(

                      ◊ NB_SECTEUR = ns         [I]
                      ◊ NUME_DIAMETRE = nl      [I]
                      ◊ RESULTAT2 = res         [evol_elas]
                                                [evol_noli]
                                                [dyna_trans]
                                                [evol_char]
                                                [mode_meca]
                    )
      ◊ TITRE = title,                          [l_Kn]
    )


**Output concept types**

* If :blue:`RESU_GENE` is of type :green:`tran_gene` then :green:`[*] = dyna_trans`
* If :blue:`RESU_GENE` is of type :green:`mode_cycl` then :green:`[*] = mode_meca`
* If :blue:`RESU_GENE` is of type :green:`mode_gene` then :green:`[*] = mode_meca`
* If :blue:`RESU_GENE` is of type :green:`harm_gene` then :green:`[*] = dyna_harmo`
* If :blue:`RESULTAT` is of type :green:`mode_meca` then :green:`[*] = mode_meca`
* If :blue:`RESULTAT` is of type :green:`evol_noli` then :green:`[*] = evol_noli`
* If :blue:`RESULTAT` is of type :green:`dyna_trans` then :green:`[*] = dyna_trans`


.. _u4.63.32.3:

Operands
========

.. _u4.63.32.3.1:

Operands :blue:`RESU_GENE/RESULTAT`
-----------------------------------

::

  ♦ / RESU_GENE = tg

Alternatives:

* concept of type :green:`tran_gene` containing (for various times) generalized vectors of
  displacement, velocity and acceleration from the transient response calculated by sub-structuring.
* concept of type :green:`mode_cycl` containing generalized vectors of the modes calculated
  by cyclic sub-structuring.
* concept of type :green:`mode_gene` containing generalized vectors of modes calculated by
  dynamic sub-structuring.
* concept of type :green:`harm_gene` containing generalized vectors of displacement, velocity and
  acceleration from the harmonic response of a structure calculated by sub-structuring

::

  ♦ / RESULTAT = resu

This keyword can be used when initially restoring a result from a dynamic sub-structuring
calculation on a skeleton (visualization mesh). In that case, ``Code_Aster`` defines an enriched
skeleton where all sub-structure interface nodes have been merged and all or part of the node or
element groups of the initial mesh have been recovered. The keyword :blue:`RESULTAT` then corresponds
to the projection of a :green:`mode_meca / dyna_trans / evol_noli` concept on a new enriched
skeleton (see example in :numref:`u4.63.32.4`).

.. _u4.63.32.3.2:

Operand :blue:`MODE_MECA`
-------------------------

::

  ◊ MODE_MECA = mode                       [mode_meca]

Concept of type :green:`mode_meca` containing a basis of eigen modes obtained by dynamic sub-structuring.

This operand is used to convert, into the physical basis, the result of a transient computation carried
out on the modal basis by dynamic sub-structuring. The modes contained in the concept :green:`mode_meca`
should have been obtained by a previous :blue:`REST_SOUS_STRUC`.

In this case, it is necessary to provide the same support (keyword :blue:`SKELETON` or :blue:`SOUS_STRUC`)
used for the conversion of the modal basis into the physical basis.

.. _u4.63.32.3.3:

Operand :blue:`NUME_DDL` :gray:`(NUM_DOF)`
------------------------------------------

::

  ◊ NUME_DDL = num_dof,                     [nume_ddl]

Concept of the :green:`nume_ddl` type containing a numbering corresponding to a reduced model in
the case of a calculation with dynamic condensation when the user wishes a conversion into the physical
basis using the degrees of freedom belonging to this reduced model.

This operand thus makes it possible to obtain, following the conversion, a :green:`mode_meca`
concept which can be used subsequently for other calculations.

.. _u4.63.32.3.4:

Operands :blue:`TOUT_ORDRE / NUME_ORDRE / TOUT_INST / LIST_INST / INST`
-----------------------------------------------------------------------

::
   
  ◊ / TOUT_ORDRE = 'OUI'

To restore (convert into physical coordinates) using **all the modes** of the
concept :green:`mode_cycl` or :green:`mode_gene`.

::

    / NUME_ORDRE = num

List of integers containing the numbers of the modes on which the restoration takes place.

::

    / NUME_MODE = num

List of integers containing the numbers of the modes in the total spectrum used for restoration.

::

    / TOUT_INST = 'OUI'

If one wishes to restore using **all times** contained in the generalized result (:green:`tran_gene`).

::

    / LIST_INST = list

List of increasing real numbers of the :green:`listr8` type containing the times for which one wishes
to compute the restoration.

::

    / INST = inst

List of real numbers containing the times on which the restoration takes place.

For a transient computation, it is checked that the times requested by the option :blue:`LIST_INST` are
inside the range of definition of :green:`tran_gene`.

The results at any time can be obtained by linear interpolation between the results of two times of
computation actually contained in the :green:`tran_gene`.

.. _u4.63.32.3.5:

Operands :blue:`FREQ / LIST_FREQ`
---------------------------------

These operands are used in the case of a restoration into physical coordinates of generalized
harmonic calculations (:green:`harm_gene`).

::

   / FREQ = freq

Frequency at which one wishes to restore the harmonic calculation

::

   / LIST_FREQ = list

List of real numbers containing the frequencies for which one wishes to carry out the restoration.
For each indicated frequency, one restores the data closest to the calculated frequency.
There is no interpolation.

.. _u4.63.32.3.6:

Operands :blue:`TOUT_CHAM / NOM_CHAM`
-------------------------------------

::

  ◊ / TOUT_CHAM = 'OUI'

Allows to restore the fields of symbolic name :blue:`DEPL`, :blue:`VITE` and :blue:`ACCE` 
(:gray:`DISPLACEMENT,VELOCITY,ACCELERATION`) contained in the generalized result
(:green:`tran_gene, harm_gene`).

::

    /NOM_CHAM = field_name

List of the symbolic names of field which one wishes to restore: :blue:`DEPL`, :blue:`VITE`, :blue:`ACCE` etc.

.. _u4.63.32.3.7:


Operand :blue:`INTERPOL`
-------------------------

::

  ◊ INTERPOL = 'LIN'

An interpolation is authorized between two times; this interpolation should not be used between two times
of calculation and can lead to errors if both times **[U4.53.21]** are separated by a long interval
relative to the periods of the phenomena studied.

::

  ◊ INTERPOL = 'NON'

The restoration must be strict (no interpolation).

.. _u4.63.32.3.8:

Operands :blue:`PRECISION / CRITERE`
------------------------------------

::

  ◊ PRECISION = prec
  ◊ CRITERE = / 'ABSOLU',
              / 'RELATIF,                   [DEFAULT]

When :blue:`INTERPOL` is :blue:`'NON'`, the criterion indicates the precision of the search for the
instant to restore:

* :blue:`'ABSOLU'` : search interval *[Inst - prec, Inst + prec]*,
* :blue:`'RELATIF`:  search interval *[(1 - prec) . Inst, (1 + prec) . Inst]* 

*Inst* being the moment of restitution.

.. _u4.63.32.3.9:

Operand :blue:`SECTEUR`
-----------------------

::

  ◊ / SECTEUR

Sector number of the cyclic structure on which the result (of type :green:`mode_cycl`) will be
restored in the physical basis in cyclic sub-structuring.

.. _u4.63.32.3.10:

Operand :blue:`SQUELETTE` :gray:`(SKELETON)`
--------------------------------------------

::

  / SQUELETTE

Name of the mesh skeleton of the total structure on which the result will be restored: see
the operator :blue:`DEFI_SQUELETTE` :ref:`[U4.24.01] <u4.24.01>`.

.. _u4.63.32.3.11:

Operand :blue:`SOUS_STRUC` :gray:`(SUB_STRUCTURE)`
---------------------------------------------------

::

  / SOUS_STRUC = name_struct

Name of the substructure on which the result will be restored: see the operator
:blue:`DEFI_MODELE_GENE` :ref:`[U4.65.02] <u4.65.02>`.

      
.. _u4.63.32.3.12:


Operands :blue:`GROUP_NO / GROUP_MA`
------------------------------------

::

   ◊ / GROUP_NO = lgrno
     / GROUP_MA = lgrma

After a computation of transient dynamics on modal basis, the user can restore kinematic fields
on a part only at the nodes or elements of the mesh.

List of the node / element groups corresponding to the places where the user wants to restore
fields.

.. _u4.63.32.3.13:

Operand :blue:`CYCLIQUE`
------------------------

::

   ◊ CYCLIQUE = _F(
                   ◊ NB_SECTEUR = ns         [I]
                   ◊ NUME_DIAMETRE = nl      [I]
                   ◊ RESULTAT2 = res         [evol_elas]
                                             [evol_noli]
                                             [dyna_trans]
                                             [evol_char]
                                             [mode_meca]
                  )

The operator :blue:`DEFI_SQUELETTE` :ref:`[U4.24.01] <u4.24.01>` 
allows us to regenerate the complete mesh of a
structure with cyclic symmetry starting from the mesh of a sector of this structure.

The option :blue:`CYCLIQUE` in :red:`REST_SOUS_STRUC` makes it possible to restore on this new mesh
skeleton the nodal fields calculated on the model of a single sector and taking into account of cyclic
symmetry (with :blue:`LIAISON_MAIL` or :blue:`LIAISON_CYCL` for example).

The action of this option of :red:`REST_SOUS_STRUC` consists of:

1. in the static case (only one nodal field) to copy this field to the nodes of the skeleton mesh
2. in the dynamic case (2 nodal fields specified with :blue:`RESULTAT` and :blue:`RESULTAT2`)
   to combine the nodal fields and to copy them to the new mesh.              

.. _u4.63.32.3.14:

Operand :blue:`TITRE`
---------------------

::

  ◊ TITRE = title

Title attached to the concept produced by this operator **[U4.03.01]**

.. _u4.63.32.4:

Example: Restoration of a :green:`mode_meca` on an enriched skeleton
====================================================================

The keyword :blue:`RESULTAT` is used in this example. The command corresponds to that used
the test case ``SDLS106A``.

* First stage: computation of the generalized eigen modes (obtained by dynamic sub-structuring) of
  the overall structure

  ::

     res_gen = CALC_MODES(MATR_RIGI = stif_gen,
                          MATR_MASS = mass_gen,
                          OPTION = 'PLUS_PETITE',
                          CALC_FREQ = _F(NMAX_FREQ = 6))

* Definition of a visualization mesh for the calculated quantities

  ::

     skel_1 = DEFI_SQUELETTE(MODEL_GENE = mode_gen,
                             SOUS_STRUC = _F(NAME = 'square1',
                                             TOUT = 'OUI'),
                             SOUS_STRUC = _F(NAME = 'square2',
                                             TOUT = 'OUI'))

* Restoration of :green:`mode_gene` on this skeleton:

  ::

     mode_v1 = REST_SOUS_STRUC(RESU_GENE = res_gen,
                               SQUELETTE = skel_1,
                               TOUT_ORDRE = 'OUI',
                               TOUT_CHAM = 'OUI')

* Definition of the enriched skeleton in which nodes at interfaces of the sub-structures have been
  merged and all or part of the node/element groups of the initial mesh have been recovered:

  ::

     skel_2 = DEFI_SQUELETTE(MODEL_GENE = mode_gen,
                             SQUELETTE = skel_1,
                             RECO_GLOBAL = _F(TOUT = 'OUI',
                                              DIST_REFE = 0.1))

* Restoration of the first :green:`mode_meca` on the new skeleton:

  ::

     mode_v2 = REST_SOUS_STRUC(SQUELETTE = skel_2,
                               RESULTAT = mode_v1)
