import salome
from salome.geom import geomBuilder
geompy = geomBuilder.New()

import SMESH
from salome.smesh import smeshBuilder
smesh = smeshBuilder.New()

from salome.StdMeshers import StdMeshersBuilder
from salome.NETGENPlugin import NETGENPluginBuilder

ExportPATH="/home/banerjee/Salome/forma03a/"

#---------------------------------
#  Geometry
#---------------------------------

# Create Sketcher 2D and add geometry
sk = geompy.Sketcher2D()
sk.addPoint(10.000000, 0.000000)
sk.addArcRadiusAbsolute(0.000000, 10.000000, 10.000000, 0)
sk.addSegmentAbsolute(0.000000, 150.000000)
sk.addSegmentAbsolute(100.000000, 150.000000)
sk.addSegmentAbsolute(100.000000, 0.000000)
sk.close()

# Add the wire sketch: marker (O(1,2,3), OX(1,2,3), OY(1,2,3))
geom_obj = geompy.MakeMarker(0, 0, 0, 1, 0, 0, 0, 1, 0)
plate_boundary = sk.wire(geom_obj)

# Create the face
plate = geompy.MakeFaceWires([plate_boundary], 1)

# Create geometry groups
[hole, left, bottom, top, right] = geompy.SubShapeAllSorted(plate, geompy.ShapeType["EDGE"])

pt_A = geompy.MakeVertex(0, 10, 0)
pt_B = geompy.MakeVertex(10, 0, 0)
pt_D = geompy.MakeVertex(100, 0, 0)
pt_F = geompy.MakeVertex(100, 150, 0)
pt_G = geompy.MakeVertex(0, 150, 0)

vert_A = geompy.GetVertexNearPoint(plate, pt_A)
vert_B = geompy.GetVertexNearPoint(plate, pt_B)
vert_D = geompy.GetVertexNearPoint(plate, pt_D)
vert_F = geompy.GetVertexNearPoint(plate, pt_F)
vert_G = geompy.GetVertexNearPoint(plate, pt_G)

plate_id = geompy.addToStudy(plate, "plate")

geompy.addToStudyInFather(plate, hole,   "hole")
geompy.addToStudyInFather(plate, left,   "left")
geompy.addToStudyInFather(plate, right,  "right")
geompy.addToStudyInFather(plate, bottom, "bottom")
geompy.addToStudyInFather(plate, top,    "top")
geompy.addToStudyInFather(plate, vert_A, "A")
geompy.addToStudyInFather(plate, vert_B, "B")
geompy.addToStudyInFather(plate, vert_D, "D")
geompy.addToStudyInFather(plate, vert_F, "F")
geompy.addToStudyInFather(plate, vert_G, "G")

#---------------------------------
#  Mesh
#---------------------------------

# Create the mesh object
plate_mesh = smesh.Mesh(plate)

# Set up Netgen parameters
NETGEN_2D = plate_mesh.Triangle(algo = smeshBuilder.NETGEN)
mesh_param = NETGEN_2D.Parameters()
mesh_param.SetFineness( 5 )
mesh_param.SetMaxSize( 10 )
mesh_param.SetNbSegPerEdge( 3.5 )
mesh_param.SetNbSegPerRadius( 6.5 )
mesh_param.SetMinSize( 0.7 )
mesh_param.SetSecondOrder( 1 )
mesh_param.SetOptimize( 3 )
mesh_param.SetQuadAllowed( 0 )
mesh_param.SetLocalSizeOnShape(hole, 18)
mesh_param.SetGrowthRate( 0.15 )

# Compute the mesh
isDone = plate_mesh.Compute()

# Tranfer the geometry groups to the mesh
hole_grp   = plate_mesh.GroupOnGeom(hole,   'hole',   SMESH.EDGE)
left_grp   = plate_mesh.GroupOnGeom(left,   'left',   SMESH.EDGE)
right_grp  = plate_mesh.GroupOnGeom(right,  'right',  SMESH.EDGE)
bottom_grp = plate_mesh.GroupOnGeom(bottom, 'bottom', SMESH.EDGE)
top_grp    = plate_mesh.GroupOnGeom(top,    'top',    SMESH.EDGE)

A_grp = plate_mesh.GroupOnGeom(vert_A, 'A', SMESH.NODE)
B_grp = plate_mesh.GroupOnGeom(vert_B, 'B', SMESH.NODE)
D_grp = plate_mesh.GroupOnGeom(vert_D, 'D', SMESH.NODE)
F_grp = plate_mesh.GroupOnGeom(vert_F, 'F', SMESH.NODE)
G_grp = plate_mesh.GroupOnGeom(vert_G, 'G', SMESH.NODE)

# Set object names
smesh.SetName(plate_mesh.GetMesh(), 'plate')

smesh.SetName(hole_grp,   'hole')
smesh.SetName(left_grp,   'left')
smesh.SetName(right_grp,  'right')
smesh.SetName(bottom_grp, 'bottom')
smesh.SetName(top_grp,    'top')

smesh.SetName(A_grp, 'A')
smesh.SetName(B_grp, 'B')
smesh.SetName(D_grp, 'D')
smesh.SetName(F_grp, 'F')
smesh.SetName(G_grp, 'G')

# Export the mesh
plate_mesh.ExportMED( r''+ExportPATH+'forma03a.mmed'+'')

# Update the object tree in the GUI
if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()
