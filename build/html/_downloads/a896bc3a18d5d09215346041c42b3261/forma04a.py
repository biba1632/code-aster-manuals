import sys

import salome
import GEOM
import math
from salome.geom import geomBuilder

import SMESH, SALOMEDS
from salome.smesh import smeshBuilder
from salome.StdMeshers import StdMeshersBuilder

ExportPATH="/home/banerjee/Salome/forma04a/"

# Start salome
salome.salome_init()

#--------------------------------------------
# Create geometry
#--------------------------------------------
geompy = geomBuilder.New()

# Create coordinate system
O = geompy.MakeVertex(0, 0, 0)
OX = geompy.MakeVectorDXDYDZ(1, 0, 0)
OY = geompy.MakeVectorDXDYDZ(0, 1, 0)
OZ = geompy.MakeVectorDXDYDZ(0, 0, 1)

# Ball radius
rad = 50
angle = math.pi / 2.0

# Create sketcher and add geometry
sk = geompy.Sketcher2D()
sk.addPoint(0.000000, 0.000000)
sk.addArcAngleRadiusLength(0, 50.000000, 45.000000)
sk.addArcAngleRadiusLength(0, 50.000000, 45.000000)
sk.addSegmentAbsolute(0.000000, 50.000000)
sk.close()

# Add the wire
coord_sys = geompy.MakeMarkerPntTwoVec(O, OX, OY)
sphere_boundary = sk.wire(coord_sys)

# Add the sphere
sphere = geompy.MakeFaceWires([sphere_boundary], 1)

# Create a mirrored copy of the sphere
sphere_mirror =  geompy.MakeMirrorByAxis(sphere, OX)

# Create an assembly
assembly = geompy.MakeCompound([sphere, sphere_mirror])

# Create geometry groups
top_sphere = geompy.CreateGroup(assembly, geompy.ShapeType["FACE"])
bot_sphere = geompy.CreateGroup(assembly, geompy.ShapeType["FACE"])
symm_axis = geompy.CreateGroup(assembly, geompy.ShapeType["EDGE"])
top_edge = geompy.CreateGroup(assembly, geompy.ShapeType["EDGE"])
bot_edge = geompy.CreateGroup(assembly, geompy.ShapeType["EDGE"])
top_surf = geompy.CreateGroup(assembly, geompy.ShapeType["EDGE"])
bot_surf = geompy.CreateGroup(assembly, geompy.ShapeType["EDGE"])
top_contact = geompy.CreateGroup(assembly, geompy.ShapeType["VERTEX"])
bot_contact = geompy.CreateGroup(assembly, geompy.ShapeType["VERTEX"])

# Assign geometry entities to groups
geompy.UnionIDs(top_sphere, [2])
geompy.UnionIDs(bot_sphere, [12])
geompy.UnionIDs(symm_axis, [11, 21])
geompy.UnionIDs(top_edge, [9])
geompy.UnionIDs(bot_edge, [19])
geompy.UnionIDs(top_surf, [4])
geompy.UnionIDs(bot_surf, [14])
geompy.UnionIDs(top_contact, [5])
geompy.UnionIDs(bot_contact, [15])

# Add all geometric entities to the study
geompy.addToStudy( O, 'O' )
geompy.addToStudy( OX, 'OX' )
geompy.addToStudy( OY, 'OY' )
geompy.addToStudy( OZ, 'OZ' )
geompy.addToStudy( sphere_boundary, 'sphere_boundary' )
geompy.addToStudy( sphere, 'sphere' )
geompy.addToStudy( sphere, 'sphere_mirror' )
geompy.addToStudy( assembly, 'assembly' )
geompy.addToStudyInFather( assembly, top_sphere, 'top_sphere' )
geompy.addToStudyInFather( assembly, bot_sphere, 'bot_sphere' )
geompy.addToStudyInFather( assembly, symm_axis, 'symm_axis' )
geompy.addToStudyInFather( assembly, top_edge, 'top_edge' )
geompy.addToStudyInFather( assembly, bot_edge, 'bot_edge' )
geompy.addToStudyInFather( assembly, top_surf, 'top_surf' )
geompy.addToStudyInFather( assembly, bot_surf, 'bot_surf' )
geompy.addToStudyInFather( assembly, top_contact, 'top_contact' )
geompy.addToStudyInFather( assembly, bot_contact, 'bot_contact' )

#--------------------------------------------
# Create Mesh
#--------------------------------------------
smesh = smeshBuilder.New()
aMeasurements = smesh.CreateMeasurements()

# Set up meshing algorithms
mesh = smesh.Mesh(assembly)
Regular_1D = mesh.Segment()
MEFISTO_2D = mesh.Triangle()

# Set up mesh size
max_size = Regular_1D.MaxSize(2)

# Compute mesh
isDone = mesh.Compute()

# Create mesh groups
top_sphere_1 = mesh.GroupOnGeom(top_sphere, 'top_sphere', SMESH.FACE)
bot_sphere_1 = mesh.GroupOnGeom(bot_sphere, 'bot_sphere', SMESH.FACE)
symm_axis_1 = mesh.GroupOnGeom(symm_axis, 'symm_axis', SMESH.EDGE)
top_edge_1 = mesh.GroupOnGeom(top_edge, 'top_edge', SMESH.EDGE)
bot_edge_1 = mesh.GroupOnGeom(bot_edge, 'bot_edge', SMESH.EDGE)
top_surf_1 = mesh.GroupOnGeom(top_surf, 'top_surf', SMESH.EDGE)
bot_surf_1 = mesh.GroupOnGeom(bot_surf, 'bot_surf', SMESH.EDGE)
top_contact_1 = mesh.GroupOnGeom(top_contact, 'top_contact', SMESH.NODE)
bot_contact_1 = mesh.GroupOnGeom(bot_contact, 'bot_contact', SMESH.NODE)

# Set object names
smesh.SetName(mesh.GetMesh(), 'mesh')
smesh.SetName(Regular_1D.GetAlgorithm(), 'Regular_1D')
smesh.SetName(MEFISTO_2D.GetAlgorithm(), 'MEFISTO_2D')
smesh.SetName(max_size, 'Max Size')
smesh.SetName(top_sphere_1, 'top_sphere')
smesh.SetName(bot_sphere_1, 'bot_sphere')
smesh.SetName(symm_axis_1, 'symm_axis')
smesh.SetName(top_edge_1, 'top_edge')
smesh.SetName(bot_edge_1, 'bot_edge')
smesh.SetName(top_surf_1, 'top_surf')
smesh.SetName(bot_surf_1, 'bot_surf')
smesh.SetName(top_contact_1, 'top_contact')
smesh.SetName(bot_contact_1, 'bot_contact')

# Save mesh
mesh.ExportMED( r''+ExportPATH+'forma04a.mmed'+'')

# Update the GUI object tree
if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()
