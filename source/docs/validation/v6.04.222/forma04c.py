import sys

import salome
import GEOM
import math
from salome.geom import geomBuilder

import SMESH, SALOMEDS
from salome.smesh import smeshBuilder
from salome.StdMeshers import StdMeshersBuilder

ExportPATH="/home/banerjee/Salome/forma04c/"

# Start salome
salome.salome_init()

#--------------------------------------------
# Create geometry
#--------------------------------------------
geompy = geomBuilder.New()

# Create coordinate system
O = geompy.MakeVertex(0, 0, 0)
OX = geompy.MakeVectorDXDYDZ(1, 0, 0)
OY = geompy.MakeVectorDXDYDZ(0, 1, 0)
OZ = geompy.MakeVectorDXDYDZ(0, 0, 1)

# Ball radius
rad = 50
angle = math.pi / 2.0

# Create sketcher and add geometry
sk = geompy.Sketcher2D()
sk.addPoint(0.000000, 0.000000)
sk.addArcAngleRadiusLength(0, 50.000000, 45.000000)
sk.addArcAngleRadiusLength(0, 50.000000, 45.000000)
sk.addSegmentAbsolute(0.000000, 50.000000)
sk.close()

# Add the wire
coord_sys = geompy.MakeMarkerPntTwoVec(O, OX, OY)
sphere_boundary = sk.wire(coord_sys)

# Add the sphere
sphere = geompy.MakeFaceWires([sphere_boundary], 1)

# Create vertices and lines for partitioning the domain
v1 = geompy.MakeVertex(0, 25, 0)
v2 = geompy.MakeVertex(25, 50, 0)
v3 = geompy.MakeVertex(25, 25, 0)
v4 = geompy.MakeVertex(50, 0, 0)

l1 = geompy.MakeLineTwoPnt(v3, v1)
l2 = geompy.MakeLineTwoPnt(v3, v2)
l3 = geompy.MakeLineTwoPnt(v4, v3)

# Partition the domain
partition = geompy.MakePartition([sphere], [l1, l2, l3], [], [], 
                                 geompy.ShapeType["FACE"], 0, [], 0)

# Create a mirrored copy of the partitioned sphere
partition_mirror =  geompy.MakeMirrorByAxis(partition, OX)

# Create an assembly
assembly = geompy.MakeCompound([partition, partition_mirror])

# Create geometry groups
top_sphere = geompy.CreateGroup(assembly, geompy.ShapeType["FACE"])
bot_sphere = geompy.CreateGroup(assembly, geompy.ShapeType["FACE"])
symm_axis = geompy.CreateGroup(assembly, geompy.ShapeType["EDGE"])
top_edge = geompy.CreateGroup(assembly, geompy.ShapeType["EDGE"])
bot_edge = geompy.CreateGroup(assembly, geompy.ShapeType["EDGE"])
top_surf = geompy.CreateGroup(assembly, geompy.ShapeType["EDGE"])
bot_surf = geompy.CreateGroup(assembly, geompy.ShapeType["EDGE"])
top_contact = geompy.CreateGroup(assembly, geompy.ShapeType["VERTEX"])
bot_contact = geompy.CreateGroup(assembly, geompy.ShapeType["VERTEX"])
ver_edges = geompy.CreateGroup(assembly, geompy.ShapeType["EDGE"])
hor_edges = geompy.CreateGroup(assembly, geompy.ShapeType["EDGE"])
rad_edges = geompy.CreateGroup(assembly, geompy.ShapeType["EDGE"])

# Locate the faces that compose the top/bottom sphere
top_faces = geompy.SubShapeAllSorted(partition, geompy.ShapeType['FACE'])
bot_faces = geompy.SubShapeAllSorted(partition_mirror, geompy.ShapeType['FACE'])
geompy.UnionList(top_sphere, top_faces)
geompy.UnionList(bot_sphere, bot_faces)

# Locate the edges that compose the symmetry axis
v_test = geompy.MakeVertex(0, rad/4.0, 0)
edge1 = geompy.GetShapesNearPoint(partition, v_test, geompy.ShapeType['EDGE'])
v_test = geompy.MakeVertex(0, 3.0*rad/4.0, 0)
edge2 = geompy.GetShapesNearPoint(partition, v_test, geompy.ShapeType['EDGE'])

v_test = geompy.MakeVertex(0, -rad/4.0, 0)
edge3 = geompy.GetShapesNearPoint(partition_mirror, v_test, geompy.ShapeType['EDGE'])
v_test = geompy.MakeVertex(0, -3.0*rad/4.0, 0)
edge4 = geompy.GetShapesNearPoint(partition_mirror, v_test, geompy.ShapeType['EDGE'])
geompy.UnionList(symm_axis, [edge1, edge2, edge3, edge4])

# Locate the edges that compose the top/bot edges
v_test = geompy.MakeVertex(rad/4, rad, 0)
edge1 = geompy.GetShapesNearPoint(partition, v_test, geompy.ShapeType['EDGE'])
v_test = geompy.MakeVertex(3*rad/4, rad, 0)
edge2 = geompy.GetShapesNearPoint(partition, v_test, geompy.ShapeType['EDGE'])
geompy.UnionList(top_edge, [edge1, edge2])

v_test = geompy.MakeVertex(rad/4, -rad, 0)
edge1 = geompy.GetShapesNearPoint(partition_mirror, v_test, geompy.ShapeType['EDGE'])
v_test = geompy.MakeVertex(3*rad/4, -rad, 0)
edge2 = geompy.GetShapesNearPoint(partition_mirror, v_test, geompy.ShapeType['EDGE'])
geompy.UnionList(bot_edge, [edge1, edge2])

# Locate the edges that compose the top/bot contact surfaces
v_test = geompy.MakeVertex(rad/4, 0, 0)
edge1 = geompy.GetShapesNearPoint(partition, v_test, geompy.ShapeType['EDGE'])
geompy.UnionList(top_surf, [edge1])
edge1 = geompy.GetShapesNearPoint(partition_mirror, v_test, geompy.ShapeType['EDGE'])
geompy.UnionList(bot_surf, [edge1])

# Locate the vertices that compose the top/bot initial contact points
v_test = geompy.MakeVertex(0, 0, 0)
vert1 = geompy.GetShapesNearPoint(partition, v_test, geompy.ShapeType['VERTEX'])
geompy.UnionList(top_contact, [vert1])
vert1 = geompy.GetShapesNearPoint(partition_mirror, v_test, geompy.ShapeType['VERTEX'])
geompy.UnionList(bot_contact, [vert1])

# Locate the vertical edges
v_test = geompy.MakeVertex(0, 3.0*rad/4.0, 0)
edge1 = geompy.GetShapesNearPoint(partition, v_test, geompy.ShapeType['EDGE'])
v_test = geompy.MakeVertex(rad/2.0, 3.0*rad/4.0, 0)
edge2 = geompy.GetShapesNearPoint(partition, v_test, geompy.ShapeType['EDGE'])
v_test = geompy.MakeVertex(rad, 3.0*rad/4.0, 0)
edge3 = geompy.GetShapesNearPoint(partition, v_test, geompy.ShapeType['EDGE'])

v_test = geompy.MakeVertex(0, -3.0*rad/4.0, 0)
edge4 = geompy.GetShapesNearPoint(partition_mirror, v_test, geompy.ShapeType['EDGE'])
v_test = geompy.MakeVertex(rad/2.0, -3.0*rad/4.0, 0)
edge5 = geompy.GetShapesNearPoint(partition_mirror, v_test, geompy.ShapeType['EDGE'])
v_test = geompy.MakeVertex(rad, -3.0*rad/4.0, 0)
edge6 = geompy.GetShapesNearPoint(partition_mirror, v_test, geompy.ShapeType['EDGE'])
geompy.UnionList(ver_edges, [edge1, edge2, edge3, edge4, edge5, edge6])

# Locate the horizontal edges
v_test = geompy.MakeVertex(rad/4.0, 0, 0)
edge1 = geompy.GetShapesNearPoint(partition, v_test, geompy.ShapeType['EDGE'])
v_test = geompy.MakeVertex(rad/4.0, rad/2.0, 0)
edge2 = geompy.GetShapesNearPoint(partition, v_test, geompy.ShapeType['EDGE'])
v_test = geompy.MakeVertex(rad/4.0, rad, 0)
edge3 = geompy.GetShapesNearPoint(partition, v_test, geompy.ShapeType['EDGE'])

v_test = geompy.MakeVertex(rad/4.0, 0, 0)
edge4 = geompy.GetShapesNearPoint(partition_mirror, v_test, geompy.ShapeType['EDGE'])
v_test = geompy.MakeVertex(rad/4.0, -rad/2.0, 0)
edge5 = geompy.GetShapesNearPoint(partition_mirror, v_test, geompy.ShapeType['EDGE'])
v_test = geompy.MakeVertex(rad/4.0, -rad, 0)
edge6 = geompy.GetShapesNearPoint(partition_mirror, v_test, geompy.ShapeType['EDGE'])
geompy.UnionList(hor_edges, [edge1, edge2, edge3, edge4, edge5, edge6])

# Identify edges that have to be reversed for meshing
hor_edge_reversed = [edge1, edge4]

# Locate the radial edges
v_test = geompy.MakeVertex(0, rad/4.0, 0)
edge1 = geompy.GetShapesNearPoint(partition, v_test, geompy.ShapeType['EDGE'])
v_test = geompy.MakeVertex(3.0*rad/4.0, rad, 0)
edge2 = geompy.GetShapesNearPoint(partition, v_test, geompy.ShapeType['EDGE'])
v_test = geompy.MakeVertex(1.1*rad/2.0, 0.9*rad/2, 0)
edge3 = geompy.GetShapesNearPoint(partition, v_test, geompy.ShapeType['EDGE'])

v_test = geompy.MakeVertex(0, -rad/4.0, 0)
edge4 = geompy.GetShapesNearPoint(partition_mirror, v_test, geompy.ShapeType['EDGE'])
v_test = geompy.MakeVertex(3.0*rad/4.0, -rad, 0)
edge5 = geompy.GetShapesNearPoint(partition_mirror, v_test, geompy.ShapeType['EDGE'])
v_test = geompy.MakeVertex(1.1*rad/2.0, -0.9*rad/2, 0)
edge6 = geompy.GetShapesNearPoint(partition_mirror, v_test, geompy.ShapeType['EDGE'])
geompy.UnionList(rad_edges, [edge1, edge2, edge3, edge4, edge5, edge6])

# Identify edges that have to be reversed for meshing
rad_edge_reversed = [edge2, edge3, edge5, edge6]


# Add the geometry groups
geompy.addToStudy( O, 'O' )
geompy.addToStudy( OX, 'OX' )
geompy.addToStudy( OY, 'OY' )
geompy.addToStudy( OZ, 'OZ' )
geompy.addToStudy( sphere_boundary, 'sphere_boundary' )
geompy.addToStudy( sphere, 'sphere' )
geompy.addToStudy( v1, 'v1' )
geompy.addToStudy( v2, 'v2' )
geompy.addToStudy( v3, 'v3' )
geompy.addToStudy( v4, 'v4' )
geompy.addToStudy( l1, 'l1' )
geompy.addToStudy( l2, 'l2' )
geompy.addToStudy( l3, 'l3' )
geompy.addToStudy( partition, 'partition' )
geompy.addToStudy( partition_mirror, 'partition_mirror' )
geompy.addToStudy( assembly, 'assembly' )

geompy.addToStudyInFather( assembly, top_sphere, 'top_sphere' )
geompy.addToStudyInFather( assembly, bot_sphere, 'bot_sphere' )
geompy.addToStudyInFather( assembly, symm_axis, 'symm_axis' )
geompy.addToStudyInFather( assembly, bot_edge, 'bot_edge' )
geompy.addToStudyInFather( assembly, top_edge, 'top_edge' )
geompy.addToStudyInFather( assembly, top_surf, 'top_surf' )
geompy.addToStudyInFather( assembly, bot_surf, 'bot_surf' )
geompy.addToStudyInFather( bot_sphere, bot_contact, 'bot_contact' )
geompy.addToStudyInFather( top_sphere, top_contact, 'top_contact' )
geompy.addToStudyInFather( assembly, ver_edges, 'ver_edges' )
geompy.addToStudyInFather( assembly, rad_edges, 'rad_edges' )
geompy.addToStudyInFather( assembly, hor_edges, 'hor_edges' )

#--------------------------------------------
# Create mesh
#--------------------------------------------
# Initialize
smesh = smeshBuilder.New()
mesh = smesh.Mesh(assembly)

# Set up element types (algorithms)
Quadrangle_2D = mesh.Quadrangle(algo = smeshBuilder.QUADRANGLE)
Quadrangle_Parameters_1 = Quadrangle_2D.QuadrangleParameters(StdMeshersBuilder.QUAD_STANDARD)
Regular_1D = mesh.Segment(geom = ver_edges)

# Set up discretization (hypotheses)
disc_ver_edge = Regular_1D.NumberOfSegments(5)
disc_ver_edge.SetDistrType( 0 )

disc_rad_edge = smesh.CreateHypothesis('NumberOfSegments')
status = mesh.AddHypothesis(Regular_1D, rad_edges)
status = mesh.AddHypothesis(disc_rad_edge, rad_edges)

disc_rad_edge.SetNumberOfSegments( 20 )
disc_rad_edge.SetScaleFactor( 0.1 )
rad_edge_reversed_id = Regular_1D.ReversedEdgeIndices(rad_edge_reversed)
disc_rad_edge.SetReversedEdges(rad_edge_reversed_id)

disc_hor_edge = smesh.CreateHypothesis('NumberOfSegments')
status = mesh.AddHypothesis(Regular_1D, hor_edges)
status = mesh.AddHypothesis(disc_hor_edge, hor_edges)

disc_hor_edge.SetNumberOfSegments( 40 )
disc_hor_edge.SetDistrType( 1 )
disc_hor_edge.SetScaleFactor( 0.1 )
hor_edge_reversed_id = Regular_1D.ReversedEdgeIndices(hor_edge_reversed)
disc_hor_edge.SetReversedEdges(hor_edge_reversed_id)

# Compute the mesh
isDone = mesh.Compute()

# Transfer groups to mesh
top_contact_1 = mesh.GroupOnGeom(top_contact, 'top_contact', SMESH.NODE)
bot_contact_1 = mesh.GroupOnGeom(bot_contact, 'bot_contact', SMESH.NODE)
top_sphere_1 = mesh.GroupOnGeom(top_sphere, 'top_sphere', SMESH.FACE)
bot_sphere_1 = mesh.GroupOnGeom(bot_sphere, 'bot_sphere', SMESH.FACE)
symm_axis_1 = mesh.GroupOnGeom(symm_axis, 'symm_axis', SMESH.EDGE)
bot_edge_1 = mesh.GroupOnGeom(bot_edge, 'bot_edge', SMESH.EDGE)
top_edge_1 = mesh.GroupOnGeom(top_edge, 'top_edge', SMESH.EDGE)
top_surf_1 = mesh.GroupOnGeom(top_surf, 'top_surf', SMESH.EDGE)
bot_surf_1 = mesh.GroupOnGeom(bot_surf, 'bot_surf', SMESH.EDGE)

# Get submeshes
submesh_1 = mesh.GetSubMesh(ver_edges, 'SubMesh_1')
subMesh_2 = mesh.GetSubMesh(rad_edges, 'SubMesh_2')
subMesh_3 = mesh.GetSubMesh(hor_edges, 'SubMesh_3')

# Set names of Mesh objects
smesh.SetName(mesh.GetMesh(), 'mesh')
smesh.SetName(submesh_1, 'Submesh_1')
smesh.SetName(subMesh_2, 'SubMesh_2')
smesh.SetName(subMesh_3, 'SubMesh_3')
smesh.SetName(Quadrangle_2D.GetAlgorithm(), 'Quadrangle_2D')
smesh.SetName(Quadrangle_Parameters_1, 'Quadrangle Parameters_1')
smesh.SetName(Regular_1D.GetAlgorithm(), 'Regular_1D')
smesh.SetName(disc_ver_edge, 'vertical_segs')
smesh.SetName(disc_rad_edge, 'radial_segs')
smesh.SetName(disc_hor_edge, 'horizontal_segs')
smesh.SetName(top_sphere_1, 'top_sphere')
smesh.SetName(bot_sphere_1, 'bot_sphere')
smesh.SetName(symm_axis_1, 'symm_axis')
smesh.SetName(top_edge_1, 'top_edge')
smesh.SetName(bot_edge_1, 'bot_edge')
smesh.SetName(bot_surf_1, 'bot_surf')
smesh.SetName(top_surf_1, 'top_surf')
smesh.SetName(bot_contact_1, 'bot_contact')
smesh.SetName(top_contact_1, 'top_contact')

# Write MED file
mesh.ExportMED( r''+ExportPATH+'forma04c.mmed'+'')

# Refresh objects in GUI
if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()
