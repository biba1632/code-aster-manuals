# Square composed of two TRI7 elements

import sys
import salome
import salome_notebook
from salome.smesh import smeshBuilder
import SMESH, SALOMEDS

ExportPATH="/home/banerjee/Salome/ssnp15/"

# Initialize
salome.salome_init()
notebook = salome_notebook.NoteBook()
smesh = smeshBuilder.New()

# Create mesh
mesh = smesh.Mesh()
smesh.SetName(mesh, 'mesh')

# Add nodes
node1 = mesh.AddNode( 0, 1, 0 )
node2 = mesh.AddNode( 1, 1, 0 )
node3 = mesh.AddNode( 0, 0, 0 )
node4 = mesh.AddNode( 1, 0, 0 )
node5 = mesh.AddNode( 0.00000000E+00, 0.05000000E+01, 0.00000000E+00)
node6 = mesh.AddNode( 0.05000000E+01, 0.00000000E+00, 0.00000000E+00)
node7 = mesh.AddNode( 0.10000000E+01, 0.05000000E+01, 0.00000000E+00)
node8 = mesh.AddNode( 0.05000000E+01, 0.10000000E+01, 0.00000000E+00)
node9 = mesh.AddNode( 0.05000000E+01, 0.05000000E+01, 0.00000000E+00)
node10 = mesh.AddNode( 0.03333333E+01, 0.06666666E+01, 0.00000000E+00)
node11 = mesh.AddNode( 0.06666666E+01, 0.03333333E+01, 0.00000000E+00)

# Add groups for the nodes
node_1 = mesh.CreateEmptyGroup( SMESH.NODE, 'node_1' )
node_2 = mesh.CreateEmptyGroup( SMESH.NODE, 'node_2' )
node_3 = mesh.CreateEmptyGroup( SMESH.NODE, 'node_3' )
node_4 = mesh.CreateEmptyGroup( SMESH.NODE, 'node_4' )
node_5 = mesh.CreateEmptyGroup( SMESH.NODE, 'node_5' )
node_6 = mesh.CreateEmptyGroup( SMESH.NODE, 'node_6' )
node_7 = mesh.CreateEmptyGroup( SMESH.NODE, 'node_7' )
node_8 = mesh.CreateEmptyGroup( SMESH.NODE, 'node_8' )
node_9 = mesh.CreateEmptyGroup( SMESH.NODE, 'node_9' )
node_10 = mesh.CreateEmptyGroup( SMESH.NODE, 'node_10' )
node_11 = mesh.CreateEmptyGroup( SMESH.NODE, 'node_11' )
nbAdd = node_1.Add([1])
nbAdd = node_2.Add([2])
nbAdd = node_3.Add([3])
nbAdd = node_4.Add([4])
nbAdd = node_5.Add([5])
nbAdd = node_6.Add([6])
nbAdd = node_7.Add([7])
nbAdd = node_8.Add([8])
nbAdd = node_9.Add([9])
nbAdd = node_10.Add([10])
nbAdd = node_11.Add([11])

# Add tri elements
face1 = mesh.AddFace([1, 3, 2, 5, 9, 8, 10])
face2 = mesh.AddFace([3, 4, 2, 6, 7, 9, 11])

# Add group for the faces
g_face1 = mesh.CreateEmptyGroup( SMESH.FACE, 'tri1' )
g_face2 = mesh.CreateEmptyGroup( SMESH.FACE, 'tri2' )
nbAdd = g_face1.Add([1])
nbAdd = g_face2.Add([2])

# Add segment elements
left_seg = mesh.AddEdge([1, 3, 5])
right_seg = mesh.AddEdge([4, 2, 7])
top_seg = mesh.AddEdge([2, 1, 8])
bot_seg = mesh.AddEdge([3, 4, 6])

# Add groups for the segments
g_left = mesh.CreateEmptyGroup( SMESH.EDGE, 'left_edge' )
g_right = mesh.CreateEmptyGroup( SMESH.EDGE, 'right_edge' )
g_top = mesh.CreateEmptyGroup( SMESH.EDGE, 'top_edge' )
g_bot = mesh.CreateEmptyGroup( SMESH.EDGE, 'bot_edge' )
nbAdd = g_left.Add([3])
nbAdd = g_right.Add([4])
nbAdd = g_top.Add([5])
nbAdd = g_bot.Add([6])

# Export mesh
mesh.ExportMED( r''+ExportPATH+'ssnp15e.mmed'+'')

# Update GUI object tree
if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()
