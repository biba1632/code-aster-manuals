.. role:: red
.. role:: blue
.. role:: green
.. role:: gray

.. _v6.03.112:

*******************************************************************************************
**V6.03.112**: FORMA10 - Elastoplastic load path with von Mises plasticity
*******************************************************************************************

This example illustrates, at a material point, the influence of the loading path
on the response of an elastoplastic material. It highlights the effects of discretization
in time.

This example is based on :ref:`v6.03.015`, in which a point
object consisting of an elastoplastic material with linear isotropic hardening, is
subjected to both tensile and shear tractions. One important feature of the test
is the non-radial loading in the octahedral stress plane.

* **Model A** corresponds to a computation with imposed force, with material model
  ``VMIS_ISOT_LINE`` and illustrates the influence of a coarse time discretization by
  comparison with the solution obtained with a time step refinement process.
* **Model B** uses substepping in time to obtain a solution with a finer time step; the
  refinement in time is based on a criterion on the increment of accumulated equivalent
  plastic strain.

Reference problem
=================

Geometry
--------

The geometry represents a material point in a state of homogeneous stress and strain. This
can be represented by a single finite element, with appropriate boundary conditions, as in
:numref:`v6.03.015` (``SSNP15``) and :numref:`v6.03.014` (``SSNP14``), or by a single
point without any associated mesh, as in this example.

.. figure:: v6.03.014/fig1_v6.03.014.svg
   :height: 300 px
   :align: center

   Geometry

Material properties
-------------------

The behavior is von Mises elastoplasticity with linear isotropic hardening.

.. figure:: v6.03.014/fig4_v6.03.014.svg
   :height: 200 px
   :align: center

   Material behavior in uniaxial stress tension

The material parameters are:

* :math:`E` = 195000 MPa
* :math:`\nu` = 0.3
* :math:`\sigma_y` = 181 MPa
* :math:`E^T` = 1930 MPa


Boundary conditions and loads
-----------------------------

The boundary conditions applied to the point element are shown in the figure below.

.. figure:: v6.03.014/fig2_v6.03.014.svg
   :height: 300 px
   :align: center

   Boundary conditions

The loading path is defined by a component of normal traction and a component of shear
traction, as shown below.

.. image:: v6.03.014/fig3_v6.03.014.svg
   :height: 200 px
   :align: center


Reference solution
==================

The reference solution is identical to that of :ref:`v6.03.015`.
See :numref:`ref_sol_ssnp15` for details.


**Model A**: Using fixed time stepping
=========================================

Defining and running the simulation
------------------------------------

Since this is test on a material point, we use the command :blue:`SIMU_POINT_MAT`, which
allows computation on only one element with only one integration point.  The loading path
between point A and point B is discretized in 5 time steps.

The chosen time discretization:

+---------------+----------------------+--------------------+-----------------+
| Time          | :math:`\sigma` (MPa) | :math:`\tau` (MPa) | Number of steps |
+===============+======================+====================+=================+
| 0.0 - Point O | 0                    | 0                  |                 |
+---------------+----------------------+--------------------+-----------------+
| 0.81          |                      |                    |    1            |
+---------------+----------------------+--------------------+-----------------+
| 1.0 - Point A | 151.2                | 93.1               |    5            |
+---------------+----------------------+--------------------+-----------------+
| 2.0 - Point B | 257.2                | 33.1               |    5            |
+---------------+----------------------+--------------------+-----------------+
| 3.0 - Point C | 259.3                | 0                  |    5            |
+---------------+----------------------+--------------------+-----------------+

The input file can be downloaded from :download:`forma10a.comm <v6.03.112/forma10a.comm>`.

.. include::  v6.03.112/forma10a.comm
     :literal:

Results and verification tests
-------------------------------

.. figure:: v6.03.112/forma10_8_sigeps.svg
   :height: 300 px
   :align: center

   Stress-strain curves for models A and B

+-------+----------------------------+-----------------+-------------+--------------+
| State | Variable                   | Reference value | Aster value | Difference % |
+=======+============================+=================+=============+==============+
| A     | :math:`\varepsilon_{xx}`   | 1.4830E-2       | 1.48297E-2  | –0.002       |
+-------+----------------------------+-----------------+-------------+--------------+
| A     | :math:`\varepsilon_{xy}`   | 1.3601E-2       | 1.3601E-2   | 0.003        |
+-------+----------------------------+-----------------+-------------+--------------+
| B     | :math:`\varepsilon_{xx}`   | 3.5265E-2       | 3.5686E-2   | 1.2          |
+-------+----------------------------+-----------------+-------------+--------------+
| B     | :math:`\varepsilon_{xy}`   | 2.0471E-2       | 1.9577E-2   | –4.3         |
+-------+----------------------------+-----------------+-------------+--------------+


**Model B**: Using adaptive time stepping
==============================================

Defining and running the simulation
------------------------------------

As in **Model A**, we use the command :blue:`SIMU_POINT_MAT` computation on only one
integration point.

The loading path between point A and point B is initially discretized in 1 time step.
We use an adaptive time stepping functionality which makes it possible to subdivide the
time step if an additional criterion is not satisfied at convergence.

We use the criterion that the cumulative equivalent plastic strain increment does not exceed
0.2E-2. As soon as this criterion is not satisfied, then the code automatically subdivides
the time step and restarts the timestep until this criterion is met.

The modified time discretization and simulation command are shown below:

::

   # Event-driven time-stepping
   t_auto = DEFI_LIST_INST(METHODE = 'MANUEL',
                           DEFI_LIST = _F(LIST_INST = l_times),
                           ECHEC = _F(EVENEMENT = 'DELTA_GRANDEUR',
                                      VALE_REF = 0.2e-2,
                                      NOM_CHAM = 'VARI_ELGA',
                                      NOM_CMP = 'V1',
                                      SUBD_NIVEAU = 10))

   # Do point simulation
   result = SIMU_POINT_MAT(COMPORTEMENT = _F(RELATION = 'VMIS_ISOT_LINE'),
                           MATER = steel,
                           INCREMENT = _F(LIST_INST = t_auto),
                           NEWTON = _F(PREDICTION='ELASTIQUE',
                                       MATRICE = 'TANGENTE',
                                       REAC_ITER = 1),
                           CONVERGENCE = _F(RESI_GLOB_RELA = 1.E-8),
                           SIGM_IMPOSE = _F(SIXX = sigma_fn,
                                            SIXY = tau_fn),
                           INFO = 1)


In this example 25 calculations are carried out between point A and point B. This
time discretization is therefore fine compared to that used in Model A. Recall that point A
corresponds to time t = 1s and point B corresponds to time t = 2s.

The input file can be downloaded from :download:`forma10b.comm <v6.03.112/forma10b.comm>`.


Verification tests
------------------

The simulation produces better results that Model A, as can be seen from the
table below.

+-------+----------------------------+-----------------+--------------+
| State | Variable                   | Reference value | Difference % |
+=======+============================+=================+==============+
| A     | :math:`\varepsilon_{xx}`   | 1.4830E-2       | –0.002       |
+-------+----------------------------+-----------------+--------------+
| A     | :math:`\varepsilon_{xy}`   | 1.3601E-2       | 0.003        |
+-------+----------------------------+-----------------+--------------+
| B     | :math:`\varepsilon_{xx}`   | 3.5265E-2       | 0.39         |
+-------+----------------------------+-----------------+--------------+
| B     | :math:`\varepsilon_{xy}`   | 2.0471E-2       | 1.1          |
+-------+----------------------------+-----------------+--------------+

One can note from :numref:`forma10_8_epstime` that the difference in the strain
:math:`\varepsilon_{xy}` between the fine temporal discretization (Model B) and the
coarse temporal discretization (Model A). This difference exists only for the non-radial
part of the loading (beyond point A). It is clearly seen that for Model B, the time step
becomes quite small when approaching point B, just before time t = 2s.

.. _forma10_8_epstime:

.. figure:: v6.03.112/forma10_8_epstime.svg
   :height: 300 px
   :align: center

   Comparison between various temporal discretizations

