# coding: utf-8
import gmsh
import sys
import math
gmsh.initialize()
# Use OpenCASCADE kernel
model = gmsh.model
occ = model.occ
mesh = model.mesh
model.add("ssnp169b")
# Default element size
el_size = 0.2
# Geometry
R1 = 1.0
R3 = 0.2
# Add points
pA = occ.addPoint(R1, 0, 0, el_size)
pB = occ.addPoint(0, R1, 0, el_size)
pC = occ.addPoint(-R1, 0, 0, el_size)
pD = occ.addPoint(0, -R1, 0, el_size)
pE = occ.addPoint(R3, 0, 0, el_size)
pF = occ.addPoint(0, R3, 0, el_size)
pG = occ.addPoint(-R3, 0, 0, el_size)
pH = occ.addPoint(0, -R3, 0, el_size)
pO = occ.addPoint(0, 0, 0, el_size)
# Add lines and arcs
lAB = occ.addCircleArc(pA, pO, pB)
lBF = occ.addLine(pB, pF)
lFE = occ.addCircleArc(pF, pO, pE)
lEA = occ.addLine(pE, pA)
lBC = occ.addCircleArc(pB, pO, pC)
lCG = occ.addLine(pC, pG)
lGF = occ.addCircleArc(pG, pO, pF)
lCD = occ.addCircleArc(pC, pO, pD)
lDH = occ.addLine(pD, pH)
lHG = occ.addCircleArc(pH, pO, pG)
lDA = occ.addCircleArc(pD, pO, pA)
lEH = occ.addCircleArc(pE, pO, pH)
# Add curve loops and plane surfaces
wABFE = occ.addCurveLoop([lAB, lBF, lFE, lEA])
wBCGF = occ.addCurveLoop([lBC, lCG, lGF, -lBF])
wCDHG = occ.addCurveLoop([lCD, lDH, lHG, -lCG])
wDAEH = occ.addCurveLoop([lDA, -lEA, lEH, -lDH])
sABFE = occ.addPlaneSurface([wABFE])
sBCGF = occ.addPlaneSurface([wBCGF])
sCDHG = occ.addPlaneSurface([wCDHG])
sDAEH = occ.addPlaneSurface([wDAEH])
occ.synchronize()
# For transfinite interpolation
num_nodes = 31
for curve in [lAB, lBC, lCD, lDA, lFE, lGF, lHG, lEH]:
    mesh.setTransfiniteCurve(curve, num_nodes, meshType = "Progression", coef = 1)
num_nodes = 29
for curve in [lBF, lEA, lCG, lDH]:
    mesh.setTransfiniteCurve(curve, num_nodes, meshType = "Progression", coef = 1)
for surf in [sABFE, sBCGF, sCDHG, sDAEH]:
    mesh.setTransfiniteSurface(surf)
mesh.setRecombine(2, sABFE)
mesh.setRecombine(2, sBCGF)
mesh.setRecombine(2, sCDHG)
mesh.setRecombine(2, sDAEH)
# Physical groups for the edges
inner_g = model.addPhysicalGroup(1, [lFE, lGF, lHG, lEH])
model.setPhysicalName(1, inner_g, 'inner')
outer_g = model.addPhysicalGroup(1, [lAB, lBC, lCD, lDA])
model.setPhysicalName(1, outer_g, 'outer')
# Physical groups for the faces
annulus_g = model.addPhysicalGroup(2, [sABFE, sBCGF, sCDHG, sDAEH])
model.setPhysicalName(2, annulus_g, 'annulus')
#gmsh.option.setNumber('Mesh.Algorithm', 8)
gmsh.option.setNumber('Mesh.RecombinationAlgorithm', 1)
gmsh.option.setNumber('Mesh.ElementOrder', 2)
gmsh.option.setNumber('Mesh.MshFileVersion', 2.2)
gmsh.option.setNumber('Mesh.MedFileMinorVersion', 0)
gmsh.option.setNumber('Mesh.SaveAll', 0)
gmsh.option.setNumber('Mesh.SaveGroupsOfNodes', 1)
gmsh.option.setNumber('Mesh.SecondOrderIncomplete', 1)
# Generate mesh
mesh.generate(2)
# Save mesh
gmsh.write("ssnp169b_upd.med")
gmsh.fltk.run()
gmsh.finalize()
