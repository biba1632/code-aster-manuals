# 3D cubic element in combined tension + shear
# J2 plasticity + isotropic hardening
# with event driven adaptive time-stepping
# (slightly different time stepping from model F)
DEBUT(LANG='EN')

# Read the mesh
mesh = LIRE_MAILLAGE(FORMAT = "MED")

# Assign 3D mechanical element
model = AFFE_MODELE(MAILLAGE = mesh,
                    AFFE = _F(TOUT = 'OUI',
                              PHENOMENE = 'MECANIQUE',
                              MODELISATION = '3D'))

# Material parameters
E = 195000.0
nu = 0.3
h_lin_iso = 1930.0
sig_y = 181.0

# Define material
steel = DEFI_MATERIAU(ELAS = _F(E = E,
                                NU = nu),
                      ECRO_LINE = _F(D_SIGM_EPSI = h_lin_iso,
                                     SY = sig_y),
                      PRAGER = _F(C = 0))

# Assign material
mater = AFFE_MATERIAU(MAILLAGE = mesh,
                      AFFE = _F(TOUT = 'OUI',
                                 MATER = steel))

# Define traction functions
# Tension
sigma_fn = DEFI_FONCTION(NOM_PARA = 'INST',
                         VALE = (0.0, 0.0,
                                 1.0, 151.2,
                                 2.0, 257.2,
                                 3.0, 0.0),
                         PROL_DROITE = 'EXCLU',
                         PROL_GAUCHE = 'EXCLU')

# Shear
tau_fn = DEFI_FONCTION(NOM_PARA = 'INST',
                       VALE = (0.0, 0.0,
                               1.0, 93.1,
                               2.0, 33.1,
                               3.0, 0.0),
                       PROL_DROITE = 'EXCLU',
                       PROL_GAUCHE = 'EXCLU')

# Apply a unit traction BC for tension in -x
tens_bc = AFFE_CHAR_MECA(MODELE = model,
                         FORCE_FACE = _F(GROUP_MA = 'left_face',
                                         FX = -1.))

# Apply unit traction bcs for shear in xy-plane
shear_bc = AFFE_CHAR_MECA(MODELE = model,
                          FORCE_FACE = (_F(GROUP_MA = 'left_face',
                                           FY = -1.),
                                        _F(GROUP_MA = 'right_face',
                                           FY = 1.),
                                        _F(GROUP_MA = 'top_face',
                                           FX = 1.),
                                        _F(GROUP_MA = 'bot_face',
                                           FX = -1.)))

# Apply displacement BCs to prevent rigid body motions
disp_bc = AFFE_CHAR_MECA(MODELE = model,
                         DDL_IMPO = (_F(GROUP_NO = 'node_4',
                                        DX = 0.,
                                        DY = 0.),
                                     _F(GROUP_NO = 'node_8',
                                        DX = 0.,
                                        DY = 0.,
                                        DZ = 0.),
                                     _F(GROUP_NO = 'node_2',
                                        DX = 0.),
                                     _F(GROUP_NO = 'node_6',
                                        DX = 0.)))


# Define time steps (fewer initial substeps than Model A)
l_times = DEFI_LIST_REEL(DEBUT = 0.0,
                         INTERVALLE = (_F(JUSQU_A = 0.1,
                                          NOMBRE = 1),
                                       _F(JUSQU_A = 0.9,
                                          NOMBRE = 5),
                                       _F(JUSQU_A = 1.0,
                                          NOMBRE = 1),
                                       _F(JUSQU_A = 2.0,
                                          NOMBRE = 20),
                                       _F(JUSQU_A = 3.0,
                                          NOMBRE = 1)))


# Assign manual substepping
times = DEFI_LIST_INST(METHODE = 'MANUEL', 
                       DEFI_LIST = _F(LIST_INST = l_times))

# Run simulation (1)
# VMIS_ISOT_LINE
result = STAT_NON_LINE(MODELE = model,
                       CHAM_MATER = mater,
                       EXCIT = (_F(CHARGE = disp_bc),
                                _F(CHARGE = tens_bc,
                                   FONC_MULT = sigma_fn),
                                _F(CHARGE = shear_bc,
                                   FONC_MULT = tau_fn)),
                       COMPORTEMENT = _F(RELATION = 'VMIS_ISOT_LINE'),
                       INCREMENT = _F(LIST_INST = times),
                       NEWTON = _F(MATRICE = 'TANGENTE',
                                   REAC_ITER = 3),
                       RECH_LINEAIRE = _F(RHO_MAX=150))

# Calculate additional data (1)
result = CALC_CHAMP(reuse = result,
                    RESULTAT = result,
                    CRITERES = ('SIEQ_ELGA'),
                    CONTRAINTE = ('SIGM_ELNO', 'SIGM_ELGA'),
                    VARI_INTERNE = ('VARI_ELNO'),
                    DEFORMATION = ('EPSI_ELNO', 'EPSI_ELGA', 'EPSP_ELGA'))

# Save list of time points actually used in the calculation
t_actual = RECU_TABLE(CO = result, 
                      NOM_TABLE = 'PARA_CALC')
IMPR_TABLE(TABLE = t_actual,
           NOM_PARA = 'INST',
           SEPARATEUR = ',')

# Create a list of times that is twice as fine as that used for (1)
times2 = DEFI_LIST_INST(DEFI_LIST = _F(RESULTAT = result, 
                                       SUBD_PAS = 2))

# Run simulation (2)
# VMIS_ISOT_LINE
result2 = STAT_NON_LINE(MODELE = model,
                        CHAM_MATER = mater,
                        EXCIT = (_F(CHARGE = disp_bc),
                                 _F(CHARGE = tens_bc,
                                    FONC_MULT = sigma_fn),
                                 _F(CHARGE = shear_bc,
                                    FONC_MULT = tau_fn)),
                        COMPORTEMENT = _F(RELATION = 'VMIS_ISOT_LINE'),
                        INCREMENT = _F(LIST_INST = times2),
                        NEWTON = _F(MATRICE = 'TANGENTE',
                                    REAC_ITER = 3),
                        RECH_LINEAIRE = _F(RHO_MAX=150))

# Calculate additional data (2)
result2 = CALC_CHAMP(reuse = result2,
                     RESULTAT = result2,
                     CRITERES = ('SIEQ_ELGA'),
                     CONTRAINTE = ('SIGM_ELNO', 'SIGM_ELGA'),
                     VARI_INTERNE = ('VARI_ELNO'),
                     DEFORMATION = ('EPSI_ELNO', 'EPSI_ELGA', 'EPSP_ELGA'))

# Save list of time points actually used in the calculation (2)
t_actua2 = RECU_TABLE(CO = result2, 
                      NOM_TABLE = 'PARA_CALC')
IMPR_TABLE(TABLE = t_actua2,
           NOM_PARA = 'INST',
           SEPARATEUR = ',')

# Write results for visualization
IMPR_RESU(FORMAT= 'MED',
          RESU = _F(RESULTAT = result),
          UNITE = 80)
IMPR_RESU(FORMAT= 'MED',
          RESU = _F(RESULTAT = result2),
          UNITE = 81)

# Extract results for stress-strain plots (xx and xy)
sig_xx_1 = RECU_FONCTION(GROUP_MA = ('cube'),
                         NOM_CHAM = 'SIGM_ELGA',
                         NOM_CMP = 'SIXX',
                         POINT = 1,
                         RESULTAT = result)
eps_xx_1 = RECU_FONCTION(GROUP_MA = ('cube'),
                         NOM_CHAM = 'EPSI_ELGA',
                         NOM_CMP = 'EPXX',
                         POINT = 1,
                         RESULTAT = result)
sig_xy_1 = RECU_FONCTION(GROUP_MA = ('cube'),
                         NOM_CHAM = 'SIGM_ELGA',
                         NOM_CMP = 'SIXY',
                         POINT = 1,
                         RESULTAT = result)
eps_xy_1 = RECU_FONCTION(GROUP_MA = ('cube'),
                         NOM_CHAM = 'EPSI_ELGA',
                         NOM_CMP = 'EPXY',
                         POINT = 1,
                         RESULTAT = result)

sig_xx_2 = RECU_FONCTION(GROUP_MA = ('cube'),
                         NOM_CHAM = 'SIGM_ELGA',
                         NOM_CMP = 'SIXX',
                         POINT = 1,
                         RESULTAT = result2)
eps_xx_2 = RECU_FONCTION(GROUP_MA = ('cube'),
                         NOM_CHAM = 'EPSI_ELGA',
                         NOM_CMP = 'EPXX',
                         POINT = 1,
                         RESULTAT = result2)
sig_xy_2 = RECU_FONCTION(GROUP_MA = ('cube'),
                         NOM_CHAM = 'SIGM_ELGA',
                         NOM_CMP = 'SIXY',
                         POINT = 1,
                         RESULTAT = result2)
eps_xy_2 = RECU_FONCTION(GROUP_MA = ('cube'),
                         NOM_CHAM = 'EPSI_ELGA',
                         NOM_CMP = 'EPXY',
                         POINT = 1,
                         RESULTAT = result2)

# Write results for plotting
IMPR_FONCTION(COURBE = (_F(FONCTION = sig_xx_1),
                        _F(FONCTION = eps_xx_1)),
              FORMAT = 'TABLEAU',
              SEPARATEUR = ',',
              UNITE = 8)
IMPR_FONCTION(COURBE = (_F(FONCTION = sig_xy_1),
                        _F(FONCTION = eps_xy_1)),
              FORMAT = 'TABLEAU',
              SEPARATEUR = ',',
              UNITE = 8)
IMPR_FONCTION(COURBE = (_F(FONCTION = sig_xx_2),
                        _F(FONCTION = eps_xx_2)),
              FORMAT = 'TABLEAU',
              SEPARATEUR = ',',
              UNITE = 8)
IMPR_FONCTION(COURBE = (_F(FONCTION = sig_xy_2),
                        _F(FONCTION = eps_xy_2)),
              FORMAT = 'TABLEAU',
              SEPARATEUR = ',',
              UNITE = 8)

# Verification tests
# **NOTE** Compare the tolerances used for (1) with (2)
TEST_RESU(RESU = (_F(INST = 2.0,
                     REFERENCE = 'ANALYTIQUE',
                     RESULTAT = result,
                     NOM_CHAM = 'EPSI_ELNO',
                     GROUP_NO = 'node_2',
                     NOM_CMP = 'EPXX',
                     VALE_CALC = 0.035382410,
                     VALE_REFE = 0.035264999999999998,
                     PRECISION = 4.0000000000000001E-3,
                     GROUP_MA = 'cube'),
                  _F(INST = 2.0,
                     REFERENCE = 'ANALYTIQUE',
                     RESULTAT = result,
                     NOM_CHAM = 'EPSI_ELNO',
                     GROUP_NO = 'node_2',
                     NOM_CMP = 'EPXY',
                     VALE_CALC = 0.020234125,
                     VALE_REFE = 0.020471,
                     PRECISION = 0.012,
                     GROUP_MA = 'cube')))

TEST_RESU(RESU = (_F(INST = 2.0,
                     REFERENCE = 'ANALYTIQUE',
                     RESULTAT = result2,
                     NOM_CHAM = 'EPSI_ELNO',
                     GROUP_NO = 'node_2',
                     NOM_CMP = 'EPXX',
                     VALE_CALC = 0.035324712,
                     VALE_REFE = 0.035264999999999998,
                     PRECISION = 2.E-3,
                     GROUP_MA = 'cube'),
                  _F(INST = 2.0,
                     REFERENCE = 'ANALYTIQUE',
                     RESULTAT = result2,
                     NOM_CHAM = 'EPSI_ELNO',
                     GROUP_NO = 'node_2',
                     NOM_CMP = 'EPXY',
                     VALE_CALC = 0.020351416,
                     VALE_REFE = 0.020471,
                     PRECISION = 6.0000000000000001E-3,
                     GROUP_MA = 'cube')))

# End  
FIN();
