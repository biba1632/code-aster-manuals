# FORMA30B: Axisymmetric thermoplastic cylinder
#......................................................................
#
#      INTERNAL       TOP            EXTERNAL
#               D -----+----- Cs
# !               !    J    !
#                 !         !
# !               !         !
#                 !         !        10 MM
# !               !         !
#                 !         !
# !               !    F    !
#    19.5 MM   P1 -----+-----  P2
# !-------------->   1.0 MM
#                 <--------->
#======================================================================
#
DEBUT(LANG = 'EN')

# Read mesh in Aster format
mesh = LIRE_MAILLAGE(FORMAT = "ASTER")

# Define node groups from elements groups
DEFI_GROUP(reuse = mesh, 
           MAILLAGE = mesh,
           CREA_GROUP_NO = _F(TOUT_GROUP_MA = 'OUI'))

# Assign axisymmetric mechanical elements to the mesh
model = AFFE_MODELE(MAILLAGE = mesh,
                    AFFE = _F(TOUT = 'OUI',
                              PHENOMENE = 'MECANIQUE',
                              MODELISATION = 'AXIS'))

# Assign axisymmetric thermal elements to the mesh
model_th = AFFE_MODELE(MAILLAGE = mesh,
                       AFFE = _F(TOUT = 'OUI',
                                 PHENOMENE = 'THERMIQUE',
                                 MODELISATION = 'AXIS'))

# Assign AXIS_DIAG thermal elements to the mesh
mod_th2 = AFFE_MODELE(MAILLAGE = mesh,
                      AFFE = _F(TOUT = 'OUI',
                                PHENOMENE = 'THERMIQUE',
                                MODELISATION = 'AXIS_DIAG'))

# Define mechanical properties
mat_mech = DEFI_MATERIAU(ELAS = _F(E = 2.E5,
                                   NU = 0.29999999999999999,
                                   RHO = 7.9999999999999996E-06,
                                   ALPHA = 1.0000000000000001E-05))

# Define thermal material properties
mat_th = DEFI_MATERIAU(THER = _F(LAMBDA = 1.0,
                                 RHO_CP = 1000.0))

# Assign thermal material properties
mater_th = AFFE_MATERIAU(MAILLAGE = mesh,
                         AFFE = _F(TOUT = 'OUI',
                                   MATER = mat_th))

#---------------------------------------------
# Thermal calculation (Step 0)
#---------------------------------------------

# Heat exchange coeff. in internal wall due to thermal shock
H_wall = DEFI_CONSTANTE(VALE = 100.0)

# Define thermal load curve for external wall
T_ext = DEFI_FONCTION(NOM_PARA = 'INST',
                      VALE = (  0.0,  100.0,
                                0.01, 0.0,
                               10.0,  0.0,
                              100.0,  0.0),
                      PROL_DROITE = 'CONSTANT',
                      PROL_GAUCHE = 'CONSTANT')

# Assign thermal BCs
therm_bc = AFFE_CHAR_THER_F(MODELE = model_th,
                            ECHANGE = _F(GROUP_MA = 'exchange',
                                         COEF_H = H_wall,
                                         TEMP_EXT = T_ext))

ther_bc2 = AFFE_CHAR_THER_F(MODELE = mod_th2,
                            ECHANGE = _F(GROUP_MA = 'exchange',
                                         COEF_H = H_wall,
                                         TEMP_EXT = T_ext))
# Define time steps
l_times = DEFI_LIST_REEL(DEBUT = 0.0,
                         INTERVALLE = (_F(JUSQU_A = 1.E-2,
                                          NOMBRE = 10),
                                       _F(JUSQU_A = 0.10000000000000001,
                                          NOMBRE = 9),
                                       _F(JUSQU_A = 1.0,
                                          NOMBRE = 9),
                                       _F(JUSQU_A = 2.0,
                                          NOMBRE = 5),
                                       _F(JUSQU_A = 10.0,
                                          NOMBRE = 8),
                                       _F(JUSQU_A = 100.0,
                                          NOMBRE =5)))

# Do linear thermal calculation
resu_th = THER_LINEAIRE(MODELE = model_th,
                        CHAM_MATER = mater_th,
                        EXCIT = _F(CHARGE = therm_bc),
                        INCREMENT = _F(LIST_INST = l_times),
                        ETAT_INIT = _F(VALE = 100.0))

resu_th2 = THER_LINEAIRE(MODELE = mod_th2,
                         CHAM_MATER = mater_th,
                         EXCIT = _F(CHARGE = ther_bc2),
                         INCREMENT = _F(LIST_INST = l_times),
                         ETAT_INIT = _F(VALE = 100.0))

resu_th3 = THER_LINEAIRE(MODELE = mod_th2,
                         CHAM_MATER = mater_th,
                         EXCIT = _F(CHARGE = ther_bc2),
                         INCREMENT = _F(LIST_INST = l_times),
                         ETAT_INIT = _F(VALE = 100.0),
                         PARM_THETA = 0.0)

# Write result
IMPR_RESU(FORMAT = "RESULTAT",
          RESU = _F(RESULTAT = resu_th,
                    INST = (0.0, 0.10000000000000001, 4.0, 10.0, 100.0),
                    VALE_MAX = 'OUI',
                    VALE_MIN = 'OUI'))

IMPR_RESU(FORMAT = "RESULTAT",
          RESU = _F(RESULTAT = resu_th2,
                    INST = (0.0, 0.10000000000000001, 4.0, 10.0, 100.0),
                    VALE_MAX = 'OUI',
                    VALE_MIN = 'OUI'))

IMPR_RESU(FORMAT = "RESULTAT",
          RESU = _F(RESULTAT = resu_th3,
                    INST = (0.0, 0.10000000000000001, 4.0, 10.0, 100.0),
                    VALE_MAX = 'OUI',
                    VALE_MIN = 'OUI'))

IMPR_RESU(FORMAT = 'MED',
          RESU = _F(MAILLAGE = mesh,
                    RESULTAT = resu_th,
                    INST = (0.1, 4.0, 10.0)))

IMPR_RESU(FORMAT = 'MED',
          RESU = _F(MAILLAGE = mesh,
                    RESULTAT = resu_th2,
                    INST = (0.1, 4.0, 10.0)))

IMPR_RESU(FORMAT = 'MED',
          RESU = _F(MAILLAGE = mesh,
                    RESULTAT = resu_th3,
                    INST = (0.1, 4.0, 10.0)))

# Extract temperature at point P2
T_p2 = RECU_FONCTION(RESULTAT = resu_th,
                     NOM_CHAM = 'TEMP',
                     NOM_CMP = 'TEMP',
                     GROUP_NO = 'p2',
                     TITRE = 'T=f(t) point p2 middle axis (no diag)')

T_p2_2 = RECU_FONCTION(RESULTAT = resu_th2,
                       NOM_CHAM = 'TEMP',
                       NOM_CMP = 'TEMP',
                       GROUP_NO = 'p2',
                       TITRE = 'T=f(t) point p2 middle axis-diag')

T_p1_3 = RECU_FONCTION(RESULTAT = resu_th3,
                       NOM_CHAM = 'TEMP',
                       NOM_CMP = 'TEMP',
                       GROUP_NO = 'p1',
                       TITRE = 'T=f(t) point p1 middle axis-diag theta = 0')

# Write the temperature
IMPR_FONCTION(UNITE = 29,
              FORMAT = 'TABLEAU',
              SEPARATEUR = ',',
              COURBE = (_F(FONCTION = T_p2),
                        _F(FONCTION = T_p2_2),
                        _F(FONCTION = T_p1_3)))

# Extract temperature at the bottom
T_bot = POST_RELEVE_T(ACTION = _F(INTITULE = 'T_bot',
                                  GROUP_NO = 'bottom',
                                  RESULTAT = resu_th,
                                  NOM_CHAM = 'TEMP',
                                  INST = 4.0,
                                  TOUT_CMP = 'OUI',
                                  OPERATION = 'EXTRACTION'),
                      TITRE = 'Temperatures at bottom A 4 S non-diag')

T_bot_2 = POST_RELEVE_T(ACTION = _F(INTITULE = 'T_bot',
                                    GROUP_NO = 'bottom',
                                    RESULTAT = resu_th2,
                                    NOM_CHAM = 'TEMP',
                                    INST = 4.0,
                                    TOUT_CMP = 'OUI',
                                    OPERATION = 'EXTRACTION'),
                        TITRE = 'Temperatures at bottom A 4 S diag')

# Write table
IMPR_TABLE(FORMAT = 'TABLEAU',
           SEPARATEUR = ',',
           TABLE = T_bot,
           NOM_PARA = ('ABSC_CURV','TEMP'))

IMPR_TABLE(FORMAT = 'TABLEAU',
           SEPARATEUR = ',',
           TABLE = T_bot_2,
           NOM_PARA = ('ABSC_CURV','TEMP'))

#---------------------------------------------
# Mechanical calculation (Step 1)
#---------------------------------------------
# Assign mechanical material properties
mater = AFFE_MATERIAU(MAILLAGE = mesh,
                      AFFE = _F(TOUT = 'OUI',
                                MATER = mat_mech),
                      AFFE_VARC = (_F(TOUT = 'OUI',
                                      EVOL = resu_th2,
                                      NOM_VARC = 'TEMP',
                                      NOM_CHAM = 'TEMP',
                                      VALE_REF = 0.0)))

# Assign boundary conditions
disp_bc = AFFE_CHAR_MECA(MODELE = model,
                         DDL_IMPO = _F(GROUP_MA = 'bottom',
                                       DY = 0.0))

# Define timesteps
l_times1 = DEFI_LIST_REEL(VALE = (0.0, 11.0))

# Do mechanical calculation
result = MECA_STATIQUE(MODELE = model,
                       CHAM_MATER = mater,
                       EXCIT = _F(CHARGE = disp_bc),
                       LIST_INST = l_times1)

# Add strains to result
result = CALC_CHAMP(reuse = result,
                    RESULTAT = result,
                    LIST_INST = l_times1,
                    DEFORMATION = ('EPSI_ELGA'))

# Write result
IMPR_RESU(FORMAT = "RESULTAT",
          RESU = _F(RESULTAT = result,
                    VALE_MAX = 'OUI',
                    VALE_MIN = 'OUI'))

IMPR_RESU(FORMAT = 'MED',
          RESU = _F(MAILLAGE = mesh,
                    RESULTAT = result))

#---------------------------------------------
# Mechanical calculation (Step 2)
#---------------------------------------------

# Assign boundary conditions
disp_bc2 = AFFE_CHAR_MECA(MODELE = model,
                          DDL_IMPO = (_F(GROUP_MA = 'bottom',
                                         DY = 0.0),
                                      _F(GROUP_MA = 'top',
                                         DY = 0.0)))
# Do mechanical calculation
result2 = MECA_STATIQUE(MODELE = model,
                        CHAM_MATER = mater,
                        EXCIT = _F(CHARGE = disp_bc2),
                        LIST_INST = l_times1)

# Write result
IMPR_RESU(FORMAT = "RESULTAT",
          RESU = _F(RESULTAT = result2,
                    VALE_MAX = 'OUI',
                    VALE_MIN = 'OUI'))

IMPR_RESU(FORMAT = 'MED',
          RESU = _F(MAILLAGE = mesh,
                    RESULTAT = result2))

#---------------------------------------------
# Mechanical calculation (Step 3)
#---------------------------------------------

# Do nonlinear mechanical calculation
result3 = STAT_NON_LINE(MODELE = model,
                        INFO = 1,
                        CHAM_MATER = mater,
                        EXCIT = _F(CHARGE = disp_bc2),
                        COMPORTEMENT = _F(RELATION = 'ELAS'),
                        INCREMENT = _F(LIST_INST = l_times1))

# Write result
IMPR_RESU(FORMAT = "RESULTAT",
          RESU = _F(RESULTAT = result3,
                    VALE_MAX = 'OUI',
                    VALE_MIN = 'OUI'))

IMPR_RESU(FORMAT = 'MED',
          RESU = _F(MAILLAGE = mesh,
                    RESULTAT = result3))

#---------------------------------------------
# Mechanical calculation (Step 4)
#---------------------------------------------
# Create a temperature field from thermal model
H_T0 = CREA_CHAMP(TYPE_CHAM = 'NOEU_TEMP_R',
                  OPERATION = 'AFFE',
                  MODELE = mod_th2,
                  AFFE = _F(TOUT = 'OUI',
                            NOM_CMP = 'TEMP',
                            VALE = 0.0))

# Create a result
resu_th2 = CREA_RESU(reuse = resu_th2,
                     RESULTAT = resu_th2,
                     OPERATION = 'AFFE',
                     TYPE_RESU = 'EVOL_THER',
                     NOM_CHAM = 'TEMP',
                     AFFE = _F(CHAM_GD = H_T0,
                               INST = -1.0))

# Assign BCs
disp_bc4 = AFFE_CHAR_MECA(MODELE = model,
                          DDL_IMPO = (_F(GROUP_MA = 'bottom',
                                         DY = 0.0),
                                      _F(GROUP_MA = 'top',
                                         DY = 0.0)))

# Define timesteps
l_times4 = DEFI_LIST_REEL(VALE = (-1.0, 0.0, 11.0))

# Do nonlinear mechanical calculation
result4 = STAT_NON_LINE(MODELE = model,
                        CHAM_MATER = mater,
                        EXCIT = _F(CHARGE = disp_bc4),
                        COMPORTEMENT = _F(RELATION = 'ELAS'),
                        INCREMENT = _F(LIST_INST = l_times4))

# Write result
IMPR_RESU(FORMAT = "RESULTAT",
          RESU = _F(RESULTAT = result4,
                    VALE_MAX = 'OUI',
                    VALE_MIN = 'OUI'))

IMPR_RESU(FORMAT = 'MED',
          RESU = _F(MAILLAGE = mesh,
                    RESULTAT = result4))

# Verification
TEST_RESU(RESU = _F(INST = 4.0,
                    TYPE_TEST = 'MAX',
                    RESULTAT = resu_th2,
                    NOM_CHAM = 'TEMP',
                    NOM_CMP = 'TEMP',
                    VALE_CALC = 99.999283381384))

# End
FIN()
