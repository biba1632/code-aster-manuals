#!/usr/bin/env python
# coding: utf-8

import gmsh
import sys
import math
import numpy as np

gmsh.initialize()

# Use OpenCASCADE kernel
model = gmsh.model
occ = model.occ
mesh = model.mesh
model.add("ssnv112d")

# Geometry
ri=0.1
re=0.2
rmid = 0.5*(ri+re)
h = 0.01
hmid = 0.5*h
NR = 10
NZ = 2

# Default element size
dens = 0.01;

# Points of construction
PO = occ.addPoint(0, 0, 0,  dens)

# Points of the plane of the base z=0
A = occ.addPoint(ri, 0., 0., dens)
B = occ.addPoint(re, 0., 0., dens)
C = occ.addPoint(ri, hmid, 0., dens)
D = occ.addPoint(re, hmid, 0., dens)
E = occ.addPoint(ri, h, 0.,  dens)
F = occ.addPoint(re, h, 0.,  dens)
G = occ.addPoint(rmid, 0., 0., dens)
H = occ.addPoint(rmid, h, 0., dens)
K = occ.addPoint(rmid, hmid, 0., dens)

# Lines
LAG = occ.addLine(A, G)
LGK = occ.addLine(G, K)
LKH = occ.addLine(K, H)
LKC = occ.addLine(K, C)
LHE = occ.addLine(H, E)
LEC = occ.addLine(E, C)
LCA = occ.addLine(C, A)

LGB = occ.addLine(G, B)
LBD = occ.addLine(B, D)
LDF = occ.addLine(D, F)
LDK = occ.addLine(D, K)
LFH = occ.addLine(F, H)

# Surfaces
quad_loop_1 = occ.addCurveLoop([LAG, LGK, LKC, LCA])
quad_1 = occ.addPlaneSurface([quad_loop_1])
quad_loop_2 = occ.addCurveLoop([-LKC, LKH, LHE, LEC])
quad_2 = occ.addPlaneSurface([quad_loop_2])
tri_loop_1 = occ.addCurveLoop([LGB, LBD, LDK, -LGK])
tri_1 = occ.addPlaneSurface([tri_loop_1])
tri_loop_2 = occ.addCurveLoop([-LDK, LDF, LFH, -LKH])
tri_2 = occ.addPlaneSurface([tri_loop_2])

occ.removeAllDuplicates()

occ.synchronize()

# Physical groups for the points
A_g = model.addPhysicalGroup(0, [A])
model.setPhysicalName(0, A_g, 'A')
B_g = model.addPhysicalGroup(0, [B])
model.setPhysicalName(0, B_g, 'B')
C_g = model.addPhysicalGroup(0, [C])
model.setPhysicalName(0, C_g, 'C')
D_g = model.addPhysicalGroup(0, [D])
model.setPhysicalName(0, D_g, 'D')
E_g = model.addPhysicalGroup(0, [E])
model.setPhysicalName(0, E_g, 'E')
F_g = model.addPhysicalGroup(0, [F])
model.setPhysicalName(0, F_g, 'F')
G_g = model.addPhysicalGroup(0, [G])
model.setPhysicalName(0, G_g, 'G')
H_g = model.addPhysicalGroup(0, [H])
model.setPhysicalName(0, H_g, 'H')
K_g = model.addPhysicalGroup(0, [K])
model.setPhysicalName(0, K_g, 'K')

# Physical groups for the edges
AB_g = model.addPhysicalGroup(1, [LAG, LGB])
model.setPhysicalName(1, AB_g, 'AB')
EF_g = model.addPhysicalGroup(1, [LFH, LHE])
model.setPhysicalName(1, EF_g, 'EF')
AE_g = model.addPhysicalGroup(1, [LEC, LCA])
model.setPhysicalName(1, AE_g, 'AE')
BF_g = model.addPhysicalGroup(1, [LBD, LDF])
model.setPhysicalName(1, BF_g, 'BF')
# CD_g = model.addPhysicalGroup(1, [LKC, LDK])
# model.setPhysicalName(1, CD_g, 'CD')

# Physical group for the faces
VOLUME_g = model.addPhysicalGroup(2, [tri_1, tri_2, quad_1, quad_2])
model.setPhysicalName(2, VOLUME_g, 'VOLUME')

num_nodes = NZ+1
for curve in [LGK, LKH, LBD, LDF, LEC, LCA]:
    #print(curve)
    mesh.setTransfiniteCurve(curve, num_nodes)
num_nodes = NR+1
for curve in [LAG, LGB, LFH, LHE, LDK, LKC]:
    #print(curve)
    mesh.setTransfiniteCurve(curve, num_nodes)

for surf in occ.getEntities(2):
    mesh.setTransfiniteSurface(surf[1], arrangement='AlternateRight')

mesh.setRecombine(2, quad_1)
mesh.setRecombine(2, quad_2)

#gmsh.option.setNumber('Mesh.Algorithm', 7)
gmsh.option.setNumber('Mesh.ElementOrder', 2)
gmsh.option.setNumber('Mesh.MshFileVersion', 2.2)
gmsh.option.setNumber('Mesh.MedFileMinorVersion', 0)
gmsh.option.setNumber('Mesh.SaveAll', 0)
gmsh.option.setNumber('Mesh.SaveGroupsOfNodes', 1)
gmsh.option.setNumber('Mesh.SecondOrderIncomplete', 1)
gmsh.option.setNumber('Mesh.AnisoMax', 0)

# Generate mesh
mesh.generate(2)
mesh.removeDuplicateNodes()

# Save mesh
gmsh.write("ssnv112d_upd.med")

gmsh.fltk.run()

gmsh.finalize()


