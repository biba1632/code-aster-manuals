# FORMA43A - Reinforced concrete beam in three-point bending
# Cross section of beam 200mmx500mm (quarter symmetry 10x25)
# Beam classification C30/37
import aster

# Start
DEBUT(LANG = 'EN',
      PAR_LOT='NON',)

# Internal (sub) point IDs
sub_point_steel_tension = 251; sub_point_steel_compression = 252; sub_point_concrete_tension = 249; sub_point_concrete_compression = 9

# Output file names
out_path = '/home/banerjee/Salome/forma43/Results/'
file_cs_mesh     = out_path + 'cs_mesh_10x25.med'
file_section     = out_path + 'section_10x25.med'
file_sig_eps_sp  = out_path + 'sig_eps_sp_10x25.med'
file_intvar_sp   = out_path + 'intvar_sp_10x25.med'
file_curve_post  = out_path + 'curves_10x25.post'
file_data_sp     = out_path + 'results_sp_10x25.med'

# Read line mesh for the beam
beam_msh = LIRE_MAILLAGE(UNITE = 20,
                         FORMAT = 'MED')

# Read cross-section mesh for the beam
cs_mesh = LIRE_MAILLAGE(UNITE = 22,
                        FORMAT = 'MED')

#  Define the geometry of the axial reinforcements
C_concrete = "C30/37"
cs_height = 0.5
cs_width = 0.2
cs_half_height = cs_height / 2 
cs_half_width = cs_width / 2
rebar_top = 0.032
rebar_bot = 0.044
rebar_top_dia = 0.008
rebar_bot_dia = 0.032
x_bot_left = -cs_half_width + rebar_bot
x_top_left = -cs_half_width + rebar_top
x_bot_right = cs_half_width - rebar_bot
x_top_right = cs_half_width - rebar_top
y_bot_left = -cs_half_height + rebar_bot
y_bot_right = -cs_half_height + rebar_bot
y_top_left =  cs_half_height - rebar_top
y_top_right = cs_half_height - rebar_top

rebar_ax = DEFI_GEOM_FIBRE(INFO = 1,
                           FIBRE = _F(GROUP_FIBRE = 'ax_steel', 
                                      CARA = 'DIAMETRE', 
                                      COOR_AXE_POUTRE = (0.,0.),
                                      VALE = (x_bot_right, y_bot_right, rebar_bot_dia,
                                              x_top_right, y_top_right, rebar_top_dia,
                                              x_bot_left,  y_bot_left,  rebar_bot_dia,
                                              x_top_left,  y_top_left,  rebar_top_dia)),
                           SECTION = _F(GROUP_FIBRE = 'cs_conc', 
                                        COOR_AXE_POUTRE = (0., 0.),
                                        MAILLAGE_SECT = cs_mesh, 
                                        TOUT_SECT = 'OUI'))

# Create rebar mesh
rebar_ms = CREA_MAILLAGE(GEOM_FIBRE = rebar_ax)

# Concrete
concrete = DEFI_MATER_GC(MAZARS = _F(CODIFICATION ='EC2', 
                                     UNITE_CONTRAINTE ="Pa", 
                                     CLASSE = C_concrete),
                         RHO = 2400.0)

# Steel
steel = DEFI_MATER_GC(ACIER = _F(E = 200000.0E+06, 
                                 D_SIGM_EPSI = 1200.0E+6, 
                                 SY = 400.0E+06),
                      RHO = 7800.0)

# Define elastic material
elastic = DEFI_MATERIAU(ELAS = _F(E = 2.0E11,  
                                  NU = 0.0, 
                                  RHO = 7800.0))

# Assign beam elements
model_bm = AFFE_MODELE(MAILLAGE = beam_msh,
                       AFFE = _F(TOUT = 'OUI', 
                                 PHENOMENE = 'MECANIQUE', 
                                 MODELISATION = 'POU_D_EM'))

# Define beam behavior
mazar_bm = DEFI_COMPOR(GEOM_FIBRE = rebar_ax,
                       MATER_SECT = elastic,
                       MULTIFIBRE = (_F(GROUP_FIBRE = 'ax_steel',  
                                        MATER = steel,  
                                        RELATION = 'VMIS_CINE_GC'),
                                     _F(GROUP_FIBRE = 'cs_conc',  
                                        MATER = concrete,  
                                        RELATION = 'MAZARS_GC')))

# Assign material
mater = AFFE_MATERIAU(MAILLAGE = beam_msh,
                      AFFE = _F(GROUP_MA = 'POUTRE', 
                                MATER = (steel, concrete, elastic)),
                      AFFE_COMPOR = _F(GROUP_MA = 'POUTRE',
                                       COMPOR = mazar_bm))


# Beam characteristics
char_bm = AFFE_CARA_ELEM(MODELE = model_bm, 
                         INFO = 1,
                         POUTRE = _F(GROUP_MA = ('POUTRE'), 
                                    SECTION = 'RECTANGLE',
                                    CARA = ('HY', 'HZ'), 
                                    VALE = (cs_width, cs_height)),
                         ORIENTATION = _F(GROUP_MA = ('POUTRE'), 
                                          CARA = 'ANGL_VRIL', 
                                          VALE = -90.0,),
                         GEOM_FIBRE = rebar_ax,
                         MULTIFIBRE = _F(GROUP_MA = ('POUTRE'), 
                                         GROUP_FIBRE = ('cs_conc', 'ax_steel'),
                         PREC_AIRE = 2.0E-02, 
                         PREC_INERTIE = 2.5E-01))

# Boundary conditions (fixed)
disp_bc = AFFE_CHAR_MECA(MODELE = model_bm,
                         DDL_IMPO = (_F(GROUP_NO = 'A', 
                                        DX = 0.0, DY = 0.0, DZ = 0.0, 
                                        DRX = 0.0, DRY = 0.0),
                                     _F(GROUP_NO = 'B', 
                                        DY = 0.0)))

# Boundary conditions (applied displacement)
disp_app = AFFE_CHAR_MECA(MODELE = model_bm,
                          DDL_IMPO = _F(GROUP_NO = 'C', 
                                        DY = -1.0E-2))

# Define load curve for applied displacement
t_init = 0.0
f_init = 0.0
t_final = 5.0
f_final = 5.0
f_load = DEFI_FONCTION(NOM_PARA = 'INST',
                       VALE = (t_init,  f_init,
                               t_final, f_final),
                       PROL_DROITE = 'EXCLU',
                       PROL_GAUCHE = 'EXCLU')

# Define time discretization
l_real = DEFI_LIST_REEL(DEBUT = 0.0,
                        INTERVALLE = (_F(JUSQU_A = 0.10,    NOMBRE =  2),
                                      _F(JUSQU_A = 1.40,    NOMBRE = 10),
                                      _F(JUSQU_A = 3.00,    NOMBRE = 10),
                                      _F(JUSQU_A = t_final, NOMBRE = 10)))

# Coarse discretization
l_times1 = DEFI_LIST_INST(METHODE = 'MANUEL',
                          DEFI_LIST = _F(LIST_INST = l_real),
                          ECHEC = _F(EVENEMENT     = 'ERREUR',
                                     ACTION        = 'DECOUPE',
                                     SUBD_METHODE  = 'MANUEL',
                                     SUBD_PAS      = 4,
                                     SUBD_PAS_MINI = 1.0E-10,
                                     SUBD_NIVEAU   = 5))

# Fine discretization
l_times2 = DEFI_LIST_INST(METHODE = 'MANUEL',
                          DEFI_LIST = _F(LIST_INST = l_real),
                          ECHEC = _F(EVENEMENT     = 'ERREUR',
                                     ACTION        = 'DECOUPE',
                                     SUBD_METHODE  = 'MANUEL',
                                     SUBD_PAS      = 20,
                                     SUBD_PAS_MINI = 1.0E-12,
                                     SUBD_NIVEAU   = 20))

# First attempt at solution
ok_continue = 0
try:

    result1 = STAT_NON_LINE(MODELE = model_bm,
                            CHAM_MATER = mater,
                            CARA_ELEM = char_bm,
                            EXCIT = (_F(CHARGE = disp_bc),
                                     _F(CHARGE = disp_app, 
                                        FONC_MULT = f_load)),
                            COMPORTEMENT = _F(RELATION = 'MULTIFIBRE'),
                            INCREMENT = _F(LIST_INST = l_times1, 
                                           INST_FIN = t_final),
                            NEWTON = _F(MATRICE = 'TANGENTE',
                                        REAC_ITER = 1),
                            CONVERGENCE = _F(RESI_GLOB_RELA = 1.0E-5, 
                                             ITER_GLOB_MAXI = 10))

except aster.NonConvergenceError as err:

    if err.id_message == "MECANONLINE9_7":
        ok_continue = 1

# Second attempt at solution
if (ok_continue == 1):
    try:

      result1 = STAT_NON_LINE(reuse = result1,
                              RESULTAT = result1, 
                              MODELE = model_bm,
                              CHAM_MATER = mater,
                              CARA_ELEM = char_bm,
                              ETAT_INIT = _F(EVOL_NOLI = result1),
                              EXCIT = (_F(CHARGE = disp_bc),
                                       _F(CHARGE = disp_app, 
                                          FONC_MULT = f_load)),
                              COMPORTEMENT = _F(RELATION = 'MULTIFIBRE'),
                              INCREMENT = _F(LIST_INST = l_times2, 
                                             INST_FIN = t_final),
                              NEWTON = _F(MATRICE = 'TANGENTE',
                                          REAC_ITER = 1),
                              CONVERGENCE = _F(RESI_GLOB_RELA = 3.0E-4, 
                                               ITER_GLOB_MAXI = 30))

    except aster.NonConvergenceError as err:

        if err.id_message == "MECANONLINE9_7":
            ok_continue = 2

# Add extra fields to the result (nodal reactions + strain at gauss points)
result1 = CALC_CHAMP(FORCE = 'REAC_NODA', 
                     reuse = result1, 
                     RESULTAT = result1)
result1 = CALC_CHAMP(reuse = result1, 
                     RESULTAT = result1, 
                     DEFORMATION = ('EPSI_ELGA'))

# Extract displecement and reaction data
uy_C = RECU_FONCTION(RESULTAT = result1, 
                     NOM_CHAM = 'DEPL',      
                     NOM_CMP = 'DY', 
                     GROUP_NO = 'C')
ux_B = RECU_FONCTION(RESULTAT = result1, 
                     NOM_CHAM = 'DEPL',      
                     NOM_CMP = 'DX', 
                     GROUP_NO = 'B')
fy_A = RECU_FONCTION(RESULTAT = result1, 
                     NOM_CHAM = 'REAC_NODA', 
                     NOM_CMP = 'DY', 
                     GROUP_NO = 'A')

# Strain in tension side (steel)
eps_x_st = RECU_FONCTION(RESULTAT = result1, 
                         NOM_CHAM = 'EPSI_ELGA', 
                         NOM_CMP = 'EPXX',
                         MAILLE = 'M9', 
                         POINT = 1, 
                         SOUS_POINT = sub_point_steel_tension)

# Strain in compression side (steel)
eps_x_sc = RECU_FONCTION(RESULTAT = result1, 
                         NOM_CHAM = 'EPSI_ELGA', 
                         NOM_CMP = 'EPXX',
                         MAILLE = 'M9', 
                         POINT = 1, 
                         SOUS_POINT = sub_point_steel_compression)

# Strain in tension side (concrete)
eps_x_ct = RECU_FONCTION(RESULTAT = result1, 
                         NOM_CHAM = 'EPSI_ELGA', 
                         NOM_CMP = 'EPXX',
                         MAILLE = 'M9', 
                         POINT = 1, 
                         SOUS_POINT = sub_point_concrete_tension)

# Strain in compression side (concrete)
eps_x_cc = RECU_FONCTION(RESULTAT = result1, 
                         NOM_CHAM = 'EPSI_ELGA', 
                         NOM_CMP = 'EPXX',
                         MAILLE = 'M9', 
                         POINT = 1, 
                         SOUS_POINT = sub_point_concrete_compression)

# Stress in tension side (steel)
sig_x_st = RECU_FONCTION(RESULTAT = result1, 
                         NOM_CHAM = 'SIEF_ELGA', 
                         NOM_CMP = 'SIXX',
                         MAILLE = 'M9', 
                         POINT = 1, 
                         SOUS_POINT = sub_point_steel_tension)

# Stress in compression side (steel)
sig_x_sc = RECU_FONCTION(RESULTAT = result1, 
                         NOM_CHAM = 'SIEF_ELGA', 
                         NOM_CMP = 'SIXX',
                         MAILLE = 'M9', 
                         POINT = 1, 
                         SOUS_POINT = sub_point_steel_compression)

# Stress in tension side (concrete)
sig_x_ct = RECU_FONCTION(RESULTAT = result1, 
                         NOM_CHAM = 'SIEF_ELGA', 
                         NOM_CMP = 'SIXX',
                         MAILLE = 'M9', 
                         POINT = 1, 
                         SOUS_POINT = sub_point_concrete_tension)

# Stress in compression side (concrete)
sig_x_cc = RECU_FONCTION(RESULTAT = result1, 
                         NOM_CHAM = 'SIEF_ELGA', 
                         NOM_CMP = 'SIXX',
                         MAILLE = 'M9', 
                         POINT = 1, 
                         SOUS_POINT = sub_point_concrete_compression)

# Save results to file
# Cross-section mesh
unite = DEFI_FICHIER(FICHIER = file_cs_mesh, 
                     ACTION = 'ASSOCIER', 
                     TYPE = 'LIBRE', 
                     ACCES = 'NEW')
IMPR_RESU(FORMAT = 'MED',
          UNITE = unite,
          RESU = _F(MAILLAGE = rebar_ms))
DEFI_FICHIER(UNITE = unite, 
             ACTION = 'LIBERER')
DETRUIRE(INFO = 1,
         CONCEPT = (_F(NOM = unite)))

# Section characteristics
unite = DEFI_FICHIER(FICHIER = file_section, 
                     ACTION = 'ASSOCIER', 
                     TYPE = 'LIBRE', 
                     ACCES = 'NEW')
IMPR_RESU(FORMAT = 'MED', 
          UNITE = unite,
          RESU = _F(MAILLAGE = beam_msh,
                    CARA_ELEM = char_bm)) 
                    #REPERE_LOCAL = 'ELNO', 
                    #MODELE = model_bm))
DEFI_FICHIER(UNITE = unite, 
             ACTION = 'LIBERER')
DETRUIRE(INFO = 1,
         CONCEPT = (_F(NOM = unite)))

# Results (stress/strain)
unite = DEFI_FICHIER(FICHIER = file_sig_eps_sp, 
                     ACTION = 'ASSOCIER', 
                     TYPE = 'LIBRE', 
                     ACCES = 'NEW')
IMPR_RESU(FORMAT = 'MED', 
          UNITE = unite,
          RESU = _F(RESULTAT = result1, 
                    CARA_ELEM = char_bm,
                    NOM_CHAM = ("SIEF_ELGA", "EPSI_ELGA")))
DEFI_FICHIER(UNITE = unite, 
             ACTION = 'LIBERER')
DETRUIRE(INFO = 1,
         CONCEPT = (_F(NOM = unite)))

# Results (internal vars)
unite = DEFI_FICHIER(FICHIER = file_intvar_sp, 
                     ACTION = 'ASSOCIER', 
                     TYPE = 'LIBRE', 
                     ACCES = 'NEW')
IMPR_RESU(FORMAT = 'MED', 
          UNITE = unite,
          RESU = _F(RESULTAT = result1, 
                    CARA_ELEM = char_bm, 
                    IMPR_NOM_VARI = 'NON',
                     NOM_CHAM = ("VARI_ELGA")))
DEFI_FICHIER(UNITE = unite, 
             ACTION = 'LIBERER')
DETRUIRE(INFO = 1,
         CONCEPT = (_F(NOM = unite)))

# Extracted curves 
unite = DEFI_FICHIER(FICHIER = file_curve_post, 
                     ACTION = 'ASSOCIER', 
                     TYPE = 'LIBRE', 
                     ACCES = 'NEW')
IMPR_FONCTION(UNITE = unite,
              FORMAT = 'TABLEAU',
              SEPARATEUR = ',',
              COURBE = (_F(FONCTION = uy_C), 
                        _F(FONCTION = ux_B),
                        _F(FONCTION = fy_A),
                        _F(FONCTION = eps_x_st), 
                        _F(FONCTION = eps_x_sc), 
                        _F(FONCTION = eps_x_ct), 
                        _F(FONCTION = eps_x_cc),
                        _F(FONCTION = sig_x_st), 
                        _F(FONCTION = sig_x_sc), 
                        _F(FONCTION = sig_x_ct), 
                        _F(FONCTION = sig_x_cc)))
DEFI_FICHIER(UNITE = unite, 
             ACTION = 'LIBERER')
DETRUIRE(INFO = 1,
         CONCEPT = (_F(NOM = unite)))

# Verification
TEST_FONCTION(VALEUR = _F(FONCTION = uy_C, 
                          NOM_PARA = 'INST', 
                          VALE_PARA = 3.480,
                          VALE_CALC = -3.48000E-02, 
                          VALE_REFE = -3.48000E-02, 
                          REFERENCE = 'AUTRE_ASTER'))

TEST_FONCTION(VALEUR = _F(FONCTION = ux_B, 
                          NOM_PARA = 'INST', 
                          VALE_PARA = 3.480,
                          VALE_CALC =  2.365868500647E-03,
                          VALE_REFE =  2.365868500647E-03,
                          REFERENCE = 'AUTRE_ASTER'))

TEST_FONCTION(VALEUR = _F(FONCTION = fy_A, 
                          NOM_PARA = 'INST', 
                          VALE_PARA = 3.480,
                          VALE_CALC =  88406.51211655,
                          VALE_REFE =  88406.51211655,
                          REFERENCE = 'AUTRE_ASTER'))

TEST_FONCTION(VALEUR = _F(FONCTION = eps_x_st, 
                          NOM_PARA = 'INST', 
                          VALE_PARA = 3.480,
                          VALE_CALC =  1.517755603155E-03,
                          VALE_REFE =  1.517755603155E-03,
                          REFERENCE = 'AUTRE_ASTER'))

TEST_FONCTION(VALEUR = _F(FONCTION = eps_x_sc, 
                          NOM_PARA = 'INST', 
                          VALE_PARA = 3.480,
                          VALE_CALC = -7.670325964416E-04,
                          VALE_REFE = -7.670325964416E-04,
                          REFERENCE = 'AUTRE_ASTER'))

TEST_FONCTION(VALEUR = _F(FONCTION = eps_x_ct, 
                          NOM_PARA = 'INST', 
                          VALE_PARA = 3.480,
                          VALE_CALC =  1.700969751236E-03,
                          VALE_REFE =  1.700969751236E-03,
                          REFERENCE = 'AUTRE_ASTER'))

TEST_FONCTION(VALEUR = _F(FONCTION = eps_x_cc, 
                          NOM_PARA = 'INST', 
                          VALE_PARA = 3.480,
                          VALE_CALC = -8.855829275527E-04,
                          VALE_REFE = -8.855829275527E-04,
                          REFERENCE = 'AUTRE_ASTER'))

TEST_FONCTION(VALEUR = _F(FONCTION = sig_x_st, 
                          NOM_PARA = 'INST', 
                          VALE_PARA = 3.480,
                          VALE_CALC =  3.035511109177E+08,
                          VALE_REFE =  3.035511109177E+08,
                          REFERENCE = 'AUTRE_ASTER'))

TEST_FONCTION(VALEUR = _F(FONCTION = sig_x_sc, 
                          NOM_PARA = 'INST', 
                          VALE_PARA = 3.480,
                          VALE_CALC = -1.534065290016E+08,
                          VALE_REFE = -1.534065290016E+08,
                          REFERENCE = 'AUTRE_ASTER'))

TEST_FONCTION(VALEUR = _F(FONCTION = sig_x_ct, 
                          NOM_PARA = 'INST', 
                          VALE_PARA = 3.480,
                          VALE_CALC =  2.241091950103E+05,
                          VALE_REFE =  2.241091950103E+05,
                          REFERENCE = 'AUTRE_ASTER'))

TEST_FONCTION(VALEUR = _F(FONCTION = sig_x_cc, 
                          NOM_PARA = 'INST', 
                          VALE_PARA = 3.480,
                          VALE_CALC = -2.444531093072E+07,
                          VALE_REFE = -2.444531093072E+07,
                          REFERENCE = 'AUTRE_ASTER'))

# Finish
FIN()
