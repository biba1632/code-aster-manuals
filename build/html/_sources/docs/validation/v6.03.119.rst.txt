.. role:: red
.. role:: blue
.. role:: green
.. role:: gray

.. _v6.03.119:

******************************************************************************************************
**V6.03.119**: FORMA20 - Adaptive meshing of a cantilevered beam in bending
******************************************************************************************************

This tutorial explores error indicators and mesh adaptation in ``Code_Aster`` for non-linear static
analyses.

An elastic computation on a metallic beam in bending under plane stress conditions is modeled.

Convergence is achieved by uniform refinement via the refinement-coarsening tool ``HOMARD`` encapsulated in
:red:`MACR_ADAP_MAIL`, then by free adaptive refinement by coupling the process to a spatial error map 
located on each finite element.

From the point of view of the data-processing validation, this tutorial makes it possible to test the 
non-regression of various coupling calculations of error map / procedure of refinement-coarsening in mechanics, 
but also the options of "pre- and post-processing" of these computations (smoothing of the stresses at the 
nodes, passage from an error by element to an error at the nodes by element).

Each model is associated with a practical question.  The models explored in this tutorial are:

* :ref:`v6.03.119.3`
* :ref:`v6.03.119.4`
* :ref:`v6.03.119.5`

.. _v6.03.119.1:

Reference problem
=================

.. _v6.03.119.1.1:

Geometry
--------

.. _fig_v6.03.119.1.1-a:

.. figure:: v6.03.119/fig1_v6.03.119.png
   :height: 250 px
   :align: center

   Deformation of the mesh

.. _fig_v6.03.119.1.1-b:

.. figure:: v6.03.119/fig2_v6.03.119.svg
   :height: 300 px
   :align: center

   Schematic of the thermal loads and the geometry

The geometry consists of a metal beam (16MND5 steel, :math:`E` = 210 GPa, :math:`\nu` = 0.2) in bending. 
The calculation is elastic (:red:`MECA_STATIQUE` or :red:`STAT_NON_LINE`) in plane stress (:blue:`C_PLAN`).
Elements are :blue:`TRIA3 / SEG2` ( in model A) and :blue:`TRIA6 / SEG3` (in models B and C).

The various key locations of calculation are assigned the following names: :green:`GM14` for the volume 
containing :blue:`TRIA`, :green:`GM13` for the fixed end (:blue:`DDL_IMPO DX = DY = 0` for all points 
:math:`(X = 0, Y \in [0, 10])`, :green:`GM12` for the distributed pressure (:blue:`PRES_REP` = 0.1 N for all 
the points :math:`(X \in [50, 100], Y = 10)` ) and :green:`GM10` (mesh-point :blue:`M1 = N2` at the point 
:math:`(X = 100, Y = 0)` where we will measure the deflection).

.. _v6.03.119.1.2:

Material properties
-------------------

On the structure (:blue:`GROUP_MA` :green:`GM14`), we will apply the material properties
:math:`E` = 210 GPa and :math:`\nu` = 0.2.

.. _v6.03.119.1.3:

Boundary conditions and loads
-----------------------------

One can the decompose of the loads by zone as shown in the following table:

+------------------------------------------------+-------------------------+
| Geometric areas (:blue:`GROUP_NO / GROUP_MA`)  |  Loads                  |
+================================================+=========================+
| GM13                                           | DDL_IMPO                |
+------------------------------------------------+-------------------------+
|                                                | DX = 0, DY = 0          |
+------------------------------------------------+-------------------------+
| GM12                                           | PRES_REP = 0.1 N        |
+------------------------------------------------+-------------------------+

.. _v6.03.119.2:

Reference solution
==================

.. _v6.03.119.2.1:

Calculation method used for reference solutions
-----------------------------------------------

In this problem, it is not possible to compute an analytical solution! The reference solution used 
for the calculations of errors on the deflection and on the potential energy of deformation is in fact 
an approximate solution obtained after a series of four uniform refinements (on the same mesh but in TRIA6).

This uniform refinement procedure can be driven by a PYTHON loop and the operator :red:`MACR_ADAP_MAIL` option 
:blue:`UNIFORME`. The first two models are precisely an illustration of this feature.

.. _v6.03.119.2.2:

Benchmark result
----------------

Strain energy of deformation = 0.102242 J
Deflection = -0.0614777 m

.. _v6.03.119.2.3:

Uncertainty about the solutions
-------------------------------

These are only approximate solutions obtained on a "quasi-converged" mesh.

.. _v6.03.119.2.4:

Bibliographical references
---------------------------

1) X. DESROCHES "Zhu-Zienkiewicz error estimators in elasticity 2D". :ref:`[R4.10.01] <r4.10.01>`, 1994.

2) X. DESROCHES "Estimator of error in residual". :ref:`[R4.10.02] <r4.10.02>`, 2000.

3) O. BOITEAU "Course and TP Error indicators & Mesh adaptation; State of the art and implementation in
   Code_Aster". 2002.

4) O. BOITEAU "FORMA21: Adaptive thermomechanical mesh on a cracked cylinder head". :ref:`[V6.03.120] <v6.03.120>`,
   2002.

.. _v6.03.119.3:

**Model A**:  Using :blue:`TRIA3` elements and uniform refinement
==================================================================

.. _v6.03.119.3.1:

Characteristics of model
------------------------

The mesh is created with elements of the type :blue:`TRIA3`. The calculation is linear elastic with
operator :red:`STAT_NON_LINE`.

We calculate the spatial error maps of the Zhu-Zienkiewicz indicator version 1 (:blue:`ERZ1_ELEM`) and
of the residual error indicator (:blue:`ERME_ELGA`). Beforehand, it is necessary to have calculated the 
stress field at the nodes (:blue:`SIGM_ELNO`) and, to post-process the error map (via GIBI), it is necessary 
to transform it from a :blue:`CHAM_ELEM` by element to a :blue:`CHAM_ELEM` with at the nodes of each 
element. We also determine the value of the deflection (:red:`POST_RELEVE_T`) and the strain energy 
(:red:`POST_ELEM`).

Everything is placed in a PYTHON loop allowing the implementation of a uniform refinement in ``nb_calc = 4``
levels (via :red:`MACR_ADAP_MAIL` option :blue:`UNIFORME = 'RAFFINEMENT'`).

One can thus observe the convergence of the values of deflection and energy, the increase of their
relative errors compared to the error indicators (which are themselves relative and for the entire structure), 
variations in the performance indices of the indicators and their correct satisfaction of the saturation 
hypothesis.

In order to illustrate "good practice" for the quality of the studies, on the geometry, mesh, and type of 
finite elements, one uses the ad hoc options of :red:`LIRE_MAILLAGE`, :red:`MACR_ADAP_MAIL` and 
:red:`MACR_INFO_MAIL`.

.. _fig_v6.03.119.3.1-a:

.. figure:: v6.03.119/fig3_v6.03.119.png
   :height: 300 px
   :align: center

   Residual error (absolute component of :blue:`ERREST`) on the initial mesh.

.. _fig_v6.03.119.3.1-b:

.. figure:: v6.03.119/forma20a_8.svg
   :height: 300 px
   :align: center

   Decreases of the relative errors of the strain energy and the deflection compared to those of the 
   relative maximum component of the error indicators.

The command file can be downloaded from :download:`forma20a_upd.comm <v6.03.119/forma20a_upd.comm>`.
The file is listed below.

.. include::  v6.03.119/forma20a_upd.comm
     :literal:

.. _v6.03.119.3.2:

Mesh characteristics
--------------------

* Initially: 61 TRIA3, 15 SEG2, 48 nodes 
* After a uniform refinement: 244 TRIA3, 30 SEG2, 156 nodes
* After two uniform refinements: 976 TRIA3, 60 SEG2, 555 nodes
* After three uniform refinements: 3904 TRIA3, 120 SEG2, 2085 nodes
* After four uniform refinements: 15616 TRIA3, 240 SEG2, 8073 knots

The initial mesh can be created with ``Salome-Meca``.

.. _v6.03.119.3.3:

Quantities tested and results
-----------------------------

Some of the results are saved in table format.  The table can be downloaded from
:download:`forma20a.8 <v6.03.119/forma20a.8>`.  The plots in :numref:`fig_v6.03.119.3.1-b` can
be created from the table with a Python script (:download:`plot_forma20.py <v6.03.119/plot_forma20.py>`).

We can compare the values of the relative errors in deflection and in strain energy with the reference solutions 
(cf :numref:`v6.03.119.2.2`) on the initial mesh and after four uniform refinements. The  relative tolerance, 
which on initial errors is set at :math:`10^{-6}` %.  This tolerance on errors is relaxed after 
four refinements to :math:`10^{-4}` %. The validation tests are carried out with PYTHON variables (via 
:red:`TEST_FONCTION`) previously converted into ASTER functions (via :red:`FORMULE`). See the command file
for details.

+----------------+------------------------+------------------+-------------------+------------------------------+----------------+
| Identification | Values from Code_Aster | Reference values | Tolerance         |  Relative error              | ASTER variable |
+================+========================+==================+===================+==============================+================+
| Energy (0)     | 39.406851%             | Same             | :math:`10^{-6}` % | :math:`-1.26\times 10^{-12}` | ERREEN0        |
+----------------+------------------------+------------------+-------------------+------------------------------+----------------+
| Energy (4)     | 0.274116%              | Same             | :math:`10^{-4}` % | :math:`1.5\times 10^{-12}`   | ERREEN4        |
+----------------+------------------------+------------------+-------------------+------------------------------+----------------+
| Deflection (0) | 39.244715%             | Same             | :math:`10^{-6}` % | :math:`1.09\times 10^{-13}`  | ERREFL0        |
+----------------+------------------------+------------------+-------------------+------------------------------+----------------+
| Deflection (4) | 0.270896%              | Same             | :math:`10^{-4}` % | :math:`-2.25\times 10^{-13}` | ERREFL4        |
+----------------+------------------------+------------------+-------------------+------------------------------+----------------+

.. _v6.03.119.3.4:

What to remember from this part of the tutorial
-----------------------------------------------

The macro :red:`MACR_INFO_MAIL` is complementary to :red:`LIRE_MAILLAGE` (:blue:`VERI_MAIL` and :blue:`INFO`) and
:red:`POST_ELEM`. Their combined "efforts" can therefore make it possible to:

* check the concordance of the mesh with the initial geometry (in mass, dimension, surface and volume),

* list the :blue:`GROUP_MA` and :blue:`GROUP_NO`, essential for a good model of the boundary conditions,

* diagnose any problems (symmetrization or connectedness, outline elements still present in the model, taking into 
  account the boundary conditions on surfaces or lines of wrong dimensions, interpenetration of elements),

* evaluate the quality of the mesh from a strictly finite element point of view.

  .. math::

     \forall K \in T_h \quad \sigma_K = \frac{h_K}{\rho_K} \rightarrow 1

For example, an empirical criterion could be:

* at least 50% of the finite elements with a quality criterion below 1.5,

* at least 90%, below 2.

The sequence "thermomechanical operators / :red:`MACR_ADAP_MAIL` :blue:`OPTION 'UNIFORME'`" allows a mesh to 
converge cleanly, automatically and easily. It's necessary, however, to be aware of the number of degrees of freedom 
generated which can quickly become prohibitive!

.. _v6.03.119.4:

**Model B**: Using TRIA6 elements and uniform refinement
========================================================

.. _v6.03.119.4.1:

Characteristics of model
------------------------

The geometry is identical to that of Model A, except that :blue:`TRIA6` elements are used.

The command file can be downloaded from :download:`forma20b_upd.comm <v6.03.119/forma20b_upd.comm>`.
The file is listed below.

.. include::  v6.03.119/forma20b_upd.comm
     :literal:

.. _v6.03.119.4.2:

Mesh characteristics
--------------------

* Initially: 61 TRIA6, 15 SEG3, 156 nodes 
* After a uniform refinement: 244 TRIA6, 30 SEG3, 555 nodes 
* After two uniform refinements: 976 TRIA6, 60 SEG3, 2085 nodes 
* After three uniform refinements: 3904 TRIA6, 120 SEG3, 8073 nodes 
* After four uniform refinements: 15616 TRIA6, 240 SEG3, 31761 nodes 

.. _v6.03.119.4.3:

Quantities tested and results
-----------------------------

.. _fig_v6.03.119.4.3-a:

.. figure:: v6.03.119/fig4_v6.03.119.png
   :height: 300 px
   :align: center

   Residual error (absolute component of :blue:`ERREST`) after third refinement.

.. _fig_v6.03.119.4.3-b:

.. figure:: v6.03.119/fig5_v6.03.119.png
   :height: 300 px
   :align: center

   Stress magnitude after third refinement.

.. _fig_v6.03.119.4.3-c:

.. figure:: v6.03.119/forma20b_8.svg
   :height: 300 px
   :align: center

   Decreases of the relative errors of the strain energy and the deflection compared to those of the 
   relative maximum component of the error indicators.

The basic assumptions for the verification tests are identical to those of Model A.

+----------------+------------------------+------------------+-------------------+------------------------------+----------------+
| Identification | Values from Code_Aster | Reference values | Tolerance         |  Relative error              | ASTER variable |
+================+========================+==================+===================+==============================+================+
| Energy (0)     |  0.125637%             | Same             | :math:`10^{-6}` % | :math:`-2.65\times 10^{-12}` | ERREEN0        |
+----------------+------------------------+------------------+-------------------+------------------------------+----------------+
| Energy (4)     | 0.0007015631%          | Same             | :math:`10^{-4}` % | :math:`4.71\times 10^{-13}`  | ERREEN4        |
+----------------+------------------------+------------------+-------------------+------------------------------+----------------+
| Deflection (0) | 0.106929%              | Same             | :math:`10^{-6}` % | :math:`1.60\times 10^{-12}`  | ERREFL0        |
+----------------+------------------------+------------------+-------------------+------------------------------+----------------+
| Deflection (4) | 0.0001546674%          | Same             | :math:`10^{-4}` % | :math:`-3.33\times 10^{-13}` | ERREFL4        |
+----------------+------------------------+------------------+-------------------+------------------------------+----------------+

.. _v6.03.119.4.4:

What to remember from this part of the tutorial
-----------------------------------------------

Linear (P1) elements are not recommended in mechanics. Good practice is rather: 

* P1 lumped in thermal and P2 (possibly under-integrated) in mechanics (so as not to artificially favor the thermal 
  component of the strain field and to try to avoid spatio-temporal oscillations of the temperature field and its 
  violation of the principle of the maximum entropy). The choice of the type of finite element takes precedence over 
  the quality of the meshes.

.. _v6.03.119.5:

**Model C**: TRIA6 elements and adaptive refinement
===================================================

.. _v6.03.119.5.1:

Characteristics of model
------------------------

Identical to Model A with the following modifications:

* mesh contains TRIA6 elements,
* free refinement-coarsening (:red:`MACR_ADAP_MAIL` option :blue:`LIBRE = 'RAFF_DERA'`) controlled by
  component :blue:`NUEST` of :blue:`ERRE_ELGA_NORE` (relative component of the indicator in residue).
  With like criteria :blue:`CRIT_RAFF_PE = CRIT_DERA_PE` = 0.2 (one refines 20% of the worst elements and we coarsen
  20% of the best).

The command file can be downloaded from :download:`forma20c_upd.comm <v6.03.119/forma20c_upd.comm>`.
The file is listed below.

.. include::  v6.03.119/forma20c_upd.comm
     :literal:

.. _v6.03.119.5.2:

Mesh characteristics
--------------------

* Initially: 61 TRIA6, 15 SEG3, 156 nodes
* After a free refinement: 107 TRIA6, 19 SEG3, 256 nodes
* After two free refinements: 212 TRIA6, 26 SEG3, 479 nodes
* After three free refinements: 404 TRIA6, 33 SEG3, 879 nodes
* After four free refinements: 786 TRIA6, 39 SEG3, 1671 nodes

.. _v6.03.119.5.3:

Quantities tested and results
-----------------------------

.. _fig_v6.03.119.5.3-a:

.. figure:: v6.03.119/fig6_v6.03.119.png
   :height: 300 px
   :align: center

   Residual error (absolute component of :blue:`NUEST`) after fourth refinement.

.. _fig_v6.03.119.5.3-b:

.. figure:: v6.03.119/fig7_v6.03.119.png
   :height: 300 px
   :align: center

   Stress magnitude after fourth refinement.

.. _fig_v6.03.119.5.3-c:

.. figure:: v6.03.119/forma20c_8.svg
   :height: 300 px
   :align: center

   Decreases of the relative errors of the strain energy and the deflection compared to those of the 
   relative maximum component of the error indicators.

+----------------+------------------------+------------------+-------------------+------------------------------+----------------+
| Identification | Values from Code_Aster | Reference values | Tolerance         |  Relative error              | ASTER variable |
+================+========================+==================+===================+==============================+================+
| Energy (0)     |  0.125637%             | Same             | :math:`10^{-6}` % | :math:`-2.65\times 10^{-12}` | ERREEN0        |
+----------------+------------------------+------------------+-------------------+------------------------------+----------------+
| Energy (4)     | 0.01245370%            | Same             | :math:`10^{-4}` % | :math:`2.27\times 10^{-13}`  | ERREEN4        |
+----------------+------------------------+------------------+-------------------+------------------------------+----------------+
| Deflection (0) | 0.106929%              | Same             | :math:`10^{-6}` % | :math:`1.60\times 10^{-12}`  | ERREFL0        |
+----------------+------------------------+------------------+-------------------+------------------------------+----------------+
| Deflection (4) | 0.01074923%            | Same             | :math:`10^{-4}` % | :math:`-2.34\times 10^{-13}` | ERREFL4        |
+----------------+------------------------+------------------+-------------------+------------------------------+----------------+

.. _v6.03.119.5.4:

What to remember from this part of the tutorial
-----------------------------------------------

The sequence "thermomechanical operators / :red:`MACR_ADAP_MAIL` :blue:`OPTION 'LIBRE'`" allows the mesh to converge 
optimally. 

The quality of the elements is little impacted by the process of refinement/coarsening. Due to the choices made in 
``HOMARD``, it can even improve in 3D!

The type of indicator and its mode of normalization has a large impact on the final mesh. Taking into account the type 
of normalization adopted for the mechanics indicators, we can define the indicator (in %):

.. math::

   \eta_{\text{rel}}(K) = 100 \time \frac{\eta(K)}{\sqrt{\eta(K)^2 + \| \sigma_h \|_{0, K}^2

For problems with singularities (embedding, discontinuity of curvature, re-entrant corner, crack etc.), it is better 
to use the absolute component of these indicators. To see swhy, note that for the cantilevered beam, 

.. math::

   & \eta_{\text{rel}}(K) \rightarrow 0\% \quad \text{whe} \quad \| \sigma_h \|_{0, K} \rightarrow \infty  \\
   & \eta_{\text{rel}}(K) \rightarrow 100\% \quad \text{whe} \quad \| \sigma_h \|_{0, K} \rightarrow 0 

The first situation occurs near the fixed end while the second occurs at the free end where the displacement is being
measured.  This is independent of the true values of the absolute error indicator :math:`\eta(K)`!

This does not at all call into question the usefulness of these indicators. You just have to take into account
these elements to refine its diagnosis and possibly "juggle" with these two components to refine in areas of interest.

The problem does not arise in thermal models because the residual error indicator for the thermal problem is normalized 
differently. However, we can deal with the components of the thermal indicator and boundary conditions, "fictitious" or 
not, to direct the construction of a refined mesh or coarsen by zones (cf [§6.3] :ref:`[R4.10.03] <r4.10.03.6.3>` and 
Model A, _TP21_, of :ref:`[V6.03.120] <v6.03.120>`).

.. _v6.03.119.6:

Summary of results
===================

In this tutorial, we have examined automatic mesh refinement for nonlinear static analyses. 

We perform an elastic computation on a metal beam in bending in plane stress. We first refine it uniformly via the 
tool of refinement-coarsening called ``HOMARD`` which is encapsulated in :red:`MACR_ADAP_MAIL`, then refine freely by 
coupling the process to a spatial error indicator located on each finite element.

The objectives of this tutorial are multiple:

* to become familiar with and put into practice the two dual issues: calculation of error indicator and mesh adaptation 
  strategies. On standard cases, but also on pathological cases

* to detail the various parameters of the operators (:red:`CALC_ERREUR`, :red:`MACR_ADAP_MAIL`) and related operators 
  which can be particularly interesting for these issues (:red:`INFO_MAILLAGE`, :red:`MACR_INFO_MAIL`, 
  :red:`PROJ_CHAMP`),

* to illustrate "good practice" for the quality of studies and the use of tools already available on the subject. We are 
  only interested in the aspects of geometry, mesh, and type of finite elements. We do not dwell here on the problems of
  time, calibration of numerical parameters and on the sensitivity aspects vis-à-vis data,

* to illustrate the formidable potentialities and facilities afforded by the ASTER / PYTHON  language in the command 
  file of a study (test, loop, display, calculation, personal macro-command, interactivity). The official benchmarks 
  being calibrated to operate in batch, some of these aspects have therefore been "commented" in the command file.

From a computer validation point of view, this tutorial allows to test the non-regression of various coupling 
calculations of error map / procedure of refinement-coarsening in mechanics, but also the options of "pre- and 
post-processing" of these computations (smoothing of the stresses at the nodes, passage from an error by element 
to an error at the nodes by element).

Each model is associated with a practical question.

