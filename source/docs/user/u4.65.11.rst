.. role:: red
.. role:: blue
.. role:: green
.. role:: gray
.. role:: python(code)
   :language: python
.. |br| raw:: html

   <br/>

.. index:: ! CREA_ELEM_SSD

.. _u4.65.11:

************************************************************************
**[U4.65.11]** : Operator ``CREA_ELEM_SSD``
************************************************************************

.. _u4.65.11.1:

Purpose
=======

This operator aims to facilitate the task of creation of a dynamic macro-element.
:red:`CREA_ELEM_SSD` calculates the basis projection vectors, defines the interfaces, and creates
the dynamic macro-element.

It produces a concept of type :green:`macr_elem_dyna`.

The dynamic macro-element created is ready to be assembled with other dynamic macro-elements
in order to obtain a generalized model. The creation of this generalized model can be done with
operator :red:`ASSE_ELEM_SSD`.

:red:`CREA_ELEM_SSD` is not intended to be able to create all the possible macro-elements. It allows the
creation of standard macro-elements which correspond to the majority of the cases. The basis vectors
of these macro-elements are of the :blue:`CLASSIQUE` type or of :blue:`RITZ` type with interface modes.

For special projection bases, the user is required to create them manually using standard operators.

 
.. _u4.65.11.2:

Syntax
======

::

   macro_el [macr_elem_dyna] = CREA_ELEM_SSD
   (
     ♦ MODELE = model,                                        [model]
     ◊ NUME_DDL = num_dof,                                    [nume_ddl]
     ♦ CHAM_MATER = mat_fld,                                  [cham_mater]
     ◊ CARA_ELEM = el_char,                                   [cara_elem]

     ◊ CHARGE = load,                                       / [char_meca]
                                                            / [char_ther]
                                                            / [char_acou]

     ♦ INTERFACE = _F(
                       ♦ NOM = 'interface_name',              [Kn]
                       ♦ TYPE = / 'MNEAL',
                                / 'CRAIGB',
                                / 'CB_HARMO',
                       / ◊ GROUP_NO = node_grp,               [l_gr_node]
                       / ◊ MASQUE = l_dof,                    [l_ddl]
                       ◊ FREQ = / 1.                          [DEFAULT]
                                / ifreq,                      [R]
                     ),

     ♦ BASE_MODALE = _F(
                         ♦ TYPE = / 'CLASSIQUE',
                                  / 'RITZ',

                         # If TYPE == 'RITZ':

                           ◊ TYPE_MODE = / 'INTERFACE'        [DEFAULT]
                                         / 'STATIQUE'

                           # If TYPE_MODE == 'INTERFACE':

                             ◊ NMAX_MODE_INTF = / 10          [DEFAULT]
                                                / nmint,      [I]
                       ),

     ◊ CALC_FREQ = F(
                      ◊ STOP_ERREUR = / 'OUI'                 [DEFAULT]
                                      / 'NON'
                      ◊ OPTION = / 'PLUS_PETITE'              [DEFAULT]
                                 / 'BANDE'
                                 / 'CENTRE'
                                 / 'SANS'

                      # One chooses OPTION = 'SANS' if one does not want to use
                      # normal modes. If not, see keyword CALC_FREQ of
                      # CALC_MODES[U4.52.02] for the options of calculation of
                      # normal modes.
                    ),

    ◊ SOLVEUR = _F(see U4.50.01)

    ◊ INFO = / 1,                                             [DEFAULT]
             / 2
    )


.. _u4.65.11.3:

Operands
========

This operator makes it possible to easily create a real dynamic macro-element (without damping) by
inputting only the mechanical characteristics of the model and the interfaces associated with the
substructure.

It performs the following operations:

* creation of the stiffness and mass matrices of the substructure
* calculation of the normal modes of the substructure
* definition of the interfaces associated with the substructure
* calculation of the static modes or the interface modes
* definition of the projection basis
* calculation of the dynamic macro-element.

For its usage in ``Code_Aster``, the user can explore models "e" and "f" of the test case
``SDLD106``. These models correspond respectively to models "b" and "d" of the same test case.


.. _u4.65.11.3.1:

Operand :blue:`MODELE`
----------------------

::

   ♦ MODELE = model

The model which contains the elements constituting the substructure.

Operand :blue:`NUME_DDL`
------------------------

::

   ◊ NUME_DDL = num_dof

Classification of the degrees of freedom assigned to the substructure.

.. note::

   This classification is necessary if one wants to calculate the response of the assembled structure due
   to a load applied to the substructure. We need the numbering in order to be able to assemble the
   nodal vector according to the classification of the degrees of freedom of the sub-structure.
   One then writes: :blue:`NUME_DDL = CO('num_dof')`, where ``num_dof`` indicates the name
   which one wishes given to the degrees of freedom of the substructure.

Operand :blue:`CHAM_MATER`
--------------------------

::
   
  ♦ CHAM_MATER = mat_fld

Name of the material field where the characteristics of the materials are defined.

Operand :blue:`CARA_ELEM`
-------------------------

::

   ◊ CARA_ELEM = el_char

Characteristics of beam, shell or discrete elements if the substructure contains some.

Operand :blue:`CHARGE`
----------------------

::

   ◊ CHARGE = load

Loading applied to the substructure.

Keyword :blue:`INTERFACE`
-------------------------

The keyword factor :blue:`INTERFACE` makes it possible to define the interfaces associated with
the substructure. The keywords associated with this keyword factor are identical to the keyword factor
:blue:`INTERFACE` of operator :red:`DEFI_INTERF_DYNA` :ref:`[U4.64.01] <u4.64.01>`. The value of the
frequency used is also entered here for the calculation of harmonic constrained modes. The default, for
this frequency is equal to 1.

We apply the overload rule. If the frequency value differs between the different interfaces then the user
is informed by means of an alarm which specifies the value of the frequency effectively taken into account.

Keyword :blue:`BASE_MODALE`
---------------------------

The keyword factor :blue:`BASE_MODALE` makes it possible to specify the type of basis on which the
substructure is projected.

Operand :blue:`TYPE`
^^^^^^^^^^^^^^^^^^^^

This operand defines the type of the projection basis.

::

   ♦ TYPE = / 'CLASSIQUE'
            / 'RITZ'

A basis of the :blue:`CLASSIQUE` type consists of normal modes and of constrained modes or of attachment
modes depending on the type of interface. The calculation of the normal modes is done according to the
indications provided in the keyword factor :blue:`CALC_FREQ`.

Operand :blue:`TYPE_MODE`
^^^^^^^^^^^^^^^^^^^^^^^^^

::

   ◊ TYPE_MODE = / 'INTERFACE'    [DEFAULT]
                 / 'STATIQUE'

Option :blue:`TYPE = 'RITZ'` makes it possible to specify the type of modes to add to the normal modes.
The user can choose modes of the :blue:`STATIQUE` type (static sum with the degrees of freedom of
interface nodes) or of :blue:`INTERFACE` type (interface modes). For the particular case where we do not
wish to use normal modes, one chooses :blue:`OPTION = 'SANS'` in the keyword :blue:`CALC_FREQ`.

Operand :blue:`NMAX_MODE_INTF`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

   ◊ NMAX_MODE_INTF = / 10        [DEFAULT]
                      / nmintf

:blue:`NMAX_MODE_INTF` corresponds to the number of interface modes to be taken into account. We consider
here the first ``nmintf`` interface modes (``nmintf`` > 0).

Keyword :blue:`CALC_FREQ`
-------------------------

This keyword factor makes it possible to choose the frequency contents of the normal modes. The operands
associated with this keyword are identical to those defined for :red:`CALC_MODES`
:ref:`[U4.52.02] <u4.52.02>`.  For the :blue:`BANDE` option, the :blue:`FREQ` operand allows you to enter
a list of frequencies and performs modal calculations on the different intervals of the list.
One chooses :blue:`OPTION = 'SANS'` if one does not wish to calculate normal modes.

The keyword :blue:`STOP_ERREUR` makes it possible to indicate to the operator if he must stop
:blue:`('OUI')` or continue :blue:`('NON')` if one of the quality control criteria of the calculated modes
is not satisfied.  This keyword is equivalent to the keyword :blue:`STOP_ERREUR` of the keyword factor
:blue:`VERI_MODE` of :red:`CALC_MODES`.

.. note::

   In order not to overload the command, this keyword factor does not make it possible to enter the
   parameters :blue:`COEF_DIM_ESPACE`, :blue:`NMAX_ITER_SHIFT`, :blue:`PREC_SHIFT`, :blue:`SEUIL_FREQ`
   and :blue:`STOPFREQ_VIDE`. On the other hand, the user can enter the dimension of the subspace
   (:blue:`DIM_SOUS_ESPACE`) when needed. The values ​​chosen for the calculation of the normal modes
   are the default values ​​of these parameters in :red:`CALC_MODES`.

Keyword :blue:`SOLVEUR`
-----------------------

Optional keyword, see **[U4.50.01]**.

Operand :blue:`INFO`
--------------------

This operand makes it possible to print information on the macro-element to the file 'MESSAGE'.

Example of use
===============

This example is extracted from the ``SDLS106e`` test case.

::
   
  MACEL1 = CREA_ELEM_SSD(MODELE = MODEL1,
                         CHARGE = CHARGE_1,
                         CHAM_MATER = CHAMAT1,
                         CARA_ELEM = PARAM1,
                         INTERFACE = _F(NOM = 'LEFT',
                                        TYPE = 'CRAIGB',
                                        GROUP_NO = 'LEFT',
                                        MASQUE = ('DX', 'DY')),
                         BASE_MODALE = _F(TYPE = 'RITZ',
                                          NMAX_MODE_INTF = 20),
                         CALC_FREQ = _F(NMAX_FREQ = 6))
