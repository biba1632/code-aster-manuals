# Cube composed of six quadratic tetrahedral elements

import sys
import salome
import salome_notebook
from salome.smesh import smeshBuilder
import SMESH, SALOMEDS

ExportPATH="/home/banerjee/Salome/ssnp14/"

# Initialize
salome.salome_init()
notebook = salome_notebook.NoteBook()
smesh = smeshBuilder.New()

# Create mesh
mesh = smesh.Mesh()
smesh.SetName(mesh, 'mesh')

# Add nodes
node1 = mesh.AddNode(0.00000000E+00, 1.00000000E+00, 0.00000000E+00)
node2 = mesh.AddNode(1.00000000E+00, 1.00000000E+00, 0.00000000E+00)
node3 = mesh.AddNode(0.00000000E+00, 0.00000000E+00, 0.00000000E+00)
node4 = mesh.AddNode(1.00000000E+00, 0.00000000E+00, 0.00000000E+00)
node5 = mesh.AddNode(0.00000000E+00, 1.00000000E+00, 1.00000000E+00)
node6 = mesh.AddNode(1.00000000E+00, 1.00000000E+00, 1.00000000E+00)
node7 = mesh.AddNode(0.00000000E+00, 0.00000000E+00, 1.00000000E+00)
node8 = mesh.AddNode(1.00000000E+00, 0.00000000E+00, 1.00000000E+00)
node10 = mesh.AddNode(0.00000000E+00, 5.00000000E-01, 5.00000000E-01)
node11 = mesh.AddNode(5.00000000E-01, 1.00000000E+00, 5.00000000E-01)
node12 = mesh.AddNode(0.00000000E+00, 1.00000000E+00, 5.00000000E-01)
node13 = mesh.AddNode(5.00000000E-01, 1.00000000E+00, 0.00000000E+00)
node14 = mesh.AddNode(0.00000000E+00, 5.00000000E-01, 0.00000000E+00)
node15 = mesh.AddNode(1.00000000E+00, 1.00000000E+00, 5.00000000E-01)
node16 = mesh.AddNode(5.00000000E-01, 5.00000000E-01, 5.00000000E-01)
node17 = mesh.AddNode(5.00000000E-01, 5.00000000E-01, 0.00000000E+00)
node18 = mesh.AddNode(1.00000000E+00, 5.00000000E-01, 0.00000000E+00)
node19 = mesh.AddNode(0.00000000E+00, 0.00000000E+00, 5.00000000E-01)
node20 = mesh.AddNode(5.00000000E-01, 0.00000000E+00, 0.00000000E+00)
node21 = mesh.AddNode(5.00000000E-01, 0.00000000E+00, 5.00000000E-01)
node22 = mesh.AddNode(1.00000000E+00, 0.00000000E+00, 5.00000000E-01)
node23 = mesh.AddNode(1.00000000E+00, 5.00000000E-01, 5.00000000E-01)
node24 = mesh.AddNode(0.00000000E+00, 5.00000000E-01, 1.00000000E+00)
node25 = mesh.AddNode(5.00000000E-01, 1.00000000E+00, 1.00000000E+00)
node26 = mesh.AddNode(5.00000000E-01, 5.00000000E-01, 1.00000000E+00)
node27 = mesh.AddNode(1.00000000E+00, 5.00000000E-01, 1.00000000E+00)
node28 = mesh.AddNode(5.00000000E-01, 0.00000000E+00, 1.00000000E+00)

# Add groups for the nodes
node_1 = mesh.CreateEmptyGroup(SMESH.NODE, 'node_1')
node_2 = mesh.CreateEmptyGroup(SMESH.NODE, 'node_2')
node_3 = mesh.CreateEmptyGroup(SMESH.NODE, 'node_3')
node_4 = mesh.CreateEmptyGroup(SMESH.NODE, 'node_4')
node_5 = mesh.CreateEmptyGroup(SMESH.NODE, 'node_5')
node_6 = mesh.CreateEmptyGroup(SMESH.NODE, 'node_6')
node_7 = mesh.CreateEmptyGroup(SMESH.NODE, 'node_7')
node_8 = mesh.CreateEmptyGroup(SMESH.NODE, 'node_8')
node_10 = mesh.CreateEmptyGroup(SMESH.NODE, 'node_10')
node_11 = mesh.CreateEmptyGroup(SMESH.NODE, 'node_11')
node_12 = mesh.CreateEmptyGroup(SMESH.NODE, 'node_12')
node_13 = mesh.CreateEmptyGroup(SMESH.NODE, 'node_13')
node_14 = mesh.CreateEmptyGroup(SMESH.NODE, 'node_14')
node_15 = mesh.CreateEmptyGroup(SMESH.NODE, 'node_15')
node_16 = mesh.CreateEmptyGroup(SMESH.NODE, 'node_16')
node_17 = mesh.CreateEmptyGroup(SMESH.NODE, 'node_17')
node_18 = mesh.CreateEmptyGroup(SMESH.NODE, 'node_18')
node_19 = mesh.CreateEmptyGroup(SMESH.NODE, 'node_19')
node_20 = mesh.CreateEmptyGroup(SMESH.NODE, 'node_20')
node_21 = mesh.CreateEmptyGroup(SMESH.NODE, 'node_21')
node_22 = mesh.CreateEmptyGroup(SMESH.NODE, 'node_22')
node_23 = mesh.CreateEmptyGroup(SMESH.NODE, 'node_23')
node_24 = mesh.CreateEmptyGroup(SMESH.NODE, 'node_24')
node_25 = mesh.CreateEmptyGroup(SMESH.NODE, 'node_25')
node_26 = mesh.CreateEmptyGroup(SMESH.NODE, 'node_26')
node_27 = mesh.CreateEmptyGroup(SMESH.NODE, 'node_27')
node_28 = mesh.CreateEmptyGroup(SMESH.NODE, 'node_28')
nbAdd = node_1.Add([1])
nbAdd = node_2.Add([2])
nbAdd = node_3.Add([3])
nbAdd = node_4.Add([4])
nbAdd = node_5.Add([5])
nbAdd = node_6.Add([6])
nbAdd = node_7.Add([7])
nbAdd = node_8.Add([8])
nbAdd = node_10.Add([9])
nbAdd = node_11.Add([10])
nbAdd = node_12.Add([11])
nbAdd = node_13.Add([12])
nbAdd = node_14.Add([13])
nbAdd = node_15.Add([14])
nbAdd = node_16.Add([15])
nbAdd = node_17.Add([16])
nbAdd = node_18.Add([17])
nbAdd = node_19.Add([18])
nbAdd = node_20.Add([19])
nbAdd = node_21.Add([20])
nbAdd = node_22.Add([21])
nbAdd = node_23.Add([22])
nbAdd = node_24.Add([23])
nbAdd = node_25.Add([24])
nbAdd = node_26.Add([25])
nbAdd = node_27.Add([26])
nbAdd = node_28.Add([27])

# Add volume elements
vol1 = mesh.AddVolume([1, 7, 6, 5, 9, 25, 10, 11, 23, 24])
vol2 = mesh.AddVolume([2, 1, 6, 7, 12, 10, 14, 15, 9, 25])
vol3 = mesh.AddVolume([3, 2, 1, 7, 16, 12, 13, 18, 15, 9])
vol4 = mesh.AddVolume([3, 4, 2, 7, 19, 17, 16, 18, 20, 15])
vol5 = mesh.AddVolume([7, 8, 4, 6, 27, 21, 20, 25, 26, 22])
vol6 = mesh.AddVolume([2, 6, 7, 4, 14, 25, 15, 17, 22, 20])

# Add group for the volumes
cube = mesh.CreateEmptyGroup(SMESH.VOLUME, 'cube')
tet1 = mesh.CreateEmptyGroup(SMESH.VOLUME, 'tet1')
tet2 = mesh.CreateEmptyGroup(SMESH.VOLUME, 'tet2')
tet3 = mesh.CreateEmptyGroup(SMESH.VOLUME, 'tet3')
tet4 = mesh.CreateEmptyGroup(SMESH.VOLUME, 'tet4')
tet5 = mesh.CreateEmptyGroup(SMESH.VOLUME, 'tet5')
tet6 = mesh.CreateEmptyGroup(SMESH.VOLUME, 'tet6')
nbAdd = cube.Add([1, 2, 3, 4, 5, 6])
nbAdd = tet1.Add([1])
nbAdd = tet2.Add([2])
nbAdd = tet3.Add([3])
nbAdd = tet4.Add([4])
nbAdd = tet5.Add([5])
nbAdd = tet6.Add([6])

# Add face elements
face7 = mesh.AddFace([4, 2, 6, 17, 14, 22])
face8 = mesh.AddFace([4, 6, 8, 22, 26, 21])
face9 = mesh.AddFace([2, 1, 6, 12, 10, 14])
face10 = mesh.AddFace([1, 5, 6, 11, 24, 10])
face11 = mesh.AddFace([5, 7, 1, 23, 9, 11])
face12 = mesh.AddFace([7, 3, 1, 18, 13, 9])
face13 = mesh.AddFace([3, 4, 7, 19, 20, 18])
face14 = mesh.AddFace([4, 8, 7, 21, 27, 20])

# Add groups for the faces
left_face = mesh.CreateEmptyGroup( SMESH.FACE, 'left_face' )
right_face = mesh.CreateEmptyGroup( SMESH.FACE, 'right_face' )
top_face = mesh.CreateEmptyGroup( SMESH.FACE, 'top_face' )
bot_face = mesh.CreateEmptyGroup( SMESH.FACE, 'bot_face' )
nbAdd = left_face.Add([11, 12])
nbAdd = right_face.Add([7, 8])
nbAdd = top_face.Add([9, 10])
nbAdd = bot_face.Add([13, 14])

# Export mesh
mesh.ExportMED( r''+ExportPATH+'ssnp14g.mmed'+'')

# Update GUI object tree
if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()
