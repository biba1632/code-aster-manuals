.. _r00.00.01:

********************************
**R0.00.01** :  Reference guides
********************************

Algorithms and methodologies
============================

Modal analysis
--------------

* :ref:`r5.01.01`
* **[R5.01.02]** Solution of the quadratic modal problem (QEP)
* **[R5.01.03]** Modal parameters and standard of the eigenvectors
* **[R5.01.04]** Procedure of enumeration of eigenvalues
* **[R5.07.01]** Calculation of non-linear modes

Transient dynamic analysis
--------------------------

* **[R4.05.01]** Seismic solution by transient analysis
* **[R5.05.01]** Solution of a differential equation of the second order by the method of NIGAM
* **[R5.05.02]** Algorithms of direct integration of the operator DYNA_LINE_TRAN
* **[R5.05.04]** Modeling of damping in linear dynamics
* **[R5.05.05]** Dynamic nonlinear algorithm
* **[R5.06.03]** Modeling of the shocks and friction in transient analysis by modal recombination
* **[R5.06.04]** Algorithms of temporal integration of the operator DYNA_TRAN_MODAL

Harmonic dynamic analysis
----------------------------

* **[R5.05.03]** Harmonic solution

Spectral analysis and random solutions
--------------------------------------

* **[R4.05.02]** Stochastic approach for the seismic analysis
* **[R4.05.03]** Seismic solution by spectral method
* **[R7.10.01]** Examination of the random solutions

Dynamic sub-structuring
-----------------------

* :ref:`r4.06.02` 
* **[R4.06.03]** Cyclic dynamic sub-structuring
* **[R5.06.01]** Reduction of model in linear and non-linear dynamics: Method of RITZ


Extrapolation of experimental measurements
------------------------------------------

* **[R7.20.02]** Extrapolation of measurements on a digital model in dynamics


Soil-structure interaction
--------------------------

* **[R4.05.04]** Interaction ground-structure with space variability (operator DYNA_ISS_VARI)
* **[R4.05.05]** Generation of seismic signals
* **[R4.05.06]** Straight-line method are equivalent for the wave propagation in 1D
* **[R5.05.09]** Calculation of reconstituted signals and the matrix transfer function transfers 


Fracture
--------

* **[R7.02.01]** Rate of refund of energy in linear thermoelasticity
* **[R7.02.03]** Rate of refund of energy in non-linear thermoelasticity
* **[R7.02.05]** Calculation of the coefficients of intensity of constraints in plane linear thermoelasticity
* **[R7.02.06]** Model of Weibull and Rice and Tracey
* **[R7.02.07]** Rate of refund of energy in thermo-élasto-plasticity - approach GTP
* **[R7.02.08]** Calculation of the factors of intensity of the constraints by extrapolation of the field of displacements
* **[R7.02.09]** Identification of the model of Weibull
* **[R7.02.10]** simplified Analysis of harmfulness of defect by the method K-beta
* **[R7.02.13]** Algorithms of propagation of cracks with X-FEM
* **[R7.02.16]** Gp method: an energy approach of the prediction of cleavage
* **[R7.02.17]** equivalent Crack starting from a field of damage
* **[R7.02.18]** Hydro-mechanical element coupled with XFEM
* **[R7.02.19]** Cohesive elements with X-FEM

Recalibration
-------------
 
* **[R4.03.06]** Algorithm of recalibration

Probabilistic methods
---------------------

* **[R4.03.05]** Model parametric and not-parametric probabilists in dynamics

Boundary conditions and of connections
--------------------------------------

* **[R3.03.01]** Dualisation of the boundary conditions
* **[R3.03.02]** Conditions of solid connection of body
* **[R3.03.03]** Connection 2D-beam and 3D-beam
* **[R3.03.05]** Elimination of the boundary conditions dualized
* **[R3.03.06]** Connection hull-beam
* **[R3.03.08]** linear Relations kinematics of type RBE3
* **[R3.03.0 9 ]** Connection Harlequin 3D – Beam 

Non-linear mechanics
--------------------

* **[R5.03.01]** quasi-static nonlinear Algorithm (operator STAT_NON_LINE)
* **[R5.03.03]** Taking into account of the assumption of the plane constraints in the nonlinear behaviors
* **[R5.03.14]** implicit Integration and clarifies relations of nonlinear behavior
* **[R5.03.80]** Methods of piloting of the loading
* **[R5.03.81]** Method IMPLEX
* **[R7.01.02]** Modeling of the cables of prestressing

Algebraic Solvers
-----------------

* **[R6.01.02]** General information on the combined gradient: GCPC Aster and use of PETSc
* **[R6.02.01]** In connection with the methods of decomposition of the GAUSS type
* **[R6.02.02]** linear Solveur by the method multifrontale MULT_FRONT
* **[R6.02.03]** General information on the direct linear solveurs and use of MUMPS
* **[R6.03.01]** Resolution of nonregular systems by a method of decomposition in singular values

Fatigue
-------

* **[R7.04.01]** Estimate of the lifetime in fatigue to a large number of cycles and oligocyclic
* **[R7.04.02]** Estimate of fatigue under random request
* **[R7.04.04]** multiaxial Criteria of starting in fatigue

Coupled Thermo-hydro-mechanics
------------------------------

* **[R7.01.10]** Modelings THHM: General information and algorithms
* **[R7.01.34]** Diagrams finished volumes SUSHI for the modeling of the miscible unsaturated flows

Contact and friction
--------------------

* :ref:`r5.03.50`
* :ref:`r5.03.52`
* :ref:`r5.03.53`
* :ref:`r5.03.54`

Large strains and large displacements
-------------------------------------

* **[R3.03.04]** Efforts external of pressure in great displacements
* **[R3.03.07]** following Pressure for the elements of voluminal hulls
* **[R5.03.21]** Modeling élasto (visco) plastic with isotropic work hardening in great deformations
* **[R5.03.22]** Law of behavior in great rotations and small deformations
* **[R5.03.24]** Model of great deformations GDEF_LOG and GDEF_HYPO_ELAS
* **[R5.03.40]** static and dynamic Modeling of the beams in great rotations

Buckling
--------
 
* **[R7.05.01]** Criteria of structural stability
 
Heat transfer
-------------

* **[R3.06.07]** Diagonalisation of the thermal matrix of mass
* **[R5.02.01]** Algorithm of linear thermics transient
* **[R5.02.02]** Thermal nonlinear
* **[R5.02.04]** Thermal nonlinear in pointer

Fluid-structure interaction
---------------------------

* **[R4.07.02]** Modeling of the turbulent excitations
* **[R4.07.03]** Calculation of matrix of mass added on modal basis
* **[R4.07.04]** Coupling fluid-structure for the tubular structures and the coaxial hulls
* **[R4.07.05]** Homogenisation of a network of beams bathing in a fluid
* **[R4.07.07]** Identification of fluid efforts

Limit analysis
--------------

* **[R7.07.01]** Calculation of load limits by the method of Norton-Hoff-Friaâ, behavior NORTON_HOFF

Error estimators
----------------

* **[R4.10.01]** Estimator of error of ZHU-ZIENKIEWICZ
* **[R4.10.02]** Estimator of error in residue
* **[R4.10.03]** Indicating of space error in residue for transient thermics
* **[R4.10.04]** Detection of the singularities and calculation of a map of size of elements
* **[R4.10.05]** Indicating of error in residue for modelings HM
* **[R4.10.06]** Estimators of error in quantities of interest
* **[R4.10.07]** Error analysis in relation of behavior in dynamics under a frequential formulation 

Postprocessing
--------------

* **[R3.06.03]** Calculation of the constraints to the nodes by local smoothing
* **[R4.09.01]** Assessment of energy into thermomechanical
* **[R4.20.01]** Indicating of discharge and loss of proportionality of the loading in elastoplasticity
* **[R7.04.03]** Postprocessing according to the RCC-M
* **[R7.04.05]** Calculation algorithm of the densities of reinforcement
* **[R7.04.10]** Operator of calculation of wear
* **[R7.10.02]** Postprocessing of modal calculations with shock
* **[R7.10.03]** Postprocessing of calculations of lines of trees: diagram of Campbell
* **[R7.20.01]** Projection of a field on a grid

Interaction with external tools
-------------------------------

* **[R7.06.01]** Modeling of the deformation of the fuel assemblies with the tool MAC3COEUR 


Constitutive models
===================

General information
-------------------

* **[R5.03.27]** Mechanical models for simulations

Discrete elements and of beams
------------------------------

* :ref:`r5.03.09`
* **[R5.03.17]** Relations of behavior of the discrete elements 


Thermoelasticity
----------------

* **[R4.01.02]** anisotropic Elasticity
* **[R4.08.01]** Calculation of the thermal deformation
* **[R5.03.19]** hyperelastic Law of behavior: almost incompressible material
* **[R5.03.20]** Relation of nonlinear elastic behavior in great displacements


Incremental elastoplasticity
----------------------------

* **[R5.03.16]** elastoplastic Behaviour with isotropic and kinematic work hardening mixed linear


Elastoviscoplasticity under irradiation
---------------------------------------

* **[R5.03.23]** elastoplastic Behaviour under irradiation of metals: application to the interns of tank


Behavior of polycrystalline metals
----------------------------------

* **[R5.03.11]** mono and polycrystalline Behaviors elastoviscoplastic

Géo-materials and porous materials
--------------------------------------

* **[R7.01.11]** Model of behavior THHM
* **[R7.01.13]** Law CJS in géomechanics
* **[R7.01.14]** Law of behavior CAM_CLAY
* **[R7.01.15]** Law of behavior of LAIGLE
* **[R7.01.16]** Integration of the behaviors of Drucker-Prager DRUCK_PRAGER and DRUCK_PRAG_N_A and postprocessing
* **[R7.01.17]** Law of behavior of the porous environments: model of BARCELONA
* **[R7.01.18]** Law of behavior of HOEK_BROWN modified
* **[R7.01.22]** viscoplastic Law of behavior VISC_DRUC_PRAG
* **[R7.01.23]** cyclic Law of behavior of Hujeux for the grounds
* **[R7.01.24]** viscoplastic Law of behavior LETK
* **[R7.01.28]** Law of Mohr-Coulomb
* **[R7.01.38]** Law of Iwan for the cyclic granular material behavior
* **[R7.01.40]** Model of behavior LKR


Behavior of concretes
---------------------

* **[R7.01.01]** Relation of behavior BETON_TO ARRANGE for the clean creep of the concrete
* **[R7.01.03]** Law of behavior BETON_DOUBLE_DP with double criterion Drücker Prager for the cracking and the compression of the concrete
* **[R7.01.04]** Law of behavior ENDO_ISOT_BETON
* **[R7.01.06]** Relation of behavior BETON_UMLV for the clean creep of the concrete
* :ref:`r7.01.08`
* **[R7.01.09]** Law of behavior ENDO_ORTH_BETON
* **[R7.01.12]** Modeling of thermohydration, the drying and the shrinking of the concrete
* **[R7.01.19]** Modeling of the coupling creep/plasticity for the concrete
* **[R7.01.26]** Law of behavior BETON_RAG
* **[R7.01.27]** Law of behavior BETON_REGLE_PR 
* **[R7.01.31]** Law of behavior of reinforced concrete plates GLRC_DAMAGE
* **[R7.01.32]** Law of behavior GLRC_DM
* **[R7.01.35]** Relation of behavior BETON_BURGER for the clean creep of the concrete
* **[R7.01.36]** Relation of behavior KIT_RGI 
* **[R7.01.37]**  Dissipative Homogenised Reinforced Concrete (DHRC) constitutive model devoted to reinforced concrete punts 

Behavior of the reinforcements in reinforced concrete structures
--------------------------------------------------------------------

* **[R3.08.10]** Element CABLE_GAINE
* **[R7.01.20]** Behavior of the steel subjected to corrosion
* **[R7.01.21]** Law of behavior (in 2D) for the connection steel/concrete: JOINT_BA

Damage
------

* **[R5.03.18]** Law of damage of a fragile elastic material
* **[R5.03.25]** Law of damage regularized ENDO_SCALAIRE
* **[R5.03.26]** quadratic Law of damage regularized ENDO_CARRE
* **[R5.03.28]** Law of damage to gradient ENDO_FISS_EXP
* **[R5.04.01]** nonlocal Modeling with gradient of internal variables
* **[R5.04.02]** nonlocal Modeling with gradient of deformation
* **[R5.04.03]** Modelings second gradient
* **[R5.04.04]** nonlocal Modeling with gradients of nodal damage GVNO
* **[R7.01.29]** Law of behavior ENDO_HETEROGENE
* **[R7.01.33]** Coupling élasto-plasticity-damage

Metallurgical models
---------------------

* **[R4.04.01]** Model of metallurgical behavior of steels
* **[R4.04.02]** Modeling élasto (visco) plastic with metallurgical transformations
* **[R4.04.03]** Law of behavior élasto (visco) plastic in great deformations with metallurgical transformations
* **[R4.04.04]** Model of metallurgical behavior of the zircaloy
* **[R4.04.05]** Model of élastovisqueux behavior  META_LEMA_ANI with taking into account of the metallurgy for the tubes of sheath of the fuel pin

Viscoelasticity
---------------

* :ref:`r5.03.08`
* **[R5.0 5.10 ]** Dynamic analysis of structures with viscoelastic materials having frequency depend properties 

Elastoviscoplasticity of metals
-------------------------------

* **[R5.03.02]** Integration of the relations of elastoplastic behavior of Von Mises
* **[R5.03.04]** Relations of behavior élasto-visco-plastic of Chaboche
* **[R5.03.05]** viscoplastic Relation of behavior TAHERI
* **[R5.03.06]** Model of Rousselier in great deformations 
* **[R5.03.07]** Model of Rousselier for the Ductile rupture 
* **[R5.03.12]** viscoplastic Behavior with effect of memory and restoration of Chaboche 
* **[R5.03.13]** viscoplastic Behavior with damage of HAYHURST 
* **[R5.03.15]** viscoplastic Behavior with damage of Chaboche
* **[R5.03.32]** Law of behaviour of the assembly ASSE_CORN

Cohesive laws and joints
------------------------

* **[R3.06.09]** Finite elements of joint mechanics and finite elements of joint coupled hydraulic
* **[R7.01.25]** Laws of behavior of the joints of stoppings: JOINT_MECA_RUPT and JOINT_MECA_FROT
* **[R7.02.11]** Laws of behavior cohesive CZM and piloting of the loading


Finite elements
===============

Basic finite elements
---------------------

* **[R3.01.00]** finite element method isoparametric
* :ref:`r3.01.01`
* **[R3.06.02]** linear Modeling of the elements of continuous medium in thermics
* **[R3.06.04]** Elements of Fourier for the axisymmetric structures
* **[R3.06.10]** quadrangular Element at a point of integration, stabilized by the method “assumed strain”
* **[R3.06.11]** hexahedral Element at a point of integration, stabilized by the method “Assumed Strain”
* **[R4.02.01]** Finite elements in acoustics
* **[R4.02.02]** vibroacoustic Elements
* **[R4.02.04]** Fluid Coupling - Structure with free surface
* **[R4.02.05]** Elements of absorbing border 


Structural finite elements
--------------------------

* **[R3.07.02]** digital Modeling of the mean structures: axisymmetric thermoelastoplastic hulls and 1D
* **[R3.07.03]** Elements of plate: modelings DKT, DST, DKTG and Q4g
* **[R3.07.04]** Finite elements of voluminal hulls
* **[R3.07.05]** voluminal Elements of hulls into nonlinear
* **[R3.07.06]** Treatment of offsetting for the elements of plate
* **[R3.07.07]** voluminal Element of hull SHB with 8 nodes
* **[R3.07.08]** Elements of voluminal hull SHB with 6.15 and 20 nodes
* **[R3.07.09]** Elements of plate: Modeling Q4GG
* **[R3.08.01]** “exact” Elements of beams (right and curved)
* **[R3.08.02]** Modeling of the cables
* **[R3.08.03]** Calculation of the characteristics of a beam of cross section unspecified
* **[R3.08.04]** Element of beam to 7 degrees of freedom for the taking into account of warping
* **[R3.08.05]** a finite element of cable-pulley
* **[R3.08.06]** Finite elements of right pipe and curve
* **[R3.08.07]** Elements MEMBRANE and GRILLE_MEMBRANE
* **[R3.08.08]** multifibre Element of beam (right)
* **[R3.08.09]** multifibre Beams in great displacements
* **[R3.11.01]** Formulation of a model of thermics for the thin hulls
* **[R4.01.01]** Pre and Postprocessing for the thin hulls “composite”
* **[R4.02.03]** Beam élasto-acoustics
* **[R5.05.07]** gyroscopic Matrix of the right beams and the discs
* **[R5.05.08]** Modeling of the rotors fissured by equivalent stiffness function of the swing angle

Incompressible finite elements
------------------------------

* :ref:`r3.06.08`

Joint, interface and discontinuity elements
-------------------------------------------

* **[R3.06.09]** Finite elements of joint mechanics and finite elements of joint coupled hydraulic
* **[R3.06.13]** Finite   element   of   mixed   interface   for   models   of   cohesive   zone   (xxx_INTERFACE  and xxx_INTERFACE_S)
* :ref:`r7.02.12`
* **[R7.02.14]** Element with internal discontinuity
* **[R7.02.15]** Modeling of the cracks with hydro-mechanical coupling in saturated porous environment

