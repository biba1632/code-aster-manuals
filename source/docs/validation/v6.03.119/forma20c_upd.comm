# FORMA20B : Quadratic triangles (TRIA6) - free refinement
import numpy as NP

#---------------------------------
# Initialize Python variables
#---------------------------------

# Reference values after four uniform remeshing runs on TRIA6
ener_ref =  0.102242
disp_ref = -0.0614777

# Initialization of Python lists
energy_vs_refine     = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
disp_y_vs_refine     = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
err_energy_vs_refine = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
err_disp_y_vs_refine = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]

energy_mat     = NP.zeros((len(energy_vs_refine), 2))
disp_y_mat     = NP.zeros((len(disp_y_vs_refine), 2))
err_energy_mat = NP.zeros((len(err_energy_vs_refine), 2))
err_disp_y_mat = NP.zeros((len(err_disp_y_vs_refine), 2))

for i in range(len(energy_vs_refine)) :
    energy_mat[i][0]     = i
    disp_y_mat[i][0]     = i
    err_energy_mat[i][0] = i
    err_disp_y_mat[i][0] = i

    energy_mat[i][1]     = 0.0
    disp_y_mat[i][1]     = 0.0
    err_energy_mat[i][1] = 0.0
    err_disp_y_mat[i][1] = 0.0

# Number of remeshing runs (initial mesh = mesh 0)
nb_calc = 4

# Initial tables
nb_calc1 = nb_calc + 1
nb_calc2 = nb_calc1 + 1

mesh    = [None]*nb_calc1
model   = [None]*nb_calc1
mater   = [None]*nb_calc1
u_bc    = [None]*nb_calc1
f_bc    = [None]*nb_calc1
#linear  = [None]*nb_calc1
nonlin  = [None]*nb_calc1
result  = [None]*nb_calc1
energy  = [None]*nb_calc1
disp_y  = [None]*nb_calc1

#-------------------------------------------------------------------
# Aster calculation
#-------------------------------------------------------------------
DEBUT(LANG = 'EN',
      PAR_LOT = 'NON')

# Material
steel = DEFI_MATERIAU(ELAS = _F(E = 210.E3,
                                NU = 0.2),
                      ECRO_LINE = _F(SY = 1.0E6,
                                     D_SIGM_EPSI = 0.))

# Time instants and time function for STAT_NON_LINE
l_times = DEFI_LIST_REEL(DEBUT = 0.0,
                         INTERVALLE = _F(JUSQU_A = 1.0,
                                         NOMBRE = 1))
f_times = DEFI_FONCTION(NOM_PARA = 'INST',
                        VALE = (0.0, 0.0,
                                1.0, 1.0))

# Read mesh and get info
num_calc=0
mesh[num_calc] = LIRE_MAILLAGE(FORMAT = 'MED',
                               INFO = 2,
                               VERI_MAIL = _F())

# Define a new node group on the mesh
mesh[num_calc] = DEFI_GROUP(reuse = mesh[num_calc],
                            MAILLAGE = mesh[num_calc],
                            CREA_GROUP_NO = _F(TOUT_GROUP_MA = 'OUI'))

# End calculation is there is a problem
#FIN();

# Python loop for error indicator / remeshing
for num_calc in range(0, nb_calc1) :

   # Set up calculation with the current mesh: mesh[num_calc]
   # Using plane stress elements C_PLAN
   model[num_calc] = AFFE_MODELE(MAILLAGE = mesh[num_calc],
                                 AFFE = _F(TOUT = 'OUI',
                                           PHENOMENE = 'MECANIQUE',
                                           MODELISATION = 'C_PLAN'))
   mater[num_calc] = AFFE_MATERIAU(MAILLAGE = mesh[num_calc],
                                   AFFE = _F(GROUP_MA = 'GM14',
                                   MATER = steel))

   # Boundary conditions (fixed disp + pressure load)
   u_bc[num_calc] = AFFE_CHAR_MECA(MODELE = model[num_calc],
                                   DDL_IMPO = _F(GROUP_MA = 'GM13',
                                                 DX = 0.0,
                                                 DY = 0.0))
   f_bc[num_calc] = AFFE_CHAR_MECA(MODELE = model[num_calc],
                                   PRES_REP = _F(GROUP_MA = 'GM12',
                                                 PRES = 1.E-1))

   # Linear calculation
   #linear[num_calc] = MECA_STATIQUE(MODELE = model[num_calc],
   #                                 CHAM_MATER = mater[num_calc],
   #                                 EXCIT = (_F(CHARGE = u_bc[num_calc]),
   #                                          _F(CHARGE = f_bc[num_calc])))

   # Nonlinear calculation
   nonlin[num_calc] = STAT_NON_LINE(MODELE = model[num_calc],
                                    CHAM_MATER = mater[num_calc],
                                    SOLVEUR = _F(POSTTRAITEMENTS = 'MINI'),
                                    EXCIT = (_F(CHARGE = u_bc[num_calc]),
                                             _F(CHARGE = f_bc[num_calc],
                                                FONC_MULT = f_times)),
                                    COMPORTEMENT = _F(RELATION = 'VMIS_ISOT_LINE',
                                                      TOUT = 'OUI'),
                                    INCREMENT = _F(LIST_INST = l_times))

   result[num_calc] = EXTR_RESU(RESULTAT = nonlin[num_calc],
                                ARCHIVAGE = _F(NUME_ORDRE = 1))

   # Add extra variables to the result 
   # - stresses at the nodes
   # - two types of error indicators (Z1 and Residual)
   result[num_calc] = CALC_CHAMP(reuse = result[num_calc],
                                 RESULTAT = result[num_calc],
                                 NUME_ORDRE = 1,
                                 CONTRAINTE = ('SIGM_ELNO'))

   result[num_calc] = CALC_ERREUR(reuse = result[num_calc],
                                  RESULTAT = result[num_calc],
                                  TOUT = 'OUI',
                                  NUME_ORDRE = 1,
                                  OPTION = ('ERZ1_ELEM', # Element-wise Z1 error indicator 
                                            'ERME_ELEM', # Element-wise residual
                                            'ERME_ELNO', # Node-wise residual
                                           ))

   # Write in MED format
   DEFI_FICHIER(UNITE = 55, 
                TYPE = 'BINARY')
   IMPR_RESU(FORMAT = 'MED', 
             UNITE = 55,
             RESU = _F(MAILLAGE = mesh[num_calc],
                       RESULTAT = result[num_calc],
                       NOM_CHAM = ('DEPL', 'SIGM_ELNO', 'ERME_ELNO')))
   DEFI_FICHIER(ACTION = 'LIBERER',
                UNITE = 55)

   # Calculate the strain energy
   energy[num_calc] = POST_ELEM(RESULTAT = result[num_calc],
                                ENER_POT = _F(TOUT = 'OUI'))

   # Calculate the displacement
   disp_y[num_calc] = POST_RELEVE_T(ACTION = _F(INTITULE = 'DISP_Y',
                                                GROUP_NO = 'GM10',
                                                RESULTAT = result[num_calc],
                                                NOM_CHAM = 'DEPL',
                                                RESULTANTE = ('DY'),
                                                OPERATION = 'EXTRACTION'))

   disp_y[num_calc] = CALC_TABLE(reuse = disp_y[num_calc],
                                 TABLE = disp_y[num_calc],
                                 ACTION = _F(OPERATION = 'FILTRE',
                                             NOM_PARA = 'NUME_ORDRE',
                                             CRIT_COMP = 'GT',
                                             VALE = 0))

   # Write tables
   IMPR_TABLE(UNITE = 8,
              TABLE = energy[num_calc],
              FORMAT_R = '1PE24.12',
              SEPARATEUR = ',')
   IMPR_TABLE(UNITE = 8,
              TABLE = disp_y[num_calc],
              FORMAT_R = '1PE24.12',
              SEPARATEUR = ',')

   # Transfer Aster data structures to Python via the tables
   energy_mat[num_calc][1] = energy[num_calc]['TOTALE', 1]
   err_energy_mat[num_calc][1] = abs((energy_mat[num_calc][1] - ener_ref)/ener_ref)*100
   disp_y_mat[num_calc][1]  = disp_y[num_calc]['DY', 1]
   err_disp_y_mat[num_calc][1]  = abs((disp_y_mat[num_calc][1] - disp_ref)/disp_ref)*100

   # Special test for TEST_FONCTION
   if num_calc == 0 :
      err_disp0 = abs((disp_y_mat[num_calc][1] - disp_ref)/disp_ref)*100
      err_ener0 = abs((energy_mat[num_calc][1] - ener_ref)/ener_ref)*100
   if num_calc == 4 :
      err_disp4 = abs((disp_y_mat[num_calc][1] - disp_ref)/disp_ref)*100
      err_ener4 = abs((energy_mat[num_calc][1] - ener_ref)/ener_ref)*100

   num_calc1 = num_calc + 1

   # Write results to screen
   print('**************')
   print('CONVERGENCE OF ENERGY = ', energy_mat[:num_calc1])
   print('**************')
   print('CONVERGENCE OF DISP = ', disp_y_mat[:num_calc1])
   print('**************')
   print('ERROR IN ENERGY = ', err_energy_mat[:num_calc1])
   print('**************')
   print('ERROR IN DISP = ', err_disp_y_mat[:num_calc1])

   # Exit is nb_calc is reached
   if num_calc == nb_calc :
     break

   # Update mesh concept name for next iteration
   mesh[num_calc1] = CO('mesh_%d' % (num_calc1))

   # Free mesh refinement with HOMARD
   # Start mesh : mesh[num_calc]
   # Refined mesh: mesh[num_calc1]
   MACR_ADAP_MAIL(ADAPTATION = 'RAFF_DERA',
                  MAILLAGE_N = mesh[num_calc],
                  MAILLAGE_NP1 = mesh[num_calc1],
                  RESULTAT_N = result[num_calc],
                  NOM_CHAM = 'ERME_ELEM',
                  NOM_CMP = 'NUEST',
                  CRIT_RAFF_PE = 0.2,
                  CRIT_DERA_PE = 0.2,
                  QUALITE = 'OUI',
                  TAILLE = 'OUI',
                  CONNEXITE = 'OUI')


#-------------------------------------------------------------------
# Verification tests
#-------------------------------------------------------------------
ERREFL0 = FORMULE(VALE = 'erfl0*1.',
                  erfl0 = err_disp0,
                  NOM_PARA = 'BIDON',)
ERREEN0 = FORMULE(VALE = 'eren0*1.',
                  eren0 = err_ener0,
                  NOM_PARA = 'BIDON',)
ERREFL4 = FORMULE(VALE = 'erfl4*1.',
                  erfl4 = err_disp4,
                  NOM_PARA = 'BIDON',)
ERREEN4 = FORMULE(VALE = 'eren4*1.',
                  eren4 = err_ener4,
                  NOM_PARA = 'BIDON',)

TEST_FONCTION(VALEUR = (_F(VALE_CALC = 0.10692930,
                           CRITERE = 'RELATIF',
                           VALE_PARA = 0.0,
                           NOM_PARA = 'BIDON',
                           FONCTION = ERREFL0),
                        _F(VALE_CALC = 0.12563700,
                           CRITERE = 'RELATIF',
                           VALE_PARA = 0.0,
                           NOM_PARA = 'BIDON',
                           FONCTION = ERREEN0),
                        _F(VALE_CALC = 0.01124863,
                           CRITERE = 'RELATIF',
                           NOM_PARA = 'BIDON',
                           VALE_PARA = 0.0,
                           FONCTION = ERREFL4),
                        _F(VALE_CALC = 0.01319903,
                           TOLE_MACHINE = 1.E-3,
                           CRITERE = 'RELATIF',
                           NOM_PARA = 'BIDON',
                           VALE_PARA = 0.0,
                           FONCTION = ERREEN4)))

# end
FIN();
