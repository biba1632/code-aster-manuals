#-----------------------------------------
# SSNP169 functions (for constant load)
#-----------------------------------------
import math
import ssnp169_config as con

def radius(x,y):
    return (x**2+y**2)**0.5
    
def theta(x,y):
    return math.atan2(y,x)

def press_c(x,y):
    return con.pres_cont_c

# annulus displacements (external)
def ux1_c(x,y):
    r = radius(x,y)
    th = theta(x,y)
    return (con.A11*r+con.B11/r)*math.cos(th)

def uy1_c(x,y):
    r = radius(x,y)
    th = theta(x,y)
    return (con.A11*r+con.B11/r)*math.sin(th)

# annulus displacements (internal)
def ux2_c(x,y):
    r = radius(x,y)
    th = theta(x,y)
    return (con.A12*r)*math.cos(th)

def uy2_c(x,y):
    r = radius(x,y)
    th = theta(x,y)
    return (con.A12*r)*math.sin(th)

# annulus polar stresses (external)
def SIGMA_rr_1_c(x,y):
    r = radius(x,y)
    th = theta(x,y)
    return con.E1/(1+con.nu1)*( con.A11-con.B11/r**2 + con.nu1/(1-2*con.nu1)*(2*con.A11) )

def SIGMA_thth_1_c(x,y):
    r = radius(x,y)
    th = theta(x,y)
    return con.E1/(1+con.nu1)*( con.A11+con.B11/r**2 + con.nu1/(1-2*con.nu1)*(2*con.A11) )
    
# annulus polar stresses (internal)
def SIGMA_rr_2_c(x,y):
    r = radius(x,y)
    th = theta(x,y)
    return con.E1/( (1+con.nu1)*(1-2*con.nu1) )*con.A12

def SIGMA_thth_2_c(x,y):
    r = radius(x,y)
    th = theta(x,y)
    return con.E1/( (1+con.nu1)*(1-2*con.nu1) )*con.A12
    
# annulus cartesian stresses (external)
def SIGMA_XX_1_c(x,y):
    r = radius(x,y)
    th = theta(x,y) 
    c = math.cos(th)
    s = math.sin(th)     
    return SIGMA_rr_1_c(x,y)*c**2 + SIGMA_thth_1_c(x,y)*s**2
    
def SIGMA_YY_1_c(x,y):
    r = radius(x,y)
    th = theta(x,y) 
    c = math.cos(th)
    s = math.sin(th)     
    return SIGMA_rr_1_c(x,y)*s**2 + SIGMA_thth_1_c(x,y)*c**2

def SIGMA_ZZ_1_c(x,y):    
    r = radius(x,y)
    th = theta(x,y) 
    return con.nu1*( SIGMA_XX_1_c(x,y) + SIGMA_YY_1_c(x,y) )

def SIGMA_XY_1_c(x,y):
    r = radius(x,y)
    th = theta(x,y) 
    c = math.cos(th)
    s = math.sin(th)     
    return ( SIGMA_rr_1_c(x,y) - SIGMA_thth_1_c(x,y) )*c*s

# annulus cartesian stresses (internal)
def SIGMA_XX_2_c(x,y):
    r = radius(x,y)
    th = theta(x,y) 
    c = math.cos(th)
    s = math.sin(th)     
    return SIGMA_rr_2_c(x,y)*c**2 + SIGMA_thth_2_c(x,y)*s**2
    
def SIGMA_YY_2_c(x,y):
    r = radius(x,y)
    th = theta(x,y) 
    c = math.cos(th)
    s = math.sin(th)     
    return SIGMA_rr_2_c(x,y)*s**2 + SIGMA_thth_2_c(x,y)*c**2

def SIGMA_ZZ_2_c(x,y):    
    r = radius(x,y)
    th = theta(x,y) 
    return con.nu2*( SIGMA_XX_2_c(x,y) + SIGMA_YY_2_c(x,y) )

def SIGMA_XY_2_c(x,y):
    r = radius(x,y)
    th = theta(x,y) 
    c = math.cos(th)
    s = math.sin(th)     
    return ( SIGMA_rr_2_c(x,y) - SIGMA_thth_2_c(x,y) )*c*s

# annulus strains (external)
def EPSI_XX_1_c(x,y):    
    return 1/con.E1*( (1+con.nu1)*SIGMA_XX_1_c(x,y) - con.nu1*(SIGMA_XX_1_c(x,y)+SIGMA_YY_1_c(x,y)+SIGMA_ZZ_1_c(x,y)) )
    
def EPSI_YY_1_c(x,y):    
    return 1/con.E1*( (1+con.nu1)*SIGMA_YY_1_c(x,y) - con.nu1*(SIGMA_XX_1_c(x,y)+SIGMA_YY_1_c(x,y)+SIGMA_ZZ_1_c(x,y)) )
    
def EPSI_ZZ_1_c(x,y):    
    return 1/con.E1*( (1+con.nu1)*SIGMA_ZZ_1_c(x,y) - con.nu1*(SIGMA_XX_1_c(x,y)+SIGMA_YY_1_c(x,y)+SIGMA_ZZ_1_c(x,y)) )

def EPSI_XY_1_c(x,y):
    return 1/con.E1*( (1+con.nu1)*SIGMA_XY_1_c(x,y) )

# annulus strains (internal)
def EPSI_XX_2_c(x,y): 
    return 1/con.E2*( (1+con.nu2)*SIGMA_XX_2_c(x,y) - con.nu2*(SIGMA_XX_2_c(x,y)+SIGMA_YY_2_c(x,y)+SIGMA_ZZ_2_c(x,y)) )
    
def EPSI_YY_2_c(x,y):
    return 1/con.E2*( (1+con.nu2)*SIGMA_YY_2_c(x,y) - con.nu2*(SIGMA_XX_2_c(x,y)+SIGMA_YY_2_c(x,y)+SIGMA_ZZ_2_c(x,y)) )

def EPSI_ZZ_2_c(x,y):    
    return 1/con.E2*( (1+con.nu2)*SIGMA_ZZ_2_c(x,y) - con.nu2*(SIGMA_XX_2_c(x,y)+SIGMA_YY_2_c(x,y)+SIGMA_ZZ_2_c(x,y)) )

def EPSI_XY_2_c(x,y):
    return 1/con.E2*( (1+con.nu2)*SIGMA_XY_2_c(x,y) )
