.. role:: red
.. role:: blue
.. role:: green
.. role:: gray
.. role:: python(code)
   :language: python
.. |br| raw:: html

   <br/>

.. index:: ! MACR_ELEM_DYNA

.. _u4.65.01:

************************************************************************
**[U4.65.01]** : Operator ``MACR_ELEM_DYNA``
************************************************************************

.. _u4.65.01.1:

Purpose
=======

To define a dynamic substructuring macro-element.

In a transient, modal or harmonic analysis with dynamic substructuring, the operator :red:`MACR_ELEM_DYNA`
performs

* the projection of the stiffness, mass, and damping matrices on the modal basis of the substructure
  defined by :red:`DEFI_BASE_MODALE` :ref:`[U4.64.02] <u4.64.02>`, and
* the extraction of the coupling matrices between the interfaces.

The output consists of the projected matrices and the coupling matrices. It can be used multiple times with
different orientations in the same model (cf. :red:`DEFI_MODELE_GENE` :ref:`[U4.65.02] <u4.65.02>`).  It can
be printed to a file by the command :red:`IMPR_MACR_ELEM` :ref:`[U7.04.33] <u7.04.33>`.

The output is a concept of type :green:`macr_elem_dyna`.

.. _u4.65.01.2:

Syntax
======

::

   macro_dyna [macr_elem_dyna] = MACR_ELEM_DYNA
    (
      ♦ BASE_MODALE = bamo,                                    [mode_meca]

      # Matrix data:

      ◊ / MATR_RIGI = stif_mat,                                [matr_asse_DEPL_R]
                                                               [matr_asse_DEPL_C]
        / MATR_MASS = mass_mat,                                [matr_asse_DEPL_R]
        / MATR_IMPE = impd_mat,                                [matr_asse_gene_C]

          # If MATR_IMPE then requires:
          ♦ FREQ_EXTR = freq,                                  [R]
          ◊ AMOR_SOL = / 0.0,                                  [DEFAULT]
                       / amosol,                               [R]
          ◊ MATR_IMPE_INIT = mi0,                              [matr_asse_gene_C]

        / | MATR_IMPE_RIGI = mr,                               [matr_asse_gene_C]
          | MATR_IMPE_AMOR = my,                               [matr_asse_gene_C]
          | MATR_IMPE_MASS = mm,                               [matr_asse_gene_C]

      ◊ / MATR_AMOR = damp_mat,                                [matr_asse_DEPL_R]
        / AMOR_REDUIT = l_damp,                                [l_R]

      ◊ SANS_GROUP_NO = node_grp,                              [group_no]

      # Static substructuring:

      ◊ CAS_CHARGE = _F(
                         ♦ NOM_CAS = case_nam,                 [k8]
                         ♦ VECT_ASSE_GENE = vgen,              [vect_asse_gene]
                       ),

      # Manual filling of reduced matrices (experimental data):

      ◊ MODELE_MESURE = _F(
                            ♦ FREQ = freq,                     [l_R]
                            ♦ MASS_GENE = mgen,                [l_R]
                            ◊ AMOR_REDUIT = xsi                [l_R])
                          ) 
    )

.. _u4.65.01.3:

Operands
========

.. _u4.65.01.3.1:

Operand :blue:`BASE_MODALE`
---------------------------

::

   ♦ BASE_MODALE = bamo

Name of the :green:`mode_meca` concept produced by the operator :red:`DEFI_BASE_MODALE`
:ref:`[U4.64.02] <u4.64.02>`.

.. _u4.65.01.3.2:

Operand :blue:`MATR_RIGI`
-------------------------

::

   ◊ MATR_RIGI = mr

Name of the assembled concept matrix of type :green:`matr_asse_DEPL_R` or :green:`matr_asse_DEPL_C` produced
by operator :red:`ASSE_MATRICE` :ref:`[U4.61.22] <u4.61.22>` or macro-command :red:`ASSEMBLAGE`
:ref:`[U4.61.21] <u4.61.21>` corresponding to the stiffness matrix of the substructure.

.. _u4.65.01.3.3:

Operand :blue:`MATR_MASS`
-------------------------

::

   ◊ MATR_MASS = mm

Name of the assembled concept matrix of type :green:`matr_asse_DEPL_R` produced by the operator
:red:`ASSE_MATRICE` :ref:`[U4.61.22] <u4.61.22>` or macro-command :red:`ASSEMBLAGE` :ref:`[U4.61.21] <u4.61.21>`
corresponding to the mass matrix. These two operands are to be used if the modal basis ``bamo`` is of type "RITZ".

.. _u4.65.01.3.4:

Operand :blue:`MATR_AMOR / AMOR_REDUIT`
----------------------------------------

::

   ◊ / MATR_AMOR = ma

Name of the assembled concept matrix of the type :green:`matr_asse_DEPL_R` produced by the operator
:red:`ASSE_MATRICE` :ref:`[U4.61.22] <u4.61.22>` or macro-command :red:`ASSEMBLAGE` :ref:`[U4.61.21] <u4.61.21>`
corresponding to the viscous damping matrix specific to the macro-element.

This damping must be of RAYLEIGH type for each element (linear combination of stiffness and mass at the
element level) and is defined by the properties of the material (operator: :red:`DEFI_MATERIAU`
:ref:`[U4.43.01] <u4.43.01>`, operands :blue:`AMOR_ALPHA` and :blue:`AMOR_BETA`).

::

   / AMOR_REDUIT = al

List of reduced damping (percentage of critical damping) corresponding to each mode of vibration of the
macro-element. The length of the list is (at most) equal to the number of eigen modes of the modal basis;
if it is lower, one completes the list with reduced damping equal to the last term in the list entered by
the user. No damping is associated with static modes. The generalized damping matrix of the macro-element
:math:`k` is thus incomplete diagonal (:math:`j` is the index of the eigen mode):

.. math::

   \mathbf{C}^k = \begin{bmatrix} \boldsymbol{\xi}_j & 0 \\ 0 & 0 \end{bmatrix}

.. _u4.65.01.3.5:

Operands :blue:`MATR_IMPE / FREQ_EXTR / AMOR_SOL`
-------------------------------------------------

::

   ◊ MATR_IMPE = mi

Name of the assembled concept matrix of the type :green:`matr_asse_gene_C` produced by the operator
:red:`LIRE_IMPE_MISS` :ref:`[U7.02.32] <u7.02.32>` corresponding to the soil impedance matrix constituting
the macro-element.

::

   ♦ FREQ_EXTR = freq

Frequency of extraction of the soil impedance matrix necessary for the calculation of the matrix of radiative
damping of soil starting from the imaginary part of the matrix ``mi``. 

::

   ◊ AMOR_SOL = amsol

Reduced damping value of soil material. It is used to distinguish the material part and the radiative part
of damping in the soil. If it is not zero, the radiative part :math:`C` is then expressed as :

.. math::

   2\pi\, \text{freq}\, C = \text{Imag} ( \text{mi} ( \text{freq} )) -
     2\, \text{amsol}\, \text{Real} ( \text{mi} ( \text{freq}))

.. _u4.65.01.3.6:

Operand :blue:`MATR_IMPE_INIT`
---------------------------------

::

   ◊ MATR_IMPE_INIT = mi0

Name of the assembled concept matrix of the type :green:`matr_asse_gene_C` produced by the operator
:red:`LIRE_IMPE_MISS` :ref:`[U7.02.32] <u7.02.32>` corresponding to a soil impedance matrix constituting the
macro-element extracted at a near-zero frequency. In particular in the case of soil-structure-fluid interaction
with the keyword :blue:`ISSF = 'OUI'` in the call to :red:`LIRE_IMPE_MISS`, that makes it possible to extract a
mass contribution :math:`M` such that:

.. math::

   (2\pi \text{freq})^2 M = \text{Real} ( \text{mi0}) - \text{Real} ( \text{mi} ( \text{freq}))

.. _u4.65.01.3.7:

Operands :blue:`MATR_IMPE_RIGI / MATR_IMPE_AMOR / MATR_IMPE_MASS`
------------------------------------------------------------------

::

   | MATR_IMPE_RIGI = mr
   | MATR_IMPE_AMOR = ma
   | MATR_IMPE_MASS = mm

Name of the assembled concept matrix of the type :green:`matr_asse_gene_C` produced by successive calls
with the operator :red:`LIRE_IMPE_MISS` :ref:`[U7.02.32] <u7.02.32>` in order to extract the respective
constitutive response of the macro-element in stiffness, damping or mass of a temporal soil impedance matrix.
If at least one of the operands is indicated, without others being present, then the contributions of the
latter in the macro-element will be filled and set to 0.

An example of use is provided by the test ``MISS03B`` **[V1.10.122]**.

.. _u4.65.01.3.8:

Operand :blue:`SANS_GROUP_NO`
--------------------------------

::

   ♦ SANS_GROUP_NO = grno

Name of the group of nodes including the list of the nodes of the physical interface of the part of the model
on which one calculates the dynamic macro-element. Its data is only necessary if this macro-element is used as
a super-mesh of substructures defined by the keyword :red:`AFFE_SOUS_STRUC` in a mixed model also including
classical finite elements, and in this case, only when the nodes of the physical and dynamic interfaces (the
latter defined by :red:`DEFI_INTERF_DYNA`) do not coincide. For example, in the case of the dynamic interfaces,
the node group is reduced to a node connected by a solid link to the physical interface.

.. _u4.65.01.3.9:

Key word :blue:`CAS_CHARGE`
------------------------------

::

   ◊ CAS_CHARGE

This keyword factor makes it possible to define a set of named load cases (keyword :blue:`NOM_CAS`). These
load cases are used to apply generalized load vectors to the part ofmodel on which one calculates the dynamic
macro-element if then this macro-element is used like a super-mesh of substructures in a mixed model that
also includes classic finite elements.

.. _u4.65.01.3.9.1:

Operand :blue:`NOM_CAS`
^^^^^^^^^^^^^^^^^^^^^^^

::

   ♦ NOM_CAS = nocas

The load case named ``nocas`` (between "quotes") corresponds to the loading defined by
the argument :red:`VECT_ASSE_GENE` on the part of model on which one calculates the dynamic macro-element.

.. _u4.65.01.3.9.2:

Operand :blue:`VECT_ASSE_GENE`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

   ♦ VECT_ASSE_GENE = vgen

The load case named ``nocas`` (between "quotes") corresponds to the loading defined by the
:red:`VECT_ASSE_GENE` argument. It is obtained by the projection of a load that is applied to the part of
the model on which one calculates the dynamic macro-element, on the modal basis ``bamo`` defined earlier.

.. _u4.65.01.3.10:

3.10 Operand :blue:`MODELE_MESURE`
----------------------------------

::

   ◊ MODELE_MESURE

This keyword factor makes it possible to manually fill the reduced matrices of the macro-element.  For example,
we can use data resulting measurements (and imported with :red:`LIRE_RESU`).  We must at least return the
generalized mass matrix and the eigenfrequencies. You can also fill in the list of reduced damping values.

The number of data points must be equal to the number of modes of the modal basis on which the macro-element
is built.

.. admonition:: Methodological point

   This use of :red:`MACR_ELEM_DYNA` is justified for structural modification using data from an experimental
   model. A presentation of the method is given in :ref:`[U2.07.03] <u3.07.03>`. The modal basis used to build
   the macro-element must be composed of only the eigen modes of the measured structure and must not include
   the static readings at the interface, because they are inaccurate (because they are not measured and, in
   the current state of knowledge, not measurable).

The ``sdll137e`` benchmark test is an example of the implementation of the methodology.

.. _u4.65.01.3.10.1:

Operand :blue:`FREQ`
^^^^^^^^^^^^^^^^^^^^

::

   ♦ FREQ = freq

List of identified natural frequencies of the measured structure.

.. _u4.65.01.3.10.2:

Operand :blue:`MASS_GENE`
^^^^^^^^^^^^^^^^^^^^^^^^^

::

   ♦ MASS_GENE = mass

List of identified generalized masses.

.. _u4.65.01.3.10.3:

Operand :blue:`AMOR_REDUIT`
^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

   ♦ AMOR_REDUIT = xsi

List of identified reduced damping values.

.. _u4.65.01.4:

Example
=======

An example of using this operator is given in the documentation of :red:`DEFI_SQUELETTE`
:ref:`[U4.24.01] <u4.24.01>`.
