import sys
import salome
import GEOM
import SMESH
from salome.geom import geomBuilder
from salome.smesh import smeshBuilder
import math

ExportPATH="/home/banerjee/Salome/forma11/"

salome.salome_init()

#-----------
# Geometry
#-----------
geompy = geomBuilder.New()

# Create points and line
point_A = geompy.MakeVertex(0, 0, 0)
point_B = geompy.MakeVertex(0.5, 0, 0)
bar = geompy.MakeLineTwoPnt(point_A, point_B)

# Create and assign groups
g_point_A = geompy.CreateGroup(bar, geompy.ShapeType["VERTEX"])
g_bar = geompy.CreateGroup(bar, geompy.ShapeType["EDGE"])
geompy.UnionIDs(g_point_A, [2])
geompy.UnionIDs(g_bar, [1])

# Add the geometries to the study
geompy.addToStudy(point_A, 'point_A')
geompy.addToStudy(point_B, 'point_B')
geompy.addToStudy(bar, 'bar')

# Add the groups to the study
geompy.addToStudyInFather(bar, g_point_A, 'point_A')
geompy.addToStudyInFather(bar, g_bar, 'bar')

#-----------
# Mesh
#-----------
smesh = smeshBuilder.New()

# Define mesh algorithms
mesh = smesh.Mesh(bar)
Regular_1D = mesh.Segment()
Number_of_Segments_1 = Regular_1D.NumberOfSegments(10)
smesh.SetName(mesh.GetMesh(), 'mesh')
smesh.SetName(Regular_1D.GetAlgorithm(), 'Regular_1D')
smesh.SetName(Number_of_Segments_1, 'Number of Segments_1')

# Compute the mesh
isDone = mesh.Compute()

# Map geometry groups to mesh
bar_2 = mesh.GroupOnGeom(g_bar, 'bar', SMESH.EDGE)
point_A_2 = mesh.GroupOnGeom(g_point_A, 'point_A', SMESH.NODE)

# Export mesh
mesh.ExportMED( r''+ExportPATH+'forma11a.mmed'+'')

# Update GUI tree
if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()
