.. role:: red
.. role:: blue
.. role:: green
.. role:: gray
.. role:: python(code)
   :language: python
.. |br| raw:: html

   <br/>

.. index:: ! LIRE_MAILLAGE

.. _u4.21.01:

************************************************************************
**[U4.21.01]** : Operator ``LIRE_MAILLAGE`` (:gray:`READ_MESH`)
************************************************************************

.. _u4.21.01.1:

Purpose
=======

Create a mesh by reading from a file.

The supported file formats are: :green:`ASTER`, :green:`MED`, :green:`GIBI`, :green:`GMSH` and :green:`IDEAS`.

The output is a data structure of the :green:`maillage` (:gray:`mesh`) type.

.. admonition:: Important note

   One can check the quality of the mesh by using the command :red:`MACR_INFO_MAIL` :ref:`[U7.03.02] <u7.03.02>`
   after reading the mesh with :red:`LIRE_MAILLAGE`.

 
.. _u4.21.01.2:

Syntax
======

::

  mesh [maillage] = LIRE_MAILLAGE(
  
    ◊ UNITE = / 20,                                   [DEFAULT]
              / i,                                    [I]
    ◊ FORMAT = / 'MED',                               [DEFAULT]
               / 'ASTER',
               / 'GIBI',
               / 'GMSH',
               / 'IDEAS',

    # If FORMAT = 'MED'

    ◊ NOM_MED = med_name,                             [K*]
    ◊ INFO_MED = / 1,                                 [DEFAULT]
                 / 2,
                 / 3,

    # If FORMAT = 'IDEAS'

    ◊ CREA_GROUP_COUL = / 'NON',                      [DEFAULT]
                        / 'OUI',

    ◊ VERI_MAIL = _F(
                     ◊ VERIF = / 'OUI' ,              [DEFAULT]
                               / 'NON' , 
                    ),

    ◊ APLAT = / 1.D-3,                                [DEFAULT]
              / ap,                                   [R]

    ◊ INFO = / 1,                                     [DEFAULT]
             / 2,
  )


.. _u4.21.01.3:

Operands
========

.. _u4.21.01.3.1:

Operand :blue:`UNITE`
---------------------

::

  ◊ UNIT = i

Logical unit number of the mesh file. Unit 20 by default.

.. _u4.21.01.3.2:

Operand :blue:`FORMAT`
----------------------

::

  ◊ FORMAT = / 'MED'                   [DEFAULT]
             / 'ASTER'
             / 'GIBI'
             / 'GMSH'
             / 'IDEAS'

This keyword is used to specify the format of the file to be read. 

The format :blue:`ASTER` is described in :ref:`[U3.01.00] <u3.01.00>`.

The :blue:`MED` format, which means "Modeling and Data Exchange", is a neutral data format developed by 
EDF R&D and the CEA for data exchanges between computer codes. The data that can be exchanged according to 
this format are the meshes and the result fields: fields at nodes, fields by element. 
:blue:`MED` files are binary and portable files (based on the ``HDF`` library, Hierarchical Data Format). 
Reading a :blue:`MED` file with :red:`LIRE_MAILLAGE` allows the user to input a mesh produced by any other 
code capable of creating a :blue:`MED` file. This is the format advised in ``Code_Aster`` and is the default.

Whatever the format, the data read are:

* the list of nodes : number, name, contact details,
* the list of elements : number, name, type, name of the nodes,
* the list of nodes groups : number, name, number of nodes, names of nodes,
* the list of element groups : number, name, number of meshes, names of elements.

.. note:: 

   In a :blue:`MED` file, there is a partition of the nodes and elements according to groups. A partition 
   corresponds to a :blue:`MED` family. In a :blue:`MED` file, the groups are distributed within families: 
   there are therefore families of nodes and families of elements. During the reading of a :blue:`MED` file, 
   lists of groups of nodes and elements are created on the fly by breaking down families.

.. _u4.21.01.3.3:

Operand :blue:`VERI_MAIL` (:gray:`CHECK_MESH`)
----------------------------------------------

The keyword :blue:`VERI_MAIL` triggers 3 checks on the mesh:

* absence of orphan nodes,
* absence of "duplicate" elements,
* absence of excessively flat elements.

If these checks are not satisfied, the code issues an alarm.

By default (i.e., in the absence of the keyword :blue:`VERI_MAIL`), the checks are performed.  If the user 
wants to avoid these checks, they can use:

::

  VERI_MAIL = _F(VERIF = 'NON',)

A node is declared to be an orphan if it is not part of the connectivity of any element. 

An element is declared to be "duplicate" if 2 elements (or more) have connectivities formed by the same
list of nodes.

The keyword :blue:`APLAT = ap` makes it possible to emit alarms when the mesh contains too many flat elements.
The flatness of an element is defined as the ``Amin / Amax`` ratio where ``Amin`` and ``Amax`` are the
shortest and longest edge lengths of the element. The name of the elements for which the flatness is less 
than :green:`ap` will be printed in the 'MESSAGE' file.

Other quality criteria for the mesh are available via the command :red:`MACR_INFO_MAIL` 
:ref:`[U7.03.02] <u7.03.02>`.

.. _u4.21.01.3.4:

Operands for the format :blue:`'MED'`
-------------------------------------

::

  ◊ NOM_MED = med_name,

A :blue:`MED` file can contain several meshes. Each mesh is identified by its name. To read a particular mesh, 
it is necessary to provide its name in the argument of the keyword :blue:`NOM_MED`. Without the keyword, the 
first mesh found in the file will be read. When the file contains only one mesh there is no need to specify
a mesh name.

::

  ◊ INFO_MED = / 1,                  [DEFAULT]
               / 2,
               / 3,

Print specific information during the course of reading the :blue:`MED` mesh file (number of nodes and meshes 
read again, information on the MED families, etc.):

* :blue:`INFO_MED = 1`: no printing,
* :blue:`INFO_MED = 2`: only messages relating to family correspondence/group,
* :blue:`INFO_MED = 3`: all the information is printed.

.. _u4.21.01.3.5:

Operands for the format :blue:`'IDEAS'`
----------------------------------------

::

   ◊ CREA_GROUP_COUL

The user can ask for the creation of mesh groups and nodes by collecting all the elements and nodes of the 
same color by indicating :blue:`CREA_GROUP_COUL = 'OUI'`. These groups are named :green:`COUL_n` where 
:green:`n` is the number of the color in :blue:`IDEAS`.

By default this is not done so as not to unnecessarily increase the number of element and node groups.

.. admonition:: Remarks 

   *  We only deal with Cartesian coordinate systems.
   *  Only one Cartesian coordinate system is managed.
   *  During the conversion of the universal :blue:`IDEAS` file, it is checked whether the user has defined 
      several coordinate systems. If this is the case, an alarm message warns the user, to ask them to verify 
      that all coordinate systems are the same.

.. _u4.21.01.3.6:

Operand :blue:`INFO`
--------------------

::

   ◊ INFO = / 1,                    [DEFAULT]
            / 2,

Printing level.

If :blue:`INFO = 1`, output

* mesh title,
* number of nodes,
* number of elements of each type,
* number of groups of nodes and for each of them its name and the number of nodes of the group
* number of element groups and for each of them its name and the number of elements in the group.

If :blue:`INFO = 2`, one prints the following additional information:

* list of nodes : number, name, contact details,
* element list : number, name, type, name of the nodes,
* list of node groups : number, name, number of nodes, names of nodes,
* list of element groups : number, name, number of elements, names of elements.
