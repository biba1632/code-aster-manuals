# coding=utf-8
DEBUT(LANG = 'EN')

# Material parameters
E = 200.0e9
nu = 0.3
rho = 8000

# Load/Time parameters
t_final = 0.5
t_inc = 0.002
freq_band = 1.0/(5.*t_inc)
fc = 15.0;
omega = ((2.0 * pi) * fc);

# Create function for computing load scaling
sin_om = FORMULE(VALE = 'sin(omega*INST)',
                 omega = omega,
                 NOM_PARA = 'INST')

# Read mesh
mesh = LIRE_MAILLAGE(FORMAT = 'MED',
                     UNITE = 20)

# Assign 3D elements
model = AFFE_MODELE(AFFE = _F(MODELISATION = ('3D', ),
                              PHENOMENE = 'MECANIQUE',
                              TOUT = 'OUI'),
                    MAILLAGE = mesh)

# Create and assign material
steel = DEFI_MATERIAU(ELAS = _F(E = E,
                                NU = nu,
                                RHO = rho))

mater = AFFE_MATERIAU(AFFE = _F(MATER = steel,
                                TOUT = 'OUI'),
                      MODELE = model)

# Write out material and inertial properties to a table
mat_geom = POST_ELEM(MASS_INER = _F(TOUT = 'OUI'),
                     MODELE = model,
                     CHAM_MATER = mater)

IMPR_TABLE(TABLE = mat_geom,
           SEPARATEUR = ',')
 
# Displacement BCs
disp_bc = AFFE_CHAR_MECA(DDL_IMPO = _F(DX = 0.0,
                                       DY = 0.0,
                                       DZ = 0.0,
                                       GROUP_MA = ('fixed_face')),
                         MODELE = model)

# Body force
gravity = AFFE_CHAR_MECA(MODELE = model,
                         PESANTEUR = _F(GRAVITE = 300.,
                                        DIRECTION = (-1., 0, 1)))

# (Transient) Linear Dynamic simulation
result = DYNA_LINE(TYPE_CALCUL = "TRAN",
                   BASE_CALCUL = "PHYS",
                   MODELE = model,
                   CHAM_MATER = mater,
                   CHARGE = disp_bc,
                   EXCIT = _F(CHARGE = gravity, FONC_MULT = sin_om),
                   BANDE_ANALYSE = freq_band,
                   SCHEMA_TEMPS = _F(SCHEMA = 'NEWMARK'),
                   INCREMENT = _F(INST_FIN = t_final,
                                  PAS = t_inc))

# Write results
IMPR_RESU(FORMAT = 'MED',
          RESU = _F(RESULTAT = result),
          UNITE = 80)

# Extract data for curves (at point P)
u_x_P = RECU_FONCTION(RESULTAT = result,
                      NOM_CHAM = 'DEPL',
                      NOM_CMP = 'DX',
                      GROUP_NO = 'point_P')

u_z_P = RECU_FONCTION(RESULTAT = result,
                      NOM_CHAM = 'DEPL',
                      NOM_CMP = 'DZ',
                      GROUP_NO = 'point_P')

# Write data to file
IMPR_FONCTION(FORMAT = 'TABLEAU',
              UNITE = 29,
              SEPARATEUR = ',',
              COURBE = (_F(FONCTION = u_x_P)))

IMPR_FONCTION(FORMAT = 'TABLEAU',
              UNITE = 29,
              SEPARATEUR = ',',
              COURBE = (_F(FONCTION = u_z_P)))

# Verification of results
TEST_FONCTION(VALEUR = (_F(VALE_CALC = 0.14537012,
                           VALE_PARA = 0.05,
                           FONCTION = u_z_P)))

TEST_FONCTION(VALEUR = (_F(VALE_CALC = 0.00017243164,
                           VALE_PARA = 0.05,
                           FONCTION = u_x_P)))

# End
FIN();
