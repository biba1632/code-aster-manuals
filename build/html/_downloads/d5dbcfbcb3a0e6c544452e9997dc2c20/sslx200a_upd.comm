# SSLX200A

DEBUT(LANG = 'EN')


# Read input mesh
mesh_ini = LIRE_MAILLAGE(FORMAT = 'MED')

# Create point element at point C
mesh = CREA_MAILLAGE(MAILLAGE = mesh_ini,
                     CREA_POI1 = (_F(NOM_GROUP_MA = 'C',   
                                     GROUP_NO = 'C')))

# Assign model
model = AFFE_MODELE(MAILLAGE = mesh,
                    AFFE = (_F(GROUP_MA = 'POU3D',
                               MODELISATION = '3D',
                               PHENOMENE = 'MECANIQUE'),
                            _F(GROUP_MA = ('AB'),
                               MODELISATION = 'POU_D_E',
                               PHENOMENE = 'MECANIQUE'),
                           _F(GROUP_MA = 'C',
                              MODELISATION = 'DIS_TR',
                              PHENOMENE = 'MECANIQUE')))

# Assign structure characteristics
section = AFFE_CARA_ELEM(MODELE = model,
                         VERIF = ('MAILLE'),
                         POUTRE = _F(GROUP_MA = ('AB'), 
                                     SECTION = 'RECTANGLE',
                                     CARA = ('HY', 'HZ'),
                                     VALE = ( 3.0,  2.0)),
                         DISCRET = (_F(GROUP_MA = 'C',
                                       CARA = 'K_TR_D_N',
                                       VALE = (0.0, 0.0, 0.0,
                                               0.0, 0.0, 0.0)),
                                    _F(GROUP_MA = 'C',
                                       CARA = 'M_TR_D_N',
                                       VALE = (0.0, 0.0, 0.0,
                                               0.0, 0.0, 0.0,
                                               0.0, 0.0, 0.0, 0.0))))

# Define material
mat_beam = DEFI_MATERIAU(ELAS = _F(E = 200000.,   
                                   NU = 0.3,
                                   RHO = 10000.0))

# Assign material
mater = AFFE_MATERIAU(MODELE = model,
                      AFFE = _F(GROUP_MA = ('POU3D', 'AB'),    
                                MATER = mat_beam))

# Apply BCs
bc = AFFE_CHAR_MECA(MODELE = model,
                    LIAISON_ELEM = (_F(OPTION = '3D_POU',
                                       GROUP_MA_1 = 'SU',
                                       GROUP_NO_2 = 'C',
                                       NUME_LAGR = 'APRES'),
                                    _F(OPTION = '3D_POU',
                                       GROUP_MA_1 = 'SF',
                                       GROUP_NO_2 = 'A')),
                    DDL_IMPO = _F(GROUP_NO = 'C',  
                                  DX = 0.0, DY = 0.0, DZ = 0.0, 
                                  DRX = 0.0, DRY = 0.0, DRZ = 0.0),
                    FORCE_NODALE = _F(GROUP_NO = 'B',  
                                      FX = 10.0, MY = 2.0, MZ = 3.0))

# Set up matrices for FE calculation
K_mat = CALC_MATR_ELEM(MODELE = model,
                       CHARGE = bc,
                       CHAM_MATER = mater,
                       CARA_ELEM = section,
                       OPTION = 'RIGI_MECA')

M_mat = CALC_MATR_ELEM(MODELE = model,
                       CHARGE = bc,
                       CHAM_MATER = mater,
                       CARA_ELEM = section,
                       OPTION = 'MASS_MECA')

f_vec = CALC_VECT_ELEM(CHARGE = bc,
                       OPTION = 'CHAR_MECA')

# Assemble the matrices adnd vector
dofs = NUME_DDL(MATR_RIGI = K_mat)

K_ass = ASSE_MATRICE(MATR_ELEM = K_mat,
                     NUME_DDL = dofs)

M_ass = ASSE_MATRICE(MATR_ELEM = M_mat,
                     NUME_DDL = dofs)

# Compute modes
modes = CALC_MODES(MATR_RIGI = K_ass,
                   MATR_MASS = M_ass,
                   SOLVEUR = _F(METHODE = 'MUMPS'))

# Compute static displacements
result = MECA_STATIQUE(MODELE = model,  
                       CHAM_MATER = mater,
                       CARA_ELEM = section,
                       EXCIT = _F(CHARGE = bc),
                       SOLVEUR = _F(METHODE = 'MUMPS'))

# Compute stresses
result = CALC_CHAMP(reuse = result,
                    RESULTAT = result,
                    GROUP_MA = 'POU3D',
                    CONTRAINTE = ('SIGM_ELNO'))

# Write output
IMPR_RESU(FORMAT = 'MED',
          UNITE = 80,
          RESU = _F(RESULTAT = modes)),

IMPR_RESU(FORMAT = 'MED',
          UNITE = 81,
          RESU = _F(RESULTAT = result)),

# Verification tests
TEST_RESU(RESU = (_F(NUME_ORDRE = 1,
                     RESULTAT = result,
                     NOM_CHAM = 'DEPL',
                     NOEUD = 'N1',
                     NOM_CMP = 'DX',
                     VALE_CALC = 8.3333333299999996E-05,
                     VALE_REFE = 8.3333333299999996E-05,
                     REFERENCE = 'ANALYTIQUE',
                     CRITERE = 'RELATIF'),
                  _F(NUME_ORDRE = 1,
                     RESULTAT = result,
                     NOM_CHAM = 'DEPL',
                     NOEUD = 'N1',
                     NOM_CMP = 'DY',
                     VALE_CALC = 1.666666667E-4,
                     VALE_REFE = 1.666666667E-4,
                     REFERENCE = 'ANALYTIQUE',
                     CRITERE = 'RELATIF'),
                  _F(NUME_ORDRE = 1,
                     RESULTAT = result,
                     NOM_CHAM = 'DEPL',
                     NOEUD = 'N1',
                     NOM_CMP = 'DZ',
                     VALE_CALC = -2.5E-4,
                     VALE_REFE = -2.5E-4,
                     REFERENCE = 'ANALYTIQUE',
                     CRITERE = 'RELATIF'),
                  _F(NUME_ORDRE = 1,
                     RESULTAT = result,
                     NOM_CHAM = 'DEPL',
                     NOEUD = 'N6',
                     NOM_CMP = 'DX',
                     VALE_CALC = -8.3333333333332999E-06,
                     VALE_REFE = -8.3333333333332999E-06,
                     REFERENCE = 'ANALYTIQUE',
                     CRITERE = 'RELATIF'),
                  _F(NUME_ORDRE = 1,
                     RESULTAT = result,
                     NOM_CHAM = 'DEPL',
                     NOEUD = 'N6',
                     NOM_CMP = 'DY',
                     VALE_CALC = 4.0583333333333003E-05,
                     VALE_REFE = 4.0583333333333003E-05,
                     REFERENCE = 'NON_DEFINI',
                     CRITERE = 'RELATIF'),
                  _F(NUME_ORDRE = 1,
                     RESULTAT = result,
                     NOM_CHAM = 'DEPL',
                     NOEUD = 'N6',
                     NOM_CMP = 'DZ',
                     VALE_CALC = -6.0875000000000002E-05,
                     VALE_REFE = -6.0875000000000002E-05,
                     REFERENCE = 'NON_DEFINI',
                     CRITERE = 'RELATIF'),
                  _F(NUME_ORDRE = 1,
                     RESULTAT = result,
                     NOM_CHAM = 'SIGM_ELNO',
                     NOEUD = 'N6',
                     NOM_CMP = 'SIXX',
                     VALE_CALC = -0.333333333,
                     VALE_REFE = -0.333333333,
                     REFERENCE = 'NON_DEFINI',
                     CRITERE = 'RELATIF',
                     MAILLE = 'M27'),
                  _F(NUME_ORDRE = 1,
                     RESULTAT = result,
                     NOM_CHAM = 'SIGM_ELNO',
                     NOEUD = 'N6',
                     NOM_CMP = 'SIYY',
                     VALE_CALC = 0.0,
                     VALE_REFE = 0.0,
                     REFERENCE = 'ANALYTIQUE',
                     CRITERE = 'ABSOLU',
                     MAILLE = 'M27'),
                  _F(NUME_ORDRE = 1,
                     RESULTAT = result,
                     NOM_CHAM = 'SIGM_ELNO',
                     NOEUD = 'N6',
                     NOM_CMP = 'SIZZ',
                     VALE_CALC = 0.0,
                     VALE_REFE = 0.0,
                     REFERENCE = 'ANALYTIQUE',
                     CRITERE = 'ABSOLU',
                     MAILLE = 'M27'),
                  _F(NUME_ORDRE = 1,
                     RESULTAT = result,
                     NOM_CHAM = 'SIGM_ELNO',
                     NOEUD = 'N6',
                     NOM_CMP = 'SIXY',
                     VALE_REFE = 0.0,
                     REFERENCE = 'ANALYTIQUE',
                     VALE_CALC = 0.0,
                     CRITERE = 'ABSOLU',
                     MAILLE = 'M27'),
                  _F(NUME_ORDRE = 1,
                     RESULTAT = result,
                     NOM_CHAM = 'SIGM_ELNO',
                     NOEUD = 'N6',
                     NOM_CMP = 'SIXZ',
                     VALE_CALC = 0.0,
                     VALE_REFE = 0.0,
                     REFERENCE = 'ANALYTIQUE',
                     CRITERE = 'ABSOLU',
                     MAILLE = 'M27'),
                  _F(NUME_ORDRE = 1,
                     RESULTAT = result,
                     NOM_CHAM = 'SIGM_ELNO',
                     NOEUD = 'N6',
                     NOM_CMP = 'SIYZ',
                     VALE_CALC = 0.0,
                     VALE_REFE = 0.0,
                     REFERENCE = 'ANALYTIQUE',
                     CRITERE = 'ABSOLU',
                     MAILLE = 'M27'),
                  _F(NUME_ORDRE = 1,
                     RESULTAT = result,
                     NOM_CHAM = 'SIGM_ELNO',
                     NOEUD = 'N25',
                     NOM_CMP = 'SIXX',
                     VALE_CALC = 1.666666666667,
                     CRITERE = 'RELATIF',
                     MAILLE = 'M25'),
                  _F(NUME_ORDRE = 1,
                     RESULTAT = result,
                     NOM_CHAM = 'SIGM_ELNO',
                     NOEUD = 'N25',
                     NOM_CMP = 'SIYY',
                     VALE_CALC = 0.0,
                     VALE_REFE = 0.0,
                     REFERENCE = 'ANALYTIQUE',
                     CRITERE = 'ABSOLU',
                     MAILLE = 'M25'),
                  _F(NUME_ORDRE = 1,
                     RESULTAT = result,
                     NOM_CHAM = 'SIGM_ELNO',
                     NOEUD = 'N25',
                     NOM_CMP = 'SIZZ',
                     VALE_CALC = 0.0,
                     CRITERE = 'ABSOLU',
                     MAILLE = 'M25'),
                  _F(NUME_ORDRE = 1,
                     RESULTAT = result,
                     NOM_CHAM = 'SIGM_ELNO',
                     NOEUD = 'N25',
                     NOM_CMP = 'SIXY',
                     VALE_REFE = 0.0,
                     REFERENCE = 'ANALYTIQUE',
                     VALE_CALC = 0.0,
                     CRITERE = 'ABSOLU',
                     MAILLE = 'M25'),
                  _F(NUME_ORDRE = 1,
                     RESULTAT = result,
                     NOM_CHAM = 'SIGM_ELNO',
                     NOEUD = 'N25',
                     NOM_CMP = 'SIXZ',
                     VALE_CALC = 0.0,
                     CRITERE = 'ABSOLU',
                     MAILLE = 'M25'),
                  _F(NUME_ORDRE = 1,
                     RESULTAT = result,
                     NOM_CHAM = 'SIGM_ELNO',
                     NOEUD = 'N25',
                     NOM_CMP = 'SIYZ',
                     VALE_CALC = 0.0,
                     CRITERE = 'ABSOLU',
                     MAILLE = 'M25')))
  
  
TEST_RESU(RESU = (_F(NUME_ORDRE = 1,
                     PARA = 'FREQ',
                     RESULTAT = modes,
                     CRITERE = 'RELATIF',
                     VALE_CALC = 0.0141878007434,
                     VALE_REFE = 0.0144853,
                     PRECISION = 0.025,
                     REFERENCE = 'ANALYTIQUE'),
                  _F(NUME_ORDRE = 5,
                     PARA = 'FREQ',
                     RESULTAT = modes,
                     CRITERE = 'RELATIF',
                     VALE_CALC = 0.106617508262,
                     VALE_REFE = 0.0905478,
                     PRECISION = 0.18,
                     REFERENCE = 'ANALYTIQUE'),
                  _F(NUME_ORDRE = 9,
                     PARA = 'FREQ',
                     RESULTAT = modes,
                     CRITERE = 'RELATIF',
                     VALE_CALC = 0.24838988531278,
                     VALE_REFE = 0.24838988531278,
                     REFERENCE = 'NON_DEFINI')))
  
# End
FIN()

