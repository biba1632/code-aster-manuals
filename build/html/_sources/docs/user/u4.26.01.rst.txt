.. role:: red
.. role:: blue
.. role:: green
.. role:: gray
.. role:: python(code)
   :language: python
.. |br| raw:: html

   <br/>

.. index:: ! DEFI_GEOM_FIBRE

.. _u4.26.01:

************************************************************************
**[U4.26.01]** : Operator ``DEFI_GEOM_FIBRE``
************************************************************************

.. _u4.26.01.1:

Purpose
=======

There are two "levels" within the framework of multi-fiber modeling of beams:

* the "longitudinal" model represented by a beam (of geometrical support :blue:`SEG2`)
* a plane model of the section (perpendicular to :blue:`SEG2`).

The operator :red:`DEFI_GEOM_FIBRE` makes it possible to define the geometry of the groups of 
fibers which will be used in the definition of the cross-section using operator 
:red:`AFFE_CARA_ELEM`.

The fibers can be described either using a plane mesh previously read in by the operator
:red:`LIRE_MAILLAGE` (keyword :blue:`SECTION`) and/or in the form of "specific" surfaces 
(keyword :blue:`FIBRE` or :blue:`ASSEMBLAGE_FIBRE`).

Each occurrence of :blue:`SECTION`, :blue:`FIBRE`, or :blue:`ASSEMBLAGE_FIBRE` defines a set of fibers that must 
have a unique name. An assemblage of fibers will have the same behavior for all of its fibers, defined using 
:red:`DEFI_COMPOR`.

The :red:`DEFI_GEOM-FIBRE` command creates a data structure of the :green:`geom_fibre` type.

.. warning::

   With the information given in :blue:`SECTION` or :blue:`FIBRE`, it is possible to calculate the majority of
   integrated characteristics of plane sections (area, static and quadratic moments). However, it is not possible 
   to calculate the torsional inertia, warping, as well as the position of the center of twist. These quantities
   must be input to the operator :red:`AFFE_CARA_ELEM`.

   If the fibers are defined via a mesh, the operator :red:`MACR_CARA_POUTRE` makes it possible to read this
   mesh and calculate all the characteristics. The result of this operator is a table that can be input to 
   operator :red:`AFFE_CARA_ELEM`.

.. _u4.26.01.2:

Syntax
======

::

  fib_geom [geom_fibre] = DEFI_GEOM_FIBRE 
  (

    ◊ INFO = / 1                                                  [DEFAULT]
             / 2

    ♦  | SECTION = _F(
                       ♦  GROUP_FIBRE = fiber_group_name          [K24]
                       ♦  MAILLAGE_SECT = mesh                    [maillage]
                       ♦  / TOUT_SECT = 'OUI'
                          / GROUP_MA_SECT = grp_mesh              [l_gr_maille]
                       ◊ COOR_AXE_POUTRE = / 0.0, 0.0             [DEFAULT]
                                           / x, y                 [l_R]
                       ◊ ANGLE = angle                            [real]
                     ),

       | FIBRE = _F(
                     ♦ GROUP_FIBRE = fiber_group_name             [K24]
                     ◊ CARA = / 'SURFACE'                         [DEFAULT]
                              / 'DIAMETRE'
                     ♦ VALE = l_val                               [l_R]
                     ◊ COOR_AXE_POUTRE = / 0.0, 0.0               [DEFAULT]
                                         / x, y                   [l_R]
                     ◊ ANGLE = angle                              [real]
                   ),

       | ASSEMBLY_FIBRE = _F(
                              ♦ GROUP_ASSE_FIBRE = group_name     [K24]
                              ♦ GROUP_FIBRE = fiber_group_name    [K24]
                              ♦ COOR_GROUP_FIBRE = coor_gf        [l_R]
                              ♦ GX_GROUP_FIBRE = gx_gf            [l_R]
                            ),
  )

.. _u4.26.01.3:

Operands
========

.. _u4.26.01.3.1:

Operand :blue:`INFO`
--------------------

::

  ◊ INFO

If the value of :blue:`INFO` is 2, the operator prints in the file 'MESSAGE', for each occurrence of keywords 
:blue:`SECTION` or :blue:`FIBRE`, the characteristics of each fiber (position and area).

.. admonition:: Remark 

   All the characteristics are given with respect to the axis defined in the operand :blue:`COOR_AXE_POUTRE`, by 
   defining the coordinates of the section as :math:`x` (horizontal) and :math:`y` (vertical).

.. _u4.26.01.3.2:

Keywords :blue:`SECTION` and :blue:`FIBRE`
------------------------------------------

::

  ♦ | SECTION
    | FIBRE

Define the entities of the beam elements concerned and the sections which are assigned to them.
The keyword :blue:`SECTION` makes it possible to define a section via a plane mesh (the elements of this mesh 
are the sections of the fibers). The fiber count is totally dependent on the mesh and the type of mesh 
(quadrilateral or triangle).

The keyword :blue:`FIBRE` makes it possible to define a section where the fibers are designated by "points". 
Fibers are numbered from 1 to n in the order in which they are defined.

In the presence of these two keywords, the numbering of the fibers will start with the fibers defined under the
keyword :blue:`SECTION` followed by those defined under the keyword :blue:`FIBRE`. To obtain the numbering, you can
define :blue:`INFO = 2` in :red:`DEFI_GEOM_FIBRE`.

.. admonition:: Remark 

   Currently the number of group of fibers on an element beam is limited to 10.

.. _u4.26.01.3.2.1:

Operands common to :blue:`SECTION` and :blue:`FIBRE`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

  ♦ GROUP_FIBRE

This operand is used to define a name for the group of fibers (24 characters). This name will be used in operator 
:red:`DEFI_COMPOR` to attribute a material and a behavior to this group of fibers. Note that all fibers defined by 
an occurrence of :blue:`SECTION` or :blue:`FIBRE` will have the same behviour.

::

  ◊ COOR_AXE_POUTRE = (xg, yg)

This operand makes it possible to define the coordinates of the axis of the beam in the reference coordinates of 
the section. The integration (for static or inertia moments) will be made with respect to this axis. The default 
position is (0.0, 0.0).

::

  ◊ ANGLE = angle

This operand makes it possible to define the angle of rotation of the reference axes of definition of the mesh to 
correspond to the main coordinate system of the beam. This rotation is always carried out after
:blue:`COOR_AXE_POUTRE`. The default angle is 0.0.

When the coordinates of the fibers are given either by a mesh (key word :blue:`SECTION`) or by triples of values 
(keyword :blue:`FIBRE`), they are in a reference coordinates system which is not related to the beam. That allows us
to define complex sections without the necessity of defining them in the principal inertial coordinate system of the 
beam.

It is easier to parameterize the mesh of an angle section (:numref:`fig_u4.26.01.3.2.1-a`) in 
the reference coordinates :math:`(x_1, y_1)` than in the coordinate system :math:`(x_2, y_2)`. The mesh of the 
section is in the XY plane.

.. _fig_u4.26.01.3.2.1-a:

.. figure:: u4.26.01/fig1_u4.26.01.png
   :height: 300 px
   :align: center

   Angle section.

The characteristics of the beams (section, moments of inertias, etc.) are defined in the principal inertial 
coordinates. It is thus necessary to define the relation between the reference coordinates of the fibers and 
the principal inertial reference coordinates of the beam. The keyword :blue:`COOR_AXE_POUTRE` makes it possible 
to define the map of the reference system to the mesh of the section to make it correspond to that of the beam. 
The :blue:`ANGLE` keyword is used to rotate the mesh of the section to match that of the beam.

In the example of the angle section (:numref:`fig_u4.26.01.3.2.1-a`), to make the mesh correspond to the principal 
coordinate system of the beam section, it is necessary to specify the two keywords:

::

    COOR_AXE_POUTRE = (xg, yg)
    ANGLE = - 90

.. _u4.26.01.3.2.2:

Operands specific to :blue:`SECTION`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The group of fibers is defined by a set of "surface" fibers.

::

  ♦ MAILLAGE_SECT

Name of the plane "mesh" which contains the "section description". By "mesh", one understands a set of 
triangular elements with 3 nodes and / or quadrilaterals with 4 nodes. By "section description" is meant a part of 
this "mesh" specified by one of the operands :blue:`TOUT_SECT` or :blue:`GROUP_MA_SECT`. Each mesh represents the 
section of a fiber.

::

  ♦ / TOUT_SECT
    / GROUP_MA_SECT

+-----------------------+-----------------------------------------------------------------------------------------------------+
| Operands              |  Content / Meaning                                                                                  |
+-----------------------+-----------------------------------------------------------------------------------------------------+
| :blue:`TOUT_SECT`     | The section is defined by the union of the elements of the mesh defined under :blue:`MAILLAGE_SECT` |
+-----------------------+-----------------------------------------------------------------------------------------------------+
| blue:`GROUP_MA_SECT`  | The section is defined by a list of element groups                                                  |
+-----------------------+-----------------------------------------------------------------------------------------------------+

.. admonition:: Remarks 

   Since it does not serve as a support for finite elements, the "mesh" does not necessarily need to be connected, it 
   can be made up of a set of juxtaposed elements or not.

   The coordinates :math:`x` and :math:`y` of the plane mesh of the section ( x horizontal, y vertical) are
   defined in a plane perpendicular to the axis of the beam. To define the twist angle, the angle between the 
   axis x of the plane mesh of the section and the axis Y of the element beam, it is necessary to use the keyword 
   :blue:`ORIENTATION` of the operator :red:`AFFE_CARA_ELEM`.


.. _u4.26.01.3.2.3:

Operands specific to :blue:`FIBRE`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The group of fibers is defined by a set of "point" fibers.

::

  ◊ CARA = 'SURFACE' | 'DIAMETRE'

Used to specify whether the third value given for each fiber is the surface or the diameter (see :blue:`VALE`).

::

  ♦ VALE

Each fiber is described by a triplet of values: ( x, y, val ). It is necessary to give the values according to this 
sequence; as many triplets as there are fibers.

* x and y are the coordinates of the center of the fiber in a plane perpendicular to the axis of the beam.
* val is either the area of a fiber or the diameter of a cylindrical fiber.

The position of the coordinate system can be modified with :blue:` COOR_AXE_POUTRE`. To give a twist angle, the 
keyword :blue:`ORIENTATION` of the operator :red:`AFFE_CARA_ELEM` should be used.


.. _u4.26.01.3.3:

Keyword :blue:`ASSEMBLAGE_FIBRE`
--------------------------------

This keyword makes it possible to define an assembly of groups of fibers. The kinematics connecting the different
groups of fibers is particular and is used to account for phenomena specific to the modeling of the fuel assemblies, 
cf. :ref:[R3.08.08] <r3.08.08>`: Straight multi-fiber beam element.

::

  ♦ GROUP_ASSE_FIBRE

This operand allows us to define a name for the group of assembly of fibers (24 characters). This name will be used 
in operator :red:`DEFI_COMPOR` to attribute a material and a behavior to this fiber assembly group. Recall that all 
fibers defined by an occurrence of :blue:`GROUP_ASSE_FIBRE` will have the same behavior.

::

  ♦ GROUP_FIBRE

This operand makes it possible to define the list of the names of the groups of fibers which will constitute the 
assembly of fibers. The names of the groups are defined either by :blue:`SECTION / GROUP_FIBRE` or by
:blue:`FIBRE / GROUP_FIBRE`.

::

  ♦ COOR_GROUP_FIBRE

This operand makes it possible to define the coordinates of the center of gravity of the defined groups of fibers
with the keyword :blue:`GROUP_FIBRE`. It is a list of real numbers :math:`(x_, y_1, \dots, x_n, y_n)` which has 
2 times the length of the list given under :blue:`GROUP_FIBRE`. The coordinates of the groups of fibers are translated 
by :math:`(x_i,y_i)`.

::

  ♦ GX_GROUP_FIBRE

This operand makes it possible to define the coefficient of torsion of the groups of fibers defined under the keyword
:blue:`GROUP_FIBRE`. It is a list of real numbers :math:`(gx_1, \dots, gx_n)` which has the same length as the list 
given under :blue:`GROUP_FIBRE`.

.. _u4.26.01.4:

Example
=======

We want to build a section of a reinforced concrete beam, with two cylindrical steel reinforcements of diameter 32 mm.

We create a data structure named :blue:`fib_grp` comprising two groups of fibers:

* one is named :blue:`conc_s`, and is created with a plane mesh,
* the other is named :blue:`rebar_s`, and is created with two point fibers for the steel rebars.

::

  sec_mesh = LIRE_MAILLAGE(UNIT = 21)

  fib_grp = DEFI_GEOM_FIBRE(SECTION = _F(GROUP_FIBRE = 'conc_s', 
                                         MAILLAGE_SECT = sec_mesh,
                                         TOUT_SECT = 'OUI',
                                         COOR_AXE_POUTRE = (0., 0.)),
                            FIBRE = _F(GROUP_FIBRE = 'rebar_s',
                                       CARA = 'DIAMETRE',
                                       VALE = (0.05, -0.2, 32.E-3,
                                               -0.05, -0.2, 32.E-3),
                                       COOR_AXE_POUTRE = (0., 0.)))

