import salome
import salome_notebook

import GEOM
import SMESH
import SALOMEDS

from salome.geom import geomBuilder
from salome.smesh import smeshBuilder
from salome.StdMeshers import StdMeshersBuilder

import math

ExportPATH="/home/banerjee/Salome/forma08/"

# Initialize
salome.salome_init()
notebook = salome_notebook.notebook

#---------------------
# Create geometry
#---------------------
geompy = geomBuilder.New()

# Parameters
length_mm = 20
width_mm = 6
height_mm = 2
crack_mm = 5
joint_width_mm = 0.1

# Create reference vertices
v_B = geompy.MakeVertex(crack_mm, height_mm, 0)
v_C = geompy.MakeVertex(length_mm, 0, width_mm)
v_D = geompy.MakeVertex(crack_mm, -joint_width_mm, 0)
v_E = geompy.MakeVertex(0, 0, width_mm)

# Add vertices to study
geompy.addToStudy(v_B, 'B')
geompy.addToStudy(v_C, 'C')
geompy.addToStudy(v_D, 'D')
geompy.addToStudy(v_E, 'E')

# Create boxes
box_EB = geompy.MakeBoxTwoPnt(v_E, v_B)
box_CB = geompy.MakeBoxTwoPnt(v_C, v_B)
box_CD = geompy.MakeBoxTwoPnt(v_C, v_D)

# Join the boxes while retaining partition
DCB = geompy.MakePartition([v_E, box_EB, box_CB, box_CD], [], [], [], geompy.ShapeType["SOLID"], 0, [], 0)

# Add the body to the study
geompy.addToStudy( DCB, 'DCB' )

# Create geometry groups
g_E         = geompy.CreateGroup(DCB, geompy.ShapeType["VERTEX"])
g_disp_edge = geompy.CreateGroup(DCB, geompy.ShapeType["EDGE"])
g_joint_bot = geompy.CreateGroup(DCB, geompy.ShapeType["FACE"])
g_joint_top = geompy.CreateGroup(DCB, geompy.ShapeType["FACE"])
g_DCB_crack = geompy.CreateGroup(DCB, geompy.ShapeType["SOLID"])
g_DCB_beam  = geompy.CreateGroup(DCB, geompy.ShapeType["SOLID"])
g_DCB_joint = geompy.CreateGroup(DCB, geompy.ShapeType["SOLID"])

# Locate and add the vertex E
vertex = geompy.GetShapesNearPoint(DCB, v_E, geompy.ShapeType['VERTEX'])
geompy.UnionList(g_E, [vertex])
geompy.addToStudyInFather( DCB, g_E, 'ref_point' )

# Locate and add the edge on which displacement bc is to be applied
vertex = geompy.MakeVertex(0, 0, 0.5*width_mm)
edge = geompy.GetShapesNearPoint(DCB, vertex, geompy.ShapeType['EDGE'])
geompy.UnionList(g_disp_edge, [edge])
geompy.addToStudyInFather( DCB, g_disp_edge, 'disp_edge' )

# Locate and add the symmetry face
vertex = geompy.MakeVertex(1.5*crack_mm, -joint_width_mm, 0.5*width_mm)
face = geompy.GetShapesNearPoint(DCB, vertex, geompy.ShapeType['FACE'])
geompy.UnionList(g_joint_bot, [face])
geompy.addToStudyInFather( DCB, g_joint_bot, 'joint_bot' )

# Locate and add the joint top face
vertex = geompy.MakeVertex(1.5*crack_mm, 0.0, 0.5*width_mm)
face = geompy.GetShapesNearPoint(DCB, vertex, geompy.ShapeType['FACE'])
geompy.UnionList(g_joint_top, [face])
geompy.addToStudyInFather( DCB, g_joint_top, 'joint_top' )

# Locate and add the box above crack
vertex = geompy.MakeVertex(0.5*crack_mm, 0.5*height_mm, 0.5*width_mm)
volume = geompy.GetShapesNearPoint(DCB, vertex, geompy.ShapeType['SOLID'])
geompy.UnionList(g_DCB_crack, [volume])
geompy.addToStudyInFather( DCB, g_DCB_crack, 'DCB_crack' )

# Locate and add the box above joint
vertex = geompy.MakeVertex(1.5*crack_mm, 0.5*height_mm, 0.5*width_mm)
volume = geompy.GetShapesNearPoint(DCB, vertex, geompy.ShapeType['SOLID'])
geompy.UnionList(g_DCB_beam, [volume])
geompy.addToStudyInFather( DCB, g_DCB_beam, 'DCB_beam' )

# Locate and add the joint 
vertex = geompy.MakeVertex(1.5*crack_mm, -joint_width_mm, 0.5*width_mm)
volume = geompy.GetShapesNearPoint(DCB, vertex, geompy.ShapeType['SOLID'])
geompy.UnionList(g_DCB_joint, [volume])
geompy.addToStudyInFather( DCB, g_DCB_joint, 'DCB_joint' )

#---------------------
# Create mesh
#---------------------
smesh = smeshBuilder.New()
Hexa_3D = smesh.CreateHypothesis('Hexa_3D')

DCB_mesh = smesh.Mesh(DCB)
smesh.SetName(DCB_mesh, 'DCB')

# Set up algorithms
Regular_1D = DCB_mesh.Segment()
Max_Size = Regular_1D.MaxSize(1.5)
Max_Size.SetLength(1)
Quadrangle_2D = DCB_mesh.Quadrangle()
status = DCB_mesh.AddHypothesis(Hexa_3D)

# Set object names
smesh.SetName(Regular_1D.GetAlgorithm(), 'Regular_1D')
smesh.SetName(Max_Size, 'Max Size')
smesh.SetName(Quadrangle_2D.GetAlgorithm(), 'Quadrangle_2D')
smesh.SetName(Hexa_3D, 'Hexa_3D')

# Compute the mesh
isDone = DCB_mesh.Compute()

# Map the geometry groups to the mesh
E_1 = DCB_mesh.GroupOnGeom(g_E, 'ref_point', SMESH.NODE)
disp_edge_1 = DCB_mesh.GroupOnGeom(g_disp_edge, 'disp_edge', SMESH.EDGE)
joint_bot_1 = DCB_mesh.GroupOnGeom(g_joint_bot, 'joint_bot', SMESH.FACE)
joint_top_1 = DCB_mesh.GroupOnGeom(g_joint_top, 'joint_top', SMESH.FACE)
DCB_crack_1 = DCB_mesh.GroupOnGeom(g_DCB_crack, 'DCB_crack', SMESH.VOLUME)
DCB_beam_1 = DCB_mesh.GroupOnGeom(g_DCB_beam, 'DCB_beam', SMESH.VOLUME)
DCB_joint_1 = DCB_mesh.GroupOnGeom(g_DCB_joint, 'DCB_joint', SMESH.VOLUME)

# Export mesh
DCB_mesh.ExportMED( r''+ExportPATH+'forma08b.mmed'+'')

# Update object tree
if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()
