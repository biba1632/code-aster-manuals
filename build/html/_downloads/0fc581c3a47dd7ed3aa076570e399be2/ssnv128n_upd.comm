# SSNV128N
DEBUT(LANG = 'EN',
      IGNORE_ALARM=('MODELE1_63'))

# Read mesh
mesh = LIRE_MAILLAGE(VERI_MAIL = _F(VERIF = 'OUI'),
                     FORMAT = 'MED')

# Reorient faces of mesh
mesh = MODI_MAILLAGE(reuse = mesh,
                     MAILLAGE = mesh,
                     ORIE_PEAU_3D = (_F(GROUP_MA = 'SPRESV'),
                                     _F(GROUP_MA = 'SPRESH')))

# Add element and node groups
mesh = DEFI_GROUP(reuse = mesh,
                  MAILLAGE = mesh,
                  CREA_GROUP_NO = (_F(NOM = 'NCONTAR',
                                      GROUP_MA = 'SCONTAR'),
                                   _F(NOM = 'NPLAQ',
                                      GROUP_MA = 'VPLAQ'),
                                   _F(NOM = 'NPLAQZ',
                                      DIFFE = ('NPLAQ', 'NCONTAR'))))

# Assign 3D elements
model = AFFE_MODELE(MAILLAGE = mesh,
                    #DISTRIBUTION = _F(METHODE = 'CENTRALISE'),
                    AFFE = _F(TOUT = 'OUI',
                              PHENOMENE = 'MECANIQUE',
                              MODELISATION = '3D'))

# Define materials
plate = DEFI_MATERIAU(ELAS = _F(E = 1.3E11,
                                NU = 0.2))

rigid = DEFI_MATERIAU(ELAS = _F(E = 1.0E16,
                                NU = 0.0))

# Assign material
mater = AFFE_MATERIAU(MAILLAGE = mesh,
                      AFFE = (_F(GROUP_MA = 'VPLAQ',
                                  MATER = plate),
                              _F(GROUP_MA = 'VBATI',
                                  MATER = rigid)))

# Assign BCs
bc = AFFE_CHAR_MECA(MODELE = model,
                    DDL_IMPO = (_F(GROUP_MA = 'SENCA',  
                                   DX = 0.0, DY = 0.0, DZ = 0.0),
                                _F(GROUP_MA = 'SBLBAX', 
                                   DX = 0.0, DY = 0.0, DZ = 0.0),
                                _F(GROUP_MA = 'SBLOCX', 
                                   DX = 0.0),
                                _F(GROUP_MA = 'LBLOCY', 
                                   DY = 0.0),
                                _F(GROUP_NO = 'NPLAQZ', 
                                   DZ = 0.0)),
                    PRES_REP = (_F(GROUP_MA = 'SPRESV', 
                                   PRES = 5.E07),
                                _F(GROUP_MA = 'SPRESH', 
                                   PRES = 15.E07)))
                  
# Define discrete penalty contact with Coulomb friction
contact = DEFI_CONTACT(MODELE = model,
                       FORMULATION = 'CONTINUE',
                       FROTTEMENT = 'COULOMB',
                       ALGO_RESO_CONT = 'POINT_FIXE',
                       ALGO_RESO_GEOM = 'POINT_FIXE',
                       ALGO_RESO_FROT = 'NEWTON',
                       RESI_FROT = 2.E-3,
                       REAC_GEOM = 'CONTROLE',
                       NB_ITER_GEOM = 2,
                       ZONE = _F(GROUP_MA_MAIT = 'SCONTA',
                                 GROUP_MA_ESCL = 'SBATI',
                                 SANS_GROUP_NO = ('PBS', 'PBZ'),
                                 COULOMB = 1.0,
                                 COEF_FROT = 1.0E5))

# Define ramp function
f_ramp = DEFI_FONCTION(NOM_PARA = 'INST',
                       PROL_GAUCHE = 'LINEAIRE',
                       PROL_DROITE = 'LINEAIRE',
                       VALE = (0.0, 0.0,
                               1.0, 1.0))

# Define time stepping list
l_reals = DEFI_LIST_REEL(DEBUT = 0.0,
                         INTERVALLE = _F(JUSQU_A = 1.0, 
                                         NOMBRE = 3))

# Define time stepping scheme
l_times = DEFI_LIST_INST(METHODE = 'MANUEL',
                         DEFI_LIST = _F(LIST_INST = l_reals),
                         ECHEC = _F(EVENEMENT = 'ERREUR',
                                    ACTION = 'DECOUPE',
                                    SUBD_METHODE = 'MANUEL',
                                    SUBD_PAS = 5,
                                    SUBD_NIVEAU = 5))

#-----------------------------------------------------------
# Run simulation (default solver)
result = STAT_NON_LINE(SOLVEUR = _F(),
                       MODELE = model,
                       CHAM_MATER = mater,
                       EXCIT = (_F(CHARGE = bc,
                                   FONC_MULT = f_ramp)),
                       CONTACT = contact,
                       COMPORTEMENT = _F(RELATION = 'ELAS'),
                       NEWTON = _F(REAC_ITER = 1),
                       INCREMENT = _F(LIST_INST = l_times),
                       CONVERGENCE = _F(ARRET = 'OUI',
                                        ITER_GLOB_MAXI = 20,
                                        RESI_GLOB_RELA = 1.0E-5),
                       INFO = 1)

# Compute stress and contact pressure
result = CALC_CHAMP(reuse = result,
                    CONTRAINTE = ('SIGM_ELNO'),
                    VARI_INTERNE = ('VARI_ELNO'),
                    RESULTAT = result)

# Extract nodal displacements
disp = CREA_CHAMP(OPERATION = 'EXTR',
                  TYPE_CHAM = 'NOEU_DEPL_R',
                  NOM_CHAM = 'DEPL',
                  RESULTAT = result,
                  INST = 1.0)

# Write out results
IMPR_RESU(FORMAT = 'MED',
          UNITE = 80,
          RESU = _F(RESULTAT = result))

#-----------------------------------------------------------
# Verify results

# Result1
TEST_RESU(RESU = (_F(GROUP_NO = 'PPA',
                     INST = 1.0,
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result,
                     NOM_CHAM = 'DEPL',
                     NOM_CMP = 'DX',
                     VALE_CALC =  2.92219493E-05,
                     VALE_REFE = 2.8600000000000001E-05,
                     PRECISION = 0.050000000000000003),
                  _F(GROUP_NO = 'PPB',
                     INST = 1.0,
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result,
                     NOM_CHAM = 'DEPL',
                     NOM_CMP = 'DX',
                     VALE_CALC =  2.77505673E-05,
                     VALE_REFE = 2.72E-05,
                     PRECISION = 0.050000000000000003),
                  _F(GROUP_NO = 'PPC',
                     INST = 1.0,
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result,
                     NOM_CHAM = 'DEPL',
                     NOM_CMP = 'DX',
                     VALE_CALC =  2.31935654E-05,
                     VALE_REFE = 2.2799999999999999E-05,
                     PRECISION = 0.050000000000000003),
                  _F(GROUP_NO = 'PPD',
                     INST = 1.0,
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result,
                     NOM_CHAM = 'DEPL',
                     NOM_CMP = 'DX',
                     VALE_CALC =  2.01196284E-05,
                     VALE_REFE = 1.98E-05,
                     PRECISION = 0.050000000000000003),
                  _F(GROUP_NO = 'PPE',
                     INST = 1.0,
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result,
                     NOM_CHAM = 'DEPL',
                     NOM_CMP = 'DX',
                     VALE_CALC =  1.56973567E-05,
                     VALE_REFE = 1.5E-05,
                     PRECISION = 0.050000000000000003)))

# End
FIN()
