# -*- coding: utf-8 -*-
import salome
from salome.geom import geomBuilder
geompy = geomBuilder.New()

import SMESH
from salome.smesh import smeshBuilder
smesh = smeshBuilder.New()

import math

############# Path where results will be stored ############
ExportPATH="/home/banerjee/Salome/forma01c/"
############################################################

#===================================================
#                  GEOMETRY
#===================================================

# Create Sketcher 2D and add geometry
sketch = geompy.Sketcher2D()
sketch.addPoint(0, 10)
sketch.addSegmentAbsolute(0, 150)
sketch.addSegmentAbsolute(100, 150)
sketch.addSegmentAbsolute(100, 0)
sketch.addSegmentAbsolute(10, 0)
sketch.addArcAngleRadiusLength(-90, 10, 90)

Wire_1 = sketch.wire([0, 0, 0, 0, 0, 1, 1, 0, -0])

Plate = geompy.MakeFaceWires([Wire_1], 1)

[hole, left, bottom, top, right] = geompy.SubShapeAllSorted(Plate, geompy.ShapeType["EDGE"])

# Vertices
A = geompy.MakeVertex(0, 10, 0)
B = geompy.MakeVertex(10, 0, 0)
D = geompy.MakeVertex(100, 0, 0)
F = geompy.MakeVertex(100, 150, 0)
G = geompy.MakeVertex(0, 150, 0)

# Node groups
GA = geompy.GetVertexNearPoint(Plate, A)
GB = geompy.GetVertexNearPoint(Plate, B)
GD = geompy.GetVertexNearPoint(Plate, D)
GF = geompy.GetVertexNearPoint(Plate, F)
GG = geompy.GetVertexNearPoint(Plate, G)

# Assign names
id_Plate = geompy.addToStudy(Plate, "Plate")
geompy.addToStudyInFather(Plate, hole, "hole")
geompy.addToStudyInFather(Plate, left, "left")
geompy.addToStudyInFather(Plate, right, "right")
geompy.addToStudyInFather(Plate, bottom, "bottom")
geompy.addToStudyInFather(Plate, top, "top")
geompy.addToStudyInFather(Plate, GA, "A")
geompy.addToStudyInFather(Plate, GB, "B")
geompy.addToStudyInFather(Plate, GD, "D")
geompy.addToStudyInFather(Plate, GF, "F")
geompy.addToStudyInFather(Plate, GG, "G")

#===================================================
#                  MESH
#===================================================

M_Plate = smesh.Mesh(Plate, "Plate")

from salome.NETGENPlugin import NETGENPluginBuilder
NETGEN_2D = M_Plate.Triangle(algo = smeshBuilder.NETGEN)
NETGEN_2D_Parameters=NETGEN_2D.Parameters()
#
NETGEN_2D_Parameters.SetFineness( 6 )
NETGEN_2D_Parameters.SetGrowthRate( 0.15 )
NETGEN_2D_Parameters.SetNbSegPerEdge( 2.0 )
NETGEN_2D_Parameters.SetNbSegPerRadius( 5.0 )
NETGEN_2D_Parameters.SetMinSize( 1.0 )
NETGEN_2D_Parameters.SetQuadAllowed( 0 )
NETGEN_2D_Parameters.SetSecondOrder( 1 )
NETGEN_2D_Parameters.SetOptimize( 1 )
#

isDone = M_Plate.Compute()
if not isDone: 
   print('Mesh', M_Plate.GetMesh(), ': computation failed')

M_Plate.Group(hole, 'hole')
M_Plate.Group(left, 'left')
M_Plate.Group(right, 'right')
M_Plate.Group(bottom, 'bottom')
M_Plate.Group(top, 'top')
M_Plate.Group(GA, 'A')
M_Plate.Group(GB, 'B')
M_Plate.Group(GD, 'D')
M_Plate.Group(GF, 'F')
M_Plate.Group(GG, 'G')

######## Export the mesh in MED format ########
M_Plate.ExportMED( r''+ExportPATH+'forma01c.mmed'+'')

# Update geometry tree (if there is a graphical interface)
#---------------------------------------------------------
if salome.sg.hasDesktop():
   gg = salome.ImportComponentGUI("GEOM")
   gg.createAndDisplayFitAllGO(id_Plate)
   salome.sg.updateObjBrowser()

