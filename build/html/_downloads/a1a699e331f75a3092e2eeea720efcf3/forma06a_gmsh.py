import gmsh
import sys
import math

# Initialize gmsh
gmsh.initialize()

# Create model
model = gmsh.model
geo = model.geo
mesh = model.mesh
model.add("forma06a")

# Plate size
W = 2
H = 2

# Element size
lc = W / 100.0

# Add four vertices
p1 = geo.addPoint(-1, -1, 0)
p2 = geo.addPoint(1, -1, 0)
p3 = geo.addPoint(1, 1, 0)
p4 = geo.addPoint(-1, 1, 0)

# Add lines for the boundary
l1 = geo.addLine(p1, p2)
l2 = geo.addLine(p2, p3)
l3 = geo.addLine(p3, p4)
l4 = geo.addLine(p4, p1)

# Add curve loops
loop1 = geo.addCurveLoop([l1, l2, l3, l4])

# Add plane surface
face1 = geo.addPlaneSurface([loop1])

# Update
geo.synchronize()

# Add physical group for the plate
plate = model.addPhysicalGroup(2, [face1])
model.setPhysicalName(2, plate, "plate")

# Add physical groups for the top and bottom edges
bot_edge = model.addPhysicalGroup(1, [l1])
top_edge = model.addPhysicalGroup(1, [l3])
model.setPhysicalName(1, bot_edge, "bot_edge")
model.setPhysicalName(1, top_edge, "top_edge")

# Add physical groups of the two bottom vertices
bot_left_vert = model.addPhysicalGroup(0, [p1])
bot_right_vert = model.addPhysicalGroup(0, [p2])
model.setPhysicalName(1, bot_left_vert, "bot_left_vert")
model.setPhysicalName(1, bot_right_vert, "bot_right_vert")

# Update
geo.synchronize()

# Set transfinite (for quadrilateral elements)
NN = 100
for curve in model.getEntities(1):
    mesh.setTransfiniteCurve(curve[1], NN)   
    gmsh.model.mesh.setRecombine(curve[0], curve[1])
    gmsh.model.mesh.setSmoothing(curve[0], curve[1], 100)
    
for surf in model.getEntities(2):
    mesh.setTransfiniteSurface(surf[1])

# Set gmsh algorithm.  Make sure the output file is in version 2.2
gmsh.option.setNumber('Mesh.Algorithm', 8)
gmsh.option.setNumber('Mesh.RecombineAll', 1)
gmsh.option.setNumber('Mesh.MshFileVersion', 2.2)

# Generate mesh
mesh.generate(2)
mesh.removeDuplicateNodes()

# Save mesh
gmsh.write("forma06a.msh")

# Finish
gmsh.finalize()

