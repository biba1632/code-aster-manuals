In this appendix, we show that the problem of constrained minimization of the functional
$\CalJ(\Bu)$ is equivalent to an algebraic problem.

The minimization problem can be stated as:
\Beq
   :label: appendix_prob1
   \min_{\Bu \in \BV} \CalJ(\Bu)
   \quad \text{where} \quad
   \CalJ(\Bu) = \tfrac{1}{2} (\BA \Bu, \Bu) - (\Bb, \Bu)
\Eeq
where the subspace $\BV \subset \mathbb{R}^n$ is spans the set of $n$ unknowns $\Bu$
minus the $p$ affine constraints $\BC_i\Bu - \Bd_i = 0~,~~i=1\dots p$.  Both
$\BA$ and $\Bb$ are defined on $\mathbb{R}^n$, and $\BA$ is positive and symmetric.

The algebraic problem can be stated as:
\Beq
   :label: appendix_prob2
   \text{Find}~\Bu \in \BV \quad \text{such that} \quad
   (\BA\Bu, \Bv_0) - (\Bb, \Bv_0) = 0 \quad \forall \Bv_0 \in \BV_0
\Eeq
Here, the space $\BV$ is the same as that in :eq:`appendix_prob1`, i.e.,
\Beq
  \BV = \left\{\Bu \in \mathbb{R}^n ~\text{such that} \BC_i\Bu - \Bd_i = 0, i=1 \dots p\right\}
\Eeq
The space $\BV_0$ is a variation of $\BV$:
\Beq
  \BV_0 = \left\{\Bv_0 \in \mathbb{R}^n ~\text{such that} \BC_i\Bv_0 = 0, i=1 \dots p\right\}
\Eeq
We wish to show that problems :eq:`appendix_prob1` and :eq:`appendix_prob2` are equivalent.

To start, observe that :eq:`appendix_prob2` can be expressed as:
\Beq
   :label: appendix_prob3
   \text{Find}~\Bu \in \BV \quad \text{such that} \quad
   (\BA\Bu, \Bv - \Bu) - (\Bb, \Bv - \Bu) = 0 \quad \forall \Bv \in \BV
\Eeq
because of the bijection between $\{\Bv - \Bu, (\Bu, \Bv) \in \BV\}$ and $\BV_0$. 

To show that :eq:`appendix_prob3` is equivalent to :eq:`appendix_prob1`, we note that
for all $\Bv \in \BV$, 
\Beq
  \Bal
    \CalJ(\Bv) - \CalJ(\Bu) & = 
      \tfrac{1}{2} (\BA \Bv, \Bv) - (\Bb, \Bv) - 
      \tfrac{1}{2} (\BA \Bu, \Bu) - (\Bb, \Bu) \\
     & =  \tfrac{1}{2} (\BA \Bv, \Bv) - (\BA\Bu, \Bv - \Bu)  -
      \tfrac{1}{2} (\BA \Bu, \Bu)  \\
     & =  \tfrac{1}{2} (\BA \Bv, \Bv) - (\BA\Bu, \Bv)  +
      \tfrac{1}{2} (\BA \Bu, \Bu)  \\
     & = \tfrac{1}{2} \BA(\Bu-\Bv, \Bu-\Bv) \ge 0
  \Eal
\Eeq
The directional directive of $\CalJ(\Bu)$ in the direction $\Bv_0$ is
\Beq
  \Bal
  \CalJ^{'}(\Bu)\cdot\Bv_0 & = \lim_{\Veps\rightarrow 0}
     \frac{\CalJ(\Bu+\Veps\Bv_0) - \CalJ(\Bu)}{\Veps} \\
   & = \lim_{\Veps\rightarrow 0} (\BA\Bu, \Bv_0) + \frac{\Veps}{2} (\BA\Bv_0,\Bv_0)
                                  - (\Bb, \Bv_0) \\
   & = (\BA\Bu - \Bb) \Bv_0
  \Eal
\Eeq
Now, if we take $\Bv \in \BV$ and let $\Bv_0 = \Bv - \Bu,~\Bv_0 \in \BV_0$, we have,
for all $\Veps > 0$,  
\Beq
  \frac{\CalJ(\Bu+\Veps\Bv_0) - \CalJ(\Bu)}{\Veps} \ge 0
  \quad \implies \quad
  \CalJ^{'}(\Bu)\cdot\Bv_0 \ge 0
\Eeq
This is only possible if the derivative is zero.  Therefore,
\Beq
  \CalJ^{'}(\Bu)\cdot(\Bv - \Bu) = & = (\BA\Bu - \Bb) (\Bv - \Bu) = 0 \quad \forall \Bv \in \BV
\Eeq
which is equivalent to :eq:`appendix_prob3`.
