.. role:: red
.. role:: blue
.. role:: green
.. role:: gray
.. role:: python(code)
   :language: python
.. |br| raw:: html

   <br/>

.. index:: ! DEFI_CONSTANTE

.. _u4.31.01:

*******************************************************************************
**[U4.31.01]** : Operator ``DEFI_CONSTANTE`` (:gray:`DEFINE_CONSTANT_FUNCTION`)
*******************************************************************************

.. _u4.31.01.1:

Purpose
========

Define the value of an invariant quantity.

This operator is a facility offered each time that a concept of the function type is expected and the data 
to be entered is constant. This makes it possible to define, for example, materials with 
temperature-independent properties for commands that allow processing of materials whose properties 
vary with temperature.

.. warning::

   Be careful not to confuse this with the definition of a real parameter (ex: a = 3.).

.. _u4.31.01.2:

Syntax
======

::

   f [function] = DEFI_CONSTANTE
   (
     ◊ NOM_RESU = / 'TOUTRESU',             [DEFAULT]
                  / nr,                     [K8]
     ♦ VALE = value,                        [R]
     ◊ TITRE = title,                       [l_Kn]
   )

.. _u4.31.01.3:

Operands
========

.. _u4.31.01.3.1:

Operand :blue:`NOM_RESU` (:gray:`RESULT_NAME`)
----------------------------------------------

::

  ◊ NOM_RESU = nr

Designates the name of the result, the function created is a function whose identifier is :green:`nr` 
(8 characters maximum).

.. note::

   Certain commands (:red:`CALC_FONCTION`, :red:`DEFI_MATERIAU` etc.) check the coherence of the
   result and parameter names according to their context.

.. _u4.31.01.3.2:

Operand :blue:`VALE` (:gray:`VALUE`)
-------------------------------------

::

  ♦ VALE = value

Value of the constant (real number).

.. _u4.31.01.3.3:

Operand :blue:`TITRE` (:gray:`TITLE`)
-------------------------------------

::

  ◊ TITRE = title

Title attached to the concept produced by this operator :ref:`[U4.03.01] <u4.03.01>`.

.. _u4.31.01.4:

Examples
========


* Define the constant function "1." :

  ::

    f_one = DEFI_CONSTANTE(VALE = 1.)

  The :green:`f_one` function represents "any kind of result" (:blue:`TOUTRESU`) by default.

* Define a constant function representing a constant Young's modulus

  ::

    f_young = DEFI_CONSTANTE(VALE = 2.1E11, 
                             NOM_RESU = ‘E’)
