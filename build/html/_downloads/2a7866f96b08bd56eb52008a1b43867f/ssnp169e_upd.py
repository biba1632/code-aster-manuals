# coding: utf-8
import gmsh
import sys
import math
gmsh.initialize()
# Use OpenCASCADE kernel
model = gmsh.model
occ = model.occ
mesh = model.mesh
model.add("ssnp169e")
# Default element size
el_size = 0.2
# Geometry
R1 = 1.0
R3 = 0.2
# Add points
pA = occ.addPoint(R1, R1, 0, el_size)
pB = occ.addPoint(-R1, R1, 0, el_size)
pC = occ.addPoint(-R1, -R1, 0, el_size)
pD = occ.addPoint(R1, -R1, 0, el_size)
# Add lines and arcs
lAB = occ.addLine(pA, pB)
lBC = occ.addLine(pB, pC)
lCD = occ.addLine(pC, pD)
lDA = occ.addLine(pD, pA)
# Add curve loops and plane surfaces
wABCD = occ.addCurveLoop([lAB, lBC, lCD, lDA])
sABCD = occ.addPlaneSurface([wABCD])
occ.synchronize()
# For transfinite interpolation
num_nodes = 31
for curve in [lAB, lBC, lCD, lDA]:
    mesh.setTransfiniteCurve(curve, num_nodes, meshType = "Progression", coef = 1)
for surf in [sABCD]:
    mesh.setTransfiniteSurface(surf)
mesh.setRecombine(2, sABCD)
# Physical groups for the edges
AB_g = model.addPhysicalGroup(1, [lAB])
model.setPhysicalName(1, AB_g, 'top')
BC_g = model.addPhysicalGroup(1, [lBC])
model.setPhysicalName(1, BC_g, 'left')
CD_g = model.addPhysicalGroup(1, [lCD])
model.setPhysicalName(1, CD_g, 'bottom')
DA_g = model.addPhysicalGroup(1, [lDA])
model.setPhysicalName(1, DA_g, 'right')
# Physical groups for the faces
square_g = model.addPhysicalGroup(2, [sABCD])
model.setPhysicalName(2, square_g, 'square')
#gmsh.option.setNumber('Mesh.Algorithm', 8)
gmsh.option.setNumber('Mesh.RecombinationAlgorithm', 1)
gmsh.option.setNumber('Mesh.ElementOrder', 1)
gmsh.option.setNumber('Mesh.MshFileVersion', 2.2)
gmsh.option.setNumber('Mesh.MedFileMinorVersion', 0)
gmsh.option.setNumber('Mesh.SaveAll', 0)
gmsh.option.setNumber('Mesh.SaveGroupsOfNodes', 1)
# gmsh.option.setNumber('Mesh.SecondOrderIncomplete', 1)
# Generate mesh
mesh.generate(2)
# Save mesh
gmsh.write("ssnp169e_upd.med")
gmsh.fltk.run()
gmsh.finalize()
