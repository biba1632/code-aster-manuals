.. role:: red
.. role:: blue
.. role:: green
.. role:: gray

.. _v6.04.222:

*********************************************************************
**V6.04.222**: FORMA04 - Tutorial on axisymmetric Hertzian contact
*********************************************************************

In this tutorial we will model Hertzian contact, one of the few non-trivial cases of contact 
for which we have an analytic solution.  The model consists of two hemispheres which are brought 
into contact with one another. This simple test will illustrate how to model contact, the 
different ways of solving the problem, and the tools available to post-process a contact computation.

We will explore three models in this tutorial:

* **Model A**: Using the default contact formulation ``DISCRETE`` and contact algorithm ``CONTRAINTE``
* **Model B**: Using ``CONTINUOUS`` contact with Lagrange multipliers 
* **Model C**: Using quadrilateral elements with ``DISCRETE`` contact

Reference problem
=================

Geometry
--------

.. image:: v6.04.222/fig_6.04.222_1.svg
   :height: 350 px
   :align: right

The model consists of two hemispheres of equal radius :math:`R` = 50 mm. We can therefore treat 
the problem as axisymmetric and only model quarter discs. The geometry of the problem is shown 
in the figure.

Material properties
-------------------

The material is linear elastic:

* Young's modulus: :math:`E` = 20000 MPa
* Poisson's ratio: :math:`\nu` = 0.3

Boundary conditions and loads
-----------------------------

* Displacement :math:`u_x` (DX) is zero on axis :math:`A_1A_2` (:blue:`symm_axis`) (this condition 
  is implicit in axisymmetric simulations, it is nevertheless imposed to include points which 
  are not perfectly on the axis)
* Displacement :math:`u_y` (DY) prescribed as 2 mm on :math:`A_1B_1` (:blue:`bot_edge`)
  and −2 mm on :math:`A_2B_2` (:blue:`top_edge`)

Reference solution
==================

.. _hertz_ref_sol:

Calculation method
------------------

In 1881, Hertz established under certain assumptions a solution to the problem which bears his 
name [#hertz1881]_.  Assuming frictionless contact and small deformations for which the half-width of contact 
:math:`a` is very small relative to the radius of the spheres :math:`R` (:math:`a  \ll R`), the 
contact pressure at the points :math:`C_1` and :math:`C_2` is [#dumont1993]_:

.. math::
    :label: hertz_pressure

    P_0 = -\frac{E}{\pi(1-\nu^2)} \sqrt{\frac{2h}{R}}

where :math:`h` corresponds to the imposed displacement, which is 4 mm in this example.

The half-width of contact :math:`a` is 

.. math::
    :label: hertz_contact_width

    a = \sqrt{\frac{Rh}{2}}

The contact surface is a disk with radius :math:`a`, the pressure distribution as a function of 
radius :math:`r` in this area is:

.. math::
    :label: hertz_pressure_distrib

    P(r) = P_0 \sqrt{1 - \left(\frac{r}{a}\right)^2} \quad \text{for} \quad r \le a

Reference values
----------------

We will compare the pressure in the center of the contact zone and the half-width of contact with 
the analytical solution.

Uncertainties on the solution
-----------------------------

None (under the assumptions given above).

References
----------

.. [#hertz1881] HERTZ H, Über die Berührung fester elastischer Körper, 1881 
.. [#dumont1993] DUMONT G, The method of the active stresses applied to the unilateral contact, 1993


**Model A**: Using the default contact formulation ``DISCRETE`` and contact algorithm ``CONTRAINTE``
====================================================================================================

Creating and running the tutorial
---------------------------------

We will first model and mesh the geometry using the graphical interface in ``Salome-Meca``.  
The Python script for creating the geometry and mesh is listed after that.

Geometry
^^^^^^^^

.. important::

   Read the entire procedure carefully before starting.

We will create the geometry with the :blue:`Geometry` module.

* The :blue:`Sketcher` tool (:red:`New Entity → Basic → 2D Sketch`) allows you to build the boundary
  of the upper disc: :math:`A_2, B_2, D_2` and :math:`C_2`. The arc is made in 2 steps so that point 
  :math:`D_2` is created during the construction of the boundary. Start with point :math:`C_2` and then
  create the arc :math:`C_2 D_2` in :blue:`Direction` mode using a start (tangent) angle of 0, a radius 
  of 50, and an arc angle of 45°. Then create :math:`D_2 B_2` using the same approach and values. Add
  the line segment :math:`B_2 A_2` and, finally, close the contour. 

* Build a face from this contour (:red:`New Entity → Build → Face`).

* Use symmetry to generate the second sphere (:red:`Operations → Transformation → Mirror Image`) after
  choosing the X axis as the axis of symmetry.

* Assemble the two spheres to constitute a single GEOM object (:red:`New Entity → Build → Compound`). 

  .. admonition:: Warning

     In this compound object, there are always two points, :math:`C_1` and :math:`C_2`, which overlap. 
     On the contrary, the two points merge if the :blue:`Partition` operation is used.

* Create groups on this geometry (with the names suggested in the figure above) using
  :red:`New Entity → Group → Create Group`. Note that groups are created last.

* Create the groups for the faces of the :blue:`top_sphere` and :blue:`bot_sphere` hemispheres

* To create groups for the superimposed points :math:`C_1` and :math:`C_2`, we use the functionality
  used to select a sub-element of a GEOM entity: check mark :blue:`Only Sub-shapes of the Second Shape`,
  then for :math:`C_2` select as :blue:`Second Shape` the upper hemisphere in the object tree, and only 
  its points will be highlighted.

* Create groups for the application of boundary conditions: along the axis of axisymmetry 
  (:math:`A_1A_2`, :blue:`symm_axis`) and on the upper and lower parts of the hemispheres 
  (:math:`A_1B_1` and :math:`A_2B_2`, :blue:`bot_edge` and :blue:`top_edge`).

* For contact modeling, we will need mesh groups representing potential contact surfaces: create 
  the :blue:`top_surf` and :blue:`bot_surf` groups.

Meshing
^^^^^^^

.. figure:: v6.04.222/fig_6.04.222_2.png
   :height: 350 px
   :align: right

   Mesh for Model A

Open the :blue:`Mesh` module. 

* Initialize mesh creation with :red:`Mesh → Create Mesh`. 

* A triangle mesh will be used in this example. Create the automatic meshing hypotheses 
  :blue:`Assign a set of hypotheses → 2D: Automatic Triangulation` and choose a maximum length of 
  2mm.

* Compute the mesh (:red:`Mesh → Compute`). The mesh will contain approximately 3000 triangles and 
  1500 nodes.

* Import the groups from the geometry (:red:`Mesh → Create Groups from Geometry`).

* Export the mesh in MED format.

Python scripting
^^^^^^^^^^^^^^^^

You can also create the model and the mesh using a Python script 
(download from :download:`here <v6.04.222/forma04a.py>`).  The annotated script is listed below.

.. include::  v6.04.222/forma04a.py
     :literal:

Command file
^^^^^^^^^^^^

To create the command file, open the :blue:`AsterStudy` module. Then in the left column, click on 
the :blue:`Case View` tab and then right click on :blue:`CurrentCase` and choose 
:blue:`Add Stage`).

* Try to first compute the solution **without contact** using the command STAT_NON_LINE.

  The steps needed to create the command file:

  * Read the mesh in MED format: :red:`LIRE_MAILLAGE`.
  * Define the material: :red:`DEFI_MATERIAU`.
  * Assign the material to all the elements: :red:`AFFE_MATERIAU`.
  * Define the finite elements used: :red:`AFFE_MODELE` with the phenomenon :blue:`MECHANIQUE` 
    and axisymmetric (:blue:`AXIS`) modeling.
  * Assign the boundary conditions / mechanical loads: :red:`AFFE_CHAR_MECA` / :blue:`DDL_IMPO` for 
    axial symmetry and unit imposed displacements that we will then multiply by a ramp function 
    over time.
  * Define the load ramp (imposed displacement) with the command :red:`DEFI_FONCTION`. Let us define
    the function to vary between (0., 0.) And (2., 2.).
  * Create the time discretization using :red:`DEFI_LIST_REEL`. Let us use 20 steps from 0s to 2s.
  * Solve the problem with :red:`STAT_NON_LINE` with :blue:`COMPORTEMENT/RELATION = 'ELAS'`, the 
    list of times defined previously in :blue:`INCREMENT`, the material in :blue:`CHAM_MATER, MODEL` 
    and also the boundary conditions and loads (:blue:`CHARGE + FONC_MULT`) in :blue:`EXCIT`.

    .. admonition:: Question

       What do you observe about convergence? What would it take to converge?
       Hint: how is the default convergence criterion constructed?

    .. admonition:: Question

        How many rigid body motions are there in axisymmetric modeling? 
        Reminder: a rigid body motion is a deformation with zero strain.

* Next let us **add contact** definitions: orient the surfaces, define the contact load with the
  command :red:`DEFI_CONTACT` and modify the options of the nonlinear solver.

  .. note::

     An alarm appears during the calculation with contact. We will first use the default contact method
     (:blue:`FORMULATION = 'DISCRETE'` and :blue:`'ALGO_CONT ='CONTRAINTE'`).

  * Reorient the normals of the mesh boundary at the contact surfaces: 
    :red:`MODI_MAILLAGE / ORIE_PEAU_2D` using the groups :blue:`bot_surf` and :blue:`top_surf`. 
    Retain the original mesh by using :blue:`reuse`. 
  * Define the contact: :red:`DEFI_CONTACT`. We will use the default contact method initially 
    (:blue:`FORMULATION = 'DISCRETE'` and :blue:`'ALGO_CONT ='CONTRAINTE'`).
    The groups :blue:`bot_surf` and :blue:`top_surf` are assigned to the pair of contact zones 
    (master and slave).
  * Modify the contact option in :blue:`STAT_NON_LINE`.

Hertz pressure
^^^^^^^^^^^^^^

.. admonition:: Question

   Calculate the Hertz pressure and contact width by hand (see :numref:`hertz_ref_sol`). Compare them 
   to the values obtained from the simulation. What do we see? Why is there a difference?
   Note: the contact pressure can be approximated by the component :blue:`SIYY` of the field 
   :blue:`SIEF_NOEU` (calculated with the command :red:`CALC_CHAMP / CONTRAINTE`).

.. figure:: v6.04.222/forma04a_29.svg
   :height: 300 px
   :align: right

Let us trace the contact pressure as a function of the radius in the contact zone and compare it to the 
analytical solution.

* Calculate the element stress field extrapolated to the nodes (:blue:`SIEF_NOEU`) using
  :red:`CALC_CHAMP / CONTRAINTE`.
* Create a group of nodes with an oriented curvilinear abscissa with :red:`DEFI_GROUP`.  Reuse the
  mesh with :blue:`reuse`. To create the node group, Use the option 
  :red:`CREA_GROUP_NO` / :blue:`OPTION = 'NOEUD_ORDO'` with the mesh group :blue:`bot_surf` and 
  the starting point (:math:`C_1`, :blue:`bot_contact`).
* Extract component :blue:`SIYY` of the field :blue:`SIEF_NOEU` at the last time point into a table
  with the command :red:`POST_RELEVE_T`.  You will have to specify the name of the node group, the 
  time instant, the field and its component for the :blue:`EXTRACTION` operation.
* Create the function :math:`\sigma_{yy} = f(x)` with :red:`RECU_FONCTION`. The x-coordinate 
  :blue:`PARA_X` will be the abscissa (:blue:`ABSC_CURV`) and the y-coordinate :blue:`PARA_Y`
  will be :blue:`SIYY`.
* Compute and store the analytical solution:

  *  Calculate the pressure at points :math:`C_1` and :math:`C_2` (:math:`P_0`) and the half-width 
     of contact :math:`a`
  *  Create the analytical formula with :red:`FORMULE` which depends on the curvilinear coordinate
     :math:`x` (:blue:`NOM_PARA = 'x' / VALE = 'P0 * sqrt (1.-x * x / a / a)'`).
  *  Create a list :math:`s(x)` of real values with :red:`DEFI_LIST_REEL`.  We can use 100 values from 
     0 to 10mm.
  *  Interpolate the formula from the list of :math:`x` with :red:`CALC_FONC_INTERP`.
  *  Print the two functions with format :blue:`TABLEAU` with :red:`IMPR_FONCTION`.

Von Mises stress
^^^^^^^^^^^^^^^^

.. figure:: v6.04.222/sig_eq_04a.png
   :height: 250 px
   :align: right

Display the von Mises equivalent stress on the deformed configuration with ``Salomé`` by selecting
the component :blue:`VMIS_SG` of the field of the equivalent stresses :blue:`SIEQ_NOEU` that has
been calculated with the command :red:`CALC_CHAMP / CRITERES`.

.. admonition:: Question

   How does this stress vary in the sphere? Where is the maximum? 

We can print the results in MED format using command :red:`IMPR_RESU` in order to visualize the stress 
field in :blue:`Results` or in :blue:`Paravis`.  We can also find the location of the maximum
with :red:`Edit → Find Data`.

Python command file
^^^^^^^^^^^^^^^^^^^

The annotated Python command file for Model A is :download:`forma04a.comm <v6.04.222/forma04a.comm>`.
The contents of the file are listed below for convenience.

.. include::  v6.04.222/forma04a.comm
     :literal:


Verification tests
------------------

The verification tests use an analytical  reference value of :math:`\sigma_{yy}` = -2798.3 MPa at point
:math:`C_1`.  The value computed by the current model is -2773.55 MPa.


**Model B**: Using ``CONTINUOUS`` contact with Lagrange multipliers
===================================================================

The geometry and the mesh in this example are the same one those of Model A but quadratic elements
are used.  The Python script for creating the model is in 
:download:`forma04b.py <v6.04.222/forma04b.py>`.  The command file also contains a few modifications
and can be found in :download:`forma04b.comm <v6.04.222/forma04b.comm>`.

Creating and running the model
------------------------------

Continuous formulation
^^^^^^^^^^^^^^^^^^^^^^^

.. image:: v6.04.222/forma04b_29.svg
   :height: 300 px
   :align: right

* Convert the mesh of Model A into a quadratic mesh using the :blue:`Mesh` module 
  (:red:`Modification → Convert to / from quadratic`).

  .. important::

      In order to take full advantage of the quadratic elements and to obtain a smooth pressure profile, it
      is important to place the midpoints of element edges on the geometry during the conversion (check 
      the :blue:`Medium nodes on geometry` box when converting).

* Change the contact method to use the formulation :blue:`CONTINUE`. 

* Observe the contact pressure using the degree of freedom :blue:`LAGS_C` of the field :blue:`DEPL`.

.. admonition:: Question

   Compare with the default contact method in Model A. What do we see? Which of the methods provides 
   the best approximation?

.. note::

   To obtain a more accurate pressure profile, it is necessary to integrate of the contact stress terms 
   at Gauss points (:blue:`INTEGRATION = 'GAUSS'` in the command :red:`DEFI_CONTACT / ZONE`).

   In order to avoid the alarm that appears with the default contact method, it is necessary to 
   impose :blue:`REAC_ITER = 1` under the keyword factor :blue:`NEWTON` of the command 
   :red:`STAT_NON_LINE` (in the continuous formulation, the tangent matrix is necessarily 
   reconstructed with each iteration).


.. raw:: html

    <video width="320" height="240" controls>
      <source src="../../_static/v6.04.222/sig_yy_04b.ogv" type="video/ogg">
      Your browser does not support the video tag.
    </video>


Verification tests
------------------

The contact pressure at :math:`C_0` can be extracted from the :blue:`LAGS_C` component 
of :blue:`DEPL`.  The analytical value is -2798.3 MPa and the simulation produces -2876.29 MPa.

**Model C**: Using quadrilateral elements with ``DISCRETE`` contact
===================================================================

In this model we mesh the geometry with quadrilaterals that are refined close to the contact area.

Creating and running the model
------------------------------

Geometry
^^^^^^^^

.. _fig_6.04.222_3:

.. figure:: v6.04.222/fig_6.04.222_3.png
   :height: 250 px
   :align: right

   Partition of the upper disk

To mesh the geometry with quadrilaterals we must partition the geometry.

* Take the :blue:`top_sphere` face and get all its points (:red:`New Entity → Explode:Vertex`), to get 
  the 4 points :math:`A_2, B_2, C_2` and :math:`D 2`

* Create points :math:`M, N` and :math:`P` in order to generate the score lines in red 
  (:red:`New Entity → Basic → Point`): :blue:`(0, 25, 0)`, :blue:`(25, 25, 0)` and 
  :blue:`(25, 50, 0)`

* Generate the score lines (:red:`New Entity → Basic → Line`)

* Partition :blue:`top_sphere` with the these three lines (:red:`Operations → Partition`).

* Generate a new lower face :blue:`bot_sphere` using reflection 
  (:red:`Operations → Transformation → Mirror Image`) after choosing the X axis as the axis of symmetry

* Assemble the two new spheres to constitute a single :blue:`GEOM` object.
  (:red:`New Entity → Build → Compound`).

* Define all the geometry groups as in Model A.  These are needed to assign the boundary conditions 
  and loads (:red:`New Entity → Group → Create Group`).

Meshing
^^^^^^^

.. figure:: v6.04.222/fig_6.04.222_4.png
   :height: 250 px
   :align: right

   Quadrilateral mesh for Model C

We will refine the mesh around the contact zone by using a geometric progression on the circumference 
and on the radius. It may be necessary to reorient some segments in the :blue:`Mesh` module:

* Initialize the mesh with :red:`Mesh → Create Mesh`. To create a quadrilateral mesh we will use the 
  automatic meshing assumption :blue:`Assign a set of hypotheses → 2D: Automatic Quadrangulation` 
  and set the number of segments to 15. The mesh is calculated using :red:`Mesh → Compute`.

* Define sub-meshes (:red:`Mesh → Creat Sub-mesh`) with the groups :blue:`bot_surf` and 
  :blue:`top_surf`.  The sub-meshes use the :blue:`Wire Discretization` algorithm, with 
  a :blue:`Start and End Length` hypothesis (e.g. 0.2mm and 2.0mm) and the 
  :blue:`Advanced hypothesis` option :blue:`Propagation of Node Distribution on Opposite Edges`. We 
  then obtain a linear mesh containing about 3500 quadrilaterals and 4000 nodes.

* If you wish, you can convert the mesh from linear to 
  quadratic: :red:`Modification -> Convert to / from quadratic`.

* Import the geometry groups (:red:`Mesh → Create Groups from Geometry`)

Python Scripting
^^^^^^^^^^^^^^^^

The Python script for creating the geometry and the mesh can be downloaded from 
:download:`forma04c.py <v6.04.222/forma04c.py>` and is listed below for convenience.  Examine
the API methods used to select various parts of the geometry.  The command file used for the simulation 
can be be downloaded from :download:`forma04c.comm <v6.04.222/forma04c.comm>`.

.. include::  v6.04.222/forma04c.py
     :literal:

Simulations
^^^^^^^^^^^

The simulations can be performed in the same way as previous tutorials.  A movie showing the
evolution of the von Mises stress in Model C  is given below.

.. raw:: html

    <video width="320" height="240" controls>
      <source src="../../_static/v6.04.222/sig_yy_04c.ogv" type="video/ogg">
      Your browser does not support the video tag.
    </video>

Verification tests
------------------

The stress :math:`\sigma_{yy}` at point :math:`C_1` is computed to be -2928.8 MPa compared
to the analytical value of -2798.3 MPa.


