# FORMA41A: Reinforced beam in three-point bending (mechanical only)
DEBUT(LANG = 'EN')

# Read mesh
mesh = LIRE_MAILLAGE(FORMAT = 'MED')

# Create node groups from element groups
mesh = DEFI_GROUP(reuse = mesh,
                  MAILLAGE = mesh,
                  CREA_GROUP_NO = (_F(GROUP_MA = 'support_edge'),
                                   _F(OPTION = 'NOEUD_ORDO',
                                      NOM = 'load_edge',
                                      GROUP_MA = 'load_edge'),
                                   _F(OPTION = 'NOEUD_ORDO',
                                      NOM = 'section_edge1',
                                      GROUP_MA = 'section_edge1'),
                                   _F(OPTION = 'NOEUD_ORDO',
                                      NOM = 'section_edge2',
                                      GROUP_MA = 'section_edge2'),
                                   _F(OPTION = 'NOEUD_ORDO',
                                      NOM = 'long_edge_top',
                                      GROUP_MA = 'long_edge_top'),
                                   _F(OPTION = 'NOEUD_ORDO',
                                      NOM = 'long_edge_bot',
                                      GROUP_MA = 'long_edge_bot'),
                                   _F(GROUP_MA = 'symm_x_face'),
                                   _F(GROUP_MA = 'symm_z_face')))

# Assign elements
model = AFFE_MODELE(MAILLAGE = mesh,
                    AFFE =(_F(GROUP_MA = ('beam','symm_z_face','symm_x_face','load_edge'),
                              PHENOMENE = 'MECANIQUE',
                              MODELISATION='3D'),
                           _F(GROUP_MA = ('rebar32','rebar8'),
                              PHENOMENE = 'MECANIQUE',
                              MODELISATION = 'BARRE')))

# Define and assign section parameters
section = AFFE_CARA_ELEM(MODELE = model,
                         BARRE = (_F(GROUP_MA = 'rebar32',
                                     SECTION = 'CERCLE',
                                     CARA = 'R',
                                     VALE = 1.60000E-2),
                                  _F(GROUP_MA = 'rebar8',
                                     SECTION = 'CERCLE',
                                     CARA = 'R',
                                     VALE = 0.40000E-2)))

# Define materials
concrete = DEFI_MATERIAU(ELAS = _F(E = 32000.0E6,
                                   NU = 0.2))

steel = DEFI_MATERIAU(ELAS = _F(E = 200000.E+6,
                                NU = 0.0))

# Assign materials
mater = AFFE_MATERIAU(MAILLAGE = mesh,
                      AFFE = (_F(GROUP_MA = 'beam',
                                 MATER = concrete),
                              _F(GROUP_MA = ('rebar32','rebar8'),
                                 MATER = steel)))

# Finish
FIN();
