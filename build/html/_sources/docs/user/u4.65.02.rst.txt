.. role:: red
.. role:: blue
.. role:: green
.. role:: gray
.. role:: python(code)
   :language: python
.. |br| raw:: html

   <br/>

.. index:: ! DEFI_MODELE_GENE

.. _u4.65.02:

************************************************************************
**[U4.65.02]** : Operator ``DEFI_MODELE_GENE``
************************************************************************

.. _u4.65.02.1:

Purpose
=======

To create the total structure from the substructures in dynamic substructuring (cf :ref:`[R4.06.02] <r4.06.02>`).

Within the framework of a calculation using the methods of dynamic substructuring (modal or harmonic analysis),
the operator :red:`DEFI_MODELE_GENE` allows us to describe the total structure starting from macro-elements
resulting from :red:`MACR_ELEM_DYNA` :ref:`[U4.65.01] <u4.65.01>` and the various interfaces which bind the
substructures to each other. A macro-element can be used to define several substructures, whatever their
orientation in the physical coordinate system if the coupling is carried out statically
(option :blue:`'CLASSIQUE'`). This possibility allows repetition of a component to be taken into account
in the overall structure.

The output of this operator is a data structure of the :green:`modele_gene` type.

.. _u4.65.02.2:

Syntax
======

::

  mo_gene [modele_gene] = DEFI_MODELE_GENE
   (
    ♦ SOUS_STRUC = _F(
                       ♦ NOM = name_substruc,                 [Kn]
                       ♦ MACR_ELEM_DYNA = macro_dy,           [macr_elem_dyna]
                       ◊ ANGL_NAUT = angl_naut,               [l_R]
                       ◊ TRANS = trans,                       [l_R]
                     ),

    ♦ LIAISON = _F(
                    ♦ SOUS_STRUC_1 = 'name_substruc1',        [Kn]
                    ♦ INTERFACE_1 = 'name_int1',              [Kn]
                    ♦ SOUS_STRUC_2 = 'nam_substruc2',         [Kn]
                    ♦ INTERFACE_2 = 'name_int2',              [Kn]
                    ◊ GROUP_MA_MAIT_1 = el_grp1,              [l_gr_maille]
                    ◊ GROUP_MA_MAIT_2 = el_grp2,              [l_gr_maille]
                    ◊ OPTION = / 'CLASSIQUE',                 [DEFAULT]
                               / 'REDUIT'
                  ),

    ◊ VERIF = _F(
                  ◊ STOP_ERREUR = / 'OUI',                    [DEFAULT]
                                  / 'NON',
                  ◊ PRECISION = / prec,                       [R]
                                / 1.E-3,                      [DEFAULT]
                  ◊ CRITERE = / 'RELATIF',                    [DEFAULT]
                              / 'ABSOLU'
                ),

    ◊ INFO = / 1,                                             [DEFAULT]
             / 2
   )
  
   
.. _u4.65.02.3:

Operands
========


.. _u4.65.02.3.1:

Key word :blue:`SOUS_STRUC`
----------------------------

::

   ♦ SOUS_STRUC

Keyword factor for defining all the substructures which compose the overall structure. The definition of a
substructure is using its name, of the macro-element associated with it, and its orientation in the physical
reference coordinate system.

.. _u4.65.02.3.1.1:

Operand :blue:`NOM`
^^^^^^^^^^^^^^^^^^^

::

   ♦ NOM = 'struc_name'

Name containing 8 characters maximum which will subsequently be used to designate the substructure in :

* operator :red:`DEFI_MODELE_GENE` :ref:`[U4.65.02] <u4.65.02>` operands: :blue:`LIAISON` and
  :blue:`SOUS_STRUC_1`,
* operator :red:`DEFI_SQUELETTE` :ref:`[U4.24.01] <u4.24.01>`, operand: :blue:`SOUS_STRUC`,
* operator: :red:`ASSE_VECT_GENE` :ref:`[U4.65.05] <u4.65.05>`, operand: :blue:`SOUS_STRUC`,
* operator: :red:`REST_SOUS_STRUC` :ref:`[U4.63.32] <u4.63.32>`, operand: :blue:`SOUS_STRUC`.

.. _u4.65.02.3.1.2:

Operand :blue:`MACR_ELEM_DYNA`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

   ♦ MACR_ELEM_DYNA = macro_dyna

Name of the concept :green:`macr_elem_dyna` resulting from the operator :red:`MACR_ELEM_DYNA`
:ref:`[U4.65.01] <u4.65.01>` which indicates the condensed model of the substructure. It is recalled that a
macro-element can serve for the definition of several substructures.

.. _u4.65.02.3.1.3:

Operand :blue:`ANGL_NAUT`
^^^^^^^^^^^^^^^^^^^^^^^^^

::

   ◊ANGL_NAUT = angl_naut

List of the 3 nautical angles, in degrees, which switch from the orientation of the  macro-element model
to that of the substructure. Refer to the operator :red:`AFFE_CARA_ELEM` :ref:`[U4.42.01] <u4.42.01>`: Operand
:blue:`ORIENTATION` for the definition and use of nautical angles.

.. _u4.65.02.3.1.4:

Operand :blue:`TRANS`
^^^^^^^^^^^^^^^^^^^^^

::

   ◊TRANS = trans

List of 3 translation components to build a new substructure from the macro-element model by applying an overall
translation.

.. _u4.65.02.3.2:

Keyword :blue:`LIAISON`:
^^^^^^^^^^^^^^^^^^^^^^^^^

::
   
    ♦ LIAISON = _F(
                    ♦ SOUS_STRUC_1 = 'name_substruc1',        [Kn]
                    ♦ INTERFACE_1 = 'name_int1',              [Kn]
                    ♦ SOUS_STRUC_2 = 'nam_substruc2',         [Kn]
                    ♦ INTERFACE_2 = 'name_int2',              [Kn]
                    ◊ GROUP_MA_MAIT_1 = el_grp1,              [l_gr_maille]
                    ◊ GROUP_MA_MAIT_2 = el_grp2,              [l_gr_maille]
                    ◊ OPTION = / 'CLASSIQUE',                 [DEFAULT]
                               / 'REDUIT'
                  ),

Keyword factor for defining all the interfaces of coupling between substructures. A coupling is defined by the
names of two adjacent substructures and, for each, the name of the corresponding interface.

In case of an
incompatibility of elements between the two substructures, it is necessary to indicate which interface will
be considered as master (keywords :blue:`GROUP_MA_MAIT_*`). The slave nodes which are projected on the master
interface are defined by :red:`DEFI_INTERF_DYNA` :ref:`[U4.64.01] <u4.64.01>`. The "reattachment" of the 2
interfaces will be done by linear relations between the degrees of freedom of the 2 faces.

.. note::

   It is recommended that in the case of incompatible interfaces, the master interface be the one whose
   discretization is the coarsest. In the case of using classical static modes (all degrees of freedom
   considered), it is advisable to use the interface with the smallest number of degrees of freedom as
   the master interface. In the case of the use of coupling modes, this choice can be more delicate. The choice
   of the master interface can significantly impact the quality of the result if the two models present
   very different discretizations. See the discussion on this in :numref:`u4.65.02.5.2`.

Displacements of the nodes of the slave face will be connected to the displacements of their projections on
the master side. For each node of the slave face, one will write 2 (in 2D) or 3 (in 3D) linear relations.

An application of this functionality is the reattachment of a mesh of linear elements (P1) on another quadratic
mesh (P2). In this case it is advised to choose the quadratic face as "slave".

It is possible to define a interface by reduced modes (or interface modes) by the keyword :blue:`OPTION`.

.. _u4.65.02.3.2.1:

Operand :blue:`SOUS_STRUC_1`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

   ♦ SOUS_STRUC_1 = 'name_sstruc1'

Name of the first of the substructures brought into play on both sides of the interface. It must have been
defined previously with the keyword: :blue:`SOUS_STRUC`.

.. _u4.65.02.3.2.2:

Operand :blue:`INTERFACE_1`
^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

   ♦ INTERFACE_1 = 'int_name1'

Name of the interface of the first substructure in the connection. It must have been defined previously by
the operator :red:`DEFI_INTERF_DYNA` :ref:`[U4.64.01] <u4.64.01>` for the macro-support element of the
substructure.

.. note::

   In the case of the use of couplings modes (operator :red:`MODE_STATIQUE`) with the keyword
   :red:`MODE_INTERF`, it is essential that the dynamic interface is of type :blue:`CRAIGB`.

.. _u4.65.02.3.2.3:

Operand :blue:`GROUP_MA_MAIT_1`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

   ◊ GROUP_MA_MAIT_1 = lgma1

This keyword makes it possible to designate the master substructure, independently of the group of
elements specified in the input. The operator :red:`DEFI_MODELE_GENE` supports the search for adjacent elements in
all the cases, by using the definition of the interfaces (operator :red:`DEFI_INTERF_DYNA` -
:ref:`U4.64.01 <u4.64.01>`). If the interface is incompatible, and the keyword is not indicated, it is
substructure 1 which is defined as master.

.. _u4.65.02.3.2.4:

Operand :blue:`SOUS_STRUC_2`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

   ♦ SOUS_STRUC_2 = 'name_sstruc2'

Name of the second of the substructures brought into play on either side of the connection. It must have been
previously defined with the keyword :blue:`SOUS_STRUC`.

.. _u4.65.02.3.2.5:

Operand :blue:`INTERFACE_2`
^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

   ♦ INTERFACE_2 = 'int_name2'

Name of the interface of the second substructure in the connection. It must have been defined previously by the
operator :red:`DEFI_INTERF_DYNA` :ref:`[U4.64.01] <u4.64.01>` for the support macro-element of the substructure.

.. note::

   In the case of the use of coupling modes (operator :red:`MODE_STATIQUE`) with the keyword :blue:`MODE_INTERF`,
   it is essential that the dynamic interface is of type :blue:`CRAIGB`.

.. _u4.65.02.3.2.6:

Operand :blue:`GROUP_MA_MAIT_2`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

   ◊ GROUP_MA_MAIT_2 = lgma2

This keyword makes it possible to designate the master substructure, independently of the group of elements
specified as input. The operator :red:`DEFI_MODELE_GENE` supports the search of adjacent elements in all the
cases, by using the definition of the interfaces (see operator :red:`DEFI_INTERF_DYNA` -
:ref:`U4.64.01 <u4.64.01>`). If the interface is incompatible, and the keyword is not indicated, it is
substructure 1 which is defined as master.

.. _u4.65.02.3.2.7:

Operand :blue:`OPTION`
^^^^^^^^^^^^^^^^^^^^^^

::

   ◊ OPTION = / 'CLASSIQUE',
              / 'REDUIT'

Allows to choose between a conventional substructuring by static modes (method of Mac-Neal, Craig-Bampton
harmonic or not) or by interface modes.

.. _u4.65.02.3.3:

Keyword :blue:`VERIF`
---------------------

::

   ◊ VERIF

Key word factor for checking the coherence of the generalized model: it is checked that the connection is
compatible with the orientations and the translations assigned to the substructures. The nodes of the two
interfaces do not a priori have to be ordered in such a way that they are perfectly aligned in number.
If the nodes of the interfaces are not perfectly aligned, the code detects this state and reorders the
nodes so as to put them back in alignment.

.. _u4.65.02.3.3.1:

Operand :blue:`STOP_ERREUR`
^^^^^^^^^^^^^^^^^^^^^^^^^^^

Carry out (or not) the consistency check of the generalized model.

.. _u4.65.02.3.3.2:

Operands :blue:`PRECISION / CRITERE`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Indicates the precision threshold beyond which the connections are incompatible. This is the distance
(relative or absolute according to :blue:`CRITERE`) beyond which the connection nodes are considered
as too far apart to be effectively connected.

.. _u4.65.02.3.4:

Keyword :blue:`INFO`
--------------------

Keyword used to specify the output message level.

.. _u4.65.02.4:

Execution phase
================

The operator carries out a certain number of checks on the consistency of the connections if the connection
does not present any mesh incompatibility:

* identical number of nodes on either side of the connection,
* coherence, at each node, after orientation of the active degrees of freedom on both sides of the connection

.. _u4.65.02.5:

Matrices and connection conditions calculated by :red:`DEFI_MODELE_GENE`
========================================================================

.. _u4.65.02.5.1:

The :blue:`'CLASSIQUE'` option
------------------------------

The operator calculates the oriented connection matrices appearing in the generalized model:

.. math::

   (\mathbf{L}_j^k)_{\text{oriented}} = \mathbf{B}_j^k \mathbf{R}^k \boldsymbol{\Phi}^k

where

* the superscript :math:`k` identifies the substructure
* the subscript :math:`j` indicates the connection interface
* :math:`\mathbf{B}_j^k` is the matrix of extraction of the degrees of freedom of the connection :math:`j`
* :math:`\mathbf{R}^k` is the matrix of rotation which makes it possible to pass from the orientation of the
  model that generated the macro-element to that of the substructure
* :math:`\boldsymbol{\Phi}^k` is the column matrix of the eigenvectors of the substructure :math:`k`.

The interface conditions between substructures 1 and 2 are written:

.. math::

   (\mathbf{q}_j^1)_{\text{oriented}} = (\mathbf{q}_j^2)_{\text{oriented}}
   \quad \text{with} \quad
   (\mathbf{q}_j^k)_{\text{oriented}} = (\mathbf{L}_j^k)_{\text{oriented}} \boldsymbol{\eta}^k

where

* :math:`\mathbf{q}_j^k` is the column vector of the physical coordinates of the connection :math:`j`
  of the substructure :math:`k`
* :math:`\boldsymbol{\eta}^k` is the vector column of the generalized coordinates of the substructure
  :math:`k`.

.. _u4.65.02.5.2:

The :blue:`'REDUIT'` option
---------------------------

The operator calculates the oriented coupling matrices in the generalized model:

.. math::

   (\mathbf{L}_j^k)_{\text{oriented}} = \mathbf{B}_j^k \mathbf{R}^k \boldsymbol{\Phi}^k

where

* the exponent :math:`k` indicates the substructure,
* the index :math:`j` indicates the connection interface,
* :math:`\mathbf{B}_j^k` is the matrix of extraction of the degrees of freedom of the connection :math:`j`,
* :math:`\mathbf{R}^k` is the matrix of rotation which makes it possible to pass from the orientation of the
  macro-element model to that of the substructure,
* :math:`\boldsymbol{\Phi}^k` is the column matrix of the eigenvectors of the substructure :math:`k`.

In the case of the option :blue:`REDUIT` :gray:`("REDUCED")`, one glues together the generalized displacements
of the two interfaces. We ensures that

.. math::

   (\boldsymbol{\Phi}_{\text{slave}})^T (\mathbf{q}_j^1)_{\text{oriented}} =
   (\boldsymbol{\Phi}_{\text{slave}})^T (\mathbf{q}_j^2)_{\text{oriented}},
   \quad \text{with} \quad
   (\mathbf{q}_j^k)_{\text{oriented}} = (\mathbf{L}_j^k)_{\text{oriented}} \boldsymbol{\eta}^k

Indeed, in the case of the option :blue:`"CLASSIQUE"`, the choice of the master and slave degrees of freedom
is equivalent to writing the coupling equation in the form

.. math::

   y_{\text{slave}} - C y_{\text{master}} = 0

where :math:`y` corresponds to the degrees of freedom. However, the use of a space of reduced dimension
for writing the relation amounts to imposing the following constraints:

.. math::
   
   \boldsymbol{\Phi}_{\text{slave}} \mathbf{q}_{\text{slave}} -
   C \boldsymbol{\Phi}_{\text{master}} \mathbf{q}_{\text{master}} = 0

We have, therefore, more equations than unknowns, the number of generalized degrees of freedom
:math:`\mathbf{q}_{\text{slave}}` being clearly lower than the number of physical degrees of freedom of the
interface :math:`y_{\text{slave}}`.  To ensure good conditioning of the coupling conditions, one thus
projects this relation on the restriction with the interface of the modes of the slave substructure, and the
relation to check then becomes:

.. math::

   \boldsymbol{\Phi}_{\text{slave}}^T \boldsymbol{\Phi}_{\text{slave}} \mathbf{q}_{\text{slave}} -
   \boldsymbol{\Phi}_{\text{slave}}^T C \boldsymbol{\Phi}_{\text{master}} \mathbf{q}_{\text{master}} = 0

The choice of master substructure master thus conditions the quality of the gluing in strongly incompatible
interfaces.

.. note::

   When using the option, special attention should be paid to the choice of the master and slave interfaces.
   This is because though the kinematic relation between the interfaces is written using the physical degrees
   of freedom, that relation is then projected on the modes of the slave interface. If the two models are
   identical (3D - 3D, or 2D - 2D), and the discretizations are equivalent, then the master interface will be
   the interface with the greatest number of modes. In the case of different models, it will be necessary to
   ensure that one has an equivalent number of interface modes for each of the substructures, ideally a little
   less for the slave substructure, and choose as the master interface the one presenting the coarsest
   discretization. In any case, while future the tools to assess the quality of the
   reattachment are being developed, it will be necessary to visually verify the results.
