.. Code-Aster documentation master file, created by
   sphinx-quickstart on Thu Feb 18 18:11:25 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Code-Aster v.14 documentation!
=========================================

.. toctree::
   :maxdepth: 1
   :numbered:
   :caption: Contents:

   docs/validation/training.rst

   docs/user/u0.00.01.rst
   docs/user/u1_toc.rst
   docs/user/u2_toc.rst
   docs/user/u3_toc.rst
   docs/user/u4_toc.rst
   docs/user/u7_toc.rst

   docs/reference/r_toc.rst
   docs/reference/r0.00.01.rst
   docs/reference/r0_toc.rst
   docs/reference/r3_toc.rst
   docs/reference/r4_toc.rst
   docs/reference/r5_toc.rst
   docs/reference/r6_toc.rst
   docs/reference/r7_toc.rst

   docs/validation/v_toc.rst
   docs/validation/v0.00.00.rst
   docs/validation/v0_toc.rst
   docs/validation/v1_toc.rst
   docs/validation/v2_toc.rst
   docs/validation/v3_toc.rst
   docs/validation/v4_toc.rst
   docs/validation/v5_toc.rst
   docs/validation/v6_toc.rst
   docs/validation/v7_toc.rst
   docs/validation/v8_toc.rst
   docs/validation/v9_toc.rst

Index
=====

:ref:`genindex`

Search
======

:ref:`search`
