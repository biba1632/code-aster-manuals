import salome
import GEOM
import math
from salome.geom import geomBuilder

import SMESH, SALOMEDS
from salome.smesh import smeshBuilder

ExportPATH="/home/banerjee/Salome/forma05/"

# Start salome
salome.salome_init()

#--------------------------------------------
# Create geometry
#--------------------------------------------
geompy = geomBuilder.New()

# Create coordinate system
O = geompy.MakeVertex(0, 0, 0)
OX = geompy.MakeVectorDXDYDZ(1, 0, 0)
OY = geompy.MakeVectorDXDYDZ(0, 1, 0)
OZ = geompy.MakeVectorDXDYDZ(0, 0, 1)

# Geometry parameters
crack_length = 0.1
height = 2.0
width = 1.0

# Create sketch
sk = geompy.Sketcher2D()
sk.addPoint(0.000000, 0.000000)
sk.addSegmentAbsolute(0.000000, height/2)
sk.addSegmentAbsolute(width, height/2)
sk.addSegmentAbsolute(width, 0.000000)
sk.addSegmentAbsolute(crack_length, 0.000000)
sk.close()

# Add the boundary wire
coord_sys_2D = geompy.MakeMarker(0, 0, 0, 1, 0, 0, 0, 1, 0)
plate_boundary = sk.wire(coord_sys_2D)

# Create the plate face
plate = geompy.MakeFaceWires([plate_boundary], 1)

# Explode the geometry to extract geometry entities
[e1,e2,e3,e4,e5] = geompy.ExtractShapes(plate, geompy.ShapeType["EDGE"], True)
[v1,v2,v3,v4,v5] = geompy.ExtractShapes(plate, geompy.ShapeType["VERTEX"], True)

# Create geometry groups
g_O = geompy.CreateGroup(v3, geompy.ShapeType["VERTEX"])
g_E = geompy.CreateGroup(v4, geompy.ShapeType["VERTEX"])
g_OE = geompy.CreateGroup(e3, geompy.ShapeType["EDGE"])
g_GO = geompy.CreateGroup(e2, geompy.ShapeType["EDGE"])
g_CD = geompy.CreateGroup(e4, geompy.ShapeType["EDGE"])

geompy.UnionIDs(g_O, [1])
geompy.UnionIDs(g_E, [1])
geompy.UnionIDs(g_OE, [1])
geompy.UnionIDs(g_GO, [1])
geompy.UnionIDs(g_CD, [1])

# Add all geometric entities to the study
geompy.addToStudy( O, 'O' )
geompy.addToStudy( OX, 'OX' )
geompy.addToStudy( OY, 'OY' )
geompy.addToStudy( OZ, 'OZ' )
geompy.addToStudy( plate_boundary, 'plate_boundary' )
geompy.addToStudy( plate, 'plate' )
geompy.addToStudy( v3, 'v3_O' )
geompy.addToStudyInFather( v3, g_O, 'O' )
geompy.addToStudyInFather( v4, g_E, 'E' )
geompy.addToStudyInFather( e3, g_OE, 'OE' )
geompy.addToStudyInFather( e2, g_GO, 'GO' )
geompy.addToStudyInFather( e4, g_CD, 'CD' )

#--------------------------------------------
# Create Mesh
#--------------------------------------------
smesh = smeshBuilder.New()
mesh = smesh.Mesh(plate)

# Meshing parameters
max_size = 0.1
min_size = 0.005
local_size_at_O = 0.005

# Set up meshing algorithm
NETGEN_1D_2D = mesh.Triangle(algo = smeshBuilder.NETGEN_1D2D)
mesh_params = NETGEN_1D_2D.Parameters()
mesh_params.SetMaxSize( max_size )
mesh_params.SetMinSize( min_size )
mesh_params.SetSecondOrder( 1 )
mesh_params.SetOptimize( 1 )
mesh_params.SetFineness( 4 )
mesh_params.SetLocalSizeOnShape(v3, local_size_at_O)

# Compute the mesh
isDone = mesh.Compute()

# Move geometry groups to the mesh
O_1 = mesh.GroupOnGeom(g_O, 'O', SMESH.NODE)
E_1 = mesh.GroupOnGeom(g_E, 'E', SMESH.NODE)
GO_1 = mesh.GroupOnGeom(g_GO, 'GO', SMESH.EDGE)
OE_1 = mesh.GroupOnGeom(g_OE, 'OE', SMESH.EDGE)
CD_1 = mesh.GroupOnGeom(g_CD, 'CD', SMESH.EDGE)

# Set names of mesh objects
smesh.SetName(E_1, 'E')
smesh.SetName(O_1, 'O')
smesh.SetName(NETGEN_1D_2D.GetAlgorithm(), 'NETGEN 1D-2D')
smesh.SetName(mesh.GetMesh(), 'mesh')
smesh.SetName(GO_1, 'GO')
smesh.SetName(OE_1, 'OE')
smesh.SetName(CD_1, 'CD')
smesh.SetName(mesh_params, 'NETGEN 2D Parameters_1')

# Save mesh
mesh.ExportMED( r''+ExportPATH+'forma05a.mmed'+'')

# Update objecte tree in GUI
if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()
