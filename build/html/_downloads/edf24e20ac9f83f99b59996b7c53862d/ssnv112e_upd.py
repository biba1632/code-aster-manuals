#!/usr/bin/env python
# coding: utf-8

import gmsh
import sys
import math
import numpy as np

gmsh.initialize()

# Use OpenCASCADE kernel
model = gmsh.model
occ = model.occ
mesh = model.mesh
model.add("ssnv112e")

# Geometry
ri=0.1
re=0.2
h = 0.01
NR = 10
NT = 10

# Default element size
dens = 0.01;

# Points of construction
PO = occ.addPoint(0, 0, 0,  dens)

# Points of the plane of the base z=0
A = occ.addPoint(0, ri, 0., dens)
B = occ.addPoint(0, re, 0., dens)
E = occ.addPoint(-(ri/(2**0.5)) , (ri/(2**0.5)), 0.,  dens)
F = occ.addPoint(-re/(2**0.5) , re/(2**0.5) , 0.,  dens)

# Lines
LAB = occ.addLine(A, B)
LBF = occ.addCircleArc(B, PO, F)
LFE = occ.addLine(F, E)
LEA = occ.addCircleArc(E, PO, A)

# Surfaces
tri_loop = occ.addCurveLoop([LAB, LBF, LFE, LEA])
tri = occ.addPlaneSurface([tri_loop])

# Volume
vol = occ.extrude([(2, tri)], 0, 0, h, [2], recombine = True)

print(vol)

occ.synchronize()

# Physical groups for the points
A_g = model.addPhysicalGroup(0, [A])
model.setPhysicalName(0, A_g, 'A')
B_g = model.addPhysicalGroup(0, [B])
model.setPhysicalName(0, B_g, 'B')
E_g = model.addPhysicalGroup(0, [E])
model.setPhysicalName(0, E_g, 'E')
F_g = model.addPhysicalGroup(0, [F])
model.setPhysicalName(0, F_g, 'F')

# Physical groups for the edges
AB_g = model.addPhysicalGroup(1, [LAB])
model.setPhysicalName(1, AB_g, 'AB')
BF_g = model.addPhysicalGroup(1, [LBF])
model.setPhysicalName(1, BF_g, 'BF')
FE_g = model.addPhysicalGroup(1, [LFE])
model.setPhysicalName(1, FE_g, 'FE')
EA_g = model.addPhysicalGroup(1, [LEA])
model.setPhysicalName(1, EA_g, 'EA')

# Physical group for the faces
ABEF_g = model.addPhysicalGroup(2, [tri])
model.setPhysicalName(2, ABEF_g, 'ABEF')
# Physical groups for the faces
FACINF_g = model.addPhysicalGroup(2, [tri])
model.setPhysicalName(2, FACINF_g, 'FACINF')
FACSUP_g = model.addPhysicalGroup(2, [6])
model.setPhysicalName(2, FACSUP_g, 'FACSUP')
FACEAB_g = model.addPhysicalGroup(2, [2])
model.setPhysicalName(2, FACEAB_g, 'FACEAB')
FACEAE_g = model.addPhysicalGroup(2, [5])
model.setPhysicalName(2, FACEAE_g, 'FACEAE')
FACEEF_g = model.addPhysicalGroup(2, [4])
model.setPhysicalName(2, FACEEF_g, 'FACEEF')

# Physical group for the volume
VOLUME_g = model.addPhysicalGroup(3, [1])
model.setPhysicalName(3, VOLUME_g, 'VOLUME')

num_nodes = NT+1
for curve in [LBF, LEA]:
    #print(curve)
    mesh.setTransfiniteCurve(curve, num_nodes)
num_nodes = NR+1
for curve in [LAB, LFE]:
    #print(curve)
    mesh.setTransfiniteCurve(curve, num_nodes)

mesh.setTransfiniteSurface(tri)

#gmsh.option.setNumber('Mesh.Algorithm', 7)
gmsh.option.setNumber('Mesh.ElementOrder', 2)
gmsh.option.setNumber('Mesh.MshFileVersion', 2.2)
gmsh.option.setNumber('Mesh.MedFileMinorVersion', 0)
gmsh.option.setNumber('Mesh.SaveAll', 0)
gmsh.option.setNumber('Mesh.SaveGroupsOfNodes', 1)
gmsh.option.setNumber('Mesh.SecondOrderIncomplete', 1)
gmsh.option.setNumber('Mesh.AnisoMax', 0)

# Generate mesh
mesh.generate(3)

# Save mesh
gmsh.write("ssnv112e_upd.med")

gmsh.fltk.run()

gmsh.finalize()


