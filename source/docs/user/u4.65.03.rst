.. role:: red
.. role:: blue
.. role:: green
.. role:: gray
.. role:: python(code)
   :language: python
.. |br| raw:: html

   <br/>

.. index:: ! NUME_DDL_GENE

.. _u4.65.03:

************************************************************************
**[U4.65.03]** : Operator ``NUME_DDL_GENE``
************************************************************************

.. _u4.65.03.1:

Purpose
=======

To number the degrees of freedom of the total structure starting from the numbering of the substructures.

For a calculation using the methods of dynamic substructuring (modal or harmonic analysis), the operator
:red:`NUME_DDL_GENE` defines the bijection between:

* the number of generalized degrees of freedom of each substructure and the numbers of the interface degrees
  of freedom of each connection and,
* the numbers of the final degrees of freedom (i.e. the indices of row or column of generalized matrices).

After the numbering is carried out, the operator constructs tables for effective storage of the assembled
matrices depending on the "skyline" storage mode. Full or diagonal skyline storage can be used for
a full matrix or to solve a transient modal problem resulting from a concept of the "green:`mode_meca` type.

The output is a data structure of the :green:`nume_ddl_gene` type.


.. _u4.65.03.2:

Syntax
======

::

  nu_gene [nume_ddl_gene] = NUME_DDL_GENE
   (
    # If MODELE_GENE:

    | ◊ MODELE_GENE = mo_gene,                                [modele_gene]
      ◊ STOCKAGE = / 'LIGN_CIEL',                             [DEFAULT]
                   / 'PLEIN',
      ◊ METHODE = / 'CLASSIQUE',                              [DEFAULT]
                  / 'ELIMINE',
                  / 'INITIAL',

    # If BASE:

    | ◊ BASE = base,                                        / [mode_gene]
                                                            / [mode_meca]
      ◊ STOCKAGE = / 'PLEIN',                                 [DEFAULT]
                   / 'DIAG',
      ◊ NB_VECT = nbvect
   )
   
.. _u4.65.03.3:

Operands
========


.. _u4.65.03.3.1:

Operand :blue:`MODEL_GENE`
--------------------------

::

   ◊ MODELE_GENE = mo_gene

Name of the concept of the :green:`modele_gene` type produced by the operator :red:`DEFI_MODELE_GENE`
:ref:`[U4.65.02] <u4.65.02>` from which the numbering is carried out.

.. _u4.65.03.3.2:

Operand :blue:`METHODE`
-----------------------

::

   ◊ METHODE = / 'CLASSIQUE'

Builds a generalized numbering that takes into account the coupling equations by the method of double
Lagrange multipliers :ref:`[R4.06.02] <r4.06.02>`.

::

   ◊ METHODE = / 'ELIMINE'

Constructs a generalized numbering that takes into account coupling equations by elimination of the
constraints :ref:`[R4.06.02] <r4.06.02>`.

::

   ◊ METHODE = / 'INITIAL'

Initializes the numbering for generalized operators for the construction of a numbering scheme controllable
by Python. Matrices (built with :red:`ASSE_MATR_GENE`) and vectors (built with :red:`ASSE_VECT_GENE`) are
of suitable size, but initialized to zero.

.. _u4.65.03.3.3:

Storage of matrices
-------------------

::

   ◊ STOCKAGE =

Choice of a mode of storage of the matrices which one will assemble with this numbering. Three options are
available:

::

   / 'LIGN_CIEL'

The :gray:`SKY_LINE` option indicates block “Skyline” storage: the assembled matrix will be stored as
blocks of columns starting from the first term that is likely to be nonzero for each column.

In the assembled matrix, a term :math:`A(i,j)` is likely to be non-zero if and only if the degree of freedom
:math:`i` and the degree of freedom :math:`j` (the i-th and j-th modes of the total structure) result from
the same substructure or are connected by at least one Lagrange degree of freedom (for two substructures
connected at an interface).

::

   / 'PLEIN'

The :gray:`FULL` option also uses "skyline" storage of the assembled generalized generalized by blocks, but
with a full profile (we store all the elements of the upper triangular matrix). This type of storage must be used
if one wants to calculate full generalized matrices (mass, rigidity, damping), as well as all the generalized
matrices to which they are added.

For the stiffness and damping matrices, the assembly is always done starting from a :green:`nume_ddl_gene`
resulting from a :green:`mode_meca`.

::
   
   / 'DIAG'

In this "skyline" storage option, the diagonal terms of the matrices projected on the modal basis are stored.
This type of storage is to be used when the basis on which one reduces the problem is orthogonal compared to the
matrices used (the mass matrices, stiffness and damping). This storage improves the performance of operators
(:red:`DYNA_TRAN_MODAL` for example). However, this can lead to incorrect results if the orthogonality is not
satisfied, for example when a basis of modes made up of static modes is used. Indeed, the static modes are not
mutually orthogonal.

.. _u4.65.03.3.4:

Operand :blue:`BASE`
--------------------

This operand is used to identify the modal basis on which one projects the matrices.

.. _u4.65.03.3.5:

Operand :blue:`NB_VECT`
-----------------------

In this keyword, one expects the number of basis projection vectors, possibly smaller than the number of modes
defined in the basis specified by the keyword :blue:`BASE`. By default, this number is equal to the number of
modes of the basis.

.. _u4.65.03.4:

Execution phase
===============

No bandwidth optimization is performed during numbering. The order of appearance of the substructures in the
numbering scheme corresponds to the order of their definition in the generalized model (operator
:red:`DEFI_MODELE_GENE`). The user can therefore limit the bandwidth by defining the substructures of the
generalized model in a judicious order.

The degrees of freedom resulting from double dualisation are then assembled on both sides of the
generalized degrees of freedom of the second assembled substructure (among the two substructures brought
into play by a connection).
