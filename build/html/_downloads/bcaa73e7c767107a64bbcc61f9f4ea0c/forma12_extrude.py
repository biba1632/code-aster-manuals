#!/usr/bin/env python

import sys
import salome
from salome.geom import geomBuilder
from salome.smesh import smeshBuilder
import GEOM
import SMESH
import math

ExportPATH="/home/banerjee/Salome/forma12/"

salome.salome_init()
geompy = geomBuilder.New()
smesh = smeshBuilder.New()

# Geometry parameters
length = 2.0
width = 2.0
thickness = 0.03

# Mesh parameters
num_thru_thick = 2
num_along_edge = 20

#-------------
# Geometry
O = geompy.MakeVertex(0, 0, 0)
OX = geompy.MakeVectorDXDYDZ(1, 0, 0)
OY = geompy.MakeVectorDXDYDZ(0, 1, 0)
OZ = geompy.MakeVectorDXDYDZ(0, 0, 1)

# Make face on ZX-plane and rotate it into position
face_init = geompy.MakeFaceHW(length, thickness, 3)
face_rot = geompy.MakeRotation(face_init, OY, math.pi/2.0)
plate_face = geompy.MakeTranslation(face_rot, 1, 0, 0.015)
plate_id = geompy.addToStudy(plate_face, "plate_face")

# Create groups
pt_O = geompy.MakeVertex(0, 0, 0)
vert_O = geompy.GetVertexNearPoint(plate_face, pt_O)
geompy.addToStudyInFather(plate_face, vert_O, "point_O")
pt_m = geompy.MakeVertex(0, 0, thickness/2.0)
edge_L = geompy.GetEdgeNearPoint(plate_face, pt_m)
geompy.addToStudyInFather(plate_face, edge_L, "edge_L")
fixed_face = geompy.GetFaceNearPoint(plate_face, pt_m)
geompy.addToStudyInFather(plate_face, fixed_face, "fixed_face")


#-------------
# Mesh

# Build face mesh
face_mesh = smesh.Mesh(plate_face)

Regular_1D = smesh.CreateHypothesis('Regular_1D')
Number_of_Segments = smesh.CreateHypothesis('NumberOfSegments')
Number_of_Segments.SetNumberOfSegments( 20 )
Number_of_Layers = smesh.CreateHypothesis('NumberOfLayers2D')
Number_of_Layers.SetNumberOfLayers( 2 )
QuadFromMedialAxis_1D2D = smesh.CreateHypothesis('QuadFromMedialAxis_1D2D')

status = face_mesh.AddHypothesis(Regular_1D)
status = face_mesh.AddHypothesis(Number_of_Segments)
status = face_mesh.AddHypothesis(QuadFromMedialAxis_1D2D)
status = face_mesh.AddHypothesis(Number_of_Layers)

isDone = face_mesh.Compute()

# Extrude face mesh
extrude_depth = width / num_along_edge
face_mesh.ExtrusionSweepObjects( [ face_mesh ], [ face_mesh ], [ face_mesh ], [ 0, extrude_depth, 0 ], num_along_edge, 1, [  ], 0, [  ], [  ], 0 )

# Move geometry groups to mesh
g_fixed_face = face_mesh.GroupOnGeom(fixed_face, 'fixed_face', SMESH.FACE)
g_edge_L = face_mesh.GroupOnGeom(edge_L, 'edge_L', SMESH.EDGE)
g_pt_O = face_mesh.GroupOnGeom(vert_O, 'point_O', SMESH.NODE)

# Add group for whole volume
plate_vol = face_mesh.CreateEmptyGroup( SMESH.VOLUME, 'plate_vol' )
nbAdd = plate_vol.AddFrom( face_mesh.GetMesh() )

# Add group for point P
eps = 1.0e-4
pt_P = geompy.MakeVertex(length, width, thickness)
box_min = geompy.MakeVertex(length-eps, width-eps, thickness-eps)
box_max = geompy.MakeVertex(length+eps, width+eps, thickness+eps)
box_P = geompy.MakeBoxTwoPnt(box_min, box_max)
filter = smesh.GetFilter(SMESH.NODE,  SMESH.FT_BelongToGeom, box_P)
ids = face_mesh.GetIdsFromFilter(filter)
g_pt_P = face_mesh.CreateEmptyGroup( SMESH.NODE, 'point_P' )
nbAdd = g_pt_P.Add(ids )

# Convert to quadratic
face_mesh.ConvertToQuadratic(0)

# Export mesh
face_mesh.ExportMED( r''+ExportPATH+'forma12a_extrude.mmed'+'')

# Update GUI
if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()
