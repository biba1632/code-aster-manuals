# FORMA40A : 3D concrete plate model with grill membrane of reinforcement
DEBUT(LANG = 'EN')

# Read and enrich meshes
mesh_in = LIRE_MAILLAGE(FORMAT = 'MED')

mesh_ret = CREA_MAILLAGE(MAILLAGE = mesh_in,
                        CREA_MAILLE = _F(NOM = 'steel_top',
                                         GROUP_MA = 'grill_top',
                                         PREF_MAILLE = 'B'))

mesh_tot = CREA_MAILLAGE(MAILLAGE = mesh_ret,
                         CREA_MAILLE = _F(NOM = 'steel_bot',
                                          GROUP_MA = 'grill_bot',
                                          PREF_MAILLE = 'C'))

# Create node groups
mesh_tot = DEFI_GROUP(reuse = mesh_tot,
                      MAILLAGE = mesh_tot,
                      CREA_GROUP_NO = _F(TOUT_GROUP_MA = 'OUI'))

# Define the materials
steel = DEFI_MATERIAU(ELAS = _F(E = 200.0e9,
                                NU = 0.3,
                                RHO = 7800.0))

concrete = DEFI_MATERIAU(ELAS =_F(E = 30.0E9,
                                  NU = 0.0,
                                  RHO = 2500.0))

# Assign materials
mater = AFFE_MATERIAU(MAILLAGE = mesh_tot,
                      AFFE = (_F(GROUP_MA = ('steel_top','steel_bot'),
                                 MATER = steel),
                              _F(GROUP_MA = 'plate',
                                 MATER = concrete)))

# Assign finite element types
model = AFFE_MODELE(MAILLAGE = mesh_tot,
                    AFFE = (_F(GROUP_MA = ('steel_top','steel_bot'),
                               PHENOMENE = 'MECANIQUE',
                               MODELISATION = 'GRILLE_MEMBRANE'),
                            _F(GROUP_MA = 'plate',
                               PHENOMENE = 'MECANIQUE',
                               MODELISATION = '3D')))

# Assign grill-membrane properties
grill = AFFE_CARA_ELEM(MODELE = model,
                       GRILLE = (_F(GROUP_MA = 'steel_top',
                                    SECTION = 0.2,
                                    ANGL_REP_1 = (0, 0)),
                                 _F(GROUP_MA = 'steel_bot',
                                    SECTION = 0.2,
                                    ANGL_REP_1 = (0, 0))))

# Boundary conditions
fixed_bc = AFFE_CHAR_MECA(MODELE = model,
                          PESANTEUR = _F(GRAVITE = 9.81,
                                         DIRECTION = (0., 0., -1.)),
                          DDL_IMPO = _F(GROUP_MA = 'fixed_face',
                                        DX = 0.,
                                        DY = 0.,
                                        DZ = 0.))

vari_bc = AFFE_CHAR_MECA(MODELE = model,
                         DDL_IMPO = _F(GROUP_MA = 'disp_face',
                                       DZ = -0.1))

# Define load curve
load_cur = DEFI_FONCTION(NOM_PARA = 'INST',
                         VALE = (0., 0.,
                                 1., 0.,
                                 2., 1.))

# Define load step list
l_load = DEFI_LIST_REEL(DEBUT = 0.,
                        INTERVALLE = _F(JUSQU_A = 2.0,
                                        NOMBRE = 2))

# Compute solution
result = STAT_NON_LINE(MODELE = model,
                       CHAM_MATER = mater,
                       CARA_ELEM = grill,
                       EXCIT = (_F(CHARGE = fixed_bc),
                                _F(CHARGE = vari_bc,
                                   FONC_MULT = load_cur)),
                       COMPORTEMENT = _F(RELATION = 'ELAS'),
                       INCREMENT = _F(LIST_INST = l_load))

# Add extra variables for post-processing
result = CALC_CHAMP(reuse = result,
                    RESULTAT = result,
                    DEFORMATION = 'EPSI_ELNO',
                    CONTRAINTE = 'SIEF_ELNO',
                    FORCE = 'FORC_NODA')

# Save the results in MED format
IMPR_RESU(FORMAT = 'MED',
          RESU = _F(RESULTAT = result))

# Extract force-displacement curves
uz_table = POST_RELEVE_T(ACTION = _F(OPERATION = 'EXTRACTION',
                                     INTITULE = 'u_z',
                                     RESULTAT = result,
                                     NOM_CHAM = 'DEPL',
                                     TOUT_ORDRE = 'OUI',
                                     PRECISION = 1.E-06,
                                     GROUP_NO = 'disp_face',
                                     NOM_CMP = 'DZ'))

fz_table = POST_RELEVE_T(ACTION = _F(OPERATION = 'EXTRACTION',
                                     INTITULE = 'f_z',
                                     RESULTAT = result,
                                     NOM_CHAM = 'FORC_NODA',
                                     TOUT_ORDRE = 'OUI',
                                     GROUP_NO = 'disp_face',
                                     RESULTANTE = 'DZ'))

# Write force-displacement curve tables
IMPR_TABLE(TABLE = uz_table,
           SEPARATEUR = ",")

IMPR_TABLE(TABLE = fz_table,
           SEPARATEUR = ",")

# Extract data for comparisons
u_z = RECU_FONCTION(TABLE = uz_table,
                    PARA_X = 'INST',
                    PARA_Y = 'DZ',
                    FILTRE = _F(NOM_PARA = 'NOEUD',
                                CRIT_COMP = 'EQ',
                                VALE_K = 'N5'))

f_z = RECU_FONCTION(TABLE = fz_table,
                    PARA_X = 'INST',
                    PARA_Y = 'DZ');

# Write out the curves
IMPR_FONCTION(FORMAT = 'TABLEAU',
              SEPARATEUR = ",",
              UNITE = 38,
              COURBE = _F(FONC_X = u_z,
                          FONC_Y = f_z))

# Create a line segment and extract displacements at t = 2
disp_lin = MACR_LIGN_COUPE(RESULTAT = result,
                           NOM_CHAM = 'DEPL',
                           INST = 2.,
                           LIGN_COUPE = _F(TYPE = 'SEGMENT',
                                           NB_POINTS = 15,
                                           COOR_ORIG = (0.0, 2.5, 0.0),
                                           COOR_EXTR = (10.0, 2.5, 0.0)))

# Write out the line segement displacements
IMPR_TABLE(TABLE = disp_lin,
           FORMAT = 'TABLEAU',
           SEPARATEUR = ",")

IMPR_TABLE(TABLE = disp_lin,
           FORMAT = 'TABLEAU',
           SEPARATEUR = ",",
           UNITE = 29,
           FILTRE = _F(NOM_PARA = 'INST',
                       CRIT_COMP = 'EQ',
                       VALE = 2.0),
           NOM_PARA = ('COOR_X', 'DZ'))

# Extract max/min stresses 
sig_bots = POST_RELEVE_T(ACTION = _F(OPERATION = 'EXTREMA',
                                     INTITULE = 'steel_bot',
                                     RESULTAT = result,
                                     NOM_CHAM = 'SIEF_ELNO',
                                     GROUP_MA = 'steel_bot'))

sig_tops = POST_RELEVE_T(ACTION = _F(OPERATION = 'EXTREMA',
                                     INTITULE = 'steel_top',
                                     RESULTAT = result,
                                     NOM_CHAM = 'SIEF_ELNO',
                                     GROUP_MA = 'steel_top'))

sig_conc = POST_RELEVE_T(ACTION = _F(OPERATION = 'EXTREMA',
                                     INTITULE = 'concrete',
                                     RESULTAT = result,
                                     NOM_CHAM = 'SIEF_ELNO',
                                     GROUP_MA = 'plate'))

IMPR_TABLE(TABLE = sig_bots,
           SEPARATEUR = ",")
IMPR_TABLE(TABLE = sig_tops,
           SEPARATEUR = ",")
IMPR_TABLE(TABLE = sig_conc,
           SEPARATEUR = ",")

# Verification tests
TEST_TABLE(CRITERE = 'RELATIF',
           VALE_CALC = 3.1656932692218E5,
           NOM_PARA = 'DZ',
           TABLE = fz_table,
           FILTRE = _F(NOM_PARA = 'INST',
                       VALE = 2.0))

TEST_TABLE(CRITERE = 'RELATIF',
           VALE_CALC = 2.7028238265542E6,
           NOM_PARA = 'VALE',
           TABLE = sig_bots,
           FILTRE = (_F(NOM_PARA = 'NUME_ORDRE',
                        VALE_I = 2),
                     _F(NOM_PARA = 'EXTREMA',
                        VALE_K = 'MAX')))

TEST_TABLE(CRITERE = 'RELATIF',
           VALE_CALC = 1.6101644711835E7,
           NOM_PARA = 'VALE',
           TABLE = sig_conc,
           FILTRE = (_F(NOM_PARA = 'NUME_ORDRE',
                        VALE_I = 2),
                     _F(NOM_PARA = 'EXTREMA',
                        VALE_K = 'MAX')))

# End
FIN()

