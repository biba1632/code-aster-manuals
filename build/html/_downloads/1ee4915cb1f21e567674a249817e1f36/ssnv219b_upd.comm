#SSNV219B

import math

DEBUT(LANG = 'EN',
      PAR_LOT = 'NON',
      IGNORE_ALARM = ('MODELE1_63'))

# Insert manufactured solution BCs and body force
# from file ssnv219a_mss.38
INCLUDE(UNITE = 38)

# Insert the function to compute the error
# from file ssnv219b_calc_norm.39
INCLUDE(UNITE = 39)

# Convert manufactured solutions into Code_Aster formulas
U_x = FORMULE(VALE = str(u_x), NOM_PARA = ('X', 'Y', 'Z'))
U_y = FORMULE(VALE = str(u_y), NOM_PARA = ('X', 'Y', 'Z'))
U_z = FORMULE(VALE = str(u_z), NOM_PARA = ('X', 'Y', 'Z'))

F_xp_x = FORMULE(VALE = str(f_xp_x), NOM_PARA = ('X', 'Y', 'Z'))
F_xp_y = FORMULE(VALE = str(f_xp_y), NOM_PARA = ('X', 'Y', 'Z'))
F_xp_z = FORMULE(VALE = str(f_xp_z), NOM_PARA = ('X', 'Y', 'Z'))

F_xm_x = FORMULE(VALE = str(f_xm_x), NOM_PARA = ('X', 'Y', 'Z'))
F_xm_y = FORMULE(VALE = str(f_xm_y), NOM_PARA = ('X', 'Y', 'Z'))
F_xm_z = FORMULE(VALE = str(f_xm_z), NOM_PARA = ('X', 'Y', 'Z'))

F_yp_x = FORMULE(VALE = str(f_yp_x), NOM_PARA = ('X', 'Y', 'Z'))
F_yp_y = FORMULE(VALE = str(f_yp_y), NOM_PARA = ('X', 'Y', 'Z'))
F_yp_z = FORMULE(VALE = str(f_yp_z), NOM_PARA = ('X', 'Y', 'Z'))

F_ym_x = FORMULE(VALE = str(f_ym_x), NOM_PARA = ('X', 'Y', 'Z'))
F_ym_y = FORMULE(VALE = str(f_ym_y), NOM_PARA = ('X', 'Y', 'Z'))
F_ym_z = FORMULE(VALE = str(f_ym_z), NOM_PARA = ('X', 'Y', 'Z'))

F_t_x = FORMULE(VALE = str(f_t_x), NOM_PARA = ('X', 'Y', 'Z'))
F_t_y = FORMULE(VALE = str(f_t_y), NOM_PARA = ('X', 'Y', 'Z'))
F_t_z = FORMULE(VALE = str(f_t_z), NOM_PARA = ('X', 'Y', 'Z'))

F_n = FORMULE(VALE = str(f_nn), NOM_PARA = ('X', 'Y', 'Z'))

F_vol_x = FORMULE(VALE = str(f_b_x), NOM_PARA = ('X', 'Y', 'Z'))
F_vol_y = FORMULE(VALE = str(f_b_y), NOM_PARA = ('X', 'Y', 'Z'))
F_vol_z = FORMULE(VALE = str(f_b_z), NOM_PARA = ('X', 'Y', 'Z'))

# Start convergence study
num_refine = 4

# Initialize lists
mesh          = [None]*(num_refine)
errnorm_inf   = [None]*(num_refine)
errnorm_u_L2  = [None]*(num_refine)
errnorm_p_L2  = [None]*(num_refine)
errnorm_inf_G = [None]*(num_refine)

mesh_size = [(1./2.**(i+1)) for i in range(num_refine)]

# Read mesh
mesh[0] = LIRE_MAILLAGE(FORMAT = 'MED')

# Change outward normal orientation on surfaces
mesh[0] = MODI_MAILLAGE(reuse = mesh[0],
                        MAILLAGE = mesh[0],
                        ORIE_PEAU_3D = (_F(GROUP_MA = 'ESCLAVE'),
                                        _F(GROUP_MA = 'BORDX'),
                                        _F(GROUP_MA = 'BORDMX'),
                                        _F(GROUP_MA = 'BORDY'),
                                        _F(GROUP_MA = 'BORDMY')))

# Material properties
E = 1
NU = 0.15
steel = DEFI_MATERIAU(ELAS = _F(E = E, NU = NU))

# Define ramp function for load application
ramp = DEFI_FONCTION(NOM_PARA = 'INST',
                     VALE = (0.0, 0.0,
                             1.0, 1.0),
                     PROL_DROITE = 'LINEAIRE',
                     PROL_GAUCHE = 'LINEAIRE')

# Define fixed time steps
l_times = DEFI_LIST_REEL(DEBUT = 0.0,
                         INTERVALLE = (_F(JUSQU_A = 1.0, 
                                          NOMBRE = 1)))

# Refinement and calculation loop
for refine in range(0, num_refine):

  # Assign model
  model = AFFE_MODELE(MAILLAGE = mesh[refine],
                      AFFE = _F(TOUT = 'OUI',
                                PHENOMENE = 'MECANIQUE',
                                MODELISATION = '3D'))

  # Assign material
  mater = AFFE_MATERIAU(MAILLAGE = mesh[refine],
                        AFFE = _F(TOUT = 'OUI',
                                  MATER = steel))

  # Assign displacement BCs (functions)
  bc_u = AFFE_CHAR_CINE_F(MODELE = model,
                          MECA_IMPO = _F(GROUP_MA = 'HAUT', 
                                         DX = U_x, DY = U_y, DZ = U_z))

  # Assign traction BCs (functions)
  bc_f = AFFE_CHAR_MECA_F(MODELE = model,
                          FORCE_FACE = (_F(GROUP_MA = 'BORDX',   
                                           FX = F_xp_x,  FY = F_xp_y,  FZ = F_xp_z),
                                        _F(GROUP_MA = 'BORDMX',  
                                           FX = F_xm_x, FY = F_xm_y, FZ = F_xm_z),
                                        _F(GROUP_MA = 'BORDY',   
                                           FX = F_yp_x,  FY = F_yp_y,  FZ = F_yp_z),
                                        _F(GROUP_MA = 'BORDMY',  
                                           FX = F_ym_x, FY = F_ym_y, FZ = F_ym_z),
                                        _F(GROUP_MA = 'ESCLAVE', 
                                           FX = F_t_x,  FY = F_t_y,  FZ = F_t_z)))

  # Assign body force (functions)
  body_f = AFFE_CHAR_MECA_F(MODELE = model,
                            FORCE_INTERNE = _F(GROUP_MA = 'VOLUME', 
                                               FX = F_vol_x, FY = F_vol_y, FZ = F_vol_z))

  # Fix the master surface
  bc_m = AFFE_CHAR_CINE(MODELE = model,
                        MECA_IMPO = _F(GROUP_MA = 'MAITRE',
                                       DX = 0.0, DY = 0.0, DZ = 0.0))

  # Define contact conditions
  contact = DEFI_CONTACT(MODELE = model,
                         FORMULATION = 'CONTINUE',
                         ALGO_RESO_GEOM = 'NEWTON',
                         ALGO_RESO_CONT = 'NEWTON',
                         ZONE = _F(GROUP_MA_MAIT = ('MAITRE'),
                                   GROUP_MA_ESCL = ('ESCLAVE'),
                                   INTEGRATION = 'GAUSS',
                                   ORDRE_INT = 4))

  # Solve
  result = STAT_NON_LINE(MODELE = model,
                         CHAM_MATER = mater,
                         COMPORTEMENT = _F(RELATION = 'ELAS',
                                           DEFORMATION = 'GROT_GDEP',
                                           TOUT = 'OUI'),
                         INCREMENT = _F(LIST_INST = l_times),
                         NEWTON = _F(REAC_ITER = 1),
                         CONVERGENCE = _F(ITER_GLOB_MAXI = 100,
                                          RESI_GLOB_RELA = 1.e-8),
                         CONTACT = contact,
                         EXCIT = (_F(CHARGE = bc_f,  
                                     FONC_MULT = ramp),
                                  _F(CHARGE = bc_u,   
                                     FONC_MULT = ramp),
                                  _F(CHARGE = body_f, 
                                     FONC_MULT = ramp),
                                  _F(CHARGE = bc_m)),
                         SOLVEUR = _F(METHODE = 'GCPC',
                                      PRE_COND = 'LDLT_SP'),
                         INFO = 1)

  # Compute stress
  result = CALC_CHAMP(reuse = result,
                      RESULTAT = result,
                      CRITERES = ('SIEQ_ELGA'),
                      CONTRAINTE = ('SIEF_NOEU', 'SIGM_ELNO'))

  
  # Compute analytical solutions and errors
  #    output: {'u_ana': disp_ana, 'p_ana': pres_ana, 
  #             'u_cal': disp_cal, 'p_cal': pres_cal, 
  #             'u_err': disp_err, 'p_err': pres_err,
  #             'u_err_L2': u_err_L2, 'p_err_L2': p_err_L2} 
  res_dict = calcErrorNorm(result, mesh[refine], model, 'VOLUME', 'ESCLAVE', 
                           U_x, U_y, U_z, F_n)

  # Extract error norms
  errnorm_u_L2[refine] = res_dict['u_err_L2']
  errnorm_p_L2[refine] = res_dict['p_err_L2']

  # Write out the results
  DEFI_FICHIER(ACTION = 'ASSOCIER',
               UNITE = 100 + refine,
               FICHIER = '/tmp/REPE_OUT/ssnv219b_upd_iter_' + str(refine) + '.rmed', 
               TYPE = 'BINARY')

  IMPR_RESU(FORMAT = 'MED',
            UNITE = 100 + refine,
            RESU = (_F(CHAM_GD = pres_ana, 
                       GROUP_MA = 'ESCLAVE'),
                    _F(CHAM_GD = pres_cal,
                       GROUP_MA = 'ESCLAVE',
                       NOM_CMP = 'LAGS_C'),
                    _F(RESULTAT = result,
                       GROUP_MA = 'VOLUME')))

  DEFI_FICHIER(ACTION = 'LIBERER',
               UNITE = 100 + refine)

  #==================================
  # Adaptive mesh refinement
  #==================================

  if refine < (num_refine - 1):

     # Create new mesh name
     mesh[refine + 1] = CO('mesh_%d' % (refine + 1))

     # Use mesh refinement tools for uniform refinement
     MACR_ADAP_MAIL(ADAPTATION = 'RAFFINEMENT_UNIFORME',
                    GROUP_MA = ('VOLUME', 'ESCLAVE'),
                    MAILLAGE_N = mesh[refine],
                    MAILLAGE_NP1 = mesh[refine + 1])

     # Destroy concepts that are not needed
     DETRUIRE(CONCEPT = (_F(NOM = model),
                         _F(NOM = mater),
                         _F(NOM = bc_m),
                         _F(NOM = bc_f),
                         _F(NOM = bc_u),
                         _F(NOM = contact),
                         _F(NOM = body_f),
                         _F(NOM = result),
                         _F(NOM = disp_ana),
                         _F(NOM = disp_cal),
                         _F(NOM = disp_err),
                         _F(NOM = pres_ana),
                         _F(NOM = pres_cal),
                         _F(NOM = pres_err)),
              INFO = 1)

# Write out convergence curves
print('Disp: L2 error norm = %e' % res_dict['u_err_L2'])
print('Disp: L2 err norm', errnorm_u_L2)

ratio_u_err_L2 = -math.log(errnorm_u_L2[-1]/errnorm_u_L2[-2])/math.log(2.0)
ratio_p_err_L2 = -math.log(errnorm_p_L2[-1]/errnorm_p_L2[-2])/math.log(2.0)

u_subtitle = 'Element P%i, order of convergence = %f' % (2, ratio_u_err_L2)
p_subtitle = 'Element P%i, order of convergence = %f' % (2, ratio_p_err_L2)

IMPR_FONCTION(FORMAT = 'TABLEAU',
              SEPARATEUR = ',',
              UNITE = 82,
              COURBE = (_F(ABSCISSE = mesh_size,
                           ORDONNEE = errnorm_u_L2)),
              TITRE = 'Error || u_cal - u_ana || in  L2',
              SOUS_TITRE = u_subtitle,
              INFO = 2)

IMPR_FONCTION(FORMAT = 'TABLEAU',
              SEPARATEUR = ',',
              UNITE = 83,
              COURBE = (_F(ABSCISSE = mesh_size,
                           ORDONNEE = errnorm_p_L2)),
              TITRE = 'Error | p_cal - p_ana | in  L2',
              SOUS_TITRE = u_subtitle,
              INFO = 2)

# Write out displacements
IMPR_RESU(FORMAT = 'MED',
          UNITE = 81,
          RESU = (_F(CHAM_GD = res_dict['u_cal'],
                     GROUP_MA = 'VOLUME',
                     NOM_CMP = ('DX', 'DY', 'DZ')),
                  _F(CHAM_GD = res_dict['u_ana'],
                     GROUP_MA = 'VOLUME',
                     NOM_CMP = ('DX', 'DY', 'DZ')),
                  _F(CHAM_GD = res_dict['u_err'],
                     GROUP_MA = 'VOLUME',
                     NOM_CMP = ('DX', 'DY', 'DZ'))))


# Write out contact pressure
IMPR_RESU(FORMAT = 'MED',
          UNITE = 85,
          RESU = (_F(CHAM_GD = pres_ana, 
                     GROUP_MA = 'ESCLAVE'),
                  _F(CHAM_GD = pres_cal,
                     GROUP_MA = 'ESCLAVE',
                     NOM_CMP = 'LAGS_C')))

#================================================================================================
# Verification

# Displacement
TEST_RESU(CHAM_NO = _F(REFERENCE = 'ANALYTIQUE',
                       PRECISION = 1.0E-10,
                       TYPE_TEST = 'MAX',
                       CHAM_GD = disp_err,
                       VALE_CALC =  4.13935271431E-05,
                       VALE_REFE =  4.13935271431E-05))

# Create a dummpy list to define function that can be used by TEST_RESU
func_u = DEFI_FONCTION(NOM_PARA = 'INST',
                       VALE = (0., 1.,
                               1., ratio_u_err_L2))

TEST_FONCTION(VALEUR = _F(VALE_CALC = 2.986100582660,
                          VALE_REFE = 3.0,
                          VALE_PARA = 1.0,
                          REFERENCE = 'ANALYTIQUE',
                          PRECISION = 1.E-2,
                          FONCTION =  func_u))

# Contact pressure

# Create a dummpy list to define function that can be used by TEST_RESU
func_p = DEFI_FONCTION(NOM_PARA = 'INST',
                       VALE = (0., 1.,
                               1., ratio_p_err_L2))

TEST_FONCTION(VALEUR = _F(VALE_CALC = 2.534001467953,
                          VALE_REFE = 2.5,
                          VALE_PARA = 1.0,
                          REFERENCE = 'ANALYTIQUE',
                          PRECISION = 0.03,
                          FONCTION = func_p))

TEST_FONCTION(VALEUR = _F(VALE_CALC = 2.534001467953,
                          VALE_PARA = 1.0,
                          FONCTION = func_p))

# end
FIN()
