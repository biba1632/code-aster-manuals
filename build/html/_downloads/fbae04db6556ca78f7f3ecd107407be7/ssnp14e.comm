# 3D cube with 2 qudratic pentahedral elements in combined tension + shear
# J2 plasticity + kinematic hardening
DEBUT(LANG='EN')

# Read the mesh
mesh = LIRE_MAILLAGE(FORMAT = "MED")

# Assign 3D mechanical element
model = AFFE_MODELE(MAILLAGE = mesh,
                    AFFE = _F(TOUT = 'OUI',
                              PHENOMENE = 'MECANIQUE',
                              MODELISATION = '3D'))

# Material parameters
E = 195000.0
nu = 0.3
h_lin_iso = 1930.0
sig_y = 181.0
h_lin_prager = 2./3. * E * h_lin_iso / (E - h_lin_iso)

# Define material
steel = DEFI_MATERIAU(ELAS = _F(E = E,
                                NU = nu),
                      ECRO_LINE = _F(D_SIGM_EPSI = h_lin_iso,
                                     SY = sig_y),
                      PRAGER = _F(C = h_lin_prager))

# Assign material
mater = AFFE_MATERIAU(MAILLAGE = mesh,
                      AFFE = _F(TOUT = 'OUI',
                                 MATER = steel))

# Define traction functions
# Tension
sigma_fn = DEFI_FONCTION(NOM_PARA = 'INST',
                         VALE = (0.0, 0.0,
                                 1.0, 151.2,
                                 2.0, 257.2,
                                 3.0, 259.3,
                                 4.0, 0.0),
                         PROL_DROITE = 'EXCLU',
                         PROL_GAUCHE = 'EXCLU')

# Shear
tau_fn = DEFI_FONCTION(NOM_PARA = 'INST',
                       VALE = (0.0, 0.0,
                               1.0, 93.1,
                               2.0, 33.1,
                               3.0, 0.0,
                               4.0, 0.0),
                       PROL_DROITE = 'EXCLU',
                       PROL_GAUCHE = 'EXCLU')

# Apply a unit traction BC for tension in -x
tens_bc = AFFE_CHAR_MECA(MODELE = model,
                         FORCE_FACE = _F(GROUP_MA = 'left_face',
                                         FX = -1.))

# Apply unit traction bcs for shear in xy-plane
shear_bc = AFFE_CHAR_MECA(MODELE = model,
                          FORCE_FACE = (_F(GROUP_MA = 'left_face',
                                           FY = -1.),
                                        _F(GROUP_MA = 'right_face',
                                           FY = 1.),
                                        _F(GROUP_MA = 'top_face',
                                           FX = 1.),
                                        _F(GROUP_MA = 'bot_face',
                                           FX = -1.)))

# Apply displacement BCs to prevent rigid body motions
disp_bc = AFFE_CHAR_MECA(MODELE = model,
                         DDL_IMPO = (_F(GROUP_NO = 'node_4',
                                        DX = 0.,
                                        DY = 0.),
                                     _F(GROUP_NO = 'node_8',
                                        DX = 0.,
                                        DY = 0.,
                                        DZ = 0.),
                                     _F(GROUP_NO = 'node_2',
                                        DX = 0.),
                                     _F(GROUP_NO = 'node_6',
                                        DX = 0.),
                                     _F(GROUP_NO = 'node_14', 
                                        DX = 0.),
                                     _F(GROUP_NO = 'node_15', 
                                        DX = 0.),
                                     _F(GROUP_NO = 'node_25', 
                                        DX = 0.),
                                     _F(GROUP_NO = 'node_20', 
                                        DX = 0.)))

# Define time steps
l_times = DEFI_LIST_REEL(DEBUT = 0.0,
                         INTERVALLE = (_F(JUSQU_A = 1.0,
                                          NOMBRE = 1),
                                       _F(JUSQU_A = 2.0,
                                          NOMBRE = 20),
                                       _F(JUSQU_A = 3.0,
                                          NOMBRE = 5),
                                       _F(JUSQU_A = 4.0,
                                          NOMBRE = 1)))

# Run simulation (plastic loading)
result = STAT_NON_LINE(MODELE = model,
                       CHAM_MATER = mater,
                       EXCIT = (_F(CHARGE = disp_bc),
                                _F(CHARGE = tens_bc,
                                   FONC_MULT = sigma_fn),
                                _F(CHARGE = shear_bc,
                                   FONC_MULT = tau_fn)),
                       COMPORTEMENT = _F(RELATION = 'VMIS_CINE_LINE'),
                       INCREMENT = _F(LIST_INST = l_times,
                                      NUME_INST_FIN = 26),
                       NEWTON = _F(MATRICE = 'TANGENTE',
                                   REAC_ITER = 1),
                       RECH_LINEAIRE = _F(ITER_LINE_MAXI = 3),
                       CONVERGENCE = _F(RESI_GLOB_MAXI = 1.0e-3))

# Run simulation (elastic unloading)
result = STAT_NON_LINE(reuse = result,
                       MODELE = model,
                       CHAM_MATER = mater,
                       EXCIT = (_F(CHARGE = disp_bc),
                                _F(CHARGE = tens_bc,
                                   FONC_MULT = sigma_fn),
                                _F(CHARGE = shear_bc,
                                   FONC_MULT = tau_fn)),
                       COMPORTEMENT = _F(RELATION = 'VMIS_CINE_LINE'),
                       ETAT_INIT = _F(EVOL_NOLI = result,
                                      NUME_ORDRE = 26),
                       INCREMENT = _F(LIST_INST = l_times,
                                      NUME_INST_FIN = 27),
                       NEWTON = _F(MATRICE = 'ELASTIQUE',
                                   REAC_ITER = 1),
                       RECH_LINEAIRE = _F(ITER_LINE_MAXI = 3),
                       CONVERGENCE = _F(RESI_GLOB_MAXI = 1.0e-3),
                       INFO = 1)

# Calculate additional data
result = CALC_CHAMP(reuse = result,
                    RESULTAT = result,
                    CONTRAINTE = ('SIGM_ELNO'),
                    VARI_INTERNE = ('VARI_ELNO'),
                    DEFORMATION = ('EPSI_ELNO'))

# Save Gauss point stresses and strains
result = CALC_CHAMP(reuse = result,
                    CONTRAINTE = ('SIGM_ELGA'),
                    DEFORMATION = ('EPSI_ELGA'),
                    RESULTAT = result)

# Extract internal variables
intvar = CREA_CHAMP(TYPE_CHAM = 'ELNO_VARI_R',
                    OPERATION = 'EXTR',
                    RESULTAT = result,
                    NOM_CHAM = 'VARI_ELNO',
                    INST = 1.)

# Verification tests
TEST_RESU(RESU = (_F(INST = 1.0,
                     RESULTAT = result,
                     NOM_CHAM = 'SIGM_ELNO',
                     GROUP_NO = 'node_2',
                     NOM_CMP = 'SIXX',
                     VALE_CALC = 151.20000168341,
                     GROUP_MA = 'pyra2'),
                  _F(INST = 1.0,
                     RESULTAT = result,
                     NOM_CHAM = 'SIGM_ELNO',
                     GROUP_NO = 'node_2',
                     NOM_CMP = 'SIXY',
                     VALE_CALC = 93.100000054896,
                     GROUP_MA = 'pyra2'),
                  _F(INST = 1.0,
                     RESULTAT = result,
                     NOM_CHAM = 'EPSI_ELNO',
                     GROUP_NO = 'node_2',
                     NOM_CMP = 'EPXX',
                     VALE_CALC = 0.014829713751963,
                     CRITERE = 'RELATIF',
                     GROUP_MA = 'pyra2'),
                  _F(INST = 1.0,
                     RESULTAT = result,
                     NOM_CHAM = 'EPSI_ELNO',
                     GROUP_NO = 'node_2',
                     NOM_CMP = 'EPXY',
                     VALE_CALC = 0.013601401184360,
                     CRITERE = 'RELATIF',
                     GROUP_MA = 'pyra2'),
                  _F(INST = 1.0,
                     RESULTAT = result,
                     NOM_CHAM = 'VARI_ELNO',
                     GROUP_NO = 'node_2',
                     NOM_CMP = 'V1',
                     VALE_CALC = 18.264003621381,
                     GROUP_MA = 'pyra2'),
                  _F(INST = 1.0,
                     RESULTAT = result,
                     NOM_CHAM = 'VARI_ELNO',
                     GROUP_NO = 'node_2',
                     NOM_CMP = 'V4',
                     VALE_CALC = 16.868836641619,
                     GROUP_MA = 'pyra2'),
                  _F(INST=1.0,
                     RESULTAT = result,
                     NOM_CHAM = 'VARI_ELNO',
                     GROUP_NO = 'node_2',
                     NOM_CMP = 'V2',
                     VALE_CALC = -9.1320017999493,
                     GROUP_MA = 'pyra2'),
                  _F(INST = 2.0,
                     RESULTAT = result,
                     NOM_CHAM = 'EPSI_ELNO',
                     GROUP_NO = 'node_2',
                     NOM_CMP = 'EPXX',
                     VALE_CALC = 0.040670995663408,
                     CRITERE = 'RELATIF',
                     GROUP_MA = 'pyra2'),
                  _F(INST = 2.0,
                     RESULTAT = result,
                     NOM_CHAM = 'EPSI_ELNO',
                     GROUP_NO = 'node_2',
                     NOM_CMP = 'EPXY',
                     VALE_CALC = 0.019667236455173,
                     CRITERE = 'RELATIF',
                     GROUP_MA = 'pyra2'),
                  _F(INST = 3.0,
                     RESULTAT = result,
                     NOM_CHAM = 'EPSI_ELNO',
                     GROUP_NO = 'node_2',
                     NOM_CMP = 'EPXX',
                     VALE_CALC = 0.044102845407440,
                     CRITERE = 'RELATIF',
                     GROUP_MA = 'pyra2'),
                  _F(INST = 3.0,
                     RESULTAT = result,
                     NOM_CHAM = 'EPSI_ELNO',
                     GROUP_NO = 'node_2',
                     NOM_CMP = 'EPXY',
                     VALE_CALC = 0.018912865621848,
                     CRITERE = 'RELATIF',
                     GROUP_MA = 'pyra2'),
                  _F(INST = 4.0,
                     RESULTAT = result,
                     NOM_CHAM = 'EPSI_ELNO',
                     GROUP_NO = 'node_2',
                     NOM_CMP = 'EPXX',
                     VALE_CALC = 0.042773101814112,
                     CRITERE = 'RELATIF',
                     GROUP_MA = 'pyra2'),
                  _F(INST = 4.0,
                     RESULTAT = result,
                     NOM_CHAM = 'EPSI_ELNO',
                     GROUP_NO = 'node_2',
                     NOM_CMP = 'EPXY',
                     VALE_CALC = 0.018912865619723,
                     CRITERE = 'RELATIF',
                     GROUP_MA = 'pyra2')))
  
# Write results for visualization
IMPR_RESU(FORMAT= 'MED',
          RESU = _F(RESULTAT = result),
          UNITE = 80)

# Extract results for stress-strain plots (xx and xy)
sig_xx_1 = RECU_FONCTION(GROUP_MA = ('pyra2'),
                         NOM_CHAM = 'SIGM_ELGA',
                         NOM_CMP = 'SIXX',
                         POINT = 1,
                         RESULTAT = result)
eps_xx_1 = RECU_FONCTION(GROUP_MA = ('pyra2'),
                         NOM_CHAM = 'EPSI_ELGA',
                         NOM_CMP = 'EPXX',
                         POINT = 1,
                         RESULTAT = result)
sig_xy_1 = RECU_FONCTION(GROUP_MA = ('pyra2'),
                         NOM_CHAM = 'SIGM_ELGA',
                         NOM_CMP = 'SIXY',
                         POINT = 1,
                         RESULTAT = result)
eps_xy_1 = RECU_FONCTION(GROUP_MA = ('pyra2'),
                         NOM_CHAM = 'EPSI_ELGA',
                         NOM_CMP = 'EPXY',
                         POINT = 1,
                         RESULTAT = result)

# Write results for plotting
IMPR_FONCTION(COURBE = (_F(FONCTION = sig_xx_1),
                        _F(FONCTION = eps_xx_1)),
              FORMAT = 'TABLEAU',
              SEPARATEUR = ',',
              UNITE = 8)
IMPR_FONCTION(COURBE = (_F(FONCTION = sig_xy_1),
                        _F(FONCTION = eps_xy_1)),
              FORMAT = 'TABLEAU',
              SEPARATEUR = ',',
              UNITE = 8)

# Finish
FIN()
