# Square composed of one QUAD9 element and two TRI7 elements
# rotated in yz plane

import sys
import salome
import salome_notebook
from salome.smesh import smeshBuilder
import SMESH, SALOMEDS

ExportPATH="/home/banerjee/Salome/ssnp15/"

# Initialize
salome.salome_init()
notebook = salome_notebook.NoteBook()
smesh = smeshBuilder.New()

# Create mesh
mesh = smesh.Mesh()
smesh.SetName(mesh, 'mesh')

# Add nodes
node1 = mesh.AddNode( 0, -0.8, 0.6 )
node2 = mesh.AddNode( 0, -0.2, 1.4 )
node3 = mesh.AddNode( 0, 0, 0 )
node4 = mesh.AddNode( 0, 0.6, 0.8 )
node5 = mesh.AddNode( 0, -0.40000000E+00, 0.30000000E+00)
node6 = mesh.AddNode( 0,  0.30000000E+00, 0.40000000E+00)
node7 = mesh.AddNode( 0,  0.20000000E+00, 0.11000000E+01)
node8 = mesh.AddNode( 0, -0.50000000E+00, 0.10000000E+01)
node9 = mesh.AddNode( 0, -0.10000000E+00, 0.70000000E+00)
node10 = mesh.AddNode(0, -0.25000000E+00, 0.50000000E+00)
node11 = mesh.AddNode(0,  0.15000000E+00, 0.20000000E+00)
node12 = mesh.AddNode(0, -0.65000000E+00, 0.80000000E+00)
node13 = mesh.AddNode(0, -0.06666666E+00, 0.46666666E+00)
node14 = mesh.AddNode(0, -0.43333333E+00, 0.53333333E+00)
node15 = mesh.AddNode(0,  0.45000000E+00, 0.60000000E+00)
node16 = mesh.AddNode(0, -0.35000000E+00, 0.12000000E+01)
node17 = mesh.AddNode(0,  0.05000000E+00, 0.90000000E+00)

# Add groups for the nodes
node_1 = mesh.CreateEmptyGroup( SMESH.NODE, 'node_1' )
node_2 = mesh.CreateEmptyGroup( SMESH.NODE, 'node_2' )
node_3 = mesh.CreateEmptyGroup( SMESH.NODE, 'node_3' )
node_4 = mesh.CreateEmptyGroup( SMESH.NODE, 'node_4' )
node_5 = mesh.CreateEmptyGroup( SMESH.NODE, 'node_5' )
node_6 = mesh.CreateEmptyGroup( SMESH.NODE, 'node_6' )
node_7 = mesh.CreateEmptyGroup( SMESH.NODE, 'node_7' )
node_8 = mesh.CreateEmptyGroup( SMESH.NODE, 'node_8' )
node_9 = mesh.CreateEmptyGroup( SMESH.NODE, 'node_9' )
node_10 = mesh.CreateEmptyGroup( SMESH.NODE, 'node_10' )
node_11 = mesh.CreateEmptyGroup( SMESH.NODE, 'node_11' )
node_12 = mesh.CreateEmptyGroup( SMESH.NODE, 'node_12' )
node_13 = mesh.CreateEmptyGroup( SMESH.NODE, 'node_13' )
node_14 = mesh.CreateEmptyGroup( SMESH.NODE, 'node_14' )
node_15 = mesh.CreateEmptyGroup( SMESH.NODE, 'node_15' )
node_16 = mesh.CreateEmptyGroup( SMESH.NODE, 'node_16' )
node_17 = mesh.CreateEmptyGroup( SMESH.NODE, 'node_17' )
nbAdd = node_1.Add([1])
nbAdd = node_2.Add([2])
nbAdd = node_3.Add([3])
nbAdd = node_4.Add([4])
nbAdd = node_5.Add([5])
nbAdd = node_6.Add([6])
nbAdd = node_7.Add([7])
nbAdd = node_8.Add([8])
nbAdd = node_9.Add([9])
nbAdd = node_10.Add([10])
nbAdd = node_11.Add([11])
nbAdd = node_12.Add([12])
nbAdd = node_13.Add([13])
nbAdd = node_14.Add([14])
nbAdd = node_15.Add([15])
nbAdd = node_16.Add([16])
nbAdd = node_17.Add([17])

# Add tri elements
face1 = mesh.AddFace([1, 3, 8, 5, 10, 12, 14])
face2 = mesh.AddFace([3, 6, 8, 11, 9, 10, 13])

# Add quad element
face3 = mesh.AddFace([8, 6, 4, 2, 9, 15, 7, 16, 17])

# Add groups for the faces
g_face1 = mesh.CreateEmptyGroup( SMESH.FACE, 'tri1' )
g_face2 = mesh.CreateEmptyGroup( SMESH.FACE, 'tri2' )
g_face3 = mesh.CreateEmptyGroup( SMESH.FACE, 'quad' )
nbAdd = g_face1.Add([1])
nbAdd = g_face2.Add([2])
nbAdd = g_face3.Add([3])

# Add segment elements
left_seg = mesh.AddEdge([1, 3, 5])
right_seg = mesh.AddEdge([4, 2, 7])
top_seg1 = mesh.AddEdge([2, 8, 16])
top_seg2 = mesh.AddEdge([8, 1, 12])
bot_seg1 = mesh.AddEdge([3, 6, 11])
bot_seg2 = mesh.AddEdge([6, 4, 15])

# Add groups for the segments
g_left = mesh.CreateEmptyGroup( SMESH.EDGE, 'left_edge' )
g_right = mesh.CreateEmptyGroup( SMESH.EDGE, 'right_edge' )
g_top = mesh.CreateEmptyGroup( SMESH.EDGE, 'top_edge' )
g_bot = mesh.CreateEmptyGroup( SMESH.EDGE, 'bot_edge' )
nbAdd = g_left.Add([4])
nbAdd = g_right.Add([5])
nbAdd = g_top.Add([6, 7])
nbAdd = g_bot.Add([8, 9])

# Export mesh
mesh.ExportMED( r''+ExportPATH+'ssnp15d.mmed'+'')

# Update GUI object tree
if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()
