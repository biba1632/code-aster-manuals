# SSNP169 fused functions

import ssnp169_funcs_con as fcon
import ssnp169_funcs_var as fvar
import ssnp169_config as con


#------------------------------------------------------
# Fusion of the declarations of the displacement fields
#------------------------------------------------------
def ux_f(x,y):
    r = fcon.radius(x,y)
    if(r>con.R2):
        return fcon.ux1_c(x,y)+fvar.ux1_v(x,y)
    else: 
        return fcon.ux2_c(x,y)+fvar.ux2_v(x,y)

def uy_f(x,y):
    r = fcon.radius(x,y)
    if(r>con.R2):
        return fcon.uy1_c(x,y)+fvar.uy1_v(x,y)
    else: 
        return fcon.uy2_c(x,y)+fvar.uy2_v(x,y)

#------------------------------------------------------
# Fusion of the declarations of the stress fields
#------------------------------------------------------
def SIG_XX_f(x,y):
    r = fcon.radius(x,y)
    if(r>con.R2):
        return fcon.SIGMA_XX_1_c(x,y)+fvar.SIGMA_XX_1_v(x,y)
    else: 
        return fcon.SIGMA_XX_2_c(x,y)+fvar.SIGMA_XX_2_v(x,y)

def SIG_YY_f(x,y):
    r = fcon.radius(x,y)
    if(r>con.R2):
        return fcon.SIGMA_YY_1_c(x,y)+fvar.SIGMA_YY_1_v(x,y)
    else: 
        return fcon.SIGMA_YY_2_c(x,y)+fvar.SIGMA_YY_2_v(x,y)

def SIG_ZZ_f(x,y):
    r = fcon.radius(x,y)
    if(r>R2):
        return fcon.SIGMA_ZZ_1_c(x,y)+fvar.SIGMA_ZZ_1_v(x,y)
    else: 
        return fcon.SIGMA_ZZ_2_c(x,y)+fvar.SIGMA_ZZ_2_v(x,y)

def SIG_XY_f(x,y):
    r = fcon.radius(x,y)
    if(r>con.R2):
        return fcon.SIGMA_XY_1_c(x,y)+fvar.SIGMA_XY_1_v(x,y)
    else: 
        return fcon.SIGMA_XY_2_c(x,y)+fvar.SIGMA_XY_2_v(x,y)

#------------------------------------------------------
# Fusion of the declarations of the strain fields
#------------------------------------------------------
def EPS_XX_f(x,y):
    r = fcon.radius(x,y)
    if(r>con.R2):
        return fcon.EPSI_XX_1_c(x,y)+fvar.EPSI_XX_1_v(x,y)
    else: 
        return fcon.EPSI_XX_2_c(x,y)+fvar.EPSI_XX_2_v(x,y)

def EPS_YY_f(x,y):
    r = fcon.radius(x,y)
    if(r>con.R2):
        return fcon.EPSI_YY_1_c(x,y)+fvar.EPSI_YY_1_v(x,y)
    else: 
        return fcon.EPSI_YY_2_c(x,y)+fvar.EPSI_YY_2_v(x,y)

def EPS_ZZ_f(x,y):
    r = fcon.radius(x,y)
    if(r>con.R2):
        return fcon.EPSI_ZZ_1_c(x,y)+fvar.EPSI_ZZ_1_v(x,y)
    else: 
        return fcon.EPSI_ZZ_2_c(x,y)+fvar.EPSI_ZZ_2_v(x,y)

def EPS_XY_f(x,y):
    r = fcon.radius(x,y)
    if(r>con.R2):
        return fcon.EPSI_XY_1_c(x,y)+fvar.EPSI_XY_1_v(x,y)
    else: 
        return fcon.EPSI_XY_2_c(x,y)+fvar.EPSI_XY_2_v(x,y)
