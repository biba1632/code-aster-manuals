.. _u1.05.00:

***************************************************************************************
**[U1.05.00]**: FORMA00: An example of an axisymmetric cylinder under internal pressure
***************************************************************************************

In this example, we will model an axisymmetric thin-walled tank under hydrostatic pressure.  The example
illustrates the essential steps in a ``Code_Aster`` analysis.

.. note::
  The data used in this example are from the ``FORMA00`` test case which is located in the ``src/astest`` 
  directory of the ``Code_Aster`` source tree.  

  * The ``Salomé`` script for the generation of CAD model and the mesh is in ``src/astest/forma00a.datg``.  
    Running the script in the ``Salome`` GUI generates a MED file which can be renamed ``forma00a.mmed``.
  * The ``Code_Aster`` command file is ``src/astest/forma00a.comm``.

To run the example directly from the command line, use::

  as_run --quick forma00a.comm forma00a.mmed forma00a.rmed >& forma00a.msg


Modeling a mechanical problem with ``Code_Aster``
=================================================

.. image:: u1.05.00/fig1.svg
   :height: 300px
   :align: right

The problem to be modeled is a thin cylindrical tank of constant thickness (thickness 0.1 m, interior
radius R = 1 m, height  L = 4 m ) subjected to an internal pressure that varies with the height and
corresponds to a hydrostatic pressure.

Because of the symmetries of the geometry and loading, an axisymmetric two-dimensional model can 
be used for the problem.

It will therefore be enough to model a vertical slice of this cylinder (in the xy-plane).

The two main stages of the modeling process are:

* generating the mesh
* writing the command file

Generating the model and the mesh
=================================

Things to keep in mind when generating the mesh
-----------------------------------------------

.. image:: u1.05.00/fig2.svg
   :height: 400px
   :align: right

Whatever meshing software is used, during the creation of the mesh it is necessary to plan the topological zones and names which will be used in the calculation to assign element properties, boundary conditions, loads, materials, etc.

In practice, these topological entities are assigned to groups: 

* groups of nodes (containing possibly only one node such as the points A, B, C, D in the figure.
* groups of elements corresponding subdomains of the mesh, or elements where loads are to be applied.
  In this example, the groupd of elements ``LDA`` contains linear elements (skin elements) which
  will be used to apply the pressure.

.. note::
  Although it is possible to directly use the node and element numbers in the command file, it is preferable
  to use named entities (groups of nodes, groups of elements). This makes it possible to have  a command file
  independent of the degree of refinement of the grid.

Creating the geometric model
----------------------------

The ``Salome`` graphical user interface is a convenient way of creating a model that
can be analyzed with ``Code_Aster``.

An alternative way is to use a Python script to generate the geometry::

  import salome
  import GEOM
  from salome.geom import geomBuilder

  salome.salome_init()
  geompy = geomBuilder.New()

  # create vertices
  A = geompy.MakeVertex(1.0 , 0.0, 0.0)
  B = geompy.MakeVertex(1.1 , 0.0, 0.0)
  C = geompy.MakeVertex(1.1 , 4.0, 0.0)
  D = geompy.MakeVertex(1.0 , 4.0, 0.0)

  # create lines, wire and  face
  LAB  = geompy.MakeLineTwoPnt(A, B)
  LBC  = geompy.MakeLineTwoPnt(B, C)
  LCD  = geompy.MakeLineTwoPnt(C, D)
  LDA  = geompy.MakeLineTwoPnt(D, A)
  Wire = geompy.MakeWire([LAB, LBC, LCD, LDA], 1e-07)
  Face = geompy.MakeFaceWires([Wire], 1)

  # create group entities
  LDA = geompy.GetInPlace(Face, LDA)
  LAB = geompy.GetInPlace(Face, LAB)
  NOEUDA = geompy.GetInPlace(Face, A)
  NOEUDB = geompy.GetInPlace(Face, B)
  NOEUDC = geompy.GetInPlace(Face, C)
  NOEUDD = geompy.GetInPlace(Face, D)

Creating the mesh
-----------------

The mesh can also be generated using the ``Salome`` graphical interface.  Alternatively,
the Python script can be used::

  import SMESH
  from salome.smesh import smeshBuilder
  smesh = smeshBuilder.New()

  # mesh the geometry
  Mesh_1 = smesh.Mesh(Face)
  Regular_1D = Mesh_1.Segment()
  Max_Size_1 = Regular_1D.MaxSize(0.1)
  MEFISTO_2D = Mesh_1.Triangle()
  isDone = Mesh_1.Compute()

  # create mesh groups
  Mesh_1.GroupOnGeom(LDA, 'LDA', SMESH.EDGE)
  Mesh_1.GroupOnGeom(LAB, 'LAB', SMESH.EDGE)
  Mesh_1.GroupOnGeom(NOEUDA, 'NOEUDA', SMESH.NODE)
  Mesh_1.GroupOnGeom(NOEUDB, 'NOEUDB', SMESH.NODE)
  Mesh_1.GroupOnGeom(NOEUDC, 'NOEUDC', SMESH.NODE)
  Mesh_1.GroupOnGeom(NOEUDD, 'NOEUDD', SMESH.NODE)

Saving the mesh in MED format
-----------------------------

The mesh can be saved in several formats from the ``Salome`` interface.  Alternatively,
you can use a Python script::

  Mesh_1.ExportMED('forma00a.mmed', 0, SMESH.MED_V2_2, 1)


Writing the command file
========================

From scratch?
-------------

When one wants to model a new thermomechanical problem, one does not generally start with a blank sheet. 
It is useful to start with a command file that models a problem close to the one of interest.  How does
one obtain these files? The sources are varied:

* the database of ``Code_Aster`` tests, with its documentation, is often an important guide
  because it covers most of the features of the code
* training courses and vaidation tests, and their associated documentation.

The drafting of the command file can be facilitated by using the command file editor ``EFICAS``.

Some essential commands
-----------------------

Let us now examine the commands needed to run the pressurized cylinder problem.::

   # Thin cylinder under hydrostatic pressure  (Comments are preceded by a # sign)

   # Command required to start the analysis
   DEBUT()

   # Reading the input mesh
   #
   # Reading the mesh in MED format from the default file (logical unit 20). 
   # This command also creates the mesh concept containing the mesh in Aster
   mesh1 = LIRE_MAILLAGE(FORMAT='MED')

   # Definition of the model
   #
   # A model is a concept, here named model1, containing the types of finite elements 
   # used for the calculation. 
   # In this case we associate axisymmetric mechanical finite elements
   # with all the topological elements of the mesh
   model1 = AFFE_MODELE(MAILLAGE = mesh1,
                        AFFE = _F(TOUT = 'OUI',
                                  PHENOMENE = 'MECANIQUE',
                                  MODELISATION = 'AXIS',),)

   # Definition of materials
   #
   # Definition of a particular material (steel in this problem) 
   # and its properties.  For an elastic material we specify the
   # Young's modulus (E) and Poisson's ratio (NU)
   steel = DEFI_MATERIAU(ELAS = _F(E = 210000000000.0,
                                   NU = 0.3,),)

   # Assignment of materials
   #
   # Assignment of the material steel to the mesh 'mesh1'. 
   # In this problem, the material # is the same for all the elements.  
   # If needed, we could assign different materials to different element groups
   material_field = AFFE_MATERIAU(MAILLAGE = mesh1,
                                  AFFE = _F(TOUT = 'OUI',
                                            MATER = steel,),)

   # Definition of displacement boundary conditions
   #
   # The boundary conditions can be applied to nodes, groups of nodes, 
   # elements, or element groups. 
   # In this example, the nodes of the mesh group "LAB" (the support nodes)
   # are fixed in the y-direction (DY = 0)
   disp_bc = AFFE_CHAR_MECA(MODELE = model1,
                            FACE_IMPO = _F(GROUP_MA = 'LAB',
                                           DY = 0,),)

   # Definition of the load function: pressure as a function of y
   # 
   # Functions are defined point by point (the default is affine interpolation
   # between two adjacent points on the function curve).  In this example,
   # the pressure varies linearly between 200 kPa (at y = 0) and 0 (at y = L).
   load_function = DEFI_FONCTION(NOM_PARA = 'Y',
                                 VALE = (0.0, 200000.0,
                                         4.0,      0.0,),)

   # Assignment of the load
   #
   # The load function is applied on the nodes that make up the union of
   # the mesh groups "LFA" and "LDF"
   load_bc = AFFE_CHAR_MECA_F(MODELE = model1,
                              PRES_REP = _F(GROUP_MA = ('LDA',),
                                            PRES = load_function,),)
                  
   # Solution
   #
   # In this example we use the command for static linear thermoelasticity
   # and association with it the model, the material, the load and the 
   # displacement boundary conditions.  
   # The result, which contains the computed displacement field,
   # is stored in "result1".
   result1 = MECA_STATIQUE(MODELE = model1,
                           CHAM_MATER = material_field,
                           EXCIT = (_F(CHARGE = load_bc,),
                                    _F(CHARGE = disp_bc,),),)

   # Calculation of the stresses
   # 
   # "reuse" means that extra information is saved in the result,
   # in this case the stress field (the displacement field has already
   # been saved by default).
   #
   # The name 'SIGM_ELNO' means stresses calculated at the nodes of 
   # each element using interpolation
   result1 = CALC_CHAMP(reuse = result1,
                        RESULTAT = result1,
                        CONTRAINTE=('SIGM_ELNO'))

   # Saving the results to a file for visualization in Salome
   #
   # The results are written out in MED format.
   # The data will contain nodal displacements and stresses for all nodes
   IMPR_RESU(FORMAT = 'MED',         
             RESU = _F(RESULTAT = result1,),)

   # Saving the results for point A in the model
   #
   # Print results in text format for only group of nodes 'NOEUDA'
   IMPR_RESU(FORMAT = "RESULTAT",
             MODELE = model1,
             RESU = _F(RESULTAT = result1,
                       GROUP_NO='NOEUDA',),)

   # Required command to end the simulation
   FIN();


The results file
================
The text format results file from the above simulation contains the following:

* A header (in French) containing the date, the version of the code, and the computer platform used.
  The content can be roughly translated as::

        -- CODE_ASTER -- VERSION: DEVELOPMENT STABILISEE (testing) --        
                                                                                
                          Version 11.1.0 of the 12/7/2011
                         Copyright EDF R & D 1991 - 2012                          
                    Execution of: Thu Apr 12 18:41: 37 2012                     
                          Name of the machine: cli75at                           
                              Architecture: 64bit                              
                          Type of processor: x86_64                           
                Operating system: Linux 2.6.32-27-generic                
                        Language of the messages: Fr (UTF-8)
                                                                                
                           Parallelism MPI: inactive                           
                      Version of bookstore HDF5: 1.8.4                      
                      Version of bookstore MED: 3.0.4                       
                          Bookstore MUMPS: installed                           
                    Version of the bookstore SCOTCH TAPE: 5.1.10
                    Limit of the static storage: 1,000 Mo                    
                  Limit of the dynamic storage: 299,000 Mo                   
                Size limits files of exchange: 48,000 Go                
                                                                                
        --------------------------------------------------------------------------------
        ASTER 11.01.00 CONCEPT result1 CALCULATES 4/12/2012 A 18:41: 37 OF TYPE EVOL_ELAS  

* The displacement field at the nodes of the group A (which contains 
  only one node in this example).  Roughly translated::

    GROUP_MA: NOEUDA                                                                 
    FIELD WITH THE NODES OF REFERENCE SYMBOL  DEPL
     SEQUENCE NUMBER: 1 INST:  0.00000E+00                                            
     NODE        DX           DY      
     N1        9.95605E-06  1.24077E-23

* The stress field at each node ordered by element number::

    FIELD BY ELEMENT WITH THE NODES OF REFERENCE SYMBOL  SIGM_ELNO
    SEQUENCE NUMBER: 1 INST:  0.00000E+00                                            
    M211      SIXX         SIYY         SIZZ         SIXY       
    N1       -1.26603E+05 -8.29982E+04  1.97655E+06  1.02990E+03
    M212      SIXX         SIYY         SIZZ         SIXY       
    N1       -1.50710E+05 -1.11440E+04  1.94959E+06 -2.92821E+04
  
* A table summarizing the commands used and CPU time taken by each command::

    ********************************************************************************
    * COMMAND                  :       TO USE:     SYSTEM:   USER+SYS:    ELAPSED *
    ********************************************************************************
    * AFFE_MODELE              :       0.08:       0.02:       0.10:       0.10 *
    ********************************************************************************
    * TOTAL_JOB                :       1.59:       0.14:       1.73:       1.67 *
    ********************************************************************************

Other files produced by the calculation
=======================================

The message file
----------------

This file contains an echo of the commands and gives additional information about the execution of each
command.  In this example, for the command `MECA_STATIQUE` we get the message (roughly translated)::

   #-------------------------------------------------------------------------
   # Orders No:  0009            Concept of the type: evol_elas
   #-------------------------------------------------------------------------
   result1 = MECA_STATIQUE (EXCIT= (_F (TYPE_CHARGE='FIXE',
                                        CHARGE=load_bc),
                                    _F (TYPE_CHARGE=' FIXE',
                                        CHARGE=disp_bc)),
                           INFO=1,
                           OPTION='SIEF_ELGA',
                           SOLVEUR=_F (RENUM='METIS',
                                       STOP_SINGULIER='OUI',
                                       METHODE='MULT_FRONT',
                                       NPREC=8),
                           INST=0.0,
                           MODELE=model1,
                           CHAM_MATER=material_field,);

   The linear system to solve contains 51 nodes of which:
      - 43 nodes carrying of the physical degrees of freedom
      - 8 nodes carrying of the degrees of freedom of Lagrange
   For a total of 94 equations.

      FIELD STORES:             URGENT DEPL:  0.00000E+00  SEQUENCE NUMBER:     1

   # End command No: 0009 user+syst: 0.13s (syst: 0.02s, Elaps: 0.10s)
   #-------------------------------------------------------------------------

The MED format results file
---------------------------

The MED format results file is produced by default by ``astk`` in the logical unit 80.

This file is essential for the visualization modules of ``Salomé`` (``POSTPRO``,
``PARAVIS``) to be able to display the simulation results (displacement, stresses).

.. image:: u1.05.00/fig3.jpg
   :height: 400px
   :align: center

