
\subsection{The mechanical problem}

The nodal unknowns are $\BU = \{u_i~,~~ i=1\dots n\}$.

Nodal displacements are not all independent: there exist $p < n$
homogeneous linear relations between these displacements:
$B_j(\BU) = 0 ~~,~j = 1\dots p$. However, these linear relations are independent, i.e., the
$p$th row of the matrix $\BB$ contains the coefficient of the $p$th relation.

The matrix $\BK$ is the stiffness matrix and $\BM$ is the mass matrix, both without 
constraints included.

\subsection{Reduced system}

Suppose the constraint equations are written in the form
\Beq
   \label{eq:modal_constraint_1}
   \BB \BU = 0
\Eeq
where $\BB$ is a $p\times n$ matrix, and $\BU$ is the vector of nodal unknowns, a $n \times 1$ matrix.
Then, 
\Beq
   \label{eq:modal_constraint_2}
   \BB \dot{\BU} = 0
\Eeq
is also valid for time rates of the unknowns.

Moreover, if $\BB$ has $p$ rows, there there exists a \textbf{square} submatrix of $\BB$ 
that has $p$ rows.  Let us call this submatrix $\BB_1$.

Let us next partition the unknowns into $\BU_1$ and $\BU_2$ such that
\Beq
   \BB \BU = 0 \leftrightarrow 
   \begin{bmatrix} \BB_1 & \BB_2 \end{bmatrix}
   \begin{bmatrix} \BU_1 \\ \BB_2 \end{bmatrix} = 0
\Eeq
where $\BU_1 \in \mathbb{R}^p$, $\BU_2 \in \mathbb{R}^{n-p}$, 
      $\BB_1 \in \mathbb{R}^p \times \mathbb{R}^p$, $\BB_2 \in \mathbb{R}^p \times\mathbb{R}^{n-p}$. 
The constraint relations can then be written as
\Beq
   \BB_1 \BU_1 + \BB_2 \BU_2 = 0
\Eeq
Since $\BB_1$ is invertible, this makes it possible to write
\Beq
   \label{eq:modal_constraint_3}
   \BU_1 = \BB_1^{-1} \BB_2 \BU_2
\Eeq

\textbf{Reduced stiffness matrix}

The elastic energy of the unforced discrete structure is
\Beq
   W_{text{def}} = \tfrac{1}{2} \BU^T \BK \BU
\Eeq
If one partitions the matrix $\BK$ in the same manner as $\BU$, we get
\Beq
  \BK = \begin{bmatrix} \BK_1 & \BK_{12} \\
                        \BK_{12}^T \\ \BK_2 \end{bmatrix} 
\Eeq
Then,
\Beq
   2 W_{text{def}} = \BU_1^T \BK_1 \BU_1 + 
                     \BU_2^T \BK_2 \BU_2 + 
                     \BU_2^T \BK_{12} \BU_1 + 
                     \BU_1^T \BK_{12}^T \BU_2
\Eeq
Let us introduce the linear constraints \ref{eq:modal_constraint_3}:
\Beq
   \Bal
   2 W_{text{def}} & = \BU_2^T \BB_2^T \BB_1^{-T} \BK_1 \BB_1^{-1} \BB_2 \BU_2 + 
                     \BU_2^T \BK_2 \BU_2 - \\
                   &  \BU_2^T \BK_{12} \BB_1^{-1} \BB_2 \BU_2 - 
                     \BU_2^T \BB_2^T \Bb_1^{-T} \BK_{12}^T \BU_2
                   & = \BU_2^T \BKtilde_2 \BU_2
   \Eal
\Eeq
with
\Beq
   \label{eq:modal_constraint_4}
   \BKtilde_2 = \BK_2 + \BB_2^T \BB_1^{-T} \BK_1 \BB_1^{-1} \BB_2 
                - \BK_{12} \BB_1^{-1} \BB_2 - \BB_2^T \BB_1^{-T} \BK_{12}^T
\Eeq
The deformation energy of the reduced structure can therefore be expressed in the form
of a blinear form in $\BU_2$. The nodal unknowns $\BU_1$ have been \textbf{eliminated} and
the nodal unknowns $\BU_2$ are \text{no longer forced}.

\textbf{Reduced mass matrix}

Let us adopt the same partition for the mass matrix $\BM$.  We can write \ref{eq:eq:modal_constraint_2}
as
\Beq
  \BB_1\dot{\BU}_1 + \BB_2\dot{\BU}_2 = 0
\Eeq
The process used for the stiffness matrix leads to
\Beq
  2 W_{\text{mass}} = \dot{\BU}_2^T \BMtilde_2 \dot{\BU}_2
\Eeq
where
\Beq
   \label{eq:modal_constraint_5}
   \BMtilde_2 = \BM_2 + \BB_2^T \BB_1^{-T} \BM_1 \BB_1^{-1} \BB_2 
                - \BM_{12} \BB_1^{-1} \BB_2 - \BB_2^T \BB_1^{-T} \BM_{12}^T
\Eeq

\textbf{Conclusion}

The system that has to be solved to find the $(n-p)$ eigenmodes of the forced structure is:
\Beq
   (\BKtilde_2 - \omega_i^2 \BMtilde_2) \BX_i = 0
\Eeq
where $\BX_i \in \mathbb{R}^{n-p}$, $\omega^2 \in \mathbb{R}$, and $\BKtilde_2$, $\BMtilde_2$ are
defined in \ref{eq:modal_constraint_4} and \ref{eq:modal_constraint_5}.

\textbf{Application to the constrained degrees of freedom}

In this case
\Beq
   \BB_1 = \BI \quad \Tand \quad \BB_2 = 0
\Eeq
from which: $\BKtilde_2 = \BK_2$ and $\BMtilde_2 = \BM_2$, i.e., it is enough to ignore the
constrained rows and columns of the two matrices.

\subsection{Dual system}

We saw in section \ref{sec:5} that the double Lagrange method led to the matrix
\Beq
  \BA^\star = \begin{bmatrix}
                 \BA & \beta \BB^T & \beta \BB^T \\
                 \beta \BB^T & -\alpha \BI & \alpha \BI \\
                 \beta \BB^T & \alpha \BI & -\alpha \BI 
              \end{bmatrix}
\Eeq
where $\BB$ is the matrix of kinematic constraints ($\BB\BU = 0$), 
$\alpha \in \mathbb{R}^{+}$ is arbitrary and $\alpha \ne 0$, and
$\beta \in \mathbb{R}$ is arbitrary and $\beta \ne 0$.

Let us apply the Lagrange dualization of boundary conditions to the matrices $\BK$ and
$\BM$ by partitioning the degrees of freedom into $\BX_,\BX_2$ as in Section \ref{sec:7.3}.
The eigenvalue problem then becomes:
\Beq
  (S)\left(
  \begin{bmatrix}
    \BK_1 & \BK_{12} & \beta_k \BB_1^T & \beta_k \BB_1^T \\
    \BK_{12} & \BK_{2} & \beta_k \BB_2^T & \beta_k \BB_2^T \\
    \beta_k \BB_1 & \beta_k \BB_2 & -\alpha_k \BI & \alpha_k \BI \\
    \beta_k \BB_1 & \beta_k \BB_2 & \alpha_k \BI & -\alpha_k \BI 
  \end{bmatrix}
  - \omega^2 
  \begin{bmatrix}
    \BM_1 & \BM_{12} & \beta_m \BB_1^T & \beta_m \BB_1^T \\
    \BM_{12} & \BM_{2} & \beta_m \BB_2^T & \beta_m \BB_2^T \\
    \beta_m \BB_1 & \beta_m \BB_2 & -\alpha_m \BI & \alpha_m \BI \\
    \beta_m \BB_1 & \beta_m \BB_2 & \alpha_m \BI & -\alpha_m \BI 
  \end{bmatrix}\right) \BX = 0
\Eeq

For a self-excitation $\omega$ and an eigenvector $\BX$, we can write
\Beq
  \label{eq:modal_dual_1}
  \BKbar_1 \BX_1 + \BKbar_{12} \BX_2 + \beta \BB_1^T (\lambda_1 + \lambda_2) = 0
\Eeq
\Beq
  \label{eq:modal_dual_2}
  \BKbar_{12} \BX_1 + \BKbar_{2} \BX_2 + \beta \BB_2^T (\lambda_1 + \lambda_2) = 0
\Eeq
\Beq
  \label{eq:modal_dual_3}
  \beta (\BB_1 \BX_1 + \BB_2 \BX_2) -\alpha  (\lambda_1 - \lambda_2) = 0
\Eeq
\Beq
  \label{eq:modal_dual_4}
  \beta (\BB_1 \BX_1 + \BB_2 \BX_2) + \alpha  (\lambda_1 - \lambda_2) = 0
\Eeq

The system $(S)$ is of order $(n + 2p)$ if $n$ is the number of physical dofs and
$p$ is the number of kinematic constraint relations.

The characteristic polynomial is $\omega^2$ is a priori of degree $n + 2p$, and its
higher order terms have the form
\Beq
  \Prod_{i=1}^n (-m_i)(\alpha_m)^{2p)
\Eeq
where $m_i$ is the $i$-th diagonal term of $\BM$.

* One thus sees that if $\alpha_m \ne 0$, the higher order terms are non-zero and the
dualized system has more eigenvalues ($n+2p$) than the reduced system ($n-pr$).
Therefore the two sytems are not equivalent (see Example below).

* If we choose $\alpha_n = \beta_n = 0$, then from \ref{eq:modal_dual_3} and
  \ref{eq:modal_dual_4}, we have
  \Beq
     \lambda_1 = \lambda_2 ~,~~ \BX_1 = -\BB_1^{-1} \BB_2 \BX_2
  \Eeq
  from \ref{eq:modal_dual_1}, we have
  \Beq
     \lambda_1 = \lambda_2 = -\frac{1}{2\beta} (-\BKbar_1 \BB_1^{-1} \BB_2 + \BKbar_{12}) \BX_2
  \Eeq
  and from \ref{eq:modal_dual_2}, we have
  \Beq
     -\BKbar_{12} \BB_1^{-1} \BB_2 \BX_2 + \BKbar_{2}) \BX_2
     -\BB_2^T\BB_1^{-T}(-\BKbar_{1} \BB_1^{-1} \BB_2 + \BKbar_{12}) \BX_2 = 0
  \Eeq
  Therefore,
  \Beq
     (\BKtilde_2 - \omega^2 \BMtilde_2)\BX_2 = 0
  \Eeq
  where
  \Beq
    \Bal
       \BKtilde_2 & = -\BKbar_{12} \BB_1^{-1} \BB_2 + \BKbar_{2}) 
         + \BB_2^T\BB_1^{-T}\BKbar_{1} \BB_1^{-1} \BB_2 -
         - \BB_2^T\BB_1^{-T}\BKbar_{12})  \\
       \BMtilde_2 & = -\BMbar_{12} \BB_1^{-1} \BB_2 + \BMbar_{2}) 
         + \BB_2^T\BB_1^{-T}\BMbar_{1} \BB_1^{-1} \BB_2 -
         - \BB_2^T\BB_1^{-T}\BMbar_{12})  
    \Eal
  \Eeq

  Note that these definitions are  identical to those in
  \ref{eq:modal_constraint_4} and \ref{eq:modal_constraint_5}. 

  If is thus seen that any eigenvector $\BX$ of the dualized system is also an
  eigenvector of the reduced system (with the same harmonic forcing) if one
  projects it on the space $\BU_2$.

  Reciprocally, any eigenvector $\BX_2$ of the reduced problem can be extended into
  one for the dualized system using $\BX^T = [\BX_1^T, \BX_2^T, \lambda_1^T, \lambda_2^T]$.
  with
  \Beq
    \Bal
       \BX_1 & = -\BB_1^{-1}\BB_2\BX_2
       \lambda_1 & = \frac{1}{2\beta} \BB_1^{-T}(-\BKbar_1 \BB_1^{-1} \BB_2 + 
                       \BKbar_{12}) \BX_2 \\
       \lambda_2 & = \frac{1}{2\beta} \BB_1^{-T}(-\BKbar_1 \BB_1^{-1} \BB_2 + 
                       \BKbar_{12}) \BX_2 \\
    \Eal
  \Eeq
  The two systems are therefore equivalent; they have the same eigenmodes and eigenvalues,
  The dualized system does not have more eigenvalues than the reduced system.

  Add note here.

\subsection{Example}

  Consider the spring-mass system shown below: (add figure here)


  This system has the stiffness and mass matrices
  \Beq
    \BK = \begin{bmatrix} k & -k \\ -k & k \end{bmatrix} ~,~~
    \BM = \begin{bmatrix} m & 0 \\ 0 & m \end{bmatrix} 
  \Eeq

  Let us add the constraint $\alpha u_1 + \beta u_2 = 0$ with $\alpha \ne 0$, i.e.,
  $\gamma = \beta/\alpha$.

  The reduced system is then:
  \Beq
    \Bal
       & \BK_1 = k ~,~~ \BK_2 = k ~,~~ \BK_{12} = -k ~,~~ \\
       & \BM_1 = m ~,~~ \BM_2 = m ~,~~ \BM_{12} = 0 ~,~~ \\
       & \BB_1 = \alpha ~,~~ \BB_2 = \beta
    \Eal
  \Eeq

  Therefore,
  \Beq
     \BKtilde_2 = k(1 + \gamma)^2 ~,~~
     \BMtilde_2 = m(1 + \gamma)^2 
  \Eeq
  which leads to
  \Beq
     \omega^2 = \frac{k}{m} \frac{(1+\gamma)^2}{1+\gamma^2) ~,~~ \BX_2 = 1
  \Eeq
  Some special cases are:
  
  * If $\gamma = 0$, $\omega^2 = k/m$.

  * If $\gamma = 1$, $\omega^2 = 2k/m$.

  * If $\gamma \rightarrow \infty 1$, $\omega^2 \rightarrow  2k/m$.

  Let us choose $\alpha = \beta = 1$ to simplif writing the dualized system:

  \Beq
    \left(
      \begin{bmatrix}
         k & -k & \beta_k & \beta_k \\
        -k &  k & \beta_k & \beta_k \\
         \beta_k & \beta_k & -\alpha_k &  \alpha_k \\
         \beta_k & \beta_k &  \alpha_k & -\alpha_k 
      \end{bmatrix}
      - \omega^2 
      \begin{bmatrix}
         m &  0 & \beta_m & \beta_m \\
         0 &  m & \beta_m & \beta_m \\
         \beta_m & \beta_m & -\alpha_m &  \alpha_m \\
         \beta_m & \beta_m &  \alpha_m & -\alpha_m 
      \end{bmatrix}
    \right)
    \begin{bmatrix}
      x_1 \\ x_2 \\ \lambda_1 \\ \lambda_2
    \end{bmatrix}
    = 0
  \Eeq

  The eigenvalues of this system are
  \Beq
    \omega^2 = \left\{ \frac{\beta_k}{\beta_m}, \frac{\beta_k}{\beta_m}, \frac{\beta_k}{\beta_m}, 
                       \frac{2k}{m} \right\}
  \Eeq
  Therefore, we find one real eigenvalue and three spurious ones because $\alpha_m$ and $\beta_m$
  are non-zero.

  If one chooses $\alpha_m = \beta_m = 0$, then the only solution is $\omega^2 = 2k/m$s, which
  is the desired solution.

\subsection{Conclusions}

  If 

  * $\BK$ and $\BM$ are the stiffness and mass matices of a non-constrained system

  * and the linear constraints can be written as
    \Beq
       \begin{bmatrix} \BB_1 & \BB_2 \end{bmatrix}
       \begin{bmatrix} \BU_1 \\ \BU_2 \end{bmatrix} = 0
    \Eeq
    with $\BB_1$ invertible
  
  then

  * the eigenmodes of the forced structure are those of the reduced system
  \Beq
     (\BKtilde_2 - \omega^2 \BMtilde_2)\BX_2 = 0
  \Eeq
  where
  \Beq
    \Bal
       \BKtilde_2 & = -\BKbar_{12} \BB_1^{-1} \BB_2 + \BKbar_{2}) 
         + \BB_2^T\BB_1^{-T}\BKbar_{1} \BB_1^{-1} \BB_2 -
         - \BB_2^T\BB_1^{-T}\BKbar_{12})  \\
       \BMtilde_2 & = -\BMbar_{12} \BB_1^{-1} \BB_2 + \BMbar_{2}) 
         + \BB_2^T\BB_1^{-T}\BMbar_{1} \BB_1^{-1} \BB_2 -
         - \BB_2^T\BB_1^{-T}\BMbar_{12})  
    \Eal
  \Eeq

  * the dualized system (double Lagrange) is written
  \Beq
     (\BKhat - \omega^2 \BMhat)\BXhat = 0
  \Eeq
  with
  \Beq
    \Bal
      \BXhat^T & = \begin{bmatrix}
                      \BX_1 & \BX_{12} & \lambda_1 & \lambda_2
                   \end{bmatrix} \\
      \BKhat & = \begin{bmatrix}
                      \BK_1 & \BK_{12} & \BB_1^T & \BB_1^T \\
                      \BK_{12}^T & \BK_{2} & \BB_2^T & \BB_2^T \\
                      \BB_1 & \BB_2 & -\BI & \BI \\
                      \BB_1 & \BB_2 & \BI & -\BI 
                   \end{bmatrix} \\
      \BMhat & = \begin{bmatrix}
                      \BM_1 & \BM_{12} & 0 & 0 \\
                      \BM_{12}^T & \BM_{2} & 0 & 0 \\
                      0 & 0 & 0 & 0 \\
                      0 & 0 & 0 & 0 
                   \end{bmatrix} 
    \Eal
  \Eeq
  and has the same solutions as the reduced system.
