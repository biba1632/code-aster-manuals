
# coding: utf-8

import gmsh
import sys
import math

gmsh.initialize()

# Use OpenCASCADE kernel
model = gmsh.model
occ = model.occ
mesh = model.mesh
model.add("ssnv209k")

# Default element size and discretization
el_size = 0.2
NX = 64
NY = 25

# Geometry
LX = 0.08
LYBase = 0.01
LYPlate = 0.04
H = 0.001

# Add points, lines, and surface
p1 = occ.addPoint(0, (-1.0*LYBase), 0, el_size)
p2 = occ.addPoint(LX, (-1.0*LYBase), 0, el_size)
p3 = occ.addPoint(LX, LYPlate, 0, el_size)
p4 = occ.addPoint(0, LYPlate, 0, el_size)
ptop = occ.addPoint(LX/2.0, LYPlate, 0, el_size)

line1 = occ.addLine(p1, p2)
line2 = occ.addLine(p2, p3)
line31 = occ.addLine(p3, ptop)
line32 = occ.addLine(ptop, p4)
line4 = occ.addLine(p4, p1)

wBase = occ.addCurveLoop([line1, line2, line31, line32, line4])
sBase = occ.addPlaneSurface([wBase])

occ.synchronize()

# For transfinite interpolation (base)
for curve in [line1]:
    mesh.setTransfiniteCurve(curve, NX+1, meshType = "Progression", coef = 1)
for curve in [line2, line4]:
    mesh.setTransfiniteCurve(curve, NY+1, meshType = "Progression", coef = 1)
for curve in [line31, line32]:
    mesh.setTransfiniteCurve(curve, int(NX/2)+1, meshType = "Progression", coef = 1)

mesh.setTransfiniteSurface(sBase, arrangement = "AlternateLeft", cornerTags = [p1, p2, p3, p4])

# mesh.setRecombine(2, sBase)

# Physical groups for the points
p1_g = model.addPhysicalGroup(0, [p1])
p2_g = model.addPhysicalGroup(0, [p2])
p3_g = model.addPhysicalGroup(0, [p3])
p4_g = model.addPhysicalGroup(0, [p4])
ptop_g = model.addPhysicalGroup(0, [ptop])

# Physical groups for the edges
line1_g = model.addPhysicalGroup(1, [line1])
line2_g = model.addPhysicalGroup(1, [line2])
line3_g = model.addPhysicalGroup(1, [line31, line32])
line4_g = model.addPhysicalGroup(1, [line4])

# Physical groups for the faces
sBase_g = model.addPhysicalGroup(2, [sBase])

# Set names of groups
model.setPhysicalName(0, p1_g, 'p1')
model.setPhysicalName(0, p2_g, 'p2')
model.setPhysicalName(0, p3_g, 'p3')
model.setPhysicalName(0, p4_g, 'p4')
model.setPhysicalName(0, ptop_g, 'ptop')

model.setPhysicalName(1, line1_g, 'line1')
model.setPhysicalName(1, line2_g, 'line2')
model.setPhysicalName(1, line3_g, 'line3')
model.setPhysicalName(1, line4_g, 'line4')

model.setPhysicalName(2, sBase_g, 'surf')

occ.synchronize()

#gmsh.option.setNumber('Mesh.Algorithm', 8)
gmsh.option.setNumber('Mesh.RecombinationAlgorithm', 1)
gmsh.option.setNumber('Mesh.ElementOrder', 1)
gmsh.option.setNumber('Mesh.MshFileVersion', 2.2)
gmsh.option.setNumber('Mesh.MedFileMinorVersion', 0)
gmsh.option.setNumber('Mesh.SaveAll', 0)
gmsh.option.setNumber('Mesh.SaveGroupsOfNodes', 1)

# Generate mesh
mesh.generate(3)

# Save mesh
gmsh.write("ssnv209k_upd.med")

gmsh.fltk.run()

gmsh.finalize()


