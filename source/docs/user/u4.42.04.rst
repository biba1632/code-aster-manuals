﻿.. role:: red
.. role:: blue
.. role:: green
.. role:: gray
.. role:: python(code)
   :language: python
.. |br| raw:: html

   <br/ >

.. index:: ! DEFI_CABLE_BP

.. _u4.42.04:

************************************************************************************
**[U4.42.04]** : Macro ``DEFI_CABLE_BP`` (:gray:`DEFINE_CABLE_PRESTRESSED_CONCRETE`)
************************************************************************************

.. contents:: Contents
   :depth: 3

.. _u4.42.04.0:

Summary
=======

The objective of this command is to calculate the initial tension profile along the prestressing cables 
of a concrete structure. The calculation inputs are the tension applied to the ends and other 
characteristic parameters of the anchorages and materials. The relationships used for the calculations
are those prescribed by ``BPEL 91`` or by the ``ETCC``. It also is used to create kinematic 
relations between cable nodes and the concrete nodes in which it is immersed. 

This command outputs a data structure of type :green:`cabl_precont`. 

The output concept :green:`cabl_precont` can then be used: 

* either by the operator :red:`AFFE_CHAR_MECA` :ref:`[U4.44.01] <u4.44.01>` with operand 
  :blue:`RELA_CINE_BP` to define the mechanical loads related to the presence of the cable (kinematic 
  relations and/or tension in the cables). These loads will be taken into account by the operator 
  :red:`STAT_NON_LINE` :ref:`[U4.51.03] <u4.51.03>`; 

* occasionally by the operator :red:`CALC_PRECONT` :ref:`[U4.42.05] <u4.42.05>` for the sequential 
  tensioning of the cables and the possibility of phasing; 

* also by the operator :red:`DEFI_GROUP` (:blue:`CREA_GROUP_NO/OPTION = 'RELA_CINE_BP'`) to create 
  node groups corresponding to the coupling relations created and to allow a visual checking of 
  these kinematic relations. 

.. _u4.42.04.1:

Syntax 
======

.. code:: 

   cabl_pr [cabl_precont] = DEFI_CABLE_BP 
   ( 
    
    ♦ MODELE = model,                                     [modele] 
    ♦ CHAM_MATER = mat_fld,                               [cham_mater] 
    ♦ CARA_ELEM = elem_prop,                              [cara_elem] 
    ♦ GROUP_MA_BETON = el_grp_conc,                       [gr_maille] 
    ♦ TYPE_ANCRAGE = / 'ACTIF', 
                     / 'PASSIVE', 
    ♦ ADHERENT = / 'OUI'                                  [DEFAULT] 
                 / 'NON' 
    ♦ TENSION_INIT = f0,                                  [R] 
    ♦ RECUL_ANCRAGE = delta,                              [R] 

    # If ADHERENT = 'NON' 

    ♦ / DEFI_CABLE = _F( 
                        ♦ / GROUP_MA = el_grp_cab,        [gr_maille] 
                            GROUP_NO_ANCRAGE = l_gnoa,    [l_gr_noeud] 
                       ) 

    # End ADHERENT = 'NON' 

    # If ADHERENT = 'OUI' 

    ◊ TYPE_RELAX = / 'SANS',                              [DEFAULT] 
                   / 'BPEL', 
                   / 'ETCC_DIRECT', 
                   / 'ETCC_REPRISE', 

    ◊ / DEFI_CABLE = _F( 
                        ◊ / GROUP_MA = el_grp_cab,        [gr_maille] 
                            GROUP_NO_ANCRAGE = l_gnoa,    [l_gr_noeud] 
                          / TABL_CABLE = table,           [table_*] 
                       )  

    ◊ / MODI_CABLE_RUPT = _F( 
                             ♦ GROUP_MA = el_grp_cab,     [gr_maille] 
                             ♦ GROUP_NO_ANCRAGE = l_gnoa, [l_gr_noeud] 
                             ♦ TENSION = table, 
                            )  

    # If TYPE_RELAX = 'ETCC_REPRISE' 

    ◊ / MODI_CABLE_ETCC = _F( 
                             ♦ GROUP_MA = el_grp_cab,     [gr_maille] 
                             ♦ GROUP_NO_ANCRAGE = l_gnoa, [l_gr_noeudSION] 
                             ♦ TENSION = table            [table_*] 
                             ♦ NBH_RELAX = nbh,           [R] 
                            )  

    # End TYPE_RELAX = 'ETCC_REPRISE' 

    # If TYPE_RELAX = 'ETCC_DIRECT' 

    ♦ NBH_RELAX = nbh,                                    [R] 

    # End TYPE_RELAX = 'ETCC_DIRECT' 

    # If TYPE_RELAX = 'BPEL' 

    ♦ R_J = rj,                                           [R] 

    # End TYPE_RELAX = 'BPEL ' 

    ◊ CONE = _F( 
                ♦ RAYON = radius,                         [R] 
                ♦ LONGEUR = long,                         [R] 
                ♦ PRESENT = l_pre,                        [l_tx]
               ) 

    # End ADHERENT = 'OUI' 

    ◊ TIRLE = l_titr,                                     [l_tx] 
   ) 

.. _u4.42.04.2:

Operands 
========

.. _u4.42.04.2.1:

Operand :blue:`MODELE` (:gray:`MODEL`)
--------------------------------------

::

  ♦ MODELE =  model

Concept output by the operator :red:`AFFE_MODELE` :ref:`[U4.41.01] <u4.41.01>` used to define the 
types of finite elements assigned to the elements of the mesh. 

.. _u4.42.04.2.2:

Operand :blue:`CHAM_MATER` (:gray:`MATERIAL_FIELD`)
----------------------------------------------------

::

  ♦ CHAM_MATER = mat_fld 

Concept output by the operator :red:`AFFE_MATERIAU` :ref:`[U4.43.02] <u4.43.02>` used to assign 
materials to the elements of the mesh. 

.. _u4.42.04.2.3:

Operand :blue:`CARA_ELEM` (:gray:`ELEMENT_PROPERTIES`)
-------------------------------------------------------

::

    ♦ CARA_ELEM = el_char 

Concept output by the operator :red:`AFFE_CARA_ELEM` :ref:`[U4.42.01] <u4.42.01>` used to assign 
mechanical and geometrical characteristics to the elements of the studied structure. 

.. _u4.42.04.2.4:

Operand :blue:`GROUP_MA_BETON` (:gray:`ELEMENT_GROUP_CONCRETE`)
----------------------------------------------------------------

::

    ♦ GROUP_MA_BETON = el_grp_conc 

Name of the element groups of the mesh representing the concrete structure. One thus precisely defines 
the geometrical locations for the projection of the cables, which is the first step for the determination 
of the kinematic relations between the nodal DOFs of the cables and the nodal DOFs of concrete structure. 

.. admonition:: Remarks 

   * It is possible to provide here a list of element groups, in particular for the case where all 
     the concrete elements surrounding the cable do not have the same behavior. However, it is essential 
     that all the concrete elements concerned have the same :blue:`BPEL_BETON` characteristics. 
   * The elements modeling the concrete can only be 3D elements or 2D elements in the case of a 
     plate model (:blue:`DKT, Q4GG`).  

.. _u4.42.04.2.5:

Definition of the cable 
------------------------

.. _u4.42.04.2.5.1:

Keyword :blue:`DEFI_CABLE` [DEFAULT] 
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

  DEFI_CABLE 

Factor keyword used to define a cable by designation of the topological entities of the mesh which 
represent it. Multiple occurrences are allowed, so that multiple cables can be defined. The cables can 
be defined individually or via an array (see test :green:`SSNV229A` :ref:`[V6.04.229] <v6.04.229>`).
 
::

   / ♦ GROUP_MA = el_grp_cab 

Name of the element group of the mesh representing the cable. 

::

     ♦ GROUP_NO_ANCRAGE = l_gnoa 

List of node groups defining the anchors. The cardinality of this list must be less than or equal to 2. 
In each node group, ``Code_Aster`` will retain for anchor only the first node of the group. 

::

   / ♦ TABL_CABLE = table 

When the number of cables is important, it can be easier to have them defined via a table containing 
3 columns, defining the element groups of the cables and the name of the 2 node groups defining 
the anchorages. The columns must be named :green:`GROUP_MA`, :green:`GROUP_NO1`, :green:`GROUP_NO2`.  

This table can be defined outside ``Code_Aster`` (``Excel, Python``) and read with command 
:red:`LIRE_TABLE`. 

.. admonition:: Example 

   ::

     # LIST of CABLES 
     GROUP_MA GROUP_NO1 GROUP_NO2 
     CAB1 CAB1_I CAB1_F 
     CAB3 CAB3_I CAB3_F 
     ....

.. _u4.42.04.2.5.2:

Keyword :blue:`MODI_CABLE_ETCC`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

    ◊ MODI_CABLE_ETCC 

Factor keyword used to define a cable by designation of the topological entities of the mesh 
which represent it and providing for each of the cables the initial short-term tension in order to 
allow calculation of the relaxation losses of the steel according to the ``ETCC`` (adherent/no-slip 
cable only). Multiple occurrences are allowed, so that multiple cables can be defined. To use it, it 
is necessary to have specified :blue:`TYPE_RELAX='ETCC_REPRISE'`. 

::

    ♦ GROUP_MA = el_grp_cab 

Name of the element group of the mesh representing the cable. 

::

    ♦ GROUP_NO_ANCRAGE = l_gnoa 

List of node groups defining the anchors. The cardinality of this list must be less than or equal to 2. 
In each node group, ``Code_Aster`` will retain for anchor only the first node of the group. 

::

    ♦ TENSION = table 

Table containing the tension in the cable for all the arc-length coordinates (curvilinear X-coordinates)
(table with 2 columns). This table is used to calculate the relaxation loss of steel if the user has
chosen the option :blue:`TYPE_RELAX = 'ETCC_REPRISE'`. This array is obtained via a 
:red:`POST_RELEVE_T` on the nodes of the studied cable, following a static computation.  

.. _u4.42.04.2.5.3:

Keyword :blue:`MODI_CABLE_RUPT` 
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

   ◊ MODI_CABLE_RUPT 

Factor keyword used to define a cable by designation of the topological entities of the mesh which 
represent it. This keyword is used to circumvent the calculation of tension by recovering the 
table provided under the keyword :blue:`TENSION`. The technique is used to simulate the rupture 
of a cable with re-anchor (adherent/no-slip cable only). 

Multiple occurrences are allowed, so that multiple cables can be defined. 

::

     ♦ GROUP_MA = el_grp_cab 

Name of the element group of the mesh representing the cable. 

::

     ♦ GROUP_NO_ANCRAGE = l_gnoa 

List of node groups defining the anchors. The cardinal of this list must be less than or equal to 2. 
In each node group, ``Code_Aster`` will retain for anchor only the first node of the group. 

::

     ♦ TENSION = table 

Table containing the tension in the cable for all the arc-length coordinates (curvilinear abscissas) 
(table with 2 columns) and which will become the tension of reference. This array is obtained via a 
:red:`POST_RELEVE_T` on the nodes of the cable following a computation, and then modified as the user wishes. 


.. _u4.42.04.2.6:

Operand :blue:`ADHERENT` 
------------------------

::

    ♦ ADHERENT = / 'OUI' [DEFAULT] 
                 / 'NON' 

This keyword is used to indicate if one wishes to model cables that adhere (no-slip) to the concrete 
(:blue:`ADHERENT = 'OUI'`) or sliding or frictional cables (:blue:`ADHERENT = 'NON'`). For the non-adherent 
cases, it is necessary to use :blue:`CABLE_GAINE` to represent the steel model and the constitutive law 
:blue:`CABLE_GAINE_FROT`. The adherent case can be simulated with models :blue:`BARRE` or 
:blue:`CABLE_GAINE`. 

.. note::
 
   In the case :blue:`ADHERENT = 'NON'`, the profile of tension is calculated even if it is not used 
   thereafter (neither in :red:`AFFE_CHAR_MECA`/:blue:`RELA_CINE_BP`, nor in :red:`CALC_PRECONT`). 

.. _u4.42.04.2.7:

Operand :blue:`TYPE_ANCRAGE` 
----------------------------

::

  ♦ TYPE_ANCRAGE = l_tya 

List of arguments of the type text characterizing the anchors of the cable: :blue:`'ACTIF'` or 
:blue:`'PASSIF'` (only valid arguments). This list must comprise 2 arguments, neither more nor less, 
and must be ordered in the same order as the list of the nodes defining the anchors (operand 
:blue:`GROUP_NO_ANCRAGE` above). It should be noted that if several cables are defined in 
:red:`DEFI_CABLE` then the first argument of :blue:`TYPE_ANCRAGE` applies to all the first nodes 
which define the anchorages. Same for the second argument. 

.. admonition:: Remarks 

   * The operator reconstitutes the connected path leading from the first to the second anchor of 
     the cable by traversing the elements which represent it. The non-existence of a connected path 
     between the two anchors causes the program to stop with a fatal error. 
   * Active anchorages are those where an initial tension is applied. 

.. _u4.42.04.2.8:

Operand :blue:`TENSION_INIT` 
----------------------------

::

  ♦ TENSION_INIT = f0 

Value of the initial force applied to the active anchorages of the cables. 
This value must be positive. 

.. _u4.42.04.2.9:

Operand :blue:`RECUL_ANCRAGE` 
-----------------------------

::

  ♦ RECUL_ANCRAGE = delta 

Value of the shrinkage of the active anchors of the cables. 
This value must be positive. 

.. _u4.42.04.2.10:

Choice of the type of calculation for the relaxation of steels 
---------------------------------------------------------------

::

    ◊ TYPE_RELAX = / 'SANS', [DEFAULT] 
                   / 'BPEL', 
                   / 'ETCC_DIRECT', 
                   / 'ETCC_REPRISE', 

Keyword used to define whether one takes into account the tension loss due to relaxation of the steel 
by the regulatory method, and if so which one (``ETCC`` or ``BPEL``) is to be used. This keyword factor 
is optional, and the default situation is that the loss of tension by relaxation of the steel are not 
taken into account. 

.. _u4.42.04.2.10.1:

:blue:`TYPE_RELAX='SANS'`
^^^^^^^^^^^^^^^^^^^^^^^^^^

This is the default value. In this case, this type of loss is not taken into account by a regulatory 
method. On the other hand, one can take it into account by using the viscoelastic constitutive law 
:blue:`RELAX_ACIER` for the cable (cf :ref:`[R5.03.09] <r5.03.09>`). 

.. _u4.42.04.2.10.2:

:blue:`TYPE_RELAX='BPEL'` 
^^^^^^^^^^^^^^^^^^^^^^^^^^

In this case, the ``BPEL`` formula is applied, and the Keyword :blue:`R_J` must be indicated (cf 
:numref:`u4.42.04.2.11` for details). 

.. _u4.42.04.2.10.3:

:blue:`TYPE_RELAX='ETCC_DIRECT'` 
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

In this case, the ``ETCC`` formula is applied, but the force used takes into account only the friction 
losses and losses from anchor shrinkage (cf :numref:`u4.42.04.4`).  

It is necessary to input keyword :blue:`NBH_RELAX` (cf :numref:`u4.42.04.2.12`).  

.. _u4.42.04.2.10.4:

:blue:`TYPE_RELAX='ETCC_REPRISE'` 
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

In this case, the exact ``ETCC`` formula is applied. This involves making an initial calculation 
without relaxation and recovering the tension in the cable. In this case it is advisable to define the cable 
using :blue:`MODI_CABLE_ETCC` instead of :blue:`DEFI_CABLE`.  

It is necessary to input Keyword :blue:`NBH_RELAX` (cf :numref:`u4.42.04.2.12`).  

.. _u4.42.04.2.11:

Operand :blue:`R_J` 
--------------------

::

    ◊ R_J = rj 

Parameter to be input only if :blue:`TYPE_RELAX='BPEL'`. 

Value of the non-dimensional function :math:`r(j)` characterizing the evolution of the relaxation of 
steel in time and only of steel (this coefficient does not apply to the losses by creep or shrinkage 
of the concrete indicated under :blue:`BPEL_BETON` in :red:`DEFI_MATERIAU`); for example ``BPEL 91`` 
recommends: 

.. math::

   r(j) = \frac{j}{j+9\times r_m}

with :math:`j` in days and 

.. math::

   r_m = \frac{\text{area of ihe concrete sectiono}}{\text{perimeter of the concrete section}}
         \equiv \text{mean radius}

This value must be positive or zero. 

:math:`j` corresponds to the date (in days) for which we want to estimate the state of stress in the 
structure. If, in the structural analysis, the bars would be modeled with a creep model, this keyword 
should not be input in :red:`DEFI_CABLE_BP`. 

.. _u4.42.04.2.12:

Operand :blue:`NBH_RELAX` 
-------------------------

::

    ◊ NBH_RELAX = nh 

Parameter to be input only if :blue:`TYPE_RELAX='ETCC_DIRECT'` or :blue:`'ETCC_REPRISE'`. Time considered 
for taking into account the losses by relaxation of the steel in the ``ETCC``, expressed in number of hours.  

The formula applied is as follows: 

.. math::

   \Delta F_{pr} = 0.66 \rho_{1000} \exp\left[9.1 \frac{\tilde{F}(s)}{P_{pk}}\right]
        \left(\frac{nh}{1000}\right)^{0.75(1-\tilde{F}(s)/P_{pk})}
        \times 10^{-5}\tilde{F}(s) 

where: 

* :math:`\rho_{1000}` is the value of the relaxation coefficient of steel at 1000 hours in %, input 
  under :blue:`ETCC_ACIER`. 
* :math:`P_{pk}` is the maximum tension of the steel at rupture, calculated starting from the data of 
  :blue:`F_PRG` under :blue:`ETCC_ACIER`. 
* :math:`\tilde{F}` is equal to the tension calculated according to the ``ETCC`` by taking into account 
  the losses by friction and by shrinkage of the anchor if :blue:`TYPE_RELAX=' ETCC_DIRECT'` or the 
  tension provided by the user under :blue:`MODI_CABLE_ETCC`/:blue:`TENSION` in the other case. 
  
.. _u4.42.04.2.13:

Keyword :blue:`CONE` 
---------------------

::

  ♦  CONE 

This factor Keyword is used to define a geometric volume around the anchorages, and to assign to
the keyword :blue:`RELA_CINE_BP` of :red:`AFFE_CHAR_MECA`  with all the nodes (concrete and cable) contained 
in this volume, a :blue:`LIAISON_SOLIDE` kinematic relation (rigid body). The definition of this volume is 
used to attenuate the stresses generated by the tensions at the ends of the cables on the concrete. In reality, 
this phenomenon is avoided thanks to the installation of a stress diffusion cone (harder material than concrete) 
which distributes the prestressing force over a large surface of the concrete. In practice, the cone being 
practically straight, a cylindrical volume is defined. 
  
.. _fig_u4.42.04.1:

.. figure:: u4.42.04/fig1_u4.42.04.svg
   :height: 250 px
   :align: center

   Use of a diffusion cone: actual situation and modeling 

Note that several cones and therefore several rigid blocks are defined if the keyword :blue:`PRESENT` 
contains two :blue:`'OUI'` (one block per end of the cable) or if several cables are defined under 
:red:`DEFI_CABLE`. 

.. admonition:: Remark: 

   In practice, the cylinder is defined with the command :red:`DEFI_GROUP` option :blue:`TUNNEL`. The 
   methodology of extraction of the nodes contained in the cone is described in the document 
   :ref:`[U4.22.01] <u4.22.01>` (command :red:`DEFI_GROUP`). 

::

    ♦ RAYON = radius 

Radius of the cone. 

::

    ♦ LONGEUR = length 

Length of the cone, in the arc-length coordinates (curvilinear abscissa direction) on the cable. The cone is 
defined as a succession of cylinders, stopping when the total length of the cylinders is equal to the 
parameter :green:`length`. 

::

    ♦ PRESENT = l_pre 
 
This list must include 2 arguments, neither more nor less, and must be ordered with regard to the list of 
the nodes defining the anchors (operand :blue:`GROUP_NO_ANCRAGE` above). 

The only valid arguments are :blue:`'OUI'` or :blue:`'NON'`; they make it possible to define the cone on 
the two anchors (:blue:`PRESENT = ('OUI', 'OUI')`), on the first anchor (:blue:`PRESENT = ('OUI', 'NON')`) 
or on the second anchor (:blue:`PRESENT = ('NON', 'OUI')`). It should be noted that if several cables are 
defined in :red:`DEFI_CABLE` then the first argument of :blue:`PRESENT` applies to all the first nodes 
which define the anchorages. Same for the second argument. 

.. _u4.42.04.2.14:

Operand :blue:`TITRE`
---------------------

::

    ◊ TITRE = l_titr 

List of arguments of text type defining a title attached to the concept :green:`[:cabl_precont]`. 

.. _u4.42.04.3:

Theoretical appendix: estimation of tension losses in a prestressing cable according to the BPEL prescriptions 
===============================================================================================================

If the user has defined the materials :blue:`BPEL_BETON` and :blue:`BPEL_ACIER`, the evolution of the tension 
(in Newtons) along a prestressing cable is calculated by using the relations prescribed by ``BPEL``. These 
relationships are as follows. 

.. _u4.42.04.3.1:

Evolution of the tension in the vicinity of the anchorage 
--------------------------------------------------------------

The tension is written:  

.. math::
   :label: eq_u4.42.04.1

   F(s) = \tilde{F}(s)- \left[x_{\text{flu}} \times F_0 + x_{\text{ret}} \times F_0 + 
            r(j) \times \frac{5}{100} \times \rho_{1000} 
              \left( \frac{\tilde{F}(s)}{ S_a\times \sigma_y}-\mu_0\right)\times \tilde{F}(s)\right] 

where :math:`s` designates the arc-length coordinate (curvilinear abscissa) along the cable. The parameters 
introduced in this expression are: 

* :math:`F_0` : initial tension (N); 
* :math:`x_{\text{flu}}`:  rate of loss of tension by creep of the concrete, compared to the initial tension; 
* :math:`x_{\text{ret}}`:  rate of loss of tension by shrinkage of the concrete, compared to the initial tension; 
* :math:`\rho_{1000}`:  relaxation of steel at 1000 hours, expressed in %; 
* :math:`S_a`: area of the cross-section of the cable defined in :red:`AFFE_CARA_ELEM` ; 
* :math:`\sigma_y` : yield stress of steel; 
* :math:`\mu_0` : dimensionless coefficient of relaxation of prestressed steel. 
* :math:`r(j)` is a dimensionless function characterizing the evolution of relaxation over time: 

  .. math::

     r(j) = \frac{j}{j+9\times r_m}

  with :math:`j` in days and 

  .. math::

     r_m = \frac{\text{area of ihe concrete sectiono}}{\text{perimeter of the concrete section}}
           \equiv \text{mean radius}

  The function :math:`r(j)` depends on the geometry of the structure and the value used is defined in the 
  operator :red:`DEFI_CABLE_BP`. 

* :math:`\tilde{F}(s)` is the evolution of the tension in the vicinity of the anchorage after taking into 
  account the loss by anchorage recoil and the contact losses between the cable and the concrete. 
* :math:`\tilde{F}(s)` is defined by the relation: 

  .. math::

     F_c(s) \times \tilde{F}(s) = [F_c(d)]^2 

* :math:`F_c(s)` designates the evolution of the tension along the cable after taking into account of the 
  losses due to contact between the cable and the concrete: 

  .. math::

     F_c(s) = F_0 \exp(-f \alpha-\varphi s) 

  where 

  * :math:`\alpha` designates the cumulative angular deviation and the parameters introduced into the 
    expression of :math:`Fc(s)` are: 

    * :math:`f` :  coefficient of friction between the cable and the partially curved sheath, in 
      :math:`\text{rad}^{-1}` ; 
    * :math:`\varphi` :  coefficient of friction between the cable and the sheath in :math:`\text{m}^{-1}`. 

.. admonition:: Remarks

   * The coefficients :math:`f, \varphi, \rho_{1000}, \sigma_y, \mu_0` are to be input in the operator 
     :red:`DEFI_MATERIAU` under the Keyword :blue:`BPEL_ACIER`, 
   * :math:`x_{\text{flu}}` and :math:`x_{\text{ret}}` are to be input in the operator :red:`DEFI_MATERIAU` 
     under the keyword :blue:`BPEL_BETON`. 

The length :math:`d` in the expression for :math:`\tilde{F}(s)` is the length on the loss of tension by recoil 
at the anchor is applied. This length is estimated using the relation:  

.. math::
   :label: eq_r4.42.04.2

   E_a S_a\Delta = \int_0^d ( F_c(s) - \tilde{F}(s)) ds 

where :math:`E_a` is the Young's modulus of the steel and :math:`\Delta` is the value of the recoil at the 
anchorage. Thus :math:`E_a S_a\Delta`  represents the strain energy (of the cable) due to the recoil at anchor. 

.. _u4.42.04.3.2:

Evolution of the tension beyond the length where the losses of tension by recoil apply to the anchorage 
--------------------------------------------------------------------------------------------------------

This tension is written:  

.. math::
   :label: eq_r4.42.04.3

   F(s) = F_c(s) - \left[x_{\text{flu}} \times F_0 + x_{\text{ret}} \times F_0 + 
           r(j) \times \frac{5}{100} \times \rho_{1000} 
             \left(\frac{\tilde{F}(s)}{S_a \times \sigma_y} -\mu_0 \right) \times \tilde{F}(s)\right]

with the same notations as those introduced in the previous paragraph. 

.. _u4.42.04.4:

Theoretical appendix: estimation of tension losses in a prestressing cable according to the ``ETCC`` prescriptions
===================================================================================================================

If the user has defined the materials :blue:`ETCC_BETON` and :blue:`ETCC_ACIER`, the evolution of the tension 
(in Newtons) along a prestressing cable is calculated by using the relations prescribed by the ``ETCC``. However, 
the losses due to the elastic strain of the concrete and to the strains due to shrinkage and creep 
of the concrete are not taken into account in the calculation. It is advisable if one wants to take them into 
account: 

* to tension the cables in 2 families to recover the losses due to the elastic strains of the concrete 
* to impose on the concrete the strains due to shrinkage and creep, after the tensioning of the cables. 

The established relationships are as follows.  

.. _u4.42.04.4.1:

Evolution of the tension in the vicinity of anchor 
--------------------------------------------------

This is written: 

.. math::
   :label: eq_r4.42.04.4

   F(s) = \tilde{F}(s) - 0.8 \Delta F_{pr}(s)

where

* :math:`F(s)` is the evolution of the tension in the vicinity of the anchorage after taking into 
  account the loss due to anchorage recoil and the contact losses between the cable and the concrete. 
* :math:`\tilde{F}(s)` is defined by the relation: 

  .. math::

     F_c(s) \times \tilde{F}(s) = [ F_c(d )]^2 

* :math:`F_c(s)` indicates the evolution of the tension along the cable after accounting for the losses 
  due to contact between the cable and the concrete: 

  .. math::

     F_c(s) = F_0 \exp[-\mu(\alpha+ ks)] 

  where :math:`\alpha` is the cumulated angular deviation and the parameters introduced into the expression 
  of  :math:`F_c(s)` are: 

  * :math:`\mu [\text{rad}^{-1}]`:  the coefficient of friction between the cable and the sheath; 
  * :math:`k [\text{rad}^{-1} . \text{m}^{-1}]` :  the line loss coefficient. 

The length :math:`d` that appears in the expression for :math:`\tilde{F}(s)` is the length to which the 
loss of tension by recoil to the anchorage applies. This length is estimated using the relationship:  

.. math::
   :label: eq_r4.42.04.5

   E_a S_a \Delta = \int_0^d ( F_c(s)-\tilde{F}(s)) ds 

where 

* :math:`E_a` is the Young's modulus of steel and :math:`\Delta` is the value of the recoil at the anchor. 
  Thus :math:`E_a S_a \Delta` represents the strain energy (of the cable) due to the recoil at the anchor. 
* :math:`\Delta F_{pr}` are the losses due to cable relaxation, estimated by the following formula  

  .. math::
     :label: eq_r4.42.04.6

     \Delta F_{pr}(s) = 0.66 \rho_{1000} \exp\left[9.1 \frac{\tilde{F}(s)}{P_{pk}}\right]
        \left(\frac{nh}{1000}\right)^{0.75(1-\tilde{F}(s)/P_{pk})}
        \times 10^{-5}\tilde{F}(s) 

  where :math:`s` is the arc-length coordinate (curvilinear abscissa) along the cable. The parameters 
  introduced into this expression are: 

  * :math:`\rho_{1000}` : Relaxation of steel at 1000 hours, expressed in %. 
  * :math:`P_{pk}` :  Force at rupture in steel (calculated starting from the parameter :blue:`F_PRG` 
    indicated in :red:`DEFI_MATERIAU`/:blue:`ETCC_ACIER`) 
  * :math:`nh`:  The number of hours after the setting in prestressing with which the losses are calculated. 

.. admonition:: Remarks

   * The coefficients :math:`\mu, k, \rho_{1000}, F_{prg}` are to be input in the operator 
     :red:`DEFI_MATERIAU` under the keyword :blue:`BPEL_ACIER` , 
   * :math:`nh` is to be input in the operator :red:`DEFI_CABLE_BP` . 

.. _u4.42.04.4.2:

Evolution of the tension beyond the length where the losses of tension by recoil apply to the anchorage 
--------------------------------------------------------------------------------------------------------

We use the following relation:  

.. math::
   :label: eq_r4.42.04.7

   F(s) = F_c(s) - 0.8 \times 0.66 \rho_{1000} \exp\left[9.1 \frac{\tilde{F}(s)}{F_{prg}}\right]
            \left(\frac{nh}{1000}\right)^{0.75(1-F_c(s)/F_{prg})}
            \times 10^{-5} F_c(s) 

with the same notations as those introduced in the previous paragraph. 

.. _u4.42.04.5:

Example 
=======

See test :green:`SSNV164` :ref:`[V6.04.164] <v6.04.164>` for an implementation, and the documents 
:ref:`[U2.03.06] <u2.03.06>` and :ref:`[U4.42.05] <u4.42.05>` to know more precisely how to put 
the prestressing cables into tension. 

See test :green:`SSNV229` :ref:`[V6.04.229] <v6.04.229>` for an example of taking into account of the 
relaxation of steel according to the ``ETCC``, according to the 2 possible methods but also for the 
definition of the cables via a table. Model C is used to validate the procedure to simulate the rupture 
of a cable with anchor. 

See test :green:`ZZZZ347` :ref:`[V1.01.347] <v1.03.347>` for an example of use with the :blue:`CABLE_GAINE`
model. 
