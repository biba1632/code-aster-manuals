side_len = 1;
mesh_size = 0.5;

Point(1) = {0, 0, 0, mesh_size};
Point(2) = {side_len, 0.0, 0, mesh_size};
Point(3) = {side_len, side_len, 0, mesh_size};
Point(4) = {0, side_len, 0, mesh_size};
Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line (4) = {4, 1};
Curve Loop(106) = {2, 3, 4, 1};
Plane Surface(6) = {106};
Physical Surface(107) = {6};

Mesh.Algorithm = 2;         // Automatic
Mesh.Format = 1;            // msh format
Mesh.MshFileVersion = 2.2;  // Msh format 2.2
Mesh 2;

Save "gmsh_mesh.msh";
