#SSNP153A
DEBUT(LANG = 'EN')

# Read mesh
mesh = LIRE_MAILLAGE(UNITE = 20,
                   FORMAT = 'MED',);

# Orient edges so that norml points outward
mesh = MODI_MAILLAGE(reuse = mesh,
                     MAILLAGE = mesh,
                     ORIE_PEAU_2D = (_F(GROUP_MA = 'Master'),
                                     _F(GROUP_MA = 'Slave')))

# Define node group
mesh = DEFI_GROUP(reuse = mesh,
                  MAILLAGE = mesh,
                  CREA_GROUP_NO = _F(TOUT_GROUP_MA = 'OUI'))

# Define materials
hard = DEFI_MATERIAU(ELAS = _F(E = 68.96E8,
                               NU = 0.32),
                     ECRO_LINE = _F(D_SIGM_EPSI = 10.0,
                                    SY = 10.E100))

soft = DEFI_MATERIAU(ELAS = _F(E = 6.896E8,
                               NU = 0.32),
                     ECRO_LINE = _F(D_SIGM_EPSI = 10.0,
                                    SY = 10.E100))

# Define time step functions
time1 = 1
time2 = 2
x_func = DEFI_FONCTION(NOM_PARA = 'INST',
                       VALE = (0.0, 0.0,
                               time1,  0.0,
                               time2, 10.0),
                       PROL_DROITE = 'LINEAIRE',
                       PROL_GAUCHE = 'LINEAIRE')

y_func = DEFI_FONCTION(NOM_PARA = 'INST',
                       VALE = (0.0, 0.0,
                               time1,  1.0,
                               time2,  1.0),
                       PROL_DROITE = 'LINEAIRE',
                       PROL_GAUCHE = 'LINEAIRE')

# Define list of time step values and data saving times
n_step1 = 10
n_step2 = 500
l_times = DEFI_LIST_REEL(DEBUT = 0.0,
                         INTERVALLE = (_F(JUSQU_A = time1,
                                          NOMBRE = n_step1),
                                       _F(JUSQU_A = time2,
                                          NOMBRE = n_step2)))

l_save = DEFI_LIST_REEL(DEBUT = 0.0,
                        INTERVALLE = (_F(JUSQU_A = time1,
                                         NOMBRE = n_step1),
                                      _F(JUSQU_A = time2,
                                         NOMBRE = n_step2)))

# Define time stepping scheme
times = DEFI_LIST_INST(METHODE = 'MANUEL',
                       DEFI_LIST = _F(LIST_INST = l_times),
                       ECHEC = _F(SUBD_PAS = 4,
                                  SUBD_NIVEAU = 5))

# Assign plane strain elements to mesh
model = AFFE_MODELE(MAILLAGE = mesh,
                    AFFE = _F(TOUT = 'OUI',
                              PHENOMENE = 'MECANIQUE',
                              MODELISATION = 'D_PLAN'))

# Assign materials
mater = AFFE_MATERIAU(MAILLAGE = mesh,
                      AFFE = (_F(GROUP_MA = 'Hard',
                                 MATER = hard),
                              _F(GROUP_MA = 'Soft',
                                 MATER = soft)))

# Assign BCs
bc_x = AFFE_CHAR_CINE(MODELE = model,
                      MECA_IMPO = (_F(GROUP_MA = 'Bloq',
                                      DX = 0.0,),
                                   _F(GROUP_MA = 'Depl',
                                      DX = 1.0)))

bc_y = AFFE_CHAR_CINE(MODELE = model,
                      MECA_IMPO = (_F(GROUP_MA = 'Bloq',
                                      DY = 0.0,),
                                   _F(GROUP_MA = 'Depl',
                                      DY = -1.0)))

# Set up contact
contact = DEFI_CONTACT(MODELE = model,
                  FORMULATION = 'CONTINUE',
                  FROTTEMENT = 'COULOMB',
                  LISSAGE = 'OUI',
                  ALGO_RESO_GEOM = 'NEWTON',
                  ALGO_RESO_CONT = 'NEWTON',
                  ZONE = _F(GROUP_MA_MAIT = 'Master',
                            GROUP_MA_ESCL = 'Slave',
                            COULOMB = 0.3))

# Solve
result = STAT_NON_LINE(MODELE = model,
                       CHAM_MATER = mater,
                       EXCIT = (_F(CHARGE = bc_x,
                                   FONC_MULT = x_func),
                                _F(CHARGE = bc_y,
                                   FONC_MULT = y_func)),
                       CONTACT = contact,
                       COMPORTEMENT = _F(RELATION = 'VMIS_ISOT_LINE',
                                         DEFORMATION = 'SIMO_MIEHE'),
                       INCREMENT = _F(LIST_INST = times,
                                      INST_FIN = 1.01),
                       NEWTON = _F(PREDICTION = 'ELASTIQUE',
                                   MATRICE = 'TANGENTE',
                                   REAC_ITER = 1),
                       CONVERGENCE = _F(RESI_GLOB_MAXI = 1.E1,
                                        ITER_GLOB_MAXI = 20,),
                       SOLVEUR = _F(METHODE = 'MUMPS'),
                       ARCHIVAGE = _F(LIST_INST = l_save))

# Compute stress and stress invariants
result = CALC_CHAMP(reuse = result,
                    RESULTAT = result,
                    CRITERES = ('SIEQ_ELGA'),
                    CONTRAINTE = ('SIGM_ELNO', 'SIGM_NOEU'),
                    FORCE = ('FORC_NODA','REAC_NODA'))

# Extract noda reactions into tables
tab_n = POST_RELEVE_T(ACTION = _F(INTITULE = 'FORCE',
                                  GROUP_NO = 'Depl',
                                  RESULTAT = result,
                                  NOM_CHAM = 'REAC_NODA',
                                  RESULTANTE = 'DY',
                                  OPERATION = 'EXTRACTION'))

tab_t = POST_RELEVE_T(ACTION = _F(INTITULE = 'FORCE',
                                  GROUP_NO = 'Depl',
                                  RESULTAT = result,
                                  NOM_CHAM = 'REAC_NODA',
                                  RESULTANTE = 'DX',
                                  OPERATION = 'EXTRACTION'))

# Write tables
IMPR_TABLE(TABLE = tab_n,
           FORMAT = 'TABLEAU',
           SEPARATEUR = ',',
           UNITE = 29,
           NOM_PARA = ('INST', 'DY',),
           TITRE = 'RESULTANT N')

IMPR_TABLE(TABLE = tab_t,
           FORMAT = 'TABLEAU',
           SEPARATEUR = ',',
           UNITE = 30,
           NOM_PARA = ('INST', 'DX',),
           TITRE = 'RESULTANT T')

IMPR_TABLE(TABLE = tab_n)
IMPR_TABLE(TABLE = tab_t)

# Write results
IMPR_RESU(FORMAT = 'MED',
          UNITE = 80,
          RESU = _F(RESULTAT = result,
                  NOM_CHAM = 'DEPL',
                  NOM_CMP = ('DX','DY')))

IMPR_RESU(FORMAT = 'MED',
          UNITE = 81,
          RESU = _F(RESULTAT = result))


# Verification tests (normal force)
TEST_TABLE(VALE_CALC = -9.4538776171532E+07,
           NOM_PARA = 'DY',
           TABLE = tab_n,
           FILTRE = _F(NOM_PARA = 'INST',
                       VALE = 0.5))

TEST_TABLE(VALE_CALC = -1.345567896841E+08,
           NOM_PARA = 'DY',
           TABLE = tab_n,
           FILTRE = _F(NOM_PARA = 'INST',
                       VALE = 0.6))

TEST_TABLE(VALE_CALC = -1.7840288703549E+08,
           NOM_PARA = 'DY',
           TABLE = tab_n,
           FILTRE = _F(NOM_PARA = 'INST',
                       VALE = 0.7))

TEST_TABLE(VALE_CALC = -2.2557081658764E+08,
           NOM_PARA = 'DY',
           TABLE = tab_n,
           FILTRE = _F(NOM_PARA = 'INST',
                       VALE = 0.8))

TEST_TABLE(VALE_CALC = -2.7555438807337E+08,
           NOM_PARA = 'DY',
           TABLE = tab_n,
           FILTRE = _F(NOM_PARA = 'INST',
                       VALE = 0.9))

TEST_TABLE(VALE_CALC = -3.2989817982847E+08,
           NOM_PARA = 'DY',
           TABLE = tab_n,
           FILTRE = _F(NOM_PARA = 'INST',
                       VALE = 1.0))

TEST_TABLE(VALE_CALC = -3.3240504054728E+08,
           NOM_PARA = 'DY',
           TABLE = tab_n,
           FILTRE = _F(NOM_PARA = 'INST',
                       VALE = 1.002))

TEST_TABLE(VALE_CALC = -3.3489814402184E+08,
           NOM_PARA = 'DY',
           TABLE = tab_n,
           FILTRE = _F(NOM_PARA = 'INST',
                       VALE = 1.004))

TEST_TABLE(VALE_CALC = -3.3737758242156E+08,
           NOM_PARA = 'DY',
           TABLE = tab_n,
           FILTRE = _F(NOM_PARA = 'INST',
                       VALE = 1.006))

TEST_TABLE(VALE_CALC = -3.3984346216101E+08,
           NOM_PARA = 'DY',
           TABLE = tab_n,
           FILTRE = _F(NOM_PARA = 'INST',
                       VALE = 1.008))

TEST_TABLE(VALE_CALC = -3.4229590424525E+08,
           NOM_PARA = 'DY',
           TABLE = tab_n,
           FILTRE = _F(NOM_PARA = 'INST',
                       VALE = 1.01,),)

# Verification tests (tangential force)
TEST_TABLE(VALE_CALC = 1.8359148468034E+07,
           NOM_PARA = 'DX',
           TABLE = tab_t,
           FILTRE = _F(NOM_PARA = 'INST',
                       VALE = 0.5))

TEST_TABLE(VALE_CALC = 2.6679896853672E+07,
           NOM_PARA = 'DX',
           TABLE = tab_t,
           FILTRE = _F(NOM_PARA = 'INST',
                       VALE = 0.59999999999999998))

TEST_TABLE(VALE_CALC = 3.5989420501441E+07,
           NOM_PARA = 'DX',
           TABLE = tab_t,
           FILTRE = _F(NOM_PARA = 'INST',
                       VALE = 0.69999999999999996))

TEST_TABLE(VALE_CALC = 4.6081844086981E+07,
           NOM_PARA = 'DX',
           TABLE = tab_t,
           FILTRE = _F(NOM_PARA = 'INST',
                       VALE = 0.8))

TEST_TABLE(VALE_CALC = 5.6885415537783E+07,
           NOM_PARA = 'DX',
           TABLE = tab_t,
           FILTRE = _F(NOM_PARA = 'INST',
                       VALE = 0.9))

TEST_TABLE(VALE_CALC = 6.9020354634835E+07,
           NOM_PARA = 'DX',
           TABLE = tab_t,
           FILTRE = _F(NOM_PARA = 'INST',
                       VALE = 1.0))

TEST_TABLE(VALE_CALC = 7.2693236508798E+07,
           NOM_PARA = 'DX',
           TABLE = tab_t,
           FILTRE = _F(NOM_PARA = 'INST',
                       VALE = 1.002))

TEST_TABLE(VALE_CALC = 7.6371652237431E+07,
           NOM_PARA = 'DX',
           TABLE = tab_t,
           FILTRE = _F(NOM_PARA = 'INST',
                       VALE = 1.004))

TEST_TABLE(VALE_CALC = 8.0055851960701E+07,
           NOM_PARA = 'DX',
           TABLE = tab_t,
           FILTRE = _F(NOM_PARA = 'INST',
                       VALE = 1.006))

TEST_TABLE(VALE_CALC = 8.3746070160476E+07,
           NOM_PARA = 'DX',
           TABLE = tab_t,
           FILTRE = _F(NOM_PARA = 'INST',
                       VALE = 1.008))

TEST_TABLE(VALE_CALC = 8.7442524098204E+07,
           NOM_PARA = 'DX',
           TABLE = tab_t,
           FILTRE = _F(NOM_PARA = 'INST',
                       VALE = 1.01))

# End
FIN()
