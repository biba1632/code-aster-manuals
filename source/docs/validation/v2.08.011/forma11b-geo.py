#!/usr/bin/env python
# coding: utf-8

import gmsh
import sys
import math

gmsh.initialize()

# Choose Built-in kernel
model = gmsh.model
geo = model.geo
mesh = model.mesh
model.add("forma11b")


# Sphere size
el_size = 1
rad = 0.01
rad2 = rad/math.sqrt(3.0)
num_nodes = 5

# Add points
p0 = geo.addPoint(0, 0, 0, el_size)
p102 = geo.addPoint(rad2, rad2, -rad2, el_size)
p103 = geo.addPoint(-rad2, rad2, -rad2, el_size)
p104 = geo.addPoint(-rad2, -rad2, -rad2, el_size)
p105 = geo.addPoint(rad2, -rad2, -rad2, el_size)

# Add circles
l1 = geo.addCircleArc(p103, p0, p102)
l2 = geo.addCircleArc(p102, p0, p105)
l3 = geo.addCircleArc(p105, p0, p104)
l4 = geo.addCircleArc(p104, p0, p103)

# Create curve loop
loop1 = geo.addCurveLoop([1, 2, 3, 4])

# Create surface
surf1 = geo.addSurfaceFilling([loop1], sphereCenterTag = 1)

# Create and rotate copies (top surf)
surf2 = geo.copy([(2, surf1)])
geo.rotate(surf2, 0, 0, 0, 1, 0, 0, math.pi/2)

# Create and rotate copies (opposite surf)
surf3 = geo.copy([(2, surf1)])
geo.rotate(surf3, 0, 0, 0, 1, 0, 0, math.pi)

# Create and rotate copies (bot surf)
surf4 = geo.copy([(2, surf1)])
geo.rotate(surf4, 0, 0, 0, 1, 0, 0, 3*math.pi/2)

# Create and rotate copies (left surf)
surf5 = geo.copy([(2, surf1)])
geo.rotate(surf5, 0, 0, 0, 0, 1, 0, math.pi/2)

# Create and rotate copies (right surf)
surf6 = geo.copy([(2, surf1)])
geo.rotate(surf6, 0, 0, 0, 0, 1, 0, -math.pi/2)

# Create surface loop
sloop1 = geo.addSurfaceLoop([surf1, surf2[0][1], surf3[0][1], surf4[0][1], surf5[0][1], surf6[0][1]])

# Create volume
sphere = geo.addVolume([sloop1])

# Synchronize
geo.synchronize()

curve_list = [1, 2, 3, 4, 6, 7, 9, 11, 12, 14, 17, 19]
surface_list = [1, 5, 10, 15, 20, 21]
vol_list = [1]

# Set transfinite interpolation
for curve in curve_list:
    mesh.setTransfiniteCurve(curve, num_nodes)

for surf in surface_list:
    mesh.setTransfiniteSurface(surf)

for vol in vol_list:
    mesh.setTransfiniteVolume(vol)

# Set meshing options
gmsh.option.setNumber('Mesh.RecombineAll', 1)
gmsh.option.setNumber('Mesh.RecombinationAlgorithm', 1)
gmsh.option.setNumber('Mesh.Recombine3DLevel', 2)
gmsh.option.setNumber('Mesh.ElementOrder', 2)
gmsh.option.setNumber('Mesh.MshFileVersion', 2.2)

# Generate mesh
mesh.generate(3)

# Save mesh
gmsh.write("forma11b.msh")

# Activate GUI
gmsh.fltk.run()

# Clean up
gmsh.finalize()
