# Compute the forces that need to be applied to satisfy the 
# manufactured displacement solution

import math

def compute_MMS_sympy(E, nu):

  try:

    import sympy
    sympy_available = True

  except ImportError:

    sympy_available = False

  print("sympy available? ", sympy_available)

  if sympy_available:

    from Utilities import TensorModule
    from Utilities import HookeTensor

    x,y,z = sympy.symbols('X Y Z')

    # Assumed solution
    u_x = 0.2*(z**2)*x*y
    u_y = 0.2*(z**2)*x*y
    u_z = -0.2*(1. + x**2 + y**2)*(1+0.01*z) -0.01*z -0.3

    u = TensorModule.Tensor([u_x, u_y, u_z])

    # Compute contact normals and deformation gradient
    normal_xp = TensorModule.Tensor([ +1. , 0. , 0.])
    normal_xm = TensorModule.Tensor([ -1. , 0. , 0.])
    normal_yp = TensorModule.Tensor([ 0. , +1. , 0.])
    normal_ym = TensorModule.Tensor([ 0. , -1. , 0.])

    normal_slave = TensorModule.Tensor([ 0. , 0. , -1.])

    Id = TensorModule.Tensor([[1.,0.,0],[0.,1.,0],[0.,0.,1.]])

    defGrad       = TensorModule.grad(u) + Id
    defGrad_T     = defGrad.transpose()
    defGrad_T_inv = defGrad_T.inverse()
   
    print(defGrad)

    defGrad_T_inv_N         = defGrad_T_inv.simpleContract(normal_slave)
    norm_sq_defGrad_T_inv_N = defGrad_T_inv_N.simpleContract(defGrad_T_inv_N)
    norm_defGrad_T_inv_N    = norm_sq_defGrad_T_inv_N.sqrt()
    normal_contact          = defGrad_T_inv_N / norm_defGrad_T_inv_N

    # Compute Green-Lagrange strain
    strain_Green_Lagrange = (defGrad.transpose().simpleContract(defGrad) - Id)*0.5

    # Second 2nd P-K stress
    C_isotropic = HookeTensor.HookeIsotropic(E, nu)
    stress_PK2 = C_isotropic.doubleContract(strain_Green_Lagrange)

    # First 2nd P-K stress
    stress_PK1 = defGrad.simpleContract(stress_PK2)

    # Divergence of first P-K stress (this is the applied body force)
    f_body = -TensorModule.div(stress_PK1)

    # Compute boundary conditions
    # Neumann BCs 
    f_surf_xp = stress_PK1.simpleContract(normal_xp)
    f_surf_xm = stress_PK1.simpleContract(normal_xm)
    f_surf_yp = stress_PK1.simpleContract(normal_yp)
    f_surf_ym = stress_PK1.simpleContract(normal_ym)

    f_surf_slave = stress_PK1.simpleContract(normal_slave)

    # Tractions on contact faces
    f_surf_slave_nn = f_surf_slave.simpleContract(normal_contact)
    f_surf_slave_t = f_surf_slave - f_surf_slave_nn*normal_contact

    # return the computed values
    return u, f_surf_xp, f_surf_xm, f_surf_yp, f_surf_ym, f_surf_slave_t, \
           f_surf_slave_nn, f_surf_slave, f_body

  else:

    return 0

