
# coding: utf-8

import gmsh
import sys
import math

gmsh.initialize()

# Use OpenCASCADE kernel
model = gmsh.model
occ = model.occ
mesh = model.mesh
model.add("ssnv209a")

# Default element size and discretization
el_size = 0.2
N1 = 64
N2 = 10
N3 = 32

# Geometry
loc = 0.04
loc1 = -0.04*5/21.0
loc2 = 0.08
locR = 0.03875
epsi= 0.000001

xA = 0.
xB = 0.00125
xC = 0.005
xD = 0.0075
xE = 0.01125

# Add points, lines, and surface for base
pBA = occ.addPoint(xA, 0, 0, el_size)
pBS = occ.addPoint(loc2, 0, 0, el_size)
pCA = occ.addPoint(xA, loc1, 0, el_size)
pCS = occ.addPoint(loc2, loc1, 0, el_size)

lBase = occ.addLine(pBS, pBA)
lFixed = occ.addLine(pCS, pCA)
lBaseV1 = occ.addLine(pBA, pCA)
lBaseV2 = occ.addLine(pBS, pCS)

wBase = occ.addCurveLoop([lBase, lBaseV1, -lFixed, -lBaseV2])
sBase = occ.addPlaneSurface([wBase])

# Add points, lines, and surface for plate
pPA = occ.addPoint(xA, 0., 0, el_size)
pPB = occ.addPoint(xB, 0., 0, el_size)
pPC = occ.addPoint(xC, 0., 0, el_size)
pPD = occ.addPoint(xD, 0., 0, el_size)
pPE = occ.addPoint(xE, 0., 0, el_size)
pPS = occ.addPoint(loc2, 0., 0, el_size)
pHA = occ.addPoint(xA, loc, 0, el_size)
pHS = occ.addPoint(loc2, loc, 0, el_size)
pPI = occ.addPoint(loc, 0., 0, el_size)
pPR = occ.addPoint(locR, 0., 0, el_size)

lCont1 = occ.addLine(pPA, pPB)
lCont2 = occ.addLine(pPB, pPC)
lCont3 = occ.addLine(pPC, pPD)
lCont4 = occ.addLine(pPD, pPE)
lCont5 = occ.addLine(pPE, pPR)
lCont6 = occ.addLine(pPR, pPI)
lCont7 = occ.addLine(pPI, pPS)
lFixedX = occ.addLine(pPS, pHS)
lPresV = occ.addLine(pHA, pHS)
lPresH = occ.addLine(pPA, pHA)

wPlate = occ.addCurveLoop([lCont1, lCont2, lCont3, lCont4, lCont5, lCont6, lCont7, lFixedX, -lPresV, -lPresH])
sPlate = occ.addPlaneSurface([wPlate])

# Create vertical lines for partitioning
pPB_top = occ.addPoint(xB, loc, 0, el_size)
pPC_top = occ.addPoint(xC, loc, 0, el_size)
pPD_top = occ.addPoint(xD, loc, 0, el_size)
pPE_top = occ.addPoint(xE, loc, 0, el_size)
pPI_top = occ.addPoint(loc, loc, 0, el_size)
pPR_top = occ.addPoint(locR, loc, 0, el_size)
pPB_bot = occ.addPoint(xB, 0, 0, el_size)
pPC_bot = occ.addPoint(xC, 0, 0, el_size)
pPD_bot = occ.addPoint(xD, 0, 0, el_size)
pPE_bot = occ.addPoint(xE, 0, 0, el_size)
pPI_bot = occ.addPoint(loc, 0, 0, el_size)
pPR_bot = occ.addPoint(locR, 0, 0, el_size)

lB = occ.addLine(pPB_top, pPB_bot)
lC = occ.addLine(pPC_top, pPC_bot)
lD = occ.addLine(pPD_top, pPD_bot)
lE = occ.addLine(pPE_top, pPE_bot)
lI = occ.addLine(pPI_top, pPI_bot)
lR = occ.addLine(pPR_top, pPR_bot)

# Create partition
frags = occ.fragment([(2, sPlate)], [(1, lB), (1, lC), (1, lD), (1, lE), (1, lR), (1, lI)])

occ.synchronize()

# # Get plate vertical lines
# vLines = occ.getEntitiesInBoundingBox(-0.001, 0.5*loc-0.001, -0.001, loc2+0.001, 0.5*loc+0.001, 0.001, dim = 1)
# print(vLines)

# For transfinite interpolation (base)
for curve in [lBase, lFixed]:
    mesh.setTransfiniteCurve(curve, N1+1, meshType = "Progression", coef = 1)
for curve in [lBaseV1, lBaseV2]:
    mesh.setTransfiniteCurve(curve, 4, meshType = "Progression", coef = 1)

# For transfinite interpolation (plate)
# for curve in [lPresV]:
#     mesh.setTransfiniteCurve(curve, N1+1, meshType = "Progression", coef = 1)
for curve in [22, 32, 23, 33]:
    mesh.setTransfiniteCurve(curve, 2, meshType = "Progression", coef = 1)
for curve in [24, 28, 25, 29]:
    mesh.setTransfiniteCurve(curve, 4, meshType = "Progression", coef = 1)
for curve in [26, 27]:
    mesh.setTransfiniteCurve(curve, 3, meshType = "Progression", coef = 1)
for curve in [30, 31]:
    mesh.setTransfiniteCurve(curve, N3-9, meshType = "Progression", coef = 1)
for curve in [34, 36]:
    mesh.setTransfiniteCurve(curve, N3+1, meshType = "Progression", coef = 1)
for curve in [15, 16, 17, 18, 19, 20, 21, 35]:
    mesh.setTransfiniteCurve(curve, N2+1, meshType = "Progression", coef = 1)

for surf in occ.getEntities(2):
    mesh.setTransfiniteSurface(surf[1])

for surf in occ.getEntities(2):
    mesh.setRecombine(2, surf[1])

# Physical groups for the points
pPA_g = model.addPhysicalGroup(0, [5])
pPB_g = model.addPhysicalGroup(0, [8])
pPC_g = model.addPhysicalGroup(0, [10])
pPD_g = model.addPhysicalGroup(0, [12])
pPE_g = model.addPhysicalGroup(0, [14])
pPR_g = model.addPhysicalGroup(0, [16])
pPI_g = model.addPhysicalGroup(0, [18])
pPS_g = model.addPhysicalGroup(0, [20])
pHA_g = model.addPhysicalGroup(0, [6])
pHS_g = model.addPhysicalGroup(0, [19])
pBA_g = model.addPhysicalGroup(0, [pBA])
pBS_g = model.addPhysicalGroup(0, [pBS])
pCA_g = model.addPhysicalGroup(0, [pCA])
pCS_g = model.addPhysicalGroup(0, [pCS])

# Physical groups for the edges
lPresH_g = model.addPhysicalGroup(1, [21])
lPresV_g = model.addPhysicalGroup(1, [22, 24, 26, 28, 30, 32, 34])
lFixedX_g = model.addPhysicalGroup(1, [35])
lConta_g = model.addPhysicalGroup(1, [23, 25, 27, 29, 31, 33, 36])
lContaR_g = model.addPhysicalGroup(1, [23, 25, 27, 29, 31])
lFixed_g = model.addPhysicalGroup(1, [lFixed])
lBase_g = model.addPhysicalGroup(1, [lBase])

# Physical groups for the faces
sBase_g = model.addPhysicalGroup(2, [sBase])
sPlate_g = model.addPhysicalGroup(2, [2, 3, 4, 5, 6, 7, 8])
sTot_g = model.addPhysicalGroup(2, [sBase, 2, 3, 4, 5, 6, 7, 8])

# Set names of groups
model.setPhysicalName(0, pPA_g, 'pPA')
model.setPhysicalName(0, pPB_g, 'pPB')
model.setPhysicalName(0, pPC_g, 'pPC')
model.setPhysicalName(0, pPD_g, 'pPD')
model.setPhysicalName(0, pPE_g, 'pPE')
model.setPhysicalName(0, pPR_g, 'pPR')
model.setPhysicalName(0, pPI_g, 'pPI')
model.setPhysicalName(0, pPS_g, 'pPS')
model.setPhysicalName(0, pHA_g, 'pHA')
model.setPhysicalName(0, pHS_g, 'pHS')
model.setPhysicalName(0, pBA_g, 'pBA')
model.setPhysicalName(0, pBS_g, 'pBS')
model.setPhysicalName(0, pCA_g, 'pCA')
model.setPhysicalName(0, pCS_g, 'pCS')

model.setPhysicalName(1, lPresH_g, 'lPresH')
model.setPhysicalName(1, lPresV_g, 'lPresV')
model.setPhysicalName(1, lFixedX_g, 'lFixedX')
model.setPhysicalName(1, lConta_g, 'lConta')
model.setPhysicalName(1, lContaR_g, 'lContaR')
model.setPhysicalName(1, lFixed_g, 'lFixed')
model.setPhysicalName(1, lBase_g, 'lBase')

model.setPhysicalName(2, sBase_g, 'sBase')
model.setPhysicalName(2, sPlate_g, 'sPlate')
model.setPhysicalName(2, sTot_g, 'sTot')

#gmsh.option.setNumber('Mesh.Algorithm', 8)
gmsh.option.setNumber('Mesh.RecombinationAlgorithm', 1)
gmsh.option.setNumber('Mesh.ElementOrder', 1)
gmsh.option.setNumber('Mesh.MshFileVersion', 2.2)
gmsh.option.setNumber('Mesh.MedFileMinorVersion', 0)
gmsh.option.setNumber('Mesh.SaveAll', 0)
gmsh.option.setNumber('Mesh.SaveGroupsOfNodes', 1)

# Generate mesh
mesh.generate(2)

# Save mesh
gmsh.write("ssnv209a_upd.med")

gmsh.fltk.run()

gmsh.finalize()


