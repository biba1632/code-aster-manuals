.. role:: red
.. role:: blue
.. role:: green
.. role:: gray
.. role:: python(code)
   :language: python
.. |br| raw:: html

   <br/>

.. index:: ! MODI_MAILLAGE

.. _u4.23.04:

************************************************************************
**[U4.23.04]** : Operator ``MODI_MAILLAGE`` (:gray:`MODIFY_MESH`)
************************************************************************

.. _u4.23.04.1:

Purpose
=======

Perform modifications on an existing mesh. The possible modifications are:

* to reorient skin elements (face/edge) being used to apply a pressure,
* to reorient the HEXA8 elements of SHB (solid shell element) models,
* to check the orientation of the normals of shell elements,
* to reorient the elements of a mono-layer of joint elements,
* the recompute the nodal positions of a mesh using displacements calculated previously,
* in an element with crack tip, to move the middle nodes of the edges touching the crack tip to 
  a position closer to the crack tip (to a quarter of these edges),
* translate a mesh,
* impose one or more rotations on a mesh,
* generate a symmetrical mesh with respect to a plane in 3D or a straight line in 2D.
* calculate the arc-length coordinates (called "curvilinear abscissa" in ``Code_Aster``)  along a 
  line formed by line elements (SEG)
* randomly modify the coordinates of the nodes

The output of this operator is a data structure of the :green:`maillage` type or 
a modified version of the input  data structure (operator :blue:`reuse`).
 
.. _u4.23.04.2:

Syntax
======

::

  mesh [maillage] = MODI_MAILLAGE
  (
  
    ◊ reuse = / mesh,
    ♦ MAILLAGE = mesh,                                            [maillage]

    ◊ ORIE_FISSURE = _F(
                        ♦ GROUP_MA = l_gm                         [l_gr_ma]
                       ),
    ◊ ORIE_SHB = _F(
                    ♦ GROUP_MA = l_gm                             [l_gr_ma]
                   ),
    ◊ DEFORME = _F (
                    ♦ OPTION = 'TRAN',
                    ♦ / DEPL = depl,                              [cham_no]
                      / ALEA = epsi,                              [R]
                   ),
    ◊ ORIE_PEAU_2D = _F(
                        ♦ GROUP_MA = lgrma,                       [l_gr_ma]
                        ◊ GROUP_MA_SURF = l_gms                   [l_gr_ma]
                       ),
    ◊ ORIE_PEAU_3D = _F(
                        ♦ GROUP_MA = lgrma,                       [l_gr_ma]
                        ◊ GROUP_MA_VOLU = l_gmv                   [l_gr_ma]
                       ),
    ◊ ORIE_NORM_COQUE = _F(
                           ♦ GROUP_MA = lgrma,                    [l_gr_ma]
                           ◊ VECT_NORM = (n1, n2, [n3]),          [l_R]
                           ◊ GROUP_NO = grno,                     [gr_no]
                          ),
    ◊ ORIE_LINE = _F(
                     ♦ GROUP_MA =lgrma,                           [l_gr_ma]
                     ◊ VECT_TANG = (n1, n2, [n3]),                [l_R]
                     ◊ GROUP_NO = grno,                           [gr_no]
                    ),
    ◊ MODI_MAILLE= _F(
                      ♦ OPTION = 'NOEUD_QUART',
                      ♦ / GROUP_MA_FOND = lgma_fo,                [l_gr_ma]
                        / GROUP_NO_FOND = lgno_fo,                [l_gr_no]
                     ),
    ◊ TRANSLATION = (n1, n2, [n3]),                               [l_R]
    ◊ ROTATION = _F(
                    ♦ POIN_1 = (n1, n2, [n3]),                    [l_R]
                    ♦ / POIN_2 = (n1, n2, [n3]),                  [l_R]
                      / DIR = (n1, n2, [n3]),                     [l_R]
                    ♦ ANGLE = / theta,                            [R]
                              / 0.,                               [DEFAULT]),
    ◊ ECHELLE = n1,                                               [R]
    ◊ MODI_BASE = _F(
                     ♦ VECT_X = (n1, n2, [n3]),                   [l_R]
                     ◊ VECT_Y = (n1, n2, [n3]),                   [l_R]
                    ),
    ◊ SYMETRIE = _F(
                    ♦ POINT = (n1, n2, [n3]),                     [l_R]
                    ♦ AXE_1 = (n1, n2, [n3]),                     [l_R]
                    ◊ AXE_2 = (n1, n2, n3),                       [l_R]
                   ),
    ◊ ABSC_CURV = _F(
                     ♦ / TOUT = 'OUI',
                       / GROUP_MA = (gm1, gm2, ...),              [l_gr_ma]
                     ♦ GROUP_NO_ORIG = gnorig,                    [gr_no]
                    ),
    ◊ INFO = / 1,                                                 [DEFAULT]
             / 2,
  )

.. _u4.23.04.3:

Operands
========

.. _u4.23.04.3.1:

Operands :blue:`MAILLAGE` (:gray:`MESH`)
-----------------------------------------------------------------------------

::

  ♦ | MAILLAGE = mesh

Mesh of type :green:`[maiiage]` on which the modifications and / or checks will be performed.

.. _u4.23.04.3.2:

Operand :blue:`INFO`
-------------------------

::

   ◊ INFO =

Indicates the amount of output to be printed in the message file (of the operator's results).

* :blue:`1` = no printing,
* :blue:`2` = printing of the elements whose connectivity has been modified, including the printing of
  old and new connectivity.

.. _u4.23.04.3.3:

Keyword :blue:`ORIE_FISSURE` (:gray:`ORIENT_CRCAK`)
----------------------------------------------------

::

   ◊ ORIE_FISSURE =

This key word is used to reorient (if necessary) the elements of a group forming a "single layer"
of elements. It works in 2D or 3D, with a linear or quadratic mesh (:numref:`fig_u4.23.04.3.3-a`).

.. _fig_u4.23.04.3.3-a:

.. figure:: u4.23.04/fig1_u4.23.04.png
   :height: 200 px
   :align: center

   A single layer of elements.  

Currently, this keyword is used to reorient joint and interface elements (models 
:blue:`AXIS_xxx`, :blue:`PLAN_xxx` and :blue:`3D_xxx` with :blue:`xxx = JOINT` or :blue:`INTERFACE`).

The user specifies (with the keyword :blue:`GROUP_MA`) which are the candidate elements for the
reorientation (the "single layer"). 

These elements must be "prisms" (QUAD in 2D, HEXA and PENTA in 3D).

The direction "transverse" to the layer is determined topologically (and not according to a flatness
criterion). To be able to be reoriented, the elements of the layer must be supported (via the bases of 
the prisms) on other meshes of the same dimension (2D or 3D) which do not belong to the group of elements 
(that are to be reoriented).

Consider the situation where we wish to reorient the group of three elements A, B and C in the figure
below.

.. image:: u4.23.04/fig2_u4.23.04.png
   :height: 300 px
   :align: center

For elements A and B, the supporting elements (above and below) determine an unambiguous transverse 
orientation (vertical). On the other hand, element C has three supporting elements (up, down, right) 
and we do not know how to determine the transverse direction. The reorintation algorithm will fail.

.. admonition:: Remark 

   The "reorientation" we are talking about here is actually changing the definition of element
   connectivity. For example, in 2D, the convention is that sides 2 and 4 of quadrangles are transverse 
   to the layer.

::

   ♦ GROUP_MA = l_gm,

List of the element groups for which one wishes to checking (and possibly modify) the orientation.

.. _u4.23.04.3.4:

Keyword :blue:`ORIE_SHB` (:gray:`ORIENT_SBH_ELEMENTS`)
-------------------------------------------------------

::

   ◊ ORIE_SHB =

The purpose of this factor keyword is to correctly reorient the volume meshes of the SHB finite elements. 
It is necessary to modify the local classification of the nodes of the elements to be able to recognize
the direction of the normal to the shell. 

The connectivity of the elements is thus possibly modified by this operator.

::

   ♦ GROUP_MA = l_gm,

List of the element groups for which one wishes to modify the orientation.

.. _u4.23.04.3.5:

Keyword :blue:`DEFORME` (:gray:`DISPLACE_MESH`)
-----------------------------------------------

::

   ♦ DEFORME 
     / OPTION = 'TRAN'

This option allows the user to modify the initial geometry of the mesh (:green:`mesh`) by :

* the translated (:blue:`TRAN`) values :math:`(dx, dy, (dz))` of the displacement field 
  :green:`depl` given by the keyword :blue:`DEPL`;

* a random amount.

::

   / DEPL = depl,

Displacement field used to update the geometry

::

   / ALEA = epsi,

In this case, a random quantity (:math:`\Delta`) is added to each coordinate of each node of the mesh.
This quantity is obtained by the formula: 

.. math::

   \Delta = \text{epsi} \times \text{dim} \times \text{alea} ()

where

* :green:`epsi` is a number supplied by the user (1.e-8 for example)
* :green:`dim` is the dimension of the mesh in the direction concerned, determined
   by the component (X, Y or Z)
* :green:`alea()` is a function returning a pseudo-random number in the interval [-1.1].

.. admonition:: Remark 

   The possibility of randomly modifying the coordinates of a mesh is, a priori, a "developer" 
   functionality. It is dangerous in certain situations: very flat meshes (joints, ...)

.. _u4.23.04.3.6:

Keywords :blue:`ORIE_PEAU_2D / ORIE_PEAU_3D` (:gray:`ORIENT_SKIN_2D/ORIENT_SKIN_3D`)
-------------------------------------------------------------------------------------

::

   ◊ ORIE_PEAU_2D =
   ◊ ORIE_PEAU_3D =

These keywords are used to reorient edges and faces so that their normals are coherent (towards the 
exterior of a body). This is an essential prerequisite if, for example, we wants to apply a pressure load 
on this "skin".

::

  ♦ GROUP_MA = lgrma,                [l_gr_ma]

Element groups that are to be reoriented. 

The elements are directed in such a way that the normal is outgoing. For each skin element (edge or 
face), one seeks the "volume" mesh that it "borders". We orient it in such a way that its normal is 
of direction opposite to the vector connecting its first node to the barycenter of the volume mesh.

It sometimes happens that the "skin" that you want to orient is inserted into the material (for example, 
when a calculation is made for which elements may be gradually added or removed from the model: modeling 
an excavation, or a layered construction). The orientation algorithm described above then fails because 
one generally finds two volume elements on both sides of the skin mesh. We do not know which one to use 
for orienting the skin mesh.

To address that problem, we introduced the optional keyword :blue:`GROUP_MA_VOLU` 
(:gray:`VOLUME_ELEMENT_GROUP`) (or :blue:`GROUP_MA_SURF` :gray:`(SURFACE_ELEMENT_GROUP)` in 2D). These 
keywords allow the user to specify which are the "volume" elements that are to be used to orient skin 
elements.

Example :

Suppose we have a group of skin elements (:green:`GPEAU`) which we want to reorient with a normal 
directed towards the outside of the body. It should be indicated that this is the skin of the "volume" 
element group :green:`GV2`. We will write:

::

   ORIE_PEAU_2D = _F(GROUP_MA = 'GPEAU', 
                     GROUP_MA_SURF = 'GV2')


.. image:: u4.23.04/fig3_u4.23.04.png
   :height: 200 px
   :align: center

.. _u4.23.04.3.7:

Keyword :blue:`ORIE_NORM_COQUE` (:gray:`ORIENT_SHELL_NORMAL`)
--------------------------------------------------------------

::

   ◊ ORIE_NORM_COQUE = _F()

This keyword is used to check that in a list of surface elements (shells), the normals are consistent 
with each other. Otherwise, certain elements are reoriented.

::

  ♦ GROUP_MA = lgrma,            [l_gr_ma]

Surface element groups to reorient. The elements of :green:`lgrma` must form a "connected" group
so that we can reorient them by continuity. 

One can impose the direction of orientation using keywords :blue:`GROUP_NO / VECT_NORM`.  Otherwise, 
the orientation selected will be that of the first mesh of :green:`lgrma`.  Since this is not necessarily 
the correct orientation, it is advised to always use the keyword :blue:`VECT_NORM` (:gray:`NORMAL_VECTOR`).

::

   ◊ VECT_NORM = (n1, n2, [n3]),               [l_R]

:green:`ni`: 2 or 3 components (depending on the dimension) of the normal vector. It is also necessary to 
specify the base node of this normal vector:

::

   ◊ GROUP_NO = grno,                          [gr_no]

:green:`grno` must be a node group containing only one node. The selected normal will be that which 
makes an acute angle with the vector given by :blue:`VECT_NORM`.

.. _u4.23.04.3.8:

Keyword :blue:`ORIE_LIGNE` (:gray:`ORIENT_LINE`)
-------------------------------------------------

::

   ◊ ORIE_LIGNE = _F()

This keyword is used to check that in a list of line elements (beams), the tangents are consistent with 
each other. Otherwise, certain elements are reoriented.

::

   ♦ GROUP_MA = lgrma,              [l_gr_ma]

Groups of line elements to be reoriented. The elements in :green:`lgrma` must form a "connected" 
group so that we can reorient them by continuity. 

One can impose the direction of orientation using keywords :blue:`GROUP_NO / VECT_TANG`. 
Otherwise, the orientation will be that of the first element of :green:`lgrma`. Since the preferred
orientation is not necessarily that of the first element of the element group, it is advisable to always 
use the keyword :blue:`VECT_TANG` (:gray:`TANGENT_VECTOR`).

::

  ◊ VECT_TANG = (n1, n2, [n3]),     [l_R]

:green:`ni`: 2 or 3 components (according to the dimension) of the tangent vector. It is also necessary 
to specify the support node of the normal vector:

::

  ◊ GROUP_NO = grno,                [gr_no]

:green:`grno` must be a node group containing only one node. The selected tangent will be that which makes 
an acute angle with the vector given by :blue:`VECT_TANG`.

.. _u4.23.04.3.9:

Keyword :blue:`MODI_MAILLE` (:gray:`MODIFY_ELEMENTS`)
------------------------------------------------------

::

   ♦ OPTION = 'NOEUD_QUART',
     ♦ / GROUP_MA_FOND = lgma_fo,                    [l_gr_ma]
       / GROUP_NO_FOND = lgno_fo,                    [l_gr_no]

This is used to move the middle nodes of the edges of an element touching a crack tip to a quarter of 
these edges (towards the crack tip) (:gray:`QUARTER_NODE`).

In 2D, one specifies the node of the crack tip with :blue:`GROUP_NO_FOND` (:gray:`NODE_GROUP_CRACK_TOP`).

In 3D, one specifies either the nodes of the crack tip, or the SEG3 elements of the crack tip (and not 
the elements of the faces of the crack or the elements at the crack tip).

.. _u4.23.04.3.10:

Keyword :blue:`TRANSLATION`
------------------------------

.. warning::

   This functionality can be used in combination with :blue:`ROTATION`, but these operations are 
   not commutative.  The translation is always executed before the rotation.  One cannot combine this 
   functionality with :blue:`SYMETRIE` (:gray:`SYMMETRY`).

::

  ◊ TRANSLATION = (n1, n2, [n3]),                    [l_R]

Keyword for the translation of a mesh using a vector.

.. _u4.23.04.3.11:

Keyword :blue:`ROTATION`
--------------------------

.. warning::

   This functionality can be used in combination with :blue:`TRANSLATION`, but these operations are 
   not commutative. On the other hand, you cannot use :blue:`ROTATION`, :blue:`MODI_BASE` and 
   :blue:`SYMETRIE` at the same time. The translation is always executed before the rotation.

::

   ◊ ROTATION =

Keyword factor for the rotation of a mesh.

::

  ♦ POIN_1 = (ni, n2, [n3]),                        [l_R]

Coordinates of the first point that defines the axis of rotation.

::

  ♦ / POIN_2 = (ni, n2, [n3]),                      [l_R]
    / DIR = (ni, n2, [n3]),                         [l_R]

Coordinates of the second point or direction that is needed to completely define the axis of rotation.

::

  ♦ ANGLE = theta,                                  [R]

Angle of rotation expressed in degrees.

The rotation is done in the clockwise direction, with respect to its oriented axis. This axis passes 
through the point :blue:`POIN_1` and its orientation is given either by the vector :blue:`DIR` or by 
the start point :blue:`POIN_1` and end point :blue:`POIN_2`.

The rotation is defined as follows.

Let :math:`\mathbf{M}(x, y, z)` be a point in space, we impose a rotation of angle :math:`\theta` 
(in radians) around the axis passing through :math:`\mathbf{P}(p_x, p_y, p_z)` and in the direction 
:math:`\mathbf{D}(d_x, d_y, d_z)`. So, after rotation,  :math:`\mathbf{M}` becomes :math:`\mathbf{M}'`
where:

.. math::

   \mathbf{M}' = \mathbf{P} + \cos\theta \mathbf{PM} + 
     (1 − \cos\theta) (\mathbf{PM} \cdot \mathbf{D} ) \mathbf{D} + 
     \sin\theta ( \mathbf{D} \wedge \mathbf{PM} )

.. _u4.23.04.3.12:

Keyword :blue:`ECHELLE` (:gray:`SCALE`)
---------------------------------------

.. warning::

   This functionality can be used in combination with :blue:`ROTATION` and 
   :blue:`TRANSLATION`.  Scaling, when it is requested, is always done after :blue:`TRANSLATION` and 
   :blue:`ROTATION`.

   One cannot combine this functionality with :blue:`SYMETRIE` (:gray:`SYMMETRY`).

::

  ◊ ECHELLE = n1,                               [R]

Keyword for the scaling of a mesh by a real number.

Let :math:`\mathbf{M}(x, y, z)` be a point on the mesh.  The scaling operation transforms this
point to one with the coordinates scaled by the transformation ratio :green:`n1` to
:math:`\mathbf{M}' (n1 \times x, n1 \times  y, n1 \times  z )`.

.. _u4.23.04.3.13:

Keyword :blue:`MODI_BASE` (:gray:`MODIFY_BASIS_VECTORS`)
---------------------------------------------------------

.. warning::

   This functionality is unavailable in combination with :blue:`ROTATION` and 
   :blue:`SYMETRIE` (:gray:`SYMMETRY`). 

::

  ◊ MODI_BASE =

Keyword factor for changing the basis vectors in which one expresses the coordinates of a mesh. The 
change of reference always takes place between two orthonormal bases.

::

  ♦ VECT_X = (n1, n2, [n3]),                        [l_R]

The first vector of the new basis.  Any length is allowed.

::

  ◊ VECT_Y = (n1, n2, [n3]),                        [l_R]

The second vector of the new basis (not used in 2D), also of arbitrary (but finite) length.

In 2D, it is enough to provide the axis :blue:`VECT_X`, and ``Code_Aster`` automatically builds the 
second vector to define a direct orthogonal basis. A test checks if :blue:`VECT_X` is not of zero length.

In 3D, one checks that :blue:`VECT_X` and :blue:`VECT_Y` are of non-zero norm and one checks that they 
are orthogonal. The third vector which completes the basis is constructed as the product vector of 
:blue:`VECT_X` with :blue:`VECT_Y`. We thus ensure the construction of an orthogonal basis.

Then, in all the cases (2D and 3D), the vectors of the basis are normalized to 1, the user does not have
to worry about it. We therefore finally have an orthonormal basis.

.. _u4.23.04.3.14:

Keyword :blue:`SYMETRIE` (:gray:`SYMMETRY`)
--------------------------------------------

.. warning::

   This functionality cannot be combined with :blue:`TRANSLATION`, :blue:`ROTATION`, 
   :blue:`ECHELLE` (:gray:`SCALE`) and :blue:`MODI_BASE`.

::

   ♦ POINT = (n1, n2, [n3])                     [l_R]

Coordinates of a point belonging to the line in 2D or to the plane in 3D.

::

   ♦ AXE_1 = (n1, n2, [n3])                     [l_R]

Direction vector of the line in 2D or the first basis vector that is used to describe the plane.

::

   ◊ AXE_2 =(n1, n2, n3)                        [l_R]

Second vector that is used to describe the plane.

In 2D, the symmetry operation is performed with respect to a line which is in the OXY plane. To define 
this line, it is necessary to provide the direction vector of the line (:blue:`AXE_1`) and a point 
(:blue:`POINT`) belonging to this line.

In 3D, the symmetry operation is performed with respect to a plane. To define this plane, it is 
necessary to provide two vectors of the plane :blue:`(AXE_1, AXE_2)` and a point (:blue:`POINT`) 
belonging to this plane.

In all the cases (2D or 3D), the symmetry operation is carried out with respect to a plane. In 2D, the 
second vector necessary for the definition of the plane is fixed at :blue:`AXE_2 = (0.0, 0.0, -1.0)`.

The algebraic distance 
:math:`\delta` between a point :math:`\mathbf{M}(x, y, z)` and a plane passing through the point
:math:`\mathbf{O}(x_0, y_0, z_0)` with for perpendicular vector 
:math:`\mathbf{V} = \text{AXE_1}~\wedge~\text{AXE_2} = (a, b, c)` is:

.. math::

   \delta = \frac{a ( x - x_0 )+ b ( y - y_0 )+ c ( z - z_0 )}{\sqrt{ a^2+ b^2+ c^2}}

The coordinates of the point :math:`\mathbf{M}'` that is symmetrical to the point :math:`\mathbf{M}` 
relative to the plane are given by:

.. math::

   \mathbf{OM}' = −2\delta \frac{\mathbf{V}}{\| \mathbf{V} \|} + \mathbf{OM}

.. _u4.23.04.3.15:

Keyword :blue:`ABS_CURV` (:gray:`ARCLENGTH_COORDINATE`)
--------------------------------------------------------

::

   ◊ ABS_CURV = _F(...),

Calculate the arc-length coordinate (called "curvilinear abscissa" in ``Code_Aster``)
for all the SEG elements provided via the keywords :blue:`GROUP_MA` or :blue:`TOUT = 'OUI'`.

For each of the nodes of the elements concerned, one calculates its curvilinear abscissa by taking into
account ts possible curvature.

The keyword :blue:`GROUP_NO_ORIG` allows the user to choose the origin of the curvilinear abscissas 
(the node where the abscissa is zero). The origin must be one of the ends of the line on which we
wish to calculate the curvilinear abscissa.

This option is necessary, for example, to carry out a modal calculation for a tube with an external 
and an internal fluid load, when the density of the external fluid is defined as a function of the 
curvilinear abscissa.

.. image:: u4.23.04/fig4_u4.23.04.png
   :height: 300 px
   :align: center

.. admonition:: Remarks :

   * All the elements involved must be of the type SEG2, SEG3 or SEG4.
   * Elements of the type SEG2 are considered to be straight and elements SEG3 and SEG4 are
     assumed to be in the form of an arc of a circle.
