import salome
import GEOM
import math
from salome.geom import geomBuilder

import SMESH, SALOMEDS
from salome.smesh import smeshBuilder

ExportPATH="/home/banerjee/Salome/forma05/"

# Start salome
salome.salome_init()

#--------------------------------------------
# Create geometry
#--------------------------------------------
geompy = geomBuilder.New()

# Create coordinate system
O = geompy.MakeVertex(0, 0, 0)
OX = geompy.MakeVectorDXDYDZ(1, 0, 0)
OY = geompy.MakeVectorDXDYDZ(0, 1, 0)
OZ = geompy.MakeVectorDXDYDZ(0, 0, 1)

# Geometry parameters
crack_length = 0.1
height = 2.0
width = 1.0

# Create sketch of full plate
sk = geompy.Sketcher2D()
sk.addPoint(0.0, 0.0)
sk.addSegmentAbsolute(0.000000, height/2)
sk.addSegmentAbsolute(width, height/2)
sk.addSegmentAbsolute(width, 0.000000)
sk.addSegmentAbsolute(width, -height/2)
sk.addSegmentAbsolute(0.0, -height/2)
sk.close()

# Add the boundary wire
coord_sys_2D = geompy.MakeMarker(0, 0, 0, 1, 0, 0, 0, 1, 0)
plate_boundary = sk.wire(coord_sys_2D)

# Create the plate face
plate = geompy.MakeFaceWires([plate_boundary], 1)

# Create geometry groups
top_edge = geompy.CreateGroup(plate, geompy.ShapeType["EDGE"])
bot_edge = geompy.CreateGroup(plate, geompy.ShapeType["EDGE"])
top_G = geompy.CreateGroup(plate, geompy.ShapeType["VERTEX"])
top_D = geompy.CreateGroup(plate, geompy.ShapeType["VERTEX"])

# Locate the top edge
v_test =  geompy.MakeVertex(width/2, height/2, 0)
edge = geompy.GetShapesNearPoint(plate, v_test, geompy.ShapeType['EDGE'])
geompy.UnionList(top_edge, [edge])

# Locate point G on the top edge
v_test =  geompy.MakeVertex(0, height/2, 0)
vertex = geompy.GetShapesNearPoint(edge, v_test, geompy.ShapeType['VERTEX'])
geompy.UnionList(top_G, [vertex])

# Locate point D on the top edge
v_test =  geompy.MakeVertex(width, height/2, 0)
vertex = geompy.GetShapesNearPoint(edge, v_test, geompy.ShapeType['VERTEX'])
geompy.UnionList(top_D, [vertex])

# Locate the bottom edge
v_test =  geompy.MakeVertex(width/2, -height/2, 0)
edge = geompy.GetShapesNearPoint(plate, v_test, geompy.ShapeType['EDGE'])
geompy.UnionList(bot_edge, [edge])

# Add all geometric entities to the study
geompy.addToStudy( O, 'O' )
geompy.addToStudy( OX, 'OX' )
geompy.addToStudy( OY, 'OY' )
geompy.addToStudy( OZ, 'OZ' )
geompy.addToStudy( plate, 'plate' )
geompy.addToStudyInFather( plate, top_edge, 'top_edge' )
geompy.addToStudyInFather( plate, bot_edge, 'bot_edge' )
geompy.addToStudyInFather( plate, top_G, 'top_G' )
geompy.addToStudyInFather( plate, top_D, 'top_D' )

#--------------------------------------------
# Create Mesh
#--------------------------------------------
smesh = smeshBuilder.New()
mesh = smesh.Mesh(plate)

# Set up meshing algorithm
Regular_1D = mesh.Segment()
num_seg = Regular_1D.NumberOfSegments(50)

Quadrangle_2D = mesh.Quadrangle(algo=smeshBuilder.QUADRANGLE)
quad_params = Quadrangle_2D.QuadrangleParameters(smeshBuilder.QUAD_STANDARD, -1, [], [])

# Compute the mesh
isDone = mesh.Compute()
mesh.ConvertToQuadratic(0)

# Move geometry groups to the mesh
top_G_1 = mesh.GroupOnGeom(top_G, 'top_G', SMESH.NODE)
top_D_1 = mesh.GroupOnGeom(top_D, 'top_D', SMESH.NODE)
top_edge_1 = mesh.GroupOnGeom(top_edge, 'top_edge', SMESH.EDGE)
bot_edge_1 = mesh.GroupOnGeom(bot_edge, 'bot_edge', SMESH.EDGE)

# Set names of mesh objects
smesh.SetName(mesh.GetMesh(), 'mesh')
smesh.SetName(Regular_1D.GetAlgorithm(), 'Regular_1D')
smesh.SetName(num_seg, 'Regular_1D_params')
smesh.SetName(Quadrangle_2D.GetAlgorithm(), 'Quadrangle_2D')
smesh.SetName(quad_params, 'Quadrangle_2D_params')
smesh.SetName(top_G_1, 'top_G')
smesh.SetName(top_D_1, 'top_D')
smesh.SetName(top_edge_1, 'top_edge')
smesh.SetName(bot_edge_1, 'bot_edge')

# Save mesh
mesh.ExportMED( r''+ExportPATH+'forma05c.mmed'+'')

# Update objecte tree in GUI
if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()
