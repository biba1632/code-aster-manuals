# Start
DEBUT()

# Read mesh
mesh = LIRE_MAILLAGE(FORMAT='MED',);

# If you want to write the mesh out in ASCII format
IMPR_RESU(FORMAT = "RESULTAT", 
          UNITE = 85,
          RESU = _F(MAILLAGE = mesh))

# Assign elements to the mesh
model = AFFE_MODELE(MAILLAGE = mesh,
                    AFFE = (_F(GROUP_MA = ('pipe1','pipe2','elbow'),
                               PHENOMENE = 'MECANIQUE',
                               MODELISATION = 'POU_D_T')))

# Define the material
steel = DEFI_MATERIAU(ELAS = _F(E = 2.04E11,
                                NU = 0.3,
                                RHO = 7800.0))

# Assign material
mater = AFFE_MATERIAU(MAILLAGE = mesh,
                      AFFE = _F(GROUP_MA = 'elbow_pipe',
                                MATER = steel))

# Beam section characteristics
beam_ch = AFFE_CARA_ELEM(MODELE = model,
                         POUTRE = _F(GROUP_MA = 'elbow_pipe',
                                     SECTION = 'CERCLE',
                                     CARA = ('R', 'EP'),
                                     VALE = (0.2, 0.02)))

# Boundary conditions (at point A)
bc_A = AFFE_CHAR_MECA(MODELE = model,
                      DDL_IMPO = _F(GROUP_NO = 'pipe_base',
                                    DX = 0.0,
                                    DY = 0.0,
                                    DZ = 0.0,
                                    DRX = 0.0,
                                    DRY = 0.0,
                                    DRZ = 0.0))

#------------------------
# Calculate first 5 modes
#------------------------

# Assemble the stiffness and mass matrices
ASSEMBLAGE(MODELE = model,
           CHAM_MATER = mater,
           CARA_ELEM = beam_ch,
           CHARGE = bc_A,
           NUME_DDL = CO('num_dof'),
           MATR_ASSE = (_F(MATRICE = CO('stif_mat'),
                           OPTION = 'RIGI_MECA'),
                        _F(MATRICE = CO('mass_mat'),
                           OPTION = 'MASS_MECA')),
           VECT_ASSE = (_F(VECTEUR = CO("f_0"),
                           OPTION = 'CHAR_MECA')))

# Compute modes
modes = CALC_MODES(MATR_RIGI = stif_mat,
                   MATR_MASS = mass_mat,
                   OPTION = 'PLUS_PETITE',
                   CALC_FREQ = _F(NMAX_FREQ = 5))

# Save modal calc. results
IMPR_RESU(FORMAT = 'MED',
          RESU = _F(MAILLAGE = mesh,
                    RESULTAT = modes,
                    NOM_CHAM = 'DEPL'))

#----------------------------------------
# Transient analysis (mode superposition)
#----------------------------------------

# Force boundary conditions (at point B)
bc_B = AFFE_CHAR_MECA(MODELE = model,
                      FORCE_NODALE = _F(GROUP_NO = 'end_face',
                                        FY = 1.E7))

# Assemble the stiffness and mass matrices
ASSEMBLAGE(MODELE = model,
           CARA_ELEM = beam_ch,
           CHARGE = bc_A,
           NUME_DDL = num_dof,
           VECT_ASSE = (_F(VECTEUR = CO("f_y"),
                           OPTION = 'CHAR_MECA',
                           CHARGE = bc_B)))

# Project the stiffness and mass matrices on to the modal basis
PROJ_BASE(BASE = modes,
          MATR_ASSE_GENE = (_F(MATRICE = CO('mass_gen'),
                               MATR_ASSE = mass_mat),
                            _F(MATRICE = CO('stif_gen'),
                               MATR_ASSE = stif_mat)),
          VECT_ASSE_GENE = _F(VECTEUR = CO('f_y_gen'),
                              VECT_ASSE = f_y,
                              TYPE_VECT = 'FORC'))

# Define a harmonic function for the load
freq = 121.0;
fy_func = FORMULE(VALE = 'cos(freq*INST)',
                  freq = freq,
                  NOM_PARA = 'INST')

# Interpolate the function with a fine time discretization
t_interp = DEFI_LIST_REEL(DEBUT = 0.0,
                          INTERVALLE = _F(JUSQU_A = 2.0,
                                          PAS = 1.E-4))

fy_inter = CALC_FONC_INTERP(FONCTION = fy_func,
                            LIST_PARA = t_interp,
                            NOM_RESU = 'fy_func',
                            NOM_PARA = 'INST')

# Create a coarser time list
t_coarse = DEFI_LIST_REEL(DEBUT = 0.0,
                          INTERVALLE = _F(JUSQU_A = 2.0,
                                          NOMBRE = 1001))

# Compute transient response
tran_res = DYNA_VIBRA(TYPE_CALCUL = 'TRAN',
                      BASE_CALCUL = 'GENE',
                      MATR_MASS = mass_gen,
                      MATR_RIGI = stif_gen,
                      AMOR_MODAL = _F(AMOR_REDUIT = 0.05),
                      INCREMENT = _F(PAS = 1.E-4,
                                     INST_INIT = 0.0,
                                     INST_FIN = 2.0),
                      SCHEMA_TEMPS = _F(SCHEMA='DIFF_CENTRE'),
                      EXCIT = _F(VECT_ASSE_GENE = f_y_gen,
                                 FONC_MULT = fy_inter))

# Extract the displacements at B
u_y_B = RECU_FONCTION(RESU_GENE = tran_res,
                      LIST_INST = t_coarse,
                      INTERP_NUME = 'LIN',
                      NOM_CHAM = 'DEPL',
                      NOM_CMP = 'DY',
                      GROUP_NO = 'end_face',
                      INTERPOL = 'LIN')

# Save the displacements at B
IMPR_FONCTION(FORMAT = 'XMGRACE',
              COURBE = _F(FONCTION = u_y_B,
                          LEGENDE = 'u_y_B',
                          FREQ_MARQUEUR =0),
              TITRE ='Displacement of pipe at B')

# Convert results back into physical basis from
# generalized coordinates
tran_gen = REST_GENE_PHYS(RESU_GENE = tran_res,
                          LIST_INST = t_coarse,
                          INTERPOL = 'LIN',
                          TOUT_CHAM = 'OUI')

# Compute beam stresses in physical basis 
tran_sig = CALC_CHAMP(MODELE = model,
                      CHAM_MATER = mater,
                      CARA_ELEM = beam_ch,
                      RESULTAT = tran_gen,
                      CONTRAINTE = ('SIPO_ELNO','SIPO_NOEU'))

# Save transient calc. results
IMPR_RESU(FORMAT = 'MED',
          UNITE = 86,
          RESU = _F(MAILLAGE = mesh,
                    RESULTAT = tran_sig,
                    NOM_CHAM = ('DEPL','SIPO_ELNO','SIPO_NOEU')))

# Extract the time instants when min and max stresses are attained 
t_min = CREA_CHAMP(TYPE_CHAM = 'ELNO_SIEF_R',
                   OPERATION = 'EXTR',
                   RESULTAT  = tran_sig,
                   NOM_CHAM  = 'SIPO_ELNO',
                   TYPE_MAXI = 'MINI',
                   TYPE_RESU = 'INST',
                   LIST_INST = t_coarse,
                   PRECISION = 1.0E-6)
t_max = CREA_CHAMP(TYPE_CHAM = 'ELNO_SIEF_R',
                   OPERATION = 'EXTR',
                   RESULTAT  = tran_sig,
                   NOM_CHAM  = 'SIPO_ELNO',
                   TYPE_MAXI = 'MAXI',
                   TYPE_RESU = 'INST',
                   LIST_INST = t_coarse,
                   PRECISION = 1.0E-6)

# Extract the values of min and max stress
sig_min = CREA_CHAMP(TYPE_CHAM = 'ELNO_SIEF_R',
                     OPERATION = 'EXTR',
                     RESULTAT  = tran_sig,
                     NOM_CHAM  = 'SIPO_ELNO',
                     TYPE_MAXI = 'MINI',
                     TYPE_RESU = 'VALE',
                     LIST_INST = t_coarse,
                     PRECISION = 1.0E-6)
sig_max = CREA_CHAMP(TYPE_CHAM = 'ELNO_SIEF_R',
                     OPERATION = 'EXTR',
                     RESULTAT  = tran_sig,
                     NOM_CHAM  = 'SIPO_ELNO',
                     TYPE_MAXI = 'MAXI',
                     TYPE_RESU = 'VALE',
                     LIST_INST = t_coarse,
                     PRECISION = 1.0E-6)

# Save the time and min/max stress in ASCII format
IMPR_RESU(MODELE = model,
          FORMAT = 'RESULTAT',
          RESU = (_F(CHAM_GD = t_max),
                  _F(CHAM_GD = sig_max)))
IMPR_RESU(MODELE = model,
          FORMAT = 'RESULTAT',
          RESU = (_F(CHAM_GD = t_min),
                  _F(CHAM_GD = sig_min)))

# Extract the value of stress at the time when min/max
# absolute stresses are attained
s_min = CREA_CHAMP(TYPE_CHAM = 'ELNO_SIEF_R',
                   OPERATION = 'EXTR',
                   RESULTAT  = tran_sig,
                   NOM_CHAM  = 'SIPO_ELNO',
                   TYPE_MAXI = 'MINI_ABS',
                   TYPE_RESU = 'VALE',
                   LIST_INST = t_coarse,
                   PRECISION = 1.0E-6)
s_max = CREA_CHAMP(TYPE_CHAM = 'ELNO_SIEF_R',
                   OPERATION = 'EXTR',
                   RESULTAT  = tran_sig,
                   NOM_CHAM  = 'SIPO_ELNO',
                   TYPE_MAXI = 'MAXI_ABS',
                   TYPE_RESU = 'VALE',
                   LIST_INST = t_coarse,
                   PRECISION = 1.0E-6)

# Extract the absolute value of stress at the time when min/max
# absolute stresses are attained
s_min_ab = CREA_CHAMP(TYPE_CHAM = 'ELNO_SIEF_R',
                      OPERATION = 'EXTR',
                      RESULTAT  = tran_sig,
                      NOM_CHAM  = 'SIPO_ELNO',
                      TYPE_MAXI = 'MINI_ABS',
                      TYPE_RESU = 'VALE_ABS',
                      LIST_INST = t_coarse,
                      PRECISION = 1.0E-6)
s_max_ab = CREA_CHAMP(TYPE_CHAM = 'ELNO_SIEF_R',
                      OPERATION = 'EXTR',
                      RESULTAT  = tran_sig,
                      NOM_CHAM  = 'SIPO_ELNO',
                      TYPE_MAXI = 'MAXI_ABS',
                      TYPE_RESU = 'VALE_ABS',
                      LIST_INST = t_coarse,
                      PRECISION = 1.0E-6)

# Save the min/max stress and absolute values in ASCII format
IMPR_RESU(MODELE = model,
          FORMAT = 'RESULTAT',
          RESU = (_F(CHAM_GD = s_max),
                  _F(CHAM_GD = s_min),
                  _F(CHAM_GD = s_max_ab),
                  _F(CHAM_GD = s_min_ab)))

# Check the results at t = 2 s
# (SN - axial force, SVY - transverse shear in Y, 
#  SMFZ - bending moment around Z)
TEST_RESU(RESU = (_F(GROUP_NO = 'pipe_base',
                     INST = 2.0,
                     RESULTAT = tran_sig,
                     NOM_CHAM = 'SIPO_NOEU',
                     NOM_CMP = 'SN',
                     VALE_CALC = -485268450.783),
                  _F(GROUP_NO = 'pipe_base',
                     INST = 2.0,
                     RESULTAT = tran_sig,
                     NOM_CHAM = 'SIPO_NOEU',
                     NOM_CMP = 'SVY',
                     VALE_CALC = 2254071472.91),
                  _F(GROUP_NO = 'pipe_base',
                     INST = 2.0,
                     RESULTAT = tran_sig,
                     NOM_CHAM = 'SIPO_NOEU',
                     NOM_CMP = 'SMFZ',
                     VALE_CALC = -25778733306.6)))

# Check the computed min/max values.
TEST_RESU(CHAM_ELEM = (_F(CHAM_GD   = s_max,
                          GROUP_MA  = 'M1',
                          GROUP_NO  = 'N1',
                          NOM_CMP   = 'SN',
                          VALE_CALC = -8.20094496775807E+08),
                       _F(CHAM_GD = s_max,
                          GROUP_MA  = 'M9',
                          GROUP_NO  = 'N12',
                          NOM_CMP   = 'SMFZ',
                          VALE_CALC = 1.93299612798964E+10)))

TEST_RESU(CHAM_ELEM = (_F(CHAM_GD   = s_max_ab,
                          GROUP_MA  = 'M1',
                          GROUP_NO  = 'N1',
                          NOM_CMP   = 'SN',
                          VALE_CALC = 8.20094496775807E+08,
                          VALE_REFE = 8.20094496775807E+08,
                          REFERENCE = 'AUTRE_ASTER'),
                       _F(CHAM_GD   = s_max_ab,
                          GROUP_MA  = 'M9',
                          GROUP_NO  = 'N12',
                          NOM_CMP   = 'SMFZ',
                          VALE_CALC = 1.93299612798964E+10,
                          VALE_REFE = 1.93299612798964E+10,
                          REFERENCE = 'AUTRE_ASTER')))

TEST_RESU(CHAM_ELEM = (_F(CHAM_GD   = s_min,
                          GROUP_MA  = 'M1',
                          GROUP_NO  = 'N1',
                          NOM_CMP   = 'SN',
                          VALE_CALC = -2.65282777575495E+04),
                       _F(CHAM_GD   = s_min,
                          GROUP_MA  = 'M1',
                          GROUP_NO  = 'N5',
                          NOM_CMP   = 'SN',
                          VALE_CALC = 4.44886809226435E+04)))

TEST_RESU(CHAM_ELEM = (_F(CHAM_GD   = s_min_ab,
                          GROUP_MA  = 'M1',
                          GROUP_NO  = 'N1',
                          NOM_CMP   = 'SN',
                          VALE_CALC = 2.65282777575495E+04,
                          VALE_REFE = 2.65282777575495E+04,
                          REFERENCE = 'AUTRE_ASTER'),
                       _F(CHAM_GD   = s_min_ab,
                          GROUP_MA  = 'M1',
                          GROUP_NO  = 'N5',
                          NOM_CMP   = 'SN',
                          VALE_CALC = 4.44886809226435E+04,
                          VALE_REFE = 4.44886809226435E+04,
                          REFERENCE = 'AUTRE_ASTER')))

#Finish
FIN();
