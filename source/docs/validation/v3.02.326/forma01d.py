# coding=utf-8

import salome
from salome.geom import geomBuilder
from salome.smesh import smeshBuilder
import NETGENPlugin

geompy = geomBuilder.New()
smesh = smeshBuilder.New()

#===================================================
#                  GEOMETRY
#===================================================
Plate = geompy.MakeFaceHW(100, 150, 1)

geompy.TranslateDXDYDZ(Plate, 50, 75, 0)

[left, bottom, top, right] = geompy.SubShapeAllSorted(Plate, geompy.ShapeType["EDGE"])

id_Plate = geompy.addToStudy( Plate, 'Plate' )
geompy.addToStudyInFather( Plate, left, 'left' )
geompy.addToStudyInFather( Plate, bottom, 'bottom' )
geompy.addToStudyInFather( Plate, top, 'top' )
geompy.addToStudyInFather( Plate, right, 'right' )

#===================================================
#                  Mesh
#===================================================
M_Plate = smesh.Mesh(Plate,"Plate")

NETGEN_2D = M_Plate.Triangle(algo=smeshBuilder.NETGEN)
NETGEN_2D_Parameters = NETGEN_2D.Parameters()
NETGEN_2D_Parameters.SetMaxSize( 5 )
NETGEN_2D_Parameters.SetSecondOrder( 1 )
NETGEN_2D_Parameters.SetOptimize( 1 )
NETGEN_2D_Parameters.SetFineness( 3 )
NETGEN_2D_Parameters.SetQuadAllowed( 0 )

isDone = M_Plate.Compute()
if not isDone:
    raise RuntimeError( 'Mesh  {}: computation failed'\
                        .format(M_Plate.GetMesh()) )

M_Plate.Group(left, 'left')
M_Plate.Group(right, 'right')
M_Plate.Group(bottom, 'bottom')
M_Plate.Group(top, 'top')

# Export the mesh in MED format
ExportPATH="/home/banerjee/Salome/forma01d/"
medFile = "plate.med"
M_Plate.ExportMED( r'' + ExportPATH + medFile + '')

# Update the GUI objects
if salome.sg.hasDesktop():
   gg = salome.ImportComponentGUI("GEOM")
   gg.createAndDisplayFitAllGO(id_Plate)
   salome.sg.updateObjBrowser()
