# SSNV209R: 3D, ELEMENTS HEXA20, XFEM INTERFACE
DEBUT(IGNORE_ALARM = 'MODELE1_63',
      LANG = 'EN')

# Define crack location
y0 = 0.005

# Define contact constraint parameters
H_plus = -2.

# Define material properties
E = 1.3E11
nu = 0.2
rho = 7800.

# Read mesh
mesh_in = LIRE_MAILLAGE(FORMAT = 'MED',
                        UNITE = 20)

# Convert HEXA8 to HEXA20
mesh = CREA_MAILLAGE(MAILLAGE = mesh_in,
                     LINE_QUAD = _F(TOUT = 'OUI'))

# Define node groups for the 0D elements
mesh = DEFI_GROUP(reuse = mesh,
                  CREA_GROUP_NO = _F(GROUP_MA = ('p1', 'p2', 'p3', 'p4', 'ptop')),
                  MAILLAGE = mesh)

# Define node groups from some element groups and an element group
mesh = DEFI_GROUP(reuse = mesh,
                  MAILLAGE = mesh,
                  CREA_GROUP_NO = (_F(GROUP_MA = 'vol'),
                                   _F(GROUP_MA = 'line3'),
                                   _F(GROUP_MA = 'sline1'),
                                   _F(NOM = 'p4P',
                                      OPTION = 'ENV_SPHERE',
                                      POINT = (0.0, 0.04, 0.001),
                                      PRECISION = 0.0001,
                                      RAYON = 0.0001),
                                   _F(NOM = 'p3P',
                                      OPTION = 'ENV_SPHERE',
                                      POINT = (0.08, 0.04, 0.001),
                                      PRECISION = 0.0001,
                                      RAYON = 0.0001),
                                   _F(NOM = 'fixedX',
                                      OPTION = 'PLAN',
                                      POINT = (0.04, 0.0, 0.0),
                                      PRECISION = 0.001,
                                      VECT_NORMALE = (1.0, 0.0, 0.0))),
                  CREA_GROUP_MA = (_F(GROUP_NO_CENTRE = 'p4',
                                      NOM = 'plate_t',
                                      OPTION = 'CYLINDRE',
                                      RAYON = 0.0399,
                                      VECT_NORMALE = (1.0, 0.0, 0.0)),
                                   _F(DIFFE = ('plate_t', 'surf', 'line1', 'line2', 'line3', 'line4', 
                                               'p1', 'p2', 'p3', 'p4', 'ptop'),
                                      NOM = 'plate'),
                                   _F(GROUP_NO_CENTRE = 'p1',
                                      NOM = 'base_t',
                                      OPTION = 'CYLINDRE',
                                      RAYON = 0.006,
                                      VECT_NORMALE = (1.0, 0.0, 0.0)),
                                   _F(DIFFE = ('base_t', 'surf', 'line1', 'line2', 'line3', 'line4',
                                               'p1', 'p2', 'p3', 'p4', 'ptop'),
                                      NOM = 'base')),
                  INFO = 1)

# Define node groups for the crack
mesh = DEFI_GROUP(reuse = mesh,
                  MAILLAGE = mesh,
                  CREA_GROUP_NO = (_F(NOM = 'crack',
                                      OPTION = 'PLAN',
                                      POINT = (0.0, 0.0, 0.0),
                                      PRECISION = 1.2e-03,
                                      VECT_NORMALE = (0.0, 1.0, 0.0)),
                                   _F(NOM = 'crack_top',
                                      OPTION = 'PLAN',
                                      POINT = (0.0, y0, 0.0),
                                      PRECISION = 3.0e-03,
                                      VECT_NORMALE = (0.0, 1.0, 0.0))),
                  INFO = 1)

# Change the face orientations if needed
mesh = MODI_MAILLAGE(reuse = mesh,
                     MAILLAGE = mesh,
                     ORIE_PEAU_3D = (_F(GROUP_MA = 'sline2'),
                                     _F(GROUP_MA = 'sline3'),
                                     _F(GROUP_MA = 'sline4')))

# Assign 3D model
model_in = AFFE_MODELE(AFFE = (_F(GROUP_MA = 'sline3',
                                  MODELISATION = '3D',
                                  PHENOMENE = 'MECANIQUE'),
                               _F(GROUP_MA = 'sline1',
                                  MODELISATION = '3D',
                                  PHENOMENE = 'MECANIQUE'),
                               _F(GROUP_MA = 'vol',
                                  MODELISATION = '3D',
                                  PHENOMENE = 'MECANIQUE'),
                               _F(GROUP_MA = 'sline2',
                                  MODELISATION = '3D',
                                  PHENOMENE = 'MECANIQUE'),
                               _F(GROUP_MA = 'sline4',
                                  MODELISATION = '3D',
                                  PHENOMENE = 'MECANIQUE')),
                       INFO = 1,
                       MAILLAGE = mesh)

# Create the X-FEM crack
f_crack = FORMULE(NOM_PARA = ['X', 'Y', 'Z'],
                  VALE = 'Y')

crack = DEFI_FISS_XFEM(DEFI_FISS = _F(FONC_LN = f_crack),
                       INFO = 1,
                       MAILLAGE = mesh,
                       TYPE_DISCONTINUITE = 'INTERFACE')

# Create crack node number field
crac_fld = CREA_CHAMP(FISSURE = crack,
                      NOM_CHAM = 'LNNO',
                      OPERATION = 'EXTR',
                      TYPE_CHAM = 'NOEU_NEUT_R')

# Add X-FEM model
model_xf = MODI_MODELE_XFEM(CONTACT = 'STANDARD',
                            FISSURE = crack,
                            INFO = 1,
                            MODELE_IN = model_in)

# Define material
mat_data = DEFI_MATERIAU(ELAS = _F(E = E,
                                   NU = nu,
                                   RHO = rho))

# Assign material
mater = AFFE_MATERIAU(AFFE = _F(GROUP_MA = 'vol',
                                MATER = mat_data),
                      AFFE_VARC = _F(CHAM_GD = crac_fld,
                                     NOM_VARC = 'NEUT1'),
                      MAILLAGE = mesh,
                      MODELE = model_xf)

# Define contact BCs
contact = DEFI_CONTACT(FORMULATION = 'XFEM',
                       FROTTEMENT = 'COULOMB',
                       INFO = 2,
                       ITER_CONT_MAXI = 5,
                       ITER_FROT_MAXI = 20,
                       RESI_FROT = 1.0e-2,
                       REAC_GEOM = 'SANS',
                       MODELE = model_xf,
                       ZONE = _F(ALGO_CONT = 'STANDARD',
                                 ALGO_FROT = 'STANDARD',
                                 ALGO_LAGR = 'VERSION2',
                                 COEF_CONT = 120000.0,
                                 COEF_FROT = 100000.0,
                                 CONTACT_INIT = 'OUI',
                                 COULOMB = 1.0,
                                 FISS_MAIT = crack,
                                 INTEGRATION = 'NOEUD'))

# Define kinematic BCs
bc_kin = AFFE_CHAR_MECA(MODELE = model_xf,
                        DDL_IMPO = (_F(GROUP_MA = 'base',
                                       DX = 0.,
                                       DY = 0.,
                                       DZ = 0.),
                                    _F(GROUP_NO = ('fixedX'),
                                       DX = 0.),
                                    _F(GROUP_NO = ('p3', 'p4', 'p3P', 'p4P'),
                                       DZ = 0.)),
                        LIAISON_GROUP = (_F(GROUP_NO_1 = ('crack_top'), # --- DX ---
                                            GROUP_NO_2 = ('crack_top'),
                                            SANS_GROUP_NO = 'fixedX',
                                            DDL_1 = ('DX', 'H1X'),
                                            DDL_2 = ('DX', 'H1X'),
                                            COEF_MULT_1 = (0., 0.),
                                            COEF_MULT_2 = (1., H_plus),
                                            COEF_IMPO =  0.),
                                         _F(GROUP_NO_1 = ('crack_top'), # --- DY ---
                                            GROUP_NO_2 = ('crack_top'),
                                            SANS_GROUP_NO = 'fixedX',
                                            DDL_1 = ('DY', 'H1Y'),
                                            DDL_2 = ('DY', 'H1Y'),
                                            COEF_MULT_1 = (0., 0.),
                                            COEF_MULT_2 = (1., H_plus),
                                            COEF_IMPO =  0.),
                                         _F(GROUP_NO_1 = ('crack_top'), # --- DY ---
                                            GROUP_NO_2 = ('crack_top'),
                                            DDL_1 = ('DZ', 'H1Z'),
                                            DDL_2 = ('DZ', 'H1Z'),
                                            COEF_MULT_1 = (0., 0.),
                                            COEF_MULT_2 = (1., H_plus),
                                            COEF_IMPO =  0.)))

# Define constant pressure BCs
bc_c = AFFE_CHAR_MECA(MODELE = model_xf,
                      PRES_REP = (_F(GROUP_MA = 'sline3',
                                     PRES = 5.E7)))

# Define y-varying pressure BCs
def pressure(y) :
   if y < -1.0e-15:  return 0.E07
   if y >  1.0e-15:  return 15.E7
   return 0.

f_pres = FORMULE(VALE = 'pressure(Y)',
                 pressure = pressure,
                 NOM_PARA = ['X', 'Y'])

bc_v = AFFE_CHAR_MECA_F(MODELE = model_xf,
                        PRES_REP = _F(GROUP_MA = ('sline2','sline4'),
                                      PRES = f_pres))


# Define ramp function for time stepping
ramp = DEFI_FONCTION(NOM_PARA = 'INST',
                     PROL_GAUCHE = 'LINEAIRE',
                     PROL_DROITE = 'LINEAIRE',
                     VALE = (0.0, 0.0,
                             1.0, 1.0))

# Define time steps
l_times = DEFI_LIST_REEL(DEBUT = 0.0,
                         INTERVALLE = _F(JUSQU_A = 1.0,
                                         NOMBRE = 1))

# Solve
result = STAT_NON_LINE(MODELE = model_xf,
                       CHAM_MATER = mater,
                       EXCIT = (_F(CHARGE = bc_kin),
                                _F(CHARGE = bc_c,
                                   FONC_MULT = ramp),
                                _F(CHARGE = bc_v,
                                   FONC_MULT = ramp)),
                       CONTACT  = contact,
                       COMPORTEMENT = _F(RELATION = 'ELAS',
                                         GROUP_MA = 'vol'),
                       INCREMENT = _F(LIST_INST = l_times,
                                      INST_FIN = 1.0),
                       CONVERGENCE = _F(ARRET = 'OUI',
                                        ITER_GLOB_MAXI = 50,
                                        RESI_GLOB_RELA = 1.E-6),
                       SOLVEUR = _F(METHODE = 'MUMPS',
                                    PCENT_PIVOT = 200,
                                    NPREC = -1,
                                    RESI_RELA = -1.0),
                       NEWTON = _F(REAC_ITER = 1),
                       ARCHIVAGE = _F(CHAM_EXCLU = 'VARI_ELGA'),
                       INFO = 1)

# Post-process for visualization
mesh_xfe = POST_MAIL_XFEM(MODELE = model_xf)

mesh_xfe = DEFI_GROUP(reuse = mesh_xfe,
                      MAILLAGE = mesh_xfe,
                      DETR_GROUP_NO = _F(NOM = 'crack'),
                      CREA_GROUP_NO = (_F(OPTION = 'ENV_SPHERE',
                                          NOM = 'pPA',
                                          POINT = (0.,0., 0.),
                                          RAYON = 0.000001,
                                          PRECISION = 0.00001),
                                       _F(OPTION = 'ENV_SPHERE',
                                          NOM = 'pPC',
                                          POINT = (0.005,0., 0.),
                                          RAYON = 0.000001,
                                          PRECISION = 0.00001)),
                      INFO = 1)

mod_viz = AFFE_MODELE(MAILLAGE = mesh_xfe,
                      AFFE = _F(TOUT = 'OUI',
                                PHENOMENE = 'MECANIQUE',
                                MODELISATION = '3D'))

resu_xfe = POST_CHAM_XFEM(MODELE_VISU = mod_viz,
                          RESULTAT = result)

# Save the mesh for visualization
IMPR_RESU(UNITE = 80,
          FORMAT = 'MED',
          RESU = _F(MAILLAGE = mesh_xfe))

IMPR_RESU(UNITE = 81,
          FORMAT = 'MED',
          RESU = _F(RESULTAT = resu_xfe))

# Extract crack displacement into table (POST_MAIL_XFEM creates NFISSU for crack)
u_crack = POST_RELEVE_T(ACTION = _F(INTITULE = 'disp_crack',
                                    GROUP_NO = ('NFISSU'),
                                    RESULTAT = resu_xfe,
                                    NOM_CHAM = 'DEPL',
                                    INST = 1.0,
                                    TOUT_CMP = 'OUI',
                                    OPERATION = 'EXTRACTION'))

u_crack = CALC_TABLE(reuse = u_crack,
                     TABLE = u_crack,
                     ACTION = (_F(OPERATION = 'FILTRE',
                                  NOM_PARA  = 'NOEUD',
                                  CRIT_COMP = 'REGEXP',
                                  VALE_K    = 'N[^M]')))

u_crack = CALC_TABLE(reuse = u_crack,
                     TABLE = u_crack,
                     ACTION = _F(OPERATION = 'TRI',
                                 NOM_PARA = 'COOR_X',
                                 ORDRE = 'CROISSANT'))

# Write table
IMPR_TABLE(UNITE = 8,
           TABLE = u_crack,
           SEPARATEUR = ',')

# Extract displacement and contact pressure at A into table
u_A = POST_RELEVE_T(ACTION = _F(INTITULE = 'u_A',
                                GROUP_NO = ('pPA'),
                                RESULTAT = resu_xfe,
                                NOM_CHAM = 'DEPL',
                                INST = 1.0,
                                TOUT_CMP = 'OUI',
                                OPERATION = 'EXTRACTION'))

u_A = CALC_TABLE(reuse = u_A,
                 TABLE = u_A,
                 ACTION = (_F(OPERATION = 'FILTRE',
                              NOM_PARA  = 'NOEUD',
                              CRIT_COMP = 'REGEXP',
                              VALE_K    = 'N[^M]')))

# Extract displacement and contact pressure at C into table
u_C = POST_RELEVE_T(ACTION = _F(INTITULE = 'u_C',
                                GROUP_NO = ('pPC'),
                                RESULTAT = resu_xfe,
                                NOM_CHAM = 'DEPL',
                                INST = 1.0,
                                TOUT_CMP = 'OUI',
                                OPERATION = 'EXTRACTION'))

u_C = CALC_TABLE(reuse = u_C,
                 TABLE = u_C,
                 ACTION = (_F(OPERATION = 'FILTRE',
                              NOM_PARA  = 'NOEUD',
                              CRIT_COMP = 'REGEXP',
                              VALE_K    = 'N[^M]')))

#----------------------------------------------
# Verification tests
#----------------------------------------------
TEST_TABLE(CRITERE = 'RELATIF',
           REFERENCE = 'AUTRE_ASTER',
           PRECISION = 3.E-2,
           VALE_CALC = 2.98564925503E-05,
           VALE_REFE = 3.06971229984E-05,
           NOM_PARA = 'DX',
           TYPE_TEST = 'MAX',
           TABLE = u_A)

TEST_TABLE(CRITERE = 'RELATIF',
           REFERENCE = 'AUTRE_ASTER',
           PRECISION = 5.E-2,
           VALE_CALC = 2.36310731675E-05,
           VALE_REFE = 2.47360419196E-05,
           NOM_PARA = 'DX',
           TYPE_TEST = 'MAX',
           TABLE = u_C)

# End
FIN()
