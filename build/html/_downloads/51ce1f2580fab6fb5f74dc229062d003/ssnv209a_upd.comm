# SSNV209A: 2D, ELEMENTS Q4, METHODE CONTINUE

DEBUT(LANG = 'EN')

# Read mesh
mesh = LIRE_MAILLAGE(FORMAT = 'MED')

# Create node groups for the 0D elements
mesh = DEFI_GROUP(reuse = mesh,
                  MAILLAGE = mesh,
                  CREA_GROUP_NO = (_F(GROUP_MA = ('pBA', 'pBS', 'pCA', 'pCS',
                                                  'pPA', 'pPB', 'pPC', 'pPD', 
                                                  'pPE', 'pPR', 'pPI', 'pPS',
                                                  'pHA', 'pHS'))))

# Create node groups for the contact interface and crack normal
mesh = DEFI_GROUP(reuse = mesh,
                  MAILLAGE = mesh,
                  CREA_GROUP_NO = (_F(NOM = 'interf',
                                      GROUP_MA = 'lConta',
                                      OPTION = 'NOEUD_ORDO',
                                      GROUP_NO_ORIG = 'pPA',
                                      GROUP_NO_EXTR = 'pPS'),
                                   _F(NOM = 'crack_n',
                                      OPTION = 'PLAN',
                                      POINT = (0.,0.),
                                      VECT_NORMALE = (0.,1.),
                                      PRECISION = 1.E-6)))

# Change face orientations if needed
mesh = MODI_MAILLAGE(reuse = mesh,
                     MAILLAGE = mesh,
                     ORIE_PEAU_2D = (_F(GROUP_MA = 'lPresV'),
                                     _F(GROUP_MA = 'lPresH'),
                                     _F(GROUP_MA = 'lFixedX'),
                                     _F(GROUP_MA = 'lConta'),
                                     _F(GROUP_MA = 'lBase')))

# Assign plane strain model
model = AFFE_MODELE(MAILLAGE = mesh,
                    AFFE = _F(TOUT = 'OUI',
                               PHENOMENE = 'MECANIQUE',
                               MODELISATION = 'D_PLAN'))

# Define materials
plat_mat = DEFI_MATERIAU(ELAS = _F(E = 1.3E11,
                                   NU = 0.2))

base_mat = DEFI_MATERIAU(ELAS = _F(E = 1.3E16,
                                   NU = 0.0))

# Assign materials
mater = AFFE_MATERIAU(MAILLAGE = mesh,
                      AFFE = (_F(GROUP_MA = 'sPlate',
                                 MATER = plat_mat),
                              _F(GROUP_MA = 'sBase',
                                 MATER = base_mat)))

# Boundary conditions (fixed)
bc_c = AFFE_CHAR_MECA(MODELE = model,
                      DDL_IMPO = (_F(GROUP_MA = 'sBase',  
                                     DX = 0.0, 
                                     DY = 0.0)),
                      PRES_REP = (_F(GROUP_MA = 'lPresV', 
                                     PRES = 5.E07)))

# Boundary conditions (y-varying)
def pressure(y) :
   if y < 1.e-15 : return 0.E07
   if y > 1.e-15 : return  15.E07
   if y == 1.e-15 : return  0.

f_pres = FORMULE(VALE = 'pressure(Y)',
                 pressure = pressure,
                 NOM_PARA = ['X', 'Y'])

bc_v = AFFE_CHAR_MECA_F(MODELE = model,
                        PRES_REP = _F(GROUP_MA = ('lPresH', 'lFixedX'),
                                      PRES = f_pres))

# Define contact conditions
contact = DEFI_CONTACT(MODELE = model,
                       FORMULATION = 'CONTINUE',
                       FROTTEMENT = 'COULOMB',
                       ALGO_RESO_CONT = 'NEWTON',
                       ALGO_RESO_GEOM = 'NEWTON',
                       ALGO_RESO_FROT = 'NEWTON',
                       ZONE = (_F(GROUP_MA_MAIT = 'lBase',
                                  GROUP_MA_ESCL = 'lConta',
                                  APPARIEMENT   = 'MAIT_ESCL',
                                  CONTACT_INIT  = 'INTERPENETRE',
                                  SEUIL_INIT    = 1.,
                                  COULOMB       = 1.0,
                                  ADAPTATION    = 'TOUT')))

# Define ramp function for time stepping
ramp = DEFI_FONCTION(NOM_PARA = 'INST',
                     PROL_GAUCHE = 'LINEAIRE',
                     PROL_DROITE = 'LINEAIRE',
                     VALE = (0.0, 0.0,
                             1.0, 1.0))

# Define time steps
l_times = DEFI_LIST_REEL(DEBUT = 0.,
                         INTERVALLE = _F(JUSQU_A = 1.0, 
                                         NOMBRE = 1))

# Solve
result = STAT_NON_LINE(MODELE = model,
                       CHAM_MATER = mater,
                       EXCIT = (_F(CHARGE = bc_c,
                                   FONC_MULT = ramp),
                                _F(CHARGE = bc_v,
                                   FONC_MULT = ramp)),
                       CONTACT = contact,
                       COMPORTEMENT = _F(RELATION = 'ELAS'),
                       NEWTON = _F(REAC_ITER = 1),
                       INCREMENT = _F(LIST_INST = l_times),
                       CONVERGENCE = _F(ARRET = 'OUI',
                                        ITER_GLOB_MAXI = 50,
                                        RESI_GLOB_RELA = 1.0E-6),
                       SOLVEUR = _F(METHODE = 'MUMPS'))

# Save results
IMPR_RESU(FORMAT = 'MED',
          UNITE = 80,
          RESU = _F(RESULTAT = result))

# Regression tests
TEST_RESU(RESU = (_F(GROUP_NO = 'pPR',
                     INST = 1.0,
                     RESULTAT = result,
                     NOM_CHAM = 'CONT_NOEU',
                     NOM_CMP = 'JEU',
                     VALE_CALC =  0.0E+00,
                     CRITERE = 'ABSOLU'),
                  _F(GROUP_NO = 'pPR',
                     INST = 1.0,
                     RESULTAT = result,
                     NOM_CHAM = 'CONT_NOEU',
                     NOM_CMP = 'CONT',
                     VALE_CALC = 1.0,
                     CRITERE = 'RELATIF'),
                  _F(GROUP_NO = 'pPR',
                     INST = 1.0,
                     RESULTAT = result,
                     NOM_CHAM = 'CONT_NOEU',
                     NOM_CMP = 'RN',
                     VALE_CALC = 1.04863767875E05,
                     CRITERE = 'RELATIF'),
                  _F(GROUP_NO = 'pPA',
                     INST = 1.0,
                     RESULTAT = result,
                     NOM_CHAM = 'DEPL',
                     NOM_CMP = 'DX',
                     VALE_CALC = 2.84594384304E-05),
                  _F(GROUP_NO = 'pPB',
                     INST = 1.0,
                     RESULTAT = result,
                     NOM_CHAM = 'DEPL',
                     NOM_CMP = 'DX',
                     VALE_CALC = 2.70792364103E-05),
                  _F(GROUP_NO = 'pPC',
                     INST = 1.0,
                     RESULTAT = result,
                     NOM_CHAM = 'DEPL',
                     NOM_CMP = 'DX',
                     VALE_CALC = 2.27402555462E-05),
                  _F(GROUP_NO = 'pPD',
                     INST = 1.0,
                     RESULTAT = result,
                     NOM_CHAM = 'DEPL',
                     NOM_CMP = 'DX',
                     VALE_CALC = 1.97270669318E-05),
                  _F(GROUP_NO = 'pPE',
                     INST = 1.0,
                     RESULTAT = result,
                     NOM_CHAM = 'DEPL',
                     NOM_CMP = 'DX',
                     VALE_CALC = 1.53641594362E-05)))
# End  
FIN()
