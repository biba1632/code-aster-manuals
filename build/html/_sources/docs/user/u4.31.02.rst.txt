.. role:: red
.. role:: blue
.. role:: green
.. role:: gray
.. role:: python(code)
   :language: python
.. |br| raw:: html

   <br/>

.. index:: ! DEFI_FONCTION

.. _u4.31.02:

****************************************************************************
**[U4.31.02]** : Operator ``DEFI_FONCTION`` (:gray:`DEFINE_FUNCTION`)
****************************************************************************

.. _u4.31.02.1:

Purpose
=======

Define a real or complex function of a real variable. This operator is used to define, for example, 
material properties that are functions of the temperature, or boundary conditions which depend on a 
space or time variable. 

The concept output by this operator is of type :green:`fonction`.

.. _u4.31.02.2:

Syntax
======

::

  func [fonction] = DEFI_FONCTION
  (
   ♦ NOM_PARA = np,
   ◊ NOM_RESU = / 'TOUTRESU',                          [DEFAULT]
                / nr,                                  [K8]
   ♦ / VALE = l_val_r,                                 [l_R]
     / VALE_C = l_val_c,                               [l_C]
     / VALE_PARA = l_para,                             [listr8]
       ♦ VALE_FONC = l_func,                           [listr8]
     / ABSCISSE = l_x_vals,                            [l_R]
       ♦ ORDONNEE = l_y_vals,                          [l_R]
     / NOEUD_PARA = l_node,                            [l_noeud]
       ♦ MAILLAGE = mesh,                              [maillage]
       ♦ VALE_Y = l_y,                                 [l_R]
   ◊ PROL_DROITE = / 'CONSTANT',
                   / 'LINEAIRE',
                   / 'EXCLU',                          [DEFAULT]
   ◊ PROL_GAUCHE = / 'CONSTANT',
                   / 'LINEAIRE', 
                   / 'EXCLU',                          [DEFAULT]
   ◊ INTERPOL = | 'LIN',                               [DEFAULT]
                | 'LOG',
   ◊ INFO = / 1,                                       [DEFAULT]
            / 2,
   ◊ VERIF = | 'CROISSANT',                            [DEFAULT]
             | 'NON',
   ◊ TITRE = title,                                    [l_Kn]
  )

.. _u4.31.02.3:

Operands
=========

.. _u4.31.02.3.1:

Operand :blue:`NOM_PARA` (:gray:`PARAMETER_NAME`)
--------------------------------------------------

::

  ♦ NOM_PARA = np

Designates the name of the parameter (independent variable or abscissa) of the function. The 
possible values for :green:`np` are:

+-----------------------+--------------------------------------------------------------+
| Parameter name        | Meaning                                                      |
+=======================+==============================================================+
| :green:`'ABSC'`       | Curvilinear abscissa (arc-length coordinates)                |
+-----------------------+--------------------------------------------------------------+
| :green:`'AMOR'`       | Damping                                                      |
+-----------------------+--------------------------------------------------------------+
| :green:`'DRX'`        | Rotation around the :math:`x` axis                           |
+-----------------------+--------------------------------------------------------------+
| :green:`'DRY'`        | Rotation around the :math:`y` axis                           |
+-----------------------+--------------------------------------------------------------+
| :green:`'DRZ'`        | Rotation around the :math:`z` axis                           |
+-----------------------+--------------------------------------------------------------+
| :green:`'DSP'`        |                                                              |
+-----------------------+--------------------------------------------------------------+
| :green:`'DX'`         | Displacement along the :math:`x` axis                        |
+-----------------------+--------------------------------------------------------------+
| :green:`'DY'`         | Displacement along the :math:`y` axis                        |
+-----------------------+--------------------------------------------------------------+
| :green:`'DZ'`         | Displacement along the :math:`z` axis                        |
+-----------------------+--------------------------------------------------------------+
| :green:`'ENDO'`       | Damage                                                       |
+-----------------------+--------------------------------------------------------------+
| :green:`'EPAIS'`      | Thickness                                                    |
+-----------------------+--------------------------------------------------------------+
| :green:`'EPSI'`       | Strain                                                       |
+-----------------------+--------------------------------------------------------------+
| :green:`'FREQ'`       | Frequency                                                    |
+-----------------------+--------------------------------------------------------------+
| :green:`'HYDR'`       | Hydration                                                    |
+-----------------------+--------------------------------------------------------------+
| :green:`'INST'`       | Time instant                                                 |
+-----------------------+--------------------------------------------------------------+
| :green:`'META'`       |                                                              |
+-----------------------+--------------------------------------------------------------+
| :green:`'NEUT1'`      | Neutral parameter 1                                          |
+-----------------------+--------------------------------------------------------------+
| :green:`'NEUT2'`      | Neutral parameter 2                                          |
+-----------------------+--------------------------------------------------------------+
| :green:`'NORM'`       |                                                              |
+-----------------------+--------------------------------------------------------------+
| :green:`'NUME_ORDRE'` | Time step sequence number                                    |
+-----------------------+--------------------------------------------------------------+
| :green:`'PAD'`        |                                                              |
+-----------------------+--------------------------------------------------------------+
| :green:`'PCAP'`       | Capillary pressure                                           |
+-----------------------+--------------------------------------------------------------+
| :green:`'PGAZ'`       | Gas pressure                                                 |
+-----------------------+--------------------------------------------------------------+
| :green:`'PLIQ'`       | Liquid pressure                                              |
+-----------------------+--------------------------------------------------------------+
| :green:`'PORO'`       | Porosity                                                     |
+-----------------------+--------------------------------------------------------------+
| :green:`'PULS'`       | Wave amplitude                                               |
+-----------------------+--------------------------------------------------------------+
| :green:`'PVAP'`       | Vapor pressure                                               |
+-----------------------+--------------------------------------------------------------+
| :green:`'SAT'`        | Saturation                                                   |
+-----------------------+--------------------------------------------------------------+
| :green:`'SECH'`       | Dessication                                                  |
+-----------------------+--------------------------------------------------------------+
| :green:`'SIGM'`       | Stress                                                       |
+-----------------------+--------------------------------------------------------------+
| :green:`'TEMP'`       | Temperature                                                  |
+-----------------------+--------------------------------------------------------------+
| :green:`'TSEC'`       |                                                              |
+-----------------------+--------------------------------------------------------------+
| :green:`'VITE'`       | Velocity                                                     |
+-----------------------+--------------------------------------------------------------+
| :green:`'X'`          | :math:`x` coordinate                                         |
+-----------------------+--------------------------------------------------------------+
| :green:`'Y'`          | :math:`y` coordinate                                         |
+-----------------------+--------------------------------------------------------------+
| :green:`'Z'`          | :math:`z` coordinate                                         |
+-----------------------+--------------------------------------------------------------+
| :green:`'XF'`         | Final :math:`x` coordinate (in the case of follower loading) |
+-----------------------+--------------------------------------------------------------+
| :green:`'YF'`         | Final :math:`y` coordinate (in the case of follower loading) |
+-----------------------+--------------------------------------------------------------+
| :green:`'ZF'`         | Final :math:`z` coordinate (in the case of follower loading) |
+-----------------------+--------------------------------------------------------------+

.. _u4.31.02.3.2:

Operand :blue:`NOM_RESU` (:gray:`RESULT_NAME`)
-----------------------------------------------

::

  ◊ NOM_RESU = nr

Designates the name of the result (8 characters). The function thus created is 
:green:`nr = f(np)`.

.. note::

   Certain commands (:red:`CALC_FONCTION`, :red:`DEFI_MATERIAU`, ...) check the consistency of
   parameter name and the result according to the context. For example, we expect a stress-strain 
   curve defined by a function whose :blue:`NOM_PARA = 'EPSI'` and :blue:`NOM_RESU = 'SIGM'`.

.. _u4.31.02.3.3:

Operand :blue:`VALE` (:gray:`VALUE`)
------------------------------------

::
  
  / VALE = l_val_r

:green:`l_val_r` is the list of values :math:`(x_1, y_1, \dots, x_n, y_n)` input in a specific
order:

* :math:`x_1, y_1` (the first value of the parameter and the corresponding value of the result),
* ...,
* :math:`x_n, y_n` (the last value of the parameter and the corresponding value of the result).

.. note::

   The list :green:`l_val_r` of values must be described in the order of the ascending abscissa 
   (:math:`x`).

.. _u4.31.02.3.4:

Operand :blue:`VALE_C` (:gray:`VALUE_COMPLEX`)
----------------------------------------------

::

  / VALE_C = l_val_c

:green:`l_val_c` is the list of values :math:`(x_1, y_1, z_1, \dots, x_n, y_n, z_n)` where:

* :math:`x_i` are the values of the parameter (independent variable)
* :math:`y_i, z_i` are the real part and the imaginary part of the complex function of this parameter.

.. _u4.31.02.3.5:

Operands :blue:`ABSCISSE / ORDONNEE` (:gray:`ABSCISSA/ORDINATE`)
-----------------------------------------------------------------

::

  / ABSCISSE = l_x_vals
  / ORDONNEE = l_y_vals

We provide the values of the :math:`x`-coordinates and :math:`y`-coordinates of the function 
separately in the form of lists of real values :math:`(x_1, x_2, \dots, x_n)` for :blue:`ABSCISSE` 
and :math:`(y_1, y_2, \dots, y_n)` for :blue:`ORDONNEE`. The two lists must have the same number
of elements.

.. _u4.31.02.3.6:

Operand :blue:`VALE_PARA / VALE_FONC` (:gray:`VALUE_INDEP_VAR, VALUE_DEP_FUNC`)
--------------------------------------------------------------------------------

::

  / VALE_PARA = l_para
  / VALE_FONC = l_func

Same operation as :blue:`ABSCISSE, ORDONNEE` except that the lists are provided in the form of
concepts of type :green:`listr8` produced by :red:`DEFI_LIST_REEL` :ref:`[U4.34.01] <u4.34.01>`.

:blue:`VALE_PARA` and :blue:`VALE_FONC` must have identical cardinal numbers (same number of
entries). If not, the command stops with a fault.

.. _u4.31.02.3.7:

Operand :blue:`NOEUD_PARA` (:gray:`NODES_INDEP_VAR`)
-----------------------------------------------------

::

  / NODE_PARA = l_node

:green:`l_node` is the list of nodes used to define the values of the :math:`x`-coordinates of 
the function to be defined. The abscissas will be equal to the curvilinear abscissas (arc-length
coordinates) of the nodes on the curve which they define.

.. _u4.31.02.3.8:

Operands :blue:`PROL_DROITE` and :blue:`PROL_GAUCHE` (:gray:`EXTRAPOLATION`)
-----------------------------------------------------------------------------

::
 
  ◊ PROL_DROITE = 
  ◊ PROL_GAUCHE = 

Define the type of extrapolation to the right (to the left) of the domain on which the variable is
defined:

* :blue:`'CONSTANT'` : for an extrapolation that uses the last (or first) value of the function,
* :blue:`'LINEAIRE'` : for an extrapolation along the first defined segment (:blue:`PROL_GAUCHE`)
  or the last defined segment (:blue:`PROL_DROITE`),
* :blue:`'EXCLU'` : if the extrapolation of values outside the domain of definition of parameter is 
  prohibited (in this case if a calculation requires a value of the function outside the definition 
  domain, the code will stop with a fatal error).

For example :

::

   PROL_DROITE = 'CONSTANT', 
   PROL_GAUCHE = 'CONSTANT'

.. figure:: u4.31.02/fig1_u4.31.02.png
   :height: 250 px
   :align: center

::

   PROL_DROITE = 'LINEAIRE', 
   PROL_GAUCHE = 'EXCLU'

.. figure:: u4.31.02/fig2_u4.31.02.png
   :height: 250 px
   :align: center

.. note::

   The extrapolation and interpolation methods are independent of each other.

.. _u4.31.02.3.9:

Operand :blue:`INTERPOL` (:gray:`INTERPOLATION`)
-------------------------------------------------

::

  ◊ INTERPOL =

Type of interpolation of the function between the values of the domain of definition of the function: 
one type for the interpolation of the parameter and one for the interpolation of the function. This 
is obtained by providing a list of strings such as:

::

  INTERPOL = ('LIN', 'LOG')

* :blue:`'LIN'`: linear,
* :blue:`'LOG'`: logarithmic.

.. note::

   If only one value is specified, it is taken into account at the same time by the interpolation 
   of the parameter and function. :blue:`INTERPOL = 'LOG'` is equivalent to :blue:`('LOG', 'LOG')`.

.. _u4.31.02.3.10:

Operand :blue:`INFO`
---------------------

::

  ◊ INFO = 

Specifies the output options printed to the MESSAGE file.

* 1: no printing (default option)
* 2: printing of the parameters plus the list of the first 10 values in ascending order
  of the parameter

.. _u4.31.02.3.11:

Operand :blue:`VERIF`
---------------------

::

  ◊ VERIF =

The operator :red:`DEFI_FONCTION` checks that the values of the abscissas are strictly increasing. If 
this is not true, an error is raised. This is the default behavior, :blue:`VERIF = 'CROISSANT'`.

The user has the possibility of not doing this check by indicating :blue:`VERIF = 'NON'`. In that 
case, the function is reordered by increasing abscissa. An alarm is issued if the abscissas of the
function were not increasing. On the other hand, the abscissas must imperatively be strictly monotonic.

.. _u4.31.02.3.12:

Operand :blue:`TITRE` (:gray:`TITLE`)
--------------------------------------

::

  ◊ TITRE = title

Title attached to the concept produced by this operator :ref:`[U4.03.01] <u4.03.01>`.

.. _u4.31.02.3.13:

Operands :blue:`MAILLAGE` (:gray:`MESH`) and :blue:`VALE_Y` (:gray:`Y_VALUE`)
-----------------------------------------------------------------------------

It is necessary to include these two keywords if one defines the function starting from 
:blue:`NOEUD_PARA` (see :numref:`u4.31.02.3.7`):

::

  MAILLAGE = mesh

Name of the mesh associated with the list of nodes :green:`l_node`.

::

  VALE_Y = l_val_y

List of the values of the ordinates of the function to be defined.

.. _u4.31.02.4:

Definition of a time dependent function
=========================================

.. _u4.31.02.4.1:

Function and variables are entered as reals
-----------------------------------------------

Definition of a function (piecewise linear) depends on time (parameter :green:`INST`)

::

  f_ex1 = DEFI_FONCTION(NOM_PARA = 'INST',
                        VALE = (0., -1.,
                                1.,  0.,
                                3.,  1.,
                                6.,  2.), 
                        PROL_GAUCHE = 'CONSTANT',
                        PROL_DROITE = 'LINEAIRE')             

.. figure:: u4.31.02/fig3_u4.31.02.png
   :height: 250 px
   :align: center

.. _u4.31.02.4.2:

Function and variables entered as :green:`listr8` concepts
------------------------------------------------------------

It is possible to define this function using concepts of type :green:`listr8` created via operator 
:red:`DEFI_LIST_REEL` :ref:`[U4.34.01] <u4.34.01>`:

::

  x_vals = DEFI_LIST_REEL(DEBUT = 0.,
                          INTERVALLE = (_F(JUSQU_A = 1., 
                                           NOMBRE = 1),
                                        _F(JUSQU_A = 3., 
                                           NOMBRE = 1),
                                        _F(JUSQU_A = 6., 
                                           NOMBRE = 1)))

  y_vals = DEFI_LIST_REEL(DEBUT = -1.,
                          INTERVALLE = (_F(JUSQU_A = 0., 
                                           NOMBRE = 1),
                                        _F(JUSQU_A = 1., 
                                           NOMBRE = 1),
                                        _F(JUSQU_A = 2., 
                                           NOMBRE = 1)))

  f_ex2 = DEFI_FONCTION(NOM_PARA = 'INST',
                        VALE_PARA = x_vals,
                        VALE_FONC = y_vals,
                        PROL_RIGHT = 'CONSTANT',
                        PROL_GAUCHE = 'LINEAIRE')

.. note::

   This example is obviously more complicated than is absolutely necessary to define the proposed 
   function. We only want to highlight the principle of using the possibility offered.

   This becomes interesting when one uses functions defined over a large number of points.

   Another reason to use the definition via :red:`DEFI_LIST_REEL` is when the lists are necessary 
   as arguments for another operator: (list of times of a time-dependent computation 
   :red:`THER_LINEAIRE`, :red:`DYNA_LINE_TRAN`, ...). This approach then avoids duplication
   of information.
