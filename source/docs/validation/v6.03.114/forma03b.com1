# Continue the 2nd stage of the calculation
POURSUITE()

#-----------------------------------------------------------------------
# Post-processing: Maximum values of equivalent stress and internal vars
#-----------------------------------------------------------------------
# Calculate the equivalent stresses as the Gauss points
result = CALC_CHAMP(reuse = result,
                    RESULTAT = result,
                    CRITERES = ('SIEQ_ELGA'))
 
# Write the results in ASCII format
IMPR_RESU(FORMAT = "RESULTAT",
          MODELE = model,
          RESU = (_F(RESULTAT = result,
                     NOM_CHAM = 'SIEQ_ELGA',
                     VALE_MAX = 'OUI'),
                  _F(RESULTAT = result,
                     NOM_CHAM = 'VARI_ELGA',
                     VALE_MAX = 'OUI')))

#-----------------------------------------------------------------------
# Post-processing: SIYY as a function of displacement at point G
#-----------------------------------------------------------------------
# Extract values
uy_G = RECU_FONCTION(RESULTAT = result,
                     NOM_CHAM = 'DEPL',
                     NOM_CMP = 'DY',
                     GROUP_NO = 'G')

sigy_G = RECU_FONCTION(RESULTAT = result,
                       NOM_CHAM = 'SIGM_NOEU',
                       NOM_CMP = 'SIYY',
                       GROUP_NO = 'G')

# Write the data into a CSV table
IMPR_FONCTION(FORMAT = 'TABLEAU',
              UNITE = 9,
              SEPARATEUR = ',',
              COURBE = _F(FONC_X = uy_G,
                          FONC_Y = sigy_G),
              TITRE = 'sigma_yy as a function of u_y at point G')

#-----------------------------------------------------------------------
# Post-processing: Stresses along the bottom of the plate
#-----------------------------------------------------------------------
# Extract values
sig_base = MACR_LIGN_COUPE(RESULTAT = result,
                           NOM_CHAM = 'SIGM_ELNO',
                           LIGN_COUPE = _F(NB_POINTS = 30,
                                           COOR_ORIG = (10, 0),
                                           COOR_EXTR = (100, 0)))

# Write the data into a table
IMPR_TABLE(TABLE = sig_base,
           FILTRE = _F(NOM_PARA = 'INST',
                       VALE = t_elas_plas),
           NOM_PARA = ('ABSC_CURV', 'SIYY'))

#-----------------------------------------------------------------------
# Post-processing: Stresses along the circumference of the hole
#-----------------------------------------------------------------------
# Extract element stresses along the hole edge
s_theta = MACR_LIGN_COUPE(RESULTAT = result, 
                          INST = t_elas, 
                          NOM_CHAM = 'SIGM_ELNO',
                          LIGN_COUPE = _F(TYPE='GROUP_MA', 
                                          REPERE = 'POLAIRE', 
                                          MAILLAGE = mesh,
                                          OPERATION = 'EXTRACTION', 
                                          GROUP_MA = 'hole', 
                                          INTITULE = 'sig_theta'))

# Write the data into a table
IMPR_TABLE(UNITE = 10,
           TABLE = s_theta,
           FORMAT = 'TABLEAU')

# Extract sig_y = F(ABSC_CURV)
st_calc = RECU_FONCTION(TABLE = s_theta,
                        PARA_X = 'ABSC_CURV',
                        PARA_Y = 'SIYY',
                        INTERPOL = 'LIN',
                        PROL_DROITE = 'CONSTANT',
                        PROL_GAUCHE = 'CONSTANT')

# Analytical solution
R = 10.0;
sig_tt = FORMULE(VALE = 't_elas*(1.+2.*cos(2.*S/R))',
                 t_elas = t_elas,
                 R = R,
                 NOM_PARA = 'S')

s_max = pi/2*R
l_s = DEFI_LIST_REEL(DEBUT = 0.0,
                     INTERVALLE = _F(JUSQU_A = s_max,
                                     NOMBRE = 30))

st_anal = CALC_FONC_INTERP(FONCTION = sig_tt,
                           NOM_PARA = 'S',
                           NOM_RESU = 'ANALYTIC',
                           LIST_PARA = l_s,
                           INTERPOL = 'LIN')

# Save comparison analytical/numerical to a table
IMPR_FONCTION(FORMAT = 'TABLEAU',
              UNITE  = 82,
              TITRE  = "sig_tt along the edge of the hole",
              COURBE = (_F(FONCTION = st_calc),
                        _F(FONCTION = st_anal)))

#------------------------------------------------------------------------
# Post-processing: Reaction at the top edge as a function of displacement 
#------------------------------------------------------------------------
# Compute the nodal forces
result = CALC_CHAMP(reuse = result,
                    RESULTAT = result,
                    FORCE = 'FORC_NODA')

# Extract values
uy_G_2 = RECU_FONCTION(RESULTAT = result,
                       NOM_CHAM = 'DEPL',
                       NOM_CMP = 'DY',
                       GROUP_NO = 'G')

f_top = MACR_LIGN_COUPE(RESULTAT = result, 
                        NOM_CHAM = 'FORC_NODA',
                        LIGN_COUPE = _F(TYPE = 'GROUP_MA', 
                                        MAILLAGE = mesh,
                                        RESULTANTE = ('DX','DY','DZ'),
                                        OPERATION = 'EXTRACTION', 
                                        GROUP_MA = 'top', 
                                        INTITULE = 'f_top'))

fy_top = RECU_FONCTION(TABLE = f_top,
                       PARA_X = 'INST',
                       PARA_Y = 'DY',
                       INTERPOL = 'LIN')

# Save values to a table
IMPR_FONCTION(FORMAT = 'TABLEAU',
              UNITE  = 91,
              TITRE  = "Reactions f_y as a function of u_y",
              COURBE = (_F(FONC_X = uy_G_2,
                           FONC_Y = fy_top)))

# finish
FIN();
