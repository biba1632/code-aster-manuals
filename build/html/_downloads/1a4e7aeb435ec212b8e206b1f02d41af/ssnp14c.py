# Cube composed of two quadratic PENTA15 elements

import sys
import salome
import salome_notebook
from salome.smesh import smeshBuilder
import SMESH, SALOMEDS

ExportPATH="/home/banerjee/Salome/ssnp14/"

# Initialize
salome.salome_init()
notebook = salome_notebook.NoteBook()
smesh = smeshBuilder.New()

# Create mesh
mesh = smesh.Mesh()
smesh.SetName(mesh, 'mesh')

# Add nodes
node1 = mesh.AddNode(0.00000000E+00, 1.00000000E+00, 0.00000000E+00)
node2 = mesh.AddNode(1.00000000E+00, 1.00000000E+00, 0.00000000E+00)
node3 = mesh.AddNode(0.00000000E+00, 0.00000000E+00, 0.00000000E+00)
node4 = mesh.AddNode(1.00000000E+00, 0.00000000E+00, 0.00000000E+00)
node5 = mesh.AddNode(0.00000000E+00, 1.00000000E+00, 1.00000000E+00)
node6 = mesh.AddNode(1.00000000E+00, 1.00000000E+00, 1.00000000E+00)
node7 = mesh.AddNode(0.00000000E+00, 0.00000000E+00, 1.00000000E+00)
node8 = mesh.AddNode(1.00000000E+00, 0.00000000E+00, 1.00000000E+00)
node9 = mesh.AddNode(5.00000000E-01, 1.00000000E+00, 0.00000000E+00)
node10 = mesh.AddNode(0.00000000E+00, 5.00000000E-01, 0.00000000E+00)
node11 = mesh.AddNode(5.00000000E-01, 5.00000000E-01, 0.00000000E+00)
node12 = mesh.AddNode(1.00000000E+00, 5.00000000E-01, 0.00000000E+00)
node13 = mesh.AddNode(5.00000000E-01, 0.00000000E+00, 0.00000000E+00)
node14 = mesh.AddNode(0.00000000E+00, 1.00000000E+00, 5.00000000E-01)
node15 = mesh.AddNode(1.00000000E+00, 1.00000000E+00, 5.00000000E-01)
node16 = mesh.AddNode(0.00000000E+00, 0.00000000E+00, 5.00000000E-01)
node17 = mesh.AddNode(1.00000000E+00, 0.00000000E+00, 5.00000000E-01)
node18 = mesh.AddNode(5.00000000E-01, 1.00000000E+00, 1.00000000E+00)
node19 = mesh.AddNode(0.00000000E+00, 5.00000000E-01, 1.00000000E+00)
node20 = mesh.AddNode(5.00000000E-01, 5.00000000E-01, 1.00000000E+00)
node21 = mesh.AddNode(1.00000000E+00, 5.00000000E-01, 1.00000000E+00)
node22 = mesh.AddNode(5.00000000E-01, 0.00000000E+00, 1.00000000E+00)

# Add groups for the nodes
node_1 = mesh.CreateEmptyGroup(SMESH.NODE, 'node_1')
node_2 = mesh.CreateEmptyGroup(SMESH.NODE, 'node_2')
node_3 = mesh.CreateEmptyGroup(SMESH.NODE, 'node_3')
node_4 = mesh.CreateEmptyGroup(SMESH.NODE, 'node_4')
node_5 = mesh.CreateEmptyGroup(SMESH.NODE, 'node_5')
node_6 = mesh.CreateEmptyGroup(SMESH.NODE, 'node_6')
node_7 = mesh.CreateEmptyGroup(SMESH.NODE, 'node_7')
node_8 = mesh.CreateEmptyGroup(SMESH.NODE, 'node_8')
node_9 = mesh.CreateEmptyGroup(SMESH.NODE, 'node_9')
node_10 = mesh.CreateEmptyGroup(SMESH.NODE, 'node_10')
node_11 = mesh.CreateEmptyGroup(SMESH.NODE, 'node_11')
node_12 = mesh.CreateEmptyGroup(SMESH.NODE, 'node_12')
node_13 = mesh.CreateEmptyGroup(SMESH.NODE, 'node_13')
node_14 = mesh.CreateEmptyGroup(SMESH.NODE, 'node_14')
node_15 = mesh.CreateEmptyGroup(SMESH.NODE, 'node_15')
node_16 = mesh.CreateEmptyGroup(SMESH.NODE, 'node_16')
node_17 = mesh.CreateEmptyGroup(SMESH.NODE, 'node_17')
node_18 = mesh.CreateEmptyGroup(SMESH.NODE, 'node_18')
node_19 = mesh.CreateEmptyGroup(SMESH.NODE, 'node_19')
node_20 = mesh.CreateEmptyGroup(SMESH.NODE, 'node_20')
node_21 = mesh.CreateEmptyGroup(SMESH.NODE, 'node_21')
node_22 = mesh.CreateEmptyGroup(SMESH.NODE, 'node_22')
nbAdd = node_1.Add([1])
nbAdd = node_2.Add([2])
nbAdd = node_3.Add([3])
nbAdd = node_4.Add([4])
nbAdd = node_5.Add([5])
nbAdd = node_6.Add([6])
nbAdd = node_7.Add([7])
nbAdd = node_8.Add([8])
nbAdd = node_9.Add([9])
nbAdd = node_10.Add([10])
nbAdd = node_11.Add([11])
nbAdd = node_12.Add([12])
nbAdd = node_13.Add([13])
nbAdd = node_14.Add([14])
nbAdd = node_15.Add([15])
nbAdd = node_16.Add([16])
nbAdd = node_17.Add([17])
nbAdd = node_18.Add([18])
nbAdd = node_19.Add([19])
nbAdd = node_20.Add([20])
nbAdd = node_21.Add([21])
nbAdd = node_22.Add([22])

# Add volume elements
vol1 = mesh.AddVolume([3, 4, 2, 7, 8, 6, 13, 12, 11, 22, 21, 20, 16, 17, 15])
vol2 = mesh.AddVolume([3, 2, 1, 7, 6, 5, 11, 9, 10, 20, 18, 19, 16, 15, 14])

# Add group for the volumes
cube = mesh.CreateEmptyGroup( SMESH.VOLUME, 'cube' )
nbAdd = cube.Add( [ 1, 2 ] )
penta1 = mesh.CreateEmptyGroup( SMESH.VOLUME, 'penta1' )
nbAdd = penta1.Add( [ 1 ] )
penta2 = mesh.CreateEmptyGroup( SMESH.VOLUME, 'penta2' )
nbAdd = penta2.Add( [ 2 ] )

# Add face elements
face3 = mesh.AddFace([ 1, 3, 7, 5, 10, 16, 19, 14 ])
face4 = mesh.AddFace([ 2, 6, 8, 4, 15, 21, 17, 12 ])
face5 = mesh.AddFace([ 5, 6, 2, 1, 18, 15, 9, 14 ])
face6 = mesh.AddFace([ 3, 4, 8, 7, 13, 17, 22, 16 ])

# Add groups for the faces
left_face = mesh.CreateEmptyGroup( SMESH.FACE, 'left_face' )
right_face = mesh.CreateEmptyGroup( SMESH.FACE, 'right_face' )
top_face = mesh.CreateEmptyGroup( SMESH.FACE, 'top_face' )
bot_face = mesh.CreateEmptyGroup( SMESH.FACE, 'bot_face' )
nbAdd = left_face.Add( [ 3 ] )
nbAdd = right_face.Add( [ 4 ] )
nbAdd = top_face.Add( [ 5 ] )
nbAdd = bot_face.Add( [ 6 ] )

# Export mesh
mesh.ExportMED( r''+ExportPATH+'ssnp14c.mmed'+'')

# Update GUI object tree
if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()
