# FORMA08: MODEL B : INTERFACE ELEMENTS

# Start
DEBUT(LANG='EN')

# Parameters
t_final = 4.5
num_steps = 10 * t_final

young_modulus = 100
poisson_ratio = 0.

G_c = 0.9
sig_c = 3
penalty = 0.00001

tangent_stiffness = 10
penalty_lag = 100

u_x = 0.0
u_y = 1.0
u_z = 0.0

u_ref = 2*G_c/sig_c

# Read the mesh
mesh_in = LIRE_MAILLAGE(FORMAT='MED')

# Conver to quadratic
mesh = CREA_MAILLAGE(MAILLAGE = mesh_in,
                     LINE_QUAD = _F(TOUT = 'OUI'))

# Add a node group
mesh = DEFI_GROUP(reuse = mesh,
                  MAILLAGE = mesh,
                  CREA_GROUP_NO = _F(NOM = 'disp_edge_nodes',
                                     GROUP_MA = 'disp_edge'))

# Change orientation of faces on which cohesive elements will be applied
mesh = MODI_MAILLAGE(reuse = mesh,
                     MAILLAGE = mesh,
                     ORIE_FISSURE = _F(GROUP_MA = 'DCB_joint'))
                     
# Assign elements
model = AFFE_MODELE(MAILLAGE = mesh,
                    AFFE= (_F(GROUP_MA = ('DCB_crack', 'DCB_beam'),
                              PHENOMENE = 'MECANIQUE',
                              MODELISATION = '3D'),
                           _F(GROUP_MA = 'DCB_joint',
                              PHENOMENE = 'MECANIQUE',
                              MODELISATION = '3D_INTERFACE')))
                       
# Assign orientation of crack in global frame for interfac elements only
crac_dir = AFFE_CARA_ELEM(MODELE = model,
                           MASSIF = (_F(GROUP_MA = 'DCB_joint',
                                        ANGL_REP = (-90, 0 ,-90))))

# Define and assign material
steel = DEFI_MATERIAU(ELAS = _F(E = young_modulus,
                                NU = poisson_ratio),
                      RUPT_FRAG = _F(GC = G_c,
                                     SIGM_C = sig_c,
                                     PENA_ADHERENCE = penalty,
                                     PENA_LAGR = penalty_lag,
                                     RIGI_GLIS = tangent_stiffness))

mater = AFFE_MATERIAU(MAILLAGE = mesh,
                      AFFE = _F(GROUP_MA = ('DCB_crack','DCB_beam','DCB_joint'),
                                MATER = steel))

# Boundary conditions
symm_bc = AFFE_CHAR_MECA(MODELE = model,
                         DDL_IMPO = (_F(GROUP_MA = 'joint_bot',
                                        DY = 0)))

disp_bc = AFFE_CHAR_MECA(MODELE = model,
                         FACE_IMPO = (_F(GROUP_MA  = 'disp_edge', 
                                         DX = u_x,
                                         DY = u_y,
                                         DZ = u_z)))

# Define time stepping
t_list = DEFI_LIST_REEL(DEBUT = 0,
                        INTERVALLE = (_F(JUSQU_A = 10,
                                         NOMBRE = 200 )))

l_times = DEFI_LIST_INST(DEFI_LIST = _F(LIST_INST = t_list),
                         ECHEC = _F(SUBD_METHODE = 'MANUEL',
                                    SUBD_PAS = 10))

t_save = DEFI_LIST_REEL(DEBUT = 0,
                        INTERVALLE = (_F(JUSQU_A = t_final,
                                         NOMBRE = num_steps)))

# Load multipliers
t_func = DEFI_FONCTION(NOM_PARA = 'INST',
                       VALE = (-1, 0, 
                                0, 0.0001, 
                                t_final, t_final))

# Compute solution
result = STAT_NON_LINE(MODELE = model,
                       CHAM_MATER = mater,
                       CARA_ELEM = crac_dir,
                       EXCIT = (_F(CHARGE = symm_bc),
                                _F(CHARGE = disp_bc,
                                   TYPE_CHARGE = 'FIXE_PILO')),
                       COMPORTEMENT = (_F(RELATION = 'ELAS', 
                                          GROUP_MA = ('DCB_crack','DCB_beam')),
                                       _F(RELATION = 'CZM_OUV_MIX', 
                                          GROUP_MA = 'DCB_joint')),
                       INCREMENT = _F(LIST_INST = l_times,
                                      INST_FIN = t_final),
                       NEWTON = _F(MATRICE = 'TANGENTE', 
                                   REAC_ITER = 1),
                       CONVERGENCE = _F(RESI_REFE_RELA = 1.E-6,
                                        SIGM_REFE = sig_c,
                                        DEPL_REFE = u_ref,
                                        ITER_GLOB_MAXI = 20),
                       PILOTAGE   = _F(SELECTION = 'RESIDU',
                                       TYPE = 'PRED_ELAS',
                                       GROUP_MA = 'DCB_joint',
                                       COEF_MULT = 1,
                                       ETA_PILO_R_MIN = 0.),
                       SOLVEUR = _F(METHODE = 'MUMPS', 
                                    RENUM='METIS'),
                       ARCHIVAGE = _F(LIST_INST = t_save))


#----------------------
# Post-processing
#----------------------
# Compute nodal forces
result = CALC_CHAMP(reuse = result,
                    RESULTAT = result,
                    FORCE = 'FORC_NODA',
                    GROUP_MA = 'DCB_crack')

# Save applied displacements
disp_app = POST_RELEVE_T(ACTION = _F(INTITULE = 'disp_control',
                                     OPERATION = 'EXTRACTION',
                                     GROUP_NO = 'ref_point',
                                     NOM_CHAM = 'DEPL',
                                     NOM_CMP = 'DY',
                                     RESULTAT = result,
                                     TOUT_ORDRE = 'OUI'))
IMPR_TABLE(TABLE = disp_app)

# Save force resultant in y-direction
f_res = POST_RELEVE_T(ACTION = _F(INTITULE = 'f_resultant',
                                  OPERATION = 'EXTRACTION',
                                  GROUP_NO = 'disp_edge_nodes',
                                  NOM_CHAM = 'FORC_NODA',
                                  RESULTANTE = 'DY',
                                  RESULTAT =  result,
                                  TOUT_ORDRE = 'OUI'))
IMPR_TABLE(TABLE = f_res)

# Save the results for visualization
mesh_V = CREA_MAILLAGE(MAILLAGE = mesh,
                       RESTREINT = _F(GROUP_MA = ('DCB_crack', 'DCB_beam')))

model_V = AFFE_MODELE(MAILLAGE = mesh_V,
                      AFFE = _F(GROUP_MA = ('DCB_crack', 'DCB_beam'), 
                                PHENOMENE = 'MECANIQUE',
                                MODELISATION = '3D',))

result_V = EXTR_RESU(RESULTAT = result, 
                     RESTREINT = _F(MODELE = model_V))

IMPR_RESU(FORMAT= 'MED', 
          RESU = _F(RESULTAT = result_V), 
          UNITE = 80)

#----------------------
# Verification
#----------------------
TEST_TABLE(CRITERE = 'RELATIF',
           REFERENCE = 'ANALYTIQUE',
           PRECISION = 0.05,
           VALE_CALC = 4.852245498,
           VALE_REFE = 4.71,
           NOM_PARA = 'DY',
           TABLE = f_res,
           FILTRE = _F(NOM_PARA = 'INST',
                       VALE = 4.5))

# Finish
FIN()
