# Start
DEBUT()

# Parameters
E  =  20000.0
nu =  0.3
disp = 2.0
disp_top = disp * (-1.0)
disp_bot = disp
R  = 50.

# Read mesh
mesh = LIRE_MAILLAGE(FORMAT='MED')

# Change orientation of surface normal at contact surfaces
mesh = MODI_MAILLAGE(reuse = mesh,
                     MAILLAGE = mesh,
                     ORIE_PEAU_2D = _F(GROUP_MA = ('bot_surf', 'top_surf')))

# Material properties
rubber = DEFI_MATERIAU(ELAS = _F(E = E,
                                 NU = nu))

# Assign material property
material = AFFE_MATERIAU(MAILLAGE = mesh,
                         AFFE = _F(GROUP_MA = ('bot_sphere', 'top_sphere'),
                                   MATER = rubber))

# Assign axisymmetric FE model
model = AFFE_MODELE(MAILLAGE = mesh,
                    AFFE = _F(GROUP_MA = ('bot_sphere','top_sphere'),
                              PHENOMENE = 'MECANIQUE',
                              MODELISATION = 'AXIS'))

# Apply BCs
bcs = AFFE_CHAR_MECA(MODELE = model,
                     DDL_IMPO = (_F(GROUP_MA = 'symm_axis',
                                    DX = 0),
                                 _F(GROUP_MA = 'bot_edge',
                                    DY = disp_bot),
                                 _F(GROUP_MA = 'top_edge',
                                    DY = disp_top)))

# Create ramp loading function
ramp = DEFI_FONCTION(NOM_PARA = 'INST',
                     VALE = (0, 0,
                             1, 1))

# Contact definition
contact = DEFI_CONTACT(MODELE = model,
                       FORMULATION = 'DISCRETE',
                       ZONE =_F(GROUP_MA_MAIT = 'top_surf',
                                GROUP_MA_ESCL = 'bot_surf',
                                ALGO_CONT = 'CONTRAINTE'))

# Define a list of time points for time-stepping
times = DEFI_LIST_REEL(DEBUT = 0,
                       INTERVALLE = _F(JUSQU_A = 1,
                                       NOMBRE = 20))

# Do the non-linear solve
result = STAT_NON_LINE(MODELE = model,
                       CHAM_MATER = material,
                       EXCIT = _F(CHARGE = bcs,
                                  FONC_MULT = ramp),
                       CONTACT = contact,
                       COMPORTEMENT = _F(RELATION='ELAS'),
                       INCREMENT = _F(LIST_INST = times),
                       SOLVEUR = _F())

# Post-processing
result = CALC_CHAMP(reuse = result,
                    RESULTAT = result,
                    CRITERES = ('SIEQ_ELGA','SIEQ_ELNO','SIEQ_NOEU'),
                    CONTRAINTE = ('SIEF_ELNO','SIEF_NOEU'))

# Create a group of nodes on the slave contact surface
mesh = DEFI_GROUP(reuse = mesh,
                  MAILLAGE = mesh,
                  CREA_GROUP_NO = _F(OPTION = 'NOEUD_ORDO',
                                     NOM = 'contact_nodes',
                                     GROUP_MA = 'bot_surf',
                                     GROUP_NO_ORIG = 'bot_contact'))

# Extract contact pressure 
sig_yy = POST_RELEVE_T(ACTION = _F(OPERATION = 'EXTRACTION',
                                   INTITULE = 'sig_yy as a function of position',
                                   RESULTAT = result,
                                   NOM_CHAM = 'SIEF_NOEU',
                                   INST = 1.0,
                                   GROUP_NO = 'contact_nodes',
                                   NOM_CMP = 'SIYY'))

# Write table is CSV format
IMPR_TABLE(TABLE = sig_yy,
           SEPARATEUR = ',')

# Store the numerical solution coordinates
coords_f = RECU_FONCTION(TABLE = sig_yy,
                         PARA_X = 'ABSC_CURV',
                         PARA_Y = 'SIYY')

# Compute and store the analytical (Hertz) solution
x_coord = DEFI_LIST_REEL(DEBUT = 0,
                         INTERVALLE = _F(JUSQU_A = 10,
                                         NOMBRE = 100))

p0 = -E/(pi*(1.0-nu*nu))*sqrt(2.0*2.0*disp/R)
a  = sqrt(R*disp)

hertz = FORMULE(VALE = 'p0*sqrt(1.0-(x/a)*(x/a))',
                p0 = p0,
                a = a,
                NOM_PARA='x')

pressure = CALC_FONC_INTERP(FONCTION = hertz,
                            LIST_PARA = x_coord,
                            NOM_PARA = 'x')

# Verify the numerical solution
TEST_RESU(RESU = _F(GROUP_NO = 'bot_contact',
                    INST = 1.0,
                    REFERENCE = 'ANALYTIQUE',
                    RESULTAT = result,
                    NOM_CHAM = 'SIEF_NOEU',
                    NOM_CMP = 'SIYY',
                    VALE_CALC = -2773.5474109439001,
                    VALE_REFE = -2798.3286697476105,
                    CRITERE = 'RELATIF',
                    PRECISION = 1.E-2))

# Write the results
IMPR_FONCTION(FORMAT = 'TABLEAU',
              SEPARATEUR = ',',
              UNITE = 29,
              COURBE = (_F(FONCTION = coords_f),
                        _F(FONCTION = pressure)),
              TITRE = 'Hertz pressure')

IMPR_RESU(FORMAT = 'MED',
          RESU = _F(RESULTAT = result))

# Finish
FIN();
