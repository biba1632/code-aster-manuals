# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
sys.path.insert(0, os.path.abspath('.'))
sys.path.insert(0, os.path.abspath('docs'))


# -- Project information -----------------------------------------------------

project = 'Code-Aster v14'
copyright = '2021-2022, Biswajit Banerjee'
author = 'Biswajit Banerjee'

# The full version, including alpha/beta/rc tags
release = '0.1'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
                  'sphinx.ext.autodoc',
                  'sphinx.ext.mathjax',
                  'sphinx.ext.doctest',
                  'sphinx.ext.intersphinx',
                  'sphinx.ext.viewcode',
                  'sphinx.ext.inheritance_diagram',
                  'matplotlib.sphinxext.plot_directive',
                  'IPython.sphinxext.ipython_directive',
                  'IPython.sphinxext.ipython_console_highlighting',
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []

# Labels
numfig = True
numfig_secnum_depth = 2

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
#html_theme = 'sphinxdoc'
import lsst_sphinx_bootstrap_theme
html_theme = "lsst_sphinx_bootstrap_theme"
html_theme_path = [lsst_sphinx_bootstrap_theme.get_html_theme_path()]

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static','docs/validation/_static']

html_css_files = [
    'code_aster.css',
]


html_sidebars = {
    '**' : ['searchbox.html', 'google_searchbox.html'],
}


