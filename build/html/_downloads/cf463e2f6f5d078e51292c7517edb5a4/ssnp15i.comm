# 3D cubic element in combined tension + shear
# J2 plasticity + isotropic hardening
# Using de Borst algorithm with MFront implementation library
DEBUT(LANG='EN')

# Create MFront library
CREA_LIB_MFRONT(UNITE_MFRONT = 38,
                UNITE_LIBRAIRIE = 39)

# Read the mesh
mesh = LIRE_MAILLAGE(FORMAT = "MED")

# Assign 3D mechanical element
model = AFFE_MODELE(MAILLAGE = mesh,
                    AFFE = _F(TOUT = 'OUI',
                              PHENOMENE = 'MECANIQUE',
                              MODELISATION = '3D'))

# Material parameters
E = 195000.0
nu = 0.3
h_lin_iso = 1930.0
sig_y = 181.0

# Define material
steel = DEFI_MATERIAU(ELAS = _F(E = E,
                                NU = nu),
                      MFRONT = _F(LISTE_COEF = (sig_y, h_lin_iso, E, nu)))

# Assign material
mater = AFFE_MATERIAU(MAILLAGE = mesh,
                      AFFE = _F(TOUT = 'OUI',
                                 MATER = steel))

# Define traction functions
# Tension
sigma_fn = DEFI_FONCTION(NOM_PARA = 'INST',
                         VALE = (0.0, 0.0,
                                 1.0, 151.2,
                                 2.0, 257.2,
                                 3.0, 0.0),
                         PROL_DROITE = 'EXCLU',
                         PROL_GAUCHE = 'EXCLU')

# Shear
tau_fn = DEFI_FONCTION(NOM_PARA = 'INST',
                       VALE = (0.0, 0.0,
                               1.0, 93.1,
                               2.0, 33.1,
                               3.0, 0.0),
                       PROL_DROITE = 'EXCLU',
                       PROL_GAUCHE = 'EXCLU')


# Apply a unit traction BC for tension in -x
tens_bc = AFFE_CHAR_MECA(MODELE = model,
                         FORCE_FACE = _F(GROUP_MA = 'left_face',
                                         FX = -1.))

# Apply unit traction bcs for shear in xy-plane
shear_bc = AFFE_CHAR_MECA(MODELE = model,
                          FORCE_FACE = (_F(GROUP_MA = 'left_face',
                                           FY = -1.),
                                        _F(GROUP_MA = 'right_face',
                                           FY = 1.),
                                        _F(GROUP_MA = 'top_face',
                                           FX = 1.),
                                        _F(GROUP_MA = 'bot_face',
                                           FX = -1.)))

# Apply displacement BCs to prevent rigid body motions
disp_bc = AFFE_CHAR_MECA(MODELE = model,
                         DDL_IMPO = (_F(GROUP_NO = 'node_4',
                                        DX = 0.,
                                        DY = 0.),
                                     _F(GROUP_NO = 'node_8',
                                        DX = 0.,
                                        DY = 0.,
                                        DZ = 0.),
                                     _F(GROUP_NO = 'node_2',
                                        DX = 0.),
                                     _F(GROUP_NO = 'node_6',
                                        DX = 0.)))

# Define time steps
l_times = DEFI_LIST_REEL(DEBUT = 0.0,
                         INTERVALLE = (_F(JUSQU_A = 0.1,
                                          NOMBRE = 1),
                                       _F(JUSQU_A = 0.9,
                                          NOMBRE = 10),
                                       _F(JUSQU_A = 1.0,
                                          NOMBRE = 1),
                                       _F(JUSQU_A = 2.0,
                                          NOMBRE = 40),
                                       _F(JUSQU_A = 3.0,
                                          NOMBRE = 1)))

# Run simulation 
# MFRONT
result = STAT_NON_LINE(MODELE = model,
                       CHAM_MATER = mater,
                       EXCIT = (_F(CHARGE = disp_bc),
                                _F(CHARGE = tens_bc,
                                   FONC_MULT = sigma_fn),
                                _F(CHARGE = shear_bc,
                                   FONC_MULT = tau_fn)),
                       COMPORTEMENT = _F(RELATION = 'MFRONT',
                                         NOM_ROUTINE = 'asterplasticity',
                                         UNITE_LIBRAIRIE = 39,
                                         ITER_INTE_MAXI = 30,
                                         RESI_INTE_MAXI = 1.E-8),
                       INCREMENT = _F(LIST_INST = l_times),
                       NEWTON = _F(MATRICE = 'TANGENTE',
                                   REAC_ITER = 3),
                       RECH_LINEAIRE = _F(RHO_MAX=150))


# Calculate additional data 
result = CALC_CHAMP(reuse = result,
                    RESULTAT = result,
                    CRITERES = ('SIEQ_ELGA'),
                    CONTRAINTE = ('SIGM_ELNO', 'SIGM_ELGA'),
                    VARI_INTERNE = ('VARI_ELNO'),
                    DEFORMATION = ('EPSI_ELNO', 'EPSI_ELGA', 'EPSP_ELGA'))

# Extract internal variables 
intvar = CREA_CHAMP(TYPE_CHAM = 'ELNO_VARI_R',
                    OPERATION = 'EXTR',
                    RESULTAT = result,
                    NOM_CHAM = 'VARI_ELNO',
                    INST = 1.)

# Write results for visualization
IMPR_RESU(FORMAT= 'MED',
          RESU = _F(RESULTAT = result),
          UNITE = 80)

# Extract results for stress-strain plots (xx and xy)
sig_xx_1 = RECU_FONCTION(GROUP_MA = ('cube'),
                         NOM_CHAM = 'SIGM_ELGA',
                         NOM_CMP = 'SIXX',
                         POINT = 1,
                         RESULTAT = result)
eps_xx_1 = RECU_FONCTION(GROUP_MA = ('cube'),
                         NOM_CHAM = 'EPSI_ELGA',
                         NOM_CMP = 'EPXX',
                         POINT = 1,
                         RESULTAT = result)
sig_xy_1 = RECU_FONCTION(GROUP_MA = ('cube'),
                         NOM_CHAM = 'SIGM_ELGA',
                         NOM_CMP = 'SIXY',
                         POINT = 1,
                         RESULTAT = result)
eps_xy_1 = RECU_FONCTION(GROUP_MA = ('cube'),
                         NOM_CHAM = 'EPSI_ELGA',
                         NOM_CMP = 'EPXY',
                         POINT = 1,
                         RESULTAT = result)

# Write results for plotting
IMPR_FONCTION(COURBE = (_F(FONCTION = sig_xx_1),
                        _F(FONCTION = eps_xx_1)),
              FORMAT = 'TABLEAU',
              SEPARATEUR = ',',
              UNITE = 8)
IMPR_FONCTION(COURBE = (_F(FONCTION = sig_xy_1),
                        _F(FONCTION = eps_xy_1)),
              FORMAT = 'TABLEAU',
              SEPARATEUR = ',',
              UNITE = 8)

# Verification tests
TEST_RESU(CHAM_ELEM = _F(GROUP_NO = 'node_2',
                         NOM_CMP = 'V7',
                         GROUP_MA = 'cube',
                         CHAM_GD = intvar,
                         VALE_CALC = 0.020547265463595))

TEST_RESU(RESU = (_F(INST = 1.0,
                     RESULTAT = result,
                     NOM_CHAM = 'SIGM_ELNO',
                     GROUP_NO = 'node_2',
                     NOM_CMP = 'SIXX',
                     VALE_CALC = 151.20000000002,
                     VALE_REFE = 151.2,
                     REFERENCE = 'ANALYTIQUE',
                     PRECISION = 1.E-3,
                     GROUP_MA = 'cube'),
                  _F(INST = 1.0,
                     RESULTAT = result,
                     NOM_CHAM = 'SIGM_ELNO',
                     GROUP_NO = 'node_2',
                     NOM_CMP = 'SIXY',
                     VALE_CALC = 93.100000000001,
                     VALE_REFE = 93.1,
                     REFERENCE = 'ANALYTIQUE',
                     PRECISION = 1.E-3,
                     GROUP_MA = 'cube'),
                  _F(INST = 1.0,
                     RESULTAT = result,
                     NOM_CHAM = 'EPSI_ELNO',
                     GROUP_NO = 'node_2',
                     NOM_CMP = 'EPXX',
                     VALE_CALC = 0.014829713606886,
                     VALE_REFE = 0.0148297,
                     REFERENCE = 'ANALYTIQUE',
                     PRECISION = 1.E-3,
                     GROUP_MA = 'cube'),
                  _F(INST = 1.0,
                     RESULTAT = result,
                     NOM_CHAM = 'EPSI_ELNO',
                     GROUP_NO = 'node_2',
                     NOM_CMP = 'EPXY',
                     VALE_CALC = 0.013601401082429,
                     VALE_REFE = 0.0136014,
                     REFERENCE = 'ANALYTIQUE',
                     PRECISION = 1.E-3,
                     GROUP_MA = 'cube'),
                  _F(INST = 1.0,
                     POINT = 1,
                     RESULTAT = result,
                     NOM_CHAM = 'EPSP_ELGA',
                     NOM_CMP = 'EPXX',
                     VALE_CALC = 0.014054328991501,
                     VALE_REFE = 0.014054,
                     REFERENCE = 'ANALYTIQUE',
                     PRECISION = 1.E-3,
                     GROUP_MA = 'cube'),
                  _F(INST = 1.0,
                     POINT = 1,
                     RESULTAT = result,
                     NOM_CHAM = 'EPSP_ELGA',
                     NOM_CMP = 'EPXY',
                     VALE_CALC = 0.012980734415762,
                     VALE_REFE = 0.012981,
                     REFERENCE = 'ANALYTIQUE',
                     PRECISION = 1.E-3,
                     GROUP_MA = 'cube'),
                  _F(INST = 2.0,
                     RESULTAT = result,
                     NOM_CHAM = 'EPSI_ELNO',
                     GROUP_NO = 'node_2',
                     NOM_CMP = 'EPXX',
                     VALE_CALC = 0.035324711958965,
                     VALE_REFE = 0.035265,
                     REFERENCE = 'ANALYTIQUE',
                     PRECISION = 1.E-2,
                     GROUP_MA = 'cube'),
                  _F(INST = 2.0,
                     RESULTAT = result,
                     NOM_CHAM = 'EPSI_ELNO',
                     GROUP_NO = 'node_2',
                     NOM_CMP = 'EPXY',
                     VALE_CALC = 0.020351415903945,
                     VALE_REFE = 0.020471,
                     REFERENCE = 'ANALYTIQUE',
                     PRECISION = 1.E-2,
                     GROUP_MA = 'cube'),
                  _F(INST = 2.0,
                     POINT = 1,
                     RESULTAT = result,
                     NOM_CHAM = 'EPSP_ELGA',
                     NOM_CMP = 'EPXX',
                     VALE_CALC = 0.034005737600229,
                     VALE_REFE = 0.033946,
                     REFERENCE = 'ANALYTIQUE',
                     PRECISION = 1.E-2,
                     GROUP_MA = 'cube'),
                  _F(INST = 2.0,
                     POINT = 1,
                     RESULTAT = result,
                     NOM_CHAM = 'EPSP_ELGA',
                     NOM_CMP = 'EPXY',
                     VALE_CALC = 0.020130749237395,
                     VALE_REFE = 0.02025,
                     REFERENCE = 'ANALYTIQUE',
                     PRECISION = 1.E-2,
                     GROUP_MA = 'cube'),
                  _F(INST = 1.0,
                     POINT = 1,
                     RESULTAT = result,
                     NOM_CHAM = 'SIEQ_ELGA',
                     NOM_CMP = 'TRIAX',
                     VALE_CALC = 0.22799999050003,
                     VALE_REFE = 0.228,
                     REFERENCE = 'ANALYTIQUE',
                     GROUP_MA = 'cube'),
                  _F(INST = 2.0,
                     POINT = 8,
                     RESULTAT = result,
                     NOM_CHAM = 'SIEQ_ELGA',
                     NOM_CMP = 'TRIAX',
                     VALE_CALC = 0.32534865272784,
                     VALE_REFE = 0.325349,
                     REFERENCE = 'ANALYTIQUE',
                     GROUP_MA = 'cube')))

# End
FIN()
