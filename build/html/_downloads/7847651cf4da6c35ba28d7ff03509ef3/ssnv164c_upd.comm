# SSNV164C

DEBUT(LANG = 'EN')

# Read mesh
mesh_ini = LIRE_MAILLAGE(FORMAT = 'MED')

# Convert cables 3 and 4 from linear to quad for sheath elements
mesh = CREA_MAILLAGE(MAILLAGE = mesh_ini,
                     LINE_QUAD = _F(GROUP_MA = ('cable3', 'cable4')))

# Change outward normal orientation on surfaces
mesh = MODI_MAILLAGE(reuse = mesh,
                     MAILLAGE = mesh,
                     ORIE_LIGNE = (_F(GROUP_MA = 'cable1'),
                                   _F(GROUP_MA = 'cable2'),
                                   _F(GROUP_MA = 'cable3'),
                                   _F(GROUP_MA = 'cable4'),
                                   _F(GROUP_MA = 'cable0')))

# Define node groups from element groups
mesh = DEFI_GROUP(reuse = mesh,
                  MAILLAGE = mesh,
                  CREA_GROUP_NO = (_F(GROUP_MA = 'bot_face'),
                                   _F(GROUP_MA = 'cable1'),
                                   _F(GROUP_MA = 'cable2'),
                                   _F(GROUP_MA = 'cable3'),
                                   _F(GROUP_MA = 'cable4'),
                                   _F(GROUP_MA = 'cable0')))

# Assign finite elements
model = AFFE_MODELE(MAILLAGE = mesh,
                    AFFE = (_F(GROUP_MA = 'vol_all',
                               PHENOMENE = 'MECANIQUE',
                               MODELISATION = '3D'),
                            _F(GROUP_MA = ('cable1', 'cable2', 'cable0'),
                               PHENOMENE = 'MECANIQUE',
                               MODELISATION = 'BARRE'),
                            _F(GROUP_MA = ('cable3', 'cable4'),
                               PHENOMENE = 'MECANIQUE',
                               MODELISATION = 'CABLE_GAINE')))

# Assign cable sections
section = AFFE_CARA_ELEM(MODELE = model,
                         BARRE = _F(GROUP_MA = ('cable1','cable2','cable3','cable4','cable0'),
                                    SECTION = 'CERCLE',
                                    CARA = 'R',
                                    VALE = 2.8209E-2))

# Define materials
concrete = DEFI_MATERIAU(ELAS = _F(E = 4.0E10,
                                   NU = 0.20,
                                   RHO = 2500.0),
                         BPEL_BETON = _F())

steel = DEFI_MATERIAU(ELAS = _F(E = 1.93E11,
                                NU = 0.3,
                                RHO = 7850.0),
                      BPEL_ACIER = _F(F_PRG = 1.94E9,
                                      FROT_COURB = 0.0,
                                      FROT_LINE = 1.5E-3),
                      ECRO_LINE = _F(SY = 1.94E11,
                                     D_SIGM_EPSI = 1000.0))

sheath = DEFI_MATERIAU(ELAS = _F(E = 1.93E11,
                                 NU = 0.3,
                                 RHO = 7850.0),
                       BPEL_ACIER = _F(F_PRG = 1.94E9,
                                       FROT_COURB = 0.0,
                                       FROT_LINE = 1.5E-3),
                       ECRO_LINE = _F(SY = 1.94E11,
                                      D_SIGM_EPSI = 1000.0),
                       CABLE_GAINE_FROT = _F(TYPE = 'ADHERENT', 
                                             PENA_LAGR = 1.E14))

# Assign materials
mater = AFFE_MATERIAU(MAILLAGE = mesh,
                      AFFE = (_F(GROUP_MA = 'vol_all',
                                 MATER = concrete),
                              _F(GROUP_MA = ('cable1', 'cable2', 'cable0'),
                                 MATER = steel),
                              _F(GROUP_MA = ('cable3', 'cable4'),
                                 MATER = sheath)))

# Define prestress in cables
pre_sig1 = DEFI_CABLE_BP(MODELE = model,
                         CHAM_MATER = mater,
                         CARA_ELEM = section,
                         GROUP_MA_BETON = 'vol_all',
                         DEFI_CABLE = _F(GROUP_MA = 'cable1',
                                         GROUP_NO_ANCRAGE = ('pbot1','ptop1')),
                         TYPE_ANCRAGE = ('ACTIF','PASSIF'),
                         TENSION_INIT = 3.75E6,
                         RECUL_ANCRAGE = 0.001)

pre_sig2 = DEFI_CABLE_BP(MODELE = model,
                         CHAM_MATER = mater,
                         CARA_ELEM = section,
                         GROUP_MA_BETON = 'vol_all',
                         DEFI_CABLE = _F(GROUP_MA = 'cable2',
                                         GROUP_NO_ANCRAGE = ('pbot2','ptop2')),
                         TYPE_ANCRAGE = ('ACTIF','PASSIF'),
                         TENSION_INIT = 3.75E6,
                         RECUL_ANCRAGE = 0.001)

pre_sig3 = DEFI_CABLE_BP(MODELE = model,
                         CHAM_MATER = mater,
                         CARA_ELEM = section,
                         GROUP_MA_BETON = 'vol_all',
                         DEFI_CABLE = _F(GROUP_MA = 'cable3',
                                         GROUP_NO_ANCRAGE = ('pbot3','ptop3')),
                         TYPE_ANCRAGE = ('ACTIF','PASSIF'),
                         TENSION_INIT = 3.75E6,
                         RECUL_ANCRAGE = 0.001)

pre_sig4 = DEFI_CABLE_BP(MODELE = model,
                         CHAM_MATER = mater,
                         CARA_ELEM = section,
                         GROUP_MA_BETON = 'vol_all',
                         DEFI_CABLE = _F(GROUP_MA = 'cable4',
                                       GROUP_NO_ANCRAGE = ('pbot4','ptop4')),
                         TYPE_ANCRAGE = ('ACTIF','PASSIF'),
                         TENSION_INIT = 3.75E6,
                         RECUL_ANCRAGE = 0.001)

pre_sig0 = DEFI_CABLE_BP(MODELE = model,
                         CHAM_MATER = mater,
                         CARA_ELEM = section,
                         GROUP_MA_BETON = 'vol_all',
                         DEFI_CABLE = _F(GROUP_MA = 'cable0',
                                       GROUP_NO_ANCRAGE = ('pbot0','ptop0')),
                         TYPE_ANCRAGE = ('ACTIF','ACTIF'),
                         TENSION_INIT = 3.750000E6,
                         RECUL_ANCRAGE = 0.001,
                         INFO = 2)

# Assign displacement bcs and gravity
bc_u  = AFFE_CHAR_MECA(MODELE = model,
                       DDL_IMPO = (_F(GROUP_NO = 'center_nodes',
                                      DX = 0.0, DY = 0.0),
                                   _F(GROUP_NO = 'px',
                                      DY = 0.0),
                                   _F(GROUP_NO = 'py',
                                      DX = 0.0),
                                   _F(GROUP_NO = 'bot_face',
                                      DZ = 0.0)),
                       PESANTEUR = _F(GRAVITE = 9.8100000000000005,
                                      DIRECTION = (0.0, 0.0, -1.0)))

bc_s  = AFFE_CHAR_MECA(MODELE = model,
                       DDL_IMPO = (_F(GROUP_NO = ('pbot3', 'ptop3', 'pbot4', 'ptop4'),
                                      GLIS = 0.0)))

# Assign sheathed cable BCs
# Assign prestress to cables
bc_f_1 = AFFE_CHAR_MECA(MODELE = model,
                        RELA_CINE_BP = _F(CABLE_BP = pre_sig1,
                                          SIGM_BPEL = 'NON',
                                          RELA_CINE = 'OUI'))

bc_f_2 = AFFE_CHAR_MECA(MODELE = model,
                        RELA_CINE_BP = _F(CABLE_BP = pre_sig2,
                                          SIGM_BPEL = 'NON',
                                          RELA_CINE = 'OUI'))

bc_f_3 = AFFE_CHAR_MECA(MODELE = model,
                        RELA_CINE_BP = _F(CABLE_BP = pre_sig3,
                                          SIGM_BPEL = 'NON',
                                          RELA_CINE = 'OUI'))

bc_f_4 = AFFE_CHAR_MECA(MODELE = model,
                        RELA_CINE_BP = _F(CABLE_BP = pre_sig4,
                                          SIGM_BPEL = 'NON',
                                          RELA_CINE = 'OUI'))

bc_f_0 = AFFE_CHAR_MECA(MODELE = model,
                        RELA_CINE_BP = _F(CABLE_BP = pre_sig0,
                                          SIGM_BPEL = 'NON',
                                          RELA_CINE = 'OUI'))

# Set up timesteps
l_times = DEFI_LIST_REEL(VALE = (0.0, 150.0, 300.0, 450.0, 600.0))

# Step 1: Apply gravity
#--------------------------------------------------------
result1 = STAT_NON_LINE(MODELE = model,
                     CHAM_MATER = mater,
                     CARA_ELEM = section,
                     COMPORTEMENT = (_F(RELATION = 'ELAS'),
                                     _F(RELATION = 'SANS',
                                        GROUP_MA =  ('cable1', 'cable2', 'cable0')),
                                     _F(RELATION = 'KIT_CG',
                                        RELATION_KIT = ('SANS', 'CABLE_GAINE_FROT'),
                                        GROUP_MA =  ('cable3', 'cable4'))),
                     EXCIT = (_F(CHARGE = bc_u),
                              _F(CHARGE = bc_s),
                              _F(CHARGE = bc_f_1),
                              _F(CHARGE = bc_f_2),
                              _F(CHARGE = bc_f_3),
                              _F(CHARGE = bc_f_4),
                              _F(CHARGE = bc_f_0)),
                     INCREMENT = _F(LIST_INST = l_times,
                                    INST_FIN = 150.0),
                     SOLVEUR = _F(METHODE = 'MUMPS'))
                                  
# Step 2: Prestress cables 1 and 2
#--------------------------------------------------------
result1 =  CALC_PRECONT(reuse = result1,
                        ETAT_INIT = _F(EVOL_NOLI = result1),
                        MODELE = model,
                        CHAM_MATER = mater,
                        CARA_ELEM = section,
                        COMPORTEMENT = (_F(RELATION = 'ELAS',
                                           GROUP_MA = 'vol_all'),
                                        _F(RELATION = 'VMIS_ISOT_LINE',
                                           GROUP_MA =  ('cable1', 'cable2', 'cable0')),
                                        _F(RELATION = 'KIT_CG',
                                           RELATION_KIT = ('VMIS_ISOT_LINE', 'CABLE_GAINE_FROT'),
                                           GROUP_MA =  ('cable3', 'cable4'))),
                        EXCIT = (_F(CHARGE = bc_u),
                                 _F(CHARGE = bc_s,
                                    TYPE_CHARGE = 'DIDI')),
                        CABLE_BP = (pre_sig1, pre_sig2),
                        CABLE_BP_INACTIF = (pre_sig3, pre_sig4, pre_sig0),
                        INCREMENT = _F(LIST_INST = l_times,
                                       INST_FIN  = 300.0),
                        SOLVEUR = _F(METHODE = 'MUMPS'))

# Step 3: Prestress cables 3 and 4
#--------------------------------------------------------
result1 = CALC_PRECONT(reuse = result1,
                       ETAT_INIT = _F(EVOL_NOLI = result1),
                       MODELE = model,
                       CHAM_MATER = mater,
                       CARA_ELEM = section,
                       COMPORTEMENT = (_F(RELATION = 'ELAS',
                                          GROUP_MA = 'vol_all'),
                                       _F(RELATION = 'VMIS_ISOT_LINE',
                                          GROUP_MA =  ('cable1', 'cable2', 'cable0')),
                                       _F(RELATION = 'KIT_CG',
                                          RELATION_KIT = ('VMIS_ISOT_LINE', 'CABLE_GAINE_FROT'),
                                          GROUP_MA =  ('cable3', 'cable4'))),
                       EXCIT = (_F(CHARGE = bc_u),
                                _F(CHARGE = bc_s,
                                   TYPE_CHARGE = 'DIDI'),
                                _F(CHARGE = bc_f_1),
                                _F(CHARGE = bc_f_2)),
                       CABLE_BP = ( pre_sig3 , pre_sig4 ),
                       CABLE_BP_INACTIF = ( pre_sig0 ),
                       INCREMENT = _F(LIST_INST = l_times,
                                      INST_FIN  = 450.0),
                       SOLVEUR = _F(METHODE = 'MUMPS'),
                       CONVERGENCE = _F(ITER_GLOB_MAXI = 20,
                                        RESI_REFE_RELA = 1.E-6,
                                        EFFORT_REFE = 1.E5,
                                        MOMENT_REFE = 0,
                                        SIGM_REFE = 1.E6,
                                        DEPL_REFE = 1.E-1))

# Step 4: Prestress cable 0
#-----------------------------------------------------------
result1 = CALC_PRECONT(reuse = result1,
                       ETAT_INIT = _F(EVOL_NOLI = result1),
                       MODELE = model,
                       CHAM_MATER = mater,
                       CARA_ELEM = section,
                       COMPORTEMENT = (_F(RELATION = 'ELAS',
                                          GROUP_MA = 'vol_all'),
                                       _F(RELATION = 'VMIS_ISOT_LINE',
                                          GROUP_MA =  ('cable1', 'cable2', 'cable0')),
                                       _F(RELATION = 'KIT_CG',
                                          RELATION_KIT = ('VMIS_ISOT_LINE', 'CABLE_GAINE_FROT'),
                                          GROUP_MA =  ('cable3', 'cable4'))),
                       EXCIT = (_F(CHARGE = bc_u),
                                _F(CHARGE = bc_s,
                                   TYPE_CHARGE = 'DIDI'),
                                _F(CHARGE = bc_f_1),
                                _F(CHARGE = bc_f_2),
                                _F(CHARGE = bc_f_3),
                                _F(CHARGE = bc_f_4)),
                       CABLE_BP = ( pre_sig0 ),
                       INCREMENT = _F(LIST_INST = l_times,
                                      INST_FIN = 600.0))

# Compute stresses
result1 = CALC_CHAMP(reuse = result1,
                     CRITERES = ('SIEQ_ELNO'),
                     CONTRAINTE = ('SIEF_ELNO', 'SIEF_NOEU'),
                     RESULTAT = result1)

# Create a table for cable 3
table = RECU_TABLE(CO = pre_sig3, 
                   NOM_TABLE = 'CABLE_BP')

table = CALC_TABLE(reuse = table, 
                   TABLE = table,
                   ACTION = (_F(OPERATION = 'RENOMME',
                                NOM_PARA = ('NOEUD_CABLE', 'NOEUD'))))

# Create tables for the results
table2 = CREA_TABLE(RESU = _F(RESULTAT = result1, 
                              NOM_CHAM = 'SIEF_NOEU', 
                              GROUP_MA = ('cable3'), 
                              NOM_CMP = 'N',
                              INST = 450.0))

table3 = CREA_TABLE(RESU = _F(RESULTAT = result1, 
                              NOM_CHAM = 'DEPL', 
                              GROUP_MA = ('cable3'), 
                              NOM_CMP = 'GLIS',
                              INST = 450.0))

tab_com2 = CALC_TABLE(TABLE = table, 
                      ACTION = _F(OPERATION = 'COMB',
                                  TABLE = table2, 
                                  NOM_PARA='NOEUD'))

tab_com3 = CALC_TABLE(TABLE = tab_com2, 
                      ACTION = _F(OPERATION = 'COMB',
                                  TABLE = table3, 
                                  NOM_PARA='NOEUD'))

# Print table
IMPR_TABLE(TABLE = tab_com3, 
           UNITE = 45,
           NOM_PARA = ('NOEUD', 'ABSC_CURV', 'TENSION', 'INST', 'N', 'GLIS'))

# Write output
IMPR_RESU(FORMAT = 'MED',
          UNITE = 80,
          RESU = _F(RESULTAT = result1,
                    INST = (300, 450, 600),
                    NOM_CHAM = ('DEPL', 'SIEF_NOEU', 'SIEF_ELNO', 'SIEQ_ELNO'), 
                    GROUP_MA = 'vol_all'))

IMPR_RESU(FORMAT = 'MED',
          UNITE = 81,
          RESU = _F(RESULTAT = result1,
                    INST = (300, 450, 600),
                    NOM_CHAM = ('DEPL', 'SIEF_NOEU', 'SIEF_ELNO'), 
                    GROUP_MA = ('cable1', 'cable2', 'cable0')))

IMPR_RESU(FORMAT = 'MED',
          UNITE = 82,
          RESU = _F(RESULTAT = result1,
                    INST = (300, 450, 600),
                    NOM_CHAM = ('DEPL', 'SIEF_NOEU', 'SIEF_ELNO'), 
                    GROUP_MA = ('cable3', 'cable4')))

#----------------------------------------------
# Verification

#  cable 1 - t = 300s
TEST_RESU(RESU = (_F(INST = 300.0,
                     GROUP_NO = 'N1', GROUP_MA = 'M5655',
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result1, NOM_CHAM = 'SIEF_ELNO', NOM_CMP = 'N',
                     VALE_CALC =  3.64939314E+06, VALE_REFE = 3.648000E6,
                     CRITERE = 'RELATIF', PRECISION = 1.E-3),
                  _F(INST = 300.0,
                     GROUP_NO = 'N6', GROUP_MA = 'M5660',
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result1, NOM_CHAM = 'SIEF_ELNO', NOM_CMP = 'N',
                     VALE_CALC =  3.67686649E+06, VALE_REFE = 3.675000E6,
                     CRITERE = 'RELATIF', PRECISION = 1.E-3),
                  _F(INST = 300.0,
                     GROUP_NO = 'N11', GROUP_MA = 'M5664',
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result1, NOM_CHAM = 'SIEF_ELNO', NOM_CMP = 'N',
                     VALE_CALC =  3.69519477E+06, VALE_REFE = 3.693000E6,
                     CRITERE = 'RELATIF', PRECISION = 1.E-3),
                  _F(INST = 300.0,
                     GROUP_NO = 'N16', GROUP_MA = 'M5670',
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result1, NOM_CHAM = 'SIEF_ELNO', NOM_CMP = 'N',
                     VALE_CALC =  3.66381927E+06, VALE_REFE = 3.667000E6,
                     CRITERE = 'RELATIF', PRECISION = 1.E-3),
                  _F(INST = 300.0,
                     GROUP_NO = 'N101', GROUP_MA = 'M5674',
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result1, NOM_CHAM = 'SIEF_ELNO', NOM_CMP = 'N',
                     VALE_CALC =  3.64190218E+06, VALE_REFE = 3.640000E6,
                     CRITERE = 'RELATIF', PRECISION = 1.E-3)))

#  cable 1 - t = 450s
TEST_RESU(RESU = (_F(INST = 450.0,
                     GROUP_NO = 'N1', GROUP_MA = 'M5655',
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result1, NOM_CHAM = 'SIEF_ELNO', NOM_CMP = 'N',
                     VALE_CALC =  3563432.35935, VALE_REFE = 3.561000E6,
                     CRITERE = 'RELATIF', PRECISION = 1.E-2),
                  _F(INST = 450.0,
                     GROUP_NO = 'N6', GROUP_MA = 'M5660',
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result1, NOM_CHAM = 'SIEF_ELNO', NOM_CMP = 'N',
                     VALE_CALC =  3590257.68864, VALE_REFE = 3.588000E6,
                     CRITERE = 'RELATIF', PRECISION = 1.E-2),
                  _F(INST = 450.0,
                     GROUP_NO = 'N11', GROUP_MA = 'M5664',
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result1, NOM_CHAM = 'SIEF_ELNO', NOM_CMP = 'N',
                     VALE_CALC =  3.6131997608551E+06, VALE_REFE = 3.628000E6,
                     CRITERE = 'RELATIF', PRECISION = 1.E-2),
                  _F(INST = 450.0,
                     GROUP_NO = 'N16', GROUP_MA = 'M5670',
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result1, NOM_CHAM = 'SIEF_ELNO', NOM_CMP = 'N',
                     VALE_CALC =  3.6417872276443E+06, VALE_REFE = 3.645000E6,
                     CRITERE = 'RELATIF', PRECISION = 1.E-2),
                  _F(INST = 450.0,
                     GROUP_NO = 'N101', GROUP_MA = 'M5674',
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result1, NOM_CHAM = 'SIEF_ELNO', NOM_CMP = 'N',
                     VALE_CALC =  3622363.10282, VALE_REFE = 3.629000E6,
                     CRITERE = 'RELATIF', PRECISION = 1.E-2)))

#  cable 1 - t = 600s
TEST_RESU(RESU = (_F(INST = 600.0,
                     GROUP_NO = 'N1', GROUP_MA = 'M5655',
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result1, NOM_CHAM = 'SIEF_ELNO', NOM_CMP = 'N',
                     VALE_CALC =  3521446.74399, VALE_REFE = 3.519000E6,
                     CRITERE = 'RELATIF', PRECISION = 1.E-2),
                  _F(INST = 600.0,
                     GROUP_NO = 'N6', GROUP_MA = 'M5660',
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result1, NOM_CHAM = 'SIEF_ELNO', NOM_CMP = 'N',
                     VALE_CALC =  3547948.70256, VALE_REFE = 3.546000E6,
                     CRITERE = 'RELATIF', PRECISION = 1.E-2),
                  _F(INST = 600.0,
                     GROUP_NO = 'N11', GROUP_MA = 'M5664',
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result1, NOM_CHAM = 'SIEF_ELNO', NOM_CMP = 'N',
                     VALE_CALC =  3573040.20398, VALE_REFE = 3.597000E6,
                     CRITERE = 'RELATIF', PRECISION = 1.E-2),
                  _F(INST = 600.0,
                     GROUP_NO = 'N16', GROUP_MA = 'M5670',
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result1, NOM_CHAM = 'SIEF_ELNO', NOM_CMP = 'N',
                     VALE_CALC =  3630831.20054, VALE_REFE = 3.635000E6,
                     CRITERE = 'RELATIF', PRECISION = 1.E-2),
                  _F(INST = 600.0,
                     GROUP_NO = 'N101', GROUP_MA = 'M5674',
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result1, NOM_CHAM = 'SIEF_ELNO', NOM_CMP = 'N',
                     VALE_CALC =  3604596.38279, VALE_REFE = 3.614000E6,
                     CRITERE = 'RELATIF', PRECISION = 1.E-2)))

#  cable 3 - t = 450s
TEST_RESU(RESU = (_F(INST = 450.0,
                     GROUP_NO = 'N41', GROUP_MA = 'M5695',
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result1, NOM_CHAM = 'SIEF_ELNO', NOM_CMP = 'N',
                     VALE_CALC =  3649300.02066, VALE_REFE = 3.647000E6,
                     CRITERE = 'RELATIF', PRECISION = 1.E-3),
                  _F(INST = 450.0,
                     GROUP_NO = 'N46', GROUP_MA = 'M5700',
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result1, NOM_CHAM = 'SIEF_ELNO', NOM_CMP = 'N',
                     VALE_CALC =  3676754.27367, VALE_REFE = 3.675000E6,
                     CRITERE = 'RELATIF', PRECISION = 1.E-3),
                  _F(INST = 450.0,
                     GROUP_NO = 'N51', GROUP_MA = 'M5705',
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result1, NOM_CHAM = 'SIEF_ELNO', NOM_CMP = 'N',
                     VALE_CALC =  3.6914158636553E+06, VALE_REFE = 3.695000E6,
                     CRITERE = 'RELATIF', PRECISION = 1.E-3),
                  _F(INST = 450.0,
                     GROUP_NO = 'N56', GROUP_MA = 'M5710',
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result1, NOM_CHAM = 'SIEF_ELNO', NOM_CMP = 'N',
                     VALE_CALC =  3663845.34959, VALE_REFE = 3.667000E6,
                     CRITERE = 'RELATIF', PRECISION = 1.E-3),
                  _F(INST = 450.0,
                     GROUP_NO = 'N103', GROUP_MA = 'M5714',
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result1, NOM_CHAM = 'SIEF_ELNO', NOM_CMP = 'N',
                     VALE_CALC =  3641896.63895, VALE_REFE = 3.640000E6,
                     CRITERE = 'RELATIF', PRECISION = 1.E-3)))

#  cable 3 - t = 600s
TEST_RESU(RESU = (_F(INST = 600.0,
                     GROUP_NO = 'N41', GROUP_MA = 'M5695',
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result1, NOM_CHAM = 'SIEF_ELNO', NOM_CMP = 'N',
                     VALE_CALC =  3607433.30285, VALE_REFE = 3.605000E6,
                     CRITERE = 'RELATIF', PRECISION = 2.5E-2),
                  _F(INST = 600.0,
                     GROUP_NO = 'N46', GROUP_MA = 'M5700',
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result1, NOM_CHAM = 'SIEF_ELNO', NOM_CMP = 'N',
                     VALE_CALC =  3634420.5768, VALE_REFE = 3.632000E6,
                     CRITERE = 'RELATIF', PRECISION = 2.5E-2),
                  _F(INST = 600.0,
                     GROUP_NO = 'N51', GROUP_MA = 'M5705',
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result1, NOM_CHAM = 'SIEF_ELNO', NOM_CMP = 'N',
                     VALE_CALC =  3671918.98471, VALE_REFE = 3.662000E6,
                     CRITERE = 'RELATIF', PRECISION = 1.E-2),
                  _F(INST = 600.0,
                     GROUP_NO = 'N56', GROUP_MA = 'M5710',
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result1, NOM_CHAM = 'SIEF_ELNO', NOM_CMP = 'N',
                     VALE_CALC =  3652867.31059, VALE_REFE = 3.655000E6,
                     CRITERE = 'RELATIF', PRECISION = 1.E-2),
                  _F(INST = 600.0,
                     GROUP_NO = 'N103', GROUP_MA = 'M5714',
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result1, NOM_CHAM = 'SIEF_ELNO', NOM_CMP = 'N',
                     VALE_CALC =  3624130.28927, VALE_REFE = 3.625000E6,
                     CRITERE = 'RELATIF', PRECISION = 1.E-2)))

#  cable 5 - t = 600s
TEST_RESU(RESU = (_F(INST = 600.0,
                     GROUP_NO = 'N81', GROUP_MA = 'M5735',
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result1, NOM_CHAM = 'SIEF_ELNO', NOM_CMP = 'N',
                     VALE_CALC =  3.64939314E+06, VALE_REFE = 3.647000E6,
                     CRITERE = 'RELATIF', PRECISION = 1.E-3),
                  _F(INST = 600.0,
                     GROUP_NO = 'N86', GROUP_MA = 'M5740',
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result1, NOM_CHAM = 'SIEF_ELNO', NOM_CMP = 'N',
                     VALE_CALC =  3.67686649E+06, VALE_REFE = 3.674000E6,
                     CRITERE = 'RELATIF', PRECISION = 1.E-3),
                  _F(INST = 600.0,
                     GROUP_NO = 'N91', GROUP_MA = 'M5745',
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result1, NOM_CHAM = 'SIEF_ELNO', NOM_CMP = 'N',
                     VALE_CALC =  3.69519477E+06, VALE_REFE = 3.695000E6,
                     CRITERE = 'RELATIF', PRECISION = 1.E-3),
                  _F(INST = 600.0,
                     GROUP_NO = 'N96', GROUP_MA = 'M5750',
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result1, NOM_CHAM = 'SIEF_ELNO', NOM_CMP = 'N',
                     VALE_CALC =  3.67135532E+06, VALE_REFE = 3.674000E6,
                     CRITERE = 'RELATIF', PRECISION = 1.E-3),
                  _F(INST = 600.0,
                     GROUP_NO = 'N105', GROUP_MA = 'M5754',
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result1, NOM_CHAM = 'SIEF_ELNO', NOM_CMP = 'N',
                     VALE_CALC =  3.64939314E+06, VALE_REFE = 3.647000E6,
                     CRITERE = 'RELATIF', PRECISION = 1.E-3)))

# End
FIN()
