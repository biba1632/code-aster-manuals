#!/usr/bin/env python
# coding: utf-8

import gmsh
import sys
import math
import numpy as np

gmsh.initialize()

# Use OpenCASCADE kernel
model = gmsh.model
occ = model.occ
mesh = model.mesh
model.add("ssnv112c")

# Geometry
ri=0.1
re=0.2
c225 = np.cos(22.5*math.pi/180)
s225 = np.sin(22.5*math.pi/180)
NR = 10
NT = 5

# Default element size
dens = 0.01;

# Points of construction
PO = occ.addPoint(0, 0, 0,  dens)

# Points of the plane of the base z=0
A = occ.addPoint(0, ri, 0., dens)
B = occ.addPoint(0, re, 0., dens)
C = occ.addPoint(-ri*s225, ri*c225, 0, dens)
D = occ.addPoint(-re*s225, re*c225, 0, dens)
E = occ.addPoint(-(ri/(2**0.5)) , (ri/(2**0.5)), 0.,  dens)
F = occ.addPoint(-re/(2**0.5) , re/(2**0.5) , 0.,  dens)

# Lines
LAB = occ.addLine(A, B)
LBD = occ.addCircleArc(B, PO, D)
LDC = occ.addLine(D, C)
LCA = occ.addCircleArc(C, PO, A)

LDF = occ.addCircleArc(D, PO, F)
LFE = occ.addLine(F, E)
LEC = occ.addCircleArc(E, PO, C)

# Surfaces
tri_loop = occ.addCurveLoop([LAB, LBD, LDC, LCA])
tri = occ.addPlaneSurface([tri_loop])
quad_loop = occ.addCurveLoop([-LDC, LDF, LFE, LEC])
quad = occ.addPlaneSurface([quad_loop])

occ.synchronize()

# Physical groups for the points
A_g = model.addPhysicalGroup(0, [A])
model.setPhysicalName(0, A_g, 'A')
B_g = model.addPhysicalGroup(0, [B])
model.setPhysicalName(0, B_g, 'B')
C_g = model.addPhysicalGroup(0, [C])
model.setPhysicalName(0, C_g, 'C')
D_g = model.addPhysicalGroup(0, [D])
model.setPhysicalName(0, D_g, 'D')
E_g = model.addPhysicalGroup(0, [E])
model.setPhysicalName(0, E_g, 'E')
F_g = model.addPhysicalGroup(0, [F])
model.setPhysicalName(0, F_g, 'F')

# Physical groups for the edges
AB_g = model.addPhysicalGroup(1, [LAB])
model.setPhysicalName(1, AB_g, 'AB')
EF_g = model.addPhysicalGroup(1, [LFE])
model.setPhysicalName(1, EF_g, 'EF')
AE_g = model.addPhysicalGroup(1, [LCA, LEC])
model.setPhysicalName(1, AE_g, 'AE')
BF_g = model.addPhysicalGroup(1, [LBD, LDF])
model.setPhysicalName(1, BF_g, 'DF')

# Physical group for the faces
VOLUME_g = model.addPhysicalGroup(2, [tri, quad])
model.setPhysicalName(2, VOLUME_g, 'VOLUME')

num_nodes = NT+1
for curve in [LCA, LEC, LBD, LDF]:
    #print(curve)
    mesh.setTransfiniteCurve(curve, num_nodes)
num_nodes = NR+1
for curve in [LAB, LFE]:
    #print(curve)
    mesh.setTransfiniteCurve(curve, num_nodes)

for surf in occ.getEntities(2):
    mesh.setTransfiniteSurface(surf[1], arrangement='AlternateRight')

mesh.setRecombine(2, quad)

#gmsh.option.setNumber('Mesh.Algorithm', 7)
gmsh.option.setNumber('Mesh.ElementOrder', 2)
gmsh.option.setNumber('Mesh.MshFileVersion', 2.2)
gmsh.option.setNumber('Mesh.MedFileMinorVersion', 0)
gmsh.option.setNumber('Mesh.SaveAll', 0)
gmsh.option.setNumber('Mesh.SaveGroupsOfNodes', 1)
gmsh.option.setNumber('Mesh.SecondOrderIncomplete', 1)
gmsh.option.setNumber('Mesh.AnisoMax', 0)

# Generate mesh
mesh.generate(2)

# Save mesh
gmsh.write("ssnv112c_upd.med")

gmsh.fltk.run()

gmsh.finalize()


