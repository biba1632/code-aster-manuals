.. role:: red
.. role:: blue
.. role:: green
.. role:: gray
.. role:: python(code)
   :language: python
.. |br| raw:: html

   <br/ >

.. index:: ! sdnl100

.. _v5.02.100:

*****************************************************************************************
**[V5.02.100]** : SDNL100 - Simple pendulum with large amplitude oscillations 
*****************************************************************************************

.. contents:: Contents
   :depth: 3

.. _v5.02.100.0:

Summary
======= 

The object of this test is to calculate the motion of a heavy bar fixed at one of its ends, free elsewhere, and 
oscillating with large amplitude in a vertical plane.

Reasons: to test the dynamics of the cable element with two nodes (which is in fact a bar element) and
its operation with the operator :red:`DYNA_NON_LINE`.


.. _v5.02.100.1:

Reference problem
==================

.. _v5.02.100.1.1:

Geometry
--------

.. _fig_v5.02.100.1:

.. figure:: v5.02.100/fig1_v5.02.100.png
   :height: 300 px
   :align: center

   Geometry of the pendulum

The rigid pendulum :green:`OP` has length :math:`1`.  Its centre of gravity is at :green:`G` and it
oscillates around the point :green:`O`.

The angular position of the pendulum is located by:  :math:`\alpha = \theta - \pi`.

.. _v5.02.100.1.2:

Material properties
--------------------

* Linear mass density of the pendulum:  :math:`1.0` kg/m.
* Axial stiffness (product of Young modulus and the area of the cross-section):  :math:`1.0 \times 10^8` N.

.. _v5.02.100.1.3:

Boundary conditions and loads
-----------------------------

The pendulum is articulated at the fixed point :green:`O` . Under the action of gravity, its end :green:`P` oscillates 
on the half-circle :math:`(\Gamma)` with center :green:`O` and radius :math:`1`. There is no friction.

.. _v5.02.100.1.4:

Initial conditions
-------------------

The pendulum is released without any initial velocity from the horizontal position :green:`OP`.

.. math::

  \theta & = + \frac{\pi}{2} \\
  \dot{\theta} & = 0

.. _v5.02.100.2:

Reference solution
===================

.. _v5.02.100.2.1:

Method of calculating the reference solution
---------------------------------------------

The period :math:`T` of a pendulum (without friction) around the fixed point :green:`O`, assuming that the mass is
concentrated at the centre of gravity :green:`G` (:green:`OG`= :math:`l`) and whose maximum angular amplitude is  
:math:`\theta_0`  is given by the series [#bib1]_:

.. math:: 

   T = 2 \pi \sqrt{\frac{l}{g}}\left[1 + \sum_{n=1}^{\infty}  a_n^2 \left(\sin \frac{\theta_0}{2} \right)^{2n} \right]  

with

.. math::

   a_n = \frac{2n - 1}{2n}

 
.. _v5.02.100.2.2:

Reference results
-----------------

For :math:`l = 0.5` m,  :math:`g = 9.81` m/s :math:`^2` and :math:`\theta_0 = \pi/2`, we find:  :math:`T = 1.6744` s

.. _v5.02.100.2.3:

Uncertainty in the solution
----------------------------

In the calculation above, we have summed the terms of the series until :math:`n = 12`, the last term taken into 
account being less than :math:`10^{-5}` times the calculated sum.

.. _v5.02.100.2.4:

Bibliographical references
---------------------------

.. [#bib1] J. HAAG, "Les mouvements vibratoires", P.U.F. (1952).


.. _v5.02.100.3:

:green:`Model A`
================

.. _v5.02.100.3.1:

Characteristics of model
-------------------------

The pendulum is modelled with a two-node cable element, which is identical to a bar element of
constant cross-section.

Discretizations:

* spatial: a cable element :blue:`CABLE`.
* temporal: analysis of the motion over one complete period :math:`T`  using time steps equal to
  :math:`T/40`.

The command file can be downloaded from :download:`sdnl100a_upd.comm <v5.02.100/sdnl100a_upd.comm>`.
The file is listed below:

.. include:: v5.02.100/sdnl100a_upd.comm
  :literal:


.. _v5.02.100.3.2:

Characteristics of the mesh
----------------------------

* Number of nodes: 2
* Number of elements and types: 1 :green:`SEG2` element

A MED version of the mesh can be downloaded from :download:`sdnl100a_upd.mmed <v5.02.100/sdnl100a_upd.mmed>`.

.. _v5.02.100.4:

Results of Model A
===================

The motion of the pendulum is shown below.

.. raw:: html

    <video width="320" height="240" controls>
      <source src="../../_static/v5.02.100/sndl100a_disp.ogv" type="video/ogg">
      Your browser does not support the video tag.
    </video>

.. _v5.02.100.4.1:

Values tested
--------------

+----------------------------------------------------+------------+--------------------+
| Identification                                     | Reference  | Tolerance          |
+====================================================+============+====================+
| :green:`DX` at node :green:`P` at :math:`t=0.4186` | -1.000000  | 2.5% (relative)    |
+----------------------------------------------------+------------+--------------------+
| :green:`DZ` at node :green:`P` at :math:`t=0.4186` | -1.000000  | 0.05% (relative)   |
+----------------------------------------------------+------------+--------------------+
| :green:`DX` at node :green:`P` at :math:`t=0.8372` | -2.000000  | 0.01% (relative)   |
+----------------------------------------------------+------------+--------------------+
| :green:`DZ` at node :green:`P` at :math:`t=0.8372` |  0.000000  | 7.0E-4% (absolute) |
+----------------------------------------------------+------------+--------------------+
| :green:`DX` at node :green:`P` at :math:`t=1.2558` | -1.000000  | 7.5% (relative)    |
+----------------------------------------------------+------------+--------------------+
| :green:`DZ` at node :green:`P` at :math:`t=1.2558` | -1.000000  | 0.3% (relative)    |
+----------------------------------------------------+------------+--------------------+
| :green:`DX` at node :green:`P` at :math:`t=1.6744` |  0.000000  | 1.0E-6% (absolute) |
+----------------------------------------------------+------------+--------------------+
| :green:`DZ` at node :green:`P` at :math:`t=1.6744` |  0.000000  | 1.5E-3% (absolute) |
+----------------------------------------------------+------------+--------------------+

We also test the following simulation parameters of the results:

+----------------------------------------------------+------------+--------------------+
| Identification                                     | Reference  | Tolerance          |
+====================================================+============+====================+
| :green:`INST` for :green:`NUME_ORDRE` = 10         | 0.418600   | 0.10%              |
+----------------------------------------------------+------------+--------------------+
| :green:`ITER_GLOB` for :green:`NUME_ORDRE` = 10    | 9.000000   | 0.00%              |
+----------------------------------------------------+------------+--------------------+
| :green:`INST` for :green:`NUME_ORDRE` = 15         | 0.837200   | 0.10%              |
+----------------------------------------------------+------------+--------------------+
| :green:`ITER_GLOB` for :green:`NUME_ORDRE` = 15    | 5.000000   | 0.00%              |
+----------------------------------------------------+------------+--------------------+
| :green:`INST` for :green:`NUME_ORDRE` = 19         | 1.674400   | 0.10%              |
+----------------------------------------------------+------------+--------------------+
| :green:`ITER_GLOB` for :green:`NUME_ORDRE` = 19    | 6.000000   | 0.00%              |
+----------------------------------------------------+------------+--------------------+

.. _v5.02.100.4.2:

Remarks
--------

* Temporal integration is done by the :blue:`NEWMARK` method (trapezoidal rule),
* At each step of time, convergence is reached in less than 9 iterations.

.. _v5.02.100.5:

Summary of the results
=======================

One sees in this test case that temporal integration by the Newmark "trapezoidal rule" only
modifies the frequency slightly and does not induce parasitic damping, since at the end of one
period the pendulum returns close to the initial position.

