.. role:: red
.. role:: blue
.. role:: green
.. role:: gray
.. role:: python(code)
   :language: python
.. |br| raw:: html

   <br/>

.. index:: ! ASSE_ELEM_SSD

.. _u4.65.12:

************************************************************************
**[U4.65.12]** : Operator ``ASSE_ELEM_SSD``
************************************************************************

.. _u4.65.12.1:

Purpose
=======

This operator aims make the assembly of dynamic macro-elements easier for the user. It connects the
commands: :red:`DEFI_MODELE_GENE`, :red:`NUME_DDL_GENE` and :red:`ASSE_MATR_GENE`.

It produces a concept of the :green:`modele_gene` type, and possibly a concept of the
:green:`nume_ddl_gene` type and concepts of type :green:`matr_asse_gene_r`.

This operator is limited to the creation of generalized (real-valued) matrices. The generalized matrices
obtained can be used directly to calculate the eigen modes of a generalized model.

.. _u4.65.12.2:

Syntax
======

::

   ASSE_ELEM_SSD
   (
     ♦ RESU_ASSE_SSD = _F(
                           ♦ MODELE = model,                  [modele_gene]
                           ♦ NUME_DDL_GENE = numdof_g,        [nume_ddl_gene]
                           ♦ RIGI_GENE = stif_gen,            [matr_asse_gene_r]
                           ♦ MASS_GENE = mass_gen,            [matr_asse_gene_r]
                         ),

     ♦ SOUS_STRUC = _F(
                        ♦ NOM = name_sst,                     [Kn]
                        ♦ MACR_ELEM_DYNA = macro_el,          [macr_elem_dyna]
                        ◊ ANGL_NAUT = angln,                  [l_R]
                        ◊ TRANS = trans,                      [l_R]
                      ),

     ♦ LIAISON = _F(
                     ♦ SOUS_STRUC_1 = name_ss1,               [Kn]
                     ♦ INTERFACE_1 = name_in1,                [Kn]
                     ♦ SOUS_STRUC_2 = name_ss2,               [Kn]
                     ♦ INTERFACE_2 = name_in2,                [Kn]
                     ◊ GROUP_MA_MAIT_1 = el_grp1,             [l_gr_maille]
                     ◊ GROUP_MA_MAIT_2 = el_grp2,             [l_gr_maille]
                     ◊ OPTION = / 'CLASSIQUE',                [DEFAULT]
                                / 'REDUIT'
                   ),

     ◊ VERIF = F(
                  # See key word factor VERIF of DEFI_MODELE_GENE [U4.65.02]
                ),

     ◊ METHODE = / 'CLASSIQUE',                               [DEFAULT]
                 / 'ELIMINE',

     ◊ STOCKAGE= / 'LIGN_CIEL',                               [DEFAULT]
                 / 'PLEIN',

     ◊ INFO = / 1,                                            [DEFAULT]
              / 2,
   )
   
.. _u4.65.12.3:

Operands
========

This operator performs the following operations:

* definition of the generalized model (:red:`DEFI_MODELE_GENE`),
* classification of the degrees of freedom of the generalized model (:red:`NUME_DDL_GENE`),
* assembly of the generalized matrices (:red:`ASSE_MATR_GENE`).

It is limited to the creation of real-valued generalized matrices.

For its usage, the user may explore models "e" and "f" of the test case ``SDLS106``. These models
correspond respectively to models "b" and "d" of the same test case.


.. _u4.65.12.3.1:

Keyword :blue:`RESU_ASSE_SSD`
-----------------------------

This keyword factor defines the output concepts.

Operand :blue:`MODELE`
^^^^^^^^^^^^^^^^^^^^^^

::
   
  ♦ MODELE = model

Generalized model which contains the elements constituting the total structure (substructure assemblies).

Operand :blue:`NUME_DDL_GENE`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::
   
  ◊ NUME_DDL_GENE = numdof_g

Numbering of the degrees of freedom of the total structure.

Operand :blue:`RIGI_GENE`
^^^^^^^^^^^^^^^^^^^^^^^^^

::

   ◊ RIGI_GENE = stif_gen

Generalized stiffness matrix of the total structure.

Operand :blue:`MASS_GENE`
^^^^^^^^^^^^^^^^^^^^^^^^^^

::

   ◊ MASS_GENE = mass_gen

Generalized mass matrix of the total structure.

Keyword :blue:`SOUS_STRUC`
--------------------------

Each occurrence of this keyword makes it possible to define the characteristics of a substructure. The
description of the operands associated with this keyword is the same one as that of the keyword
:blue:`SOUS_STRUC` of operator :red:`DEFI_MODELE_GENE` :ref:`[U4.65.02] <u4.65.02>`.

.. note::

   By default, it is considered that the substructure undergoes neither a translation nor a rotation:
   :blue:`TRANS = (0., 0., 0.)` and :blue:`ANGL_NAUT = (0., 0., 0.)`.

Keyword :blue:`LIAISON`
------------------------

Each occurrence of this keyword makes it possible to define the connection between two substructures. The
description of the operands associated with this keyword is the same one as that of keyword :blue:`LIAISON`
of operator :red:`DEFI_MODELE_GENE` :ref:`[U4.65.02] <u4.65.02>`.

Among the operands of this keyword, the operand :blue:`OPTION` makes it possible to choose the type of
mode (static or interface modes) which one adds in addition to the normal modes.

::

   ◊ OPTION = / 'CLASSIQUE'

A static constraint mode is applied to each dof of the interface

::

   ◊ OPTION = / 'REDUIT'

If the :gray:`REDUCED` option is chosen, interface modes are used (previously calculated with
:red:`CREA_ELEM_SSD`, for example)

Keyword :blue:`VERIF`
---------------------

This keyword makes it possible to check the coherence of the generalized model. Description of operands
associated with this keyword is the same one as that of keyword :blue:`VERIF` of the operator
:red:`DEFI_MODELE_GENE`  :ref:`[U4.65.02] <u4.65.02>`.

Operand :blue:`METHODE`
------------------------

This operand makes it possible to choose the method of numbering the degrees of freedom of the generalized
model. Two choices are possible.

::

   ◊ METHOD = / 'CLASSIQUE'

Builds a classification of the generalized degrees of freedom taking into account the interface equations
between substructures by the method of the double Lagrange multipliers.

::

   ◊ METHOD = / 'ELIMINE'

The :gray:`ELIMINATE` method constructs a numbering of the generalized degrees of freedom by the taking
into account interface equations by the method of elimination of the constraints on the variables.

Operand :blue:`STOCKAGE`
------------------------

The :gray:`STORAGE` operand makes it possible to choose the mode of storage of assembled matrices.

::

   ◊ STORAGE = / 'LIGN_CIEL'

The :gray:`SKYLINE` storage option stores the matrices in skyline mode.

::

   ◊ STORAGE = / 'PLEIN'

The :gray:`FULL` storage mode stores all the elements of the upper triangular part of the matrix.

Operand :blue:`INFO`
--------------------

This operand makes it possible to print information on the generalized model to the 'MESSAGE' file.

Example of use
===============

This example is extracted from the ``SDLS106E`` test case.

::

   ASSE_ELEM_SSD(RESU_ASSE_SSD = _F(MODELE = CO('MODEGE'),
                                    NUME_DDL_GENE = CO('NUMEGE'),
                                    RIGI_GENE = CO('RIGGEN'),
                                    MASS_GENE = CO('MASGEN')),
                 SOUS_STRUC = (_F(NOM = 'SQUARE1',
                                  MACR_ELEM_DYNA = MACEL1,),
                               _F(NOM = 'SQUARE2',
                                  MACR_ELEM_DYNA = MACEL2,
                                  TRANS = (0., 0.5,0.),
                                  ANGL_NAUT = (-90., 0., 0.))),
                 LIAISON = (_F(SOUS_STRUC_1 = 'SQUARE1',
                               INTERFACE_1 = 'LEFT',
                               GROUP_MA_MAIT_1 = 'CALCUL',
                               OPTION = 'REDUIT',
                               SOUS_STRUC_2 = 'SQUARE2',
                               INTERFACE_2 = 'BLOCKED')),
                 VERIF = _F(STOP_ERREUR = 'OUI',
                            PRECISION = 1.E-6,
                            CRITERE = 'RELATIF'),
                 METHODE = 'ELIMINE')

