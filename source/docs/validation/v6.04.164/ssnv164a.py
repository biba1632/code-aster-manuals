import salome
import salome_notebook

import GEOM
import SMESH
import SALOMEDS

from salome.geom import geomBuilder
from salome.smesh import smeshBuilder
from salome.StdMeshers import StdMeshersBuilder

import math

ExportPATH="/home/banerjee/Salome/ssnv164/"

# Initialize
salome.salome_init()
notebook = salome_notebook.notebook

# Parameters
am = -1
bm = -0.5
cm = -0.275
cc = 0
cp = 0.275
bp = 0.5
ap = 1

nca2 = 10
nca = nca2 * 2

#---------------------
# Create geometry
#---------------------
geompy = geomBuilder.New()

# Create points
p_A = geompy.MakeVertex(am, am, nca2)
p_B = geompy.MakeVertex(bm, am, nca2)
p_C = geompy.MakeVertex(cm, am, nca2)
p_O = geompy.MakeVertex(cc, am, nca2)
p_D = geompy.MakeVertex(cp, am, nca2)
p_E = geompy.MakeVertex(bp, am, nca2)
p_F = geompy.MakeVertex(ap, am, nca2)

# Add vertices to study
geompy.addToStudy(p_A, 'A')
geompy.addToStudy(p_B, 'B')
geompy.addToStudy(p_C, 'C')
geompy.addToStudy(p_O, 'p_O')
geompy.addToStudy(p_D, 'D')
geompy.addToStudy(p_E, 'E')
geompy.addToStudy(p_F, 'F')

# Add lines
l_AB = geompy.MakeLineTwoPnt(p_A, p_B)
l_BC = geompy.MakeLineTwoPnt(p_B, p_C)
l_CO = geompy.MakeLineTwoPnt(p_C, p_O)
l_OD = geompy.MakeLineTwoPnt(p_O, p_D)
l_DE = geompy.MakeLineTwoPnt(p_D, p_E)
l_EF = geompy.MakeLineTwoPnt(p_E, p_F)

# Add lines to study
geompy.addToStudy(l_AB, 'AB')
geompy.addToStudy(l_BC, 'BC')
geompy.addToStudy(l_CO, 'CO')
geompy.addToStudy(l_OD, 'OD')
geompy.addToStudy(l_DE, 'DE')
geompy.addToStudy(l_EF, 'EF')

# Make copies and translate (make rows)
lines = (l_AB, l_BC, l_CO, l_OD, l_DE, l_EF)
lines_ID = ('AB', 'BC', 'CO', 'OD', 'DE', 'EF')
lines1 = []
lines2 = []
lines3 = []
lines4 = []
lines5 = []
lines6 = []
for line, ID in zip(lines, lines_ID):
  l_tran1 = geompy.MakeTranslation(line, 0, bp, 0)
  l_tran2 = geompy.MakeTranslation(l_tran1, 0, 0.225, 0)
  l_tran3 = geompy.MakeTranslation(l_tran2, 0, 0.275, 0)
  l_tran4 = geompy.MakeTranslation(l_tran3, 0, 0.275, 0)
  l_tran5 = geompy.MakeTranslation(l_tran4, 0, 0.225, 0)
  l_tran6 = geompy.MakeTranslation(l_tran5, 0, bp, 0)
  geompy.addToStudy(l_tran1, ID+'1') 
  geompy.addToStudy(l_tran2, ID+'2') 
  geompy.addToStudy(l_tran3, ID+'3') 
  geompy.addToStudy(l_tran4, ID+'4') 
  geompy.addToStudy(l_tran5, ID+'5') 
  geompy.addToStudy(l_tran6, ID+'6') 
  lines1.append(l_tran1)
  lines2.append(l_tran2)
  lines3.append(l_tran3)
  lines4.append(l_tran4)
  lines5.append(l_tran5)
  lines6.append(l_tran6)

#  Join the rows of lines (vertical columns)
linesv0 = []
linesv1 = []
linesv2 = []
linesv3 = []
linesv4 = []
linesv5 = []
for l0, l1, l2, l3, l4, l5, l6, ID in zip(lines, lines1, lines2, lines3, lines4, lines5, lines6, lines_ID):
  v0 = geompy.GetVertexByIndex(l0, 0)
  v1 = geompy.GetVertexByIndex(l1, 0)
  v2 = geompy.GetVertexByIndex(l2, 0)
  v3 = geompy.GetVertexByIndex(l3, 0)
  v4 = geompy.GetVertexByIndex(l4, 0)
  v5 = geompy.GetVertexByIndex(l5, 0)
  v6 = geompy.GetVertexByIndex(l6, 0)
  l_0 = geompy.MakeLineTwoPnt(v0, v1)
  l_1 = geompy.MakeLineTwoPnt(v1, v2)
  l_2 = geompy.MakeLineTwoPnt(v2, v3)
  l_3 = geompy.MakeLineTwoPnt(v3, v4)
  l_4 = geompy.MakeLineTwoPnt(v4, v5)
  l_5 = geompy.MakeLineTwoPnt(v5, v6)
  geompy.addToStudy(l_0, ID+'01') 
  geompy.addToStudy(l_1, ID+'12') 
  geompy.addToStudy(l_2, ID+'23') 
  geompy.addToStudy(l_3, ID+'34') 
  geompy.addToStudy(l_4, ID+'45') 
  geompy.addToStudy(l_5, ID+'56') 
  linesv0.append(l_0)
  linesv1.append(l_1)
  linesv2.append(l_2)
  linesv3.append(l_3)
  linesv4.append(l_4)
  linesv5.append(l_5)

  if (ID == 'EF'):
    v0 = geompy.GetVertexByIndex(l0, 1)
    v1 = geompy.GetVertexByIndex(l1, 1)
    v2 = geompy.GetVertexByIndex(l2, 1)
    v3 = geompy.GetVertexByIndex(l3, 1)
    v4 = geompy.GetVertexByIndex(l4, 1)
    v5 = geompy.GetVertexByIndex(l5, 1)
    v6 = geompy.GetVertexByIndex(l6, 1)
    l_0 = geompy.MakeLineTwoPnt(v0, v1)
    l_1 = geompy.MakeLineTwoPnt(v1, v2)
    l_2 = geompy.MakeLineTwoPnt(v2, v3)
    l_3 = geompy.MakeLineTwoPnt(v3, v4)
    l_4 = geompy.MakeLineTwoPnt(v4, v5)
    l_5 = geompy.MakeLineTwoPnt(v5, v6)
    geompy.addToStudy(l_0, ID+'101')
    geompy.addToStudy(l_1, ID+'112')
    geompy.addToStudy(l_2, ID+'123')
    geompy.addToStudy(l_3, ID+'134')
    geompy.addToStudy(l_4, ID+'145')
    geompy.addToStudy(l_5, ID+'156')
    linesv0.append(l_0)
    linesv1.append(l_1)
    linesv2.append(l_2)
    linesv3.append(l_3)
    linesv4.append(l_4)
    linesv5.append(l_5)

# Create faces
faces = []
for seg in range(len(lines_ID)):

  face1 = geompy.MakeFaceWires([lines[seg], linesv0[seg+1], lines1[seg], linesv0[seg]], 1)
  face2 = geompy.MakeFaceWires([lines1[seg], linesv1[seg+1], lines2[seg], linesv1[seg]], 1)
  face3 = geompy.MakeFaceWires([lines2[seg], linesv2[seg+1], lines3[seg], linesv2[seg]], 1)
  face4 = geompy.MakeFaceWires([lines3[seg], linesv3[seg+1], lines4[seg], linesv3[seg]], 1)
  face5 = geompy.MakeFaceWires([lines4[seg], linesv4[seg+1], lines5[seg], linesv4[seg]], 1)
  face6 = geompy.MakeFaceWires([lines5[seg], linesv5[seg+1], lines6[seg], linesv5[seg]], 1)
  geompy.addToStudy(face1, 'face1'+str(seg))
  geompy.addToStudy(face2, 'face2'+str(seg))
  geompy.addToStudy(face3, 'face3'+str(seg))
  geompy.addToStudy(face4, 'face4'+str(seg))
  geompy.addToStudy(face5, 'face5'+str(seg))
  geompy.addToStudy(face6, 'face6'+str(seg))
  faces.append(face1)
  faces.append(face2)
  faces.append(face3)
  faces.append(face4)
  faces.append(face5)
  faces.append(face6)

# Add points for extrusion (and central cable)
c0 = geompy.MakeVertex(0, 0, nca2)
c1 = geompy.MakeVertex(0, 0, nca)
c2 = geompy.MakeVertex(0, 0, 0)
geompy.addToStudy(c0, 'C0')
geompy.addToStudy(c1, 'C1')
geompy.addToStudy(c2, 'C2')

cable01 = geompy.MakeLineTwoPnt(c0, c1)
cable02 = geompy.MakeLineTwoPnt(c0, c2)
geompy.addToStudy(cable01, 'CAB01')
geompy.addToStudy(cable02, 'CAB02')

# Create extrusions
boxes_up = []
for ii, face in enumerate(faces):

  box = geompy.MakePrism(face, c0, c1)
  geompy.addToStudy(box, 'box'+str(ii))
  boxes_up.append(box)

boxes_down = []
count = 0
for ii in range(1,5):
  for jj in range(1,5):
    index = 6*ii + jj      
    box = geompy.MakePrism(faces[index], c0, c2)
    geompy.addToStudy(box, 'box_d'+str(index))
    boxes_down.append(box)

# Join the boxes while retaining partition
box_top = geompy.MakePartition(boxes_up, [], [], [], geompy.ShapeType["SOLID"], 0, [], 0)
geompy.addToStudy( box_top, 'box_top' )

box_bot = geompy.MakePartition(boxes_down, [], [], [], geompy.ShapeType["SOLID"], 0, [], 0)
geompy.addToStudy( box_bot, 'box_bot' )

box_all = geompy.MakePartition([box_top, box_bot], [], [], [], geompy.ShapeType["SOLID"], 0, [], 0)
geompy.addToStudy( box_all, 'box_all' )

# Create geometry groups for the volume
g_vol  = geompy.CreateGroup(box_all, geompy.ShapeType["SOLID"])

eps = 1.0e-4
v_max = geompy.MakeVertex(am-eps, am-eps, -eps)
v_min = geompy.MakeVertex(ap+eps, ap+eps, nca+eps)
search_box = geompy.MakeBoxTwoPnt(v_min, v_max)
all_box = geompy.GetShapesOnBox(search_box, box_all, geompy.ShapeType["SOLID"], GEOM.ST_IN)
geompy.UnionList(g_vol, all_box)
geompy.addToStudyInFather(box_all, g_vol, 'volume' )

# Create geometry groups for the four faces
g_bot = geompy.CreateGroup(box_all, geompy.ShapeType["FACE"])
g_top = geompy.CreateGroup(box_all, geompy.ShapeType["FACE"])
g_mid_top = geompy.CreateGroup(box_all, geompy.ShapeType["FACE"])
g_mid_bot = geompy.CreateGroup(box_all, geompy.ShapeType["FACE"])

v_max = geompy.MakeVertex(ap, ap, nca2-eps)
v_min = geompy.MakeVertex(am, am, nca2+eps)
search_box = geompy.MakeBoxTwoPnt(v_min, v_max)
mid_top = geompy.GetShapesOnBox(search_box, box_all, geompy.ShapeType["FACE"], GEOM.ST_ONIN)
geompy.UnionList(g_mid_top, mid_top)
geompy.addToStudyInFather(box_all, g_mid_top, 'mid_top' )

v_max = geompy.MakeVertex(bp, bp, nca2-eps)
v_min = geompy.MakeVertex(bm, bm, nca2+eps)
search_box = geompy.MakeBoxTwoPnt(v_min, v_max)
mid_bot = geompy.GetShapesOnBox(search_box, box_all, geompy.ShapeType["FACE"], GEOM.ST_ONIN)
geompy.UnionList(g_mid_bot, mid_bot)
geompy.addToStudyInFather(box_all, g_mid_bot, 'mid_bot' )

v_max = geompy.MakeVertex(ap, ap, nca-eps)
v_min = geompy.MakeVertex(am, am, nca+eps)
search_box = geompy.MakeBoxTwoPnt(v_min, v_max)
top = geompy.GetShapesOnBox(search_box, box_all, geompy.ShapeType["FACE"], GEOM.ST_ONIN)
geompy.UnionList(g_top, top)
geompy.addToStudyInFather(box_all, g_top, 'top' )

v_max = geompy.MakeVertex(bp, bp, -eps)
v_min = geompy.MakeVertex(bm, bm, +eps)
search_box = geompy.MakeBoxTwoPnt(v_min, v_max)
bot = geompy.GetShapesOnBox(search_box, box_all, geompy.ShapeType["FACE"], GEOM.ST_ONIN)
geompy.UnionList(g_bot, bot)
geompy.addToStudyInFather(box_all, g_bot, 'bot' )

# Create geometry groups for edges for meshing (along X)
g_AB = geompy.CreateGroup(box_all, geompy.ShapeType["EDGE"])
g_BC = geompy.CreateGroup(box_all, geompy.ShapeType["EDGE"])
g_CO = geompy.CreateGroup(box_all, geompy.ShapeType["EDGE"])
g_OD = geompy.CreateGroup(box_all, geompy.ShapeType["EDGE"])
g_DE = geompy.CreateGroup(box_all, geompy.ShapeType["EDGE"])
g_EF = geompy.CreateGroup(box_all, geompy.ShapeType["EDGE"])

vertex = geompy.MakeVertex(0.5*(am+bm), am, nca2)
edge = geompy.GetShapesNearPoint(box_all, vertex, geompy.ShapeType['EDGE'])
geompy.UnionList(g_AB, [edge])
geompy.addToStudyInFather(box_all, g_AB, 'AB' )

vertex = geompy.MakeVertex(0.5*(bm+cm), am, nca2)
edge = geompy.GetShapesNearPoint(box_all, vertex, geompy.ShapeType['EDGE'])
geompy.UnionList(g_BC, [edge])
geompy.addToStudyInFather(box_all, g_BC, 'BC' )

vertex = geompy.MakeVertex(0.5*cm, am, nca2)
edge = geompy.GetShapesNearPoint(box_all, vertex, geompy.ShapeType['EDGE'])
geompy.UnionList(g_CO, [edge])
geompy.addToStudyInFather(box_all, g_CO, 'CO' )

vertex = geompy.MakeVertex(0.5*cp, am, nca2)
edge = geompy.GetShapesNearPoint(box_all, vertex, geompy.ShapeType['EDGE'])
geompy.UnionList(g_OD, [edge])
geompy.addToStudyInFather(box_all, g_OD, 'OD' )

vertex = geompy.MakeVertex(0.5*(cp+bp), am, nca2)
edge = geompy.GetShapesNearPoint(box_all, vertex, geompy.ShapeType['EDGE'])
geompy.UnionList(g_DE, [edge])
geompy.addToStudyInFather(box_all, g_DE, 'DE' )

vertex = geompy.MakeVertex(0.5*(bp+ap), am, nca2)
edge = geompy.GetShapesNearPoint(box_all, vertex, geompy.ShapeType['EDGE'])
geompy.UnionList(g_EF, [edge])
geompy.addToStudyInFather(box_all, g_EF, 'EF' )

# Create geometry groups for edges for meshing (along Y)
g_ABv = geompy.CreateGroup(box_all, geompy.ShapeType["EDGE"])
g_BCv = geompy.CreateGroup(box_all, geompy.ShapeType["EDGE"])
g_COv = geompy.CreateGroup(box_all, geompy.ShapeType["EDGE"])
g_ODv = geompy.CreateGroup(box_all, geompy.ShapeType["EDGE"])
g_DEv = geompy.CreateGroup(box_all, geompy.ShapeType["EDGE"])
g_EFv = geompy.CreateGroup(box_all, geompy.ShapeType["EDGE"])

vertex = geompy.MakeVertex(am, 0.5*(am+bm), nca2)
edge = geompy.GetShapesNearPoint(box_all, vertex, geompy.ShapeType['EDGE'])
geompy.UnionList(g_ABv, [edge])
geompy.addToStudyInFather(box_all, g_ABv, 'ABv' )

vertex = geompy.MakeVertex(am, 0.5*(bm+cm), nca2)
edge = geompy.GetShapesNearPoint(box_all, vertex, geompy.ShapeType['EDGE'])
geompy.UnionList(g_BCv, [edge])
geompy.addToStudyInFather(box_all, g_BCv, 'BCv' )

vertex = geompy.MakeVertex(am, 0.5*cm, nca2)
edge = geompy.GetShapesNearPoint(box_all, vertex, geompy.ShapeType['EDGE'])
geompy.UnionList(g_COv, [edge])
geompy.addToStudyInFather(box_all, g_COv, 'COv' )

vertex = geompy.MakeVertex(am, 0.5*cp, nca2)
edge = geompy.GetShapesNearPoint(box_all, vertex, geompy.ShapeType['EDGE'])
geompy.UnionList(g_ODv, [edge])
geompy.addToStudyInFather(box_all, g_ODv, 'ODv' )

vertex = geompy.MakeVertex(am, 0.5*(cp+bp), nca2)
edge = geompy.GetShapesNearPoint(box_all, vertex, geompy.ShapeType['EDGE'])
geompy.UnionList(g_DEv, [edge])
geompy.addToStudyInFather(box_all, g_DEv, 'DEv' )

vertex = geompy.MakeVertex(am, 0.5*(bp+ap), nca2)
edge = geompy.GetShapesNearPoint(box_all, vertex, geompy.ShapeType['EDGE'])
geompy.UnionList(g_EFv, [edge])
geompy.addToStudyInFather(box_all, g_EFv, 'EFv' )

# Create geometry groups for the five cables
g_cab0 = geompy.CreateGroup(box_all, geompy.ShapeType["EDGE"])
g_cab1 = geompy.CreateGroup(box_all, geompy.ShapeType["EDGE"])
g_cab2 = geompy.CreateGroup(box_all, geompy.ShapeType["EDGE"])
g_cab3 = geompy.CreateGroup(box_all, geompy.ShapeType["EDGE"])
g_cab4 = geompy.CreateGroup(box_all, geompy.ShapeType["EDGE"])
g_cable = geompy.CreateGroup(box_all, geompy.ShapeType["EDGE"])

cables = []
vertex = geompy.MakeVertex(0, 0, 0.5*nca2)
edge1 = geompy.GetShapesNearPoint(box_all, vertex, geompy.ShapeType['EDGE'])
vertex = geompy.MakeVertex(0, 0, 1.5*nca2)
edge2 = geompy.GetShapesNearPoint(box_all, vertex, geompy.ShapeType['EDGE'])
geompy.UnionList(g_cab0, [edge1, edge2])
geompy.addToStudyInFather(box_all, g_cab0, 'cable0' )
cables.append(edge1)
cables.append(edge2)

vertex = geompy.MakeVertex(cm, cm, 0.5*nca2)
edge1 = geompy.GetShapesNearPoint(box_all, vertex, geompy.ShapeType['EDGE'])
vertex = geompy.MakeVertex(cm, cm, 1.5*nca2)
edge2 = geompy.GetShapesNearPoint(box_all, vertex, geompy.ShapeType['EDGE'])
geompy.UnionList(g_cab1, [edge1, edge2])
geompy.addToStudyInFather(box_all, g_cab1, 'cable1' )
cables.append(edge1)
cables.append(edge2)

vertex = geompy.MakeVertex(cm, cp, 0.5*nca2)
edge1 = geompy.GetShapesNearPoint(box_all, vertex, geompy.ShapeType['EDGE'])
vertex = geompy.MakeVertex(cm, cp, 1.5*nca2)
edge2 = geompy.GetShapesNearPoint(box_all, vertex, geompy.ShapeType['EDGE'])
geompy.UnionList(g_cab2, [edge1, edge2])
geompy.addToStudyInFather(box_all, g_cab2, 'cable2' )
cables.append(edge1)
cables.append(edge2)

vertex = geompy.MakeVertex(cp, cp, 0.5*nca2)
edge1 = geompy.GetShapesNearPoint(box_all, vertex, geompy.ShapeType['EDGE'])
vertex = geompy.MakeVertex(cp, cp, 1.5*nca2)
edge2 = geompy.GetShapesNearPoint(box_all, vertex, geompy.ShapeType['EDGE'])
geompy.UnionList(g_cab3, [edge1, edge2])
geompy.addToStudyInFather(box_all, g_cab3, 'cable3' )
cables.append(edge1)
cables.append(edge2)

vertex = geompy.MakeVertex(cp, cm, 0.5*nca2)
edge1 = geompy.GetShapesNearPoint(box_all, vertex, geompy.ShapeType['EDGE'])
vertex = geompy.MakeVertex(cp, cm, 1.5*nca2)
edge2 = geompy.GetShapesNearPoint(box_all, vertex, geompy.ShapeType['EDGE'])
geompy.UnionList(g_cab4, [edge1, edge2])
geompy.addToStudyInFather(box_all, g_cab4, 'cable4' )
cables.append(edge1)
cables.append(edge2)

geompy.UnionList(g_cable, cables)
geompy.addToStudyInFather(box_all, g_cable, 'cables' )

# Create geometry groups for other points
g_px = geompy.CreateGroup(box_all, geompy.ShapeType["VERTEX"])
g_py = geompy.CreateGroup(box_all, geompy.ShapeType["VERTEX"])

vertex = geompy.MakeVertex(bp, 0, 0.0)
v1 = geompy.GetShapesNearPoint(box_all, vertex, geompy.ShapeType['VERTEX'])
geompy.UnionList(g_px, [v1])
geompy.addToStudyInFather(box_all, g_px, 'px' )

vertex = geompy.MakeVertex(bm, 0, 0.0)
v1 = geompy.GetShapesNearPoint(box_all, vertex, geompy.ShapeType['VERTEX'])
geompy.UnionList(g_py, [v1])
geompy.addToStudyInFather(box_all, g_py, 'py' )

g_pbot0 = geompy.CreateGroup(box_all, geompy.ShapeType["VERTEX"])
g_pbot1 = geompy.CreateGroup(box_all, geompy.ShapeType["VERTEX"])
g_pbot2 = geompy.CreateGroup(box_all, geompy.ShapeType["VERTEX"])
g_pbot3 = geompy.CreateGroup(box_all, geompy.ShapeType["VERTEX"])
g_pbot4 = geompy.CreateGroup(box_all, geompy.ShapeType["VERTEX"])

vertex = geompy.MakeVertex(0, 0, 0.0)
v1 = geompy.GetShapesNearPoint(box_all, vertex, geompy.ShapeType['VERTEX'])
geompy.UnionList(g_pbot0, [v1])
geompy.addToStudyInFather(box_all, g_pbot0, 'pbot0' )

vertex = geompy.MakeVertex(cm, cm, 0.0)
v1 = geompy.GetShapesNearPoint(box_all, vertex, geompy.ShapeType['VERTEX'])
geompy.UnionList(g_pbot1, [v1])
geompy.addToStudyInFather(box_all, g_pbot1, 'pbot1' )

vertex = geompy.MakeVertex(cm, cp, 0.0)
v1 = geompy.GetShapesNearPoint(box_all, vertex, geompy.ShapeType['VERTEX'])
geompy.UnionList(g_pbot2, [v1])
geompy.addToStudyInFather(box_all, g_pbot2, 'pbot2' )

vertex = geompy.MakeVertex(cp, cp, 0.0)
v1 = geompy.GetShapesNearPoint(box_all, vertex, geompy.ShapeType['VERTEX'])
geompy.UnionList(g_pbot3, [v1])
geompy.addToStudyInFather(box_all, g_pbot3, 'pbot3' )

vertex = geompy.MakeVertex(cp, cm, 0.0)
v1 = geompy.GetShapesNearPoint(box_all, vertex, geompy.ShapeType['VERTEX'])
geompy.UnionList(g_pbot4, [v1])
geompy.addToStudyInFather(box_all, g_pbot4, 'pbot4' )

g_ptop0 = geompy.CreateGroup(box_all, geompy.ShapeType["VERTEX"])
g_ptop1 = geompy.CreateGroup(box_all, geompy.ShapeType["VERTEX"])
g_ptop2 = geompy.CreateGroup(box_all, geompy.ShapeType["VERTEX"])
g_ptop3 = geompy.CreateGroup(box_all, geompy.ShapeType["VERTEX"])
g_ptop4 = geompy.CreateGroup(box_all, geompy.ShapeType["VERTEX"])

vertex = geompy.MakeVertex(0, 0, nca)
v1 = geompy.GetShapesNearPoint(box_all, vertex, geompy.ShapeType['VERTEX'])
geompy.UnionList(g_ptop0, [v1])
geompy.addToStudyInFather(box_all, g_ptop0, 'ptop0' )

vertex = geompy.MakeVertex(cm, cm, nca)
v1 = geompy.GetShapesNearPoint(box_all, vertex, geompy.ShapeType['VERTEX'])
geompy.UnionList(g_ptop1, [v1])
geompy.addToStudyInFather(box_all, g_ptop1, 'ptop1' )

vertex = geompy.MakeVertex(cm, cp, nca)
v1 = geompy.GetShapesNearPoint(box_all, vertex, geompy.ShapeType['VERTEX'])
geompy.UnionList(g_ptop2, [v1])
geompy.addToStudyInFather(box_all, g_ptop2, 'ptop2' )

vertex = geompy.MakeVertex(cp, cp, nca)
v1 = geompy.GetShapesNearPoint(box_all, vertex, geompy.ShapeType['VERTEX'])
geompy.UnionList(g_ptop3, [v1])
geompy.addToStudyInFather(box_all, g_ptop3, 'ptop3' )

vertex = geompy.MakeVertex(cp, cm, nca)
v1 = geompy.GetShapesNearPoint(box_all, vertex, geompy.ShapeType['VERTEX'])
geompy.UnionList(g_ptop4, [v1])
geompy.addToStudyInFather(box_all, g_ptop4, 'ptop4' )


#---------------------
# Create mesh
#---------------------
smesh = smeshBuilder.New()
box_mesh = smesh.Mesh(box_all)
smesh.SetName(box_mesh, 'mesh')

# Set up algorithms for the main mesh
Regular_1D = box_mesh.Segment()
Number_of_Segments_1 = Regular_1D.NumberOfSegments(1)
Quadrangle_2D = box_mesh.Quadrangle(algo=smeshBuilder.QUADRANGLE)
Quadrangle_Parameters_1 = Quadrangle_2D.QuadrangleParameters(smeshBuilder.QUAD_STANDARD,-1,[],[])
Hexa_3D = box_mesh.Hexahedron(algo=smeshBuilder.Hexa)

# Set up submeshing parameters 
submesh_1D_1 = box_mesh.Segment(geom = g_AB)
num_seg_1D_1 = submesh_1D_1.NumberOfSegments(2)
propagate_1 = submesh_1D_1.Propagation()

submesh_1D_2 = box_mesh.Segment(geom = g_BC)
status = box_mesh.AddHypothesis(num_seg_1D_1, g_BC)
status = box_mesh.AddHypothesis(propagate_1, g_BC)

submesh_1D_3 = box_mesh.Segment(geom = g_CO)
status = box_mesh.AddHypothesis(num_seg_1D_1, g_CO)
status = box_mesh.AddHypothesis(propagate_1, g_CO)

submesh_1D_4 = box_mesh.Segment(geom = g_OD)
status = box_mesh.AddHypothesis(num_seg_1D_1, g_OD)
status = box_mesh.AddHypothesis(propagate_1, g_OD)

submesh_1D_5 = box_mesh.Segment(geom = g_DE)
status = box_mesh.AddHypothesis(num_seg_1D_1, g_DE)
status = box_mesh.AddHypothesis(propagate_1, g_DE)

submesh_1D_6 = box_mesh.Segment(geom = g_EF)
status = box_mesh.AddHypothesis(num_seg_1D_1, g_EF)
status = box_mesh.AddHypothesis(propagate_1, g_EF)

submesh_1D_1v = box_mesh.Segment(geom = g_ABv)
status = box_mesh.AddHypothesis(num_seg_1D_1, g_ABv)
status = box_mesh.AddHypothesis(propagate_1, g_ABv)

submesh_1D_2v = box_mesh.Segment(geom = g_BCv)
status = box_mesh.AddHypothesis(num_seg_1D_1, g_BCv)
status = box_mesh.AddHypothesis(propagate_1, g_BCv)

submesh_1D_3v = box_mesh.Segment(geom = g_COv)
status = box_mesh.AddHypothesis(num_seg_1D_1, g_COv)
status = box_mesh.AddHypothesis(propagate_1, g_COv)

submesh_1D_4v = box_mesh.Segment(geom = g_ODv)
status = box_mesh.AddHypothesis(num_seg_1D_1, g_ODv)
status = box_mesh.AddHypothesis(propagate_1, g_ODv)

submesh_1D_5v = box_mesh.Segment(geom = g_DEv)
status = box_mesh.AddHypothesis(num_seg_1D_1, g_DEv)
status = box_mesh.AddHypothesis(propagate_1, g_DEv)

submesh_1D_6v = box_mesh.Segment(geom = g_EFv)
status = box_mesh.AddHypothesis(num_seg_1D_1, g_EFv)
status = box_mesh.AddHypothesis(propagate_1, g_EFv)

submesh_1D_2 = box_mesh.Segment(geom = g_cab0)
num_seg_1D_2 = submesh_1D_2.NumberOfSegments(10)
propagate_2 = submesh_1D_2.Propagation()

# Compute the mesh
isDone = box_mesh.Compute()

# Map the geometry groups to the mesh
box_all_1 = box_mesh.GroupOnGeom(g_vol, 'vol_all', SMESH.VOLUME)

bot_1 = box_mesh.GroupOnGeom(g_bot, 'bot_face', SMESH.FACE)
top_1 = box_mesh.GroupOnGeom(g_top, 'top_face', SMESH.FACE)
bot_mid_1 = box_mesh.GroupOnGeom(g_mid_bot, 'mid_bot_face', SMESH.FACE)
top_mid_1 = box_mesh.GroupOnGeom(g_mid_top, 'mid_top_face', SMESH.FACE)

cab0_1 = box_mesh.GroupOnGeom(g_cab0, 'cable0', SMESH.EDGE)
cab1_1 = box_mesh.GroupOnGeom(g_cab1, 'cable1', SMESH.EDGE)
cab2_1 = box_mesh.GroupOnGeom(g_cab2, 'cable2', SMESH.EDGE)
cab3_1 = box_mesh.GroupOnGeom(g_cab3, 'cable3', SMESH.EDGE)
cab4_1 = box_mesh.GroupOnGeom(g_cab4, 'cable4', SMESH.EDGE)
cable_1 = box_mesh.GroupOnGeom(g_cable, 'cable', SMESH.EDGE)

px_1 = box_mesh.GroupOnGeom(g_px, 'px', SMESH.NODE)
py_1 = box_mesh.GroupOnGeom(g_py, 'py', SMESH.NODE)

pbot0_1 = box_mesh.GroupOnGeom(g_pbot0, 'pbot0', SMESH.NODE)
pbot1_1 = box_mesh.GroupOnGeom(g_pbot1, 'pbot1', SMESH.NODE)
pbot2_1 = box_mesh.GroupOnGeom(g_pbot2, 'pbot2', SMESH.NODE)
pbot3_1 = box_mesh.GroupOnGeom(g_pbot3, 'pbot3', SMESH.NODE)
pbot4_1 = box_mesh.GroupOnGeom(g_pbot4, 'pbot4', SMESH.NODE)

ptop0_1 = box_mesh.GroupOnGeom(g_ptop0, 'ptop0', SMESH.NODE)
ptop1_1 = box_mesh.GroupOnGeom(g_ptop1, 'ptop1', SMESH.NODE)
ptop2_1 = box_mesh.GroupOnGeom(g_ptop2, 'ptop2', SMESH.NODE)
ptop3_1 = box_mesh.GroupOnGeom(g_ptop3, 'ptop3', SMESH.NODE)
ptop4_1 = box_mesh.GroupOnGeom(g_ptop4, 'ptop4', SMESH.NODE)

# Create a node group for the nodes of the edge where displacement BCs
# are applied
center_nodes = box_mesh.CreateDimGroup([cab0_1], SMESH.NODE, 
                                       'center_nodes', SMESH.ALL_NODES, 0)

# Create groups for verification tests (cable 1)
x_cable1 = -2.75e-01
y_cable1 =  2.75e-01
N1 = box_mesh.FindNodeClosestTo(x_cable1, y_cable1, 0.0)
N6 = box_mesh.FindNodeClosestTo(x_cable1, y_cable1, 5.00000007450581E+00)
N11 = box_mesh.FindNodeClosestTo(x_cable1, y_cable1, 1.00000001490116E+01)
N16 = box_mesh.FindNodeClosestTo(x_cable1, y_cable1, 1.50000002235174E+01)
N101 = box_mesh.FindNodeClosestTo(x_cable1, y_cable1, 2.00000000000000E+01)

M5655 = box_mesh.FindElementsByPoint(x_cable1, y_cable1, 0.001, SMESH.EDGE) 
M5660 = box_mesh.FindElementsByPoint(x_cable1, y_cable1, 5.00100007450581E+00, SMESH.EDGE)
M5664 = box_mesh.FindElementsByPoint(x_cable1, y_cable1, 1.00100001490116E+01, SMESH.EDGE)
M5670 = box_mesh.FindElementsByPoint(x_cable1, y_cable1, 1.50100002235174E+01, SMESH.EDGE)
M5674 = box_mesh.FindElementsByPoint(x_cable1, y_cable1, 1.99900000000000E+01, SMESH.EDGE)

n_1_1 = box_mesh.CreateEmptyGroup(SMESH.NODE, 'N1')
n_1_1.Add([N1])
n_2_1 = box_mesh.CreateEmptyGroup(SMESH.NODE, 'N6')
n_2_1.Add([N6])
n_3_1 = box_mesh.CreateEmptyGroup(SMESH.NODE, 'N11')
n_3_1.Add([N11])
n_4_1 = box_mesh.CreateEmptyGroup(SMESH.NODE, 'N16')
n_4_1.Add([N16])
n_5_1 = box_mesh.CreateEmptyGroup(SMESH.NODE, 'N101')
n_5_1.Add([N101])

e_1_1 = box_mesh.CreateEmptyGroup(SMESH.EDGE, 'M5655')
e_1_1.Add(M5655)
e_2_1 = box_mesh.CreateEmptyGroup(SMESH.EDGE, 'M5660')
e_2_1.Add(M5660)
e_3_1 = box_mesh.CreateEmptyGroup(SMESH.EDGE, 'M5664')
e_3_1.Add(M5664)
e_4_1 = box_mesh.CreateEmptyGroup(SMESH.EDGE, 'M5670')
e_4_1.Add(M5670)
e_5_1 = box_mesh.CreateEmptyGroup(SMESH.EDGE, 'M5674')
e_5_1.Add(M5674)

# Create groups for verification tests (cable 3)
x_cable3 = 2.75e-01
y_cable3 = 2.75e-01
N41 = box_mesh.FindNodeClosestTo(x_cable3, y_cable3, 0.0)
N46 = box_mesh.FindNodeClosestTo(x_cable3, y_cable3, 5.00000007450581E+00)
N51 = box_mesh.FindNodeClosestTo(x_cable3, y_cable3, 1.00000001490116E+01)
N56 = box_mesh.FindNodeClosestTo(x_cable3, y_cable3, 1.50000002235174E+01)
N103 = box_mesh.FindNodeClosestTo(x_cable3, y_cable3, 2.00000000000000E+01)

M5695 = box_mesh.FindElementsByPoint(x_cable3, y_cable3, 0.001, SMESH.EDGE) 
M5700 = box_mesh.FindElementsByPoint(x_cable3, y_cable3, 5.00100007450581E+00, SMESH.EDGE)
M5705 = box_mesh.FindElementsByPoint(x_cable3, y_cable3, 1.00100001490116E+01, SMESH.EDGE)
M5710 = box_mesh.FindElementsByPoint(x_cable3, y_cable3, 1.50100002235174E+01, SMESH.EDGE)
M5714 = box_mesh.FindElementsByPoint(x_cable3, y_cable3, 1.99900000000000E+01, SMESH.EDGE)

n_1_3 = box_mesh.CreateEmptyGroup(SMESH.NODE, 'N41')
n_1_3.Add([N41])
n_2_3 = box_mesh.CreateEmptyGroup(SMESH.NODE, 'N46')
n_2_3.Add([N46])
n_3_3 = box_mesh.CreateEmptyGroup(SMESH.NODE, 'N51')
n_3_3.Add([N51])
n_4_3 = box_mesh.CreateEmptyGroup(SMESH.NODE, 'N56')
n_4_3.Add([N56])
n_5_3 = box_mesh.CreateEmptyGroup(SMESH.NODE, 'N103')
n_5_3.Add([N103])

e_1_3 = box_mesh.CreateEmptyGroup(SMESH.EDGE, 'M5695')
e_1_3.Add(M5695)
e_2_3 = box_mesh.CreateEmptyGroup(SMESH.EDGE, 'M5700')
e_2_3.Add(M5700)
e_3_3 = box_mesh.CreateEmptyGroup(SMESH.EDGE, 'M5705')
e_3_3.Add(M5705)
e_4_3 = box_mesh.CreateEmptyGroup(SMESH.EDGE, 'M5710')
e_4_3.Add(M5710)
e_5_3 = box_mesh.CreateEmptyGroup(SMESH.EDGE, 'M5714')
e_5_3.Add(M5714)

# Create groups for verification tests (cable 0)
x_cable0 = 0.0
y_cable0 = 0.0

N81 = box_mesh.FindNodeClosestTo(x_cable0, y_cable0, 0.0)
N86 = box_mesh.FindNodeClosestTo(x_cable0, y_cable0, 5.00000007450581E+00)
N91 = box_mesh.FindNodeClosestTo(x_cable0, y_cable0, 1.00000001490116E+01)
N96 = box_mesh.FindNodeClosestTo(x_cable0, y_cable0, 1.50000002235174E+01)
N105 = box_mesh.FindNodeClosestTo(x_cable0, y_cable0, 2.00000000000000E+01)

M5735 = box_mesh.FindElementsByPoint(x_cable0, y_cable0, 0.001, SMESH.EDGE) 
M5740 = box_mesh.FindElementsByPoint(x_cable0, y_cable0, 5.00100007450581E+00, SMESH.EDGE)
M5745 = box_mesh.FindElementsByPoint(x_cable0, y_cable0, 1.00100001490116E+01, SMESH.EDGE)
M5750 = box_mesh.FindElementsByPoint(x_cable0, y_cable0, 1.50100002235174E+01, SMESH.EDGE)
M5754 = box_mesh.FindElementsByPoint(x_cable0, y_cable0, 1.99900000000000E+01, SMESH.EDGE)

n_1_0 = box_mesh.CreateEmptyGroup(SMESH.NODE, 'N81')
n_1_0.Add([N81])
n_2_0 = box_mesh.CreateEmptyGroup(SMESH.NODE, 'N86')
n_2_0.Add([N86])
n_3_0 = box_mesh.CreateEmptyGroup(SMESH.NODE, 'N91')
n_3_0.Add([N91])
n_4_0 = box_mesh.CreateEmptyGroup(SMESH.NODE, 'N96')
n_4_0.Add([N96])
n_5_0 = box_mesh.CreateEmptyGroup(SMESH.NODE, 'N105')
n_5_0.Add([N105])

e_1_0 = box_mesh.CreateEmptyGroup(SMESH.EDGE, 'M5735')
e_1_0.Add(M5735)
e_2_0 = box_mesh.CreateEmptyGroup(SMESH.EDGE, 'M5740')
e_2_0.Add(M5740)
e_3_0 = box_mesh.CreateEmptyGroup(SMESH.EDGE, 'M5745')
e_3_0.Add(M5745)
e_4_0 = box_mesh.CreateEmptyGroup(SMESH.EDGE, 'M5750')
e_4_0.Add(M5750)
e_5_0 = box_mesh.CreateEmptyGroup(SMESH.EDGE, 'M5754')
e_5_0.Add(M5754)

# Export mesh
box_mesh.ExportMED( r''+ExportPATH+'ssnv164a_new.mmed'+'')

# Update object tree
if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()
