.. role:: red
.. role:: blue
.. role:: green
.. role:: gray
.. role:: python(code)
   :language: python
.. | br|  raw:: html

   <br
        / >

.. index:: ! CREA_MAILLAGE

.. _u4.23.02:

************************************************************************
**[U4.23.02]** : Operator ``CREA_MAILLAGE`` (:gray:`CREATE_MESH`)
************************************************************************

.. contents:: Contents
   :depth: 3

.. _u4.23.02.1:

Purpose
=======

Create a :green:`maillage` (:gray:`mesh`) data structure from another mesh. The new mesh is created from 
an existing mesh by duplicating, destroying, transforming or exploding elements or by copying a mesh 
(e.g., :red:`GEOM_FIBRE`).

Produces a data structure of type :green:`maillage`.

.. _u4.23.02.2:

Syntax
=======

::

  mesh_2 [maillage] = CREA_MAILLAGE
  (
    ◊ MAILLAGE = mesh_1,                                             [maillage]
    ♦ | CREA_MAILLE = _F(
                         ♦ NOM = name,                               [K8]
                         ♦ | GROUP_MA = l_el_grp,                    [l_group_ma]
                           | TOUT = 'OUI',
                         ♦ PREF_MAILLE = pref_el,                    [kn]
                         ◊ PREF_NUME = ind ,                         [I]
                        ),
      | MODI_MAILLAGE = _F(
                           ♦ | TOUT = 'OUI',
                             | GROUP_MA  = l_el_grp,                 [l_group_ma]
                           ♦ / OPTION = / 'TRIA6_7',
                                        / 'QUAD8_9' ,
                                        / 'SEG3_4' ,
                                        / 'QUAD_TRIA3',
                               ◊ PREF_NOEUD = / 'NS',                [DEFAULT]
                                              / pref_nd,             [Kn]
                               ◊ PREF_NUME = / ind,                  [I]
                                             / 1,                    [DEFAULT]

                             / OPTION = 'QUAD_TRIA3',
                               ◊ PREF_MAILLE = / 'MS',               [DEFAULT]
                                               / pref_el,            [Kn]
                               ◊ PREF_NUME = / ind,                  [I]
                                             / 1,                    [DEFAULT]
                          ),
      | REPERE = _F(
                    ♦ TABLE = tab,                                   [tabl_cara_geom]
                    ◊ NOM_ORIG = / 'CGD',                            [DEFAULT]
                                 / 'TORSION',
                    ◊ NOM_ROTA = / 'INERTIE',                        [DEFAULT]
                    ◊ GROUP_MA = el_grp,                             [group_ma]
                   ),

      | CREA_POI1 = _F(
                       ♦ | TOUT = 'OUI',
                         | GROUP_MA = l_el_grp,                      [l_group_ma]
                         | GROUP_NO = l_no_grp,                      [l_group_no]
                       ♦ NOM_GROUP_MA = name_el ,                    [group_ma]
                      ),

      | LINE_QUAD = _F(
                       ♦ | TOUT = 'OUI',
                         | GROUP_MA = l_el_grp,                      [l_group_ma]
                       ◊ PREF_NOEUD = / 'NS',                        [DEFAULT]
                                      / pref_nd ,                    [kn]
                       ◊ PREF_NUME = / ind,                          [I]
                                     / 1,                            [DEFAULT]
                      ),

      | PENTA15_18 = _F(
                        ♦ | TOUT = 'OUI',
                          | GROUP_MA = l_el_grp,                     [l_group_ma]
                        ◊ PREF_NOEUD = / 'NS',                       [DEFAULT]
                                       / pref_nd,                    [kn]
                        ◊ PREF_NUME = / ind,                         [I]
                                      / 1,                           [DEFAULT]
                       ),

      | HEXA20_27 = _F( 
                       ♦ | TOUT = 'OUI',
                         | GROUP_MA = l_el_grp,                      [l_group_ma]
                       ◊ PREF_NOEUD = / 'NS',                        [DEFAULT]
                                      / pref_nd,                     [kn]
                       ◊ PREF_NUME = / ind,                          [I]
                                     / 1,                            [DEFAULT]
                      ),

      | TETRA4_8 = _F(
                      ♦ | TOUT = 'OUI',
                        | GROUP_MA = l_el_grp,                       [l_group_ma]
                      ◊ PREF_NOEUD = / 'NS',                         [DEFAULT]
                                     / pref_nd,                      [kn]
                      ◊ PREF_NUME = / ind,                           [I]
                                    / 1,                             [DEFAULT]
                     ),

      | QUAD_LINE = _F(
                       ♦ | TOUT = 'OUI',
                         | GROUP_MA = l_el_grp,                      [l_group_ma]
                      ),

      | COQU_VOLU = _F(
                       ♦ NOM = name,                                 [TXM]
                       ♦ GROUP_MA = el_grp,                          [group_ma]
                       ♦ EPAIS = thick,                              [R8]
                       ◊ PREF_MAILLE = / 'MS',                       [DEFAULT]
                                       / pre_ma,                     [kn]
                       ◊ PREF_NOEUD = / 'NS',                        [DEFAULT]
                                      / pref_nd,                     [kn]
                       ◊ PREF_NUME = / ind,                          [I]
                                     / 1,                            [DEFAULT]
                       ♦ / PLAN = / 'SUP',                           [TXM]
                                  / 'INF',
                                  / 'MOY',
                          ♦ TRANSLATION = / 'SUP',                   [TXM]
                                          / 'INF',                   [DEFAULT]
                      ),

      | CREA_FISS = _F(
                       ♦ NOM = name_el_grp,                          [TXM]
                       ♦ GROUP_NO_1 = no_grp_1,                      [group_no]
                       ♦ GROUP_NO_2 = no_grp_2,                      [group_no]
                       ♦ PREF_MAILLE = pre_el,                       [kn]
                       ◊ PREF_NUME = / ind,                          [I]
                                     / 1,                            [DEFAULT]
                      ),

      | RESTREINT = _F(
                       ♦ GROUP_MA = l_el_grp,                        [l_group_ma]
                       ◊ GROUP_NO = l_no_grp,                        [l_group_no]
                       ◊ TOUT_GROUP_MA = / 'NON',                    [DEFAULT]
                                         / 'OUI',
                       ◊ TOUT_GROUP_NO = / 'NON',                    [DEFAULT]
                                         / 'OUI'
                      ),

      | ECLA_PG = _F(...) # used by [U4.44.14]

      | GEOM_FIBRE = fiber,                                          [gfiber]

    ♦ | DECOUPE_LAC = _F(
                         ♦ GROUP_MA_ESCL = l_el_grp,                 [l_group_ma]
                         ◊ DECOUPE_HEXA = / 'PYRA',                  [DEFAULT]
                                          / 'HEXA'),

    ◊ INFO = / 1,                                                    [DEFAULT]
             / 2,

    ◊ TITRE  = title,                                                [TXM]
  )

.. _u4.23.02.3:

Risk of producing a nonconforming mesh
=======================================

A certain number of functionalities of the command :red:`CREA_MAILLAGE` can lead to a nonconforming mesh. 
For this reason, the user must be particularly vigilant when using :red:`CREA_MAILLAGE` to transform 
meshes.

A mesh is nonconforming when the shape functions of 2 adjacent elements do not have the same trace on 
their common border.

For example :

* 2 pentahedra assembled to form a hexahedron and placed on another hexahedron (1 quadrilateral facing 
  2 triangles).
* 1 :green:`QUAD8` sharing an edge with 1 :green:`QUAD4` or 1 :green:`TRIA3`
* 1 :green:`TRIA6` sharing an edge with 2 :green:`TRIA3`

Nonconforming meshes generally lead to incorrect results (at least locally).

Among the possibilities of :red:`CREA_MAILLAGE`, several situations are potentially dangerous:

* Use of one of the keywords :blue:`QUAD_TRIA3` [:numref:`u4.23.02.4.4.1`], :blue:`LINE_QUAD` 
  [:numref:`u4.23.02.4.5`], :blue:`QUAD_LINE` [:numref:`u4.23.02.4.9`], :blue:`HEXA20_27` 
  [:numref:`u4.23.02.4.7`], :blue:`PENTA15_18` [:numref:`u4.23.02.4.6`] with keyword :blue:`GROUP_MA`.

  If one partially transforms a linear mesh into a quadratic mesh, the mesh will be nonconforming on 
  the border between the linear elements and the quadratic elements.

  During the use of the keyword :blue:`GROUP_MA`, it is necessary to take care to provide all the elements
  involved in the transformation, in particular the surface elements, failing which, a
  :green:`HEXA27` could for example be lined with :green:`QUAD8` surface elements.

* Use of the keyword :blue:`HEXA20_27` [:numref:`u4.23.02.4.7`] (or :blue:`PENTA15_18` 
  [:numref:`u4.23.02.4.6`], :blue:`TETRA4_8` [:numref:`u4.23.02.4.8`]) if it exists in the volume element
  mesh with quadrilateral faces of type different from the elements that we modify. For example, if there 
  are pentahedrons or pyramids when modifying hexahedrons. The risk is that, for example, a quadrilateral 
  face of :green:`HEXA27` (9 nodes) is attached to a quadrilateral face with 8 nodes of an adjacent 
  :green:`PENTA15`.

* Use of the keyword :blue:`QUAD_TRIA3` in a mesh of :green:`TRIA6` elements. In this case, the 
  quadrilaterals transformed into :green:`TRIA3` will be incompatible with the :green:`TRIA6`.

.. _u4.23.02.4:

Operands
=========

.. _u4.23.02.4.1:

Operand :blue:`MAILLAGE` (:gray:`MESH`)
---------------------------------------

::

    ♦ MAILLAGE  =  mesh_ini

:green:`mesh_ini` is the name of the initial mesh that one wants to copy before "enriching it" with new elements or 
nodes, or to "impoverish" it.

.. note::

   The keyword :blue:`MAILLAGE` is required except when using the keywords :blue:`ECLA_PG` and :blue:`GEOM_FIBRE`.

.. _u4.23.02.4.2:

Keyword :blue:`CREA_MAILLE` (:gray:`CREATE_ELEMENT`)
-----------------------------------------------------

::

    ◊ CREA_MAILLE

An occurrence of this factor keyword is used to define a new group of elements made of new elements which rely on 
existing nodes. 

To duplicate several element groups, we will have to repeat the factor keyword :blue:`CREA_MAILLE`.

Contrary to command :red:`DEFI_GROUP` :ref:`[U4.22.01] <u4.22.01>` for which the concept :green:`maillage` 
always retains the same number of elements and nodes, here the number of elements of the new mesh is increased 
(the number of nodes remains identical because the new elements are based on already existing nodes).

This can facilitate the creation of new loci to be able to apply different models on the same element group.

.. _u4.23.02.4.2.1:

Operand :blue:`NOM` (:gray:`NAME`)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

    ♦ NOM  =  name

The name of the new group of elements which will be created.

.. _u4.23.02.4.2.2:

Operands :blue:`GROUP_MA / TOUT` (:gray:`ELEMENT_GROUP/ALL`)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

    ♦ | GROUP_MA  =  l_el_grp,
      | TOUT =  'OUI',

All the elements provided by the user with these two keywords will be duplicated and the new elements will be collected 
in an element group bearing the name stipulated by the keyword :blue:`NOM`. If the set of elements to be duplicated 
contains duplicate elements, they are eliminated.

.. _u4.23.02.4.2.3:

Operands :blue:`PREF_MAILLE/PREF_NUME` (:gray:`ELEMENT/NUMBER_PREFIX`)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

    ♦ PREF_MAILLE = pre_el

The value of this keyword is used to define the prefix of the new elements. We get the name of the new element by 
adding in front of its old name, the text specified under the keyword :blue:`PREF_MAILLE`. If this new name is 
longer than eight characters, ``CODE_ASTER`` stops with a fatal error message.

::

    ◊ PREF_NUME  = / index

If an integer :green:`index` is given under the keyword :blue:`PREF_NUME`, the numbering of the new elements is
constructed by concatenating the uppercase text given under the keyword :blue:`PREF_MAILLE` and an integer
obtained by incrementing :green:`index` by 1 each time new elements are created.

.. warning::

   The user must be careful in choosing the prefix to prevent the the creation of new elements that have the 
   same name as old elements. This collision of names is detected by the command and will stop ``Code_Aster``.

.. _u4.23.02.4.3:

Keyword :blue:`CREA_POI1` (:gray:`CREATE_POINT_ELEMENT`)
--------------------------------------------------------

::

  ◊ CREA_POI1

An occurrence of this factor keyword is used to define elements of the type :green:`"POI1"` (element with a
single node) from nodes or element nodes.

.. _u4.23.02.4.3.1:

Operands :blue:`TOUT/GROUP_MA/GROUP_NO` (:gray:`ALL/ELEM_GROUP/NODE_GROUP`)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

    ♦ | TOUT = 'OUI',
      | GROUP_MA  = l_el_grp,
      | GROUP_NO  = lno_grp,

All nodes that belong to entities stipulated by the user with these five keywords generate an element of the 
:green:`POI1` type. The element created will have the same name as the node which supports it.

.. _u4.23.02.4.3.2:

Operand :blue:`NOM_GROUP_MA` (:gray:`ELEM_GROUP_NAME`)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

    ♦ NOM_GROUP_MA = gr_name

All the :green:`POI1` elements thus created can be grouped together in the same group of elements named 
:green:`gr_name`.

.. _u4.23.02.4.3.3:

How to create several :blue:`POI` elements at each node?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:green:`POI1` elements created by keyword :blue:`CREA_POI1` have the same names as the nodes from which they were created. 
It is therefore not possible to create several :green:`POI1` elements at the same node by using several occurrences of 
the keyword :blue:`CREA_POI1`.

A possible workaround is to combine the use of :red:`CREA_MAILLAGE`/:blue:`CREA_POI1` with 
:red:`CREA_MAILLAGE`/:blue:`CREA_MAILLE`. 

For example, to create two :green:`POI1` elements at each node of the group :green:`'nd_grp1'`, we can do:

::

  mesh_2 = CREA_MAILLAGE(MAILLAGE = mesh_1,
                         CREA_POI1 = _F(NOM_GROUP_MA = 'el_grp1', 
                                        GROUP_NO = 'nd_grp1'))

  mesh_3 = CREA_MAILLAGE(MAILLAGE = mesh_2,
                         CREA_MAILLE = _F(NOM_GROUP_MA = 'el_grp2', 
                                          GROUP_MA = 'el_grp1', 
                                          PREF_MAIL = 'S'))

.. _u4.23.02.4.4:

Keyword :blue:`MODI_MAILLE` (:gray:`MODIFY_ELEMENT`)
-----------------------------------------------------

::

    ◊ MODI_MAILLE

An occurrence of this factor keyword is used to transform a set of elements.

.. _u4.23.02.4.4.1:

Operand :blue:`OPTION`
^^^^^^^^^^^^^^^^^^^^^^

::

    ♦ OPTION = / 'SEG3_4'
               / 'TRIA6_7'
               / 'QUAD8_9'
               / 'QUAD_TRIA3'

This keyword indicates the transformation to perform:

1) transformation of segments with three nodes into segments with four nodes (usable, for example, for :green:`"TUYAU"`
   (:gray:`PIPE`) models :ref:`[U3.11.06] <u3.11.06>`,
2) transformation of triangles with six nodes into triangles with seven nodes,
3) transformation of eight-node quadrilaterals into nine-node quadrilaterals,
4) transformation of the quadrilaterals into triangles with 3 nodes:

   * transformation of elements of the :green:`QUAD4` type into two elements of the :green:`TRIA3` type
   * transformation of elements of the :green:`QUAD8` type into six elements of the :green:`TRIA3` type
   * transformation of elements of the :green:`QUAD9` type into eight elements of the :green:`TRIA3` type

.. _u4.23.02.4.4.2:

Operands :blue:`PREF_NOEUD/PREF_MAILLE/PREF_NUME` (:gray:`PREFIX_NODE/ELEMENT/NUMBER`)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

    ◊ PREF_NOEUD = / pref_nd,
                   / 'NS',

The value of this keyword is used to define the prefix of the new nodes. We get the name of the new node by adding, in 
front of its old name, the text specified under the keyword :blue:`PREF_NOEUD`. If this new name is longer than eight 
characters, ``Code_Aster`` stops with a fatal error message.

::

    ◊ PREF_MAILLE = pref_el

The value of this keyword is used to define the prefix of the new elements. We get the name of the new mesh by adding,
in front of its old name, the text specified under the keyword :blue:`PREF_MAILLE`. If this new name is longer than eight 
characters, ``Code_Aster`` stops with a fatal error message.

::

    ◊ PREF_NUME = / index,
                  / 1,

If an integer :green:`index` is given under the keyword :blue:`PREF_NUME`, the numbering of the new nodes (new elements) 
is constructed by concatenating the uppercase text given under the keyword :blue:`PREF_NOEUD` (:blue:`PREF_MAILLE`) and 
an integer obtained by incrementing :green:`index` by 1 at each new node (new element) creation.

.. warning::

   The user must be careful in choosing the prefix to prevent new nodes (new elements) from having the same name as 
   old nodes (old elements). This collision of names is detected by the command and stops ``Code_Aster``.

   An automatic procedure for cutting quadrilaterals elements into triangles can generate a "polarization" of the mesh: 
   from a given :green:`QUAD` element, all the diagonals are oriented in the same direction.

.. warning::

   The use of the option :blue:`"QUAD_TRIA3"` can lead to a non-compliant mesh. See :numref:`u4.23.02.3`.

.. _u4.23.02.4.5:

Keyword :blue:`LINE_QUAD` (:gray:`LINEAR_TO_QUADRATIC`)
--------------------------------------------------------

::

    ◊ LINE_QUAD

This feature allows you to create a quadratic mesh from a linear mesh. One can apply it only to part of the mesh 
(keyword :blue:`GROUP_MA`), but that is not advised. See :numref:`u4.23.02.3`.

The element groups are preserved, as are the groups of nodes (without change).

As during the refinement of a mesh, the newly created created are not introduced into the existing node groups.
If a group of nodes corresponds to an edge, after :blue:`LINE_QUAD`, this group does not contain the middle nodes of 
edges. To obtain a complete :blue:`GROUP_NO`, one can use the command :red:`DEFI_GROUP`/:blue:`OPTION = "APPUI"`.

.. _u4.23.02.4.5.1:

Operands :blue:`GROUP_MA/TOUT` (:gray:`ELEM_GROUP/ALL`)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

    ♦ | GROUP_MA = l_el_grp,
      | TOUT = 'OUI',

All the elements stipulated by the user with these two keywords will be transformed into quadratic elements.

.. warning::

   The use of keyword :blue:`GROUP_MA` is not advised. See :numref:`u4.23.02.3`.

.. _u4.23.02.4.5.2:

Operands :blue:`PREF_NOEUD/PREF_NUME`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Same as for :blue:`MODI_MAILLE` (:numref:`u4.23.02.4.4.2`).

.. _u4.23.02.4.6:

Keyword :blue:`PENTA15_18` (:gray:`PENTAHEDRON15_to_PENTA18`)
--------------------------------------------------------------

This factor keyword functions like the factor keyword :blue:`LINE_QUAD` (same syntax). It serves to transform 
:green:`PENTA15` into :green:`PENTA18` by adding nodes to the midpoints of the quadrilateral faces.

.. warning::

   The use of this keyword is not advised if the mesh contains other types of volume elements (:green:`HEXA` and 
   :green:`PYRAM`). See :numref:`u4.23.02.3`.

.. _u4.23.02.4.7:

Keyword :blue:`HEXA20_27` (:gray:`HEXAHEDRON20_To_HEXA27`)
----------------------------------------------------------

This factor keyword functions like the factor keyword :blue:`PENTA15_18` with hexahedra. It serves to transform 
:green:`HEXA20` into :green:`HEXA27` by adding nodes to the midpoints of the faces and to the center of each hexahedron.

.. warning::

   The use of this keyword is not advised if the mesh contains other types of volume elements (:green:`PENTA` and 
   :green:`PYRAM`). See :numref:`u4.23.02.3`.

.. _u4.23.02.4.8:

Keyword :blue:`TETRA4_8` (:gray:`TETRAHEDRON4_TO_TETRA8`)
---------------------------------------------------------

This factor keyword works like the factor keyword :blue:`PENTA15_18` for tetrahedra. It serves to transform :green:`TETRA4` 
into :green:`TETRA8` by adding a node in the middle of each face of the tetrahedron.

.. warning::

   The use of this keyword is not advised if the mesh contains other types of volume elements (:green:`PENTA`, :green:`HEXA` 
   and :green:`PYRAM`). See :numref:`u4.23.02.3`.

.. _u4.23.02.4.9:

Keyword :blue:`QUAD_LINE` (:gray:`QUADRATIC_TO_LINEAR`)
--------------------------------------------------------

::

    ◊ QUAD_LINE

This functionality is used to create a linear mesh starting from a quadratic mesh, one cannot apply it only to part of 
the mesh (pay attention in this case to the connection of the linear and quadratic regions). See :numref:`u4.23.02.3`.

.. _u4.23.02.4.9.1:

Operands :blue:`GROUP_MA/TOUT`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

    ♦ | GROUP_MA = l_el_grp,
      | TOUT =  'OUI',

The elements stipulated by the user with these two keywords will be transformed into quadratic elements.

.. warning::

   The use of keyword :blue:`GROUP_MA` is not advised. See :numref:`u4.23.02.3`.

.. _u4.23.02.4.10:

Keyword :blue:`REPERE` (:gray:`REFERENCE_FRAME`)
-------------------------------------------------

::

    ◊ REPERE

An occurrence of this factor keyword is used to define a new mesh starting from the old mesh by carrying out a change 
of reference. 

This functionality is used in particular in the macro-command :red:`MACR_CARA_POUTRE` :ref:`[U4.42.02] <u4.42.02>` for 
the calculation of the warping constant.

.. _u4.23.02.4.10.1:

Operands :blue:`TABLE/NOM_ORIG/NOM_ROTA/GROUP_MA`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

    ♦ TABLE = tab

One gives here the name of the concept :green:`table` of "geometrical characteristics" which contains, in particular, the 
coordinates of the center of inertia and the center of torsion, the nautical angles defining the main inertial frame, etc.
This table can be obtained by the command :red:`POST_ELEM` with the factor keywords :blue:`CARA_GEOM` or 
:blue:`CARA_POUTRE` :ref:`[U4.81.22] <u4.81.22>`.

::

    ◊ NOM_ORIG = / 'CGD',
                 / 'TORSION',

The origin of the new reference frame is indicated: either the center of gravity or the center of torsion.

::

    ◊ NOM_ROTA = / 'INERTIE',

Indicate the direction of the new reference frame. Only one solution is possible: the directions are those of the 
main inertial frame.

::

    ◊ GROUP_MA = el_grp 

If :blue:`NOM_ORIG = 'CDG'`, one can indicate the name of the element group whose center of gravity will be the origin 
of the new frame. If :blue:`GROUP_MA` is not used, the center of gravity of the whole :blue:`MODELE` will be the origin 
of the new coordinate system.

If :blue:`NOM_ORIG = "TORSION"`, the keyword :blue:`GROUP_MA` is inoperative.

.. _u4.23.02.4.11:

Keyword :blue:`COQU_VOLU` (:gray:`SHELL_TO_VOLUME_ELEMENTS`)
-------------------------------------------------------------

::

    ◊ COQU_VOLU

From the data of a group of surface elements (:green:`QUAD, TRIA3`), we build volume elements (:green:`HEXA8, PENTA6`) 
by extrusion along the normal to the surface elements (at a node, we take the average of the normals of the coincident 
elements). A single element layer is created.

The operation only applies to linear elements; if you want to create a quadratic element, you will need to add
:red:`CREA_MAILLAGE`/:blue:`LINE_QUAD`.

.. _u4.23.02.4.11.1:

Operand :blue:`NOM`
^^^^^^^^^^^^^^^^^^^

::

    ♦ NOM = name,

Name of the group of volume elements created during this operation.

.. _u4.23.02.4.11.2:

Operand :blue:`GROUP_MA`
^^^^^^^^^^^^^^^^^^^^^^^^^^

::

    ♦ GROUP_MA = l_el_grp,

Element groups constituting the surface mesh to be extruded.

.. _u4.23.02.4.11.3:

Operand :blue:`EPAIS` (:gray:`THICKNESS`)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

    ♦ EPAIS = thick,

Thickness of the element layer created (thickness of the shell).

.. _u4.23.02.4.11.4:

Operand :blue:`PLAN` (:gray:`REFERENCE_PLANE_OF_SHELL`)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

    ♦ PLAN = / 'SUP',
             / 'INF',
             / "MOY",

Specifies whether the surface that is composed of :green:`l_el_grp` will be the UPPER (:blue:`SUP`), LOWER (:blue:`INF`)
or MIDDLE ("blue:`MOY`) plane of the shell.

.. _u4.23.02.4.11.5:

Operand :blue:`TRANSLATION`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

    ♦ TRANSLATION = / 'SUP',
                    / 'INF',

If :blue:`PLAN = 'MOY'`, we specifies whether the initial surface :green:`l_el_grp` is translated into the
UPPER (:blue:`SUP`) or the LOWER (:blue:`INF`) skin.

.. _u4.23.02.4.11.6:

Operands :blue:`PREF_MAILLE/PREF_NOEUD/PREF_NUME` (:gray:`PREFIX)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Same as for :blue:`MODI_MAILLE` (:numref:`u4.23.02.4.4.2`).

.. _u4.23.02.4.12:

Keyword :blue:`CREA_FISS` (:gray:`CREATE_CRACK`)
------------------------------------------------

::

    ◊ CREA_FISS = _F(♦ NOM = el_grp,             [TXM]
                     ♦ GROUP_NO_1 = nd_grp1,     [group_no]
                     ♦ GROUP_NO_2 = nd_grp2,     [group_no]
                     ♦ PREF_MESH = prefix_e,     [kn]
                     ◊ PREF_NUME = / index,      [I]
                                   / 1,          [DEFAULT]
                   )

Used to create a crack with joint elements :ref:`[R3.06.09] <r3.06.09>` or elements with discontinuities
:ref:`[R7.02.12] <r7.02.12>` along a line defined by two groups of nodes arranged opposite each other. Both
node groups must have the same number of nodes and be ordered beforehand (for example, with 
:red:`DEFI_GROUP`/:blue:`CREA_GROUP_NO`/:blue:`OPTION = 'NOEUD_ORDO'`) so that their numbering "starts" on the 
same side (see :numref:`fig_u4.23.02.1`).

One will then be able to assign a model of the type "joint" to these new :green:`QUAD4` elements (for example,
:blue:`'PLAN_JOINT'`).

The created elements will have names formed from the prefix :green:`prefix_e` followed by a number.
For example, if :blue:`PREF_MAILE = 'FS'` and :blue:`PREF_NUME = 7`, the created elements will be called: 
:green:`FS7, FS8`, etc.  A new :blue:`GROUP_MA` (called :green:`el_grp`) will also be created that contains all 
the newly created :green:`QUAD4` elements.

.. _u4.23.02.4.12.1:

Operand :blue:`NOM`
^^^^^^^^^^^^^^^^^^^

Name of the group of volume elements created during this operation.

.. _u4.23.02.4.12.2:

Operands :blue:`GROUP_NO_1/GROUP_NO_2`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Node groups constituting the crack faces. The node group :blue:`GROUP_NO_1` contains the local nodes 1 and 2 (the first 
node in the group has local numbering equal to 1), the :blue:`GROUP_NO_2` contains the local nodes 3 and 4 (the first 
node of the group has a local numbering equal to 4).

It is necessary to choose these groups of nodes according to the geometry so that the local numbering of the elements 
is carried out in the counterclockwise direction.

.. _fig_u4.23.02.1:

.. figure:: u4.23.02/fig1_u4.23.02.png
   :height: 250 px
   :align: center

   Segment ordering

.. _u4.23.02.4.12.3:

Operand :blue:`PREF_MAILLE` 
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

   /  PREF_NUME

Same as for :blue:`MODI_MAILLE` (:numref:`u4.23.02.4.4.2`).

.. _u4.23.02.4.13:

Keyword :blue:`ECLA_PG`
-----------------------

::

    ◊ ECLA_PG

This factor keyword does not have to be used directly. It is used by the command :red:`MACR_ECLA_PG` 
:ref:`[U4.44.14] <u4.44.14>`.

.. _u4.23.02.4.14:


Keyword :blue:`GEOM_FIBRE`
--------------------------

::

    ◊ GEOM_FIBRE

This keyword is used to obtain the mesh created by :red:`DEFI_GEOM_FIBRE` :ref:`[U4.26.01] <u4.26.01>`. This mesh
contains all the groups of fibers of the study as well as the mesh containing all the fibers.

.. _u4.23.02.4.15:

Keyword :blue:`RESTREINT` (:gray:`RESTRICTED`)
-----------------------------------------------

This factor keyword (non-repeatable) is used to generate a "sub" mesh extracted from an existing mesh (:green:`mesh_1`).
The new "restricted" mesh (:green:`mesh_2`) is formed starting from a list of elements provided by the user.

.. _u4.23.02.4.15.1:

Elements
^^^^^^^^^

The keyword :blue:`GROUP_MA` is used to define the elements of the restricted mesh.

.. _u4.23.02.4.15.2:

Nodes
^^^^^^

The nodes retained are those of the elements retained. Moreover, if keyword :blue:`GROUP_NO = l_nd_grp` is used, the 
nodes of the :green:`l_nd_grp` groups are added.

.. _u4.23.02.4.15.3:

Element groups
^^^^^^^^^^^^^^

The mesh :green:`mesh_2` will contain all the :blue:`GROUP_MA` of :green:`l_el_grp`. Moreover, if the keyword
:blue:`TOUT_GROUP_MA = 'OUI'` is used, the nonempty element groups of :green:`mesh_1` are added.

.. _u4.23.02.4.15.4:

Node groups
^^^^^^^^^^^^^^^^

The mesh :green:`mesh_2` will contain all the :blue:`GROUP_NO` in :green:`l_nd_grp`. Moreover, if the keyword
:blue:`TOUT_GROUP_NO = 'OUI'` is used, the non-empty node groups of :green:`mesh_1` are added.

.. _u4.23.02.4.16:

Keyword :blue:`DECOUPE_LAC` (:gray:`CUT_ELEMENT_LOCAL_AREA_CONTACT`)
---------------------------------------------------------------------

::

    ♦ DECOUPE_LAC

This keyword is used to obtain a mesh containing "patches" created on the element group specified in 
:blue:`GROUP_MA_ESCL`. It is an operation of pre-processing of the slave elements for the treatment of contact 
by the mortar ``LAC`` method.

It should be noted that the mesh underlying the elements of :blue:`GROUP_MA_ESCL` are also subdivided. The subdivisions
of the :green:`PYRA5, PYRA13, PENTA6, PENTA15` and :green:`HEXA8, HEXA20` elements for the case
:blue:`DECOUPE_HEXA = 'PYRA'` are "non-compliant" in the sense that they introduce elements that are different from 
the element that is subdivided (pyramids and / or tetrahedrons) so as not to add than one node per subdivided element 
in the mesh.

Subdivision of :green:`PENTA18` is not supported.

.. warning::

   :blue:`DECOUPE_LAC` must be the last command applied to the mesh. The :red:`MODI_MAILLAGE` operations must be 
   carried out before this command is used.

.. _fig_u4.23.02.2:

.. figure:: u4.23.02/fig2_u4.23.02.png
   :height: 500 px
   :align: center

   2D case (Thesis G. Drouet)

.. _fig_u4.23.02.3:

.. figure:: u4.23.02/fig3_u4.23.02.png
   :height: 500 px
   :align: center

   3D case (:blue:`DECOUPE_HEXA = 'HEXA'`)

::

    ◊ DECOUPE_HEXA

This keyword is used to define the type of subdivision used on the :green:`HEXA8` and the :green:`HEXA20` elements. By 
default, these elements are divided into pyramids to limit the number of nodes added. However when using the 
:blue:`HEXA` value, they are divided into :green:`HEXA` which can be useful if introducing pyramids into the mesh 
poses a problem.

.. _u4.23.02.4.17:

Operand :blue:`INFO`
---------------------

::

    ◊ INFO = / 1
             / 2

Specifies the information printed in the message file (:blue:`1`: no printing, :blue:`2`: details of the number 
of elements created, modified, etc.)

.. _u4.23.02.4.18:

Operand :blue:`TITRE` (:gray:`TITLE`)
--------------------------------------

::
    ◊ TITRE = title

Allows you to specify a title.


.. _u4.23.02.5:

Examples
========

.. _u4.23.02.5.1:

Element duplication
--------------------

Let :green:`mesh_1` be a mesh already containing the elements: :green:`M1 M2 M3` and the element 
group :green:`shell` containing :green:`M1 M2`.

Each element is based on the following nodes:

::

  M1: N1 N2 N3
  M2: N3 N4 N5
  M3: N4 N5 N6

::

  mesh_2 = CREA_MAILLAGE(MAILLAGE = mesh_1,
                         CREA_MAILLE = _F(NOM = ground,
                                          GROUP_MA = 'shell',
                                          PREF_MAILLE = 'A',
                                          PREF_NUME = 100))

After the call to command :red:`CREA_MAILLAGE`, the new mesh contains:

* element groups:

  * shell (initial)
  * ground = (elements: :green:`A100 A101`)

* the elements contain the following nodes:

  * :green:`M1: N1 N2 N3`
  * :green:`M2: N3 N4 N5`
  * :green:`M3: N4 N5 N6`
  * :green:`A100: N1 N2 N3`
  * :green:`A101: N3 N4 N5`

.. _u4.23.02.5.2:

Transformation of 6-noded triangles into 7-noded triangles
-----------------------------------------------------------

::

   mesh_2 = CREA_MAILLAGE(MAILLAGE = mesh_1,
                          MODI_MAILLE = _F(GROUP_MA = 'triangle',
                                           OPTION = 'TRIA6_7',
                                           PREF_NOEUD = 'NMI',
                                           PREF_NUME = 10))

Suppose that in :green:`mesh_1` the :blue:`GROUP_MA` :green:`triangle` is composed of two elements 
:green:`M1, M2` having the following nodes:

::

  M1: N1 N2 N3 N4 N5 N6
  M2: N1 N2 N7 N4 N8 N9

In mesh :green:`mesh_2`, the two elements :green:`M1, M2` will have the following nodes:

::

  M1: N1 N2 N3 N4 N5 N6 NMI10
  M2: N1 N2 N7 N4 N8 N9 NMI11

.. _u4.23.02.5.3:

Transforming 4-node quadrilaterals into 3-node triangles
---------------------------------------------------------

This example comes from test :green:`SSLV04E` :ref:`[V3.04.004] <v3.04.004>`: The geometry, representing 
a quarter disc, is meshed with quadrilateral elements. We want an eighth of the disc with a triangle mesh.

::

  mesh = CREA_MAILLAGE(MAILLAGE = mesh_0,
                       MODI_MAILLE = _F(GROUP_MA = 'S2',
                                        OPTION = 'QUAD_TRIA3',
                                        PREF_MAILLE = 'MS',
                                        PREF_NUME = 1))

.. _u4.23.02.5.4:

Example of use of :blue:`DECOUPE_LAC` 
-------------------------------------

This example comes from benchmark :green:`ZZZZ383` :ref:`[V1.01.383] <v1.01.383>`.

::

   # Convert TETRA4/Face

   mesh = LIRE_MAILLAGE(UNITE = 20,
                        FORMAT='MED')
   mesh81 = CREA_MAILLAGE(MAILLAGE = mesh,
                          DECOUPE_LAC = _F(GROUP_MA_ESCL = 'face_2'))

.. _fig_u4.23.02.5.4-1:

.. figure:: u4.23.02/fig4_u4.23.02.png
   :height: 400 px
   :align: center

   Division of surface elements for the :blue:`LAC` contact algorithm.
