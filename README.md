Code-Aster Manuals and Documentation
====================================

This project intends to provide English translations of selected Code-Aster manuals in HTML format.
The currently available machine-generated English translations are unreadable and have 
limited the use of Code-Aster around the world.

## Downloading the documentation

To download the documentation, use:

```
  git clone https://gitlab.com/biba1632/code-aster-manuals.git code-aster-manuals
```

## Building the documentation

Go into the <code>code-aster-manuals</code> directory

```
  cd code-aster-manuals
```

* Create a Python 3 virtual environment

```
  python -m venv code-aster-manual-venv
```

* Activate the virtual environment

```
  source code-aster-manual-venv/bin/activate
```

* Install the required Python packages (Sphinx + others)

```
  pip install -U Sphinx
  pip install -U lsst-sphinx-bootstrap-theme
  pip install matplotlib
  pip install IPython
```

etc.  The requirements will depend on what you have in the documentation.  A bit of trial and error may be 
needed. 

* Build the documentation

```
  make html
```

This will update the <code>build</code> diretory.

## Reading the documentation

To read the documentation, open the file <code>code-aster-manuals/build/html.index.html</code> in a browser.

## Adding new documentation

* Activate the virtual environment that was created for building the documentation HTML files:

```
  source code-aster-manual-venv/bin/activate
```
*  If the <code>pdfminer.six</code> is not installed, install it using

```
  pip install pdfminer.six
```
* Locate the PDF documentation file that is to be translated.  For example, if you want to translate
  the document U1.03.01 that is contained in file <code>u1.03.01.pdf</code>, download the machine-translated
  English version from [https://www.code-aster.org/V2/doc/v14/fr/index.php?man=U1] and save it in

```
  code-aster-manuals/source/docs/PDF/u1.03.01.pdf
```

* Go to the <code>source/docs</code> directory and use the <code>pdf2txt</code> translator from <code>pdfminer.six</code>
  to convert the PDF into a text file.  If there are figures in the document, use

```
  cd source/docs
  pdf2txt.py PDF/u1.03.01.pdf --output-dir u1.03.01 > u1.03.01.rst
  
```

  If there are no figures, use

```
  cd source/docs
  pdf2txt.py PDF/u1.03.01.pdf > u1.03.01.rst
  
```

* Go back to the root directory of the documentation (the one which contains the <code>Makefile</code>):

```
  cd  ../..
```

* In a new temporary directory,
  download the French version of the PDF from [https://www.code-aster.org/V2/doc/v14/fr/index.php?man=U1]
  and use Google Translate to do a machine translation of the PDF file.  This translation, while not
  perfect, is typically better than the one provided in the official Code-Aster documents.  Keep the
  Google-translated version for reference during the process.

  Also open the English PDF versio that you had downloaded earlier.  These two translated versions
  will be need in the editing process. 

* Open a new window where you can edit the <code>u1.03.01.rst</code> file that you have just created
  using <code>pdf2txt.py</code>.

* Add the file to the <code>index.rst</code> file in the <code>source</code> directory.

```
  cd source
```

```
  .. toctree::
   :maxdepth: 2
   :caption: Contents:

   docs/u0.00.01.rst
   docs/u1.03.00.rst
   docs/u1.03.01.rst
   ...
   ...
```

* Now you are ready to start modifying the file and updating the HTML output.

## Modifying documentation

* At this stage you will need two work windows (one containing the Python virtual environment - needed
  for building the documentation, open in directory <code>code-aster-manuals</code>) and the other
  for editing the documentation, directory <code>code-aster-manuals/source/docs</code>.

  In addition, there will be two reference windows open: the Google-translate window on your browser 
  and the PDF reader window for the Code-Aster official English translaton.

* In the virtual environment window, build the documentation:

```
  make html
```

  You may get some warnings.  Ignore them and open a browser window, and check that the new file
  added to <code>index.rst</code> has been identified and processed.  Whe you open the
  <code>build/html/index.html</code> file in the browser, you probably see a <code>#</code> instead of
  a title for the new document file.

* You will now need to add a label that the document generator can recognize, and use to write 
  out the correct titles.  Edit the <code>u1.03.01.rst</code> in the <code>docs</code> directory
  and, at the top of the file, add

```
  .. _supervisor_and_commands:

  *********************************************
  **U1.03.01**: Supervisor and command language 
  *********************************************
```
  These restructuredText directives, set a header and an associated label for the document.
  
  From the Python environment, run

```
  make html
```

  Now the <code>build/html/index.html</code> file shows the title of the document.

* The rest of the editing process consists of improving the language and adding
  appropriate restructureText directives to the document.

* After you are satisfied with a ranslation, submit a pull request and we will take it
  from there.

## Restuctured text FAQ:

A detailed list of possible directives can be found at https://docutils.sourceforge.io/docs/ref/rst/directives.html.

### *Titles and section headers* 

Document titles are specified using

```
************************
This is a document title
************************
```

   Section headers can be specified using:

```
This is a section header
========================

This is a subsection header
---------------------------

This is a subsubsection header
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This is a paragraph header
++++++++++++++++++++++++++

```

### *Adding a label* 

To add a label, use

```
.. _document_title_label_name:

************************
This is a document title
************************

.. _section_label_name:

This is a section header
========================

.. _subsection_label_name:

This is a subsection header
---------------------------

.. _subsubsection_label_name:

This is a subsubsection header
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
```

### *Adding a reference to a label*

```
:ref:`label_name`
```

  where the label name has been defined earlier using

```
.. _label_name:
```

  and associated with a section header or figure name.

  If you don't want the default name of the header to be referred to, use

```
:ref:`My Label Name <label_name>`
```

### *Bold/italic fonts*

   A section of text can be highlighted using bold (<code>**</code>), 
   italic (<code>`</code>), or sans serif (<code>``</code>) fonts.

```
   This is **bold**, this is `italic`, this is ``sans serif``.
```

### *Note boxes*  

   Note boxes can be created in several ways.

   The standard note box is specified using

```
.. note::

   This is a note that should be noted.

.. important::

   This is a very important note.

.. admonition:: My note title

   This note contains your own title.
```

### *Inserting code snippets/pre-formatted text*  

```
  The code snippet follows ::

    x = 1
    y = 2
```

Alternatively, code-blocks can be lexically analyzed and color highlighted using

```
  .. code-block:: python

    x = 1
    y = 2
```

```
  This text has been preformatted

  ::

    a   1   2   3   4
    b   5   6   7   8
```

```
  Importing preformatted text from a file:

  .. include::  filename.txt
     :literal:
```

  For inline code snippets, use

```
  this is a code snippet in a line -  :python:`x = 4`
```

  where the "role" "python" is defined at the beginning of the file as

```
  .. role:: python(code)
     :language: python
```

### *Bulleted lists*

```
  * Item 1
  * Item 2
  * Item 3
```

```
  1. First one
  2. Second one
  3. Third one
```

### *Coloring text*

  First define CSS colors in <code>source/_static/code_aster.css</code>:

```
.red {
    color: red;
}
.blue {
    color: blue;
}
.green {
    color: green;
}

```

  To color small segments of the text, define roles at the beginning of the file:

```
.. role:: red
.. role:: blue
.. role:: green

```

  You can use these roles later in the text with:

```
This text is colored :red:`red` and this is :blue:`blue`.
```

### *Capturing images from the screen*

  When describing a graphical user interface, you may need to capture windows from your screen.

  For *single windows*, the easiest way is to get the window ID using:

```
  xwininfo
```

  The window Id will be of the form <code>0x5200ede</code> as you can see from the <code>xwininfo</code>
  output:

```
  xwininfo: Window id: 0x5200ede "~"
  ....
```
  Next, you can just use Imagemagick <code>import</code> to capture an image of the window:

```
  import -window 0x5200ede  my_window.png
```

  However, when you want to also capture a *dropdown menu*, the above method will not work and
  you will have to capture the root window, giving <code>import</code> enough time that you can 
  go and activate the dropdown menu:

```
  sleep 10; import -window root my_screen.png
```

  You can then open <code>my_screen.png</code> in ImageMagick or Gimp and crop the parts of
  the screen that you don't need.

### *Images and figures*

  Once you have the images/figures available, you can include them in the document in 
  several ways.

  Directly embed the image (SVG and PNG are preferred), using

```
  .. image:: u3.01.00/fig1.svg
     :height: 400px
     :align: center
```

  Assign a caption to the image (space required after <code>:align:</code>)

```
  .. figure:: u3.01.00/fig1.svg
     :height: 400px
     :align: center

     This is the figure caption
```

  Figures can be referred to using labels:

```
  .. _my_figure::

  .. figure:: my_figure_1.png

     Caption of my figure.
  
   ...............

   In :numref:`my_figure` we observe that ...
```

  Alternatively, you can use substitution

```
  .. |point_elem| image:: u3.01.00/point_elem.png
     :width: 300px
     :align: middle
  .. |segment_elem| image:: u3.01.00/segment_elem.png
     :width: 300px
     :align: middle

  .....

  .. note:: 

     This is a substituted image

     |segment_elem|
```

If you want to add the image (e.g., icon) to a line of text, you can use

```
   This is a line of text with an image: |point_elem| embedded in it.
```

To place two figures side-by-side use something like:

```
.. list-table::

   * - .. figure:: forma01b_1.png
             :height: 300px

             Original mesh

     - .. figure:: forma01b_2.png
             :height: 300px

             Refined mesh
```

### *Equations*

Inline math can be included using

```
   The analytical function :math:`F(t) = \cos(\Omega t)` is then calculated
```

For equations on separate lines, use

```
  .. math::

    \alpha_t(i) = \lambda_p + \int_0^1 \exp(x) dx

```

To label equations and to refer to them in the text, use

```
  .. math::

    :label: equation_1

    \alpha_t(i) = \lambda_p + \int_0^1 \exp(x) dx


  In :eq:`equation_1`, we see that ...

```

If a large number of equations ar involved, the process of transcribing to ".rst"
format can become tedious.  The amount of effort can be reduced slightly by 
writing out the equations in a ".tex" file using the abbreviations defined in
"source/docs/reference/latex_expand.py".

This program can then be run on the LaTeX file using a command of the form

```
  ./latex_expand.py r3.03.01/appendix1.tex appendix1.tex_extra
```
The generated ".tex_extra" file can then be converted to ".rst" using "pandoc":

```
  pandoc -s -t rst appendix1.tex_extra -o appendix1.rst
```
Some editing may be required at this stage to take the restructuredText file
produced by pandoc and incorporate it into the document.

### *Footnotes*

To add footnotes, use

```
  two meshes is managed at the level of the command file by [#footnote3]_

  ....
  ....

  .. rubric:: Footnotes

  .. [#footnote3] This is a footnote
```

### *Tables*

Tables can be added using either

```

  ============= =============== =============
    Header 1       Header 2         Header 3
  ============= =============== =============
    aaaa           bbbb              cccc
    aaaa           bbbb              cccc
  ============= =============== =============
```

or

```

 +--------------------+---------------------+-------------+
 |  Header 1          | Header 2            |   Header 3  |
 +====================+=====================+=============+
 |   aaaa             | bbbb                |cccc         |
 +--------------------+---------------------+-------------+
 |   aaaa             | bbbb                |cccc         |
 +--------------------+---------------------+-------------+
```

### *Index entries*

  Index entries can be added using directives at the beginning of the relevant paragraph:

```
  2.4 Macro-commands 
  ------------------

  .. index:: DEBUT(), FORMULE(), INCLUDE(), INCLUDE_MATERIAU(), POURSUITE()
```

### *Including HTML*

  Use

```
  .. raw:: html

     This is going to be rendered as HTML.
```
