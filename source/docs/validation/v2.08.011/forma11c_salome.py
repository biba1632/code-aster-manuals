#!/usr/bin/env python
import salome
from salome.geom import geomBuilder
from salome.smesh import smeshBuilder
import GEOM, SMESH
import math
import json

ExportPATH="/home/banerjee/Salome/forma11/"

# Read the profile coordinates
file_id = open( r''+ExportPATH+'forma11c_profile.json'+'', 'r')
coords = json.load(file_id)
file_id.close()

salome.salome_init()

#-------------------
# Geometry
geompy = geomBuilder.New()

O = geompy.MakeVertex(0, 0, 0)
OX = geompy.MakeVectorDXDYDZ(1, 0, 0)
OY = geompy.MakeVectorDXDYDZ(0, 1, 0)
OZ = geompy.MakeVectorDXDYDZ(0, 0, 1)

v_profile = []
for coord in coords:
    v = geompy.MakeVertex(coord[0], coord[1], coord[2])
    v_profile.append(v)

curve = geompy.MakeInterpol(v_profile, False, True)
surf = geompy.MakeRevolution2Ways(curve, OZ, math.pi)
geompy.addToStudy(surf, 'tower' )

# Geometry groups
surf_ID = geompy.GetSubShapeID(surf, surf)
g_surf = geompy.CreateGroup(surf, geompy.ShapeType["FACE"])
geompy.AddObject(g_surf, surf_ID)
id_g_surf = geompy.addToStudy(g_surf, 'GM1')

surf_bb = geompy.BoundingBox(surf)
x_max = surf_bb[1]
z_min = surf_bb[4]
pt = geompy.MakeVertex(x_max, 0, z_min)
bot_edge = geompy.GetEdgeNearPoint(surf, pt)
bot_edge_ID = geompy.GetSubShapeID(surf, bot_edge)
g_bot_edge = geompy.CreateGroup(surf, geompy.ShapeType["EDGE"])
geompy.AddObject(g_bot_edge, bot_edge_ID)
id_g_bot_edge = geompy.addToStudy(g_bot_edge, 'GM4')


#-------------------
# Mesh
smesh = smeshBuilder.New()

# Initialize mesh
mesh = smesh.Mesh(surf)
smesh.SetName(mesh.GetMesh(), 'mesh')

# Algorithms
Regular_1D = mesh.Segment()
QuadFromMedialAxis_1D2D = mesh.Quadrangle(algo = smeshBuilder.QUAD_MA_PROJ)
Number_of_Segments = Regular_1D.NumberOfSegments(60)
Number_of_Layers = QuadFromMedialAxis_1D2D.NumberOfLayers(31)

smesh.SetName(Number_of_Layers, 'Number of Layers')
smesh.SetName(Number_of_Segments, 'Number of Segments')

# Compute mesh
isDone = mesh.Compute()

# Map geometry groups to mesh
base = mesh.GroupOnGeom(g_bot_edge, 'GM4', SMESH.EDGE)
tower = mesh.GroupOnGeom(g_surf, 'GM1', SMESH.FACE)

# Export mesh
mesh.ExportMED( r''+ExportPATH+'forma11c_salome.mmed'+'')

# Update GUI
if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()
