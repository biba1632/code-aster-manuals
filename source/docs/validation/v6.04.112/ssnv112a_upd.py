#!/usr/bin/env python
# coding: utf-8

import gmsh
import sys
import math

gmsh.initialize()

# Use OpenCASCADE kernel
model = gmsh.model
occ = model.occ
mesh = model.mesh
model.add("ssnv112a")

# Default element size
el_size = 0.01

# Geometry
RI = 0.1
RE = 0.2
NR = 12
NT = 10
NZ = 2
H = 0.01

# Add points
PO = occ.addPoint(0.,  0.,  0., el_size)
POP = occ.addPoint(0.,  0.,  H, el_size)
A = occ.addPoint(0.,  RI,  0., el_size)
B = occ.addPoint(0.,  RE,  0., el_size)
E = occ.addPoint((RI*(2.**0.5)/(-2.)), (RI*(2.**0.5)/2.),  0., el_size)
F = occ.addPoint((RE*(2.**0.5)/(-2.)), (RE*(2.**0.5)/2.),  0., el_size)
node_mid = occ.addPoint(0., RI, (H/NZ/2.), el_size)

# Add lines and arcs
LAB = occ.addLine(A, B)
LBF = occ.addCircleArc(B, PO, F)
LFE = occ.addLine(F, E)
LEA = occ.addCircleArc(E, PO, A)

# Add curve loop and plane surface
FACINF_loop = occ.addCurveLoop([LAB, LBF, LFE, LEA])
FACINF = occ.addPlaneSurface([FACINF_loop])

# Create volume by extrusion
VOLUME = occ.extrude([(2, FACINF)], 0., 0., H, [2], recombine=True)

occ.synchronize()

print(VOLUME)

# Physical groups for the points
A_g = model.addPhysicalGroup(0, [A])
model.setPhysicalName(0, A_g, 'A')
B_g = model.addPhysicalGroup(0, [B])
model.setPhysicalName(0, B_g, 'B')
E_g = model.addPhysicalGroup(0, [E])
model.setPhysicalName(0, E_g, 'E')
F_g = model.addPhysicalGroup(0, [F])
model.setPhysicalName(0, F_g, 'F')
AP_g = model.addPhysicalGroup(0, [7])
model.setPhysicalName(0, AP_g, 'AP')
EP_g = model.addPhysicalGroup(0, [10])
model.setPhysicalName(0, EP_g, 'EP')
nodemid_g = model.addPhysicalGroup(0, [node_mid])
model.setPhysicalName(0, nodemid_g, 'NOEUMI')

# Physical groups for the edges
LAB_g = model.addPhysicalGroup(1, [LAB])
model.setPhysicalName(1, LAB_g, 'LAB')
LBF_g = model.addPhysicalGroup(1, [LBF])
model.setPhysicalName(1, LBF_g, 'LBF')
LFE_g = model.addPhysicalGroup(1, [LFE])
model.setPhysicalName(1, LFE_g, 'LFE')
LEA_g = model.addPhysicalGroup(1, [LEA])
model.setPhysicalName(1, LEA_g, 'LEA')

# Physical groups for the faces
FACEAB_g = model.addPhysicalGroup(2, [2])
model.setPhysicalName(2, FACEAB_g, 'FACEAB')
FACEAE_g = model.addPhysicalGroup(2, [5])
model.setPhysicalName(2, FACEAE_g, 'FACEAE')
FACSUP_g = model.addPhysicalGroup(2, [6])
model.setPhysicalName(2, FACSUP_g, 'FACSUP')
FACEEF_g = model.addPhysicalGroup(2, [4])
model.setPhysicalName(2, FACEEF_g, 'FACEEF')
FACINF_g = model.addPhysicalGroup(2, [FACINF])
model.setPhysicalName(2, FACINF_g, 'FACINF')

# Physical group for the volume
VOLUME_g = model.addPhysicalGroup(3, [1])
model.setPhysicalName(3, VOLUME_g, 'VOLUME')

num_nodes = NT+1
for curve in [2, 4, 9, 12]:
    #print(curve)
    mesh.setTransfiniteCurve(curve, num_nodes)
num_nodes = NR+1
for curve in [1, 3, 7, 11]:
    #print(curve)
    mesh.setTransfiniteCurve(curve, num_nodes)
num_node = NZ+1
for curve in [5, 6, 8, 10]:
    mesh.setTransfiniteCurve(curve, num_nodes)

for surf in occ.getEntities(2):
    mesh.setTransfiniteSurface(surf[1])

for vol in occ.getEntities(3):
    mesh.setTransfiniteVolume(vol[1])

#gmsh.option.setNumber('Mesh.Algorithm', 8)
gmsh.option.setNumber('Mesh.RecombineAll', 1)
gmsh.option.setNumber('Mesh.RecombinationAlgorithm', 1)
gmsh.option.setNumber('Mesh.Recombine3DLevel', 2)
gmsh.option.setNumber('Mesh.ElementOrder', 2)
gmsh.option.setNumber('Mesh.MshFileVersion', 2.2)
gmsh.option.setNumber('Mesh.MedFileMinorVersion', 0)
gmsh.option.setNumber('Mesh.SaveAll', 0)
gmsh.option.setNumber('Mesh.SaveGroupsOfNodes', 1)
gmsh.option.setNumber('Mesh.SecondOrderIncomplete', 1)

# Generate mesh
mesh.generate(3)
mesh.recombine()

# Save mesh
gmsh.write("ssnv112a_upd.med")

gmsh.fltk.run()

gmsh.finalize()


