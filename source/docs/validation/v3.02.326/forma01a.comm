# coding=utf-8

# Start
DEBUT()

# Define material
elastic = DEFI_MATERIAU(ELAS = _F(E = 200000.0,
                                   NU = 0.3,),)

# Read mesh
mesh = LIRE_MAILLAGE(FORMAT = 'MED',)

# Assign model to mesh
model = AFFE_MODELE(MAILLAGE = mesh,
                    AFFE = _F(TOUT = 'OUI',
                              PHENOMENE = 'MECANIQUE',
                              MODELISATION = 'C_PLAN',),)

# Modify mesh orientation
mesh = MODI_MAILLAGE(reuse = mesh,
                     MAILLAGE = mesh,
                     ORIE_PEAU_2D = _F(GROUP_MA = 'top',),)

# Assign material
material = AFFE_MATERIAU(MAILLAGE = mesh,
                         AFFE = _F(TOUT = 'OUI',
                                   MATER = elastic,),)

# Assign boundary conditions
bcs = AFFE_CHAR_MECA(MODELE = model,
                      DDL_IMPO = (_F(GROUP_MA = 'bottom',
                                     DY=0.0,),
                                  _F(GROUP_MA = 'left',
                                     DX=0.0,),),
                      PRES_REP = _F(GROUP_MA = 'top',
                                    PRES = -100.0,),)

# Run simulation
result = MECA_STATIQUE(MODELE = model,
                       CHAM_MATER = material,
                       EXCIT = _F(CHARGE = bcs,),)

# Add element stress results to basic simulation results
result = CALC_CHAMP(reuse = result,
                    RESULTAT = result,
                    CRITERES = ('SIEQ_ELNO','SIEQ_ELGA'),
                    CONTRAINTE = ('SIGM_ELNO'))

# Add nodal stress results to basic simulation results
result = CALC_CHAMP(reuse = result,
                    RESULTAT = result,
                    CONTRAINTE = 'SIGM_NOEU',
                    CRITERES = 'SIEQ_NOEU')

# Write results to MED file
IMPR_RESU(FORMAT = 'MED',
          UNITE = 80,
          RESU = _F(MAILLAGE = mesh,
                    RESULTAT = result,
                    NOM_CHAM=('DEPL', 'SIGM_NOEU', 'SIEQ_NOEU', 'SIEQ_ELGA',),),)

# Check accuracy
TEST_RESU(RESU = _F(NUME_ORDRE = 1,
                    GROUP_NO = 'B',
                    REFERENCE = 'ANALYTIQUE',
                    RESULTAT = result,
                    NOM_CHAM = 'SIGM_NOEU',
                    NOM_CMP = 'SIYY',
                    VALE_CALC = 303.4614397158003,
                    VALE_REFE = 303.0,
                    CRITERE = 'RELATIF',
                    PRECISION = 0.05,),)

# Check accuracy
TEST_RESU(RESU = _F(NUME_ORDRE = 1,
                    GROUP_NO = 'A',
                    REFERENCE = 'ANALYTIQUE',
                    RESULTAT = result,
                    NOM_CHAM = 'SIGM_NOEU',
                    NOM_CMP = 'SIXX',
                    VALE_CALC = -104.4118958498019,
                    VALE_REFE = -100.0,
                    CRITERE = 'RELATIF',
                    PRECISION = 0.15,),)

# Finish
FIN()
