
# coding: utf-8

# In[1]:

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from functools import reduce
import cmath


# In[24]:

energy = [None]*5
disp_y = [None]*5
err_Z1 = [None]*5
err_BR = [None]*5


# In[25]:

# energy, disp, error estimates (Z1 and Residual)
total = 140
start = 5
end = 7
skipfooter = total-end
energy[0] = pd.read_csv("./forma20a.8", engine = 'python', skiprows=start-1, 
                        skipfooter=skipfooter)

start = 12
end = 14
skipfooter = total-end
disp_y[0] = pd.read_csv("./forma20a.8", engine = 'python', skiprows=start-1, 
                        skipfooter=skipfooter)

start = 19
end = 21
skipfooter = total-end
err_Z1[0] = pd.read_csv("./forma20a.8", engine = 'python', skiprows=start-1, 
                        skipfooter=skipfooter)

start = 26
end = 28
skipfooter = total-end
err_BR[0] = pd.read_csv("./forma20a.8", engine = 'python', skiprows=start-1, 
                        skipfooter=skipfooter)

start = 33
end = 35
skipfooter = total-end
energy[1] = pd.read_csv("./forma20a.8", engine = 'python', skiprows=start-1, 
                        skipfooter=skipfooter)

start = 40
end = 42
skipfooter = total-end
disp_y[1] = pd.read_csv("./forma20a.8", engine = 'python', skiprows=start-1, 
                        skipfooter=skipfooter)

start = 47
end = 49
skipfooter = total-end
err_Z1[1] = pd.read_csv("./forma20a.8", engine = 'python', skiprows=start-1, 
                        skipfooter=skipfooter)

start = 54
end = 56
skipfooter = total-end
err_BR[1] = pd.read_csv("./forma20a.8", engine = 'python', skiprows=start-1, 
                        skipfooter=skipfooter)

start = 61
end = 63
skipfooter = total-end
energy[2] = pd.read_csv("./forma20a.8", engine = 'python', skiprows=start-1, 
                        skipfooter=skipfooter)

start = 68
end = 70
skipfooter = total-end
disp_y[2] = pd.read_csv("./forma20a.8", engine = 'python', skiprows=start-1, 
                        skipfooter=skipfooter)

start = 75
end = 77
skipfooter = total-end
err_Z1[2] = pd.read_csv("./forma20a.8", engine = 'python', skiprows=start-1, 
                        skipfooter=skipfooter)

start = 82
end = 84
skipfooter = total-end
err_BR[2] = pd.read_csv("./forma20a.8", engine = 'python', skiprows=start-1, 
                        skipfooter=skipfooter)

start = 89
end = 91
skipfooter = total-end
energy[3] = pd.read_csv("./forma20a.8", engine = 'python', skiprows=start-1, 
                        skipfooter=skipfooter)

start = 96
end = 98
skipfooter = total-end
disp_y[3] = pd.read_csv("./forma20a.8", engine = 'python', skiprows=start-1, 
                        skipfooter=skipfooter)

start = 103
end = 105
skipfooter = total-end
err_Z1[3] = pd.read_csv("./forma20a.8", engine = 'python', skiprows=start-1, 
                        skipfooter=skipfooter)

start = 110
end = 112
skipfooter = total-end
err_BR[3] = pd.read_csv("./forma20a.8", engine = 'python', skiprows=start-1, 
                        skipfooter=skipfooter)

start = 117
end = 119
skipfooter = total-end
energy[4] = pd.read_csv("./forma20a.8", engine = 'python', skiprows=start-1, 
                        skipfooter=skipfooter)

start = 124
end = 126
skipfooter = total-end
disp_y[4] = pd.read_csv("./forma20a.8", engine = 'python', skiprows=start-1, 
                        skipfooter=skipfooter)

start = 131
end = 133
skipfooter = total-end
err_Z1[4] = pd.read_csv("./forma20a.8", engine = 'python', skiprows=start-1, 
                        skipfooter=skipfooter)

start = 138
end = 140
skipfooter = total-end
err_BR[4] = pd.read_csv("./forma20a.8", engine = 'python', skiprows=start-1, 
                        skipfooter=skipfooter)


# In[26]:

print(energy[4])
print(disp_y[4])
print(err_Z1[4])
print(err_BR[4])


# In[70]:

# Reference values after four uniform remeshing runs on TRIA6
ener_ref =  0.102242
disp_ref = -0.0614777
Z1_ref = 1.0e-6
BR_ref = 1.0e-6


# In[71]:

# Initialization of Python lists
energy_vs_refine     = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
disp_y_vs_refine     = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
err_energy_vs_refine = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
err_disp_y_vs_refine = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
Z1_indic_vs_refine   = [42.663, 27.752 , 15.253 , 7.882 , 3.994 , 2.016, 0.0, 0.0]
BR_indic_vs_refine   = [36.691, 24.666 , 13.950 , 7.316 , 3.734 , 1.891, 0.0, 0.0]
num_dof_vs_refine    = [48, 156 , 555 , 2085 , 8076 , 31761, 0.0, 0.0]


# In[72]:

energy_mat     = np.zeros((len(energy_vs_refine), 2))
disp_y_mat     = np.zeros((len(disp_y_vs_refine), 2))
err_energy_mat = np.zeros((len(err_energy_vs_refine), 2))
err_disp_y_mat = np.zeros((len(err_disp_y_vs_refine), 2))
Z1_indic_mat   = np.zeros((len(Z1_indic_vs_refine), 2))
BR_indic_mat   = np.zeros((len(BR_indic_vs_refine), 2))
err_Z1_mat     = np.zeros((len(Z1_indic_vs_refine), 2))
err_BR_mat     = np.zeros((len(BR_indic_vs_refine), 2))
num_dof_mat    = np.zeros((len(num_dof_vs_refine), 2))


# In[73]:

for i in range(len(energy_vs_refine)) :
    energy_mat[i][0]     = i
    disp_y_mat[i][0]     = i
    err_energy_mat[i][0] = i
    err_disp_y_mat[i][0] = i
    Z1_indic_mat[i][0]   = i
    BR_indic_mat[i][0]   = i
    err_Z1_mat[i][0]     = i
    err_BR_mat[i][0]     = i
    num_dof_mat[i][0]    = i

    energy_mat[i][1]     = 0.0
    disp_y_mat[i][1]     = 0.0
    err_energy_mat[i][1] = 0.0
    err_disp_y_mat[i][1] = 0.0
    Z1_indic_mat[i][1]   = 0.0
    BR_indic_mat[i][1]   = 0.0
    err_Z1_mat[i][1]     = 0.0
    err_BR_mat[i][1]     = 0.0
    num_dof_mat[i][1]    = 0.0


# In[84]:

for num_calc in np.arange(0,5):
    energy_mat[num_calc][1] = energy[num_calc]['TOTALE']
    err_energy_mat[num_calc][1] = abs((energy_mat[num_calc][1] - ener_ref)/ener_ref)*100
    disp_y_mat[num_calc][1]  = disp_y[num_calc]['DY']
    err_disp_y_mat[num_calc][1]  = abs((disp_y_mat[num_calc][1] - disp_ref)/disp_ref)*100
    Z1_indic_mat[num_calc][1]  = err_Z1[num_calc]['MAX_ERREST']
    err_Z1_mat[num_calc][1]  = np.log(abs((Z1_indic_mat[num_calc][1] - Z1_ref)/Z1_ref))
    BR_indic_mat[num_calc][1]  = err_BR[num_calc]['MAX_ERREST']
    err_BR_mat[num_calc][1]  = np.log(abs((BR_indic_mat[num_calc][1] - BR_ref)/BR_ref))


# In[85]:

print(err_BR_mat)


# In[89]:

# Plot the data
fig = plt.figure(figsize=(6,6))
ax = fig.add_subplot(111)
plt.plot(err_energy_mat[:,0], err_energy_mat[:,1], 'C0-o', label='Energy error')
plt.plot(err_disp_y_mat[:,0], err_disp_y_mat[:,1], 'C1--o', label='Displacement error')
plt.plot(err_Z1_mat[:,0], err_Z1_mat[:,1], 'C2-o', label='log(Max. Z1 error)')
plt.plot(err_BR_mat[:,0], err_BR_mat[:,1], 'C3-o', label='log(Max. Residual error)')
plt.xlabel('Iteration number (for uniform refinement)', fontsize=16)
plt.ylabel('Error measures (%)', fontsize=16)
plt.grid(True)
ax.set_xlim([0, 4])
#ax.set_ylim([0, 120])
ax.legend(loc='best', fontsize=10)
fig.savefig('forma20a_8.svg')


# In[ ]:



