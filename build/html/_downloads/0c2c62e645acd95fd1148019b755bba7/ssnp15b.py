# Square composed of one QUAD4 element

import sys
import salome
import salome_notebook
from salome.smesh import smeshBuilder
import SMESH, SALOMEDS

ExportPATH="/home/banerjee/Salome/ssnp15/"

# Initialize
salome.salome_init()
notebook = salome_notebook.NoteBook()
smesh = smeshBuilder.New()

# Create mesh
mesh = smesh.Mesh()
smesh.SetName(mesh, 'mesh')

# Add nodes
node1 = mesh.AddNode( 0, 0.1, 0 )
node2 = mesh.AddNode( 0.1, 0.1, 0 )
node3 = mesh.AddNode( 0, 0, 0 )
node4 = mesh.AddNode( 0.1, 0, 0 )

# Add groups for the nodes
node_1 = mesh.CreateEmptyGroup( SMESH.NODE, 'node_1' )
node_2 = mesh.CreateEmptyGroup( SMESH.NODE, 'node_2' )
node_3 = mesh.CreateEmptyGroup( SMESH.NODE, 'node_3' )
node_4 = mesh.CreateEmptyGroup( SMESH.NODE, 'node_4' )
nbAdd = node_1.Add([1])
nbAdd = node_2.Add([2])
nbAdd = node_3.Add([3])
nbAdd = node_4.Add([4])

# Add quad element
face1 = mesh.AddFace([3, 4, 2, 1])

# Add group for the face
g_face = mesh.CreateEmptyGroup( SMESH.FACE, 'cube' )
nbAdd = g_face.Add([1])

# Add segment elements
left_seg = mesh.AddEdge([1, 3])
right_seg = mesh.AddEdge([4, 2])
top_seg = mesh.AddEdge([2, 1])
bot_seg = mesh.AddEdge([3, 4])

# Add groups for the segments
g_left = mesh.CreateEmptyGroup( SMESH.EDGE, 'left_edge' )
g_right = mesh.CreateEmptyGroup( SMESH.EDGE, 'right_edge' )
g_top = mesh.CreateEmptyGroup( SMESH.EDGE, 'top_edge' )
g_bot = mesh.CreateEmptyGroup( SMESH.EDGE, 'bot_edge' )
nbAdd = g_left.Add([2])
nbAdd = g_right.Add([3])
nbAdd = g_top.Add([4])
nbAdd = g_bot.Add([5])

# Export mesh
mesh.ExportMED( r''+ExportPATH+'ssnp15b.mmed'+'')

# Update GUI object tree
if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()
