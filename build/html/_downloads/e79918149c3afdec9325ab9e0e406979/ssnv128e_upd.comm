# SSNV128E
DEBUT(LANG = 'EN',
      IGNORE_ALARM=('MODELE1_63'))

# Read mesh
mesh = LIRE_MAILLAGE(VERI_MAIL = _F(VERIF = 'OUI'),
                     FORMAT = 'MED')

# Reorient faces of mesh
mesh = MODI_MAILLAGE(reuse = mesh,
                     MAILLAGE = mesh,
                     ORIE_PEAU_3D = (_F(GROUP_MA = 'SPRESV'),
                                     _F(GROUP_MA = 'SPRESH')))

# Reorient shell normals
mesh = MODI_MAILLAGE(reuse = mesh,
                     MAILLAGE = mesh,
                     ORIE_NORM_COQUE = (_F(GROUP_MA = 'SBATI',
                                           GROUP_NO = 'PBS',
                                           VECT_NORM = (0, 1, 0))))

# Assign 3D elements
model = AFFE_MODELE(MAILLAGE = mesh,
                    #DISTRIBUTION = _F(METHODE = 'CENTRALISE'),
                    AFFE = _F(TOUT = 'OUI',
                              PHENOMENE = 'MECANIQUE',
                              MODELISATION = '3D'))

# Define material
plate = DEFI_MATERIAU(ELAS = _F(E = 1.3E11,
                                NU = 0.2))

# Assign material
mater = AFFE_MATERIAU(MAILLAGE = mesh,
                      AFFE = _F(TOUT = 'OUI',
                                MATER = plate))

# Assign BCs
bc = AFFE_CHAR_MECA(MODELE = model,
                    DDL_IMPO = (_F(GROUP_MA = 'SBATI',  
                                   DX = 0.0, DY = 0.0, DZ = 0.0),
                                _F(GROUP_MA = 'SBLOCX', 
                                   DX = 0.0),
                                _F(GROUP_MA = 'LBLOCY', 
                                   DY = 0.0),
                                _F(GROUP_MA = 'VPLAQ', 
                                   DZ = 0.0),
                                _F(GROUP_MA = 'SPRESV', 
                                   DZ = 0.0)),
                    PRES_REP = (_F(GROUP_MA = 'SPRESV', 
                                   PRES = 5.E07),
                                _F(GROUP_MA = 'SPRESH', 
                                   PRES = 15.E07)))
                  
# Define discrete penalty contact with Coulomb friction
contact = DEFI_CONTACT(MODELE = model,
                       FORMULATION = 'DISCRETE',
                       FROTTEMENT = 'COULOMB',
                       REAC_GEOM = 'CONTROLE',
                       NB_ITER_GEOM = 2,
                       ZONE = _F(GROUP_MA_MAIT = 'SBATI',
                                 GROUP_MA_ESCL = 'SCONTA',
                                 SANS_GROUP_NO  = ('PPS', 'PPSZ'),
                                 APPARIEMENT = 'NODAL',
                                 ALGO_CONT = 'PENALISATION',
                                 COULOMB = 1.0,
                                 ALGO_FROT = 'PENALISATION',
                                 E_N = 1.E14,
                                 E_T = 1.0E11,
                                 COEF_MATR_FROT = 0.4))

# Define ramp function
f_ramp = DEFI_FONCTION(NOM_PARA = 'INST',
                       PROL_GAUCHE = 'LINEAIRE',
                       PROL_DROITE = 'LINEAIRE',
                       VALE = (0.0, 0.0,
                               1.0, 1.0))

# Define time stepping list
l_times = DEFI_LIST_REEL(DEBUT = 0.0,
                         INTERVALLE = _F(JUSQU_A = 1.0, 
                                         NOMBRE = 1))

#-----------------------------------------------------------
# Run simulation (default solver)
result = STAT_NON_LINE(SOLVEUR = _F(),
                       MODELE = model,
                       CHAM_MATER = mater,
                       EXCIT = (_F(CHARGE = bc,
                                   FONC_MULT = f_ramp)),
                       CONTACT = contact,
                       COMPORTEMENT = _F(RELATION = 'ELAS'),
                       NEWTON = _F(REAC_ITER = 1),
                       INCREMENT = _F(LIST_INST = l_times),
                       CONVERGENCE = _F(ARRET = 'OUI',
                                        ITER_GLOB_MAXI = 200,
                                        RESI_GLOB_RELA = 1.0E-5),
                       INFO = 1)

# Compute stress and contact pressure
result = CALC_CHAMP(reuse = result,
                    CONTRAINTE = ('SIGM_ELNO'),
                    VARI_INTERNE = ('VARI_ELNO'),
                    RESULTAT = result)

# Extract nodal displacements
disp = CREA_CHAMP(OPERATION = 'EXTR',
                  TYPE_CHAM = 'NOEU_DEPL_R',
                  NOM_CHAM = 'DEPL',
                  RESULTAT = result,
                  INST = 1.0)

# Write out results
IMPR_RESU(FORMAT = 'MED',
          UNITE = 80,
          RESU = _F(RESULTAT = result))

#-----------------------------------------------------------
# Run simulation (LDLT solver)
result1 = STAT_NON_LINE(SOLVEUR = _F(METHODE = 'LDLT'),
                        MODELE = model,
                        CHAM_MATER = mater,
                        EXCIT = (_F(CHARGE = bc,
                                    FONC_MULT = f_ramp)),
                        CONTACT = contact,
                        COMPORTEMENT = _F(RELATION = 'ELAS'),
                        NEWTON = _F(REAC_ITER = 1),
                        INCREMENT = _F(LIST_INST = l_times),
                        CONVERGENCE = _F(ARRET = 'OUI',
                                         ITER_GLOB_MAXI = 200,
                                         RESI_GLOB_RELA = 1.0E-5),
                        INFO = 1)

# Compute stress and contact pressure
result1 = CALC_CHAMP(reuse = result1,
                     CONTRAINTE = ('SIGM_ELNO'),
                     VARI_INTERNE = ('VARI_ELNO'),
                     RESULTAT = result1)

# Extract nodal displacements
disp1 = CREA_CHAMP(OPERATION = 'EXTR',
                   TYPE_CHAM = 'NOEU_DEPL_R',
                   NOM_CHAM = 'DEPL',
                   RESULTAT = result1,
                   INST = 1.0)

# Write out results
IMPR_RESU(FORMAT = 'MED',
          UNITE = 81,
          RESU = _F(RESULTAT = result1))

#-----------------------------------------------------------
# Verify results

# Result1
TEST_RESU(RESU = (_F(GROUP_NO = 'PPA',
                     INST = 1.0,
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result,
                     NOM_CHAM = 'DEPL',
                     NOM_CMP = 'DX',
                     VALE_CALC =  2.84594696E-05,
                     VALE_REFE = 2.8600000000000001E-05,
                     PRECISION = 0.050000000000000003),
                  _F(GROUP_NO = 'PPB',
                     INST = 1.0,
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result,
                     NOM_CHAM = 'DEPL',
                     NOM_CMP = 'DX',
                     VALE_CALC =  2.70792676E-05,
                     VALE_REFE = 2.72E-05,
                     PRECISION = 0.050000000000000003),
                  _F(GROUP_NO = 'PPC',
                     INST = 1.0,
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result,
                     NOM_CHAM = 'DEPL',
                     NOM_CMP = 'DX',
                     VALE_CALC =  2.27402877E-05,
                     VALE_REFE = 2.2799999999999999E-05,
                     PRECISION = 0.050000000000000003),
                  _F(GROUP_NO = 'PPD',
                     INST = 1.0,
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result,
                     NOM_CHAM = 'DEPL',
                     NOM_CMP = 'DX',
                     VALE_CALC =  1.97271021E-05,
                     VALE_REFE = 1.98E-05,
                     PRECISION = 0.050000000000000003),
                  _F(GROUP_NO = 'PPE',
                     INST = 1.0,
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result,
                     NOM_CHAM = 'DEPL',
                     NOM_CMP = 'DX',
                     VALE_CALC =  1.53641964E-05,
                     VALE_REFE = 1.5E-05,
                     PRECISION = 0.050000000000000003)))

# Result2
TEST_RESU(RESU = (_F(GROUP_NO = 'PPA',
                     INST = 1.0,
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result1,
                     NOM_CHAM = 'DEPL',
                     NOM_CMP = 'DX',
                     VALE_CALC =  2.84594696E-05,
                     VALE_REFE = 2.8600000000000001E-05,
                     PRECISION = 0.050000000000000003),
                  _F(GROUP_NO = 'PPB',
                     INST = 1.0,
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result1,
                     NOM_CHAM = 'DEPL',
                     NOM_CMP = 'DX',
                     VALE_CALC =  2.70792676E-05,
                     VALE_REFE = 2.72E-05,
                     PRECISION = 0.050000000000000003),
                  _F(GROUP_NO = 'PPC',
                     INST = 1.0,
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result1,
                     NOM_CHAM = 'DEPL',
                     NOM_CMP = 'DX',
                     VALE_CALC =  2.27402877E-05,
                     VALE_REFE = 2.2799999999999999E-05,
                     PRECISION = 0.050000000000000003),
                  _F(GROUP_NO = 'PPD',
                     INST = 1.0,
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result1,
                     NOM_CHAM = 'DEPL',
                     NOM_CMP = 'DX',
                     VALE_CALC =  1.97271021E-05,
                     VALE_REFE = 1.98E-05,
                     PRECISION = 0.050000000000000003),
                  _F(GROUP_NO = 'PPE',
                     INST = 1.0,
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result1,
                     NOM_CHAM = 'DEPL',
                     NOM_CMP = 'DX',
                     VALE_CALC =  1.53641964E-05,
                     VALE_REFE = 1.5E-05,
                     PRECISION = 0.050000000000000003)))

# End
FIN()
