# SSNV128Q
DEBUT(LANG = 'EN',
      IGNORE_ALARM=('MODELE1_63'))

# Read mesh
mesh = LIRE_MAILLAGE(VERI_MAIL = _F(VERIF = 'OUI'),
                     FORMAT = 'MED')

# Reorient edges of mesh
mesh = MODI_MAILLAGE(reuse = mesh,
                     MAILLAGE = mesh,
                     ORIE_PEAU_2D = (_F(GROUP_MA = 'LPRESV'),
                                     _F(GROUP_MA = 'LPRESH')))

# Define contact node group
mesh = DEFI_GROUP(reuse = mesh,
                  MAILLAGE = mesh,
                  CREA_GROUP_NO = _F(NOM = 'RELEVE',
                                     GROUP_MA = 'LCONTA',
                                     OPTION = 'NOEUD_ORDO',
                                     GROUP_NO_ORIG = 'PPA',
                                     GROUP_NO_EXTR = 'PPS'))

# Assign plane strain elements to quad mesh
model = AFFE_MODELE(MAILLAGE = mesh,
                    #DISTRIBUTION = _F(METHODE = 'CENTRALISE'),
                    AFFE = _F(TOUT = 'OUI',
                              PHENOMENE = 'MECANIQUE',
                              MODELISATION = 'D_PLAN'))

# Define materials
plate = DEFI_MATERIAU(ELAS = _F(E = 1.3E11,
                                NU = 0.2))

# Assign material
mater = AFFE_MATERIAU(MAILLAGE = mesh,
                      AFFE = _F(TOUT = 'OUI',
                                MATER = plate))

# Assign BCs
bc = AFFE_CHAR_MECA(MODELE = model,
                    DDL_IMPO = (_F(GROUP_MA = 'LBATI',  
                                   DX = 0.0, DY = 0.0),
                                _F(GROUP_NO = 'PPS', 
                                   DX = 0.0),
                                _F(GROUP_MA = 'LBLOCX', 
                                   DX = 0.0)),
                    PRES_REP = (_F(GROUP_MA = 'LPRESV', 
                                   PRES = 5.E07),
                                _F(GROUP_MA = 'LPRESH', 
                                   PRES = 15.E07)))
                  
# Define continuous contact (newton) with Coulomb friction
contact = DEFI_CONTACT(MODELE = model,
                       FORMULATION = 'CONTINUE',
                       FROTTEMENT = 'COULOMB',
                       ALGO_RESO_CONT = 'NEWTON',
                       ALGO_RESO_GEOM = 'NEWTON',
                       ALGO_RESO_FROT = 'NEWTON',
                       RESI_FROT = 1.0E-6,
                       ZONE = _F(GROUP_MA_MAIT = 'LBATI',
                                 GROUP_MA_ESCL = 'LCONTA',
                                 SANS_GROUP_NO_FR = 'PPS',
                                 ALGO_CONT = 'STANDARD',
                                 COEF_CONT = 1.0,
                                 COULOMB = 1.0,
                                 ALGO_FROT = 'STANDARD',
                                 COEF_FROT = 1.0,
                                 CONTACT_INIT = 'INTERPENETRE',
                                 SEUIL_INIT = 1.0E-1,
                                 ADAPTATION = 'ADAPT_COEF'))

# Define ramp function
f_ramp = DEFI_FONCTION(NOM_PARA = 'INST',
                       PROL_GAUCHE = 'LINEAIRE',
                       PROL_DROITE = 'LINEAIRE',
                       VALE = (0.0, 0.0,
                               1.0, 1.0))

# Define time stepping list of reals
l_times = DEFI_LIST_REEL(DEBUT = 0.0,
                         INTERVALLE = _F(JUSQU_A = 1.0, 
                                         NOMBRE = 1))

#-----------------------------------------------------------
# Run simulation (default solver)
result = STAT_NON_LINE(SOLVEUR = _F(),
                       MODELE = model,
                       CHAM_MATER = mater,
                       EXCIT = (_F(CHARGE = bc,
                                   FONC_MULT = f_ramp)),
                       CONTACT = contact,
                       COMPORTEMENT = _F(RELATION = 'ELAS'),
                       NEWTON = _F(REAC_ITER = 1),
                       INCREMENT = _F(LIST_INST = l_times),
                       CONVERGENCE = _F(ARRET = 'OUI',
                                        ITER_GLOB_MAXI = 200,
                                        RESI_GLOB_RELA = 1.0E-5),
                       MESURE = _F(TABLE = 'OUI',
                                   UNITE = 50),
                       AFFICHAGE = _F(INFO_TEMPS = 'OUI'),
                       INFO = 2,
                       INFO_DBG = 'MECANONLINE')

# Compute stress and contact pressure
result = CALC_CHAMP(reuse = result,
                    CONTRAINTE = ('SIGM_ELNO'),
                    VARI_INTERNE = ('VARI_ELNO'),
                    RESULTAT = result)

# Extract nodal displacements
disp = CREA_CHAMP(OPERATION = 'EXTR',
                  TYPE_CHAM = 'NOEU_DEPL_R',
                  NOM_CHAM = 'DEPL',
                  RESULTAT = result,
                  INST = 1.0)

# Write out results
IMPR_RESU(FORMAT = 'MED',
          UNITE = 80,
          RESU = _F(RESULTAT = result))

#-----------------------------------------------------------
# Verify results

# Result1
TEST_RESU(RESU = (_F(GROUP_NO = 'PPR',
                     INST = 1.0,
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result,
                     NOM_CHAM = 'CONT_NOEU',
                     NOM_CMP = 'JEU',
                     VALE_CALC =  2.151491063319E-34,
                     VALE_REFE = 0.0,
                     CRITERE = 'ABSOLU',
                     PRECISION = 1.E-12,
                    ),
                  _F(GROUP_NO = 'PPR',
                     INST = 1.0,
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result,
                     NOM_CHAM = 'CONT_NOEU',
                     NOM_CMP = 'CONT',
                     VALE_CALC = 1.000000000,
                     VALE_REFE = 1.0,
                     CRITERE = 'RELATIF',
                     PRECISION = 1.0000000000000001E-05,
                    ),
                  _F(GROUP_NO = 'PPR',
                     INST = 1.0,
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result,
                     NOM_CHAM = 'CONT_NOEU',
                     NOM_CMP = 'RN',
                     VALE_CALC =  1.04863768E+05,
                     VALE_REFE = 1.04864E5,
                     CRITERE = 'RELATIF',
                     PRECISION = 1.0000000000000001E-05,
                     ),
                  _F(GROUP_NO = 'PPA',
                     INST = 1.0,
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result,
                     NOM_CHAM = 'DEPL',
                     NOM_CMP = 'DX',
                     VALE_CALC =  2.84594384E-05,
                     VALE_REFE = 2.8600000000000001E-05,
                     PRECISION = 0.050000000000000003),
                  _F(GROUP_NO = 'PPB',
                     INST = 1.0,
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result,
                     NOM_CHAM = 'DEPL',
                     NOM_CMP = 'DX',
                     VALE_CALC =  2.70792364E-05,
                     VALE_REFE = 2.72E-05,
                     PRECISION = 0.050000000000000003),
                  _F(GROUP_NO = 'PPC',
                     INST = 1.0,
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result,
                     NOM_CHAM = 'DEPL',
                     NOM_CMP = 'DX',
                     VALE_CALC =  2.27402555E-05,
                     VALE_REFE = 2.2799999999999999E-05,
                     PRECISION = 0.050000000000000003),
                  _F(GROUP_NO = 'PPD',
                     INST = 1.0,
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result,
                     NOM_CHAM = 'DEPL',
                     NOM_CMP = 'DX',
                     VALE_CALC =  1.97270669E-05,
                     VALE_REFE = 1.98E-05,
                     PRECISION = 0.050000000000000003),
                  _F(GROUP_NO = 'PPE',
                     INST = 1.0,
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result,
                     NOM_CHAM = 'DEPL',
                     NOM_CMP = 'DX',
                     VALE_CALC =  1.53641594E-05,
                     VALE_REFE = 1.5E-05,
                     PRECISION = 0.050000000000000003)))

# End
FIN()
