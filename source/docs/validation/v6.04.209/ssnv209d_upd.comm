# SSNV209D: 2D, ELEMENTS TRIA3, METHOD XFEM

DEBUT(LANG = 'EN',
      IGNORE_ALARM = 'MODELE1_63')

# Define contact constraint parameters
H_plus = -2.

# Define material properties
E = 1.3E11
nu = 0.2
rho = 7800.

# Read mesh
mesh = LIRE_MAILLAGE(FORMAT = 'MED')

# Define node groups for the 0D elements
mesh = DEFI_GROUP(reuse = mesh,
                  MAILLAGE = mesh,
                  CREA_GROUP_NO =  (_F(GROUP_MA = ('p1', 'p2', 'p3', 'p4', 'ptop'))))

# Define node groups from some element groups and an element group
mesh = DEFI_GROUP(reuse = mesh,
                  MAILLAGE = mesh,
                  CREA_GROUP_NO =  (_F(GROUP_MA = 'surf'),
                                    _F(NOM = 'line1',
                                       GROUP_MA = 'line1'),
                                    _F(NOM = 'base',
                                       OPTION = 'PLAN',
                                       GROUP_NO_CENTRE = 'p1',
                                       VECT_NORMALE =  (0., 1.),
                                       PRECISION = 0.009)),
                  CREA_GROUP_MA =  (_F(NOM = 'plate',
                                       OPTION = 'BANDE',
                                       GROUP_NO_CENTRE = 'p4',
                                       VECT_NORMALE =  (0., 1.),
                                       DIST = 0.0399)),
                  INFO = 1)

# Define node groups for the reference point and the crack
mesh = DEFI_GROUP(reuse  = mesh,
                  MAILLAGE = mesh,
                  CREA_GROUP_NO = (_F(NOM = 'pPR',
                                      OPTION = 'ENV_SPHERE',
                                      POINT = (0.03875, 0.),
                                      RAYON = 0.000001,
                                      PRECISION = 0.00001),
                                   _F(NOM = 'crack',
                                      OPTION = 'PLAN',
                                      POINT = (0., 0.),
                                      VECT_NORMALE = (0., 1.),
                                      PRECISION = 1E-7)),
                  INFO = 1)

# Change the face orientations if needed
mesh = MODI_MAILLAGE(reuse = mesh,
                     MAILLAGE = mesh,
                     ORIE_PEAU_2D = (_F(GROUP_MA = 'line2'),
                                     _F(GROUP_MA = 'line3'),
                                     _F(GROUP_MA = 'line4')))

# Assign plane strain model
model_in = AFFE_MODELE(MAILLAGE = mesh,
                       AFFE = (_F(GROUP_MA = ('line3'),
                                  PHENOMENE = 'MECANIQUE',
                                  MODELISATION = 'D_PLAN'),
                               _F(GROUP_MA = ('line1'),
                                  PHENOMENE = 'MECANIQUE',
                                  MODELISATION = 'D_PLAN'),
                               _F(GROUP_MA = ('surf'),
                                  PHENOMENE = 'MECANIQUE',
                                  MODELISATION = 'D_PLAN'),
                               _F(GROUP_MA = ('line2'),
                                  PHENOMENE = 'MECANIQUE',
                                  MODELISATION = 'D_PLAN'),
                               _F(GROUP_MA = ('line4'),
                                  PHENOMENE = 'MECANIQUE',
                                  MODELISATION = 'D_PLAN')),
                       INFO = 1)

# Create the X-FEM crack
f_crack = FORMULE(VALE = 'Y',
                  NOM_PARA = ['X', 'Y'])

crack = DEFI_FISS_XFEM(MAILLAGE = mesh,
                       TYPE_DISCONTINUITE = 'INTERFACE',
                       DEFI_FISS = _F(FONC_LN = f_crack),
                       INFO = 1)

# Add X-FEM model
model_xf = MODI_MODELE_XFEM(MODELE_IN = model_in,
                            FISSURE = crack,
                            CONTACT = 'STANDARD',
                            INFO = 1)

# Create crack node number field
crac_fld = CREA_CHAMP(TYPE_CHAM = 'NOEU_NEUT_R',
                      OPERATION = 'EXTR',
                      FISSURE = crack,
                      NOM_CHAM = 'LNNO')


# Define material
f_nu = DEFI_FONCTION(NOM_PARA = 'NEUT1',
                     VALE = (-1.0,   0.0,
                             -1.E-9, 0.0,
                              1.E-9, nu,
                              1.0,   nu))
f_E = DEFI_FONCTION(NOM_PARA = 'NEUT1',
                    VALE = (-1.0,   1.E16,
                            -1.E-9, 1.E16,
                             1.E-9, E,
                             1.0,   E))
f_rho = DEFI_CONSTANTE(VALE = rho)

mat_data = DEFI_MATERIAU(ELAS_FO = _F(E = f_E,
                                      NU = f_nu,
                                      RHO = f_rho))

# Assign material
mater = AFFE_MATERIAU(MAILLAGE = mesh,
                      MODELE = model_xf,
                      AFFE = (_F(GROUP_MA = ('surf'),
                                 MATER = mat_data)),
                      AFFE_VARC = _F(NOM_VARC = 'NEUT1',
                                     CHAM_GD = crac_fld))

# Define contact BCs
contact = DEFI_CONTACT(MODELE = model_xf,
                       FORMULATION = 'XFEM',
                       FROTTEMENT = 'COULOMB',
                       ITER_CONT_MAXI = 4,
                       ITER_FROT_MAXI = 20,
                       REAC_GEOM = 'SANS',
                       ELIM_ARETE     = 'ELIM',
                       ZONE = (_F(FISS_MAIT = crack,
                                  INTEGRATION = 'NCOTES',
                                  CONTACT_INIT = 'OUI',
                                  COULOMB = 1.0,
                                  ALGO_CONT = 'STANDARD',
                                  COEF_CONT = 100.,
                                  ALGO_FROT = 'STANDARD',
                                  COEF_FROT = 100.)))

# Define kinematic BCs
bc_kin = AFFE_CHAR_MECA(MODELE = model_xf,
                        DDL_IMPO = (_F(GROUP_NO = 'line1',
                                       DX = 0.,
                                       DY = 0.)),
                        LIAISON_GROUP = (_F(GROUP_NO_1 = ('crack'), # --- DX ---
                                            GROUP_NO_2 = ('crack'),
                                            DDL_1 = ('DX', 'H1X'),
                                            DDL_2 = ('DX', 'H1X'),
                                            COEF_MULT_1 = (0., 0.),
                                            COEF_MULT_2 = (1., H_plus),
                                            COEF_IMPO =  0.),
                                         _F(GROUP_NO_1 = ('crack'), # --- DY ---
                                            GROUP_NO_2 = ('crack'),
                                            DDL_1 = ('DY', 'H1Y'),
                                            DDL_2 = ('DY', 'H1Y'),
                                            COEF_MULT_1 = (0., 0.),
                                            COEF_MULT_2 = (1., H_plus),
                                            COEF_IMPO =  0.)))

# Define constant pressure BCs
bc_c = AFFE_CHAR_MECA(MODELE = model_xf,
                      PRES_REP = (_F(GROUP_MA = 'line3',
                                     PRES = 5.E7)))

# Define y-varying pressure BCs
def pressure(y) :
   if y  < 1e-15:  return 0.E07
   if y  > 1e-15:  return 15.E7
   if y ==  1e-15:  return 0.

f_pres = FORMULE(VALE = 'pressure(Y)',
                 pressure = pressure,
                 NOM_PARA = ['X', 'Y'])

bc_v = AFFE_CHAR_MECA_F(MODELE = model_xf,
                        PRES_REP = _F(GROUP_MA = ('line2','line4'),
                                      PRES = f_pres))


# Define ramp function for time stepping
ramp = DEFI_FONCTION(NOM_PARA = 'INST',
                     PROL_GAUCHE = 'LINEAIRE',
                     PROL_DROITE = 'LINEAIRE',
                     VALE = (0.0, 0.0,
                             1.0, 1.0))

# Define time steps
l_times = DEFI_LIST_REEL(DEBUT = 0.0,
                         INTERVALLE = _F(JUSQU_A = 1.0,
                                         NOMBRE = 1))

# Solve
result = STAT_NON_LINE(MODELE = model_xf,
                       CHAM_MATER = mater,
                       EXCIT = (_F(CHARGE = bc_kin),
                                _F(CHARGE = bc_c,
                                   FONC_MULT = ramp),
                                _F(CHARGE = bc_v,
                                   FONC_MULT = ramp)),
                       CONTACT  = contact,
                       COMPORTEMENT = _F(RELATION = 'ELAS',
                                         GROUP_MA = 'surf'),
                       INCREMENT = _F(LIST_INST = l_times,
                                      INST_FIN = 1.0),
                       CONVERGENCE = _F(ARRET = 'OUI',
                                        ITER_GLOB_MAXI = 40,
                                        RESI_GLOB_RELA = 1.E-6),
                       SOLVEUR = _F(METHODE = 'MUMPS',
                                    NPREC = -1),
                       NEWTON = _F(REAC_ITER = 1),
                       ARCHIVAGE = _F(CHAM_EXCLU = 'VARI_ELGA'),
                       INFO = 1)

# Post-process for visualization
mesh_xfe = POST_MAIL_XFEM(MODELE = model_xf)

mesh_xfe = DEFI_GROUP(reuse = mesh_xfe,
                      MAILLAGE = mesh_xfe,
                      DETR_GROUP_NO = _F(NOM = 'crack'),
                      CREA_GROUP_NO = (_F(OPTION = 'ENV_SPHERE',
                                          NOM = 'pPA',
                                          POINT = (0.,0.),
                                          RAYON = 0.000001,
                                          PRECISION = 0.00001),
                                       _F(OPTION = 'ENV_SPHERE',
                                          NOM = 'pPB',
                                          POINT = (0.00125,0.),
                                          RAYON = 0.000001,
                                          PRECISION = 0.00001),
                                       _F(OPTION = 'ENV_SPHERE',
                                          NOM = 'pPC',
                                          POINT = (0.005,0.),
                                          RAYON = 0.000001,
                                          PRECISION = 0.00001),
                                       _F(OPTION = 'ENV_SPHERE',
                                          NOM = 'pPD',
                                          POINT = (0.0075,0.),
                                          RAYON = 0.000001,
                                          PRECISION = 0.00001),
                                       _F(OPTION = 'ENV_SPHERE',
                                          NOM = 'pPE',
                                          POINT = (0.01125,0.),
                                          RAYON = 0.000001,
                                          PRECISION = 0.00001),
                                       _F(NOM = 'crack',
                                          OPTION='PLAN',
                                          POINT=(0.,0.),
                                          VECT_NORMALE=(0.,1.),
                                          PRECISION=1.E-6)),
                      INFO = 1)

mod_viz = AFFE_MODELE(MAILLAGE = mesh_xfe,
                      AFFE = _F(TOUT = 'OUI',
                                PHENOMENE = 'MECANIQUE',
                                MODELISATION = 'D_PLAN'))

resu_xfe = POST_CHAM_XFEM(MODELE_VISU = mod_viz,
                          RESULTAT = result)

# Save the mesh for visualization
IMPR_RESU(UNITE = 80,
          FORMAT = 'MED',
          RESU = _F(MAILLAGE = mesh_xfe))

IMPR_RESU(UNITE = 81,
          FORMAT = 'MED',
          RESU = _F(RESULTAT = resu_xfe))

# Extract crack displacement into table (POST_MAIL_XFEM creates NFISSU for crack)
u_crack = POST_RELEVE_T(ACTION = _F(INTITULE = 'DEPLE',
                                    GROUP_NO = ('NFISSU'),
                                    RESULTAT = resu_xfe,
                                    NOM_CHAM = 'DEPL',
                                    INST = 1.0,
                                    TOUT_CMP = 'OUI',
                                    OPERATION = 'EXTRACTION'))

u_crack = CALC_TABLE(reuse = u_crack,
                     TABLE = u_crack,
                     ACTION = (_F(OPERATION = 'FILTRE',
                                  NOM_PARA  = 'NOEUD',
                                  CRIT_COMP = 'REGEXP',
                                  VALE_K    = 'N[^M]')))

u_crack = CALC_TABLE(reuse = u_crack,
                     TABLE = u_crack,
                     ACTION = _F(OPERATION = 'TRI',
                                 NOM_PARA = 'COOR_X',
                                 ORDRE = 'CROISSANT'))

# Write table
IMPR_TABLE(UNITE = 8,
           TABLE = u_crack,
           SEPARATEUR = ',')

# Extract contact status into table
cont_R = POST_RELEVE_T(ACTION = _F(INTITULE = 'CONT_NOEU',
                                   GROUP_NO = ('pPR'),
                                   RESULTAT = result,
                                   NOM_CHAM = 'CONT_NOEU',
                                   INST = 1.0,
                                   TOUT_CMP = 'OUI',
                                   OPERATION = 'EXTRACTION'))

cont_R = CALC_TABLE(reuse = cont_R,
                    TABLE = cont_R,
                    ACTION = (_F(OPERATION = 'FILTRE',
                                 NOM_PARA  = 'NOEUD',
                                 CRIT_COMP = 'REGEXP',
                                 VALE_K    = 'N[^M]')))

# Write table
IMPR_TABLE(UNITE = 8,
           TABLE = cont_R,
           SEPARATEUR = ',')

# Extract displacement and contact pressure at A into table
u_A = POST_RELEVE_T(ACTION = _F(INTITULE = 'u_A',
                                GROUP_NO = ('pPA'),
                                RESULTAT = resu_xfe,
                                NOM_CHAM = 'DEPL',
                                INST = 1.0,
                                TOUT_CMP = 'OUI',
                                OPERATION = 'EXTRACTION'))

u_A = CALC_TABLE(reuse = u_A,
                 TABLE = u_A,
                 ACTION = (_F(OPERATION = 'FILTRE',
                              NOM_PARA  = 'NOEUD',
                              CRIT_COMP = 'REGEXP',
                              VALE_K    = 'N[^M]')))

# Extract displacement and contact pressure at B into table
u_B = POST_RELEVE_T(ACTION = _F(INTITULE = 'u_B',
                                GROUP_NO = ('pPB'),
                                RESULTAT = resu_xfe,
                                NOM_CHAM = 'DEPL',
                                INST = 1.0,
                                TOUT_CMP = 'OUI',
                                OPERATION = 'EXTRACTION'))

u_B = CALC_TABLE(reuse = u_B,
                 TABLE = u_B,
                 ACTION = (_F(OPERATION = 'FILTRE',
                              NOM_PARA  = 'NOEUD',
                              CRIT_COMP = 'REGEXP',
                              VALE_K    = 'N[^M]')))

# Extract displacement and contact pressure at C into table
u_C = POST_RELEVE_T(ACTION = _F(INTITULE = 'u_C',
                                GROUP_NO = ('pPC'),
                                RESULTAT = resu_xfe,
                                NOM_CHAM = 'DEPL',
                                INST = 1.0,
                                TOUT_CMP = 'OUI',
                                OPERATION = 'EXTRACTION'))

u_C = CALC_TABLE(reuse = u_C,
                 TABLE = u_C,
                 ACTION = (_F(OPERATION = 'FILTRE',
                              NOM_PARA  = 'NOEUD',
                              CRIT_COMP = 'REGEXP',
                              VALE_K    = 'N[^M]')))

# Extract displacement and contact pressure at D into table
u_D = POST_RELEVE_T(ACTION = _F(INTITULE = 'u_D',
                                GROUP_NO = ('pPD'),
                                RESULTAT = resu_xfe,
                                NOM_CHAM = 'DEPL',
                                INST = 1.0,
                                TOUT_CMP = 'OUI',
                                OPERATION = 'EXTRACTION'))

u_D = CALC_TABLE(reuse = u_D,
                 TABLE = u_D,
                 ACTION = (_F(OPERATION = 'FILTRE',
                              NOM_PARA  = 'NOEUD',
                              CRIT_COMP = 'REGEXP',
                              VALE_K    = 'N[^M]')))

# Extract displacement and contact pressure at E into table
u_E = POST_RELEVE_T(ACTION = _F(INTITULE = 'u_E',
                                GROUP_NO = ('pPE'),
                                RESULTAT = resu_xfe,
                                NOM_CHAM = 'DEPL',
                                INST = 1.0,
                                TOUT_CMP = 'OUI',
                                OPERATION = 'EXTRACTION'))

u_E = CALC_TABLE(reuse = u_E,
                 TABLE = u_E,
                 ACTION = (_F(OPERATION = 'FILTRE',
                              NOM_PARA  = 'NOEUD',
                              CRIT_COMP = 'REGEXP',
                              VALE_K    = 'N[^M]')))

#----------------------------------------------
# Verification tests
#----------------------------------------------
TEST_TABLE(CRITERE = 'ABSOLU',
           REFERENCE = 'AUTRE_ASTER',
           PRECISION = 9.9999999999999998E-13,
           VALE_CALC = 0.,
           VALE_REFE = 0.0,
           NOM_PARA = 'JEU',
           TYPE_TEST = 'MAX',
           TABLE = cont_R)

TEST_TABLE(CRITERE = 'RELATIF',
           REFERENCE = 'AUTRE_ASTER',
           PRECISION = 0.10000000000000001,
           VALE_CALC = 96528.5506724,
           VALE_REFE = 1.048637688477E5,
           NOM_PARA = 'RN',
           TYPE_TEST = 'MAX',
           TABLE = cont_R)

TEST_TABLE(CRITERE = 'RELATIF',
           REFERENCE = 'AUTRE_ASTER',
           PRECISION = 1.E-2,
           VALE_CALC = 2.84874012245E-05,
           VALE_REFE = 2.84594384304E-05,
           NOM_PARA = 'DX',
           TYPE_TEST = 'MAX',
           TABLE = u_A)

TEST_TABLE(CRITERE = 'RELATIF',
           REFERENCE = 'AUTRE_ASTER',
           PRECISION = 1.E-2,
           VALE_CALC = 2.71147418354E-05,
           VALE_REFE = 2.70792364103E-05,
           NOM_PARA = 'DX',
           TYPE_TEST = 'MAX',
           TABLE = u_B)

TEST_TABLE(CRITERE = 'RELATIF',
           REFERENCE = 'AUTRE_ASTER',
           PRECISION = 1.E-2,
           VALE_CALC = 2.27673455717E-05,
           VALE_REFE = 2.27402555462E-05,
           NOM_PARA = 'DX',
           TYPE_TEST = 'MAX',
           TABLE = u_C)

TEST_TABLE(CRITERE = 'RELATIF',
           REFERENCE = 'AUTRE_ASTER',
           PRECISION = 1.E-2,
           VALE_CALC = 1.97480934002E-05,
           VALE_REFE = 1.97270669318E-05,
           NOM_PARA = 'DX',
           TYPE_TEST = 'MAX',
           TABLE = u_D)

TEST_TABLE(CRITERE = 'RELATIF',
           REFERENCE = 'AUTRE_ASTER',
           PRECISION = 1.E-2,
           VALE_CALC = 1.53717902207E-05,
           VALE_REFE = 1.53641594362E-05,
           NOM_PARA = 'DX',
           TYPE_TEST = 'MAX',
           TABLE = u_E)

# End
FIN()
