# Start
DEBUT()

# Read the mesh
mesh = LIRE_MAILLAGE(FORMAT='MED')

# Change edge orientation for load application
mesh = MODI_MAILLAGE(reuse = mesh,
                     MAILLAGE = mesh,
                     ORIE_PEAU_2D = _F(GROUP_MA = 'top'))

# Assign plane stress elements to mesh
model = AFFE_MODELE(MAILLAGE = mesh,
                    AFFE = _F(TOUT = 'OUI',
                              PHENOMENE = 'MECANIQUE',
                              MODELISATION = 'C_PLAN'))

# Read the stress-strain curve from file
sig_eps = LIRE_FONCTION(UNITE = 21,
                        NOM_PARA = 'EPSI',
                        PROL_DROITE = 'CONSTANT')

# Write the function in XMGrace format
IMPR_FONCTION(FORMAT = 'XMGRACE',
              UNITE = 29,
              COURBE = _F(FONCTION = sig_eps),
              TITRE = 'Traction curve',
              LEGENDE_X = 'Strain',
              LEGENDE_Y = 'Stress (MPa)');

# Define material
steel = DEFI_MATERIAU(ELAS = _F(E = 200000.0,
                                NU = 0.3),
                      TRACTION = _F(SIGM = sig_eps))

# Assign material
mater = AFFE_MATERIAU(MAILLAGE = mesh,
                      AFFE = _F(TOUT = 'OUI',
                                MATER = steel))

# Symmetry BCs
symm_bc = AFFE_CHAR_CINE(MODELE = model,
                         MECA_IMPO = (_F(GROUP_MA = 'bottom',
                                         DY = 0.0),
                                      _F(GROUP_MA = 'left',
                                         DX = 0.0)))

# Force BC
load_bc = AFFE_CHAR_MECA(MODELE = model,
                         FORCE_CONTOUR = _F(GROUP_MA = 'top',
                                            FY = 1.0))

# Ramp function for load
ramp = DEFI_FONCTION(NOM_PARA = 'INST',
                     VALE = (   0.0,    0.0,
                             1000.0, 1000.0))

# Time discretization
t_elas = 10.0
t_elas_plas = 230.0
time = DEFI_LIST_REEL(DEBUT = 0.0,
                      INTERVALLE = (_F(JUSQU_A = t_elas,
                                       NOMBRE = 1),
                                    _F(JUSQU_A = t_elas_plas,
                                       NOMBRE = 50)))

l_time = DEFI_LIST_INST(DEFI_LIST = _F(LIST_INST = time),
                        ECHEC = _F(EVENEMENT = 'ERREUR',
                                   SUBD_NIVEAU = 5,
                                   ACTION = 'DECOUPE'))

#------------------------------------------
# Elastic loading
#------------------------------------------
# Compute static nonlinear (elastic)
result = STAT_NON_LINE(MODELE = model,
                       CHAM_MATER = mater,
                       EXCIT = (_F(CHARGE = symm_bc),
                                _F(CHARGE = load_bc,
                                   FONC_MULT = ramp)),
                       COMPORTEMENT = _F(RELATION = 'VMIS_ISOT_TRAC'),
                       INCREMENT = _F(LIST_INST = l_time, 
                                      INST_FIN = t_elas))

#------------------------------------------
# Plastic loading
#------------------------------------------
# Compute static nonlinear (plastic)
result = STAT_NON_LINE(reuse = result,
                       MODELE = model,
                       CHAM_MATER = mater,
                       EXCIT = (_F(CHARGE = symm_bc),
                                _F(CHARGE = load_bc,
                                   FONC_MULT = ramp)),
                       COMPORTEMENT = _F(RELATION = 'VMIS_ISOT_TRAC'),
                       ETAT_INIT = _F(EVOL_NOLI = result),
                       INCREMENT = _F(LIST_INST = l_time, 
                                      INST_FIN = t_elas_plas),
                       NEWTON = _F(REAC_ITER=1))

# Interpolate the stresses to the nodes
result = CALC_CHAMP(reuse = result,
                    RESULTAT = result,
                    CONTRAINTE = ('SIGM_ELNO', 'SIGM_NOEU'))

# We will extract the equivalent stresses in the second stage of
# calculaion
# Interpolate the equivalent stresses to the nodes
#result = CALC_CHAMP(reuse = result,
#                    RESULTAT = result,
#                    CRITERES = ('SIEQ_ELGA', 'SIEQ_ELNO', 'SIEQ_NOEU'))

# Interpolate the internal variables to the nodes
result = CALC_CHAMP(reuse = result,
                    RESULTAT = result,
                    VARI_INTERNE = ('VARI_ELNO', 'VARI_NOEU'))

# Write the results
IMPR_RESU(FORMAT = 'MED',
          RESU = _F(RESULTAT = result))


#------------------------------------------
# Plastic loading then elastic unloading
#------------------------------------------
t_load = 230.0
t_unload = 300.0

# Ramp function for load-unload
ramp2 = DEFI_FONCTION(NOM_PARA = 'INST',
                      VALE = (      0.0,    0.0,
                                 t_load, t_load,
                               t_unload,    0.0))

# Time discretization
time2 = DEFI_LIST_REEL(DEBUT = 0.0,
                       INTERVALLE = (_F(JUSQU_A = t_elas,
                                        NOMBRE = 1),
                                     _F(JUSQU_A = t_load,
                                        NOMBRE = 30),
                                     _F(JUSQU_A = t_unload,
                                        NOMBRE = 10)))

l_time2 = DEFI_LIST_INST(DEFI_LIST = _F(LIST_INST = time2),
                         ECHEC = _F(EVENEMENT = 'ERREUR',
                                    ACTION = 'DECOUPE'))

# Compute static nonlinear (elastic unloading)
result2 = STAT_NON_LINE(MODELE = model,
                        CHAM_MATER = mater,
                        EXCIT = (_F(CHARGE = symm_bc),
                                 _F(CHARGE = load_bc,
                                    FONC_MULT = ramp2)),
                        COMPORTEMENT = _F(RELATION = 'VMIS_ISOT_TRAC'),
                        INCREMENT = _F(LIST_INST = l_time2), 
                        NEWTON = _F(PREDICTION = 'ELASTIQUE',
                                    REAC_ITER = 1),
                        ARCHIVAGE = _F(LIST_INST = time2))

# Interpolate the stresses to the nodes
result2 = CALC_CHAMP(reuse = result2,
                     RESULTAT = result2,
                     CONTRAINTE = ('SIGM_ELNO', 'SIGM_NOEU'))

# We will extract the equivalent stresses in the second stage of
# calculaion
# Interpolate the equivalent stresses to the nodes
#result2 = CALC_CHAMP(reuse = result2,
#                     RESULTAT = result2,
#                     CRITERES = ('SIEQ_ELGA', 'SIEQ_ELNO', 'SIEQ_NOEU'))

# Interpolate the internal variables to the nodes
result2 = CALC_CHAMP(reuse = result2,
                     RESULTAT = result2,
                      VARI_INTERNE = ('VARI_ELNO', 'VARI_NOEU'))

# Write the results
IMPR_RESU(FORMAT = 'MED',
          RESU = _F(RESULTAT = result2))

#------------------------------
# Verification tests : Elastic
#------------------------------
# Verification test 1 (sig_yy at point B)
TEST_RESU(RESU = _F(INST = 10.0,
                    GROUP_NO = 'B',
                    REFERENCE = 'AUTRE_ASTER',
                    RESULTAT = result,
                    NOM_CHAM = 'SIGM_NOEU',
                    NOM_CMP = 'SIYY',
                    VALE_CALC = 30.6077958737,
                    VALE_REFE = 30.6077958737,
                    CRITERE = 'RELATIF',
                    PRECISION = 1.E-08))

# Verification test 2 (sig_xx at point A)
TEST_RESU(RESU = _F(INST = 10.0,
                    GROUP_NO = 'A',
                    REFERENCE = 'AUTRE_ASTER',
                    RESULTAT = result,
                    NOM_CHAM = 'SIGM_NOEU',
                    NOM_CMP = 'SIXX',
                    VALE_CALC = -9.9967257536,
                    VALE_REFE = -9.9967257536,
                    CRITERE = 'RELATIF',
                    PRECISION = 1.E-08))

#------------------------------
# Verification tests : Plastic
#------------------------------
# Verification test 1 (sig_yy at point B)
TEST_RESU(RESU = _F(INST = 230.,
                    GROUP_NO = 'B',
                    RESULTAT = result,
                    NOM_CHAM = 'SIGM_NOEU',
                    NOM_CMP = 'SIYY',
                    VALE_CALC = 271.642151153,
                    CRITERE = 'RELATIF'))

# Verification test 2 (sig_xx at point A)
TEST_RESU(RESU = _F(INST = 230.,
                    GROUP_NO = 'A',
                    RESULTAT = result,
                    NOM_CHAM = 'SIGM_NOEU',
                    NOM_CMP = 'SIXX',
                    VALE_CALC = -213.06249364,
                    CRITERE = 'RELATIF'))

# Verification test 3 (int_var 1 at point B)
TEST_RESU(RESU = _F(INST = 230.,
                    GROUP_NO = 'B',
                    RESULTAT = result,
                    NOM_CHAM = 'VARI_NOEU',
                    NOM_CMP = 'V1',
                    VALE_CALC = 0.3251779025,
                    CRITERE = 'RELATIF'))

# Verification test 4 (int_var 2 at point B)
TEST_RESU(RESU = _F(INST = 230.,
                    GROUP_NO = 'B',
                    RESULTAT = result,
                    NOM_CHAM = 'VARI_NOEU',
                    NOM_CMP = 'V2',
                    VALE_CALC = 1.0,
                    CRITERE = 'RELATIF'))

# Verification test 5 (int_var 1 at point A)
TEST_RESU(RESU = _F(INST = 230.,
                    GROUP_NO = 'A',
                    RESULTAT = result,
                    NOM_CHAM = 'VARI_NOEU',
                    NOM_CMP = 'V1',
                    VALE_CALC = 0.00369151195696,
                    CRITERE = 'RELATIF'))

# Verification test 6 (int_var 2 at point A)
TEST_RESU(RESU = _F(INST = 230.,
                    GROUP_NO = 'A',
                    RESULTAT = result,
                    NOM_CHAM = 'VARI_NOEU',
                    NOM_CMP = 'V2',
                    VALE_CALC = 1.0,
                    CRITERE = 'RELATIF'))

#---------------------------------------
# Verification tests : Elastic unloading
#---------------------------------------
# Verification test 1 (sig_yy at point B)
TEST_RESU(RESU = _F(INST = 300.,
                    GROUP_NO = 'B',
                    REFERENCE='ANALYTIQUE',
                    RESULTAT = result2,
                    NOM_CHAM = 'SIGM_NOEU',
                    NOM_CMP = 'SIYY',
                    VALE_CALC = -271.777841344,
                    VALE_REFE = -271.777841344,
                    CRITERE = 'RELATIF',
                    PRECISION=1.E-08))

# Verification test 2 (sig_xx at point A)
TEST_RESU(RESU = _F(INST = 300.,
                    GROUP_NO = 'A',
                    REFERENCE='ANALYTIQUE',
                    RESULTAT = result2,
                    NOM_CHAM = 'SIGM_NOEU',
                    NOM_CMP = 'SIXX',
                    VALE_CALC = 23.8828857921,
                    VALE_REFE = 23.8828857921,
                    CRITERE = 'RELATIF',
                    PRECISION=1.E-08))

# Verification test 3 (int_var 1 at point B)
TEST_RESU(RESU = _F(INST = 300.,
                    GROUP_NO = 'B',
                    RESULTAT = result2,
                    NOM_CHAM = 'VARI_NOEU',
                    NOM_CMP = 'V1',
                    VALE_CALC = 0.327601333404,
                    CRITERE = 'RELATIF'))

# Verification test 4 (int_var 2 at point B)
TEST_RESU(RESU = _F(INST = 300.,
                    GROUP_NO = 'B',
                    RESULTAT = result2,
                    NOM_CHAM = 'VARI_NOEU',
                    NOM_CMP = 'V2',
                    VALE_CALC = 1.0,
                    CRITERE = 'RELATIF'))

# Verification test 5 (int_var 1 at point A)
TEST_RESU(RESU = _F(INST = 300.,
                    GROUP_NO = 'A',
                    RESULTAT = result2,
                    NOM_CHAM = 'VARI_NOEU',
                    NOM_CMP = 'V1',
                    VALE_CALC = 0.00367869888705,
                    CRITERE = 'RELATIF'))

# Verification test 6 (int_var 2 at point A)
TEST_RESU(RESU = _F(INST = 300.,
                    GROUP_NO = 'A',
                    RESULTAT = result2,
                    NOM_CHAM = 'VARI_NOEU',
                    NOM_CMP = 'V2',
                    VALE_CALC = 0.0,
                    CRITERE = 'ABSOLU'))

# Finish
FIN();
