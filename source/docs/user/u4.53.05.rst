.. role:: red
.. role:: blue
.. role:: green
.. role:: gray
.. role:: python(code)
   :language: python
.. |br| raw:: html

   <br/>

.. index:: ! DYNA_LINE

.. _u4.53.05:

************************************************************************
**[U4.53.05]** : Operator ``DYNA_LINE``
************************************************************************

.. _u4.53.05.1:

Purpose
=======

:red:`DYNA_LINE` is the command that performs a linear transient or harmonic response calculation.
The basis can consist of either the physical coordinates or generalized coordinates.  It can also 
process localized non-linearities for transient response calculations, soil-structure interaction 
or fluid-structure interaction on a generalized basis.

The advantage of the operator :red:`DYNA_LINE`, compared to the operators :red:`DYNA_VIBRA` or 
:red:`CALC_MISS`, is that the user does not need to explicitly assemble the system of equations. 
Building a relevant projection basis for the solution of the problem on the generalized basis is 
automatically carried out by the operator using the input information provided.

The :red:`DYNA_LINE` operator produces an output concept that depends on the type of 
computation carried out (:green:`dyna_trans` or :green:`dyna_harmo`).

 
.. _u4.53.05.2:

Syntax
======

::

  concept_name [dyna_line_prod] = DYNA_LINE (

    ♦ TYPE_CALCUL = / 'HARM',
                    / 'TRAN',

    ♦ BASE_CALCUL = / 'PHYS',
                    / 'GENE',

    # Keywords concerning the physical data of the finite element model:
    ♦ MODELE = model,                                     [modele]
    ◊ CHAM_MATER = mater,                                 [cham_mater]
    ◊ CARA_ELEM = charac,                                 [cara_elem]
    ♦ CHARGE = load,                                      [char_meca]
                                                          [char_cine_meca]

    # Recovery of the concepts on the generalized basis:
    # If BASE_CALCUL == 'GENE'

    ◊ BASE_RESU = base_resu,                              [mode_meca]
    ◊ RESU_GENE = resu_gene,                              [tran_gene, harm_gene]

    # Keywords concerning the computation of the generalized basis:
    # If BASE_CALCUL == 'GENE' and IFS is not considered
   
    ◊ ENRI_STAT = / 'OUI',                                [DEFAULT]
                  / 'NON',

    # If ENRI_STAT == 'OUI'

       ◊ ORTHO = / 'NON',                                 [DEFAULT]
                 / 'OUI',
       ◊ FREQ_COUP_STAT = fc_stat                         [R]

    # Keywords for freqency intervals
    ◊ BANDE_ANALYSE = [f_min, f_max],                     [l_I]
    ◊ PCENT_COUP = / pcent_coup,                          [R]
                   / 90.0

    # Keywords concerning the intervals for harmonic calculations:
    # If TYPE_CALCUL == 'HARM'

    ◊ / FREQ = freq,                                      [l_R]
      / LIST_FREQ = list_freq,                            [listr8]

    # Keywords concerning the increments for a transient computation:
    # If TYPE_CALCUL == 'TRAN'

    ◊ SCHEMA_TEMPS = _F(see the document [U4.53.03]),

    ♦ INCREMENT = _F(

         ◊ INST_INIT = ti,                                  [R]
         ◊ PAS = dt,                                        [R]
         ♦ INST_FIN = tf,                                   [R]
       ),

    ◊ ETAT_INIT = _F(

         ◊ DEPL = disp,                                     [cham_no]
         ◊ VITE = vel,                                      [cham_no]
       ),

    ◊ ARCHIVAGE = _F(see the document [U4.53.03]),

    # Damping
    ◊ AMORTISSEMENT = _F(

         ♦ TYPE_AMOR = / 'RAYLEIGH',
                      / 'HYST',
                      / 'MODAL',
       ),

       # If TYPE_AMOR == 'MODAL'

         ♦ AMOR_REDUIT = l_amor,                            [l_R]

    # Loads
    ◊ EXCIT = _F(

         ◊ TYPE_APPUI = / 'MONO',
                        / 'MULTI',
         ♦ / FONC_MULT = f,                                [function]
                                                           [layer]
                                                           [formula]
           / COEF_MULT = coef,                             [R]
           / FONC_MULT_C = hci,                            [function_C]
                                                           [formula_C]
           / COEF_MULT_C = aci,                            [VS]
                                                           [formula]
         ◊ PHAS_DEG = / 0.,                                [DEFAULT]
                      / phi,                               [R]
         ◊ PUIS_PULS = / 0,                                [DEFAULT]
                       / ni,                               [Is]

         # Without TYPE_APPUI
         ♦ CHARGE = load,                                  [char_meca]

    # Operands and keywords specific to seismic analysis
         # With TYPE_APPUI
         ♦ DIRECTION = (dx, dy, dz, drx, dry, drz),        [l_R]
         ◊ / GROUP_NO = lgrno,                             [l_groupe_no]
         ◊ ACCE = acc,                                     [function]
                                                           [layer]
                                                           [formula]
         ◊ VITE = vel,                                     [function]
                                                           [layer]
                                                           [formula]
         ◊ DEPL = disp,                                    [function]
                                                           [layer]

    # Soil-structure interactioni
    ◊ ISS = 'OUI',

    # If ISS == 'YES'
    ◊ VERSION_MISS = /'V6.7',                              [DEFAULT]
                      'V6.6',
                      'V6.5',
    ◊ CALC_IMPE_FORC = / 'OUI',                            [DEFAULT]
                       / 'NON',
    ♦ GROUP_MA_INTERF = grma,                              [grma]
    ♦ GROUP_NO_INTERF = grno,                              [grno]
    ◊ TYPE_MODE = / 'PSEUDO',
                  / 'CRAIG_BAMPTON',                       [DEFAULT]
                  / 'INTERF',

    # If TYPE_MODE == 'INTERF'
    ♦ NB_MODE_INTERF = int                                 [I]

    # If CALC_IMPE_FORC == 'NON'

    ♦ UNITE_RESU_IMPE = uresimp                           [I]
    ♦ UNITE_RESU_FORC = uresfor                           [I]

    # If CALC_IMPE_FORC == 'OUI'

    ◊ UNITE_IMPR_ASTER = uimpast                          [I]
    ◊ UNITE_RESU_IMPE = uresimp                           [I]
    ◊ UNITE_RESU_FORC = uresfor                           [I]

    ♦ / TABLE_SOL = (see the document [U7.03.12]),
      / MATER_SOL = (see the document [U7.03.12]),

    ◊ PARAMETER = _F(see the document [U7.03.12]),

    # If TYPE_CALCUL == 'TRAN'

    ♦ /| ACCE_X = acce_x                                  [c_function]
       | ACCE_Y = acce_y                                  [c_function]
       | ACCE_Z = acce_z                                  [c_function]
      /| DEPL_X = depl_x                                  [c_function]
       | DEPL_Y = depl_y                                  [c_function]
       | DEPL_Z = depl_z                                  [c_function]

    # Non-linear behaviors
    ◊ COMPORTEMENT = _F(see the document [U4.53.03]),

    # Fluid-structure interaction

    ◊ IFS = 'OUI',

    # If IFS == 'OUI'

    ◊ FORC_AJOU = 'OUI',
    ♦ GROUP_MA_INTERF = interf,                           [gr_ma]
    ♦ GROUP_MA_FLUIDE = fluid,                            [gr_ma]
    ♦ MODELISATION_FLU = / '3D'
                         / 'COQUE'
    ♦ RHO_FLUIDE = _F(

         ♦ RHO = rho,                                     [R]
         ♦ / TOUT = 'OUI',
           / GROUP_MA = grma,                             [gr_ma]
      ),

    ♦ PRESSURE_FLU_IMPO = _F(

         ♦ PRESS_FLUIDE = pres                            [R]
         ♦ GROUP_NO = grno                                [gr_no]
      ),

    # Solver

    ◊ SOLVEUR = _F(see the document [U4.50.01]),


**Data structure produced**:

+-----------------------+------------+
| TYPE_CALCUL == 'TRAN' | dyna_trans |
+-----------------------+------------+
| TYPE_CALCUL == 'HARM' | dyna_harmo |
+-----------------------+------------+

.. _u4.53.05.3:

Indirection operands
====================

.. _u4.53.05.3.1:

:blue:`TYPE_CALCUL`
-------------------

This keyword makes it possible to choose between a transient computation (:blue:`TYPE_CALCUL = 'TRAN'`) and 
a harmonic computation (:blue:`TYPE_CALCUL = 'HARM'`).

.. _u4.53.05.3.2:

:blue:`BASE_CALCUL`
-------------------

This keyword makes it possible to choose between a calculation that uses the physical basis 
(:blue:`BASE_CALCUL = 'PHYS'`) and a computation using a generalized basis (:blue:`BASE_CALCUL = 'GENE'`).

.. _u4.53.05.4:

Operands common to any type of calculation
==========================================

The following operands, common to any type of calculation, make it possible to define the model components.

.. _u4.53.05.4.1:

:blue:`MODELE`
--------------

::

  ♦ MODELE = model,                     [modele]


Defines the physical model used for the dynamics calculation.

.. _u4.53.05.4.2:

:blue:`CHAM_MATER`
------------------

::

  ◊ CHAM_MATER = chmat,                 [cham_mater]

Material field used for calculation.

.. _u4.53.05.4.3:

:blue:`CARA_ELEM`
-----------------

::

  ◊ CARA_ELEM = charac,                 [cara_elem]

Geometric and elementary characteristics of the structural elements used for the calculation.

.. _u4.53.05.4.4:

:blue:`CHARGE`
--------------

::

  ♦ CHARGE = load,                      [char_meca]

Dirichlet boundary conditions needed to fix the structure.


.. _u4.53.05.5:

Additional generalized basis output concepts
============================================

The data structure produced by the operator :red:`DYNA_LINE` is always restored on the physical basis, included 
in the case of a calculation on a generalized basis. In principle, therefore, the user does not need to recover 
the basis or the generalized result that has been used for computation. However, these elements maybe useful for 
some advanced post-processing.

.. _u4.53.05.5.1:

:blue:`BASE_RESU`
-----------------

::

  ◊ BASE_RESU = base_resu,              [mode_meca]

Optionally allows the user to recover the calculation basis built by the operator and which was used for dynamic 
computation on the generalized basis. The output concept is of type :green:`[mode_meca]`.

.. _u4.53.05.5.2:

:blue:`RESU_GENE`
-----------------

::

  ◊ RESU_GENE = resu_gene,              [tran_gene]

Operand :blue:`RESU_GENE` makes it possible to recover the result optionally on a generalized basis. The
output concept is of type :green:`[tran_gene, harm_gene]` according to the type of computation (:blue:`TYPE_CALCUL`).

.. _u4.53.05.6:

Calculation of static enrichment
--------------------------------

.. _u4.53.05.6.1:

:blue:`ENRI_STAT`
-----------------

::

  ◊ ENRI_STAT = / 'OUI',                [DEFAULT]
                / 'NON'

In the case of a calculation on a generalized basis, a static correction is by default set up via the calculation 
of the static modes, added to the modal basis. 

Static modes are calculated for each load applied with the keyword factor :blue:`EXCIT` (see :numref:`u4.53.05.9`), 
according to strategies that depend on the type of loading. Static modes are also calculated for the nonlinear 
behaviors assigned with the keyword factor :blue:`COMPORTEMENT` (see :numref:`u4.53.05.11.1`). 

The static modes are then assembled with the dynamic modes to form the generalized calculation basis.

The keyword :blue:`ENRI_STAT` is not usable in the case of a calculation with fluid-structure interaction
(:blue:`IFS = 'OUI'`) (see :numref:`u4.53.05.12`).

.. _u4.53.05.6.2:

:blue:`ORTHO`
-------------

::

  ◊ ORTHO = / 'NON',                    [DEFAULT]
            / 'OUI'

Allows the orthogonalization of the assembled basis of the dynamic modes and the static modes. Orthogonalization 
is carried out with the eigenvalues of a matrix made up of the stiffness and mass matrices projected onto this 
assembled basis.

.. _u4.53.05.6.3:

:blue:`FREQ_COUP_STAT`
----------------------

::

  ◊ FREQ_COUP_STAT = fc_stat            [R]

Cutoff frequency (expressed in Hz) used for modal analysis allowing orthogonalization. If the keyword 
:blue:`FREQ_COUP_STAT` is not defined by the user, no cutoff is taken into account.

.. _u4.53.05.7:

Defining the calculation range and increments
=============================================

.. _u4.53.05.7.1:

Operands :blue:`BANDE_ANALYSE`, :blue:`PCENT_COUP`
--------------------------------------------------

.. _u4.53.05.7.1.1:

Keyword :blue:`BANDE_ANALYSE`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

  ◊ BANDE_ANALYSE = [f_min, f_max],

Allows you to define the frequency band in which you want to perform the dynamic analysis. In the case of a 
calculation on a generalized basis, the values of the band are used for the calculation of the dynamic modes. 
This keyword can contain only one value, in this case this value will correspond to the maximum frequency 
:green:`f_max`, the minimum frequency :green:`f_min` being set at zero. 

If the key word :blue:`BANDE_ANALYSE` is not indicated, in the case of a transient computation, the minimum 
frequency :green:`f_min` is set to zero, and the maximum frequency :green:`f_max` will be calculated automatically 
from the cutoff frequency of the input signals.

.. _u4.53.05.7.1.2:

Keyword :blue:`PCENT_COUP`
^^^^^^^^^^^^^^^^^^^^^^^^^^

::

  ◊ PCENT_COUP = / pcent_coup,                   [R]
                 / 90.0

The cutoff frequency of an input signal is calculated so as to contain :green:`pcent_coup` % ofthe energy of the 
input signal. The value of the keyword :blue:`PCENT_COUP` must be between 80 and 100. The cutoff frequency 
corresponds to the maximum value of the cutoff frequencies of each of the signals; the maximum frequency 
:green:`f_max` is imposed at twice the cut-off frequency.

.. _u4.53.05.7.2:

Operands :blue:`FREQ`, :blue:`LIST_FREQ`
----------------------------------------

::

  ◊ / FREQ = freq,                             [l_R]
    / LIST_FREQ = list_freq,                   [listr8]

In the harmonic case, these keywords make it possible to define the frequencies for which the dynamic response 
will be calculated.

.. _u4.53.05.7.3:

Operands :blue:`SCHEMA_TEMPS, INCREMENT, ETAT_INIT, ARCHIVAGE`
--------------------------------------------------------------

The following keywords are useful in the case of a transient computation.

.. _u4.53.05.7.3.1:

Keyword factor :blue:`SCHEMA_TEMPS`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

  ◊ SCHEMA_TEMPS = _F(...)

This keyword factor makes it possible to define the parameters concerning the time integration scheme
in the case of a transient computation. Refer to the documentation of :red:`DYNA_VIBRA` :ref:`[U4.53.03] <u4.53.03>` 
for more information concerning the meaning of the simple keywords used.

Note that, by default, the operator :red:`DYNA_LINE` uses the :blue:`DEVOGE` scheme contrary to 
:red:`DYNA_VIBRA` which uses the :blue:`DIFF_CENTRE` scheme by default.

.. _u4.53.05.7.3.2:

Keyword factor :blue:`INCREMENT`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

  ♦  INCREMENT = _F( ◊ INST_INIT = t0,              [R]
                     ◊ PAS = dt,                    [R]
                     ♦ INST_FIN = tf                [R]
                   )

This keyword factor makes it possible to define the time increments in the case of a transient computation. Refer 
to the documentation of :red:`DYNA_VIBRA` :ref:`[U4.53.03] <u4.53.03>` for more information concerning the meaning 
of the keywords used.

Note that, contrary to the operator :red:`DYNA_VIBRA`, it is not obligatory to specify the simple keyword 
:blue:`PAS` (:gray:`STEP`) defining the computation step. In this case, the calculation step is calculated 
automatically if the keyword :blue:`BANDE_ANALYSE` is present (see :numref:`u4.53.05.7.1.1`), and the step is 
calculated as follows:

.. math::

   \Delta t = \frac{1}{5 f_{\text{max}}}

Otherwise, the step is calculated directly from the cutoff frequency (see :numref:`u4.53.05.7.1.2`), as follows:

.. math::

   \Delta t = \frac{1}{5 f_c}

.. _u4.53.05.7.3.3:

Keyword factor :blue:`ETAT_INIT`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

  ◊ ETAT_INIT = _F(...)

This factor keyword makes it possible to define an initial state. Refer to the documentation of
:red:`DYNA_VIBRA` :ref:`[U4.53.03] <u4.53.03>` for more information concerning the meaning of the simple keywords
used.

Note that in the case of a calculation on a generalized basis, contrary to what is provided for in the setting of
data for :red:`DYNA_VIBRA`, the user cannot provide the vectors projected on the generalized basis. It is necessary 
to provide, in the same way as for a calculation on the physical basis, the nodal fields on the physical basis, 
which will be automatically projected on to the generalized basis if necessary.

.. _u4.53.05.7.3.4:

Keyword factor :blue:`ARCHIVAGE`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

  ◊ ARCHIVAGE = _F(...)

This factor keyword makes it possible to control the instants where the data are saved. Refer to the
documentation of :red:`DYNA_VIBRA` :ref:`[U4.53.03] <u4.53.03>` for more information concerning the meaning of
the simple keywords used.

.. _u4.53.05.8:

Definition of damping
=====================

.. _u4.53.05.8.1:

Keyword factor :blue:`AMORTISSEMENT`
------------------------------------

::

  ◊ AMORTISSEMENT = _F(...)

Keyword factor allowing the user to introduce various types of damping.

.. _u4.53.05.8.1.1:

:blue:`TYPE_AMOR = 'RAYLEIGH'`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

  ♦ TYPE_AMOR = 'RAYLEIGH',

This option makes it possible to calculate the Rayleigh damping matrix based on the parameters 
(:blue:`AMOR_ALPHA, AMOR_BETA`) of the materials defined in CHAM_MATER (see :numref:`u4.53.05.4.2`) and
of the discrete elements of the type (:blue:`A_T*`) defined in :blue:`CARA_ELEM` (see :numref:`u4.53.05.4.3`).

.. _u4.53.05.8.1.2:

:blue:`TYPE_AMOR = 'HYST'`
^^^^^^^^^^^^^^^^^^^^^^^^^^

::

  ♦ TYPE_AMOR = 'HYST',

This option is used in the case of a harmonic calculation (:blue:`TYPE_CALCUL == 'HARM'`). It allows the 
calculation the matrix of hysteretic damping based on the parameter :blue:`AMOR_HYST` defined in :blue:`CHAM_MATER` 
(see :numref:`u4.53.05.4.2`) and discrete elements of the type (:blue:`A_T*`) defined in :blue:`CARA_ELEM` 
(see :numref:`u4.53.05.4.3`).

.. _u4.53.05.8.1.3:

:blue:`TYPE_AMOR = 'MODAL'`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

  ♦ TYPE_AMOR = 'MODAL',

In the case of a calculation on a generalized basis, it is sometimes simpler and more relevant to define a
modal damping. In this case, it is necessary to specify in :blue:`AMOR_REDUIT` the reduced damping factors 
(percentages of critical damping) corresponding to each mode of the system in the form of a list of reals.

::

  ♦ AMOR_REDUIT = l_amor,               [l_R]

If the number of reduced damping modes given is lower than the number of basis vectors used in the modal basis, 
the damping of the additional vectors are taken equal to the last damping factor in the list :green:`l_amor`.

.. _u4.53.05.9:

Keyword factor :blue:`EXCIT`
============================

::

  ◊ EXCIT = F(...)

This factor keyword makes it possible to define with the loads. Operands have a meaning equivalent to those 
described in the document :ref:`[U4.54.03] <u4.54.03>` with the difference that, for operator :red:`DYNA_LINE`, 
it is not necessary to provide all the information concerning the right hand sides, which are automatically 
calculated by the operator.

.. _u4.53.05.9.1:

Keyword :blue:`CHARGE`
----------------------

Loading applied to the structure, previously defined with operator :red:`AFFE_CHAR_MECA`. The keyword 
:blue:`CHARGE` must appear only in the occurrence of the keyword factor if the keyword :blue:`TYPE_APPUI` is 
not present.

.. _u4.53.05.9.2:

Keywords :blue:`TYPE_APPUI, DIRECTION, GROUP_NO`
------------------------------------------------

Keywords used in the case of seismic loadings, refer to the document :ref:`[U4.63.01] <u4.63.01>` for more 
details. 

.. warning::

   The :blue:`TYPE_APPUI` keyword must appear only in the occurrence of the keyword factor if the keyword 
   :blue:`CHARGE` is not present.

.. _u4.53.05.9.2.1:

:blue:`TYPE_APPUI = 'MONO'`
^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

  ◊ TYPE_APPUI = 'MONO'

The structure is excited uniformly at all supports. It is necessary to specify the direction of excitation via 
the keyword :blue:`DIRECTION`.

.. _u4.53.05.9.2.2:

:blue:`TYPE_APPUI = 'MULTI'`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

  ◊ TYPE_APPUI = 'MULTI'

The excitation is applied to only part of the supports. It is necessary to specify the direction of excitation
via keyword :blue:`DIRECTION` as well as the nodes concerned via key word :blue:`GROUP_NO`.

.. _u4.53.05.9.2.3:

Keyword :blue:`DIRECTION`
^^^^^^^^^^^^^^^^^^^^^^^^^

::

  ◊ DIRECTION = (dx, dy, dz, drx, dry, drz)

Component of a vector giving the direction of the earthquake in the global reference coordinates.

.. _u4.53.05.9.2.4:

Keyword :blue:`GROUP_NO`
^^^^^^^^^^^^^^^^^^^^^^^^

::

  ◊ GROUP_NO = g_node

List of the node groups of the structure subjected to the excitation for the cases where 
:blue:`TYPE_APPUI` is activated. 

.. note::

   If :blue:`TYPE_APPUI = 'MULTI'`, the keyword :blue:`GROUP_NO` must be specified. If :blue:`TYPE_APPUI ='MONO'`, 
   the keyword :blue:`GROUP_NO` is not compulsory. However, the occurrences for the keyword :blue:`GROUP_NO` will have 
   to be the same for all the occurrences of the keyword factor :blue:`EXCIT`.

.. _u4.53.05.9.3:

Operands :blue:`FONC_MULT, COEF_MULT, FONC_MULT_C, COEF_MULT_C, ACCE, VITE, DEPL, PHAS_DEG, PUIS_PULS`
------------------------------------------------------------------------------------------------------

Coefficient or multiplying function of the assembled vector. In the transient case, it is necessary to provide 
a function of time. In the harmonic case, it is possible to provide a coefficient or a complex function depending 
on the frequency. Refer to the document :ref:`[U4.53.03] <u4.53.03>` for more details on the use of these keywords.

.. _u4.53.05.10:

Soil-structure interaction
==========================

::

  ◊ ISS = 'OUI'

Activation of the soil-structure interaction feature. In the case of soil-structure interaction, the seismic 
calculation will use parameters calculated using :blue:`MISS3D` (via :red:`CALC_MISS`).

.. _u4.53.05.10.1:

Inputs-outputs of the soil calculation
--------------------------------------

.. _u4.53.05.10.1.1:

Keyword :blue:`CALC_IMPE_FORC`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

  ◊ CALC_IMPE_FORC = / 'OUI',                   [DEFAULT]
                     / 'NON'

By default, the operator :red:`CALC_MISS` is called the first time by using the option :blue:`FICHIER`, which
launches an execution of :blue:`MISS3D` and to save the results in files, defined on the appropriate logical unit.

The user can deactivate this step by setting :blue:`CALC_IMPE_MISS = 'NON'`. In that case, it is necessary to 
define the logical units which correspond to already existing files.

.. _u4.53.05.10.1.2:

Keywords :blue:`UNITE_RESU_IMPE, UNITE_RESU_FORC`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

  ♦ UNITE_RESU_IMPE = uresimp                          [I]
  ♦ UNITE_RESU_FORC = uresfor                          [I]
  ◊ UNITE_RESU_FORC = uresfor                          [I]

These key words make it possible to define the logical units associated with the impedances and equivalent
seismic forces calculated by :blue:`MISS3D`.

For the case where :blue:`CALC_IMPE_FORC == 'NON'`, these keywords are obligatory and correspond to already 
existing files that have been generated earlier in a different computation step.

If :blue:`CALC_IMPE_FORC == 'OUI'`, these keywords are optional and make it possible to compute the impedances 
and the equivalent seismic forces calculated by :blue:`MISS3D`.

.. _u4.53.05.10.1.3:

Keywords :blue:`TABLE_SOL / MATER_SOL`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

  ♦ / TABLE_SOL = tabsol                      [table]
    / MATER_SOL = matsol

These keywords are used to define the soil stratification used in a :blue:`MISS3d` computation.

.. warning::

    One of these keywords must obligatorily be defined if :blue:`CALC_IMPE_FORC == 'OUI'`. 

Refer to the document :ref:`[U7.03.12] <u7.03.12>` for more details concerning these keywords.

.. _u4.53.05.10.2:

Soil-structure interface
-------------------------

.. _u4.53.05.10.2.1:

Keyword :blue:`VERSION_MISS`
-----------------------------

::

  ♦ VERSION_MISS = / 'V6.7',                     [DEFAULT]
                     'V6.6',
                     'V6.5'

Version number of :blue:`MISS3D`. The default value corresponds to the version of :blue:`MISS3D` in operation.

.. warning::

   :blue:`MISS3D` calculations on large models (or with many frequencies) can be long and memory intensive. 
   Fortunately, these can be sped up by activating one or two parallelism levels. 

For more information, consult documents :ref:`[U2.06.07] <u2.06.07>` and :ref:`[U2.08.06] <u2.08.06>`.

.. _u4.53.05.10.2.2:

Keywords :blue:`GROUP_MA_INTERF, GROUP_NO_INTERF`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

  ♦  GROUP_MA_INTERF = elem_grp,                     [grma]
  ♦  GROUP_NO_INTERF = node_grp                      [grno]

:blue:`GROUP_MA_INTERF` corresponds to the element group constituting the soil-structure interface.

:blue:`GROUP_NO_INTERF` corresponds to the group of nodes where the conditions of coupling are imposed. 
It is a question of automatically carrying out an embedding of the degrees of freedom of the nodes concerned 
to calculate the eigenmodes of the structure. The user should not add boundary conditions on this group as 
in the case of :red:`CALC_MISS`.

.. warning::

   In the case of a rigid foundation, the user must define a :blue:`LIAISON_SOLIDE` via the keyword :blue:`CHARGE` 
   and a :blue:`GROUP_NO_INTERF` containing a node corresponding to the center the foundation. In the case of a 
   flexible foundation, no condition should be defined on the :blue:`GROUP_NO_INTERF` which must contain all the 
   nodes of the foundation.

.. _u4.53.05.10.2.3:

Keywords :blue:`TYPE_MODE / NB_MODE_INTERF`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

  TYPE_MODE = / 'CRAIG_BAMPTON',                    [DEFAULT]
              / 'INTERF',
              / 'PSEUDO',

Allows the definition of the method of calculation of the interface modes associated with :blue:`GROUP_MA_INTERF`.

If :blue:`TYPE_MODE == 'CRAIG_BAMPTON'`, all the coupling or constrained modes associated with the soil-structure 
interface are calculated.

If :blue:`TYPE_MODE == 'INTERF'`, the user must specify the number of static modes to be calculated via
the keyword :blue:`NB_MODE_INTERF`.

If :blue:`TYPE_MODE == 'PSEUDO'`, six rigid body modes are calculated.

.. _u4.53.05.10.3:

Soil calculation parameters
----------------------------

::

  ◊ PARAMETER = _F(...)

This factor keyword makes it possible to enter the parameters of a :blue:`MISS3D` calculation. Refer to the 
document :ref:`[U7.03.12] <u7.03.12>` for more details.

.. _u4.53.05.10.4:

Operands :blue:`ACCE_X, ACCE_Y, ACCE_Z, DEPL_X, DEPL_Y, DEPL_Z`
---------------------------------------------------------------

In the case of a transient computation, these operands make it possible to input accelerograms using accelerations 
in one or more directions (:blue:`ACCE_X, ACCE_Y, ACCE_Z`) or of displacements imposed in one or more directions 
(:blue:`DEPL_X, DEPL_Y, DEPL_Z`).

.. _u4.53.05.11:

Non-linear behaviors
====================

.. _u4.53.05.11.1:

Keyword factor :blue:`COMPoRTEMENT`
-----------------------------------

::

  ◊ COMPORTEMENT = _F(...)

Keyword to define various types of localized non-linearities in the case of a transient computation on a 
generalized basis. Refer to documentation :ref:`[U4.53.03] <u4.53.03>` for more details.

.. _u4.53.05.12:

Fluid-structure interaction
===========================

::

  ◊ IFS = 'OUI',

Activation of the fluid-structure interaction feature. In this case, the fluid interaction-structure is taken 
into account via added masses with a dynamic calculation on the generalized basis.

.. _u4.53.05.12.1:

Taking into account the added force
-----------------------------------

::

  ◊ FORC_AJOU = 'OUI',

This keyword manages the added forces due to seismic motions.  Refer to the document :ref:`[U4.66.03] <u4.66.03>` 
for more details.

.. _u4.53.05.12.2:

Fluid-structure interface
--------------------------

::

  ♦ GROUP_MA_INTERF = interf,                  [gr_ma]

Defines the element groups forming the fluid-structure interface

.. _u4.53.05.12.3:

Fluid model
-----------

.. _u4.53.05.12.3.1:

:blue:`MODELISATION_FLU`
^^^^^^^^^^^^^^^^^^^^^^^^^

::

  ♦  MODELISATION_FLU = / '3D'
                        / 'COQUE'

Type of of the fluid model (:gray:`3D` or :gray:`SHELL`)

.. _u4.53.05.12.3.2:

:blue:`GROUP_MA_FLUIDE`
^^^^^^^^^^^^^^^^^^^^^^^^

::

  ♦ GROUP_MA_FLUIDE = fluid,    [gr_ma]

Group of elements on which the fluid model is applied.

.. _u4.53.05.12.4:

Fluid properties
----------------

::

  ♦  RHO_FLUIDE = _F(...)

Keyword factor for defining the characteristics of the fluid material. 

.. note::

   If the density of the fluid varies on 
   the fluid field, it is necessary to specify these various densities by several occurrences of the keyword factor 
   :blue:`RHO_FLUIDE`.

.. _u4.53.05.12.4.1:

Operand :blue:`RHO`
^^^^^^^^^^^^^^^^^^^

::

  ♦ RHO = rho,              [R]

Density of the fluid.

.. _u4.53.05.12.4.2:

Operands :blue:`TOUT / GROUP_MA`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

   ♦ / TOUT = 'OUI',
     / GROUP_MA = elem_grp,                  [gr_ma]

Part of the fluid model to which the density :green:`rho` is applied.

.. _u4.53.05.12.5:

Boundary conditions for the fluid problem
-----------------------------------------

::

  ♦  PRESSURE_FLU_IMPO = _F(...)

Keyword factor by which one specifies the boundary conditions of the fluid (of Dirichlet type).

.. _u4.53.05.12.5.1:

Operand :blue:`PRESS_FLUIDE`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

  ♦ PRESS_FLUIDE = pres                    [R]

Keyword with which one specifies the value which one imposes on the hydrodynamic pressure (i.e, pressure 
disturbance created by the vibration of the structure) on the topological entity determined below.

.. _u4.53.05.12.5.2:

Operand :blue:`GROUP_NO`
^^^^^^^^^^^^^^^^^^^^^^^^

::

  ♦  GROUP_NO = node_grp                    [gr_no]

Groups of nodes where one imposes the pressure :green:`pres` on the fluid field.

.. _u4.53.05.13:

Keyword factor :blue:`SOLVEUR`
===============================

Refer to the document :ref:`[U4.50.01] <u4.50.01>`.

.. warning::

   The use of the keyword :blue:`SOLVEUR` is not possible in the case of a calculation with soil-structure
   interaction.
