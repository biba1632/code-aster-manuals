# Forma13a: substructuring
# Start
DEBUT(LANG='EN')

# Material parameters
E = 210.E9
rho = 7800.0
nu = 0.3

# Read mesh of assembly
mesh_ass = LIRE_MAILLAGE(UNITE = 20,
                         FORMAT = 'MED')

# Assign elements to assembly (3D and shell)
model_as = AFFE_MODELE(MAILLAGE = mesh_ass,
                       AFFE = (_F(GROUP_MA = ('Pal_3D','Vol_3D','Tuy_3D'),
                                  PHENOMENE = 'MECANIQUE',
                                  MODELISATION = '3D'),
                               _F(GROUP_MA = ('Pal_2D','Vol_2D','Tuy_2D'),
                                  PHENOMENE = 'MECANIQUE',
                                  MODELISATION = 'DKT')))

# Assign shell thicknesses and interface face thicknesses
shell_th = AFFE_CARA_ELEM(MODELE = model_as,
                          COQUE = (_F(GROUP_MA = 'Pal_2D',
                                      EPAIS = 0.03),
                                   _F(GROUP_MA = 'Tuy_2D',
                                      EPAIS = 0.01),
                                   _F(GROUP_MA = 'Vol_2D',
                                      EPAIS = 0.05),
                                   _F(GROUP_MA = ('tuy_vol', 'vol_tuy',
                                                   'vol_pal', 'pal_vol',),
                                      EPAIS = 0.05)))

# Define and assign material
steel = DEFI_MATERIAU(ELAS = _F(E = E,
                                NU = nu,
                                RHO = rho))
mater_as = AFFE_MATERIAU(MAILLAGE = mesh_ass,
                         AFFE = _F(TOUT = 'OUI',
                                   MATER = steel))

# Assign fixed displacement BCs
u_bc_ass = AFFE_CHAR_MECA(MODELE = model_as,
                          DDL_IMPO = _F(GROUP_MA = ('BC_pal','BC_vol','BC_tuy'),
                                        LIAISON = 'ENCASTRE'))

# Assemble system of equations
ASSEMBLAGE(MODELE = model_as,
           CHAM_MATER = mater_as,
           CARA_ELEM = shell_th,
           CHARGE = u_bc_ass,
           NUME_DDL = CO('num_dof'),
           MATR_ASSE = (_F(MATRICE = CO('mass_mat'),
                           OPTION = 'MASS_MECA'),
                        _F(MATRICE = CO('stif_mat'),
                           OPTION = 'RIGI_MECA')))


# Modal analysis
mode_ass = CALC_MODES(TYPE_RESU = 'DYNAMIQUE',
                      OPTION = 'CENTRE',
                      SOLVEUR_MODAL = _F(METHODE = 'SORENSEN'),
                      CALC_FREQ = _F(NMAX_FREQ = 10, FREQ = 60.),
                      MATR_RIGI = stif_mat,
                      MATR_MASS = mass_mat,
                      VERI_MODE = _F(SEUIL = 1e-05))

# Read the subassembly parts
msh_bear = LIRE_MAILLAGE(UNITE = 21,
                       FORMAT='MED')
msh_volu = LIRE_MAILLAGE(UNITE = 22,
                       FORMAT='MED')
msh_pipe = LIRE_MAILLAGE(UNITE = 23,
                       FORMAT = 'MED')

# Assign elements and create models to parts
bearing = AFFE_MODELE(MAILLAGE = msh_bear,
                      AFFE = (_F(GROUP_MA = 'Pal_3D',
                                 PHENOMENE = 'MECANIQUE',
                                 MODELISATION = '3D'),
                              _F(GROUP_MA = 'Pal_2D',
                                 PHENOMENE = 'MECANIQUE',
                                 MODELISATION = 'DKT')))

volute = AFFE_MODELE(MAILLAGE = msh_volu,
                     AFFE = (_F(GROUP_MA = 'Vol_3D',
                                PHENOMENE = 'MECANIQUE',
                                MODELISATION = '3D'),
                             _F(GROUP_MA = 'Vol_2D',
                                PHENOMENE = 'MECANIQUE',
                                MODELISATION = 'DKT')))

pipe = AFFE_MODELE(MAILLAGE = msh_pipe,
                   AFFE = (_F(GROUP_MA = 'Tuy_3D',
                              PHENOMENE = 'MECANIQUE',
                              MODELISATION = '3D'),
                           _F(GROUP_MA = 'Tuy_2D',
                              PHENOMENE = 'MECANIQUE',
                              MODELISATION = 'DKT')))

# Assign shell thickness + interface thickness
shl_bear = AFFE_CARA_ELEM(MODELE = bearing,
                          COQUE = (_F(GROUP_MA = 'Pal_2D',
                                      EPAIS = 0.03),
                                   _F(GROUP_MA = 'pal_vol',
                                      EPAIS = 0.05)))

shl_volu = AFFE_CARA_ELEM(MODELE = volute,
                          COQUE = (_F(GROUP_MA = 'Vol_2D',
                                      EPAIS = 0.05),
                                   _F(GROUP_MA = ('vol_pal','vol_tuy'),
                                      EPAIS = 0.05)))

shl_pipe = AFFE_CARA_ELEM(MODELE = pipe,
                          COQUE = (_F(GROUP_MA = 'Tuy_2D',
                                      EPAIS = 0.01),
                                   _F(GROUP_MA = 'tuy_vol',
                                      EPAIS = 0.05)))
# Assign materials to parts
mat_bear = AFFE_MATERIAU(MAILLAGE = msh_bear,
                         AFFE = _F(TOUT = 'OUI',
                                   MATER = steel))
mat_volu = AFFE_MATERIAU(MAILLAGE = msh_volu,
                         AFFE = _F(TOUT = 'OUI',
                                   MATER = steel))
mat_pipe = AFFE_MATERIAU(MAILLAGE = msh_pipe,
                         AFFE = _F(TOUT = 'OUI',
                                   MATER = steel))

# Assign fixed displacement BCs to parts
bc_bear = AFFE_CHAR_MECA(MODELE = bearing,
                         DDL_IMPO = (_F(GROUP_MA = 'BC_pal',
                                        LIAISON = 'ENCASTRE'),
                                     _F(GROUP_MA = 'pal_vol',
                                        LIAISON = 'ENCASTRE')))

bc_volu = AFFE_CHAR_MECA(MODELE = volute,
                         DDL_IMPO = (_F(GROUP_MA = 'BC_vol',
                                        LIAISON = 'ENCASTRE',),
                                     _F(GROUP_MA = ('vol_pal','vol_tuy'),
                                        LIAISON = 'ENCASTRE')))

bc_pipe = AFFE_CHAR_MECA(MODELE = pipe,
                         DDL_IMPO = (_F(GROUP_MA = 'BC_tuy',
                                        LIAISON = 'ENCASTRE'),
                                     _F(GROUP_MA = 'tuy_vol',
                                        LIAISON = 'ENCASTRE')))

# Create dynamic macro-element and interface elements
mac_bear = CREA_ELEM_SSD(MODELE = bearing,
                         CHAM_MATER = mat_bear,
                         CARA_ELEM = shl_bear,
                         CHARGE = bc_bear,
                         INTERFACE = _F(NOM = 'bear_vol',
                                        TYPE = 'CRAIGB',
                                        GROUP_NO = 'pal_vol'),
                         BASE_MODALE = _F(TYPE = 'CLASSIQUE'))

mac_pipe = CREA_ELEM_SSD(MODELE = pipe,
                         CHAM_MATER = mat_pipe,
                         CARA_ELEM = shl_pipe,
                         CHARGE = bc_pipe,
                         INTERFACE = _F(NOM = 'pipe_vol',
                                        TYPE = 'CRAIGB',
                                        GROUP_NO = 'tuy_vol'),
                         BASE_MODALE = _F(TYPE = 'CLASSIQUE'))

mac_volu = CREA_ELEM_SSD(MODELE = volute,
                         CHAM_MATER = mat_volu,
                         CARA_ELEM = shl_volu,
                         CHARGE = bc_volu,
                         INTERFACE = (_F(NOM = 'vol_bear',
                                         TYPE = 'CRAIGB',
                                         GROUP_NO = 'vol_pal'),
                                      _F(NOM = 'vol_pipe',
                                         TYPE = 'CRAIGB',
                                         GROUP_NO = 'vol_tuy')),
                         BASE_MODALE = _F(TYPE = 'CLASSIQUE'))

# Assemble stiffness and mass matrices of the composite structure
# in the generalized eigenbasis
ASSE_ELEM_SSD(RESU_ASSE_SSD = _F(MODELE = CO('ass_gen'),
                                 NUME_DDL_GENE = CO('dof_gen'),
                                 RIGI_GENE = CO('stif_gen'),
                                 MASS_GENE = CO('mass_gen')),
              SOUS_STRUC = (_F(NOM = 'bear_gen',
                               MACR_ELEM_DYNA = mac_bear,
                               TRANS = (-0.35,0.0,0.0)),
                            _F(NOM = 'volu_gen',
                               MACR_ELEM_DYNA = mac_volu,
                               TRANS = (0.0,0.0,0.0)),
                            _F(NOM = 'pipe_gen',
                               MACR_ELEM_DYNA = mac_pipe,
                               TRANS = (0.24,0.0,0.0))),
              LIAISON = (_F(SOUS_STRUC_1 = 'bear_gen',
                            INTERFACE_1 = 'bear_vol',
                            SOUS_STRUC_2 = 'volu_gen',
                            INTERFACE_2 = 'vol_bear'),
                         _F(SOUS_STRUC_1 = 'pipe_gen',
                            INTERFACE_1 = 'pipe_vol',
                            SOUS_STRUC_2 = 'volu_gen',
                            INTERFACE_2 = 'vol_pipe')),
              METHODE = 'ELIMINE')

# Define mesh for visualizing substructuring
mesh_v = DEFI_SQUELETTE(MODELE_GENE = ass_gen,
                        SOUS_STRUC = (_F(NOM = 'bear_gen',
                                         GROUP_MA = ('Pal_3D','Pal_2D')),
                                      _F(NOM = 'volu_gen',
                                         GROUP_MA = ('Vol_3D','Vol_2D')),
                                      _F(NOM = 'pipe_gen',
                                         GROUP_MA = ('Tuy_3D','Tuy_2D'))))

# Compute modes
mode_gen = CALC_MODES(OPTION = 'PLUS_PETITE',
                      CALC_FREQ = _F(NMAX_FREQ = 10),
                      MATR_RIGI = stif_gen,
                      MATR_MASS = mass_gen)

# Convert results from eigenbasis to physical basis
mode_phy = REST_SOUS_STRUC(RESU_GENE = mode_gen,
                           TOUT_ORDRE = 'OUI',
                           NOM_CHAM = 'DEPL',
                           SQUELETTE = mesh_v)

# Write results
IMPR_RESU(FORMAT = 'MED',
          UNITE = 80,
          RESU = (_F(RESULTAT = mode_ass),
                  _F(RESULTAT = mode_phy)))

# Verification
TEST_RESU(RESU = (_F(NUME_ORDRE = 1,
                     PARA = 'FREQ',
                     RESULTAT = mode_phy,
                     VALE_CALC = 58.3199128567),
                  _F(NUME_ORDRE = 2,
                     PARA = 'FREQ',
                     RESULTAT = mode_phy,
                     VALE_CALC = 123.467250916),
                  _F(NUME_ORDRE = 3,
                     PARA = 'FREQ',
                     RESULTAT = mode_phy,
                     VALE_CALC = 155.274997187),
                  _F(NUME_ORDRE = 4,
                     PARA = 'FREQ',
                     RESULTAT = mode_phy,
                     VALE_CALC = 190.566620965),
                  _F(NUME_ORDRE = 5,
                     PARA = 'FREQ',
                     RESULTAT = mode_phy,
                     VALE_CALC = 224.846841505),
                  _F(NUME_ORDRE = 6,
                     PARA = 'FREQ',
                     RESULTAT = mode_phy,
                     VALE_CALC = 230.773539034),
                  _F(NUME_ORDRE = 7,
                     PARA = 'FREQ',
                     RESULTAT = mode_phy,
                     VALE_CALC = 249.223496312),
                  _F(NUME_ORDRE = 8,
                     PARA = 'FREQ',
                     RESULTAT = mode_phy,
                     VALE_CALC = 259.379355318),
                  _F(NUME_ORDRE = 9,
                     PARA = 'FREQ',
                     RESULTAT = mode_phy,
                     VALE_CALC = 261.071699943),
                  _F(NUME_ORDRE = 10,
                     PARA = 'FREQ',
                     RESULTAT = mode_phy,
                     VALE_CALC = 278.070045602)))
  
TEST_RESU(RESU = (_F(NUME_ORDRE = 1,
                     PARA = 'FREQ',
                     RESULTAT = mode_ass,
                     REFERENCE = 'AUTRE_ASTER',
                     VALE_REFE = 58.3199128567,
                     VALE_CALC = 58.3209995588),
                  _F(NUME_ORDRE = 2,
                     PARA = 'FREQ',
                     RESULTAT = mode_ass,
                     REFERENCE = 'AUTRE_ASTER',
                     VALE_REFE = 123.467250916,
                     VALE_CALC = 123.547059826),
                  _F(NUME_ORDRE = 3,
                     PARA = 'FREQ',
                     RESULTAT = mode_ass,
                     REFERENCE = 'AUTRE_ASTER',
                     VALE_REFE = 155.274997187,
                     VALE_CALC = 155.308103794),
                  _F(NUME_ORDRE = 4,
                     PARA = 'FREQ',
                     RESULTAT = mode_ass,
                     REFERENCE = 'AUTRE_ASTER',
                     PRECISION = 1.1E-3,
                     VALE_REFE = 190.566620965,
                     VALE_CALC = 190.773789059),
                  _F(NUME_ORDRE = 5,
                     PARA = 'FREQ',
                     RESULTAT = mode_ass,
                     REFERENCE = 'AUTRE_ASTER',
                     PRECISION = 4.E-3,
                     VALE_REFE = 224.846841505,
                     VALE_CALC = 225.707788922),
                  _F(NUME_ORDRE = 6,
                     PARA = 'FREQ',
                     RESULTAT = mode_ass,
                     REFERENCE = 'AUTRE_ASTER',
                     PRECISION = 1.E-2,
                     VALE_REFE = 230.773539034,
                     VALE_CALC = 232.990802961),
                  _F(NUME_ORDRE = 7,
                     PARA = 'FREQ',
                     RESULTAT = mode_ass,
                     REFERENCE = 'AUTRE_ASTER',
                     PRECISION = 1.5E-2,
                     VALE_REFE = 249.223496312,
                     VALE_CALC = 252.727212656),
                  _F(NUME_ORDRE = 8,
                     PARA = 'FREQ',
                     RESULTAT = mode_ass,
                     REFERENCE = 'AUTRE_ASTER',
                     PRECISION = 3.E-3,
                     VALE_REFE = 259.379355318,
                     VALE_CALC = 259.995449785),
                  _F(NUME_ORDRE = 9,
                     PARA = 'FREQ',
                     RESULTAT = mode_ass,
                     REFERENCE = 'AUTRE_ASTER',
                     PRECISION = 6.E-3,
                     VALE_REFE = 261.071699943,
                     VALE_CALC = 262.404357484),
                  _F(NUME_ORDRE = 10,
                     PARA = 'FREQ',
                     RESULTAT = mode_ass,
                     REFERENCE = 'AUTRE_ASTER',
                     PRECISION = 2.5E-2,
                     VALE_REFE = 278.070045602,
                     VALE_CALC = 284.769516921)))
# Finish  
FIN()
