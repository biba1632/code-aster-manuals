# FORMA42C: Phased loading of Reinforced beams with CALC_PRECONT
#---------------------------------------------------------------------
# Start
DEBUT(LANG = 'EN')

# Parameters
cable_rad = 2.8209E-2
init_tension = 3.75E6

# Read mesh
mesh = LIRE_MAILLAGE(FORMAT = 'MED')

# Define node groups
mesh = DEFI_GROUP(reuse = mesh,
                  MAILLAGE = mesh,
                  CREA_GROUP_NO = _F(GROUP_MA = ('cable4',
                                                 'cable0',
                                                 'cable3',
                                                 'cable2',
                                                 'cable1',
                                                 'bot_face')))

# Assign elements
model = AFFE_MODELE(MAILLAGE = mesh,
                    AFFE = (_F(GROUP_MA = 'vol_all',
                               PHENOMENE = 'MECANIQUE',
                               MODELISATION = '3D'),
                            _F(GROUP_MA = ('cable0',
                                           'cable1',
                                           'cable2',
                                           'cable3',
                                           'cable4'),
                               PHENOMENE = 'MECANIQUE',
                               MODELISATION = 'BARRE')))

# Assign element sections
section = AFFE_CARA_ELEM(MODELE = model,
                         BARRE = _F(GROUP_MA = ('cable4',
                                                'cable3',
                                                'cable2',
                                                'cable1',
                                                'cable0'),
                                    SECTION = 'CERCLE',
                                    CARA = 'R',
                                    VALE = 2.8209E-2))

# Define materials
concrete = DEFI_MATERIAU(ELAS = _F(E = 4.E10,
                                   NU = 0.2,
                                   RHO = 2500.),
                         BPEL_BETON = _F(PERT_FLUA = 0))

steel = DEFI_MATERIAU(ELAS = _F(E = 1.93E11,
                                NU = 0.3,
                                RHO = 7850.),
                      BPEL_ACIER = _F(F_PRG = 1.94E9,
                                      FROT_COURB = 0.0,
                                      FROT_LINE = 1.5E-3))

# Assign materials
mater = AFFE_MATERIAU(MAILLAGE = mesh,
                      AFFE = (_F(GROUP_MA = 'vol_all',
                                 MATER = concrete,),
                              _F(GROUP_MA = ('cable0',
                                             'cable1',
                                             'cable2',
                                             'cable3',
                                             'cable4'),
                                 MATER = steel)))

# Body force and displacement boundary conditions
disp_bc = AFFE_CHAR_MECA(MODELE = model,
                         PESANTEUR = _F(GRAVITE = 9.81,
                                        DIRECTION = (0.,0.,-1.)),
                         DDL_IMPO = (_F(GROUP_NO = 'px',
                                        DY = 0.),
                                     _F(GROUP_NO = 'py',
                                        DX = 0.),
                                     _F(GROUP_NO = 'center_nodes',
                                        DX = 0.,
                                        DY = 0.),
                                     _F(GROUP_NO = 'bot_face',
                                        DZ = 0.)))

# Define reinforcement cable tension BC
cab01_bc = DEFI_CABLE_BP(MODELE = model,
                         CHAM_MATER = mater,
                         CARA_ELEM = section,
                         GROUP_MA_BETON = 'vol_all',
                         DEFI_CABLE = (_F(GROUP_MA = 'cable0',
                                          GROUP_NO_ANCRAGE = ('pbot0','ptop0')),
                                       _F(GROUP_MA = 'cable1',
                                          GROUP_NO_ANCRAGE = ('pbot1','ptop1'))),
                         TYPE_ANCRAGE = ('ACTIF', 'PASSIF'),
                         TENSION_INIT = init_tension,
                         RECUL_ANCRAGE = 0.001)

cab23_bc = DEFI_CABLE_BP(MODELE = model,
                         CHAM_MATER = mater,
                         CARA_ELEM = section,
                         GROUP_MA_BETON = 'vol_all',
                         DEFI_CABLE = (_F(GROUP_MA = 'cable2',
                                          GROUP_NO_ANCRAGE = ('pbot2','ptop2')),
                                       _F(GROUP_MA = 'cable3',
                                          GROUP_NO_ANCRAGE = ('pbot3','ptop3'))),
                         TYPE_ANCRAGE = ('ACTIF', 'PASSIF'),
                         TENSION_INIT = init_tension,
                         RECUL_ANCRAGE = 0.001)

cab4_bc = DEFI_CABLE_BP(MODELE = model,
                        CHAM_MATER = mater,
                        CARA_ELEM = section,
                        GROUP_MA_BETON = 'vol_all',
                        DEFI_CABLE = (_F(GROUP_MA = 'cable4',
                                         GROUP_NO_ANCRAGE = ('pbot4','ptop4'))),
                        TYPE_ANCRAGE = ('ACTIF', 'PASSIF'),
                        TENSION_INIT = init_tension,
                        RECUL_ANCRAGE = 0.001)
# Traction bcs
load_01 = AFFE_CHAR_MECA(MODELE = model,
                         RELA_CINE_BP = _F(CABLE_BP = cab01_bc,
                                           SIGM_BPEL = 'NON',
                                           RELA_CINE = 'OUI'))

load_23 = AFFE_CHAR_MECA(MODELE = model,
                         RELA_CINE_BP = _F(CABLE_BP = cab23_bc,
                                           SIGM_BPEL = 'NON',
                                           RELA_CINE = 'OUI'))

load_4 = AFFE_CHAR_MECA(MODELE = model,
                        RELA_CINE_BP = _F(CABLE_BP = cab4_bc,
                                          SIGM_BPEL = 'NON',
                                          RELA_CINE = 'OUI'))

# Set up time stepping
l_time = DEFI_LIST_REEL(DEBUT = 0.0,
                        INTERVALLE = (_F(JUSQU_A = 150.0,
                                         NOMBRE = 1),
                                      _F(JUSQU_A = 300.0, 
                                         NOMBRE = 1),
                                      _F(JUSQU_A = 450.0, 
                                         NOMBRE = 1),
                                      _F(JUSQU_A = 600.0, 
                                         NOMBRE = 1)),
                        INFO = 1)

# Apply gravity load
result0 = STAT_NON_LINE(MODELE = model,
                        CHAM_MATER = mater,
                        CARA_ELEM = section,
                        EXCIT = (_F(CHARGE = disp_bc),
                                 _F(CHARGE = load_01),
                                 _F(CHARGE = load_23),
                                 _F(CHARGE = load_4)),
                        COMPORTEMENT = (_F(RELATION = 'ELAS',
                                           GROUP_MA = 'vol_all'),
                                        _F(RELATION = 'SANS',
                                           GROUP_MA = 'cable')),
                        INCREMENT = _F(LIST_INST = l_time,
                                       INST_FIN = 150.0),
                        NEWTON = _F())

# Apply tensions in cables 0 and 1
result0 = CALC_PRECONT(reuse = result0,
                       ETAT_INIT = _F(EVOL_NOLI = result0),
                       MODELE = model,
                       CHAM_MATER = mater,
                       CARA_ELEM = section,
                       CABLE_BP = cab01_bc,
                       CABLE_BP_INACTIF = (cab23_bc, cab4_bc),
                       EXCIT = _F(CHARGE = disp_bc),
                       COMPORTEMENT = (_F(RELATION = 'ELAS',
                                          GROUP_MA = 'vol_all'),
                                       _F(RELATION = 'ELAS',
                                          GROUP_MA = 'cable')),
                       INCREMENT = _F(LIST_INST = l_time,
                                      INST_FIN = 300.0))

# Apply tensions in cables 2 and 3
result0 = CALC_PRECONT(reuse = result0,
                       ETAT_INIT = _F(EVOL_NOLI = result0),
                       MODELE = model,
                       CHAM_MATER = mater,
                       CARA_ELEM = section,
                       CABLE_BP = cab23_bc,
                       CABLE_BP_INACTIF = (cab4_bc),
                       EXCIT = (_F(CHARGE = disp_bc),
                                _F(CHARGE = load_01)),
                       COMPORTEMENT = (_F(RELATION = 'ELAS',
                                          GROUP_MA = 'vol_all'),
                                       _F(RELATION = 'ELAS',
                                          GROUP_MA = 'cable')),
                       INCREMENT = _F(LIST_INST = l_time,
                                      INST_FIN = 450.0))

# Apply tension in cable 4 
result0 = CALC_PRECONT(reuse = result0,
                       ETAT_INIT = _F(EVOL_NOLI = result0),
                       MODELE = model,
                       CHAM_MATER = mater,
                       CARA_ELEM = section,
                       CABLE_BP = cab4_bc,
                       EXCIT = (_F(CHARGE = disp_bc),
                                _F(CHARGE = load_01),
                                _F(CHARGE = load_23)),
                       COMPORTEMENT = (_F(RELATION = 'ELAS',
                                          GROUP_MA = 'vol_all'),
                                       _F(RELATION = 'ELAS',
                                          GROUP_MA = 'cable')),
                       INCREMENT = _F(LIST_INST = l_time,
                                      INST_FIN = 600.0))

# Add extra fields for post-processing
result0 = CALC_CHAMP(reuse = result0,
                     RESULTAT = result0,
                     CONTRAINTE = 'SIEF_ELNO')

# Write initial result
IMPR_RESU(FORMAT = 'MED',
          UNITE = 55,
          RESU = _F(RESULTAT = result0))

# Extract initial tension in the cables and write table
table1_i = RECU_TABLE(CO = cab01_bc,
                      NOM_TABLE = 'CABLE_BP')
IMPR_TABLE(TABLE = table1_i,
           SEPARATEUR = ',')

# Extract tension after loading and write table
table1_f = POST_RELEVE_T(ACTION = _F(OPERATION = 'EXTRACTION',
                                     INTITULE = 'cable0',
                                     RESULTAT = result0,
                                     NOM_CHAM = 'SIEF_ELNO',
                                     GROUP_NO = 'cable0',
                                     NOM_CMP = 'N'))
IMPR_TABLE(TABLE = table1_f,
           SEPARATEUR = ',')

# Extract stress profiles from the tables
ten1_i = RECU_FONCTION(TABLE = table1_i,
                       PARA_X = 'ABSC_CURV',
                       PARA_Y = 'TENSION',
                       FILTRE = _F(NOM_PARA = 'NOM_CABLE',
                                   VALE_K = 'cable0'))

ten1_0 = RECU_FONCTION(TABLE = table1_f,
                       PARA_X = 'ABSC_CURV',
                       PARA_Y = 'N',
                       FILTRE = _F(NOM_PARA = 'INST',
                                   VALE = 0))

ten1_150 = RECU_FONCTION(TABLE = table1_f,
                         PARA_X = 'ABSC_CURV',
                         PARA_Y = 'N',
                         FILTRE = _F(NOM_PARA = 'INST',
                                     VALE = 150))

ten1_300 = RECU_FONCTION(TABLE = table1_f,
                         PARA_X = 'ABSC_CURV',
                         PARA_Y = 'N',
                         FILTRE = _F(NOM_PARA = 'INST',
                                     VALE = 300))

ten1_450 = RECU_FONCTION(TABLE = table1_f,
                         PARA_X = 'ABSC_CURV',
                         PARA_Y = 'N',
                         FILTRE = _F(NOM_PARA = 'INST',
                                     VALE = 450))

ten1_600 = RECU_FONCTION(TABLE = table1_f,
                         PARA_X = 'ABSC_CURV',
                         PARA_Y = 'N',
                         FILTRE = _F(NOM_PARA = 'INST',
                                     VALE = 600))

# Write stress profiles
IMPR_FONCTION(FORMAT = 'TABLEAU',
              SEPARATEUR = ',',
              UNITE = 38,
              COURBE = (_F(FONCTION = ten1_i),
                        _F(FONCTION = ten1_0),
                        _F(FONCTION = ten1_150),
                        _F(FONCTION = ten1_300),
                        _F(FONCTION = ten1_450),
                        _F(FONCTION = ten1_600)))

# Verification tests (reference values are from CASTEM simulations)

#  CABLE 0 - Time 600s
TEST_RESU(RESU=(_F(INST = 600.0,
                   REFERENCE = 'SOURCE_EXTERNE',
                   RESULTAT = result0,
                   NOM_CHAM = 'SIEF_ELNO',
                   GROUP_NO = 'N1',
                   NOM_CMP = 'N',
                   VALE_CALC = 3.52144457E+06,
                   VALE_REFE = 3.519000E6,
                   CRITERE = 'RELATIF',
                   PRECISION = 3.E-2,
                   GROUP_MA = 'M5655'),
                _F(INST = 600.0,
                   REFERENCE = 'SOURCE_EXTERNE',
                   RESULTAT = result0,
                   NOM_CHAM = 'SIEF_ELNO',
                   GROUP_NO = 'N6',
                   NOM_CMP = 'N',
                   VALE_CALC = 3.54794644E+06,
                   VALE_REFE = 3.546000E6,
                   CRITERE = 'RELATIF',
                   PRECISION = 3.E-2,
                   GROUP_MA = 'M5660'),
                _F(INST = 600.0,
                   REFERENCE = 'SOURCE_EXTERNE',
                   RESULTAT = result0,
                   NOM_CHAM = 'SIEF_ELNO',
                   GROUP_NO = 'N11',
                   NOM_CMP = 'N',
                   VALE_CALC = 3.57304023E+06,
                   VALE_REFE = 3.597000E6,
                   CRITERE = 'RELATIF',
                   PRECISION = 3.5E-2,
                   GROUP_MA = 'M5664'),
                _F(INST = 600.0,
                   REFERENCE = 'SOURCE_EXTERNE',
                   RESULTAT = result0,
                   NOM_CHAM = 'SIEF_ELNO',
                   GROUP_NO = 'N16',
                   NOM_CMP = 'N',
                   VALE_CALC = 3.63085369E+06,
                   VALE_REFE = 3.635000E6,
                   CRITERE = 'RELATIF',
                   PRECISION = 1.E-2,
                   GROUP_MA = 'M5670'),
                _F(INST = 600.0,
                   REFERENCE = 'SOURCE_EXTERNE',
                   RESULTAT = result0,
                   NOM_CHAM = 'SIEF_ELNO',
                   GROUP_NO = 'N101',
                   NOM_CMP = 'N',
                   VALE_CALC = 3.60463283E+06,
                   VALE_REFE = 3.614000E6,
                   CRITERE = 'RELATIF',
                   PRECISION = 6.E-2,
                   GROUP_MA = 'M5674')))

# Finish
FIN()
