# Start
DEBUT()

#--------------------------------
# Read and modify mesh
#--------------------------------
# Read the linear mesh
mesh_lin = LIRE_MAILLAGE(FORMAT='MED')

# Change edge orientation 
mesh_lin = MODI_MAILLAGE(reuse = mesh_lin,
                         MAILLAGE = mesh_lin,
                         ORIE_PEAU_2D = _F(GROUP_MA=('top_edge',
                                                     'bot_edge',
                                                     'crack_top_surf',
                                                     'crack_bot_surf')))

# Convert linear elements to quadratic
# PREF_NOEUD allows you to set a prefix for the new nodes that are created. The default
# is NS. For large meshes it is preferable to use a letter that is not used by
# other entities.  The letter Z is used in this case.
mesh = CREA_MAILLAGE(MAILLAGE = mesh_lin,
                     LINE_QUAD = _F(TOUT = 'OUI',
                                    PREF_NOEUD = 'Z'))
                                  
# Barsoum
#mesh = MODI_MAILLAGE(reuse = mesh,
#                   MAILLAGE = mesh,
#                   MODI_MAILLE = _F(OPTION = 'NOEUD_QUART',
#                                    GROUP_NO_FOND = 'crack_tip'))

#--------------------------------
# Do simulation
#--------------------------------
# Assign plane strain element (D_PLAN)
model = AFFE_MODELE(MAILLAGE = mesh,
                    AFFE = _F(TOUT = 'OUI',
                              PHENOMENE = 'MECANIQUE',
                              MODELISATION = 'D_PLAN'))
                         
# Define material
elastic = DEFI_MATERIAU(ELAS = _F(E = 210000e6,
                                  NU = 0.3))

# Assign material
mater = AFFE_MATERIAU(MAILLAGE = mesh,
                      AFFE = _F(TOUT = 'OUI',
                                MATER = elastic))

# Assign BCs
bcs = AFFE_CHAR_MECA(MODELE = model,
                     DDL_IMPO = (_F(GROUP_NO = 'top_G',
                                    DY = 0.,
                                    DX = 0.),
                                 _F(GROUP_NO = 'top_D',
                                    DY = 0.)),
                     PRES_REP = _F(GROUP_MA =('top_edge', 'bot_edge'),
                                   PRES = -10e6))

# Run static simulation
result = MECA_STATIQUE(MODELE = model,
                       CHAM_MATER = mater,
                       EXCIT = _F(CHARGE = bcs))

# Add stress components and invariants to the result
result = CALC_CHAMP(reuse = result,
                    RESULTAT = result,
                    CRITERES = ('SIEQ_ELNO', 'SIEQ_NOEU'),
                    CONTRAINTE = ('SIGM_ELNO', 'SIGM_NOEU'))

# Write output MED file
IMPR_RESU(FORMAT = 'MED',
          UNITE = 80,
          RESU = _F(MAILLAGE = mesh,
                    RESULTAT = result,
                    NOM_CHAM = ('SIGM_NOEU','SIEQ_NOEU','DEPL')))

#--------------------------------
# Post-process
#--------------------------------
# Define crack faces
crack = DEFI_FOND_FISS(MAILLAGE = mesh,
                       SYME = 'NON',
                       FOND_FISS = _F(GROUP_NO = 'crack_tip'),
                       LEVRE_SUP = _F(GROUP_MA = 'crack_top_surf'),
                       LEVRE_INF = _F(GROUP_MA = 'crack_bot_surf'))

# Compute strain energy rate
r_max = 0.002 * 5
r_min = 0.002 * 2
G = CALC_G(OPTION = 'CALC_G',
           RESULTAT = result,
           THETA = _F(FOND_FISS = crack,
                      R_INF = r_min,
                      R_SUP = r_max))

GK = CALC_G(OPTION = 'CALC_K_G',
            RESULTAT = result,
            THETA = _F(FOND_FISS = crack,
                       R_INF = r_min,
                       R_SUP = r_max))

# Write table of strain energy values
IMPR_TABLE(TABLE = G)
IMPR_TABLE(TABLE = GK)

# Compute stress intensity factors
K = POST_K1_K2_K3(RESULTAT = result,
                  FOND_FISS = crack)

# Write table of stress intensity
IMPR_TABLE(TABLE = K)

#--------------------------------
# Verification
#--------------------------------
# G
G_ref = 192
TEST_TABLE(REFERENCE = 'ANALYTIQUE',
           PRECISION = 3.0000000000000001E-3,
           VALE_CALC = 192.511256986,
           VALE_REFE = 192,
           NOM_PARA = 'G',
           TABLE = G)

TEST_TABLE(REFERENCE = 'ANALYTIQUE',
           PRECISION = 3.0000000000000001E-3,
           VALE_CALC = 192.511256986,
           VALE_REFE = 192,
           NOM_PARA = 'G',
           TABLE = GK)

TEST_TABLE(REFERENCE = 'ANALYTIQUE',
           PRECISION = 3.0000000000000001E-3,
           VALE_CALC = 192.527315126,
           VALE_REFE = 192,
           NOM_PARA = 'G_IRWIN',
           TABLE = GK)

TEST_TABLE(REFERENCE = 'ANALYTIQUE',
           PRECISION = 0.012,
           VALE_CALC = 193.008786982,
           VALE_REFE = 192,
           NOM_PARA = 'G',
           TABLE = K)

# K1
K1_ref = 6.65e6
TEST_TABLE(REFERENCE = 'ANALYTIQUE',
           PRECISION = 3.0000000000000001E-3,
           VALE_CALC = 6665536.76861,
           VALE_REFE = 6.650000E6,
           NOM_PARA = 'K1',
           TABLE = GK)

TEST_TABLE(REFERENCE = 'ANALYTIQUE',
           PRECISION = 5.0000000000000001E-3,
           VALE_CALC = 6673866.14366,
           VALE_REFE = 6.650000E6,
           NOM_PARA = 'K1',
           TABLE = K)

# Finish
FIN()
