#!/usr/bin/env python
# coding: utf-8

# %load ssnv112b_upd.py

import gmsh
import sys
import math

gmsh.initialize()

# Use OpenCASCADE kernel
model = gmsh.model
occ = model.occ
mesh = model.mesh
model.add("ssnv112b")

# Geometry
ri=0.1
re=0.2
h=0.01

# Default element size
dens = 0.01;

# Points of construction
PO = occ.addPoint(0, 0, 0,  dens)
POP = occ.addPoint(0, 0, h,  dens)

# Points of the plane of the base z=0
A = occ.addPoint(ri, 0., 0,  dens)
B = occ.addPoint(re, 0., 0,  dens)
E = occ.addPoint((ri/(2**0.5)) , (ri/(2**0.5)), 0.,  dens)
F = occ.addPoint(re/(2**0.5) , re/(2**0.5) , 0.,  dens)

# Points of the plane supérieur z=h
PC = occ.addPoint(ri, 0., h,  dens)
PD = occ.addPoint(re, 0., h,  dens)
PG = occ.addPoint(ri*(2**0.5)/2 , ri*(2**0.5)/2 , h,  dens)
PH = occ.addPoint(re*(2**0.5)/2 , re*(2**0.5)/2 , h,  dens)

# Lines
LAB = occ.addLine(A, B)
LBF = occ.addCircleArc(B, PO, F)
LFE = occ.addLine(F, E)
LEA = occ.addCircleArc(E, PO, A)

LCD = occ.addLine(PC, PD)
LDH = occ.addCircleArc(PD, POP, PH)
LHG = occ.addLine(PH, PG)
LGC = occ.addCircleArc(PG, POP, PC)

LAC = occ.addLine(A, PC)
LBD = occ.addLine(B, PD)
LEG = occ.addLine(E, PG)
LFH = occ.addLine(F, PH)

# Surfaces
FACINF_loop = occ.addCurveLoop([LAB, LBF, LFE, LEA])
FACINF = occ.addPlaneSurface([FACINF_loop])
FACSUP_loop = occ.addCurveLoop([LCD, LDH, LHG, LGC])
FACSUP = occ.addPlaneSurface([FACSUP_loop])
FACEAB_loop = occ.addCurveLoop([-LBD, -LAB, LAC, LCD])
FACEAB = occ.addPlaneSurface([FACEAB_loop])
FACEAE_loop = occ.addCurveLoop([LGC, -LAC, -LEA, LEG])
FACEAE = occ.addSurfaceFilling(FACEAE_loop)
FACEEF_loop = occ.addCurveLoop([-LEG, -LFE, LFH, LHG])
FACEEF = occ.addPlaneSurface([FACEEF_loop])
extrados_loop = occ.addCurveLoop([-LDH, -LBD, LBF, LFH])
extrados = occ.addSurfaceFilling(extrados_loop)

# Volume
surf_loop = occ.addSurfaceLoop([FACSUP, FACEAB, extrados, FACINF, FACEEF, FACEAE])
VOLUME = occ.addVolume([surf_loop])

occ.synchronize()

# Physical groups for the points
A_g = model.addPhysicalGroup(0, [A])
model.setPhysicalName(0, A_g, 'A')
F_g = model.addPhysicalGroup(0, [F])
model.setPhysicalName(0, F_g, 'F')

# Physical groups for the faces
FACEAB_g = model.addPhysicalGroup(2, [FACEAB])
model.setPhysicalName(2, FACEAB_g, 'FACEAB')
FACEAE_g = model.addPhysicalGroup(2, [FACEAE])
model.setPhysicalName(2, FACEAE_g, 'FACEAE')
FACSUP_g = model.addPhysicalGroup(2, [FACSUP])
model.setPhysicalName(2, FACSUP_g, 'FACSUP')
FACEEF_g = model.addPhysicalGroup(2, [FACEEF])
model.setPhysicalName(2, FACEEF_g, 'FACEEF')
FACINF_g = model.addPhysicalGroup(2, [FACINF])
model.setPhysicalName(2, FACINF_g, 'FACINF')

# Physical group for the volume
VOLUME_g = model.addPhysicalGroup(3, [1])
model.setPhysicalName(3, VOLUME_g, 'VOLUME')

gmsh.option.setNumber('Mesh.Algorithm', 8)
gmsh.option.setNumber('Mesh.ElementOrder', 2)
gmsh.option.setNumber('Mesh.MshFileVersion', 2.2)
gmsh.option.setNumber('Mesh.MedFileMinorVersion', 0)
gmsh.option.setNumber('Mesh.SaveAll', 0)
gmsh.option.setNumber('Mesh.SaveGroupsOfNodes', 1)
gmsh.option.setNumber('Mesh.SecondOrderIncomplete', 1)

# Generate mesh
mesh.generate(3)

# Save mesh
gmsh.write("ssnv112b_upd.med")

gmsh.fltk.run()

gmsh.finalize()


