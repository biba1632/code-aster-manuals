.. role:: red
.. role:: blue
.. role:: green
.. role:: gray

.. _v6.03.120:

******************************************************************************************************
**V6.03.120**: FORMA21 - Thermomechanics and adaptive meshing of a cracked cylinder head
******************************************************************************************************

In this tutorial, we explore a thermoelastic calculation on a cracked metallic cylinder head in 
plane stress (for the mechanical part) and lumped (for the thermal part). In accordance with "good
practice", we use two distinct meshes: linear for the thermal simulation and quadratic for mechanics.

We first perform a thermal calculation (Model A) with P1 (linear) elements that converges with
adaptive refinement based on an indicator of spatial errors (:red:`CALC_ERREUR` + :blue:`'ERTH_ELEM'`) and
the refinement-coarsening operator (:red:`MACR_ADAP_MAIL` + :blue:`'RAFF_DERA'`).

In the second model (Model B), the two meshes are adapted jointly according to the same process during a 
chained thermo-mechanical calculation.  For the free adaptation of the mechanical mesh, we use the
residual indicator :blue:`"ERME_ELEM"`.

Options for "pre- and post-processing" of these calculations are discussed.
This tutorial can also be used to test the non-regression of various error indicators and 
refinement-coarsening procedures in thermomechanics. 

The models explored in this tutorial are:

* :ref:`v6.03.120.3`
* :ref:`v6.03.120.4`

.. _v6.03.120.1:

Reference problem
=================

.. _v6.03.120.1.1:

Geometry
--------

The model represents a cracked metallic cylinder head (16 MND5 steel).

.. _fig_v6.03.120.1.1-a:

.. figure:: v6.03.120/fig1_v6.03.120.svg
   :height: 300 px
   :align: center

   Schematic of thermal loads and geometry (Models A and B)

.. _fig_v6.03.120.1.1-c:

.. figure:: v6.03.120/fig3_v6.03.120.svg
   :height: 300 px
   :align: center

   Schematic of thermomechanical loads (Model B)

The key regions are designated: :green:`GM38` for all the volume meshed with :blue:`TRIA`,
:green:`GM33` for the outgoing heat flux, :green:`GM36 / 37` for the exchange conditions, 
:green:`GM39 / 40` for points that are fixed, :green:`GM34` for the distributed pressure, and 
:green:`GM35` for the area at which the average temperature is computed.

In the two models (A and B), we carry out isotropic transient linear thermal computations
(:red:`THER_LINEAIRE` or :red:`THER_NON_LINE`) in lumped mode (:blue:`PLAN_DIAG`) on a thermal 
mesh containing :blue:`TRIA3/SEG2` elements.

In the second model, this calculation is chained with an elastic calculation 
(:red:`MECA_STATIQUE` or :red:`STAT_NON_LINE`) in plane stress (:blue:`C_PLAN`) on a mechanical mesh 
containing :blue:`TRIA6/SEG3` elements.

.. _fig_v6.03.120.1.1-b:

.. figure:: v6.03.120/fig2_v6.03.120.png
   :height: 250 px
   :align: center

   Temperature field on the initial thermal mesh (Models A and B)

.. _fig_v6.03.120.1.1-d:

.. figure:: v6.03.120/forma21b_8.svg
   :height: 300 px
   :align: center

   Decrease of the strain energy during the process of free adaptation of the meshes (Model B)

.. _fig_v6.03.120.1.1-e:

.. figure:: v6.03.120/fig4_v6.03.120.png
   :height: 300 px
   :align: center

   Deformation of the mechanical mesh (Model B)

.. _v6.03.120.1.2:

Material properties
-------------------

The following material properties are assigned to the structure (:blue:`GROUP_MA = 'GM38'`):

* :math:`E` = 210 GPa, 
* :math:`\nu` = 0.2, 
* :math:`\rho C_p` = 5260 kJ/m :math:`^3`-C, 
* :math:`\lambda` = 33.5 W/m-C.

.. _v6.03.120.1.3:

Boundary conditions and loads
-----------------------------

The boundary conditions can be decomposed by zone as follows:

+-----------------------------------------------+--------------------------------+
| Geometric areas (:blue:`GROUP_NO / GROUP_MA`) | Loads                          |
+===============================================+================================+
| GM33                                          | FLUX_REP                       |
+-----------------------------------------------+--------------------------------+
|                                               | FLUN = −400 W/m :math:`^2`     |
+-----------------------------------------------+--------------------------------+
| GM36                                          | ECHANGE                        |
+-----------------------------------------------+--------------------------------+
|                                               | COEF_H = 1000 W/m :math:`^2`-C |
+-----------------------------------------------+--------------------------------+
|                                               | TEMP_EXT = 350 C               |
+-----------------------------------------------+--------------------------------+
| GM37                                          | ECHANGE                        |
+-----------------------------------------------+--------------------------------+
|                                               | COEF_H = 5000 W/m :math:`2`-C  |
+-----------------------------------------------+--------------------------------+
|                                               | TEMP_EXT = 150 C               |
+-----------------------------------------------+--------------------------------+
| GM39 / 40                                     | DDL_IMPO                       |
+-----------------------------------------------+--------------------------------+
|                                               | DX = DY = 0.0                  |
+-----------------------------------------------+--------------------------------+
| GM34                                          | PRES_REP                       |
+-----------------------------------------------+--------------------------------+
|                                               | PRES = −0.1 N                  |
+-----------------------------------------------+--------------------------------+

.. _v6.03.120.2:

Reference solution
==================

.. _v6.03.120.2.1:

Calculation method used for reference solutions
------------------------------------------------

In this problem it is not possible to compute an analytical solution. The reference solution used for 
the calculations of errors on the average temperature at :green:`GM35` (Model A) and on the strain 
energy (Models A and B), is an approximate solution obtained after a series of three uniform refinements. 
This uniform refinement procedurec an be controlled by a PYTHON loop and the operator 
:red:`MACR_ADAP_MAIL` option :blue:`UNIFORME`.

.. _v6.03.120.2.2:

Benchmark result
----------------

**Model A**:

* Strain energy (purely thermal) = -2016.80291 J
* Average temperature at :green:`GM35` = 170.2 C

**Model B**:

* Strain energy (thermo-mechanical) = 6.75073756 :math:`\times` 10 :math:`^{-5}` J

.. _v6.03.120.2.3:

Uncertainty about the solutions
-------------------------------

These are only approximate solutions obtained on a "quasi-converged" mesh.

.. _v6.03.120.2.4:

Bibliographical references
---------------------------

1) X.DESROCHES. "Zhu-Zienkiewicz error estimators in 2D elasticity". :ref:`[R4.10.01] <r4.10.01>`, 1994.
2) X.DESROCHES. "Residual error estimator". :ref:`[R4.10.02] <r4.10.02>`, 2000.
3) O. BOITEAU. "Indicators of spatial error in residue for the transient thermal". 
   :ref:`[R4.10.03] <r4.10.03>`,2001.
4) O. BOITEAU. "Course and practical work Error indicators & Mesh adaptation; State of the art and
   implementation in Code_Aster". 2002.
5) O. BOITEAU. "FORMA04: Mechanical adaptive mesh on a beam in bending". :ref:`[V6.03.119] <v6.03.119>`, 2002.

.. _v6.03.120.3:

**Model A**: Adaptive mesh refinement for a purely thermal problem
===================================================================

.. _v6.03.120.3.1:

Characteristics of model
------------------------

The geometry is meshed with :blue:`TRIA3` elements. The simulation is static linear isotropic thermal and
uses the operator :red:`THER_LINEAIRE` in lumped mode (:blue:`PLAN_DIAG`).

We calculate the spatial error indicator for the residual (:blue:`ERTH_ELEM`). To use the error indicator
during post-processing, it is necessary to smooth the thermal flux at the Gauss points by projecting
the values to the nodes (:blue:`FLUX_ELNO`).  To post-process the error indicator, it is necessary to 
transform it from a element-based :blue:`CHAM_ELEM` to a :blue:`CHAM_ELEM` at nodes per element. We also 
determine the value of the average temperature at :green:`GM35` (:red:`POST_RELEVE_T`) and that of the 
strain energy (:red:`POST_ELEM`).

Everything is placed in a PYTHON loop for the implementation of a free refinement in ``nb_calc = 4`` steps 
(via :red:`MACR_ADAP_MAIL` option :blue:`LIBRE = 'RAFF_DERA'`) coupled to the previously extracted error 
indicator. This process is controlled by the component :blue:`ERTREL` of :blue:`ERTH_ELEM` (relative 
component of the residual error indicator).  We use the criteria :blue:`CRIT_RAFF_PE = 0.2` and 
:blue:`CRIT_DERA_PE = 0.1` (refines 20% of the worst elements and one coarsens 10% of the best).

One can thus observe the convergence of the values of the temperature and energy, the increase in their 
relative errors compared to the errors provided by the indicator (which are themselves relative and on 
the entire structure), variations in the performance indices of the indicator and its correct verification 
of the saturation hypothesis.

In order to illustrate "good practice", we uses the special options of :red:`LIRE_MAILLAGE`, 
:red:`MACR_ADAP_MAIL` and :red:`MACR_INFO_MAIL`.

The command file can be downloaded from :download:`forma21a_upd.comm <v6.03.120/forma21a_upd.comm>`.
The file is listed below.

.. include::  v6.03.120/forma21a_upd.comm
     :literal:

.. _v6.03.120.3.2:

Mesh characteristics
--------------------

* Initially: 1619 TRIA3, 102 SEG2, 911 nodes 
* After free refinement: 3088 TRIA3, 134 SEG2, 1681 nodes 
* After two free refinements: 6105 TRIA3, 180 SEG2, 3253 nodes 
* After three free refinements: 12345 TRIA3, 245 SEG2, 6462 nodes 
* After four free refinements: 25063 TRIA3, 347 SEG2, 12962 nodes

.. _v6.03.120.3.3:

Results and verification tests
------------------------------

.. _fig_v6.03.120.3.3-a:

.. figure:: v6.03.120/fig6_v6.03.120.png
   :height: 300 px
   :align: center

   The absolute error component (ERTABS) of the error indicator

.. _fig_v6.03.120.3.1-a:

.. figure:: v6.03.120/fig5_v6.03.120.png
   :height: 300 px
   :align: center

   Isovalues of the exchange component (TERME2) of the error indicator

.. _fig_v6.03.120.3.1-b:

.. figure:: v6.03.120/forma21a_8.svg
   :height: 300 px
   :align: center

   Decreases of the relative errors of the strain energy and the average temperature compared to 
   that of the total component indicator relative (ERTREL)

We test the values of the relative errors on average of the temperature and strain energy  compared to the 
reference solutions (cf :numref:`v6.03.120.2.2`) on the initial mesh and after four free refinements. The 
test relative tolerance is, on the initial errors, fixed at 10 :math:`^{-6}` %, and relaxed on the errors 
after four refinements to 10 :math:`^{-4}` %.

These verification tests are carried out on PYTHON variables (via :red:`TEST_FONCTION`) previously computed
using (:red:`FORMULE`).

+-----------------+------------------+--------------------+----------------------------------------+----------+
| Identification  | Reference Values | Tolerance          | Relative error                         | Variable |
+=================+==================+====================+========================================+==========+
| Energy (0)      | 0.491819%        | 10 :math:`^{-6}` % | 1.10 :math:`\times` 10 :math:`^{-11}`  | ERREEN0  |
+-----------------+------------------+--------------------+----------------------------------------+----------+
| Energy (4)      | 0.016287%        | 10 :math:`^{-4}` % | 3.05 :math:`\times` 10 :math:`^{-12}`  | ERREEN4  |
+-----------------+------------------+--------------------+----------------------------------------+----------+
| Temperature (0) | 4.797588%        | 10 :math:`^{-6}` % | 2.42 :math:`\times` 10 :math:`^{-12}`  | ERRETM0  |
+-----------------+------------------+--------------------+----------------------------------------+----------+
| Temperature (4) | 0.042547%        | 10 :math:`^{-4}` % | -6.65 :math:`\times` 10 :math:`^{-13}` | ERRETM4  |
+-----------------+------------------+--------------------+----------------------------------------+----------+

.. _v6.03.120.3.4:

Remarks
-------

It should be kept in mind that as a "simple post-processing" of the thermo-mechanical problem, unfortunately 
the indicator cannot provide a more reliable diagnosis in the areas where the solution of the initial problem 
stumbles (cracks, corners, multi-material, embedding, shock etc.). It is therefore necessary to start an 
adaptation process (UNIFORM or LIBRE), with a mesh that has already been refined a little by the user near 
the zones of discontinuities (materials, geometric etc.).

:red:`MACR_ADAP_MAIL` does not have a regularization process, therefore a bad initial mesh will produce, 
even when coupled with an indicator, probably a bad adapted mesh! As in mechanics, the sequence "thermal 
operators / :red:`MACR_ADAP_MAIL` option :blue:`"LIBRE"`" allows the mesh to converge optimally. We 
can, moreover, "juggle" with the components of the thermal indicator and the boundary, "fictitious" or not, 
to guide the construction of a refined or coarsened mesh by region 
(cf :ref:`[R4.10.03] Sec 6.3 <r4.10.3.6.3>`).

.. _v6.03.120.4:

**Model B** Adaptive meshing using coupled thermo-mechanics
============================================================

.. _v6.03.120.4.1:

Characteristics of model
------------------------

The coupled thermo-mechanical model requires two meshes for better results: a linear mesh for the thermal
part and a quadratic mesh for the mechanical part.  In this tutorial, the thermal mesh consists of 
:blue:`TRIA3` elements while the mechanical mesh contains :blue:`TRIA6` elements.  We loosely couple a
quasistatic isotropic linear thermal calculation (via :red:`THER_LINEAIRE` with :blue:`PLAN_DIAG` elements) 
and a linear elastic calculation (via :red:`STAT_NON_LINE` with :blue:`C_PLAN` elements).

We calculate the spatial residual error indicator maps for the purely thermal and purely mechanical 
cases (:blue:`ERTH_ELEM` and :blue:`ERME_ELEM`).  Before the error indicators are computed, it is necessary 
smooth nodal projection of the heat flux and the stresses at the Gauss points (:blue:`FLUX_ELNO` and 
:blue:`SIEF_ELNO`). To post-process the error indicators, it is necessary to transform the data from an
element-based :blue:`CHAM_ELEM` to an element-node based :blue:`CHAM_ELEM`. We also determine the value of 
the strain energy (:red:`POST_ELEM`).

A PYTHON loop is used to compute a progressively refined mesh in ``nb_calc = 2`` steps (via 
:red:`MACR_ADAP_MAIL` option :blue:`LIBRE = 'RAFF_DERA'`) that depends on the previously computed
error indicator. We control this process:

* by component :blue:`ERTREL` of :blue:`ERTH_ELEM` (relative component of the residual error indicator)
  for the thermal mesh,

* by the component :blue:`NUEST` of :blue:`ERME_ELEM` (relative component of the residual error indicator)
  for the mechanical mesh.

We use the criteria :blue:`CRIT_RAFF_PE = 0.2` and :blue:`CRIT_DERA_PE = 0.1` (refines 20% of the worst
elements and coarsen 10% of the best elements).

After each thermal computation we project the temperature field from the thermal mesh on to the mechanical 
mesh (via :red:`PROJ_CHAMP`).

We will store the strain energy computed at each iteration of the refinement loop to examine the
convergence of energy.  If desired, we can also compare the increase of its relative error with the 
errors provided by the indicators (which are themselves relative and computed on the whole structure), 
the differences between various indicators to explore the effectiveness of the indicators and their 
correct verification of convergence and saturation.

The command file can be downloaded from :download:`forma21b_upd.comm <v6.03.120/forma21b_upd.comm>`.
The file is listed below.

.. include::  v6.03.120/forma21b_upd.comm
     :literal:


.. _v6.03.120.4.2:

Mesh characteristics
--------------------

**Thermal mesh**

* Initially: 1619 TRIA3, 102 SEG2, 911 nodes 
* After free refinement: 3088 TRIA3, 134 SEG2, 1681 nodes 
* After two free refinements: 6105 TRIA3, 180 SEG2, 3253 nodes

**Mechanical mesh**

* Initially: 1619 TRIA6, 102 SEG3, 3443 nodes 
* After free refinement: 2881 TRIA6, 152 SEG3, 6065 nodes 
* After two free refinements: 5319 TRIA6, 180 SEG3, 11097 nodes

.. _v6.03.120.4.3:

Results and verification
------------------------

.. _fig_v6.03.120.4.3-a:

.. figure:: v6.03.120/fig8_v6.03.120.png
   :height: 300 px
   :align: center

   Displacements of the refined mesh after two iterations.

.. _fig_v6.03.120.4.3-b:

.. figure:: v6.03.120/fig7_v6.03.120.png
   :height: 300 px
   :align: center

   Isovalues of the error indicator (NUEST). 

The verification tests follow the same principles as those for Model A.

+-----------------+------------------+--------------------+----------------------------------------+----------+
| Identification  | Reference Values | Tolerance          | Relative error                         | Variable |
+=================+==================+====================+========================================+==========+
| Energy (0)      | 10.077761%       | 10 :math:`^{-6}` % | 4.79 :math:`\times` 10 :math:`^{-12}`  | ERREEN0  |
+-----------------+------------------+--------------------+----------------------------------------+----------+
| Energy (4)      | 0.459330%        | 10 :math:`^{-4}` % | -1.03 :math:`\times` 10 :math:`^{-12}` | ERREEN2  |
+-----------------+------------------+--------------------+----------------------------------------+----------+


.. _v6.03.120.4.4:

Remarks
-------

In thermo-mechanics, different mesh adaptation strategies are available to the user:

* adapt the mesh only according to a thermal criterion,

* alternatively, only according to a mechanical criterion,

* adapt first according to a thermal criterion, then according to a mechanical criterion (two separate 
  adaptation loops).

* adapt jointly according to a thermal criterion then mechanical (a loop as in this benchmark),

* adapt according to a thermo-mechanical criterion. 

In ``Code_Aster``, one does not have access to explicitly thermo-mechanical indicators, although the
mechanical indicators may incidentally have thermal dependence.

According to the needs of the study (thermal or rather mechanical, mesh convergence, better taking into 
account of certain boundary conditions, etc.) we can use one of the first four strategies.

A good practice during a thermo-mechanical calculation is to use P1 lumped elements in thermal calculation
and P2 in mechanics. That process requires two meshes and to project the thermal field linear solution on 
to the quadratic mechanical mesh (via :red:`PROJ_CHAMP`).

Nevertheless, if one wishes to work with only one mesh, one can easily decide one of the four 
strategies via the option :blue:`MAJ_CHAM` of :red:`MACR_ADAP_MAIL`. This allows, while adapting the mesh 
according to a thermal criterion (or mechanics), to update the complementary field, mechanical (or thermal), 
on the new adapted mesh.

.. _v6.03.120.5:

Summary of results
==================

In this tutorial we have outlined the steps needed to perform adaptive thermo-elastic computations on a 
cracked metallic cylinder in plane stress (for the mechanical part) and lumped (for the thermal part). In 
accordance with good practice, one uses two distinct meshes: linear for thermal and quadratic for mechanics.

We first carried out (Model A) the thermal computation with adaptive mesh convergence using free adaptivity
of a P1 mesh with a coupling of spatial error indicator (:red:`CALC_ERREUR` + :blue:`'ERTH_ELEM'`)
/refinement-coarsening (:red:`MACR_ADAP_MAIL` :blue:`'RAFF_DERA'`).

In the second model (Model B), the two meshes are adapted jointly according to the same process during a 
chained thermo-mechanical calculation. For the free adaptation of the mechanical mesh, we resort to the 
indicator for the residual :blue:`"ERME_ELEM"`.

The objectives of this tutorial are multiple:

* to become familiar with and put into practice two issues: calculation of error indicator and mesh 
  adaptation strategies; both for standard cases, but also for pathological cases and for chaining of 
  calculations,

* to detail the various parameters of the operators (:red:`CALC_ERREUR`, :red:`MACR_ADAP_MAIL`) and 
  related operators which can be particularly interesting for these issues (:red:`INFO_MAILLAGE`, 
  :red:`MACR_INFO_MAIL`, :red:`PROJ_CHAMP` etc.),

* to provide "good practice" advice for studies and the use of tools already available on the subject. We are 
  only interested in the aspects of meshed geometry, the actual mesh and type of finite elements. We do not 
  dwell here on the problems of time, calibration of numerical parameters and on sensitivity aspects 
  vis-à-vis data,

* to illustrate the potential and facilities afforded by the combination of Python and ``Code_Aster`` in 
  the command file of a study (test, loop, display, calculation, macro-command, interactivity etc.). The 
  official verification tests are calibrated for batch operation and some of these aspects have therefore 
  been ignored in the command files.

From a computer validation point of view, this tutorial allows regression test various couplings 
calculations of error map / procedure of refinement-coarsening in thermo-mechanical computations, and also 
the options of "pre- and post-processing" of these computations (smoothing of stresses and thermal flows 
at the nodes, conversion from an error by element to an error at the nodes per element).

