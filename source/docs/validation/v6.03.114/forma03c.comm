# Start
DEBUT()

# Read the mesh
mesh = LIRE_MAILLAGE(FORMAT='MED')

# Change edge orientation for load application
mesh = MODI_MAILLAGE(reuse = mesh,
                     MAILLAGE = mesh,
                     ORIE_PEAU_2D = _F(GROUP_MA = 'top'))

# Assign plane stress elements to mesh
model = AFFE_MODELE(MAILLAGE = mesh,
                    AFFE = _F(TOUT = 'OUI',
                              PHENOMENE = 'MECANIQUE',
                              MODELISATION = 'C_PLAN'))

# Read the stress-strain curve from file
sig_eps = LIRE_FONCTION(UNITE = 21,
                        NOM_PARA = 'EPSI',
                        PROL_DROITE = 'CONSTANT')

# Define material
steel = DEFI_MATERIAU(ELAS = _F(E = 200000.0,
                                NU = 0.3),
                      TRACTION = _F(SIGM = sig_eps))

# Assign material
mater = AFFE_MATERIAU(MAILLAGE = mesh,
                      AFFE = _F(TOUT = 'OUI',
                                MATER = steel))

# Symmetry BCs
symm_bc = AFFE_CHAR_CINE(MODELE = model,
                         MECA_IMPO = (_F(GROUP_MA = 'bottom',
                                         DY = 0.0),
                                      _F(GROUP_MA = 'left',
                                         DX = 0.0)))

# Force BC
load_bc = AFFE_CHAR_MECA(MODELE = model,
                         FORCE_CONTOUR = _F(GROUP_MA = 'top',
                                            FY = 1.0))

# Ramp function for load
ramp = DEFI_FONCTION(NOM_PARA = 'INST',
                     VALE = (   0.0,    0.0,
                             1000.0, 1000.0))

# Max time
t_final = 243.0

#-------------------------------------------------------
# Find limit load manually 
#-------------------------------------------------------
# 1. Pre-defined load steps
time = DEFI_LIST_REEL(DEBUT = 0.0,
                      INTERVALLE = _F(JUSQU_A = t_final,
                                      NOMBRE = 10))
t_fixed = DEFI_LIST_INST(DEFI_LIST = _F(LIST_INST = time),
                         ECHEC = _F(EVENEMENT = 'ERREUR',
                                    ACTION = 'DECOUPE'))

# Compute static nonlinear von Mises tabular plasticity
res_fix = STAT_NON_LINE(MODELE = model,
                        CHAM_MATER = mater,
                        EXCIT = (_F(CHARGE = symm_bc),
                                 _F(CHARGE = load_bc,
                                    FONC_MULT = ramp)),
                        COMPORTEMENT = _F(RELATION = 'VMIS_ISOT_TRAC'),
                        INCREMENT = _F(LIST_INST = t_fixed, 
                                       INST_FIN = t_final),
                        NEWTON = _F(REAC_ITER =1))

# 2. Automatic load steps
t_auto = DEFI_LIST_INST(METHODE = 'AUTO',
                        DEFI_LIST = _F(VALE = (0., 50., t_final),
                                       PAS_MINI = 1.e-6,
                                       PAS_MAXI = 100.))

# Compute static nonlinear von Mises tabular plasticity
res_auto = STAT_NON_LINE(MODELE = model,
                         CHAM_MATER = mater,
                         EXCIT = (_F(CHARGE = symm_bc),
                                  _F(CHARGE = load_bc,
                                     FONC_MULT = ramp)),
                         COMPORTEMENT = _F(RELATION = 'VMIS_ISOT_TRAC'),
                         INCREMENT = _F(LIST_INST = t_auto, 
                                        INST_FIN = t_final),
                         NEWTON = _F(REAC_ITER =1))

# Save element nodal stresses and interpolated stresses
res_fix = CALC_CHAMP(reuse = res_fix,
                     RESULTAT = res_fix,
                     CONTRAINTE = ('SIGM_ELNO', 'SIGM_NOEU'))

# Interpolate the internal variables to the nodes
res_fix = CALC_CHAMP(reuse = res_fix,
                     RESULTAT = res_fix,
                     VARI_INTERNE = ('VARI_ELNO', 'VARI_NOEU'))

# Write the results
IMPR_RESU(FORMAT = 'MED',
          UNITE = 80,
          RESU = _F(RESULTAT = res_fix))

# Save element nodal stresses and interpolated stresses
res_auto = CALC_CHAMP(reuse = res_auto,
                      RESULTAT = res_auto,
                      CONTRAINTE = ('SIGM_ELNO', 'SIGM_NOEU'))

# Interpolate the internal variables to the nodes
res_auto = CALC_CHAMP(reuse = res_auto,
                      RESULTAT = res_auto,
                      VARI_INTERNE = ('VARI_ELNO', 'VARI_NOEU'))

# Write the results
IMPR_RESU(FORMAT = 'MED',
          UNITE = 81,
          RESU = _F(RESULTAT = res_auto))

#-------------------------------------------------------
# Find limit load in a displacement controlled manner 
#-------------------------------------------------------
# Pre-defined load steps
t_final2 = 6.0
time2 = DEFI_LIST_REEL(DEBUT = 0.0,
                       INTERVALLE = _F(JUSQU_A = t_final2,
                                       NOMBRE = 10))
t_fixed2 = DEFI_LIST_INST(DEFI_LIST = _F(LIST_INST = time2),
                          ECHEC = _F(EVENEMENT = 'ERREUR',
                                     ACTION = 'DECOUPE'))

# Compute static nonlinear von Mises tabular plasticity
res_pil = STAT_NON_LINE(MODELE = model,
                        CHAM_MATER = mater,
                        EXCIT = (_F(CHARGE = symm_bc),
                                 _F(CHARGE = load_bc,
                                    TYPE_CHARGE = 'FIXE_PILO')),
                        COMPORTEMENT = _F(RELATION = 'VMIS_ISOT_TRAC'),
                        INCREMENT = _F(LIST_INST = t_fixed2, 
                                        INST_FIN = t_final2),
                        NEWTON = _F(REAC_ITER =1),
                        PILOTAGE = _F(TYPE = 'DDL_IMPO',
                                      COEF_MULT = 1.0,
                                      GROUP_NO = 'G',
                                      NOM_CMP = 'DY'))

# Save element nodal stresses and interpolated stresses
res_pil = CALC_CHAMP(reuse = res_pil,
                     RESULTAT = res_pil,
                     CONTRAINTE = ('SIGM_ELNO', 'SIGM_NOEU'))

# Interpolate the internal variables to the nodes
res_pil = CALC_CHAMP(reuse = res_pil,
                     RESULTAT = res_pil,
                     VARI_INTERNE = ('VARI_ELNO', 'VARI_NOEU'))

# Extract the automatically applied force as a function of time
f_pil = RECU_FONCTION(RESULTAT = res_pil,
                      NOM_PARA_RESU = 'ETA_PILOTAGE')

# Save the force in a table
IMPR_FONCTION(FORMAT = 'TABLEAU',
              COURBE = _F(FONCTION = f_pil),
              UNITE=8)

# Write the results
IMPR_RESU(FORMAT = 'MED',
          UNITE = 82,
          RESU = _F(RESULTAT = res_pil))

# Verification tests: without displacement control
TEST_RESU(RESU = _F(GROUP_NO = 'G',
                    INST = 243.0,
                    RESULTAT = res_fix,
                    NOM_CHAM = 'SIGM_NOEU',
                    NOM_CMP = 'SIYY',
                    VALE_CALC = 243.058194461))

TEST_RESU(RESU = _F(GROUP_NO = 'G',
                    INST = 243.0,
                    REFERENCE = 'ANALYTIQUE',
                    RESULTAT = res_fix,
                    NOM_CHAM = 'SIGM_NOEU',
                    NOM_CMP = 'SIYY',
                    VALE_CALC = 243.058194461,
                    VALE_REFE = 243.0,
                    PRECISION = 2.9999999999999997E-4))

# Verification tests: with displacement control
TEST_RESU(RESU = _F(GROUP_NO='G',
                    INST = 6.0,
                    REFERENCE = 'ANALYTIQUE',
                    RESULTAT = res_pil,
                    NOM_CHAM = 'SIGM_NOEU',
                    NOM_CMP = 'SIYY',
                    VALE_CALC = 243.289510614,
                    VALE_REFE = 243.0,
                    PRECISION = 3.0000000000000001E-3))

TEST_RESU(RESU = _F(GROUP_NO = 'G',
                    INST = 6.0,
                    REFERENCE = 'AUTRE_ASTER',
                    RESULTAT = res_pil,
                    NOM_CHAM = 'SIGM_NOEU',
                    NOM_CMP = 'SIYY',
                    VALE_CALC = 243.289510614,
                    VALE_REFE = 243.058194461,
                    PRECISION = 3.0000000000000001E-3))

TEST_RESU(RESU = _F(GROUP_NO = 'G',
                    INST = 6.0,
                    RESULTAT = res_pil,
                    NOM_CHAM = 'SIGM_NOEU',
                    NOM_CMP = 'SIYY',
                    VALE_CALC = 243.289510614))
  
# Test the compute forces
TEST_FONCTION(VALEUR = _F(VALE_CALC = 243.2274781927,
                          VALE_REFE = 243.0,
                          VALE_PARA = 6.0,
                          REFERENCE = 'ANALYTIQUE',
                          PRECISION = 3.0000000000000001E-3,
                          FONCTION = f_pil))

TEST_FONCTION(VALEUR = _F(VALE_CALC = 243.2274781927,
                          VALE_PARA = 6.0,
                          FONCTION = f_pil))
  
#Finish
FIN();
