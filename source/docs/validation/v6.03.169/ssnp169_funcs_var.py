#----------------------------------------------------------
# SSNP169 functions for variable load (function of theta)
#----------------------------------------------------------

import ssnp169_config as con
from ssnp169_funcs_con import *

# contact pressure
def press_v(x,y):
    r = radius(x,y)
    th = theta(x,y)    
    return ( 2*con.A21 + 6*con.C21/r**4 + 4*con.D21/r**2 )*math.cos(2*th)

# annulus polar displacements (external)
def Ur1_v(x,y):
    r = radius(x,y)
    th = theta(x,y)    
    return (1+con.nu1)/con.E1 * ( (-2*con.A21*r + 2*con.C21/r**3 + 4*con.D21/r) - con.nu1*(4*con.B21*r**3 + 4*con.D21/r) ) * math.cos(2*th)

def Uth1_v(x,y):
    r = radius(x,y)
    th = theta(x,y)    
    return (1+con.nu1)/con.E1 * ( (2*con.A21*r + 6*con.B21*r**3 + 2*con.C21/r**3 - 2*con.D21/r) - con.nu1*(4*con.B21*r**3 - 4*con.D21/r) ) * math.sin(2*th)

# annulus polar displacements (internal)
def Ur2_v(x,y):
    r = radius(x,y)
    th = theta(x,y)    
    return (1+con.nu2)/con.E2 * ( (-2*con.A22*r) - con.nu2*(4*con.B22*r**3) ) * math.cos(2*th)

def Uth2_v(x,y):
    r = radius(x,y)
    th = theta(x,y)    
    return (1+con.nu2)/con.E2 * ( (2*con.A22*r + 6*con.B22*r**3) - con.nu2*(4*con.B22*r**3) ) * math.sin(2*th)

# annulus cartesian displacements (external)
def ux1_v(x,y):
    r = radius(x,y)
    th = theta(x,y)
    return Ur1_v(x,y)*math.cos(th) - Uth1_v(x,y)*math.sin(th)

def uy1_v(x,y):
    r = radius(x,y)
    th = theta(x,y)
    return Ur1_v(x,y)*math.sin(th) + Uth1_v(x,y)*math.cos(th)

# annulus cartesian displacements (internal)
def ux2_v(x,y):
    r = radius(x,y)
    th = theta(x,y)
    return Ur2_v(x,y)*math.cos(th) - Uth2_v(x,y)*math.sin(th)

def uy2_v(x,y):
    r = radius(x,y)
    th = theta(x,y)
    return Ur2_v(x,y)*math.sin(th) + Uth2_v(x,y)*math.cos(th)

# annulus polar stresses (external)
def SIGMA_rr_1_v(x,y):
    r = radius(x,y)
    th = theta(x,y) 
    return ( -2*con.A21 - 6*con.C21/r**4 - 4*con.D21/r**2 )*math.cos(2*th)
    
def SIGMA_thth_1_v(x,y):
    r = radius(x,y)
    th = theta(x,y) 
    return ( 2*con.A21 + 12*con.B21*r**2 + 6*con.C21/r**4 )*math.cos(2*th)

def SIGMA_rth_1_v(x,y):
    r = radius(x,y)
    th = theta(x,y) 
    return 2*( con.A21 + 3*con.B21*r**2 - 3*con.C21/r**4 - con.D21/r**2 )*math.sin(2*th)
    
# annulus polar stresses (internal)
def SIGMA_rr_2_v(x,y):
    r = radius(x,y)
    th = theta(x,y) 
    return ( -2*con.A22 )*math.cos(2*th)
    
def SIGMA_thth_2_v(x,y):
    r = radius(x,y)
    th = theta(x,y) 
    return ( 2*con.A22 + 12*con.B22*r**2 )*math.cos(2*th)

def SIGMA_rth_2_v(x,y):
    r = radius(x,y)
    th = theta(x,y) 
    return 2*( con.A22 + 3*con.B22*r**2 )*math.sin(2*th)

# annulus cartesian stresses (external)
def SIGMA_XX_1_v(x,y):
    r = radius(x,y)
    th = theta(x,y) 
    c = math.cos(th)
    s = math.sin(th)     
    return SIGMA_rr_1_v(x,y)*c*c + SIGMA_thth_1_v(x,y)*s*s - SIGMA_rth_1_v(x,y)*2*s*c
    
def SIGMA_YY_1_v(x,y):
    r = radius(x,y)
    th = theta(x,y) 
    c = math.cos(th)
    s = math.sin(th)     
    return SIGMA_rr_1_v(x,y)*s*s + SIGMA_thth_1_v(x,y)*c*c + SIGMA_rth_1_v(x,y)*2*s*c
    
def SIGMA_ZZ_1_v(x,y):    
    r = radius(x,y)
    th = theta(x,y) 
    return con.nu1*( SIGMA_XX_1_v(x,y) + SIGMA_YY_1_v(x,y) )

def SIGMA_XY_1_v(x,y):
    r = radius(x,y)
    th = theta(x,y) 
    c = math.cos(th)
    s = math.sin(th)     
    return SIGMA_rr_1_v(x,y)*s*c - SIGMA_thth_1_v(x,y)*s*c + SIGMA_rth_1_v(x,y)*(c*c - s*s)

# annulus cartesian stresses (internal)
def SIGMA_XX_2_v(x,y):
    r = radius(x,y)
    th = theta(x,y) 
    c = math.cos(th)
    s = math.sin(th)     
    return SIGMA_rr_2_v(x,y)*c*c + SIGMA_thth_2_v(x,y)*s*s - SIGMA_rth_2_v(x,y)*2*s*c
    
def SIGMA_YY_2_v(x,y):
    r = radius(x,y)
    th = theta(x,y) 
    c = math.cos(th)
    s = math.sin(th)     
    return SIGMA_rr_2_v(x,y)*s*s + SIGMA_thth_2_v(x,y)*c*c + SIGMA_rth_2_v(x,y)*2*s*c

def SIGMA_ZZ_2_v(x,y):    
    r = radius(x,y)
    th = theta(x,y) 
    return con.nu2*( SIGMA_XX_2_v(x,y) + SIGMA_YY_2_v(x,y) )

def SIGMA_XY_2_v(x,y):
    r = radius(x,y)
    th = theta(x,y) 
    c = math.cos(th)
    s = math.sin(th)     
    return SIGMA_rr_2_v(x,y)*s*c - SIGMA_thth_2_v(x,y)*s*c + SIGMA_rth_2_v(x,y)*(c*c - s*s)

# annulus strains (external)
def EPSI_XX_1_v(x,y):    
    return 1/con.E1*( (1+con.nu1)*SIGMA_XX_1_v(x,y) - con.nu1*(SIGMA_XX_1_v(x,y)+SIGMA_YY_1_v(x,y)+SIGMA_ZZ_1_v(x,y)) )
    
def EPSI_YY_1_v(x,y):    
    return 1/con.E1*( (1+con.nu1)*SIGMA_YY_1_v(x,y) - con.nu1*(SIGMA_XX_1_v(x,y)+SIGMA_YY_1_v(x,y)+SIGMA_ZZ_1_v(x,y)) )
    
def EPSI_ZZ_1_v(x,y):    
    return 1/con.E1*( (1+con.nu1)*SIGMA_ZZ_1_v(x,y) - con.nu1*(SIGMA_XX_1_v(x,y)+SIGMA_YY_1_v(x,y)+SIGMA_ZZ_1_v(x,y)) )

def EPSI_XY_1_v(x,y):
    return 1/con.E1*( (1+con.nu1)*SIGMA_XY_1_v(x,y) )

# annulus strains (internal)
def EPSI_XX_2_v(x,y): 
    return 1/con.E2*( (1+con.nu2)*SIGMA_XX_2_v(x,y) - con.nu2*(SIGMA_XX_2_v(x,y)+SIGMA_YY_2_v(x,y)+SIGMA_ZZ_2_v(x,y)) )
    
def EPSI_YY_2_v(x,y):
    return 1/con.E2*( (1+con.nu2)*SIGMA_YY_2_v(x,y) - con.nu2*(SIGMA_XX_2_v(x,y)+SIGMA_YY_2_v(x,y)+SIGMA_ZZ_2_v(x,y)) )

def EPSI_ZZ_2_v(x,y):    
    return 1/con.E2*( (1+con.nu2)*SIGMA_ZZ_2_v(x,y) - con.nu2*(SIGMA_XX_2_v(x,y)+SIGMA_YY_2_v(x,y)+SIGMA_ZZ_2_v(x,y)) )

def EPSI_XY_2_v(x,y):
    return 1/con.E2*( (1+con.nu2)*SIGMA_XY_2_v(x,y) )

