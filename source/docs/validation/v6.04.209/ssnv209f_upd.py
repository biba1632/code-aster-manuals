
# coding: utf-8

import gmsh
import sys
import math

gmsh.initialize()

# Use OpenCASCADE kernel
model = gmsh.model
occ = model.occ
mesh = model.mesh
model.add("ssnv209f")

# Default element size and discretization
el_size = 0.2
N1 = 64
N2 = 10
N3 = 32

# Geometry
loc = 0.04
loc1 = -0.04*5/21.0
loc2 = 0.08
loc3 = 0.001
locR = 0.03875
epsi= 0.000001

xA = 0.
xB = 0.00125
xC = 0.005
xD = 0.0075
xE = 0.01125

# Add points, lines, and surface for base
pBA = occ.addPoint(xA, 0, 0, el_size)
pBS = occ.addPoint(loc2, 0, 0, el_size)
pCA = occ.addPoint(xA, loc1, 0, el_size)
pCS = occ.addPoint(loc2, loc1, 0, el_size)

lBase = occ.addLine(pBS, pBA)
lFixed = occ.addLine(pCS, pCA)
lBaseV1 = occ.addLine(pBA, pCA)
lBaseV2 = occ.addLine(pBS, pCS)

wBase = occ.addCurveLoop([lBase, lBaseV1, -lFixed, -lBaseV2])
sBase = occ.addPlaneSurface([wBase])

# Add points, lines, and surface for plate
pPA = occ.addPoint(xA, 0., 0, el_size)
pPB = occ.addPoint(xB, 0., 0, el_size)
pPC = occ.addPoint(xC, 0., 0, el_size)
pPD = occ.addPoint(xD, 0., 0, el_size)
pPE = occ.addPoint(xE, 0., 0, el_size)
pPS = occ.addPoint(loc2, 0., 0, el_size)
pHA = occ.addPoint(xA, loc, 0, el_size)
pHS = occ.addPoint(loc2, loc, 0, el_size)
pPI = occ.addPoint(loc, 0., 0, el_size)
pPR = occ.addPoint(locR, 0., 0, el_size)

lCont1 = occ.addLine(pPA, pPB)
lCont2 = occ.addLine(pPB, pPC)
lCont3 = occ.addLine(pPC, pPD)
lCont4 = occ.addLine(pPD, pPE)
lCont5 = occ.addLine(pPE, pPR)
lCont6 = occ.addLine(pPR, pPI)
lCont7 = occ.addLine(pPI, pPS)
lFixedX = occ.addLine(pPS, pHS)
lPresV = occ.addLine(pHA, pHS)
lPresH = occ.addLine(pPA, pHA)

wPlate = occ.addCurveLoop([lCont1, lCont2, lCont3, lCont4, lCont5, lCont6, lCont7, lFixedX, -lPresV, -lPresH])
sPlate = occ.addPlaneSurface([wPlate])

occ.synchronize()

# For transfinite interpolation (base)
for curve in [lBase, lFixed]:
    mesh.setTransfiniteCurve(curve, N1+1, meshType = "Progression", coef = 1)
for curve in [lBaseV1, lBaseV2]:
    mesh.setTransfiniteCurve(curve, 4, meshType = "Progression", coef = 1)

# For transfinite interpolation (plate)
for curve in [lCont1, lCont6]:
    mesh.setTransfiniteCurve(curve, 2, meshType = "Progression", coef = 1)
for curve in [lCont2, lCont4]:
    mesh.setTransfiniteCurve(curve, 4, meshType = "Progression", coef = 1)
for curve in [lCont3]:
    mesh.setTransfiniteCurve(curve, 3, meshType = "Progression", coef = 1)
for curve in [lCont5]:
    mesh.setTransfiniteCurve(curve, N3-9, meshType = "Progression", coef = 1)
for curve in [lCont7]:
    mesh.setTransfiniteCurve(curve, N3+1, meshType = "Progression", coef = 1)
for curve in [lFixedX, lPresH]:
    mesh.setTransfiniteCurve(curve, N2+1, meshType = "Progression", coef = 1)
for curve in [lPresV]:
    mesh.setTransfiniteCurve(curve, N1+1, meshType = "Progression", coef = 1)

mesh.setTransfiniteSurface(sBase)
mesh.setTransfiniteSurface(sPlate, cornerTags = [pPA, pPS, pHS, pHA])

for surf in occ.getEntities(2):
    mesh.setRecombine(2, surf[1])

# Physical groups for the points
pPA_g = model.addPhysicalGroup(0, [pPA])
pPB_g = model.addPhysicalGroup(0, [pPB])
pPC_g = model.addPhysicalGroup(0, [pPC])
pPD_g = model.addPhysicalGroup(0, [pPD])
pPE_g = model.addPhysicalGroup(0, [pPE])
pPR_g = model.addPhysicalGroup(0, [pPR])
pPI_g = model.addPhysicalGroup(0, [pPI])
pPS_g = model.addPhysicalGroup(0, [pPS])
pHA_g = model.addPhysicalGroup(0, [pHA])
pHS_g = model.addPhysicalGroup(0, [pHS])
pBA_g = model.addPhysicalGroup(0, [pBA])
pBS_g = model.addPhysicalGroup(0, [pBS])
pCA_g = model.addPhysicalGroup(0, [pCA])
pCS_g = model.addPhysicalGroup(0, [pCS])

# Physical groups for the edges
lPresH_g = model.addPhysicalGroup(1, [lPresH])
lPresV_g = model.addPhysicalGroup(1, [lPresV])
lFixedX_g = model.addPhysicalGroup(1, [lFixedX])
lConta_g = model.addPhysicalGroup(1, [lCont1, lCont2, lCont3, lCont4, lCont5, lCont6, lCont7])
lContaR_g = model.addPhysicalGroup(1, [lCont1, lCont2, lCont3, lCont4, lCont5])
lFixed_g = model.addPhysicalGroup(1, [lFixed])
lBase_g = model.addPhysicalGroup(1, [lBase])

# Physical groups for the faces
sBase_g = model.addPhysicalGroup(2, [sBase])
sPlate_g = model.addPhysicalGroup(2, [sPlate])

# Set names of groups
model.setPhysicalName(0, pPA_g, 'pPA')
model.setPhysicalName(0, pPB_g, 'pPB')
model.setPhysicalName(0, pPC_g, 'pPC')
model.setPhysicalName(0, pPD_g, 'pPD')
model.setPhysicalName(0, pPE_g, 'pPE')
model.setPhysicalName(0, pPR_g, 'pPR')
model.setPhysicalName(0, pPI_g, 'pPI')
model.setPhysicalName(0, pPS_g, 'pPS')
model.setPhysicalName(0, pHA_g, 'pHA')
model.setPhysicalName(0, pHS_g, 'pHS')
model.setPhysicalName(0, pBA_g, 'pBA')
model.setPhysicalName(0, pBS_g, 'pBS')
model.setPhysicalName(0, pCA_g, 'pCA')
model.setPhysicalName(0, pCS_g, 'pCS')

model.setPhysicalName(1, lPresH_g, 'lPresH')
model.setPhysicalName(1, lPresV_g, 'lPresV')
model.setPhysicalName(1, lFixedX_g, 'lFixedX')
model.setPhysicalName(1, lConta_g, 'lConta')
model.setPhysicalName(1, lContaR_g, 'lContaR')
model.setPhysicalName(1, lFixed_g, 'lFixed')
model.setPhysicalName(1, lBase_g, 'lBase')

model.setPhysicalName(2, sBase_g, 'sBase')
model.setPhysicalName(2, sPlate_g, 'sPlate')

# Extrude base
vBase = occ.extrude([(2, sBase)], 0, 0, loc3, numElements = [1], recombine = True )
# Extrude plate
vPlate = occ.extrude([(2, sPlate)], 0, 0, loc3, numElements = [1], recombine = True )

occ.synchronize()

vB = [dimtag[1] for dimtag in vBase if dimtag[0] == 3]
vP = [dimtag[1] for dimtag in vPlate if dimtag[0] == 3]

mesh.setTransfiniteVolume(vB[0])
mesh.setTransfiniteVolume(vP[0], cornerTags = [5, 10, 16, 19, 11, 12, 27, 28])

# Other physical groups based on the volumes
pHAP_g = model.addPhysicalGroup(0, [28])
pHSP_g = model.addPhysicalGroup(0, [27])

sPresH_g = model.addPhysicalGroup(2, [17])
sPresV_g = model.addPhysicalGroup(2, [16])
sFixedX_g = model.addPhysicalGroup(2, [15])
sConta_g = model.addPhysicalGroup(2, [8, 9, 10, 11, 12, 13, 14])
sContaR_g = model.addPhysicalGroup(2, [8, 9, 10, 11, 12])

sFixed_g = model.addPhysicalGroup(2, [5])
sBase3_g = model.addPhysicalGroup(2, [3])

vBase_g = model.addPhysicalGroup(3, [vB[0]])
vPlate_g = model.addPhysicalGroup(3, [vP[0]])
vTot_g = model.addPhysicalGroup(3, [vB[0], vP[0]])

# Set names of groups
model.setPhysicalName(0, pHAP_g, 'pHAP')
model.setPhysicalName(0, pHSP_g, 'pHSP')

model.setPhysicalName(2, sPresH_g, 'sPresH')
model.setPhysicalName(2, sPresV_g, 'sPresV')
model.setPhysicalName(2, sFixedX_g, 'sFixedX')
model.setPhysicalName(2, sConta_g, 'sConta')
model.setPhysicalName(2, sContaR_g, 'sContaR')
model.setPhysicalName(2, sFixed_g, 'sFixed')
model.setPhysicalName(2, sBase3_g, 'sBase3')

model.setPhysicalName(3, vBase_g, 'vBase')
model.setPhysicalName(3, vPlate_g, 'vPlate')
model.setPhysicalName(3, vTot_g, 'vTot')

occ.synchronize()

#gmsh.option.setNumber('Mesh.Algorithm', 8)
gmsh.option.setNumber('Mesh.RecombinationAlgorithm', 1)
gmsh.option.setNumber('Mesh.ElementOrder', 1)
gmsh.option.setNumber('Mesh.MshFileVersion', 2.2)
gmsh.option.setNumber('Mesh.MedFileMinorVersion', 0)
gmsh.option.setNumber('Mesh.SaveAll', 0)
gmsh.option.setNumber('Mesh.SaveGroupsOfNodes', 1)

# Generate mesh
mesh.generate(3)

# Save mesh
gmsh.write("ssnv209f_upd.med")

gmsh.fltk.run()

gmsh.finalize()


