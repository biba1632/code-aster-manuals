In this appendix, we will see that the block submatrix matrix $\BK_i$ can be made invertible
by appropriate ordering of the rows and columns (cond1_).  

**Definitions**

Recall that 

* $\BA$ is the $n \times n$ unconstrained stiffness matrix (symmetric and positive)
* $\BC$ is the $n \times p$ ($p < n$) constraint matrix : $\BC\Bu - \Bd = 0$.
* $\Bu$ is the $n \times 1$ vector of the physical degrees of freedom
* $\lambda^1$ is the $p \times 1$ vector of the first Lagrange multiplier degrees of freedom
* $\lambda^2$ is the $p \times 1$ vector of the second Lagrange multiplier degrees of freedom

The combined vector of degrees of freeom is
\Beq
  \Bx = (\Bu, \lambda^1, \lambda^2) \quad \in \mathbb{R}^n \times \mathbb{R}^p \times \mathbb{R}^p
\Eeq

We use the notation:

* $\BU$ for the global vector of physical degrees of freedom
* $\BLambda^1$ for the global vector of the first set of Lagrange multipliers
* $\BLambda^2$ for the global vector of the second set of Lagrange multipliers

The combined stiffness $\BK$ is a symmetric, $(n+2p) \times (n+2p)$, matrix given by
\Beq
   \BK = \begin{bmatrix}
           \BA & \BC^T & \BC^T \\
           \BC & -\alpha \BI & \alpha \BI \\
           \BC & \alpha \BI & -\alpha \BI 
         \end{bmatrix}
   \quad \alpha \in \mathbb{R}^{+}
\Eeq
This organization of matrix $\BK$ corresponds to the following ordering of the unknowns:
$\Bx = (\Bu, \lambda^1, \lambda^2)$. However, if we wish to obtain a 
$\BL\BD\BL^T$ factorization of this matrix (without permutation of the row/columns), we
cannot write the matrix in this order.  

In the following, we will take $\alpha = 1$ to simplify the discussion.

The rule that is used in ``Aster`` to determine
the correct order of the rows and columns is called **Rule R0** (discussed earlier):

.. admonition: Rule R0

  The two Lagrange multiplier degrees of freedom associated with the constraint equation
  $\BC_i - \BU - \Bd_i = 0$ frame the physical dofs constrained by this equation.

We seek to show that every sub-matrix $\BK_i$ of $\BK$ is invertible.  Recall that a
submatrix divides the original matrix into two parts and corresponds to a division of
the degrees of freedom: those before (and including) row $i$ and those after 
row $i$.

.. admonition: Notation 

  In the following discussion, we will use the following notation:

  * $\BUtilde$ is the subset of $\U$ that corresponds to dof of rows $\le i$.
  * $\BUhat$ is the subset of $\U$ that corresponds to dof of rows $\gt i$.
  * $L_1$ the set of pairs $(\lambda_1^1,\lambda_1^2)$ such that for the 
    associated constrained dofs we have $\text{range}(\lambda_1^1)
    < \text{range}(\lambda_1^2) \le i$.  
  * $L_1^1 = \{\lambda_1^1\}$ ~,~~ $L_1^2 = \{\lambda_1^2\}$
  * $L_2$ the set of pairs $(\lambda_2^1,\lambda_2^2)$ such that for the 
    associated constrained dofs we have $\text{range}(\lambda_2^1)
    \le i < \text{range}(\lambda_2^2)$.  
  * $L_2^1 = \{\lambda_2^1\}$ ~,~~ $L_2^2 = \{\lambda_1^2\}
  * $L_3$ the set of pairs $(\lambda_3^1,\lambda_3^2)$ such that for the 
    associated constrained dofs we have $i < \text{range}(\lambda_3^1)
    < \text{range}(\lambda_3^2)$.  
  * $L_3^1 = \{\lambda_3^1\}$ ~,~~ $L_3^2 = \{\lambda_3^2\}$

  The combined set is 
  \Beq
    L = \union_{i=1,3;\,j=1,2} L_i^j
  \Eeq

The constraint matrix $\BC$ can be divided (along rows) into three blocks $(\BC_1,\BC_2,\BC_3)$
that correspond to the sets $(L_1, L_2, L_3)$.  Each row block matrix $\BC_i$ can be
dvided into two column blocks $(\BCtilde_i, \BChat_i)$ that correspond to the dof 
blocks $\BUtilde$ and $\BUhat$.  That is, 
\Beq
  \BC = \begin{bmatrix} \BC_1 \\ \BC_2 \\ \BC_3 \end{bmatrix}
      = \begin{bmatrix} \BCtilde_1 & \BChat_1 \\ 
                        \BCtilde_2 & \BChat_2 \\ 
                        \BCtilde_3 & \BChat_3 \end{bmatrix}
\Eeq
Similarly, the matrix $\BA$ can be divided into blocks:
\Beq
  \BA = \begin{bmatrix} \BAtilde & \BAbar \\ 
                        \BAbar^T & \BAhat \end{bmatrix}
\Eeq
Now consider the constrained stiffness sub-matrix ($\BK_i$) organized as follows:
\Beq
  \BK_i = \begin{bmatrix}
            -\BI & \BI & 0 & \BCtilde_1 \\
            \BI & -\BI & 0 & \BCtilde_1 \\
            0 & 0 & -\BI & \BCtilde_2 \\
            \BCtilde_1^T & \BCtilde_1^T & \BCtilde_2^T & \BAtilde 
          \end{bmatrix}
\Eeq
This matrix cooresponds to the vector 
\Beq
  \BX_i = \begin{bmatrix}
            \lambda_1^1 \\ \lambda_1^2 \\ \lambda_2^1 \\ \BUtilde
          \end{bmatrix}
\Eeq
We have to show that the matrix $\BK_i$ is invertible, i.e.,
\Beq
  \BK_i \BX_i = 0 \quad  \implies \quad \BX_i = 0
\Eeq
This problem is equivalent to:

.. admonition:: Problem 1

   \Beq
      (S) = \begin{cases}
               -\lambda_1^1 + \lambda_1^2 + \BCtilde_1 \BUtilde = 0 \\
                \lambda_1^1 - \lambda_1^2 + \BCtilde_1 \BUtilde = 0 \\
               -\lambda_1^2 + \BCtilde_2 \BUtilde = 0 \\
               \BCtilde_1^T (\lambda_1^1 + \lambda_1^2) + \BCtilde_2^T \lambda_2^1 + \BAtilde \BUtilde = 0 
            \end{cases}
   \Eeq

   which implies that
   \Beq
     \BUtilde = 0~,~~\lambda_1^1 = \lambda_1^2 = 0 ~,~~ \lambda_2^1 = 0
   \Eeq

**General case:**

Suppose that $\BUtilde \ne \emptyset$, $L_1 \ne \emptyset$, $L_2 \ne \emptyset$.  Then
\Beq
   :label: appendix2_1
   \lambda_1^1 = \lambda_1^2
\Eeq

\Beq
   :label: appendix2_2
   \BCtilde_1 \BUtilde = 0
\Eeq

\Beq
   :label: appendix2_3
   \lambda_2^1 = \BCtilde_2 \BUtilde
\Eeq

\Beq
   :label: appendix2_4
   2 \BCtilde_1^T \lambda_1^1 + (\BCtilde_2^T\BCtilde_2 + \BAtilde) \BUtilde = 0 
\Eeq

From :eq:`appendix2_4` we observe that
\Beq
   2 \BUtilde^T \BCtilde_1^T \lambda_1^1 + \BUtilde^T (\BCtilde_2^T\BCtilde_2 + \BAtilde) \BUtilde = 0 
\Eeq
Using :eq:`appendix2_2`, we have $\BUtilde^T \BCtilde_1^ = 0$, and we obtain
\Beq
   \BUtilde^T \BCtilde_2^T\BCtilde_2 \BUtilde  + \BUtilde^T \BAtilde \BUtilde = 0 
\Eeq

Since $\BAtilde$ is a submatrix of a symmetric, positive, matrix, it is also symmetric and 
positive.  Also, $\BCtilde_2^T\BCtilde_2$ is a symmetric positive matrix.  Therefore, their sum
will be zero only if bot the terms are zero, i.e.,
\Beq
   \BUtilde^T \BCtilde_2^T\BCtilde_2 \BUtilde = 0 \quad \tand \quad
   \BUtilde^T \BAtilde \BUtilde = 0 
\Eeq
The first equation impies that
\Beq
   :label: appendix2_5
   \BCtilde_2 \BUtilde = 0 \quad \implies \quad \lambda_2^1 = 0
\Eeq
From the second equation we want to show that
\Beq
   :label: appendix2_6
   \BUtilde^T \BAtilde \BUtilde = 0 \quad \implies \quad \BUtilde = 0
\Eeq
We also have to show that $\lambda_1^1 = 0$.

* $ \BUtilde = 0 $ :

  In the decomposition of $\BU = (\BUtilde, \BUhat)$ let us set $\BUhat = 0$.  Then
  \Beq
    \BU^T \BA \BU = 0 \quad \implies \quad 
    \BUtilde^T \BAtilde \BUtilde = 0 
  \Eeq
  Therefore, in this case, $\BU \in \ker(\BA)$ and
  \Beq
    \BC\BU = \begin{bmatrix}
               \BC_1\BU \\ \BC_2\BU \\ \BC_3\BU 
             \end{bmatrix}
           = \begin{bmatrix}
               \BCtilde_1\BUtilde \\ \BCtilde_2\BUtilde \\ \BCtilde_3\BUtilde 
             \end{bmatrix}
           = \begin{bmatrix}
               0 \\ 0 \\ 0
             \end{bmatrix}
  \Eeq
  Also note that if $\BCtilde_3 \ne 0$, then there would exist dofs of $\BUtilde$ that are
  constrained by equations that have not yet been taken into account (rows $> i$) which is
  contrary to Rule R0.

  Therefore the vector $\BU$ is in the null-space of both $\BA$ and $\BC$.  We have to show that
  $\BU = 0$.  Consider the "single" Lagrange multiplier problem
  \Beq
     (S_2) : \begin{cases}
                \BA\BU + \BC^T\lambda = \BB \\
                \BC\BU = \Bd
             \end{cases}
  \Eeq
  Let us assume a solution $\BU_0 \ne 0$ such that $\BA\BU_0 = 0$ and $\BC\BU_0 = 0$.
  If $\BU_1$ is a solution of $(S_2)$, then $\BU_1 + \mu\BU_0$ is also a solution, i.e., the
  solution is not unique.  This is not possible because we have assumed that our problem is 
  well-posed.  Therefore, we must have $\BU = 0$, i.e., $\BUtilde = 0$.

* $ \lambda_1^1 = 0 $ :

  Equation :eq:`appendix2_4` gives
  \Beq
     :label: appendix2_7
     
     \BCtilde_1^T \lambda_1^1 = 0
  \Eeq

  Using Rule R0 (and using the same argument that was used to show that $\BCtilde_3 - 0$, we
  can show that $\BChat_1 = 0$.

  Equation :eq:`appendix2_6` gives
  \Beq
     :label: appendix2_8
     
     \BC_1^T \lambda_1^1 = 
        \begin{bmatrix}
          \BCtilde_1^T \lambda_1^1 \\ 
          \BChat_1^T \lambda_1^1 = 
        \end{bmatrix} = 
        \begin{bmatrix} 0 \\ 0 \end{bmatrix} = 0
  \Eeq
  If $\lambda_1^1 \ne 0$ but $\BC_1^T \lambda_1^1 = 0$, there exists a linear combination of
  the rows of $\BC_1$ which is zero, i.e., they are not independent.  This contradicts the 
  assumption that the constraints are linearly independent, which is reqired for physical
  well-posedness.

  Therefore, $\lambda_1^1 = 0$.
