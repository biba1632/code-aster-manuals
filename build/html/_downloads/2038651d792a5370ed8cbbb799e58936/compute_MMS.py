import compute_manuf_sol as MMS

u, f_surf_xp, f_surf_xm, f_surf_yp, f_surf_ym, \
f_surf_slave_t, f_surf_slave_nn, f_surf_slave, f_body = MMS.compute_MMS_sympy(1.0, 0.15)
#u, f_surf_xp, f_surf_xm, f_surf_yp, f_surf_ym, \
#f_surf_slave_t, f_surf_slave_nn, f_surf_slave, f_body = MMS.compute_MMS_sympy(1.0, 0.0)

ux = str(u[0])
uy = str(u[1])
uz = str(u[2])

f_xp_x = str(f_surf_xp[0])
f_xp_y = str(f_surf_xp[1])
f_xp_z = str(f_surf_xp[2])

f_xm_x = str(f_surf_xm[0])
f_xm_y = str(f_surf_xm[1])
f_xm_z = str(f_surf_xm[2])

f_yp_x = str(f_surf_yp[0])
f_yp_y = str(f_surf_yp[1])
f_yp_z = str(f_surf_yp[2])

f_ym_x = str(f_surf_ym[0])
f_ym_y = str(f_surf_ym[1])
f_ym_z = str(f_surf_ym[2])

f_t_x = str(f_surf_slave_t[0])
f_t_y = str(f_surf_slave_t[1])
f_t_z = str(f_surf_slave_t[2])

f_nn = str(f_surf_slave_nn.array.item())

f_b_x = str(f_body[0])
f_b_y = str(f_body[1])
f_b_z = str(f_body[2])

with open("ssnv219a_mms.py", "w") as text_file:
# with open("ssnv219e_mms.py", "w") as text_file:
    text_file.write("u_x = '%s'\n" % ux)
    text_file.write("u_y = '%s'\n" % uy)
    text_file.write("u_z = '%s'\n" % uz)
    text_file.write("f_xp_x = '%s'\n" % f_xp_x)
    text_file.write("f_xp_y = '%s'\n" % f_xp_y)
    text_file.write("f_xp_z = '%s'\n" % f_xp_z)
    text_file.write("f_xm_x = '%s'\n" % f_xm_x)
    text_file.write("f_xm_y = '%s'\n" % f_xm_y)
    text_file.write("f_xm_z = '%s'\n" % f_xm_z)
    text_file.write("f_yp_x = '%s'\n" % f_yp_x)
    text_file.write("f_yp_y = '%s'\n" % f_yp_y)
    text_file.write("f_yp_z = '%s'\n" % f_yp_z)
    text_file.write("f_ym_x = '%s'\n" % f_ym_x)
    text_file.write("f_ym_y = '%s'\n" % f_ym_y)
    text_file.write("f_ym_z = '%s'\n" % f_ym_z)
    text_file.write("f_t_x = '%s'\n" % f_t_x)
    text_file.write("f_t_y = '%s'\n" % f_t_y)
    text_file.write("f_t_z = '%s'\n" % f_t_z)
    text_file.write("f_nn = '%s'\n" % f_nn)
    text_file.write("f_b_x = '%s'\n" % f_b_x)
    text_file.write("f_b_y = '%s'\n" % f_b_y)
    text_file.write("f_b_z = '%s'\n" % f_b_z)

