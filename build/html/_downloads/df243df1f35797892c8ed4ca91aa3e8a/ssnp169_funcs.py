# SSNP169 functions

import sys
sys.path.append('/home/banerjee/Packages/aster-stable/14.6/lib/aster')

from code_aster.Cata.Commands import FORMULE

from ssnp169_funcs_con import *
from ssnp169_funcs_var import *
from ssnp169_funcs_fused import *

import ssnp169_config as config

#-----------------------------------------
# Calculate the fields for the total load
#-----------------------------------------
# annulus displacement (external)
ux1s  =  FORMULE(VALE = 'ux1_c(X,Y)+ux1_v(X,Y)',
               ux1_c = ux1_c,
               ux1_v = ux1_v,
               NOM_PARA = ['X', 'Y'],)

uy1s  =  FORMULE(VALE = 'uy1_c(X,Y)+uy1_v(X,Y)',
               uy1_c = uy1_c,
               uy1_v = uy1_v,
               NOM_PARA = ['X', 'Y'],)

# annulus displacement (internal)
ux2s  =  FORMULE(VALE = 'ux2_c(X,Y)+ux2_v(X,Y)',
               ux2_c = ux2_c,
               ux2_v = ux2_v,
               NOM_PARA = ['X', 'Y'],)

uy2s  =  FORMULE(VALE = 'uy2_c(X,Y)+uy2_v(X,Y)',
               uy2_c = uy2_c,
               uy2_v = uy2_v,
               NOM_PARA = ['X', 'Y'],)

# annulus stresses (external)
SIG_XX_1  =  FORMULE(VALE = 'SIGMA_XX_1_c(X,Y)+SIGMA_XX_1_v(X,Y)',
                   SIGMA_XX_1_c = SIGMA_XX_1_c,
                   SIGMA_XX_1_v = SIGMA_XX_1_v,
                   NOM_PARA = ['X', 'Y'],)
SIG_YY_1  =  FORMULE(VALE = 'SIGMA_YY_1_c(X,Y)+SIGMA_YY_1_v(X,Y)',
                   SIGMA_YY_1_c = SIGMA_YY_1_c,
                   SIGMA_YY_1_v = SIGMA_YY_1_v,
                   NOM_PARA = ['X', 'Y'],)
SIG_ZZ_1  =  FORMULE(VALE = 'SIGMA_ZZ_1_c(X,Y)+SIGMA_ZZ_1_v(X,Y)',
                   SIGMA_ZZ_1_c = SIGMA_ZZ_1_c,
                   SIGMA_ZZ_1_v = SIGMA_ZZ_1_v,
                   NOM_PARA = ['X', 'Y'],)
SIG_XY_1  =  FORMULE(VALE = 'SIGMA_XY_1_c(X,Y)+SIGMA_XY_1_v(X,Y)',
                   SIGMA_XY_1_c = SIGMA_XY_1_c,
                   SIGMA_XY_1_v = SIGMA_XY_1_v,
                   NOM_PARA = ['X', 'Y'],)

# annulus stresses (internal)
SIG_XX_2  =  FORMULE(VALE = 'SIGMA_XX_2_c(X,Y)+SIGMA_XX_2_v(X,Y)',
                   SIGMA_XX_2_c = SIGMA_XX_2_c,
                   SIGMA_XX_2_v = SIGMA_XX_2_v,
                   NOM_PARA = ['X', 'Y'],)
SIG_YY_2  =  FORMULE(VALE = 'SIGMA_YY_2_c(X,Y)+SIGMA_YY_2_v(X,Y)',
                   SIGMA_YY_2_c = SIGMA_YY_2_c,
                   SIGMA_YY_2_v = SIGMA_YY_2_v,
                   NOM_PARA = ['X', 'Y'],)
SIG_ZZ_2  =  FORMULE(VALE = 'SIGMA_ZZ_2_c(X,Y)+SIGMA_ZZ_2_v(X,Y)',
                   SIGMA_ZZ_2_c = SIGMA_ZZ_2_c,
                   SIGMA_ZZ_2_v = SIGMA_ZZ_2_v,
                   NOM_PARA = ['X', 'Y'],)
SIG_XY_2  =  FORMULE(VALE = 'SIGMA_XY_2_c(X,Y)+SIGMA_XY_2_v(X,Y)',
                   SIGMA_XY_2_c = SIGMA_XY_2_c,
                   SIGMA_XY_2_v = SIGMA_XY_2_v,
                   NOM_PARA = ['X', 'Y'],)

# annulus strains (external)
EPS_XX_1  =  FORMULE(VALE = 'EPSI_XX_1_c(X,Y)+EPSI_XX_1_v(X,Y)',
                   EPSI_XX_1_c = EPSI_XX_1_c,
                   EPSI_XX_1_v = EPSI_XX_1_v,
                   NOM_PARA = ['X', 'Y'],)
EPS_YY_1  =  FORMULE(VALE = 'EPSI_YY_1_c(X,Y)+EPSI_YY_1_v(X,Y)',
                   EPSI_YY_1_c = EPSI_YY_1_c,
                   EPSI_YY_1_v = EPSI_YY_1_v,
                   NOM_PARA = ['X', 'Y'],)
EPS_ZZ_1  =  FORMULE(VALE = 'EPSI_ZZ_1_c(X,Y)+EPSI_ZZ_1_v(X,Y)',
                   EPSI_ZZ_1_c = EPSI_ZZ_1_c,
                   EPSI_ZZ_1_v = EPSI_ZZ_1_v,
                   NOM_PARA = ['X', 'Y'],)
EPS_XY_1  =  FORMULE(VALE = 'EPSI_XY_1_c(X,Y)+EPSI_XY_1_v(X,Y)',
                   EPSI_XY_1_c = EPSI_XY_1_c,
                   EPSI_XY_1_v = EPSI_XY_1_v,
                   NOM_PARA = ['X', 'Y'],)

# annulus strains (internal)
EPS_XX_2  =  FORMULE(VALE = 'EPSI_XX_2_c(X,Y)+EPSI_XX_2_v(X,Y)',
                   EPSI_XX_2_c = EPSI_XX_2_c,
                   EPSI_XX_2_v = EPSI_XX_2_v,
                   NOM_PARA = ['X', 'Y'],)
EPS_YY_2  =  FORMULE(VALE = 'EPSI_YY_2_c(X,Y)+EPSI_YY_2_v(X,Y)',
                   EPSI_YY_2_c = EPSI_YY_2_c,
                   EPSI_YY_2_v = EPSI_YY_2_v,
                   NOM_PARA = ['X', 'Y'],)
EPS_ZZ_2  =  FORMULE(VALE = 'EPSI_ZZ_2_c(X,Y)+EPSI_ZZ_2_v(X,Y)',
                   EPSI_ZZ_2_c = EPSI_ZZ_2_c,
                   EPSI_ZZ_2_v = EPSI_ZZ_2_v,
                   NOM_PARA = ['X', 'Y'],)
EPS_XY_2  =  FORMULE(VALE = 'EPSI_XY_2_c(X,Y)+EPSI_XY_2_v(X,Y)',
                   EPSI_XY_2_c = EPSI_XY_2_c,
                   EPSI_XY_2_v = EPSI_XY_2_v,
                   NOM_PARA = ['X', 'Y'],)

# pressure
pressure  =  FORMULE(VALE = ' -press_c(X,Y)-press_v(X,Y)',
                   press_c = press_c,
                   press_v = press_v,
                   NOM_PARA = ['X', 'Y'],)

p_app  =  FORMULE(VALE = ' +press_c(X,Y)+press_v(X,Y)',
                press_c = press_c,
                press_v = press_v,
                NOM_PARA = ['X', 'Y'],)

#-------------------------------------------
# Fused field declarations
#-------------------------------------------
# Displacement
ux  =  FORMULE(VALE = 'ux_f(X,Y)',
             ux_f = ux_f,
             NOM_PARA = ['X', 'Y'],)

uy  =  FORMULE(VALE = 'uy_f(X,Y)',
             uy_f = uy_f,
             NOM_PARA = ['X', 'Y'],)

# Stresses
SIG_XX  =  FORMULE(VALE = 'SIG_XX_f(X,Y)',
                 SIG_XX_f = SIG_XX_f,
                 NOM_PARA = ['X', 'Y'],)
SIG_YY  =  FORMULE(VALE = 'SIG_YY_f(X,Y)',
                 SIG_YY_f = SIG_YY_f,
                 NOM_PARA = ['X', 'Y'],)
SIG_ZZ  =  FORMULE(VALE = 'SIG_ZZ_f(X,Y)',
                 SIG_ZZ_f = SIG_ZZ_f,
                 NOM_PARA = ['X', 'Y'],)
SIG_XY  =  FORMULE(VALE = 'SIG_XY_f(X,Y)',
                 SIG_XY_f = SIG_XY_f,
                 NOM_PARA = ['X', 'Y'],)

# Strains
EPS_XX  =  FORMULE(VALE = 'EPS_XX_f(X,Y)',
                 EPS_XX_f = EPS_XX_f,
                 NOM_PARA = ['X', 'Y'],)
EPS_YY  =  FORMULE(VALE = 'EPS_YY_f(X,Y)',
                 EPS_YY_f = EPS_YY_f,
                 NOM_PARA = ['X', 'Y'],)
EPS_ZZ  =  FORMULE(VALE = 'EPS_ZZ_f(X,Y)',
                 EPS_ZZ_f = EPS_ZZ_f,
                 NOM_PARA = ['X', 'Y'],)
EPS_XY  =  FORMULE(VALE = 'EPS_XY_f(X,Y)',
                 EPS_XY_f = EPS_XY_f,
                 NOM_PARA = ['X', 'Y'],)

