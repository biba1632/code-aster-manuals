# Cube composed of six linear pyramid elements

import sys
import salome
import salome_notebook
from salome.smesh import smeshBuilder
import SMESH, SALOMEDS

ExportPATH="/home/banerjee/Salome/ssnp14/"

# Initialize
salome.salome_init()
notebook = salome_notebook.NoteBook()
smesh = smeshBuilder.New()

# Create mesh
mesh = smesh.Mesh()
smesh.SetName(mesh, 'mesh')

# Add nodes
node1 = mesh.AddNode(0.00000000E+00, 1.00000000E+00, 0.00000000E+00)
node2 = mesh.AddNode(1.00000000E+00, 1.00000000E+00, 0.00000000E+00)
node3 = mesh.AddNode(0.00000000E+00, 0.00000000E+00, 0.00000000E+00)
node4 = mesh.AddNode(1.00000000E+00, 0.00000000E+00, 0.00000000E+00)
node5 = mesh.AddNode(0.00000000E+00, 1.00000000E+00, 1.00000000E+00)
node6 = mesh.AddNode(1.00000000E+00, 1.00000000E+00, 1.00000000E+00)
node7 = mesh.AddNode(0.00000000E+00, 0.00000000E+00, 1.00000000E+00)
node8 = mesh.AddNode(1.00000000E+00, 0.00000000E+00, 1.00000000E+00)
node9 = mesh.AddNode(5.00000000E-01, 5.00000000E-01, 5.00000000E-01)

# Add groups for the nodes
node_1 = mesh.CreateEmptyGroup(SMESH.NODE, 'node_1')
node_2 = mesh.CreateEmptyGroup(SMESH.NODE, 'node_2')
node_3 = mesh.CreateEmptyGroup(SMESH.NODE, 'node_3')
node_4 = mesh.CreateEmptyGroup(SMESH.NODE, 'node_4')
node_5 = mesh.CreateEmptyGroup(SMESH.NODE, 'node_5')
node_6 = mesh.CreateEmptyGroup(SMESH.NODE, 'node_6')
node_7 = mesh.CreateEmptyGroup(SMESH.NODE, 'node_7')
node_8 = mesh.CreateEmptyGroup(SMESH.NODE, 'node_8')
node_9 = mesh.CreateEmptyGroup(SMESH.NODE, 'node_9')
nbAdd = node_1.Add([1])
nbAdd = node_2.Add([2])
nbAdd = node_3.Add([3])
nbAdd = node_4.Add([4])
nbAdd = node_5.Add([5])
nbAdd = node_6.Add([6])
nbAdd = node_7.Add([7])
nbAdd = node_8.Add([8])
nbAdd = node_9.Add([9])

# Add volume elements
vol1 = mesh.AddVolume([3, 4, 8, 7, 9])
vol2 = mesh.AddVolume([4, 2, 6, 8, 9])
vol3 = mesh.AddVolume([2, 1, 5, 6, 9])
vol4 = mesh.AddVolume([1, 3, 7, 5, 9])
vol5 = mesh.AddVolume([5, 7, 8, 6, 9])
vol6 = mesh.AddVolume([1, 3, 4, 2, 9])

# Add group for the volumes
cube = mesh.CreateEmptyGroup(SMESH.VOLUME, 'cube')
pyra1 = mesh.CreateEmptyGroup(SMESH.VOLUME, 'pyra1')
pyra2 = mesh.CreateEmptyGroup(SMESH.VOLUME, 'pyra2')
pyra3 = mesh.CreateEmptyGroup(SMESH.VOLUME, 'pyra3')
pyra4 = mesh.CreateEmptyGroup(SMESH.VOLUME, 'pyra4')
pyra5 = mesh.CreateEmptyGroup(SMESH.VOLUME, 'pyra5')
pyra6 = mesh.CreateEmptyGroup(SMESH.VOLUME, 'pyra6')
nbAdd = cube.Add([1, 2, 3, 4, 5, 6])
nbAdd = pyra1.Add([1])
nbAdd = pyra2.Add([2])
nbAdd = pyra3.Add([3])
nbAdd = pyra4.Add([4])
nbAdd = pyra5.Add([5])
nbAdd = pyra6.Add([6])

# Add face elements
face7 = mesh.AddFace([1, 3, 7, 5])
face8 = mesh.AddFace([2, 6, 8, 4])
face9 = mesh.AddFace([5, 6, 2, 1])
face10 = mesh.AddFace([3, 4, 8, 7])

# Add groups for the faces
left_face = mesh.CreateEmptyGroup( SMESH.FACE, 'left_face' )
right_face = mesh.CreateEmptyGroup( SMESH.FACE, 'right_face' )
top_face = mesh.CreateEmptyGroup( SMESH.FACE, 'top_face' )
bot_face = mesh.CreateEmptyGroup( SMESH.FACE, 'bot_face' )
nbAdd = left_face.Add([7])
nbAdd = right_face.Add([8])
nbAdd = top_face.Add([9])
nbAdd = bot_face.Add([10])

# Export mesh
mesh.ExportMED( r''+ExportPATH+'ssnp14d.mmed'+'')

# Update GUI object tree
if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()
