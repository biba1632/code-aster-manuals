# FORMA08: MODEL A : JOINT ELEMENTS

# Start
DEBUT(LANG='EN')

# Parameters
t_final = 10
num_steps = 10 * t_final

young_modulus = 100
poisson_ratio = 0.

G_c = 0.9
sig_c = 3
penalty = 0.00001

tangent_stiffness = 10
penalty_lag = 100

u_x = 0.0
u_y = 1.0
u_z = 0.0

# Read the mesh
mesh = LIRE_MAILLAGE(FORMAT='MED')

# Change orientation of faces on which cohesive elements will be applied
mesh = MODI_MAILLAGE(reuse = mesh,
                     MAILLAGE = mesh,
                     ORIE_FISSURE = _F(GROUP_MA = 'DCB_joint'),
                     INFO = 2)

# Assign elements
model = AFFE_MODELE(MAILLAGE = mesh,
                    AFFE= (_F(GROUP_MA = ('DCB_crack', 'DCB_beam'),
                              PHENOMENE = 'MECANIQUE',
                              MODELISATION = '3D'),
                           _F(GROUP_MA = 'DCB_joint',
                              PHENOMENE = 'MECANIQUE',
                              MODELISATION = '3D_JOINT')))

# Define and assign material
steel = DEFI_MATERIAU(ELAS = _F(E = young_modulus,
                                NU = poisson_ratio),
                      RUPT_FRAG = _F(GC = G_c,
                                     SIGM_C = sig_c,
                                     PENA_ADHERENCE = penalty,
                                     PENA_LAGR = penalty_lag,
                                     RIGI_GLIS = tangent_stiffness))

mater = AFFE_MATERIAU(MAILLAGE = mesh,
                      AFFE = _F(GROUP_MA = ('DCB_crack','DCB_beam','DCB_joint'),
                                MATER = steel))

# Boundary conditions
symm_bc = AFFE_CHAR_MECA(MODELE = model,
                         DDL_IMPO = (_F(GROUP_MA = 'joint_bot',
                                        DY = 0)))

disp_bc = AFFE_CHAR_MECA(MODELE = model,
                         FACE_IMPO = (_F(GROUP_MA  = 'disp_edge', 
                                         DX = u_x,
                                         DY = u_y,
                                         DZ = u_z)))

# Define time stepping
t_list = DEFI_LIST_REEL(DEBUT = 0,
                        INTERVALLE = (_F(JUSQU_A = 10,
                                         NOMBRE = 200 )))

l_times = DEFI_LIST_INST(DEFI_LIST = _F(LIST_INST = t_list),
                         ECHEC = _F(SUBD_METHODE = 'MANUEL',
                                    SUBD_PAS = 10))

t_save = DEFI_LIST_REEL(DEBUT = 0,
                        INTERVALLE = (_F(JUSQU_A = t_final,
                                         NOMBRE = num_steps)))

# Load multipliers
t_func = DEFI_FONCTION(NOM_PARA = 'INST',
                       VALE = (-1, 0, 
                                0, 0.0001, 
                                t_final, t_final))

# Compute solution
result = STAT_NON_LINE(MODELE = model,
                       CHAM_MATER = mater,
                       EXCIT = (_F(CHARGE = symm_bc),
                                _F(CHARGE = disp_bc,
                                   FONC_MULT = t_func)),
                       COMPORTEMENT = (_F(RELATION = 'ELAS', 
                                          GROUP_MA = ('DCB_crack','DCB_beam')),
                                       _F(RELATION = 'CZM_EXP_REG', 
                                          GROUP_MA = 'DCB_joint'),
#                                      _F(RELATION = 'CZM_LIN_REG',
#                                         GROUP_MA = 'DCB_joint'),
                                      ),
                       INCREMENT = _F(LIST_INST = l_times,
                                      INST_FIN = t_final),
                       NEWTON = _F(MATRICE = 'TANGENTE', 
                                   REAC_ITER = 1),
                       CONVERGENCE = _F(RESI_GLOB_RELA = 5.E-5,
                                        ITER_GLOB_MAXI = 20),
                       SOLVEUR = _F(METHODE = 'MUMPS', 
                                    NPREC = -1),
                       ARCHIVAGE = _F(LIST_INST = t_save))


#----------------------
# Post-processing
#----------------------
# Compute nodal forces
result = CALC_CHAMP(reuse = result,
                    RESULTAT = result,
                    FORCE = 'FORC_NODA',
                    GROUP_MA = 'DCB_crack')

# Save applied displacements
disp_app = POST_RELEVE_T(ACTION = _F(INTITULE = 'disp_control',
                                     OPERATION = 'EXTRACTION',
                                     GROUP_NO = 'ref_point',
                                     NOM_CHAM = 'DEPL',
                                     NOM_CMP = 'DY',
                                     RESULTAT = result,
                                     TOUT_ORDRE = 'OUI'))
IMPR_TABLE(TABLE = disp_app)

# Save force resultant in y-direction
f_res = POST_RELEVE_T(ACTION = _F(INTITULE = 'f_resultant',
                                  OPERATION = 'EXTRACTION',
                                  GROUP_NO = 'disp_edge_nodes',
                                  NOM_CHAM = 'FORC_NODA',
                                  RESULTANTE = 'DY',
                                  RESULTAT =  result,
                                  TOUT_ORDRE = 'OUI'))
IMPR_TABLE(TABLE = f_res)

# Save the results for visualization
mesh_V = CREA_MAILLAGE(MAILLAGE = mesh,
                       RESTREINT = _F(GROUP_MA = ('DCB_crack', 'DCB_beam')))

model_V = AFFE_MODELE(MAILLAGE = mesh_V,
                      AFFE = _F(TOUT = 'OUI', 
                                PHENOMENE = 'MECANIQUE',
                                MODELISATION = '3D',))

result_V = EXTR_RESU(RESULTAT = result, 
                     RESTREINT = _F(MODELE = model_V))

IMPR_RESU(FORMAT= 'MED', 
          RESU = _F(RESULTAT = result_V), 
          UNITE = 81)

#----------------------
# Verification
#----------------------
TEST_TABLE(CRITERE = 'RELATIF',
           REFERENCE = 'ANALYTIQUE',
           PRECISION = 1.E-2,
           VALE_CALC = 4.901842832,
           VALE_REFE = 4.8600000000000003,
           NOM_PARA = 'DY',
           TABLE = f_res,
           FILTRE = _F(NOM_PARA = 'INST',
                       VALE = 10.0))

# Finish
FIN()
