# SSNV128C
DEBUT(LANG = 'EN',
      IGNORE_ALARM=('MODELE1_63'))

# Read mesh
mesh = LIRE_MAILLAGE(VERI_MAIL = _F(VERIF = 'OUI'),
                     FORMAT = 'MED')

# Reorient edges of mesh
mesh = MODI_MAILLAGE(reuse = mesh,
                     MAILLAGE = mesh,
                     ORIE_PEAU_2D = (_F(GROUP_MA = 'LPRESV'),
                                     _F(GROUP_MA = 'LPRESH')))

# Define contact node group
mesh = DEFI_GROUP(reuse = mesh,
                  MAILLAGE = mesh,
                  CREA_GROUP_NO = _F(NOM = 'LCONTA',
                                     GROUP_MA = 'LCONTA'))
# Assign plane strain elements
model = AFFE_MODELE(MAILLAGE = mesh,
                    #DISTRIBUTION = _F(METHODE = 'CENTRALISE'),
                    AFFE = _F(TOUT = 'OUI',
                              PHENOMENE = 'MECANIQUE',
                              MODELISATION = 'D_PLAN'))

# Define material
plate = DEFI_MATERIAU(ELAS = _F(E = 1.3E11,
                                NU = 0.2))

# Assign material
mater = AFFE_MATERIAU(MAILLAGE = mesh,
                      AFFE = _F(TOUT = 'OUI',
                                MATER = plate))

# Assign BCs
bc = AFFE_CHAR_MECA(MODELE = model,
                    DDL_IMPO = (_F(GROUP_MA = 'LBATI',  
                                   DX = 0.0, DY = 0.0),
                                _F(GROUP_NO = 'PPS', 
                                   DX = 0.0, DY = 0.0),
                                _F(GROUP_MA = 'LBLOCX', 
                                   DX = 0.0)),
                    PRES_REP = (_F(GROUP_MA = 'LPRESV', 
                                   PRES = 5.E07),
                                _F(GROUP_MA = 'LPRESH', 
                                   PRES = 15.E07)))
                  
# Define discrete penalty contact with Coulomb friction
contact = DEFI_CONTACT(MODELE = model,
                       FORMULATION = 'DISCRETE',
                       FROTTEMENT = 'COULOMB',
                       REAC_GEOM = 'AUTOMATIQUE',
                       ZONE = _F(GROUP_MA_MAIT = 'LBATI',
                                 GROUP_MA_ESCL = 'LCONTA',
                                 APPARIEMENT = 'MAIT_ESCL',
                                 NORMALE = 'MAIT',
                                 ALGO_CONT = 'PENALISATION',
                                 COULOMB = 1.0,
                                 ALGO_FROT = 'PENALISATION',
                                 E_N = 1.E18,
                                 E_T = 1.0E11,
                                 COEF_MATR_FROT = 0.8))

# Define ramp function
f_ramp = DEFI_FONCTION(NOM_PARA = 'INST',
                       PROL_GAUCHE = 'LINEAIRE',
                       PROL_DROITE = 'LINEAIRE',
                       VALE = (0.0, 0.0,
                               1.0, 1.0))

# Define time stepping list
l_times = DEFI_LIST_REEL(DEBUT = 0.0,
                         INTERVALLE = _F(JUSQU_A = 1.0, 
                                         NOMBRE = 5))

#-----------------------------------------------------------
# Run simulation (default solver)
result = STAT_NON_LINE(SOLVEUR = _F(),
                       MODELE = model,
                       CHAM_MATER = mater,
                       EXCIT = (_F(CHARGE = bc,
                                   FONC_MULT = f_ramp)),
                       CONTACT = contact,
                       COMPORTEMENT = _F(RELATION = 'ELAS'),
                       NEWTON = _F(REAC_ITER = 1),
                       INCREMENT = _F(LIST_INST = l_times),
                       CONVERGENCE = _F(ARRET = 'OUI',
                                        ITER_GLOB_MAXI = 200,
                                        RESI_GLOB_RELA = 1.0E-5),
                       INFO = 1)

# Compute stress and contact pressure
result = CALC_CHAMP(reuse = result,
                    CONTRAINTE = ('SIGM_ELNO'),
                    VARI_INTERNE = ('VARI_ELNO'),
                    RESULTAT = result)

# Extract nodal displacements
disp = CREA_CHAMP(OPERATION = 'EXTR',
                  TYPE_CHAM = 'NOEU_DEPL_R',
                  NOM_CHAM = 'DEPL',
                  RESULTAT = result,
                  INST = 1.0)

# Write out results
IMPR_RESU(FORMAT = 'MED',
          UNITE = 80,
          RESU = _F(RESULTAT = result))

#-----------------------------------------------------------
# Run simulation (MUMPS solver)
result1 = STAT_NON_LINE(SOLVEUR = _F(METHODE = 'MUMPS'),
                        MODELE = model,
                        CHAM_MATER = mater,
                        EXCIT = (_F(CHARGE = bc,
                                    FONC_MULT = f_ramp)),
                        CONTACT = contact,
                        COMPORTEMENT = _F(RELATION = 'ELAS'),
                        NEWTON = _F(REAC_ITER = 1),
                        INCREMENT = _F(LIST_INST = l_times),
                        CONVERGENCE = _F(ARRET = 'OUI',
                                         ITER_GLOB_MAXI = 200,
                                         RESI_GLOB_RELA = 1.0E-5),
                        INFO = 1)

# Compute stress and contact pressure
result1 = CALC_CHAMP(reuse = result1,
                     CONTRAINTE = ('SIGM_ELNO'),
                     VARI_INTERNE = ('VARI_ELNO'),
                     RESULTAT = result1)

# Extract nodal displacements
disp1 = CREA_CHAMP(OPERATION = 'EXTR',
                   TYPE_CHAM = 'NOEU_DEPL_R',
                   NOM_CHAM = 'DEPL',
                   RESULTAT = result1,
                   INST = 1.0)

# Extract nodal contact data into a table
table1 = POST_RELEVE_T(ACTION = _F(INTITULE = 'CONT',
                                   GROUP_NO = 'LCONTA',
                                   RESULTAT = result1,
                                   NOM_CHAM = 'CONT_NOEU',
                                   INST = 1.0,
                                   TOUT_CMP = 'OUI',
                                   OPERATION = 'EXTRACTION'))

# Write table
IMPR_TABLE(TABLE = table1,
           SEPARATEUR = ',')

# Write out results
IMPR_RESU(FORMAT = 'MED',
          UNITE = 81,
          RESU = _F(RESULTAT = result1))

#-----------------------------------------------------------
# Verify results

# Result1
TEST_RESU(RESU = (_F(GROUP_NO = 'PPA',
                     INST = 1.0,
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result,
                     NOM_CHAM = 'DEPL',
                     NOM_CMP = 'DX',
                     VALE_CALC =  2.87322863E-05,
                     VALE_REFE = 2.8600000000000001E-05,
                     PRECISION = 0.050000000000000003),
                  _F(GROUP_NO = 'PPB',
                     INST = 1.0,
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result,
                     NOM_CHAM = 'DEPL',
                     NOM_CMP = 'DX',
                     VALE_CALC =  2.73714989E-05,
                     VALE_REFE = 2.72E-05,
                     PRECISION = 0.050000000000000003),
                  _F(GROUP_NO = 'PPC',
                     INST = 1.0,
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result,
                     NOM_CHAM = 'DEPL',
                     NOM_CMP = 'DX',
                     VALE_CALC =  2.30331880E-05,
                     VALE_REFE = 2.2799999999999999E-05,
                     PRECISION = 0.050000000000000003),
                  _F(GROUP_NO = 'PPD',
                     INST = 1.0,
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result,
                     NOM_CHAM = 'DEPL',
                     NOM_CMP = 'DX',
                     VALE_CALC =  2.00362304E-05,
                     VALE_REFE = 1.98E-05,
                     PRECISION = 0.050000000000000003),
                  _F(GROUP_NO = 'PPE',
                     INST = 1.0,
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result,
                     NOM_CHAM = 'DEPL',
                     NOM_CMP = 'DX',
                     VALE_CALC =  1.56680068E-05,
                     VALE_REFE = 1.5E-05,
                     PRECISION = 0.050000000000000003)))

# Result2
TEST_RESU(RESU = (_F(GROUP_NO = 'PPA',
                     INST = 1.0,
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result1,
                     NOM_CHAM = 'DEPL',
                     NOM_CMP = 'DX',
                     VALE_CALC =  2.87322863E-05,
                     VALE_REFE = 2.8600000000000001E-05,
                     PRECISION = 0.050000000000000003),
                  _F(GROUP_NO = 'PPB',
                     INST = 1.0,
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result1,
                     NOM_CHAM = 'DEPL',
                     NOM_CMP = 'DX',
                     VALE_CALC =  2.73714989E-05,
                     VALE_REFE = 2.72E-05,
                     PRECISION = 0.050000000000000003),
                  _F(GROUP_NO = 'PPC',
                     INST = 1.0,
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result1,
                     NOM_CHAM = 'DEPL',
                     NOM_CMP = 'DX',
                     VALE_CALC =  2.30331880E-05,
                     VALE_REFE = 2.2799999999999999E-05,
                     PRECISION = 0.050000000000000003),
                  _F(GROUP_NO = 'PPD',
                     INST = 1.0,
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result1,
                     NOM_CHAM = 'DEPL',
                     NOM_CMP = 'DX',
                     VALE_CALC =  2.00362304E-05,
                     VALE_REFE = 1.98E-05,
                     PRECISION = 0.050000000000000003),
                  _F(GROUP_NO = 'PPE',
                     INST = 1.0,
                     REFERENCE = 'SOURCE_EXTERNE',
                     RESULTAT = result1,
                     NOM_CHAM = 'DEPL',
                     NOM_CMP = 'DX',
                     VALE_CALC =  1.56680068E-05,
                     VALE_REFE = 1.5E-05,
                     PRECISION = 0.050000000000000003)))

# End
FIN()
